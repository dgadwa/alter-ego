/*******************************
       Popup Overrides
*******************************/

/**
 * Popup menus.
 */
.ui.menu_popup.popup,
.ui.morph.popup {
  text-align: initial;
  max-width: 90vw;
  min-width: initial;

  /**
   * Inverted needs border to have same size with non-inverted popups.
   */
  &.inverted {
    border: 1px solid transparent;
  }

  /* Grid integration: grid.overrides */
  .ui.nav.grid {
    .column {
      min-width: @mainMenuPopupColumnMinWidth;
      padding: @mainMenuPopupColumnPadding;

      .ui.menu {
        .item {
          margin: 0;
          border-radius: 0 !important;

          /* Don't show pointing arrows for active items. */
          &.active:after {
            display: none;
          }
        }
      }
    }
  }

  /**
   * No bottom margin for last child in vertical menu.
   */
  .ui.vertical.menu > .item:last-child {
    margin-bottom: 0;
  }

}

/**
 * Morph popups.
 */
.chap-morph {
  position: absolute;
  top: 0;
  /*rtl:begin:ignore*/
  left: 0;
  /*rtl:end:ignore*/
  right: 0;

  opacity: 0;
  pointer-events: none;
  display: block;
  z-index: @zIndex;

  transition-property: transform, opacity;
  will-change: transform, opacity;
  transform: perspective(2000px) rotateX(-15deg);
  transform-origin: 50% -50px;

  .ui.morph.popup {
    visibility: hidden;
    position: absolute;
    max-width: 100%;
    top: 0;
    /*rtl:begin:ignore*/
    left: 0;
    /*rtl:end:ignore*/
    padding: 0; /* Relocated to .chap-morph-content */
    margin: 0;
    transition-property: visibility, transform, width, height, background, border, color;
    will-change: visibility, transform, width, height;

    & > .chap-morph-arrow {
      position: absolute;
      width: @arrowSize;
      height: @arrowSize;

      background: @arrowBackground;
      transform: rotate(45deg);

      z-index: @arrowZIndex;
      box-shadow: @bottomArrowBoxShadow;

      top: @arrowOffset;
      margin-left: @arrowOffset;

      transition-property: transform, background;
      will-change: transform;
    }
    &.inverted > .chap-morph-arrow {
      background-color: @invertedArrowColor;
      box-shadow: none !important;
    }

    & > .chap-morph-container {
      height: 100%;
      overflow: hidden;

      & > .chap-morph-wrapper {
        opacity: 0;
        pointer-events: none;
        transition-property: transform, opacity;
        will-change: transform, opacity;

        &.active {
          opacity: 1;
          transform: translateX(0);
          pointer-events: auto;
        }

        &.left {
          transform: translateX(-300px);
        }

        &.right {
          transform: translateX(300px);
        }

        & > .chap-morph-content {
          position: absolute;
          top: 0;
          left: 0;
          padding: @verticalPadding @horizontalPadding; /* .ui.popup's padding */

          /* Immediate Nested Grid */
          & > .ui.grid:not(.padded):not(.nav) {
            width: @nestedGridWidth;
            margin: @nestedGridMargin;
          }
        }

      }

    }

    &.paddingless > .chap-morph-container > .chap-morph-wrapper > .chap-morph-content {
      padding: 0 !important;
    }

    /**
     * Outline bugged due to invisible element, use stronger shadow instead.
     */
    &:focus {
      outline-style: none;
      box-shadow: @floatingShadowHover;
    }

  }

}

.chap-morph-active + .chap-morph {
  opacity: 1;
  pointer-events: auto;
  transform: perspective(2000px) rotateX(0deg);

  .ui.morph.popup {
    visibility: visible;
  }
}

.chap-morph-no-transition + .chap-morph .ui.morph.popup {
  &,
  & > .chap-morph-arrow,
  & > .chap-morph-container,
  & > .chap-morph-container > .chap-morph-wrapper {
    transition: none !important;
  }
}
/*******************************
       Popup Overrides
*******************************/

/*******************************
        Header Overrides
*******************************/

/**
 * Header font style.
 */
.ui.header {
  font-style: @headerFontStyle;
}

/**
 * Display headers.
 */
.ui.huge.display.header {
  min-height: 1em;
  font-size: @hugeDisplayFontSize;
}
.ui.large.display.header {
  font-size: @largeDisplayFontSize;
}
.ui.display.header,
.ui.medium.display.header {
  font-size: @mediumDisplayFontSize;
}
.ui.small.display.header {
  font-size: @smallDisplayFontSize;
}
.ui.tiny.display.header {
  font-size: @tinyDisplayFontSize;
}
.ui.display.header > .icon {
  padding-top: 0;
}

/* Mobile */
@media only screen and (max-width: @largestMobileScreen) {
  .ui.huge.display.header {
    font-size: @hugeDisplayFontSize / @displayHeaderMobileRatio;
  }
  .ui.large.display.header {
    font-size: @largeDisplayFontSize / @displayHeaderMobileRatio;
  }
  .ui.display.header,
  .ui.medium.display.header {
    font-size: @mediumDisplayFontSize / @displayHeaderMobileRatio;
  }
  .ui.small.display.header {
    font-size: @smallDisplayFontSize / @displayHeaderMobileRatio;
  }
  .ui.tiny.display.header {
    font-size: @tinyDisplayFontSize / @displayHeaderMobileRatio;
  }
}

/* Tablet */
@media only screen and (min-width: @tabletBreakpoint) and (max-width: @largestTabletScreen) {
  .ui.huge.display.header {
    font-size: @hugeDisplayFontSize / @displayHeaderTabletRatio;
  }
  .ui.large.display.header {
    font-size: @largeDisplayFontSize / @displayHeaderTabletRatio;
  }
  .ui.display.header,
  .ui.medium.display.header {
    font-size: @mediumDisplayFontSize / @displayHeaderTabletRatio;
  }
  .ui.small.display.header {
    font-size: @smallDisplayFontSize / @displayHeaderTabletRatio;
  }
  .ui.tiny.display.header {
    font-size: @tinyDisplayFontSize / @displayHeaderTabletRatio;
  }
}

/* Small Monitor */
@media only screen and (min-width: @computerBreakpoint) and (max-width: @largestSmallMonitor) {
  .ui.huge.display.header {
    font-size: @hugeDisplayFontSize / @displayHeaderSmallMonitorRatio;
  }
  .ui.large.display.header {
    font-size: @largeDisplayFontSize / @displayHeaderSmallMonitorRatio;
  }
  .ui.display.header,
  .ui.medium.display.header {
    font-size: @mediumDisplayFontSize / @displayHeaderSmallMonitorRatio;
  }
  .ui.small.display.header {
    font-size: @smallDisplayFontSize / @displayHeaderSmallMonitorRatio;
  }
  .ui.tiny.display.header {
    font-size: @tinyDisplayFontSize / @displayHeaderSmallMonitorRatio;
  }
}

/**
 * When an invisible element precedes a header, make
 * the header still behave as the first element.
 */
.invisible + .ui.header {
  margin-top: @firstMargin;
}

/**
 * Left/right aligned icon headers.
 */
.ui.icon.header {
  &[class*="left aligned"] {
    .icon {
      text-align: left;
    }
  }
  &[class*="right aligned"] {
    .icon {
      text-align: right;
    }
  }
}

/**
 * Use muted text color for meta.
 */
.grid.loop {
  article.column {
    .content {
      .meta {
        color: @subHeaderColor;
      }
    }
  }
}
/*******************************
        Header Overrides
*******************************/

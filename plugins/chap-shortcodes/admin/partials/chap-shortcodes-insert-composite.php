<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

/**
 * Render the modal for inserting composites.
 *
 * @link       http://chap.website
 * @since      1.0.0
 *
 * @package    Chap_Shortcodes
 * @subpackage Chap_Shortcodes/admin/partials
 */

$id = 'chap-insert-composite';

/**
 * Render the modal for inserting composites.
 */
?>

<div id="<?php echo esc_attr($id); ?>" style="display:none;">
	<div class="media-modal-content chap-shortcode-modal">
		<div class="media-frame mode-select wp-core-ui">
			<div class="media-frame-title" style="left:0;">
				<h1><?php esc_html_e('Insert composite', 'chap-shortcodes'); ?></h1>
			</div>
			<button type="button" class="media-modal-close chap-modal-close">
				<span class="media-modal-icon">
					<span class="screen-reader-text"><?php esc_html_e('Close modal', 'chap-shortcodes'); ?></span>
				</span>
			</button>
			<div class="media-frame-router" style="left:0;">
				<div class="media-router">
					<a id="front-page" href="#" class="media-menu-item active"><?php esc_html_e('Front page', 'chap-shortcodes'); ?></a>
					<a id="pages" href="#" class="media-menu-item"><?php esc_html_e('Pages', 'chap-shortcodes'); ?></a>
					<a id="slides" href="#" class="media-menu-item"><?php esc_html_e('Slides', 'chap-shortcodes'); ?></a>
					<a id="misc" href="#" class="media-menu-item"><?php esc_html_e('Misc', 'chap-shortcodes'); ?></a>
				</div>
			</div>
			<div class="media-frame-content" style="left:0;">
				<p class="csc-modal-info">
					<?php printf(
						esc_html__('%sPlease note%s: It\'s highly recommended to keep the editor in %sText mode%s to maintain the indentation of shortcode composites.', 'chap-shortcodes'),
						'<strong>',
						'</strong>',
						'<a href="' . esc_url(get_plugin_url() . 'images/editor-text-mode.png') . '" target="_blank">',
						'</a>'
					); ?>
					<br />
					<?php printf(
						esc_html__('%sLearn more about composites.%s', 'chap-shortcodes'),
						'<a href="http://chap.website/composites" target="_blank" rel="noopener">',
						'</a>'
					); ?>
				</p>
				<?php foreach($this->composites as $composite): ?>
				<?php $composite->render_ui(); ?>
				<?php endforeach; ?>
			</div>
			<div class="media-frame-toolbar" style="left:0;">
				<div class="media-toolbar">
					<div class="media-toolbar-primary">
						<button id="insert-to-editor" type="button" class="button media-button button-primary button-large media-button-insert" disabled="disabled">
							<?php esc_html_e('Insert to editor', 'chap-shortcodes'); ?>
						</button>
						<button id="copy-to-clipboard" type="button" class="button media-button button-large media-button-insert" disabled="disabled">
							<?php esc_html_e('Copy to clipboard', 'chap-shortcodes'); ?>
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php

namespace Chap\Shortcodes;
use Chap\Helpers;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

/**
 * Render the admin UI for inserting shortcodes.
 *
 * @link       http://chap.website
 * @since      1.0.1
 *
 * @package    Chap_Shortcodes
 * @subpackage Chap_Shortcodes/admin/partials
 */

$shortcode_button_atts = [
	// 'id' => 'btn-chap-insert-shortcode',
	'title' => esc_html__('Insert shortcode', 'chap-shortcodes'),
	'href' => '#TB_inline?width=300&height=550&inlineId=chap-insert-shortcode',
	'classes' => ['thickbox', 'button', 'btn-csc-insert', 'btn-chap-insert-shortcode'],
	'data-editor' => $editor,
];

$composite_button_atts = [
	// 'id' => 'btn-chap-insert-composite',
	'title' => esc_html__('Insert composite', 'chap-shortcodes'),
	'href' => '#TB_inline?width=300&height=550&inlineId=chap-insert-composite',
	'classes' => ['thickbox', 'button', 'btn-csc-insert', 'btn-chap-insert-composite'],
	'data-editor' => $editor,
];

echo ' ';
echo Helpers\html_tag('a', $shortcode_button_atts) . esc_html__('Shortcode', 'chap-shortcodes') . '</a>';
echo '<div class="chap-button-divider"></div>';
echo Helpers\html_tag('a', $composite_button_atts) . esc_html__('Composite', 'chap-shortcodes') . '</a>';

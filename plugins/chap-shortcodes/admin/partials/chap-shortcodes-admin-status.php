<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

/**
 * Chap Shortcodes Status page.
 *
 * @link       http://chap.website
 * @since      1.0.0
 *
 * @package    Chap_Shortcodes
 * @subpackage Chap_Shortcodes/admin/partials
 */

global $plugin_chap_shortcodes;
$shortcodes = $plugin_chap_shortcodes->get_loaded_shortcodes();
?>


<h1><?php esc_html_e('Status', 'chap-shortcodes'); ?></h1>
<p><?php esc_html_e('Loaded shortcodes and aliases.', 'chap-shortcodes'); ?></p>

<table class="form-table shortcodes-table">
	<?php foreach($shortcodes as $id => $shortcode): ?>
	<tr>
		<th scope="row"><?php echo ucfirst($id); ?></th>
		<td><?php echo '[' . join(']<br />[', $shortcode->ids) . ']'; ?></td>
	</tr>
	<?php endforeach; ?>
</table>

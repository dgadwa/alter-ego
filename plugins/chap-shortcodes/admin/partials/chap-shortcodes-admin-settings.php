<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

/**
 * Chap Shortcodes settings page.
 *
 * @link       http://chap.website
 * @since      1.0.0
 *
 * @package    Chap_Shortcodes
 * @subpackage Chap_Shortcodes/admin/partials
 */
?>

<div class="wrap">

	<h1><?php esc_html_e('Chap Shortcodes', 'chap-shortcodes'); ?></h1>

	<form method="post" action="options.php">

		<?php settings_fields('chap-shortcodes'); ?>

		<table class="form-table">

			<tr valign="top">
				<th scope="row"><?php esc_html_e('Prefix', 'chap-shortcodes'); ?></th>
				<td>
					<input type="text" name="csc-prefix" value="<?php echo esc_attr(get_option('csc-prefix')); ?>" />
					<p class="description" id="csc-prefix-description"><?php esc_html_e('Custom prefix for Chap shortcodes.', 'chap-shortcodes'); ?></p>
				</td>
			</tr>

			<tr valign="top">
				<th scope="row"><?php esc_html_e('No prefix', 'chap-shortcodes'); ?></th>
				<td>
					<fieldset>
						<input type="checkbox" name="csc-no-prefix" value="1"<?php checked(get_option('csc-no-prefix') == 1); ?> />
						<?php printf(
							esc_html__('Enable the use of Chap shortcodes without a prefix. %1$sIf a different shortcode with that name already exists then %1$sthe chosen prefix must be used for that shortcode only. %1$sAll the sucessfully registered names for Chap Shortcodes are listed below.', 'chap-shortcodes'),
							'<br />'
						); ?>
					</fieldset>
				</td>
			</tr>

			<tr valign="top">
				<th scope="row"><?php esc_html_e('Semantic UI CSS file', 'chap-shortcodes'); ?></th>
				<td>
					<input type="text" class="regular-text" name="csc-sui-css" value="<?php echo esc_attr(get_option('csc-sui-css')); ?>" />
					<p class="description" id="csc-sui-css-description"><?php esc_html_e('Loaded only when Chap theme is not activated.', 'chap-shortcodes'); ?></p>
				</td>
			</tr>

			<tr valign="top">
				<th scope="row"><?php esc_html_e('Semantic UI JavaScript file', 'chap-shortcodes'); ?></th>
				<td>
					<input type="text" class="regular-text" name="csc-sui-js" value="<?php echo esc_attr(get_option('csc-sui-js')); ?>" />
					<p class="description" id="csc-sui-js-description"><?php esc_html_e('Loaded only when Chap theme is not activated.', 'chap-shortcodes'); ?></p>
				</td>
			</tr>

			<tr valign="top">
				<th scope="row"><?php esc_html_e('Swiper CSS file', 'chap-shortcodes'); ?></th>
				<td>
					<input type="text" class="regular-text" name="csc-swiper-css" value="<?php echo esc_attr(get_option('csc-swiper-css')); ?>" />
					<p class="description" id="csc-swiper-css-description"><?php esc_html_e('Loaded only when Chap theme is not activated.', 'chap-shortcodes'); ?></p>
				</td>
			</tr>

			<tr valign="top">
				<th scope="row"><?php esc_html_e('Swiper JavaScript file', 'chap-shortcodes'); ?></th>
				<td>
					<input type="text" class="regular-text" name="csc-swiper-js" value="<?php echo esc_attr(get_option('csc-swiper-js')); ?>" />
					<p class="description" id="csc-swiper-js-description"><?php esc_html_e('Loaded only when Chap theme is not activated.', 'chap-shortcodes'); ?></p>
				</td>
			</tr>

			<tr valign="top">
				<th scope="row"><?php esc_html_e('Shortcodes in widgets', 'chap-shortcodes'); ?></th>
				<td>
					<fieldset>
						<input type="checkbox" name="csc-shortcodes-in-widgets" value="1"<?php checked(get_option('csc-shortcodes-in-widgets') == 1); ?> />
						<?php esc_html_e('Allow shortcodes in widget titles and content.', 'chap-shortcodes'); ?>
					</fieldset>
				</td>
			</tr>

			<tr valign="top">
				<th scope="row"><?php esc_html_e('Shortcodes in menus', 'chap-shortcodes'); ?></th>
				<td>
					<fieldset>
						<input type="checkbox" name="csc-shortcodes-in-menus" value="1"<?php checked(get_option('csc-shortcodes-in-menus') == 1); ?> />
						<?php esc_html_e('Allow shortcodes in menu item text.', 'chap-shortcodes'); ?>
					</fieldset>
				</td>
			</tr>

			<tr valign="top">
				<th scope="row"><?php esc_html_e('Shortcodes in comments', 'chap-shortcodes'); ?></th>
				<td>
					<fieldset>
						<input type="checkbox" name="csc-shortcodes-in-comments" value="1"<?php checked(get_option('csc-shortcodes-in-comments') == 1); ?> />
						<?php esc_html_e('Allow shortcodes in comments.', 'chap-shortcodes'); ?>
					</fieldset>
				</td>
			</tr>

			<tr valign="top">
				<th scope="row"><?php esc_html_e('Shortcodes in excerpts', 'chap-shortcodes'); ?></th>
				<td>
					<fieldset>
						<input type="checkbox" name="csc-shortcodes-in-excerpts" value="1"<?php checked(get_option('csc-shortcodes-in-excerpts') == 1); ?> />
						<?php esc_html_e('Allow shortcodes in excerpts.', 'chap-shortcodes'); ?>
					</fieldset>
				</td>
			</tr>

			<tr valign="top">
				<th scope="row"><?php esc_html_e('Shortcodes in author bio', 'chap-shortcodes'); ?></th>
				<td>
					<fieldset>
						<input type="checkbox" name="csc-shortcodes-in-bio" value="1"<?php checked(get_option('csc-shortcodes-in-bio') == 1); ?> />
						<?php esc_html_e('Allow shortcodes in author biographical info.', 'chap-shortcodes'); ?>
					</fieldset>
				</td>
			</tr>

			<?php if(defined('WPCF7_VERSION')): ?>
			<tr valign="top">
				<th scope="row"><?php esc_html_e('Shortcodes in CF7', 'chap-shortcodes'); ?></th>
				<td>
					<fieldset>
						<input type="checkbox" name="csc-shortcodes-in-cf7" value="1"<?php checked(get_option('csc-shortcodes-in-cf7') == 1); ?> />
						<?php esc_html_e('Allow shortcodes in Contact Form 7 forms.', 'chap-shortcodes'); ?>
					</fieldset>
				</td>
			</tr>
			<?php endif; ?>

			<tr valign="top">
				<th scope="row"><?php esc_html_e('Lazyload images', 'chap-shortcodes'); ?></th>
				<td>
					<fieldset>
						<input type="checkbox" name="csc-lazy-loading" value="1"<?php checked(get_option('csc-lazy-loading') == 1); ?> />
						<?php
							printf(
								/* Translators: HTML code tags, new line tag. */
								esc_html__('Use lazy loading for images created with %1$s[image]%2$s shortcode by default.%3$sIf disabled, the %1$slazyload%2$s class can be added manually to still use lazy loading.', 'chap-shortcodes'),
								'<code>',
								'</code>',
								'<br />'
							);
						?>
					</fieldset>
				</td>
			</tr>

		</table>

		<?php submit_button(); ?>

	</form>

	<?php include __DIR__ . '/chap-shortcodes-admin-social.php'; ?>
	<?php include __DIR__ . '/chap-shortcodes-admin-status.php'; ?>

</div>

<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

/**
 * Render the admin UI for inserting a shortcode.
 *
 * @link       http://chap.website
 * @since      1.0.0
 *
 * @package    Chap_Shortcodes
 * @subpackage Chap_Shortcodes/admin/partials
 */

$tab = $this->get_tab();
$hidden = $tab === 'elements' ? '' : ' hidden';

/**
 * Render the UI of a shortcode in the modal for inserting shortcodes.
 */
?>

<div class="chap-postbox closed<?php echo esc_attr($hidden); ?>" data-tab="<?php echo esc_attr($tab); ?>">
	<button type="button" class="handlediv button-link" aria-expanded="false">
		<span class="screen-reader-text"><?php esc_html_e('Toggle panel:', 'chap-shortcodes') . ' ' . $this->id; ?></span>
		<span class="toggle-indicator" aria-hidden="true"></span>
	</button>
	<h2 class="hndle">
		<span><?php echo esc_html($this->id); ?></span>
	</h2>
	<div class="inside">
		<div>
			<?php $this->render_docs(); ?>
			<label class="embed-shortcode">
				<input class="embed-shortcode-field" type="text" value="<?php echo esc_attr($this->shortcode_from_args()); ?>" />
			</label>
			<?php foreach($this->presets as $title => $preset): ?>
			<label class="embed-shortcode">
				<?php echo esc_html($title); ?>
				<input class="embed-shortcode-field" type="text" value="<?php echo esc_attr($this->shortcode_from_args($preset)); ?>" />
			</label>
			<?php endforeach; ?>
		</div>
	</div>
</div>

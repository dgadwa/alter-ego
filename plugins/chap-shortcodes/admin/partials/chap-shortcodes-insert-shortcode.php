<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

/**
 * Render the modal for inserting shortcodes.
 *
 * @link       http://chap.website
 * @since      1.0.0
 *
 * @package    Chap_Shortcodes
 * @subpackage Chap_Shortcodes/admin/partials
 */

$id = 'chap-insert-shortcode';

/**
 * Render the modal for inserting shortcodes.
 */
?>

<div id="<?php echo esc_attr($id); ?>" style="display:none;">
	<div class="media-modal-content chap-shortcode-modal">
		<div class="media-frame mode-select wp-core-ui">
			<div class="media-frame-title" style="left:0;">
				<h1><?php esc_html_e('Insert shortcode', 'chap-shortcodes'); ?></h1>
			</div>
			<button type="button" class="media-modal-close chap-modal-close">
				<span class="media-modal-icon">
					<span class="screen-reader-text"><?php esc_html_e('Close modal', 'chap-shortcodes'); ?></span>
				</span>
			</button>
			<div class="media-frame-router" style="left:0;">
				<div class="media-router">
					<a id="elements" href="#" class="media-menu-item active"><?php esc_html_e('Elements', 'chap-shortcodes'); ?></a>
					<a id="containers" href="#" class="media-menu-item"><?php esc_html_e('Containers', 'chap-shortcodes'); ?></a>
					<a id="card" href="#" class="media-menu-item"><?php esc_html_e('Card', 'chap-shortcodes'); ?></a>
					<a id="form" href="#" class="media-menu-item"><?php esc_html_e('Form', 'chap-shortcodes'); ?></a>
					<a id="misc" href="#" class="media-menu-item"><?php esc_html_e('Misc', 'chap-shortcodes'); ?></a>
				</div>
			</div>
			<div class="media-frame-content" style="left:0;">
			<?php foreach($this->shortcodes as $shortcode): ?>
				<?php $shortcode->render_ui(); ?>
			<?php endforeach; ?>
			</div>
			<div class="media-frame-toolbar" style="left:0;">
				<div class="media-toolbar">
					<div class="media-toolbar-primary">
						<button id="insert-to-editor" type="button" class="button media-button button-primary button-large media-button-insert" disabled="disabled">
							<?php esc_html_e('Insert to editor', 'chap-shortcodes'); ?>
						</button>
						<button id="copy-to-clipboard" type="button" class="button media-button button-large media-button-insert" disabled="disabled">
							<?php esc_html_e('Copy to clipboard', 'chap-shortcodes'); ?>
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
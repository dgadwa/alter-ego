<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

/**
 * Chap Shortcodes Social settings page.
 *
 * @link       http://chap.website
 * @since      1.0.0
 *
 * @package    Chap_Shortcodes
 * @subpackage Chap_Shortcodes/admin/partials
 */

global $plugin_chap_shortcodes;
$social_medias = $plugin_chap_shortcodes->get_chap_shortcodes_social();
$options = get_option('csc-social');
?>


<h1><?php esc_html_e('Social', 'chap-shortcodes'); ?></h1>

<p><?php printf(esc_html__('These social links are used to display buttons in the %1$s[social]%2$s shortcode.', 'chap-shortcodes'), '<code>', '</code>'); ?></p>

<form method="post" action="options.php">

	<?php settings_fields('chap-social'); ?>

	<table class="form-table">

		<?php foreach($social_medias as $id => $social): ?>

			<?php $value = isset($options[$id]) ? $options[$id] : ''; ?>

			<tr valign="top">
				<th scope="row"><?php echo esc_html($social['name']); ?></th>
				<td>
					<input type="text" class="regular-text" name="csc-social[<?php echo esc_attr($id); ?>]" value="<?php echo esc_attr($value); ?>" placeholder="<?php echo esc_attr($social['placeholder']); ?>" />
				</td>
			</tr>

		<?php endforeach; ?>

	</table>

	<?php submit_button(); ?>

</form>

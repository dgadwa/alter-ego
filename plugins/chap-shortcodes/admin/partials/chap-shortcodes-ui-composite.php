<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

/**
 * Render the admin UI for inserting a composite.
 *
 * @link       http://chap.website
 * @since      1.0.0
 *
 * @package    Chap_Shortcodes
 * @subpackage Chap_Shortcodes/admin/partials
 */

$tab = $this->get_tab();
$hidden = $tab === 'front-page' ? '' : ' hidden';
$description = !empty($this->description) ? '<p>' . $this->description . '</p>' : '';
$image = $this->get_image();
$image = !empty($image) ? '<p class="composite image"><img src="' . $image . '" alt="' . $this->id . '" /></p>' : '';

/**
 * Render the UI of a composite in the modal for inserting shortcodes.
 */
?>

<div class="chap-postbox closed<?php echo esc_attr($hidden); ?>" data-tab="<?php echo esc_attr($tab); ?>">
	<button type="button" class="handlediv button-link" aria-expanded="false">
		<span class="screen-reader-text"><?php esc_html_e('Toggle panel:', 'chap-shortcodes') . ' ' . $this->id; ?></span>
		<span class="toggle-indicator" aria-hidden="true"></span>
	</button>
	<h2 class="hndle">
		<span><?php echo esc_html($this->name); ?></span>
	</h2>
	<div class="inside">
		<div>
			<?php echo wp_kses_post($description); ?>
			<label class="embed-shortcode">
				<textarea class="embed-shortcode-field" rows="5"><?php echo esc_textarea($this->content); ?></textarea>
			</label>
			<?php echo wp_kses_post($image); ?>
		</div>
	</div>
</div>

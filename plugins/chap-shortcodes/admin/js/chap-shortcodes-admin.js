/**
 * chap-shortcodes-admin.js
 *
 * Handles Chap shortcodes insertion into editor.
 */

(function($) {
	'use strict';

	$(document).ready(function(){

		$('.btn-csc-insert').on('click', function(){
			window.csc_editor = $(this).attr('data-editor');
		});

		/**
		 * Toggle docs.
		 */
		$('.chap-shortcode-modal .docs_toggle').on('click', function(){
			var docs = $(this).next();
			if(docs.hasClass('hidden')) {
				$(this).removeClass('closed');
				docs.removeClass('hidden');
			} else {
				$(this).addClass('closed');
				docs.addClass('hidden');
			}
			return false;
		});

		/**
		 * Handle tabs.
		 */
		$('.chap-shortcode-modal .media-router a').on('click', function(){

			$(this).parent().find('a').removeClass('active');
			$(this).addClass('active');

			var id = $(this).attr('id');

			$(this).parent().parent().parent().find('.media-frame-content').find('.chap-postbox').addClass('hidden');
			$('.chap-postbox[data-tab="' + id + '"]').removeClass('hidden');

			return false;
		});

		/**
		 * Handle postbox toggling.
		 */
		$('.chap-postbox .hndle').on('click', function(){

			var postbox = $(this).parent();
			var closed = postbox.hasClass('closed')

			if(!closed) {
				postbox.addClass('closed');
			} else {
				$('.chap-postbox').addClass('closed');
				postbox.removeClass('closed');
				postbox.children('.inside').find('.embed-shortcode-field:first').focus();
			}

		});

		/**
		 * Selected shortcode is stored here.
		 */
		var selection = '';
		/**
		 * Marker ID.
		 */
		var marker_id = 'chap-marker';
		/**
		 * Marker used to mark desired cursor position.
		 */
		var marker = '<span id="' + marker_id + '"></span>';
		/**
		 * Timeout for disabling buttons.
		 */
		var disableButtonsTimeout;

		/**
		 * Shortcode field focus event.
		 */
		$('.embed-shortcode-field').on('focus', function(){
			clearTimeout(disableButtonsTimeout);
			selection = $(this).val();
			$(this).select();
			$('.chap-shortcode-modal .media-button-insert').removeAttr('disabled');
		});

		/**
		 * Shortcode field change event.
		 */
		$('.embed-shortcode-field').on('change', function(){
			selection = $(this).val();
		});

		/**
		 * Shortcode field blur event.
		 */
		$('.embed-shortcode-field').on('blur', function(){
			clearTimeout(disableButtonsTimeout);
			disableButtonsTimeout = setTimeout(function(){
				selection = '';
				$('.chap-shortcode-modal .media-button-insert').attr('disabled', 'disabled');
			}, 500);
		});

		/**
		 * Insert to editor button.
		 */
		$('.chap-shortcode-modal button#insert-to-editor').on('click', function(){

			/**
			 * Shortcode string is marked.
			 */
			var marked = false;

			/**
			 * Add a marker inside the shortcode.
			 */
			if(selection.count('][/') === 1) {
				selection = selection.replace('][/', ']' + marker + '[/');
				marked = true;
			}

			/**
			 * Send shortcode string to editor.
			 */
			window._csc_editor = window.wpActiveEditor;
			window.wpActiveEditor = window.csc_editor;
			window.wp.media.editor.insert(selection);
			window.wpActiveEditor = window._csc_editor;

			/**
			 * Move cursor to marker and remove marker.
			 */
			if(marked) {

				/**
				 * Attempt to remove marker from Visual editor.
				 */
				if(typeof tinyMCE !== 'undefined') {
					var visual_editor = tinyMCE.get(window.csc_editor);
					if(visual_editor) {
						var editor_marker = $(visual_editor.getBody()).find('#' + marker_id);
						if(editor_marker.length) {
							visual_editor.selection.select(editor_marker.get(0));
							editor_marker.remove();
						}
					}
				}

				/**
				 * Attempt to remove marker from Text editor.
				 */
				var content = $('#' + window.csc_editor);
				var value = content.val();
				var marker_position = value.indexOf(marker);
				if(marker_position !== -1) {
					content.val(value.replace(marker, ''));
					content.selectRange(marker_position);
				}

			}

		});

		/**
		 * Copy to clipboard button.
		 */
		$('.chap-shortcode-modal button#copy-to-clipboard').on('click', function(){
			/**
			 * Copy selection to clipboard.
			 */
			copy_to_clipboard(selection);
			/**
			 * Closes the modal.
			 */
			window.parent.send_to_editor('');
		});

		/**
		 * Button to close the modals.
		 */
		$('.chap-modal-close').on('click', function(){
			window.parent.send_to_editor('');
		});

	});
})(jQuery);

/**
 * Count string in string occurrence.
 *
 * @see http://stackoverflow.com/questions/4009756/how-to-count-string-occurrence-in-string
 */
String.prototype.count = function(find) {
	return this.split(find).length - 1;
}

/**
 * Copy to clipboard.
 *
 * @see http://stackoverflow.com/questions/400212/how-do-i-copy-to-the-clipboard-in-javascript
 */
function copy_to_clipboard(text) {

	if(window.clipboardData && window.clipboardData.setData) {

		// IE specific code path to prevent textarea being shown while dialog is visible.
		return clipboardData.setData('Text', text);

	} else if(document.queryCommandSupported && document.queryCommandSupported('copy')) {

		var textarea = document.createElement('textarea');

		textarea.textContent = text;
		textarea.style.position = 'fixed';  // Prevent scrolling to bottom of page in MS Edge.
		document.body.appendChild(textarea);
		textarea.select();

		try {
			return document.execCommand('copy');  // Security exception may be thrown by some browsers.
		} catch(ex) {
			console.warn('Copy to clipboard failed.', ex);
			return false;
		} finally {
			document.body.removeChild(textarea);
		}

	}

}

/**
 * jQuery select range.
 *
 * @see http://stackoverflow.com/questions/499126/jquery-set-cursor-position-in-text-area
 */
jQuery.fn.selectRange = function(start, end) {

	if(end === undefined) {
		end = start;
	}

	return this.each(function() {

		if('selectionStart' in this) {

			this.selectionStart = start;
			this.selectionEnd = end;

		} else if(this.setSelectionRange) {

			this.setSelectionRange(start, end);

		} else if(this.createTextRange) {

			var range = this.createTextRange();

			range.collapse(true);
			range.moveEnd('character', end);
			range.moveStart('character', start);
			range.select();

		}

	});

};

<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://chap.website
 * @since      1.0.0
 *
 * @package    Chap_Shortcodes
 * @subpackage Chap_Shortcodes/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Chap_Shortcodes
 * @subpackage Chap_Shortcodes/admin
 * @author     websevendev <websevendev@gmail.com>
 */
class Chap_Shortcodes_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $chap_shortcodes    The ID of this plugin.
	 */
	private $chap_shortcodes;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $chap_shortcodes       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct($chap_shortcodes, $version) {

		$this->chap_shortcodes = $chap_shortcodes;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Chap_Shortcodes_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Chap_Shortcodes_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style($this->chap_shortcodes, plugin_dir_url(__FILE__) . 'css/chap-shortcodes-admin.css', [], $this->version, 'all');

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Chap_Shortcodes_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Chap_Shortcodes_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script($this->chap_shortcodes, plugin_dir_url(__FILE__) . 'js/chap-shortcodes-admin.js', ['jquery'], $this->version, false);

	}

	/**
	 * Register Chap Shortcodes settings menu item.
	 */
	public function chap_shortcodes_settings_menu() {

		add_submenu_page(
			apply_filters('chap_shortcodes_menu_item_parent', 'options-general.php'),
			esc_html__('Chap Shortcodes settings', 'chap-shortcodes'),
			esc_html__('Chap Shortcodes', 'chap-shortcodes'),
			'manage_options',
			'chap-shortcodes',
			[$this, 'chap_shortcodes_settings_page']
		);

	}

	/**
	 * Displays the partial file as the settings page.
	 */
	public function chap_shortcodes_settings_page() {
		include(__DIR__ . '/partials/chap-shortcodes-admin-settings.php');
	}

}

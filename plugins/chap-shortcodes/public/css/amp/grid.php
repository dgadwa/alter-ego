.ui.grid {
	display: flex;
	flex-direction: row;
	flex-wrap: wrap;
	align-items: stretch;
	padding: 0;
	width: auto;
}

.ui.grid + .grid {
	margin-top: 1rem;
}

.ui.grid > .column:not(.row),
.ui.grid > .row > .column {
	position: relative;
	display: inline-block;
	vertical-align: top;
    width: 100%;
    margin: 0;
    box-shadow: none;
    padding: 1rem 0;
}


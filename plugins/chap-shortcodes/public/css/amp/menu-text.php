.ui.text.menu {
	background: none transparent;
	border-radius: 0;
	box-shadow: none;
	border: none;
	margin: 1em -.5em;
}
.ui.text.menu .item {
	border-radius: 0;
	box-shadow: none;
	align-self: center;
	margin: 0 0;
	padding: .357em .5em;
	font-weight: normal;
	color: rgba(0, 0, 0, .6);
	transition: opacity .1s ease;
}

.ui.text.menu .item:before,
.ui.text.menu .menu .item:before {
	display: none;
}

.ui.text.menu .header.item {
	background-color: transparent;
	opacity: 1;
	color: rgba(0, 0, 0, .85);
	font-size: .928em;
	text-transform: uppercase;
	font-weight: bold;
}

.ui.text.menu .item > img:not(.ui) {
	margin: 0;
}

.ui.text.item.menu .item {
	margin: 0;
}

.ui.vertical.text.menu {
	margin: 1em 0;
}
.ui.vertical.text.menu:first-child {
	margin-top: 0rem;
}
.ui.vertical.text.menu:last-child {
	margin-bottom: 0rem;
}
.ui.vertical.text.menu .item {
	margin: .571em 0;
	padding-left: 0;
	padding-right: 0;
}
.ui.vertical.text.menu .item > i.icon {
	float: none;
	margin: 0 .357em 0 0;
}
.ui.vertical.text.menu .header.item {
	margin: .571em 0 .714em;
}

.ui.vertical.text.menu .item:not(.dropdown) > .menu {
	margin: 0;
}
.ui.vertical.text.menu .item:not(.dropdown) > .menu > .item {
	margin: 0;
	padding: .5em 0;
}

.ui.text.menu .item:hover {
	opacity: 1;
	background-color: transparent;
}

.ui.text.menu .active.item {
	background-color: transparent;
	border: none;
	box-shadow: none;
	font-weight: normal;
	color: rgba(0, 0, 0, .95);
}

.ui.text.menu .active.item:hover {
	background-color: transparent;
}

.ui.text.pointing.menu .active.item:after {
	box-shadow: none;
}
.ui.text.attached.menu {
	box-shadow: none;
}

.ui.fluid.text.menu {
	margin-left: 0;
	margin-right: 0;
}

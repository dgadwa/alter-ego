.ui.attached.menu {
	top: 0;
	bottom: 0;
	border-radius: 0;
	margin: -1px;
	width: calc(100% + 2px);
	max-width: calc(100% + 2px);
	box-shadow: none;
}
.ui.attached + .ui.attached.menu:not(.top) {
	border-top: none;
}

.ui[class*="top attached"].menu {
	bottom: 0;
	margin-bottom: 0;
	top: 0;
	margin-top: 1rem;
	border-radius: .285rem .285rem 0 0;
}
.ui.menu[class*="top attached"]:first-child {
	margin-top: 0;
}

.ui[class*="bottom attached"].menu {
	bottom: 0;
	margin-top: 0;
	top: 0;
	margin-bottom: 1rem;
	box-shadow: 0 1px 2px 0 rgba(34, 36, 38, .15), none;
	border-radius: 0 0 .285rem .285rem;
}
.ui[class*="bottom attached"].menu:last-child {
	margin-bottom: 0;
}

.ui.top.attached.menu > .item:first-child {
	border-radius: .285rem 0 0 0;
}
.ui.bottom.attached.menu > .item:first-child {
	border-radius: 0 0 0 .285rem;
}

.ui.attached.menu:not(.tabular) {
	border: 1px solid #D4D4D5;
}
.ui.attached.tabular.menu {
	margin-left: 0;
	margin-right: 0;
	width: 100%;
}

.demo.segment .ui.top.attached.label:first-child + :not(.attached) {
	margin: 3.5rem 0 1rem;
}

.demo.segment .button,
.demo.segment .ui.label {
	margin-bottom: 0.5em;
}

.demo.segment .ui.top.attached.label .detail {
	float: right;
	background: rgba(0, 0, 0, 0.1);
	margin: -0.75em -1em -0.75em 0.5em;
	padding: 0.75em 1em;
}

.ui.recipe.card > .content > .header {
	font-size: 1.35em;
	font-weight: bold;
}

.ui.recipe.card > .content > .meta,
.wprm-recipe-rating-details {
	font-size: .83em;
	line-height: 1em;
}

.ui.recipe.card > .content > .meta + .ui.list {
	margin-bottom: 0;
}
.ui.recipe.card > .content > .meta + .ui.list .item {
	line-height: 1.75em;
	font-size: .8em;
}

.ui.recipe.statistics {
	flex-direction: column;
	align-items: center;
}
.ui.recipe.statistics .statistic {
	margin: 1em 0;
}
.ui.recipe.statistic > .value,
.ui.recipe.statistics > .statistic > .value {
    text-transform: initial;
}
.ui.recipe.serving.statistics {
	margin-top: 0;
}

.ui.ingredients.segment > .ui.ingredients.list {
	margin-top: 0;
	margin-bottom: 0;
}
.ui.ingredients.list .ui.list {
	margin-top: 0;
}
.ui.ingredients.list .ui.list .item {
    line-height: 1.25em;
    padding-left: .5em;
}
.ui.ingredients.list > .item:last-child .ui.list {
	margin-bottom: 0;
}
.ui.ingredients.list > .item > .content > .header {
	margin-bottom: .5em;
}
.ui.ingredients.list span.amount,
.ui.ingredients.list span.unit {
	font-weight: bold;
}
.ui.ingredients.list span.notes {
	font-size: .9em;
	font-style: italic;
	margin-left: .2em;
}

.ui.recipe.steps .step:after {
    display: none;
}
.ui.recipe.steps .step:not(:last-child) {
	border-bottom: 1px solid rgba(34, 36, 38, .15);
}
.ui.recipe.steps .ui.divided.image {
	margin-bottom: 0;
}
.ui.recipe.steps .ui.image {
	width: 320px;
	max-width: 80vw;
	margin-left: auto;
	margin-right: auto;
}

.ui.segment {
	position: relative;
	background: #fff;
	box-shadow: 0 1px 2px 0 rgba(34, 36, 38, 0.15);
	margin: 1rem 0;
	padding: 1em 1em;
	border-radius: 0.285rem;
	border: 1px solid rgba(34, 36, 38, 0.15);
}
.ui.segment:first-child {
	margin-top: 0;
}
.ui.segment:last-child {
	margin-bottom: 0;
}

.ui.vertical.segment {
	margin: 0;
	padding-left: 0;
	padding-right: 0;
	background: none transparent;
	border-radius: 0;
	box-shadow: none;
	border: none;
}
.ui.vertical.segment:last-child {
	border-bottom: none;
}

.ui.vertical.stripe.segment:not(:last-child) {
	border-bottom: 1px solid rgba(34, 36, 38, 0.15);
	margin-bottom: 1.5em;
}
.ui.vertical.stripe.segment:after {
	content: '';
	display: block;
	margin-top: 1.5em;
}

.ui[class*="bottom attached"].segment > [class*="top attached"].label {
	border-top-left-radius: 0;
	border-top-right-radius: 0;
}
.ui[class*="top attached"].segment > [class*="bottom attached"].label {
	border-bottom-left-radius: 0;
	border-bottom-right-radius: 0;
}
.ui.attached.segment:not(.top):not(.bottom) > [class*="top attached"].label {
	border-top-left-radius: 0;
	border-top-right-radius: 0;
}
.ui.attached.segment:not(.top):not(.bottom) > [class*="bottom attached"].label {
	border-bottom-left-radius: 0;
	border-bottom-right-radius: 0;
}

.ui.basic.segment {
	background: none transparent;
	box-shadow: none;
	border: none;
	border-radius: 0;
}

.ui[class*="left aligned"].segment {
	text-align: left;
}
.ui[class*="right aligned"].segment {
	text-align: right;
}
.ui[class*="center aligned"].segment {
	text-align: center;
}

.ui.secondary.segment {
	background: #F3F4F5;
	color: rgba(0, 0, 0, 0.6);
}
.ui.secondary.inverted.segment {
	background: #4c4f52 linear-gradient(rgba(255, 255, 255, 0.2) 0%, rgba(255, 255, 255, 0.2) 100%);
	color: rgba(255, 255, 255, 0.8);
}

.ui.tertiary.segment {
	background: #DCDDDE;
	color: rgba(0, 0, 0, 0.6);
}
.ui.tertiary.inverted.segment {
	background: #717579 linear-gradient(rgba(255, 255, 255, 0.35) 0%, rgba(255, 255, 255, 0.35) 100%);
	color: rgba(255, 255, 255, 0.8);
}

.ui.attached.segment {
	top: 0;
	bottom: 0;
	border-radius: 0;
	margin: 0;
	width: calc(100% + 2px);
	max-width: calc(100% + 2px);
	box-shadow: none;
	border: 1px solid #D4D4D5;
}
.ui.attached:not(.message) + .ui.attached.segment:not(.top) {
	border-top: none;
}

.ui[class*="top attached"].segment {
	bottom: 0;
	margin-bottom: 0;
	top: 0;
	margin-top: 1rem;
	border-radius: 0.285rem 0.285rem 0 0;
}
.ui.segment[class*="top attached"]:first-child {
	margin-top: 0;
}

.ui.segment[class*="bottom attached"] {
	bottom: 0;
	margin-top: 0;
	top: 0;
	margin-bottom: 1rem;
	box-shadow: 0 1px 2px 0 rgba(34, 36, 38, 0.15);
	border-radius: 0 0 0.285rem 0.285rem;
}
.ui.segment[class*="bottom attached"]:last-child {
	margin-bottom: 0;
}

.ui.compact.segment {
	display: table;
}

.ui.segment > amp-carousel {
	background: transparent;
	max-width: 100%;
	margin: 1.2em auto 0 auto;
}

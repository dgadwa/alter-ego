.ui.statistic {
	display: inline-flex;
	flex-direction: column;
	margin: 1em 0;
	max-width: auto;
}
.ui.statistic + .ui.statistic {
	margin: 0 0 0 1.5em;
}
.ui.statistic:first-child {
	margin-top: 0;
}
.ui.statistic:last-child {
	margin-bottom: 0;
}

.ui.statistics .statistic > .text.value,
.ui.statistic > .text.value {
	line-height: 1em;
	text-align: center;
}
.ui.statistics .statistic > .text.value + .label,
.ui.statistic > .text.value + .label {
	text-align: center;
}

.ui.statistics .statistic > .value,
.ui.statistic > .value {
	font-size: 3em;
	font-weight: normal;
	line-height: 1em;
	text-transform: uppercase;
	text-align: center;
}

.ui.statistics .statistic > .label,
.ui.statistic > .label {
	font-size: 1em;
	font-weight: bold;
	color: rgba(0, 0, 0, 0.87);
	text-transform: uppercase;
	text-align: center;
}

.ui.statistics .statistic > .label ~ .value,
.ui.statistic > .label ~ .value {
	margin-top: 0;
}

.ui.statistics .statistic > .value ~ .label,
.ui.statistic > .value ~ .label {
	margin-top: 0;
}

.ui.statistics .statistic > .value .fa,
.ui.statistic > .value .fa {
	opacity: 1;
	width: auto;
	margin: 0;
}

.ui.horizontal.statistic {
	flex-direction: row;
	align-items: center;
}
.ui.horizontal.statistics {
	flex-direction: column;
	margin: 0;
	max-width: none;
}
.ui.horizontal.statistics .statistic {
	flex-direction: row;
	align-items: center;
	max-width: none;
	margin: 1em 0;
}
.ui.horizontal.statistic > .text.value,
.ui.horizontal.statistics > .statistic > .text.value {
	min-height: 0;
}
.ui.horizontal.statistics .statistic > .value .fa,
.ui.horizontal.statistic > .value .fa {
	width: 1.18em;
}
.ui.horizontal.statistics .statistic > .value,
.ui.horizontal.statistic > .value {
	display: inline-block;
	vertical-align: middle;
}
.ui.horizontal.statistics .statistic > .label,
.ui.horizontal.statistic > .label {
	display: inline-block;
	vertical-align: middle;
	margin: 0 0 0 0.75em;
}

.ui.red.statistics .statistic > .value,
.ui.statistics .red.statistic > .value,
.ui.red.statistic > .value {
	color: #DB2828;
}
.ui.orange.statistics .statistic > .value,
.ui.statistics .orange.statistic > .value,
.ui.orange.statistic > .value {
	color: #F2711C;
}
.ui.yellow.statistics .statistic > .value,
.ui.statistics .yellow.statistic > .value,
.ui.yellow.statistic > .value {
	color: #FBBD08;
}
.ui.olive.statistics .statistic > .value,
.ui.statistics .olive.statistic > .value,
.ui.olive.statistic > .value {
	color: #B5CC18;
}
.ui.green.statistics .statistic > .value,
.ui.statistics .green.statistic > .value,
.ui.green.statistic > .value {
	color: #21BA45;
}
.ui.teal.statistics .statistic > .value,
.ui.statistics .teal.statistic > .value,
.ui.teal.statistic > .value {
	color: #00B5AD;
}
.ui.blue.statistics .statistic > .value,
.ui.statistics .blue.statistic > .value,
.ui.blue.statistic > .value {
	color: #2185D0;
}
.ui.violet.statistics .statistic > .value,
.ui.statistics .violet.statistic > .value,
.ui.violet.statistic > .value {
	color: #6435C9;
}
.ui.purple.statistics .statistic > .value,
.ui.statistics .purple.statistic > .value,
.ui.purple.statistic > .value {
	color: #A333C8;
}
.ui.pink.statistics .statistic > .value,
.ui.statistics .pink.statistic > .value,
.ui.pink.statistic > .value {
	color: #E03997;
}
.ui.brown.statistics .statistic > .value,
.ui.statistics .brown.statistic > .value,
.ui.brown.statistic > .value {
	color: #A5673F;
}
.ui.grey.statistics .statistic > .value,
.ui.statistics .grey.statistic > .value,
.ui.grey.statistic > .value {
	color: #767676;
}

.ui[class*="left floated"].statistic {
	float: left;
	margin: 0 2em 1em 0;
}
.ui[class*="right floated"].statistic {
	float: right;
	margin: 0 0 1em 2em;
}
.ui.floated.statistic:last-child {
	margin-bottom: 0;
}

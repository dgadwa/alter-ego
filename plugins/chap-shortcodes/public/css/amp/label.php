.ui.label {
	font-size: .857rem;
	display: inline-block;
	line-height: 1;
	vertical-align: baseline;
	margin: 0 .142em;
	background-color: #E8E8E8;
	padding: 0.583em .833em;
	color: rgba(0, 0, 0, .6);
	font-weight: bold;
	border-radius: .285rem;
}
.ui.label:first-child {
	margin-left: 0;
}
.ui.label:last-child {
	margin-right: 0;
}

.ui.ribbon.label {
	display: none;
}

.ui.attached.segment > .ui.top.left.attached.label,
.ui.bottom.attached.segment > .ui.top.left.attached.label {
	border-top-left-radius: 0;
}
.ui.attached.segment > .ui.top.right.attached.label,
.ui.bottom.attached.segment > .ui.top.right.attached.label {
	border-top-right-radius: 0;
}
.ui.top.attached.segment > .ui.bottom.left.attached.label {
	border-bottom-left-radius: 0;
}
.ui.top.attached.segment > .ui.bottom.right.attached.label {
	border-bottom-right-radius: 0;
}

.ui.top.attached.label:first-child + :not(.attached),
.ui.top.attached.label + [class*="right floated"] + * {
	margin-top: 2rem;
}
.ui.bottom.attached.label:first-child ~ :last-child:not(.attached) {
	margin-top: 0;
	margin-bottom: 2rem;
}

.ui[class*="top attached"].label,
.ui.attached.label {
	width: 100%;
	position: absolute;
	margin: 0;
	top: 0;
	left: 0;
	padding: .75em 1em;
	border-radius: .214rem .214rem 0 0;
}
.ui[class*="bottom attached"].label {
	top: auto;
	bottom: 0;
	border-radius: 0 0 .214rem .214rem;
}
.ui[class*="top left attached"].label {
	width: auto;
	margin-top: 0;
	border-radius: .214rem 0 .285rem 0;
}
.ui[class*="top right attached"].label {
	width: auto;
	left: auto;
	right: 0em;
	border-radius: 0 .214rem 0 .285rem;
}
.ui[class*="bottom left attached"].label {
	width: auto;
	top: auto;
	bottom: 0em;
	border-radius: 0 .285rem 0 .214rem;
}
.ui[class*="bottom right attached"].label {
	top: auto;
	bottom: 0em;
	left: auto;
	right: 0em;
	width: auto;
	border-radius: .285rem 0 .214rem 0;
}
.ui.transparent.label {
	background-color: transparent;
}

.ui.corner.label {
	position: absolute;
	top: 0;
	right: 0;
	margin: 0;
	padding: 0;
	text-align: center;
	border-color: #E8E8E8;
	width: 4em;
	height: 4em;
	z-index: 1;
	transition: border-color 0.1s ease;
	background-color: transparent;
}
.ui.corner.label:after {
	position: absolute;
	content: "";
	right: 0;
	top: 0;
	z-index: -1;
	width: 0;
	height: 0;
	background-color: transparent;
	border-top: 0;
	border-right: 4em solid transparent;
	border-bottom: 4em solid transparent;
	border-left: 0;
	border-right-color: inherit;
	transition: border-color 0.1s ease;
}
.ui.corner.label .fa {
	cursor: default;
	position: relative;
	top: .642em;
	left: .785em;
	margin: 0;
}

.ui.segment > .ui.corner.label {
	top: -1px;
	right: -1px;
}
.ui.segment > .ui.left.corner.label {
	right: auto;
	left: -1px;
}

.ui.image.label {
  width: auto;
  margin-top: 0em;
  margin-bottom: 0em;
  max-width: 9999px;
  vertical-align: baseline;
  text-transform: none;
  background: #E8E8E8;
  padding: 0.583em 0.833em 0.583em 0.5em;
  border-radius: 0.285rem;
  box-shadow: none;
}
.ui.image.label amp-img {
  display: inline-block;
  vertical-align: top;
  height: 2.1666em;
  margin: -0.583em 0.5em -0.583em -0.5em;
  border-radius: 0.285rem 0em 0em 0.285rem;
}
.ui.image.label .detail {
  background: rgba(0, 0, 0, 0.1);
  margin: -0.583em -0.833em -0.583em 0.5em;
  padding: 0.583em 0.833em;
  border-radius: 0em 0.285rem 0.285rem 0em;
}

.ui.label > amp-img {
  width: auto;
  vertical-align: middle;
  height: 2.1666em;
}

.ui.label > .fa {
  width: auto;
  margin: 0 0.5em 0 0;
}

.ui.label > .fa-close,
.ui.label > .fa-delete {
  cursor: pointer;
  margin: 0 0 0 .5em;
}

.ui.label > .detail {
  display: inline-block;
  vertical-align: top;
  font-weight: bold;
  margin-left: 1em;
  opacity: 0.8;
}
.ui.label > .detail .fa {
  margin: 0em 0.25em 0em 0em;
}

.ui.circular.labels .label,
.ui.circular.label {
  min-width: 2em;
  min-height: 2em;
  padding: 0.5em;
  line-height: 1em;
  text-align: center;
  border-radius: 500rem;
}
.ui.empty.circular.labels .label,
.ui.empty.circular.label {
  min-width: 0em;
  min-height: 0em;
  overflow: hidden;
  width: 0.5em;
  height: 0.5em;
  vertical-align: baseline;
}

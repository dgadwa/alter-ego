.ui.divider {
	margin: 1rem 0;
	height: 0;
	font-size: 0;
	border-top: 1px solid rgba(34, 36, 38, 0.15);
	border-bottom: 1px solid rgba(255, 255, 255, 0.1);
}

.ui.hidden.divider {
	border-color: transparent;
}

.ui.clearing.divider {
	clear: both;
}

.ui.section.divider {
	margin-top: 2rem;
	margin-bottom: 2rem;
}

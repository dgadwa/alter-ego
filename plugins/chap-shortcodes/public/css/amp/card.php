.ui.card {
	margin: 1em auto;
}
.ui.cards > .card,
.ui.card {
	max-width: 100%;
	position: relative;
	display: flex;
	flex-direction: column;
	width: 290px;
	min-height: 0;
	background: #fff;
	padding: 0em;
	border: none;
	border-radius: 0.285rem;
	box-shadow: 0 1px 3px 0 #D4D4D5, 0 0 0 1px #D4D4D5;
	line-height: initial;
}
.ui.cards > .card a,
.ui.card a {
	cursor: pointer;
}

.ui.card > .image:first-child,
.ui.card > .image:first-child > amp-img,
.ui.card > .image:first-child > amp-img > img {
	border-top-left-radius: inherit;
	border-top-right-radius: inherit;
	min-width: 100%;
}

.ui.card > .content {
	flex-grow: 1;
	border: none;
	background: none;
	margin: 0;
	padding: 1em 1em;
	box-shadow: none;
	font-size: 1em;
	border-radius: 0;
}

.ui.card > .content:not(:first-child) {
	border-top: 1px solid rgba(34, 36, 38, 0.1);
}

.ui.card > .content:after {
	display: block;
	content: ' ';
	height: 0;
	clear: both;
	overflow: hidden;
	visibility: hidden;
}

.ui.card > .content > .header {
	display: block;
	color: rgba(0, 0, 0, .85);
}

.ui.cards > .card [class*="left floated"],
.ui.card [class*="left floated"] {
	float: left;
}
.ui.cards > .card [class*="right floated"],
.ui.card [class*="right floated"] {
	float: right;
}

.ui.cards > .card [class*="left aligned"],
.ui.card [class*="left aligned"] {
	text-align: left;
}
.ui.cards > .card [class*="center aligned"],
.ui.card [class*="center aligned"] {
	text-align: center;
}
.ui.cards > .card [class*="right aligned"],
.ui.card [class*="right aligned"] {
	text-align: right;
}

.ui.cards > .card .content amp-img,
.ui.card .content amp-img {
	display: inline-block;
	vertical-align: middle;
	width: '';
}
.ui.cards > .card amp-img.avatar,
.ui.cards > .card .avatar amp-img,
.ui.card amp-img.avatar,
.ui.card .avatar amp-img {
	width: 2em;
	height: 2em;
	border-radius: 500rem;
}

.ui.cards > .card > .button,
.ui.card > .button {
	margin: -1px;
	width: calc(100% + 2px);
}

.ui.card .image + .ui.button {
	margin-top: 0;
}

.ui.card > .ui.button:last-child,
.ui.card .ui.button[class*="bottom attached"] {
	border-top-left-radius: 0;
	border-top-right-radius: 0;
}

.ui.cards > .card .meta,
.ui.card .meta,
.ui.card .meta > a {
	font-size: 1em;
	color: rgba(0, 0, 0, 0.4);
}
.ui.card .meta > a:hover,
.ui.card .meta > a:active,
.ui.card .meta > a:focus {
	color: rgba(0, 0, 0, 0.9);
}
.ui.cards > .card .meta *,
.ui.card .meta * {
	margin-right: 0.3em;
}
.ui.cards > .card .meta :last-child,
.ui.card .meta :last-child {
	margin-right: 0;
}
.ui.cards > .card .meta [class*="right floated"],
.ui.card .meta [class*="right floated"] {
	margin-right: 0;
	margin-left: 0.3em;
}

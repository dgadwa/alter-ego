.ui.header {
	border: none;
	margin: calc(2rem - .142em) 0 .5rem;
	padding: 0;
	line-height: 1.285em;
	text-transform: none;
	color: rgba(0, 0, 0, .87);
}
.ui.header:first-child {
	margin-top: -0.142em;
}
.ui.header:last-child {
	margin-bottom: 0;
}

.ui.header + p {
	margin-top: 0;
}

.ui[class*="left aligned"] {
	text-align: left;
}
.ui[class*="center aligned"] {
	text-align: center;
}
.ui[class*="right aligned"] {
	text-align: right;
}

.ui.header .sub.header {
	display: block;
	font-weight: normal;
	padding: 0;
	margin: 0;
	font-size: 1rem;
	line-height: 1.2em;
	color: rgba(0, 0, 0, .6);
}

.ui.sub.header {
	padding: 0em;
	margin-bottom: .142rem;
	font-weight: bold;
	font-size: .857em;
	text-transform: uppercase;
	color: '';
}

.ui.dividing.header {
	padding-bottom: .214rem;
	border-bottom: 1px solid rgba(34, 36, 38, .15);
}
.ui.dividing.header .sub.header {
	padding-bottom: .214rem;
}
.ui.dividing.header .fa {
	margin-bottom: 0;
}
.ui.inverted.dividing.header {
	border-bottom-color: rgba(255, 255, 255, .1);
}

.ui.block.header {
	background: #F3F4F5;
	padding: .785rem 1rem;
	box-shadow: none;
	border: 1px solid #D4D4D5;
	border-radius: .285rem;
}

.ui.attached.header {
	background: #FFFFFF;
	padding: .785rem 1rem;
	margin-left: -1px;
	margin-right: -1px;
	box-shadow: none;
	border: 1px solid #D4D4D5;
}
.ui.attached.block.header {
	background: #F3F4F5;
}
.ui.attached:not(.top):not(.bottom).header {
	margin-top: 0;
	margin-bottom: 0;
	border-top: none;
	border-radius: 0;
}
.ui.top.attached.header {
	margin-bottom: 0;
	border-radius: .285rem .285rem 0 0;
}
.ui.bottom.attached.header {
	margin-top: 0;
	border-top: none;
	border-radius: 0 0 .285rem .285rem;
}

.ui.header > amp-img {
	display: none;
}

.ui.attached.header + .ui.attached.segment {
	margin: 0 -1px;
}

.ui.header > .fa {
	display: table-cell;
	opacity: 1;
	font-size: 1.5em;
	vertical-align: middle;
}

.ui.header .fa:only-child {
	display: inline-block;
	padding: 0;
	margin-top: -4px;
	margin-left: 0.75rem;
	margin-right: 0.75rem;
}

.ui.header .content {
	display: inline-block;
	vertical-align: top;
}

.ui.header:not(.icon) > .fa + .content {
	padding-left: 0.75rem;
	display: table-cell;
	vertical-align: middle;
}

.ui.icon.header {
	display: block;
}
.ui.icon.header:not([class*="aligned"]) {
	text-align: center;
	display: inline-block;
}
.ui.icon.header[class*="left aligned"] {
	text-align: left;
}
[class*="center aligned"] .ui.icon.header,
.ui.icon.header[class*="center aligned"] {
	text-align: center;
}
.ui.icon.header[class*="right aligned"] {
	text-align: right;
}
.ui.icon.header > .fa {
	float: none;
	display: block;
	font-size: 3em;
	margin: 0 auto 0.5rem;
}
.ui.icon.header > .content {
	padding-left: 0;
}

.ui.huge.display.header {
  min-height: 1em;
  font-size: 3.5rem;
}
.ui.large.display.header {
  font-size: 2.84rem;
}
.ui.display.header,
.ui.medium.display.header {
  font-size: 2.2rem;
}
.ui.small.display.header {
  font-size: 1.8rem;
}
.ui.tiny.display.header {
  font-size: 1.5rem;
}
.ui.display.header > .icon {
  padding-top: 0;
}

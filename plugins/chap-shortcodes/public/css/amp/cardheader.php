.ui.cards > .card > .content > .header:not(.ui),
.ui.card > .content > .header:not(.ui) {
	font-weight: bold;
	font-size: 1.285em;
	margin-top: -0.214em;
	line-height: 1.285em;
}
.ui.cards > .card > .content > .meta + .description,
.ui.cards > .card > .content > .header + .description,
.ui.card > .content > .meta + .description,
.ui.card > .content > .header + .description {
	margin-top: 0.5em;
}

.ui.cards > .card > .content > a.header,
.ui.card > .content > a.header {
	color: rgba(0, 0, 0, 0.85);
}
.ui.cards > .card > .content > a.header:hover,
.ui.card > .content > a.header:hover {
	color: #1e70bf;
}

.ui.list:not(.bulleted):not(.ordered) {
	list-style-type: none;
	margin: 1em 0;
	padding: 0 0;
}
ul.ui.list:first-child,
ol.ui.list:first-child,
.ui.list:first-child {
	margin-top: 0;
	padding-top: 0;
}
ul.ui.list:last-child,
ol.ui.list:last-child,
.ui.list:last-child {
	margin-bottom: 0;
	padding-bottom: 0;
}
.ui[class*="right floated"].list {
    float: right;
}
.ui.list .list > .disabled.item, .ui.list > .disabled.item {
    pointer-events: none;
    color: rgba(40, 40, 40, 0.3);
}
ul.ui.list > li:first-child:after,
ol.ui.list > li:first-child:after,
.ui.list > .list > .item,
.ui.list > .item:after {
	content: '';
	display: block;
	height: 0;
	clear: both;
	visibility: hidden;
}

<?php /* Content */ ?>
.ui.list .list > .item > .image + .content,
.ui.list .list > .item > .fa + .content,
.ui.list > .item > .image + .content,
.ui.list > .item > .fa + .content {
	display: table-cell;
	padding: 0 0 0 0.5em;
	vertical-align: top;
}
.ui.list .list > .item > amp-img.image + .content,
.ui.list > .item > amp-img.image + .content {
	display: inline-block;
}
.ui.list .list > .item > .content > .list,
.ui.list > .item > .content > .list {
	margin-left: 0;
	padding-left: 0;
}

<?php /* Icon lists */ ?>
.ui.list .list > .item > .fa,
.ui.list > .item > .fa {
	display: table-cell;
	margin: 0;
	padding-top: 0.071em;
	padding-right: 0.285em;
	vertical-align: middle;
	min-width: 1.4em;
	text-align: center;
}
.ui.list .list > .item > .fa:only-child,
.ui.list > .item > .fa:only-child {
	display: inline-block;
	vertical-align: top;
}

<?php /* Header */ ?>
.ui.list .list > .item .header,
.ui.list > .item .header {
	display: block;
	font-weight: 700;
	color: rgba(0, 0, 0, 0.87);
	margin: 0.5em 0 0 0;
	line-height: 1em;
}

<?php /* Description */ ?>
ui.list .list > .item .description,
.ui.list > .item .description {
	display: block;
	color: rgba(0, 0, 0, 0.7);
	margin-bottom: 0.5em;
	line-height: 1em;
}

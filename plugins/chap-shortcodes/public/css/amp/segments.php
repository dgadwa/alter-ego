.ui.segments {
	flex-direction: column;
	position: relative;
	margin: 1rem 0;
	border: 1px solid rgba(34, 36, 38, 0.15);
	box-shadow: 0 1px 2px 0 rgba(34, 36, 38, 0.15);
	border-radius: 0.285rem;
}
.ui.segments:first-child {
	margin-top: 0;
}
.ui.segments:last-child {
	margin-bottom: 0;
}
.ui.segments > .segment {
	top: 0;
	bottom: 0;
	border-radius: 0;
	margin: 0;
	width: auto;
	box-shadow: none;
	border: none;
	border-top: 1px solid rgba(34, 36, 38, 0.15);
}
.ui.segments:not(.horizontal) > .segment:first-child {
	border-top: none;
	margin-top: 0;
	bottom: 0;
	margin-bottom: 0;
	top: 0;
	border-radius: 0.285rem 0.285rem 0 0;
}
.ui.segments:not(.horizontal) > .segment:last-child {
	top: 0;
	bottom: 0;
	margin-top: 0;
	margin-bottom: 0;
	box-shadow: 0 1px 2px 0 rgba(34, 36, 38, 0.15), none;
	border-radius: 0 0 0.285rem 0.285rem;
}

.ui.grid > .row {
	position: relative;
	display: flex;
	flex-direction: row;
	flex-wrap: wrap;
	justify-content: inherit;
	align-items: stretch;
	width: 100%;
	padding: 0;
	padding-top: 1rem;
	padding-bottom: 1rem;
}

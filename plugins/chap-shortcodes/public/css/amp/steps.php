.ui.steps {
	display: inline-flex;
	overflow: visible;
	flex-direction: column;
	align-items: stretch;
	margin: 1em 0;
	line-height: 1.142em;
	border-radius: 0.285rem;
	border: 1px solid rgba(34, 36, 38, 0.15);
}
.ui.steps:first-child {
	margin-top: 0;
}
.ui.steps:last-child {
	margin-bottom: 0;
}

.ui.steps .step {
	position: relative;
	display: flex;
	flex: 1 0 auto;
	flex-wrap: wrap;
	flex-direction: column;
	width: 100%;
	vertical-align: middle;
	align-items: center;
	justify-content: center;
	margin: 0 0;
	padding: 1.142em 2em;
	background: #fff;
	color: rgba(0, 0, 0, 0.87);
	box-shadow: none;
	border-radius: 0;
	border: none;
}

.ui.steps .step:after {
	display: none;
	position: absolute;
	z-index: 2;
	content: '';
	top: 50%;
	right: 0;
	background-color: #fff;
	width: 1.143em;
	height: 1.143em;
	border-style: solid;
	border-color: rgba(34, 36, 38, 0.15);
	border-width: 0 1px 1px 0;
	transform: translateY(-50%) translateX(50%) rotate(-45deg);
}

.ui.steps .step:first-child {
	padding: 1.143em 2em;
	border-radius: 0.285rem 0.285rem 0 0;
}
.ui.steps .step:last-child {
	border-radius: 0 0 0.285rem 0.285rem;
	border-right: none;
	margin-right: 0;
}

.ui.steps .step:only-child {
  border-radius: 0.285rem;
}

.ui.steps .step .content {
	text-align: center;
}

.ui.steps .step .title {
	font-size: 1.143em;
	font-weight: bold;
}
.ui.steps .step > .title {
	width: 100%;
}

.ui.steps .step .description {
	font-weight: normal;
	font-size: 0.928em;
	color: rgba(0, 0, 0, 0.87);
}
.ui.steps .step > .description {
	width: 100%;
}
.ui.steps .step .title ~ .description {
	margin-top: 0.25em;
}

.ui.steps .step > .fa {
	line-height: 1;
	font-size: 2.5em;
	margin: 0 1rem 0 0;
}
.ui.steps .step > .fa,
.ui.steps .step > .fa ~ .content {
	display: block;
	flex: 0 1 auto;
	align-self: middle;
}
.ui.steps .step > .fa ~ .content {
	flex-grow: 1 0 auto;
}

.ui.steps:not(.vertical) .step > .fa {
	width: auto;
}

.ui.steps .link.step,
.ui.steps a.step {
	cursor: pointer;
}

.ui.ordered.steps {
	counter-reset: ordered;
}
.ui.ordered.steps .step:before {
	display: block;
	position: static;
	text-align: center;
	content: counters(ordered, ".");
	align-self: middle;
	margin-right: 1rem;
	font-size: 2.5em;
	counter-increment: ordered;
	font-family: inherit;
	font-weight: bold;
}
.ui.ordered.steps .step > * {
	display: block;
	align-self: middle;
}

.ui.steps .step > .fa,
.ui.ordered.steps .step:before {
	margin: 0 0 1rem 0;
}

.ui.steps .link.step:hover::after,
.ui.steps .link.step:hover,
.ui.steps a.step:hover::after,
.ui.steps a.step:hover {
  background: #F9FAFB;
  color: rgba(0, 0, 0, 0.8);
}

.ui.steps .link.step:active::after,
.ui.steps .link.step:active,
.ui.steps a.step:active::after,
.ui.steps a.step:active {
	background: #F3F4F5;
	color: rgba(0, 0, 0, 0.9);
}

.ui.steps .step.active {
	cursor: auto;
	background: #F3F4F5;
}
.ui.steps .step.active:after {
	background: #F3F4F5;
}
.ui.steps .step.active .title {
	color: #4183C4;
}
.ui.ordered.steps .step.active:before,
.ui.steps .active.step .fa {
	color: rgba(0, 0, 0, 0.85);
}

.ui.steps .step:after {
	display: block;
}
.ui.steps .active.step:after {
	display: block;
}
.ui.steps .step:last-child:after {
	display: none;
}
.ui.steps .active.step:last-child:after {
	display: none;
}

.ui.steps .link.active.step:hover::after,
.ui.steps .link.active.step:hover,
.ui.steps a.active.step:hover::after,
.ui.steps a.active.step:hover {
	cursor: pointer;
	background: #DCDDDE;
	color: rgba(0, 0, 0, 0.87);
}

.ui.steps .step.completed > .fa:before,
.ui.ordered.steps .step.completed:before {
	color: #21BA45;
}

.ui.steps .disabled.step {
	cursor: auto;
	background: #fff;
	pointer-events: none;
}
.ui.steps .disabled.step,
.ui.steps .disabled.step .title,
.ui.steps .disabled.step .description {
	color: rgba(40, 40, 40, 0.3);
}
.ui.steps .disabled.step:after {
	background: #fff;
}

.ui.fluid.steps {
	display: flex;
	width: 100%;
}

.ui.attached.steps {
	width: calc(100% + 2px);
	margin: 0 -1px 0;
	max-width: calc(100% + 2px);
	border-radius: 0.285rem 0.285rem 0 0;
}
.ui.attached.steps .step:first-child {
	border-radius: 0.285rem 0 0 0;
}
.ui.attached.steps .step:last-child {
  border-radius: 0 0.285rem 0 0;
}
.ui.bottom.attached.steps {
	margin: 0 -1px 0;
	border-radius: 0 0 0.285rem 0.285rem;
}
.ui.bottom.attached.steps .step:first-child {
	border-radius: 0 0 0 0.285rem;
}
.ui.bottom.attached.steps .step:last-child {
	border-radius: 0 0 0.285rem 0;
}

.ui.compact.menu {
	display: inline-flex;
	margin: 0;
	vertical-align: middle;
}
.ui.compact.vertical.menu {
	display: inline-block;
}
.ui.compact.menu .item:last-child {
	border-radius: 0 .285rem .285rem 0;
}
.ui.compact.menu .item:last-child:before {
	display: none;
}
.ui.compact.vertical.menu {
	width: auto;
}
.ui.compact.vertical.menu .item:last-child::before {
	display: block;
}

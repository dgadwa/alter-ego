.ui.cards > .card > .extra, .ui.card > .extra {
	max-width: 100%;
	min-height: 0;
	flex-grow: 0;
	border-top: 1px solid rgba(0,0,0,.05);
	position: static;
	width: auto;
	margin: 0 0;
	padding: .75em 1em;
	top: 0;
	left: 0;
	color: rgba(0,0,0,.4);
	box-shadow: none;
}

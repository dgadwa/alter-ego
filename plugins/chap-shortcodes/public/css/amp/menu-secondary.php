.ui.secondary.menu {
	background: none;
	margin-left: -.357em;
	margin-right: -.357em;
	border-radius: 0;
	border: none;
	box-shadow: none;
}

.ui.secondary.menu .item {
	align-self: center;
	box-shadow: none;
	border: none;
	padding: .785em .928em;
	margin: 0 .357em;
	background: none;
	transition: color .1s ease;
	border-radius: .285rem;
}

.ui.secondary.menu .item:before {
	display: none;
}

.ui.secondary.menu .header.item {
	border-radius: 0;
	border-right: none;
	background: none transparent;
}

.ui.secondary.menu .item > img:not(.ui) {
	margin: 0;
}

.ui.secondary.menu .dropdown.item:hover,
.ui.secondary.menu .link.item:hover,
.ui.secondary.menu a.item:hover {
	background: rgba(0, 0, 0, .05);
	color: rgba(0, 0, 0, .95);
}

.ui.secondary.menu .active.item {
	box-shadow: none;
	background: rgba(0, 0, 0, .05);
	color: rgba(0, 0, 0, .95);
	border-radius: .285rem;
}

.ui.secondary.menu .active.item:hover {
	box-shadow: none;
	background: rgba(0, 0, 0, .05);
	color: rgba(0, 0, 0, .95);
}

.ui.secondary.item.menu {
	margin-left: 0;
	margin-right: 0;
}
.ui.secondary.item.menu .item:last-child {
	margin-right: 0;
}
.ui.secondary.attached.menu {
	box-shadow: none;
}

.ui.vertical.secondary.menu .item:not(.dropdown) > .menu {
	margin: 0 -.928em;
}
.ui.vertical.secondary.menu .item:not(.dropdown) > .menu > .item {
	margin: 0;
	padding: .5em 1.333em;
}

.ui.secondary.vertical.menu > .item {
	border: none;
	margin: 0 0 .357em;
	border-radius: .285rem;
}
.ui.secondary.vertical.menu > .header.item {
	border-radius: 0;
}

.ui.vertical.secondary.menu .item > .menu .item {
	background-color: transparent;
}

.ui.secondary.pointing.menu {
	margin-left: 0;
	margin-right: 0;
	border-bottom: 2px solid rgba(34, 36, 38, .15);
}
.ui.secondary.pointing.menu .item {
	border-bottom-color: transparent;
	border-bottom-style: solid;
	border-radius: 0;
	align-self: flex-end;
	margin: 0 0 -2px;
	padding: .857em 1.142em;
	border-bottom-width: 2px;
	transition: color .1s ease;
}

.ui.secondary.pointing.menu .header.item {
	color: rgba(0, 0, 0, .85);
}
.ui.secondary.pointing.menu .text.item {
	box-shadow: none;
}
.ui.secondary.pointing.menu .item:after {
	display: none;
}

.ui.secondary.pointing.menu .dropdown.item:hover,
.ui.secondary.pointing.menu .link.item:hover,
.ui.secondary.pointing.menu a.item:hover {
	background-color: transparent;
	color: rgba(0, 0, 0, .87);
}

.ui.secondary.pointing.menu .dropdown.item:active,
.ui.secondary.pointing.menu .link.item:active,
.ui.secondary.pointing.menu a.item:active {
	background-color: transparent;
	border-color: rgba(34, 36, 38, .15);
}

.ui.secondary.pointing.menu .active.item {
	background-color: transparent;
	box-shadow: none;
	border-color: #1B1C1D;
	font-weight: bold;
	color: rgba(0, 0, 0, .95);
}

.ui.secondary.pointing.menu .active.item:hover {
	border-color: #1B1C1D;
	color: rgba(0, 0, 0, .95);
}

.ui.secondary.pointing.menu .active.dropdown.item {
	border-color: transparent;
}

.ui.secondary.vertical.pointing.menu {
	border-bottom-width: 0;
	border-right-width: 2px;
	border-right-style: solid;
	border-right-color: rgba(34, 36, 38, .15);
}
.ui.secondary.vertical.pointing.menu .item {
	border-bottom: none;
	border-right-style: solid;
	border-right-color: transparent;
	border-radius: 0;
	margin: 0 -2px 0 0;
	border-right-width: 2px;
}

.ui.secondary.vertical.pointing.menu .active.item {
	border-color: #1B1C1D;
}

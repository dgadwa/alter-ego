.ui.message {
	position: relative;
	min-height: 1em;
	margin: 1em 0;
	background: #F8F8F9;
	padding: 1em 1.5em;
	line-height: 1.428em;
	color: rgba(0, 0, 0, .87);
	transition: opacity 0.1s ease, color 0.1s ease, background 0.1s ease, box-shadow 0.1s ease;
	border-radius: .285rem;
	box-shadow: 0 0 0 1px rgba(34, 36, 38, .22) inset;
}
.ui.message:first-child {
	margin-top: 0;
}
.ui.message:last-child {
	margin-bottom: 0;
}
.ui.message .header {
	display: block;
	font-weight: bold;
	margin: -.149em 0 0 0;
}

.ui.attached.message {
	margin-bottom: -1px;
	border-radius: .285rem .285rem 0 0;
	box-shadow: 0 0 0 1px rgba(34, 36, 38, .15) inset;
	margin-left: 0;
	margin-right: -2px;
}
.ui.attached + .ui.attached.message:not(.top):not(.bottom) {
	margin-top: -1px;
	border-radius: 0;
}
.ui.bottom.attached.message {
	margin-top: -1px;
	border-radius: 0 0 .285rem .285rem;
	box-shadow: 0 0 0 1px rgba(34, 36, 38, .15) inset, 0 1px 2px 0 rgba(34, 36, 38, .15);
}
.ui.bottom.attached.message:not(:last-child) {
	margin-bottom: 1em;
}
.ui.attached.icon.message {
	width: auto;
}

.ui.positive.message,
.ui.success.message {
	background-color: #FCFFF5;
	color: #2C662D;
}
.ui.negative.message,
.ui.error.message {
	background-color: #FFF6F6;
	color: #9F3A38;
}
.ui.info.message {
	background-color: #F8FFFF;
	color: #276F86;
}
.ui.warning.message {
	background-color: #FFFAF3;
	color: #573A08;
}
.ui.message .fa {
	margin-right: 0.5em;
}
.ui.message:not(.icon) .fa {
	min-width: 1em;
	float: left;
	line-height: 1.5em;
	margin-right: 0.5em;
	text-align: center;
}
.ui.icon.message {
	display: flex;
	width: 100%;
	align-items: center;
}
.ui.icon.message > .fa:not(.close) {
	display: block;
	flex: 0 0 auto;
	width: auto;
	line-height: 1;
	vertical-align: middle;
	font-size: 3em;
	opacity: 0.8;
}
.ui.compact.message {
	display: inline-block;
}
.ui.floating.message {
	box-shadow: 0px 0px 0px 1px rgba(34,36,38,0.22) inset, 0px 2px 4px 0px rgba(34,36,38,0.12), 0px 2px 10px 0px rgba(34,36,38,0.15);
}

<?php /* Horizontal list */ ?>
.ui.horizontal.list {
	display: inline-block;
	font-size: 0;
}
.ui.horizontal.list > .item {
	display: inline-block;
	margin-left: 1em;
	font-size: 1rem;
}
.ui.horizontal.list:not(.celled) > .item:first-child {
	margin-left: 0;
	padding-left: 0;
}
.ui.horizontal.list .list {
	padding-left: 0;
	padding-bottom: 0;
}

.ui.horizontal.list > .item > .fa,
.ui.horizontal.list .list > .item > .fa,
.ui.horizontal.list > .item > .content,
.ui.horizontal.list .list > .item > .content {
	vertical-align: middle;
}
.ui.horizontal.list > .item:first-child,
.ui.horizontal.list > .item:last-child {
	padding-top: 0.214em;
	padding-bottom: 0.214em;
}

.ui.horizontal.list > .item > .fa {
	margin: 0;
	padding: 0 0.25em 0 0;
}
.ui.horizontal.list > .item > .fa,
.ui.horizontal.list > .item > .fa + .content {
	float: none;
	display: inline-block;
}

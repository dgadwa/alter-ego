.ui.grid > .row > .column {
	margin-top: 0;
	margin-bottom: 0;
}

.ui.grid .column[class*="left aligned"] {
	text-align: left;
}
.ui.grid .column[class*="center aligned"] {
	text-align: center;
}
.ui.grid .column[class*="right aligned"] {
	text-align: right;
}
.ui.grid .justified.column {
	text-align: justify;
}

.ui.menu {
	display: flex;
	margin: 1rem 0;
	background: #fff;
	font-weight: normal;
	border: 1px solid rgba(34, 36, 38, .15);
	box-shadow: 0 1px 2px 0 rgba(34, 36, 38, .15);
	border-radius: 0.285rem;
	min-height: 2.857em;
}
.ui.menu:after {
	content: '';
	display: block;
	height: 0;
	clear: both;
	visibility: hidden;
}
.ui.menu:first-child {
	margin-top: 0;
}
.ui.menu:last-child {
	margin-bottom: 0;
}
.ui.menu .menu {
	margin: 0;
}

.ui.stackable.menu {
	flex-direction: column;
}
.ui.stackable.menu .item:before {
    position: absolute;
    content: '';
    top: auto;
    bottom: 0;
    left: 0;
    width: 100%;
    height: 1px;
    background: rgba(34, 36, 38, .1);
}

.ui.menu:not(.vertical) .item {
	display: flex;
	align-items: center;
}
.ui.menu .item {
	position: relative;
	vertical-align: middle;
	line-height: 1;
	text-decoration: none;
	flex: 0 0 auto;
	background: none;
	padding: .928em 1.142em;
	text-transform: none;
	color: rgba(0, 0, 0, .87);
	font-weight: normal;
}
.ui.menu > .item:first-child {
	border-radius: .285rem 0 0 .285rem;
}

.ui.menu .item:before {
	position: absolute;
	content: '';
	top: 0;
	right: 0;
	height: 100%;
	width: 1px;
	background: rgba(34, 36, 38, .1);
}

.ui.menu .item.disabled,
.ui.menu .item.disabled:hover {
	cursor: default;
	background-color: transparent;
	color: rgba(40, 40, 40, .3);
}

.ui.menu .text.item > *,
.ui.menu .item > a:not(.ui),
.ui.menu .item > p:only-child {
	user-select: text;
	line-height: 1.3;
}
.ui.menu .item > p:first-child {
	margin-top: 0;
}
.ui.menu .item > p:last-child {
	margin-bottom: 0;
}

.ui.menu .active.item {
	background: rgba(0, 0, 0, .05);
	color: rgba(0, 0, 0, .95);
	font-weight: normal;
	box-shadow: none;
}

.ui.link.menu .item:hover,
.ui.menu .dropdown.item:hover,
.ui.menu .link.item:hover,
.ui.menu a.item:hover {
  cursor: pointer;
  background: rgba(0, 0, 0, .03);
  color: rgba(0, 0, 0, .95);
}


.ui.vertical.menu {
	display: block;
	flex-direction: column;
	background: #fff;
	box-shadow: 0 1px 2px 0 rgba(34, 36, 38, .15);
	width: 15rem;
}

.ui.vertical.menu .item {
	display: block;
	border-top: none;
	border-right: none;
}
.ui.vertical.menu > .item:first-child {
	border-radius: .285rem .285rem 0 0;
}
.ui.vertical.menu > .item:last-child {
	border-radius: 0 0 .285rem .285rem;
}

.ui.vertical.menu .item:before {
	position: absolute;
	content: '';
	top: 0;
	left: 0;
	width: 100%;
	height: 1px;
	background: rgba(34, 36, 38, .1);
}
.ui.vertical.menu .item:first-child:before {
	display: none;
}

.ui.vertical.menu .item > .menu {
	margin: .5em -1.142em 0;
}
.ui.vertical.menu .menu .item {
	background: none;
	padding: .5em 1.3em;
	font-size: 0.857em;
	color: rgba(0, 0, 0, .5);
}
.ui.vertical.menu .item .menu a.item:hover,
.ui.vertical.menu .item .menu .link.item:hover {
	color: rgba(0, 0, 0, .85);
}
.ui.vertical.menu .menu .item:before {
	display: none;
}

.ui.pagination.menu {
	margin: 0;
	display: inline-flex;
	vertical-align: middle;
}
.ui.pagination.menu .item:last-child {
	border-radius: 0 .285rem .285rem 0;
}
.ui.compact.menu .item:last-child {
	border-radius: 0 .285rem .285rem 0;
}
.ui.pagination.menu .item:last-child:before {
	display: none;
}
.ui.pagination.menu .item {
	min-width: 3em;
	text-align: center;
}
.ui.pagination.menu .icon.item i.fa {
	vertical-align: top;
}

.ui.vertical.menu .item > i.fa {
	width: 1.18em;
    float: right;
    margin: 0em 0em 0em .5em;
}

.ui.menu .header.item,
.ui.vertical.menu .header.item {
	margin: 0;
	text-transform: normal;
	font-weight: bold;
}
.ui.vertical.menu .item > .header:not(.ui) {
	margin: 0 0 .5em;
	font-size: 1em;
	font-weight: bold;
}

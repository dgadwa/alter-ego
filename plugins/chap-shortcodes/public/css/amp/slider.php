[type="slides"] .csc-slide {
	display: flex;
	flex-direction: column;
	align-items: center;
	justify-content: center;
	padding: 0 64px;
	line-height: initial;
}

[type="slides"] .ui.card > .content {
	width: calc(100% - 2em);
}

[type="slides"] .ui.card > .ui.bottom.attached.button {
	width: calc(100% - 3em);
}

.ui.items.loop[type="slides"] .csc-slide.item,
.ui.grid.loop[type="slides"] .csc-slide.column {
	align-items: flex-start;
}

.csc-slide .woocommerce {
	padding: 0.5em;
}

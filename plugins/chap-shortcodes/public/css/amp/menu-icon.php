.ui.vertical.icon.menu {
	display: inline-block;
	width: auto;
}

.ui.icon.menu .item {
	height: auto;
	text-align: center;
	color: #1B1C1D;
}

.ui.icon.menu .item > .fa:not(.dropdown) {
	margin: 0;
	opacity: 1;
}

.ui.icon.menu .fa:before {
	opacity: 1;
}

.ui.menu .icon.item > .fa {
	width: auto;
	margin: 0 auto;
}

.ui.vertical.icon.menu .item > .fa:not(.dropdown) {
	display: block;
	opacity: 1;
	margin: 0 auto;
	float: none;
}

.ui.labeled.icon.menu {
	text-align: center;
}

.ui.labeled.icon.menu .item {
	min-width: 6em;
	flex-direction: column;
}

.ui.labeled.icon.menu .item > .fa:not(.dropdown) {
	height: 1em;
	display: block;
	font-size: 1.714em;
	margin: 0 auto .5rem;
}

.ui.fluid.labeled.icon.menu > .item {
	min-width: 0;
}

.ui.button > .fa:not(.button) {
	height: .857em;
	opacity: .8;
	margin: 0 .423em 0 -.214em;
}
.ui.icon.button {
	padding: .785em;
}
.ui.icon.button:not(.labeled) > .fa {
	margin: 0;
}
.ui.icon.button:not(.circular) > .fa {
	width: 1.1em;
}
.ui.button:not(.icon) > .fa:not(.button):not(.dropdown) {
	margin: 0 .423em 0 -.214em;
}
.ui.button:not(.icon) > .right.fa:not(.button):not(.dropdown) {
	margin: 0 -.214em 0 .423em;
}

<?php /* Labeled button. */ ?>
.ui.labeled.icon.button {
	position: relative;
	padding-right: 1.5em;
	padding-left: 3em;
}
.ui[class*="right labeled"].icon.button {
	padding-right: 3em;
	padding-left: 1.5em;
}
.ui.labeled.icon.button > .fa {
	position: absolute;
	left: .8em;
	right: auto;
	border-top-left-radius: inherit;
	border-bottom-left-radius: inherit;
	width: 2em;
}
.ui[class*="right labeled"].icon.button > .fa {
	left: auto;
	right: 0;
	border-top-right-radius: inherit;
	border-bottom-right-radius: inherit;
}

.ui.disabled.button {
	cursor: default;
	opacity: .45;
	background-image: none;
	box-shadow: none;
	pointer-events: none;
}

.ui.active.button {
	background-color: #C0C1C2;
	background-image: none;
	box-shadow: 0 0 0 1px transparent inset;
	color: rgba(0, 0, 0, .95);
}
.ui.active.button:hover {
	background-color: #C0C1C2;
	background-image: none;
	color: rgba(0, 0, 0, .95);
}
.ui.active.button:active {
	background-color: #C0C1C2;
	background-image: none;
}

.ui[class*="left floated"].buttons,
.ui[class*="left floated"].button {
	float: left;
	margin-left: 0;
	margin-right: .25em;
}
.ui[class*="right floated"].buttons,
.ui[class*="right floated"].button {
	float: right;
	margin-right: 0;
	margin-left: .25em;
}

.ui.mini.button {
	font-size: .6875rem;
}
.ui.tiny.button {
	font-size: .75rem;
}
.ui.small.button {
	font-size: .815rem;
}
.ui.button {
	font-size: .875rem;
}
.ui.large.button {
	font-size: 1rem;
}
.ui.big.button {
	font-size: 1.125rem;
}
.ui.huge.button {
	font-size: 1.25rem;
}
.ui.massive.button {
	font-size: 1.5rem;
}

.ui.circular.button {
	border-radius: 10em;
}
.ui.circular.button > .fa {
	width: 1em;
	vertical-align: baseline;
}

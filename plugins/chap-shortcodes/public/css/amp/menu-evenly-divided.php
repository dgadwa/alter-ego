.ui.item.menu,
.ui.item.menu .item {
	width: 100%;
	padding-left: 0;
	padding-right: 0;
	margin-left: 0;
	margin-right: 0;
	text-align: center;
	justify-content: center;
}
.ui.attached.item.menu {
	margin: -1px;
}
.ui.item.menu .item:last-child:before {
	display: none;
}
.ui.menu.two.item .item {
	width: 50%;
}
.ui.menu.three.item .item {
	width: 33.333%;
}
.ui.menu.four.item .item {
	width: 25%;
}
.ui.menu.five.item .item {
	width: 20%;
}
.ui.menu.six.item .item {
	width: 16.666%;
}
.ui.menu.seven.item .item {
	width: 14.285%;
}
.ui.menu.eight.item .item {
	width: 12.500%;
}
.ui.menu.nine.item .item {
	width: 11.11%;
}
.ui.menu.ten.item .item {
	width: 10.0%;
}
.ui.menu.eleven.item .item {
	width: 9.09%;
}
.ui.menu.twelve.item .item {
	width: 8.333%;
}

.ui.facebook.button {
	background-color: #3B5998;
	color: #fff;
}
.ui.twitter.button {
	background-color: #55ACEE;
	color: #fff;
}
.ui.google.plus.button {
	background-color: #DD4B39;
	color: #fff;
}
.ui.linkedin.button {
	background-color: #1F88BE;
	color: #fff;
}
.ui.vk.button {
	background-color: #4D7198;
	color: #fff;
}
.ui.pinterest.button {
	background-color: #BD081C;
	color: #fff;
}
.ui.instagram.button {
	background-color: #49769C;
	color: #fff;
}
.ui.youtube.button {
	background-color: #CC181E;
	color: #fff;
}

.ui.facebook.button:hover,
.ui.twitter.button:hover,
.ui.google.plus.button:hover,
.ui.linkedin.button:hover,
.ui.vk.button:hover,
.ui.pinterest.button:hover,
.ui.instagram.button:hover,
.ui.youtube.button:hover {
	opacity: .95;
}

.ui.sub.header {
	padding: 0;
	font-weight: bold;
	text-transform: uppercase;
}

h1.ui.header .sub.header {
	font-size: 1.142rem;
}
h2.ui.header .sub.header {
	font-size: 1.142rem;
}
h3.ui.header .sub.header {
	font-size: 1rem;
}
h4.ui.header .sub.header {
	font-size: 1rem;
}
h5.ui.header .sub.header {
	font-size: 0.928rem;
}

.ui.huge.header .sub.header {
	font-size: 1.142rem;
}
.ui.large.header .sub.header {
	font-size: 1.142rem;
}
.ui.header .sub.header {
	font-size: 1rem;
}
.ui.small.header .sub.header {
	font-size: 1rem;
}
.ui.tiny.header .sub.header {
	font-size: 0.928rem;
}

<?php /* Link list */ ?>
.ui.link.list .item,
.ui.link.list a.item,
.ui.link.list .item a:not(.ui) {
	color: rgba(0, 0, 0, 0.4);
}
.ui.link.list a.item:hover,
.ui.link.list .item a:not(.ui):hover {
	color: rgba(0, 0, 0, 0.8);
}
.ui.link.list a.item:active,
.ui.link.list .item a:not(.ui):active {
	color: rgba(0, 0, 0, 0.9);
}
.ui.link.list .active.item,
.ui.link.list .active.item a:not(.ui) {
	color: rgba(0, 0, 0, 0.95);
}

<?php /* Bulleted list */ ?>
ul.ui.list, .ui.bulleted.list {
	margin-left: 1.25rem;
}
ul.ui.list li::before,
.ui.bulleted.list .list > .item::before,
.ui.bulleted.list > .item::before {
	user-select: none;
	pointer-events: none;
	position: absolute;
	top: auto;
	left: auto;
	font-weight: normal;
	margin-left: -1.25rem;
	content: "•";
	opacity: 1;
	color: inherit;
	vertical-align: top;
}
ul.ui.list ul, .ui.bulleted.list .list {
	padding-left: 1.25rem;
}

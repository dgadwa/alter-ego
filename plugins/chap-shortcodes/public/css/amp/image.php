.ui.image {
	position: relative;
	display: inline-block;
	vertical-align: middle;
	max-width: 100%;
	background-color: transparent;
}
amp-img.ui.image {
	display: block;
}
.ui.image svg,
.ui.image amp-img {
	display: block;
	max-width: 100%;
	height: auto;
}

.ui.image:not([class*="center aligned"]):not(.centered):not(.mca) amp-img {
	margin: auto;
}

.ui.inline.image,
.ui.inline.image svg,
.ui.inline.image amp-img {
	display: inline-block;
}

.ui.disabled.image {
	opacity: 0.7;
}

.ui.rounded.images .image,
.ui.rounded.image,
.ui.rounded.images .image > *,
.ui.rounded.image > * {
	border-radius: 0.3125em;
}

.ui.bordered.images .image,
.ui.bordered.images amp-img,
.ui.bordered.images svg,
.ui.bordered.image amp-img,
.ui.bordered.image svg,
amp-img.ui.bordered.image {
	border: 1px solid rgba(0, 0, 0, 0.1);
}

.ui.floated.image,
.ui.floated.images {
	float: left;
	margin-right: 1em;
	margin-bottom: 1em;
}
.ui.right.floated.images,
.ui.right.floated.image {
	float: right;
	margin-right: 0em;
	margin-bottom: 1em;
	margin-left: 1em;
}
.ui.floated.images:last-child,
.ui.floated.image:last-child {
	margin-bottom: 0;
}
.ui.centered.images,
.ui.centered.image {
	margin-left: auto;
	margin-right: auto;
}

.ui.circular.images,
.ui.circular.image {
	overflow: hidden;
}
.ui.circular.images .image,
.ui.circular.image,
.ui.circular.images .image > *,
.ui.circular.image > * {
	border-radius: 500rem;
}

.ui.mini.images .image,
.ui.mini.images amp-img,
.ui.mini.images svg,
.ui.mini.image {
	width: 35px;
	height: auto;
	font-size: 0.78571429rem;
}
.ui.tiny.images .image,
.ui.tiny.images amp-img,
.ui.tiny.images svg,
.ui.tiny.image {
	width: 80px;
	height: auto;
	font-size: 0.85714286rem;
}
.ui.small.images .image,
.ui.small.images amp-img,
.ui.small.images svg,
.ui.small.image {
	width: 150px;
	height: auto;
	font-size: 0.92857143rem;
}
.ui.medium.images .image,
.ui.medium.images amp-img,
.ui.medium.images svg,
.ui.medium.image {
	width: 300px;
	height: auto;
	font-size: 1rem;
}
.ui.large.images .image,
.ui.large.images amp-img,
.ui.large.images svg,
.ui.large.image {
	width: 450px;
	height: auto;
	font-size: 1.14285714rem;
}
.ui.big.images .image,
.ui.big.images amp-img,
.ui.big.images svg,
.ui.big.image,
.ui.huge.images .image,
.ui.huge.images amp-img,
.ui.huge.images svg,
.ui.huge.image,
.ui.massive.images .image,
.ui.massive.images amp-img,
.ui.massive.images svg,
.ui.massive.image {
	width: 600px;
	height: auto;
	font-size: 1.286rem;
}

.ui.spaced.image {
	display: inline-block;
	margin-left: 0.5em;
	margin-right: 0.5em;
}
.ui[class*="left spaced"].image {
	margin-left: 0.5em;
	margin-right: 0;
}
.ui[class*="right spaced"].image {
	margin-left: 0;
	margin-right: 0.5em;
}

.ui.fluid.images,
.ui.fluid.image,
.ui.fluid.images amp-img,
.ui.fluid.images svg,
.ui.fluid.image svg,
.ui.fluid.image amp-img {
	display: block;
	width: 100%;
	height: auto;
}

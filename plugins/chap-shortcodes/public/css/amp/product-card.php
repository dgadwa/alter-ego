.ui.woocommerce-LoopProduct-link {
	font-size: 1.35em;
}
.ui.woocommerce-LoopProduct-link.huge {
	font-size: 1.55em;
}
.ui.woocommerce-LoopProduct-link.large {
	font-size: 1.5em;
}
.ui.woocommerce-LoopProduct-link.small {
	font-size: 1.17em;
}
.ui.woocommerce-LoopProduct-link.tiny {
	font-size: 1em;
}

.woocommerce .ui.product.card .meta {
	font-size: .83em;
}
.woocommerce .ui.product.card .star-rating {
	font-size: .8em;
}
.woocommerce .ui.product.card .meta > a {
	margin-right: 0;
}

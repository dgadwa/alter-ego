.ui.cards {
	display: flex;
	margin: 0 -0.5em;
	flex-wrap: wrap;
	justify-content: center;
}
.ui.cards > .card {
	display: flex;
	margin: 0.875em 0.5em;
	float: none;
}

.ui.cards:after,
.ui.card:after {
	display: block;
	content: ' ';
	height: 0;
	clear: both;
	overflow: hidden;
	visibility: hidden;
}

.ui.cards ~ .ui.cards {
	margin-top: 0.875em;
}

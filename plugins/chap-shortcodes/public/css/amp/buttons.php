.ui.buttons {
	display: inline-flex;
	flex-direction: row;
	font-size: 0;
	vertical-align: baseline;
	margin: 0 0.25em 0 0;
}
.ui.buttons:not(.basic):not(.inverted) {
	box-shadow: none;
}

.ui.buttons:after {
	content: ".";
	display: block;
	height: 0;
	clear: both;
	visibility: hidden;
}

.ui.buttons:not(.stackable) .button {
	flex: 1 0 auto;
	margin: 0;
	border-radius: 0;
	margin: 0;
}
.ui.buttons:not(.stackable):not(.vertical):not(.attached) .button:first-child {
	border-left: none;
	margin-left: 0;
	border-top-left-radius: 0.285rem;
	border-bottom-left-radius: 0.285rem;
}
.ui.buttons:not(.stackable):not(.vertical):not(.attached) .button:last-child {
	border-top-right-radius: 0.285rem;
	border-bottom-right-radius: 0.285rem;
}

.ui.vertical.buttons {
	display: inline-flex;
	flex-direction: column;
}
.ui.vertical.buttons .button {
	display: block;
	float: none;
	width: 100%;
	margin: 0;
	box-shadow: none;
	border-radius: 0;
}
.ui.vertical.buttons .button:first-child {
	border-top-left-radius: 0.285rem;
	border-top-right-radius: 0.285rem;
}
.ui.vertical.buttons .button:last-child {
	margin-bottom: 0;
	border-bottom-left-radius: 0.285rem;
	border-bottom-right-radius: 0.285rem;
}
.ui.vertical.buttons .button:only-child {
	border-radius: 0.285rem;
}
.ui.mini.buttons .button {
	font-size: .6875rem;
}
.ui.tiny.buttons .button {
	font-size: .75rem;
}
.ui.small.buttons .button {
	font-size: .815rem;
}
.ui.buttons .button {
	font-size: .875rem;
}
.ui.large.buttons .button {
	font-size: 1rem;
}
.ui.big.buttons .button {
	font-size: 1.125rem;
}
.ui.huge.buttons .button {
	font-size: 1.25rem;
}
.ui.massive.buttons .button {
	font-size: 1.5rem;
}

.ui.attached.buttons {
	position: relative;
	display: flex;
	border-radius: 0em;
	width: auto;
	z-index: 2;
	margin-left: 0;
	margin-right: -3px;
}
.ui.attached.buttons .button {
	margin: 0em;
}
.ui[class*="top attached"].buttons {
	margin-bottom: -1px;
	border-radius: 0.285rem 0.285rem 0em 0em;
}
.ui[class*="top attached"].buttons .button:first-child {
	border-radius: 0.285rem 0em 0em 0em;
}
.ui[class*="top attached"].buttons .button:last-child {
	border-radius: 0em 0.285rem 0em 0em;
}
.ui[class*="bottom attached"].buttons {
	margin-top: -1px;
	border-radius: 0em 0em 0.285rem 0.285rem;
}
.ui[class*="bottom attached"].buttons .button:first-child {
	border-radius: 0em 0em 0em 0.285rem;
}
.ui[class*="bottom attached"].buttons .button:last-child {
	border-radius: 0em 0em 0.285rem 0em;
}

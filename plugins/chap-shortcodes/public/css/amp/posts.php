.ui.loop .column .ui.header,
.ui.loop .card .ui.header,
.ui.loop .item .ui.header {
	color: rgba(0, 0, 0, .85);
	margin-bottom: 0;
}

.ui.items.loop .content .meta,
.ui.grid.loop .content .meta {
	margin: 0 0 .5em;
	font-size: 1em;
	line-height: 1em;
	color: rgba(0, 0, 0, .6);
}
.ui.loop .content .meta * {
	margin-right: .25em;
}
.ui.loop .content .meta .ui.list:not(.invisible) {
	display: inline-block;
}
.ui.loop .content .meta .ui.list > .item {
	display: inline-block;
	margin-left: .95em;
	font-size: 1em;
	padding-bottom: .214em;
}
.ui.loop .content .meta .ui.link.list .compact.item {
	margin-right: -.85em;
}
.ui.loop .content .meta .ui.list > .item:first-child {
	margin-left: 0;
	padding-left: 0;
}

.ui.items > .item > .content a:not(.ui) {
	color: '';
	transition: color 0.1s ease;
}
.ui.link.list .item, .ui.link.list a.item, .ui.link.list .item a:not(.ui) {
	color: rgba(0, 0, 0, .4);
	transition: 0.1s color ease;
}
.ui.loop .extra .ui.labels {
	display: none;
}
.center.aligned.column {
	text-align: center;
}

.ui.items > .item:not(:last-child) {
	margin-bottom: 1.5em;
}

.ui.items.loop .item .image amp-img,
.ui.grid.loop .column .image {
	margin-left: 0;
	margin-bottom: 0.5em;
	max-width: 100%;
	display: block;
}

.ui.loop .description > p {
	margin-bottom: 0.5em;
}

.ui.cards.loop .extra.content {
	display: none;
}

.ui.compact.items > .item {
	margin: 0.5em 0;
}
.ui.relaxed.items > .item {
	margin: 1.5em 0;
}
.ui[class*="very relaxed"].items > .item {
	margin: 3em 0;
}

.ui.divided.items > .item {
	border-top: 1px solid rgba(34, 36, 38, .15);
	margin: 0;
	padding: 1em 0;
}
.ui.divided.items > .item:first-child {
	border-top: none;
	margin-top: 0;
	padding-top: 0;
}
.ui.divided.items > .item:last-child {
	margin-bottom: 0;
	padding-bottom: 0;
}

.ui.relaxed.divided.items > .item {
	margin: 0;
	padding: 1.5em 0;
}
.ui[class*="very relaxed"].divided.items > .item {
	margin: 0;
	padding: 3em 0;
}

.ui.loop a.header > h2.huge {
	font-size: 1.5em;
}
.ui.loop a.header > h2.large {
	font-size: 1.4em;
}
.ui.loop a.header > h2.medium {
	font-size: 1.3em;
}
.ui.loop a.header > h2.small {
	font-size: 1.2em;
}
.ui.loop a.header > h2.tiny {
	font-size: 1.1em;
}

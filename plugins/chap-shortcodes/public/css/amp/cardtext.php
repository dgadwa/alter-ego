.ui.cards > .card > .content > .description,
.ui.card > .content > .description {
	clear: both;
	color: rgba(0, 0, 0, 0.68);
}

.ui.cards > .card > .content p,
.ui.card > .content p {
	margin: 0 0 0.5em;
}
.ui.cards > .card > .content p:last-child,
.ui.card > .content p:last-child {
	margin-bottom: 0;
}

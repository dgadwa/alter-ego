.ui.arguments.segment .docs {
	font-family: monospace;
	letter-spacing: 1px;
	line-height: 1.4em;
}

.ui.arguments.segment .docs .arg {
	display: inline-block;
	font-weight: bold;
	min-width: 8em;
	letter-spacing: normal;
}

.ui.arguments.segment .docs > .docs {
	margin-left: 1em;
}

.ui.arguments.segment .docs > .docs > .arg {
	min-width: 7em;
	text-align: right;
	padding-right: 0.5em;
	font-weight: normal;
}

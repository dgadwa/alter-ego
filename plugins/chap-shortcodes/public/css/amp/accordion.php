.ui.accordion > section > .title {
	background-color: transparent;
	border: none;
	margin-bottom: 1em;
}
.ui.accordion > section > .title:focus {
	outline: none;
}
.ui.accordion > section:last-child > .title,
.ui.accordion > section[expanded] > .title {
	margin-bottom: 0;
}
.ui.accordion > section > .title > .fa-caret-down {
	margin-right: 0.5em;
}
.ui.accordion > section > .content {
	margin-bottom: 1em;
}
.ui.accordion > section:last-child > .content,
.ui.accordion > section > .content p:last-child {
	margin-bottom: 0;
}

.ui.styled.accordion,
.ui.styled.accordion .accordion {
	border-radius: 0.285rem;
	background: transparent;
	box-shadow: 0 1px 2px 0 rgba(34, 36, 38, 0.15), 0 0 0 1px rgba(34, 36, 38, 0.15);
}
.ui.styled.accordion > section > .title {
	margin: 0;
	padding: 0.75em 1em;
	color: rgba(0, 0, 0, 0.4);
	font-weight: bold;
	border-top: 1px solid rgba(34, 36, 38, 0.15);
}
.ui.styled.accordion > section:first-child > .title {
	border-top: 0;
}
.ui.styled.accordion > section > .title:hover,
.ui.styled.accordion > section[expanded] > .title {
	color: rgba(0, 0, 0, 0.95);
	transition: background 0.1s ease, color 0.1s ease;
}
.ui.styled.accordion > .title:first-child {
	border-top: none;
}
.ui.styled.accordion .content {
	margin: 0;
	padding: 0 1em 1em;
}

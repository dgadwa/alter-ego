.dimmable {
	cursor: pointer;
}
.dimmable > .ui.dimmer[on] {
	position: absolute;
	display: block;
	width: 100%;
	height: 100%;
}
.ui.dimmable:focus, .ui.dimmer:focus {
	outline: 0;
}

.ui.dimmer .content {
	width: 100%;
	height: 100%;
	position: absolute;
	display: flex;
	align-items: center;
	justify-content: center;
	user-select: text;
	background-color: rgba(0, 0, 0, 0.85);
	text-align: center;
}
.ui.dimmer .content > * {
	display: table-cell;
	vertical-align: middle;
	color: #fff;
}
.ui.dimmer .content .ui.header {
	color: #fff;
}
.ui.dimmer .content .ui.button:last-child {
	margin: 0;
}

.ui.inverted.dimmer .content {
	background-color: rgba(255, 255, 255, 0.85);
}
.ui.inverted.dimmer .content > * {
	color: #000;
}
.ui.inverted.dimmer .content .ui.header {
	color: #000;
}

.ui.dimmer .top.aligned.content > * {
	vertical-align: top;
}
.ui.dimmer .bottom.aligned.content > * {
	vertical-align: bottom;
}

.ui.tabular.menu {
	border-radius: 0;
	box-shadow: none;
	border: none;
	background: none transparent;
	border-bottom: 1px solid #D4D4D5;
}
.ui.tabular.fluid.menu {
	width: calc(100% + 2px);
}
.ui.tabular.menu .item {
	background: transparent;
	border-bottom: none;
	border-left: 1px solid transparent;
	border-right: 1px solid transparent;
	border-top: 2px solid transparent;
	padding: .928em 1.428em;
	color: rgba(0, 0, 0, .87);
}
.ui.tabular.menu .item:before {
	display: none;
}

.ui.tabular.menu .item:hover {
	background-color: transparent;
	color: rgba(0, 0, 0, .8);
}

.ui.tabular.menu .active.item {
	background: none #fff;
	color: rgba(0, 0, 0, .95);
	border-top-width: 1px;
	border-color: #D4D4D5;
	font-weight: bold;
	margin-bottom: -1px;
	box-shadow: none;
	border-radius: .285rem .285rem 0 0;
}

.ui.tabular.menu + .attached:not(.top).segment,
.ui.tabular.menu + .attached:not(.top).segment + .attached:not(.top).segment {
	border-top: none;
	margin-left: 0;
	margin-top: 0;
	margin-right: 0;
	width: 100%;
}
.top.attached.segment + .ui.bottom.tabular.menu {
	position: relative;
	width: calc(100% + 2px);
	left: -1px;
}

.ui.bottom.tabular.menu {
	background: none transparent;
	border-radius: 0;
	box-shadow: none;
	border-bottom: none;
	border-top: 1px solid #D4D4D5;
}
.ui.bottom.tabular.menu .item {
	background: none;
	border-left: 1px solid transparent;
	border-right: 1px solid transparent;
	border-bottom: 1px solid transparent;
	border-top: none;
}
.ui.bottom.tabular.menu .active.item {
	background: none #fff;
	color: rgba(0, 0, 0, .95);
	border-color: #D4D4D5;
	margin: -1px 0 0 0;
	border-radius: 0 0 .285rem .285rem;
}

.ui.vertical.tabular.menu {
	background: none transparent;
	border-radius: 0;
	box-shadow: none;
	border-bottom: none;
	border-right: 1px solid #D4D4D5;
}
.ui.vertical.tabular.menu .item {
	background: none;
	border-left: 1px solid transparent;
	border-bottom: 1px solid transparent;
	border-top: 1px solid transparent;
	border-right: none;
}
.ui.vertical.tabular.menu .active.item {
	background: none #fff;
	color: rgba(0, 0, 0, .95);
	border-color: #D4D4D5;
	margin: 0 -1px 0 0;
	border-radius: .285rem 0 0 .285rem;
}

.ui.vertical.right.tabular.menu {
	background: none transparent;
	border-radius: 0;
	box-shadow: none;
	border-bottom: none;
	border-right: none;
	border-left: 1px solid #D4D4D5;
}
.ui.vertical.right.tabular.menu .item {
	background: none;
	border-right: 1px solid transparent;
	border-bottom: 1px solid transparent;
	border-top: 1px solid transparent;
	border-left: none;
}
.ui.vertical.right.tabular.menu .active.item {
	background: none #fff;
	color: rgba(0, 0, 0, .95);
	border-color: #D4D4D5;
	margin: 0 0 0 -1px;
	border-radius: 0 .285rem .285rem 0;
}

.ui.tabular.menu .active.dropdown.item {
	margin-bottom: 0;
	border-left: 1px solid transparent;
	border-right: 1px solid transparent;
	border-top: 2px solid transparent;
	border-bottom: none;
}

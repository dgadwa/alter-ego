.ui.container {
	width: auto;
}
.ui.grid.container {
	width: auto;
}
.ui.relaxed.grid.container {
	width: auto;
}
.ui.very.relaxed.grid.container {
	width: auto;
}

.ui.text.container {
	margin: 0 auto;
	line-height: 1.5;
}
.ui.text.container {
	font-size: 1.142rem;
}

.ui.fluid.container {
	max-width: 100%;
}
.ui[class*="left aligned"].container {
	text-align: left;
}
.ui[class*="center aligned"].container {
	text-align: center;
}
.ui[class*="right aligned"].container {
	text-align: right;
}
.ui.justified.container {
	text-align: justify;
}

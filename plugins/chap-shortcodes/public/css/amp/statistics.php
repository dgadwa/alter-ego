.ui.statistics {
	display: flex;
	align-items: flex-start;
	flex-wrap: wrap;
}
.ui.statistics > .statistic {
	display: inline-flex;
	flex: 0 1 auto;
	flex-direction: column;
	margin: 0 1.5em 2em;
	max-width: auto;
}
.ui.statistics {
	display: flex;
	margin: 1em -1.5em -2em;
}
.ui.statistics:after {
	display: block;
	content: ' ';
	height: 0;
	clear: both;
	overflow: hidden;
	visibility: hidden;
}
.ui.statistics:first-child {
	margin-top: 0;
}
.ui.statistics:last-child {
	margin-bottom: 0;
}

.ui.evenly.divided.statistics {
	justify-content: center;
}

.ui.evenly.divided.statistics  > .statistic:first-child {
	margin: 0;
}

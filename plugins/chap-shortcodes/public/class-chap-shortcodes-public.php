<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://chap.website
 * @since      1.0.0
 *
 * @package    Chap_Shortcodes
 * @subpackage Chap_Shortcodes/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Chap_Shortcodes
 * @subpackage Chap_Shortcodes/public
 * @author     websevendev <websevendev@gmail.com>
 */
class Chap_Shortcodes_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $chap_shortcodes    The ID of this plugin.
	 */
	private $chap_shortcodes;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $chap_shortcodes       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct($chap_shortcodes, $version) {

		$this->chap_shortcodes = $chap_shortcodes;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		// wp_enqueue_style( $this->chap_shortcodes, plugin_dir_url( __FILE__ ) . 'css/chap-shortcodes-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		// wp_enqueue_script( $this->chap_shortcodes, plugin_dir_url( __FILE__ ) . 'js/chap-shortcodes-public.js', array( 'jquery' ), $this->version, false );

	}

}

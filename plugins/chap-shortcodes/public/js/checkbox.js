/**
 * Initialize checkboxes.
 */
(function($) {
	'use strict';
	$(document).ready(function(){
		$('.ui.checkbox').checkbox();
	});
})(jQuery);

/**
 * Do some string replacements after code highlighting.
 */
Prism.hooks.add('after-highlight', function(e){
	var code = jQuery(e.element).html();

	/**
	 * Restore brackets.
	 */
	if(e.language === 'wiki') {
		code = code.split('&lt;').join('[');
		code = code.split('&gt;').join(']');
		code = code.split('{lt}').join('&lt;');
		code = code.split('{gt}').join('&gt;');
	}

	/**
	 * Highlight.
	 */
	code = code.split('(((chap-mark-start)))').join('<mark>');
	code = code.split('(((chap-mark-end)))').join('</mark>');

	jQuery(e.element).html(code);
});
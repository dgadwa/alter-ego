/**
 * Initialize embedded content.
 */
(function($) {
	'use strict';
	$(document).ready(function(){
		$('.ui.embed').embed();
	});
})(jQuery);

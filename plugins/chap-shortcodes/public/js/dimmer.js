/**
 * Initialize dimmers.
 */
(function($) {
	'use strict';
	$(document).ready(function(){
		$('.ui.dimmable').dimmer({
			on: 'hover',
		});
	});
})(jQuery);

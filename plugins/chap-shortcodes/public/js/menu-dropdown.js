/**
 * Initialize menu dropdowns.
 */
(function($) {
	'use strict';
	$(document).ready(function(){
		$('.csc-menu .ui.dropdown.item').dropdown({
			on: 'hover',
		});
	});
})(jQuery);

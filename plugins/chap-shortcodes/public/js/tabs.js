/**
 * Initialize tabs.
 */
(function($) {
	'use strict';
	$(document).ready(function(){
		$('.csc-tabs .item:not(.disabled)').tab();
	});
})(jQuery);

/**
 * Smooth scrolling to anchor links.
 *
 * @since 1.0.3
 * @see https://css-tricks.com/snippets/jquery/smooth-scrolling/
 */
(function($) {
	'use strict';
	$(document).ready(function(){

		$('a[href*="#"]:not([href="#"])').click(function(){
			if(location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
				var target = $(this.hash);
				target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
				if(target.length) {
					$('html, body').animate({
						scrollTop: target.offset().top,
					}, 500);
					return false;
				}
			}
		});

	});
})(jQuery);

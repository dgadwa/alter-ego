/**
 * Initialize dropdowns.
 */
(function($) {
	'use strict';
	$(document).ready(function(){
		$('select.chap-sc.dropdown').dropdown();
	});
})(jQuery);

/**
 * Initialize accordions.
 */
(function($) {
	'use strict';
	$(document).ready(function(){
		$('.ui.accordion').accordion();
	});
})(jQuery);

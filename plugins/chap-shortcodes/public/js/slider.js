/**
 * Initialize Swiper sliders.
 */
var csc_swipers = {};
var csc_init_sliders = function() {
    (typeof csc_init_swipers === 'undefined' ? [] : csc_init_swipers).forEach(function(csc_swiper){
        csc_swiper = csc_swiper();
        csc_swipers[csc_swiper.csc_id] = new Swiper('#' + csc_swiper.csc_id, csc_swiper);
    });
};
if(wp.hooks) {
    wp.hooks.addAction('chap_init', csc_init_sliders, 10);
} else {
    document.addEventListener('ready', csc_init_sliders);
}

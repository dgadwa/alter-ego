<?php

/**
 * Fired when the plugin is uninstalled.
 *
 * @link       http://chap.website
 * @since      1.0.0
 *
 * @package    Chap_Shortcodes
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

if(!defined('WP_UNINSTALL_PLUGIN')) {
	exit; // If uninstall not called from WordPress, then exit.
}

<?php
/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://chap.website
 * @since             1.0.0
 * @package           Chap_Shortcodes
 *
 * @wordpress-plugin
 * Plugin Name:       Chap Shortcodes
 * Plugin URI:        http://chap.website
 * Description:       Contains shortcodes for the Chap theme.
 * Version:           1.8.6
 * Author:            websevendev
 * Author URI:        http://themeforest.net/user/websevendev
 * License:           Regular License
 * License URI:       http://themeforest.net/licenses/terms/regular
 * Text Domain:       chap-shortcodes
 * Domain Path:       /languages
 */

namespace Chap\Shortcodes;

// If this file is called directly, abort.
if(!defined('WPINC')) {
	die;
}

/**
 * Returns the plugins root dir
 * Ex.: C:\www\site\wp-content\plugins\this-plugin\
 */
function get_plugin_dir() {
	return plugin_dir_path(__FILE__);
}

/**
 * Returns the plugins root url
 * Ex.: http://127.0.01/wp-content/plugins/this-plugin/
 */
function get_plugin_url() {
	return plugin_dir_url(__FILE__);
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-chap-shortcodes-activator.php
 */
function activate_chap_shortcodes() {
	require_once plugin_dir_path(__FILE__) . 'includes/class-chap-shortcodes-activator.php';
	Chap_Shortcodes_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-chap-shortcodes-deactivator.php
 */
function deactivate_chap_shortcodes() {
	require_once plugin_dir_path(__FILE__) . 'includes/class-chap-shortcodes-deactivator.php';
	Chap_Shortcodes_Deactivator::deactivate();
}

register_activation_hook(__FILE__, __NAMESPACE__ . '\\activate_chap_shortcodes');
register_deactivation_hook(__FILE__, __NAMESPACE__ . '\\deactivate_chap_shortcodes');

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path(__FILE__) . 'includes/class-chap-shortcodes.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_chap_shortcodes() {

	/**
	 * Initialize and run.
	 */
	$plugin = new Chap_Shortcodes();
	$plugin->run();

	/**
	 * Global variable to access the plugin.
	 *
	 * @global $plugin_chap_shortcodes
	 */
	global $plugin_chap_shortcodes;
	$plugin_chap_shortcodes = $plugin;

}
run_chap_shortcodes();

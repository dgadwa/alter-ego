<?php

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

/**
 * Social Medias to use with [social] shortcode.
 *
 * @see https://github.com/bradvin/social-share-urls
 */
$social_medias = [
	'facebook' => [
		'name' => esc_html__('Facebook', 'chap-shortcodes'),
		'placeholder' => 'https://facebook.com/user',
		'share' => 'https://www.facebook.com/sharer.php?u={url}',
	],
	'twitter' => [
		'name' => esc_html__('Twitter', 'chap-shortcodes'),
		'placeholder' => 'https://twitter.com/@user',
		'share' => 'https://twitter.com/intent/tweet?url={url}&text={title}',
	],
	'google plus' => [
		'name' => esc_html__('Google Plus', 'chap-shortcodes'),
		'placeholder' => 'https://plus.google.com/+user',
		'share' => 'https://plus.google.com/share?url={url}',
	],
	'linkedin' => [
		'name' => esc_html__('LinkedIn', 'chap-shortcodes'),
		'placeholder' => 'https://linkedin.com/in/user',
		'share' => 'https://www.linkedin.com/shareArticle?url={url}&title={title}',
	],
	'vk' => [
		'name' => esc_html__('VK', 'chap-shortcodes'),
		'placeholder' => 'https://vk.com/user',
		'share' => 'http://vk.com/share.php?url={url}',
	],
	'pinterest' => [
		'name' => esc_html__('Pinterest', 'chap-shortcodes'),
		'placeholder' => 'https://pinterest.com/user',
		'share' => 'https://pinterest.com/pin/create/bookmarklet/?media={img}&url={url}&description={title}',
	],
	'instagram' => [
		'name' => esc_html__('Instagram', 'chap-shortcodes'),
		'placeholder' => 'https://instagram.com/user',
	],
	'youtube' => [
		'name' => esc_html__('YouTube', 'chap-shortcodes'),
		'placeholder' => 'https://youtube.com/user',
	],
];

return $social_medias;

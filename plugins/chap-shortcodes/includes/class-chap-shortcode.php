<?php

namespace Chap\Shortcodes;
use Chap\Helpers;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

/**
 * Chap_Shortcode class is used to register
 * a shortcode and render it's content.
 *
 * Each shortcode extends this class by supplying
 * it's own parameters and a rendering function.
 */
abstract class Chap_Shortcode {

	/**
	 * True if "shortcodes without prefix" option is enabled.
	 *
	 * @var boolean
	 */
	public $no_prefix;

	/**
	 * Custom prefix for shortcodes.
	 *
	 * @var string
	 */
	public $prefix;

	/**
	 * True if a shortcode with this ID already exists.
	 *
	 * @var boolean
	 */
	protected $already_exists = false;

	/**
	 * The ID of the shortcode.
	 * May change in the shortcode registration process.
	 *
	 * @var string
	 */
	public $id;

	/**
	 * The original ID, without any prefixes.
	 *
	 * @var string
	 */
	public $original_id;

	/**
	 * All IDs of the registered shortcode variations.
	 * Such as: button, chap-button, custom_prefix_button.
	 *
	 * @var array
	 */
	public $ids = [];

	/**
	 * Default args of the shortcode.
	 *
	 * @var array
	 */
	public $default_args = [];

	/**
	 * Set to false if shortcode doesn't accept any content.
	 *
	 * true  - [shortcode]content[/shortcode]
	 * false - [shortcode]
	 *
	 * @var boolean
	 */
	public $has_content = true;

	/**
	 * The description of the shortcode.
	 *
	 * @var string
	 */
	public $description = '';

	/**
	 * Documentation of arguments.
	 *
	 * @var array
	 */
	public $arg_docs = [];

	/**
	 * The presets for shortcode, displayed
	 * when inserting the shortcode in editor.
	 *
	 * @var array
	 */
	public $presets = [];

	/**
	 * Shortcode arguments.
	 *
	 * @var array
	 */
	public $args = [];

	/**
	 * Shortcode content passed through do_shortcode().
	 *
	 * @var string
	 */
	public $content = '';

	/**
	 * Shortcode content.
	 *
	 * @var string
	 */
	public $raw_content = '';

	/**
	 * HTML attributes collected from the shortcode arguments that
	 * can be used to render an HTML tag by using $this->html_tag().
	 *
	 * @var array
	 */
	public $atts = [];

	/**
	 * Initialize the shortcode.
	 */
	public function __construct($id, $settings) {
		$this->id = $id;
		$this->original_id = $id;
		$this->prefix = $settings['prefix'];
		$this->no_prefix = $settings['no-prefix'];

		$this->register();
	}

	/**
	 * Reset the shortcode values, because only
	 * 1 instance of this class is used for rendering
	 * multiple instances of the same shortcode.
	 */
	public function reset() {
		$this->args = [];
		$this->atts = [];
		$this->has_content = true;
		$this->content = '';
		$this->raw_content = '';
	}

	/**
	 * Register the shortcode.
	 */
	protected function register() {

		/**
		 * Register the default [csc-shortcode] format, used internally.
		 */
		$shortcode = 'csc-' . $this->original_id;
		add_shortcode($shortcode, [&$this, 'render']);

		/**
		 * Register the default [chap-shortcode] format.
		 */
		$shortcode = 'chap-' . $this->original_id;
		add_shortcode($shortcode, [clone $this, 'render']);
		$this->id = $shortcode;
		$this->ids[] = $shortcode;

		/**
		 * Register the shortcode with prefix.
		 */
		if(strlen($this->prefix) > 0 && $this->prefix !== 'chap-') {

			$shortcode = $this->prefix . $this->original_id;
			if(!shortcode_exists($shortcode)) {
				add_shortcode($shortcode, [clone $this, 'render']);
				$this->id = $shortcode;
				$this->ids[] = $shortcode;
			}

		}

		/**
		 * Register the shortcode without prefix.
		 */
		if($this->no_prefix) {

			$shortcode = $this->original_id;
			if(!shortcode_exists($shortcode)) {
				add_shortcode($shortcode, [clone $this, 'render']);
				$this->id = $shortcode;
				$this->ids[] = $shortcode;
			}

		}

		/**
		 * Prioritize prefixless > custom prefix > default prefix.
		 */
		$this->ids = array_reverse($this->ids);

	}

	/**
	 * Parse the shortcode arguments.
	 *
	 * @param  array $args
	 * @param  array $defaults
	 * @return array
	 */
	public function parse_args($args = [], $defaults = []) {

		$this->reset();

		/**
		 * Holds the class arguments of the shortcode.
		 *
		 * @var array
		 */
		$classes = [];

		/**
		 * Collect classes from the "classes" argument.
		 */
		if(isset($args['classes'])) {
			$classes = explode(' ', $args['classes']);
			unset($args['classes']);
		}

		/**
		 * Collect classes from anonymous arguments.
		 */
		if(!empty($args)) {
			foreach($args as $key => $value) {
				if(is_int($key)) {
					$classes[] = $value;
					unset($args[$key]);
				}
			}
		}

		/**
		 * Parse the other arguments.
		 */
		$args = wp_parse_args($args, $defaults);

		/**
		 * Add collected classes to the parsed arguments.
		 */
		$args['classes'] = $classes;

		/**
		 * Ellipsis.
		 * Add jQuery.dotdotdot trigger classes.
		 */
		if(in_array('ellipsis', $args['classes']) && $this->original_id !== 'icon') {
			$key = array_search('ellipsis', $args['classes']);
			$args['classes'][$key] = 'dot-ellipsis dot-resize-update';
		}

		/**
		 * Arguments that should be added to the HTML attributes list.
		 *
		 * @var array
		 */
		$html_atts = [
			'id',
			'name',
			'type',
			'value',
			'classes',
			'url',
			'link',
			'href',
			'method',
			'action',
			'placeholder',
			'style',
			'target',
			'rel',
			'download',
			'onclick',
			'clip',
			'anim',
			'anim-offset',
			'anim-duration',
			'anim-easing',
			'anim-delay',
			'anim-anchor',
			'anim-anchor-placement',
			'anim-once',
			'role',
			'tabindex',
			'gradient',
			'text-gradient',
			'required',
		];

		/**
		 * Add AMP HTML attributes.
		 */
		if(Helpers\is_amp()) {
			$html_atts = array_merge($html_atts, [
				'on',
				'layout',
				'sizes',
			]);
		}

		/**
		 * Allow to customize atts.
		 */
		$html_atts = apply_filters('csc_html_atts_passthrough', $html_atts);

		/**
		 * Add matching shortcode arguments to the HTML attributes list.
		 */
		foreach($args as $key => $value) {

			if(in_array($key, $html_atts) || strpos($key, 'data-') !== false) {

				/**
				 * URL may be specified as href, link or url argument.
				 */
				if($key === 'url' || $key === 'link') {
					$key = 'href';
					$args['href'] = $value; // Store the link in href arg.
				}

				/**
				 * Shortcode arguments cannot contain square brackets,
				 * so double parenthesis can be used instead.
				 *
				 * Make the replacements here to support that.
				 */
				$value = str_replace(['((', '))'], ['[', ']'], $value);

				/**
				 * Create clip-path CSS.
				 *
				 * Clip attribute can be specified in a shortcode as:
				 *   clip="0 0" or clip="0"
				 *
				 * First value: top angle
				 * Second value: bottom angle
				 * One value: top and bottom angle
				 */
				if($key === 'clip') {

					$clip = explode(' ', $value);

					// If single number, us it as both values.
					if(count($clip) < 2) {
						$clip[1] = $clip[0];
					}

					$clip_top = round(intval($clip[0]));
					$top_left = $clip_top > 0 ? $clip_top / 2 : 0;
					$top_right = $clip_top < 0 ? abs($clip_top) / 2 : 0;

					$clip_bottom = round(intval($clip[1]));
					$bottom_left = $clip_bottom < 0 ? abs($clip_bottom) / 2 : 0;
					$bottom_right = $clip_bottom > 0 ? $clip_bottom / 2 : 0;

					// Add to styles.
					$key = 'style';
					$value = 'clip-path:polygon(' .
						'0 calc(0% + ' . $top_left . 'vw),' .
						'100% calc(0% + ' . $top_right . 'vw),' .
						'100% calc(100% - ' . $bottom_right . 'vw),' .
						'0 calc(100% - ' . $bottom_left . 'vw)' .
					')';

				}

				/**
				 * Create gradient background CSS.
				 */
				if($key === 'gradient' || $key === 'text-gradient') {

					/**
					 * Create a linear gradient or use custom CSS function.
					 */
					if(strpos($value, 'gradient(') === false) {
						$value = 'linear-gradient(' . $value . ')';
					}
					$value = 'background-image:' . $value . ';';

					/**
					 * Create gradient text CSS.
					 */
					if($key === 'text-gradient') {
						$value .= '-webkit-background-clip:text;';
						$value .= '-moz-background-clip:text;';
						$value .= 'background-clip:text;';
						$value .= '-webkit-text-fill-color:transparent;';
						$value .= '-moz-text-fill-color:transparent;';
						$value .= 'text-fill-color:transparent;';
					}

					$key = 'style';

				}

				/**
				 * Convert animation-* to data-aos*.
				 */
				if(strpos($key, 'animation') === 0) {
					$key = str_replace('animation', 'data-aos', $key);
				}

				/**
				 * Convert anim-* to data-aos*.
				 */
				if(strpos($key, 'anim') === 0) {
					$key = str_replace('anim', 'data-aos', $key);
				}

				/**
				 * Store the value in attributes, used to render HTML tag.
				 */
				if($key === 'style') {
					/**
					 * Styles are stored as array.
					 */
					$this->atts['style'][] = rtrim($value, ';');
				} else {
					/**
					 * Store the value normally.
					 */
					$this->atts[$key] = $value;
				}

			}

		}

		/**
		 * Combine the classes to a single string.
		 */
		$args['classes'] = join(' ', $args['classes']);

		/**
		 * Use defaults if no classes are provided.
		 */
		if(empty($args['classes']) && isset($this->default_args['classes'])) {
			$args['classes'] = $this->default_args['classes'];
		}

		/**
		 * Store the shortcode arguments in the Chap_Shortcode instance.
		 */
		$this->args = $args;

		/**
		 * Return the shortcode arguments.
		 */
		return $args;

	}

	/**
	 * Determines if the current shortcode has an argument.
	 *
	 * @param  string  $key The argument name.
	 * @return boolean
	 */
	public function has_arg($key) {
		return isset($this->args[$key]) && strlen($this->args[$key]) > 0;
	}

	/**
	 * Determines if the current shortcode has all of the specified arguments.
	 *
	 * @param  array   $keys The argument names.
	 * @return boolean
	 */
	public function has_args($keys) {
		foreach($keys as $key) {
			if(!$this->has_arg($key)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Returns a boolean of an argument
	 *
	 * @param  array   $key The argument name.
	 * @return boolean
	 */
	public function bool_arg($key) {
		if(!$this->has_arg($key)) {
			return false;
		}
		if(!$this->args[$key]) {
			return false;
		}
		if($this->args[$key] === 'false') {
			return false;
		}
		if($this->args[$key] === 'true') {
			return true;
		}
		if(intval($this->args[$key]) === 1) {
			return true;
		}
		return false;
	}

	/**
	 * Adds a class to the front of the list of HTML attributes.
	 *
	 * @param string $class The class name.
	 */
	public function prepend_class($class) {
		array_unshift($this->atts['classes'], $class);
	}

	/**
	 * Adds multiple classes to the front of the list of HTML attributes.
	 *
	 * @param array $class The class names.
	 */
	public function prepend_classes($classes) {
		foreach($classes as $class) {
			$this->append_class($class);
		}
	}

	/**
	 * Determines if the current shortcode has specified class.
	 *
	 * @param  string  $class The class name.
	 * @return boolean
	 */
	public function has_class($class) {
		return strpos($this->args['classes'], $class) !== false;
	}

	/**
	 * Determines if the current shortcode has the specified classes.
	 *
	 * @param  array   $classes The class names.
	 * @return boolean
	 */
	public function has_classes($classes) {

		foreach($classes as $class) {
			if(!$this->has_class($class)) {
				return false;
			}
		}

		return true;
	}

	/**
	 * Determines if the current shortcode has the specified class string.
	 * Useful to check existance of multiple consecutive classes, where order matters.
	 *
	 * @param  string   $classes The class(es).
	 * @return boolean
	 */
	public function has_class_string($classes) {
		return (strpos($this->args['classes'], $classes) !== false);
	}

	/**
	 * Adds a class to the list of HTML attributes.
	 *
	 * @param string $class The class name.
	 */
	public function add_class($class) {
		$this->atts['classes'][] = $class;
	}

	/**
	 * Adds multiple classes to the list of HTML attributes.
	 *
	 * @param array $classes The class names.
	 */
	public function add_classes($classes) {
		foreach($classes as $class) {
			$this->add_class($class);
		}
	}

	/**
	 * Returns an HTML tag with attributes and content.
	 *
	 * @param  string  $tag          The HTML tag name.
	 * @param  boolean $self_closing Tag should close itself.
	 * @return string                The HTML output.
	 */
	public function html_tag($tag, $self_closing = false) {

		if($tag === 'div' && $this->has_arg('href')) {
			$tag = 'a';
		}

		/**
		 * Role attribute shouldn't be outputted if it's empty.
		 */
		if(isset($this->atts['role']) && empty($this->atts['role'])) {
			unset($this->atts['role']);
		}

		$html_tag = Helpers\html_tag($tag, $this->atts, $self_closing);

		if(!$self_closing) {
			$html_tag .= $this->content;
			$html_tag .= '</' . $tag . '>';
		}

		return $html_tag;
	}

	/**
	 * Adds inline style HTML attribute.
	 *
	 * @param string $att   The CSS attribute.
	 * @param string $value The CSS value.
	 */
	public function add_inline_style($att, $value) {
		$this->atts['style'][] = $att . ':' . $value;
	}

	/**
	 * Enqueues a style from the styles
	 * directory to be used with the shortcode.
	 *
	 * @param string $name The name of the JavaScript file.
	 */
	public function add_style($name) {
		wp_enqueue_style('csc-' . $name, get_plugin_url() . 'public/css/' . $name . '.css');
	}

	/**
	 * Enqueues a script from the scripts
	 * directory to be used with the shortcode.
	 *
	 * @param string $name The name of the JavaScript file.
	 */
	public function add_script($name) {
		wp_enqueue_script('csc-' . $name, get_plugin_url() . 'public/js/' . $name . '.js', ['chap/js'], false, true);
	}

	/**
	 * Add AMP stylesheet to a global list of styles to be loaded.
	 */
	public function add_amp_style($id = '') {
		if(!Helpers\is_amp()) {
			return;
		}

		if(empty($id)) {
			$id = $this->original_id;
		}

		if(is_array($GLOBALS['csc']['amp-styles'])) {
			$GLOBALS['csc']['amp-styles'][$id] = true;
		}
	}

	/**
	 * Add AMP component script.
	 */
	public function add_amp_script($script, $version = '0.1') {
		if(!Helpers\is_amp()) {
			return;
		}

		if(is_array($GLOBALS['csc']['amp-scripts'])) {
			$GLOBALS['csc']['amp-scripts'][$script] = $version;
		}
	}

	/**
	 * Render the shortcode.
	 * Can be overridden to parse args differently.
	 */
	public function render($args, $content = '') {

		/**
		 * Don't render shortcodes disabled in AMP.
		 */
		if(Helpers\is_amp() && in_array($this->original_id, [
			'form',
			'fields',
			'field',
			'input',
			'checkbox',
			'select',
			'option',
			'textarea',
		])) {
			return;
		}

		$this->parse_args($args, $this->default_args);

		/**
		 * Don't render based on AMP and classes.
		 */
		if(Helpers\is_amp()) {
			if($this->has_class('amp-invisible')) {
				return;
			}
		} else {
			if($this->has_class('amp-only')) {
				return;
			}
		}

		$this->add_amp_style();
		$this->raw_content = $content;
		$this->content = do_shortcode($content);

		return $this->render_shortcode();
	}

	/**
	 * Override to render the shortcode output.
	 */
	public function render_shortcode() {
		return;
	}

	/**
	 * Returns the text that is used to call
	 * the shortcode with the provided args.
	 *
	 * @param  array  $args
	 * @return string
	 */
	public function shortcode_from_args($args = []) {

		$all_args = '';

		foreach($args as $key => $arg) {
			if(is_int($key)) {
				$all_args .= ' ' . $arg;
			} else {
				$all_args .= ' ' . $key . '=&quot;' . $arg . '&quot;';
			}
		}

		if(!$this->has_content) {
			return '[' . $this->id . $all_args . ']';
		}

		return '[' . $this->id . $all_args . '][/' . $this->id . ']';

	}

	/**
	 * Returns the modal tab the shortcode belongs to.
	 */
	public function get_tab() {

		$tabs = include plugin_dir_path(dirname(__FILE__)) . 'includes/modal-tabs-shortcode.php';

		foreach($tabs as $name => $shortcodes) {
			if(in_array($this->original_id, $shortcodes)) {
				return $name;
			}
		}

		return 'elements';

	}

	/**
	 * Composes the arguments documentation HTML.
	 *
	 * @return string
	 */
	public function get_docs_html() {

		$docs = '';

		foreach($this->arg_docs as $arg => $doc) {

			$docs .= '<div class="docs"><div class="arg">' . $arg . ':</div>';

			if(is_array($doc)) {
				foreach($doc as $key => $val) {
					$docs .= '<div class="docs"><div class="arg">' . ucfirst($key) . ' -</div>&#91;' . join('|', $val) . '&#93;</div>';
				}
			} else {
				$docs .= $doc;
			}

			$docs .= '</div>';

		}

		return $docs;

	}

	/**
	 * Returns the online documentation URI.
	 *
	 * @return string
	 */
	public function get_docs_uri() {

		$uris = include plugin_dir_path(dirname(__FILE__)) . 'includes/shortcodes-online-docs.php';

		if(isset($uris[$this->original_id])) {
			return 'https://chap.website/' . $uris[$this->original_id];
		}

		return false;

	}

	/**
	 * Render description and arguments documentation.
	 */
	public function render_docs() {

		$docs = $this->get_docs_html();
		$description = !empty($this->description) ? '<p>' . $this->description . '</p>' : '';
		if($uri = $this->get_docs_uri()) {
			echo '<a href="' . esc_url($uri) . '" target="_blank" class="online-docs dashicons-before dashicons-book">' . esc_html__('Online docs', 'chap-shortcodes') . '</a>';
		}

		echo wp_kses_post($description);
		if(!empty($docs)) {
			echo '<p class="docs_toggle closed">' . esc_html__('Arguments', 'chap-shortcodes') . '<span class="toggle-indicator"></span></p>';
			echo '<div class="docs_wrapper hidden">' . $docs . '</div>';
		}

	}

	/**
	 * Renders the admin UI for inserting this shortcode.
	 * Can be overridden when custom UI is necessary.
	 */
	public function render_ui() {
		include plugin_dir_path(dirname(__FILE__)) . 'admin/partials/chap-shortcodes-ui-shortcode.php';
	}

}

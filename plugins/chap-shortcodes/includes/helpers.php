<?php

namespace Chap\Helpers;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

/**
 * Takes an array of attributes and composes them
 * to be used inside an HTML element.
 *
 * Ex.: echo '<div ' . Helpers\html_atts($atts) . '></div>';
 *
 * @param  array
 * @return string
 */
if(!function_exists(__NAMESPACE__ . '\\html_atts')):

	function html_atts($atts) {

		$output = [];

		if(!isset($atts['classes'])) {
			$atts['classes'] = [];
		}

		if(isset($atts['class'])) {
			$classes = explode(' ', $atts['class']);
			$atts['classes'] = array_merge($atts['classes'], $classes);
			unset($atts['class']);
		}

		foreach($atts as $key => $val) {

			if($key === 'classes') {

				if(count($val) > 0) {
					$output[] = 'class="' . esc_attr(join(' ', $val)) . '"';
				}

			} elseif($key === 'style') {

				if(count($val) > 0) {
					$output[] = 'style="' . esc_attr(join(';', $val)) . '"';
				}

			} else {

				if($key === 'href' && empty($val)) {
					$val = '#';
				}

				$output[] = $key . '="' . esc_attr($val) . '"';

			}

		}

		return join(' ', $output);

	}

endif;

/**
 * Renders an HTML tag.
 *
 * @param  string  $tag   tag name
 * @param  array   $atts  attributes
 * @param  boolean $close self-closing tag
 * @return string         html tag
 */
if(!function_exists(__NAMESPACE__ . '\\html_tag')):

	function html_tag($tag, $atts, $close = false) {

		if($tag !== 'a' && isset($atts['href'])) {
			unset($atts['href']);
		}

		$end = $close ? ' />' : '>';

		return '<' . $tag . ' ' . html_atts($atts) . $end;

	}

endif;


/**
 * Replace text between 2 strings.
 */
if(!function_exists(__NAMESPACE__ . '\\replace_string_between')):

	function replace_string_between($str, $needle_start, $needle_end, $replacement) {

		$pos = strpos($str, $needle_start);
		$start = $pos === false ? 0 : $pos + strlen($needle_start);
		$pos = strpos($str, $needle_end, $start);
		$end = $pos === false ? strlen($str) : $pos;

		return substr_replace($str, $replacement, $start, $end - $start);

	}

endif;

/**
 * Turns numbers into words to use as Sematic UI classes.
 */
if(!function_exists(__NAMESPACE__ . '\\number_to_word')):

	function number_to_word($input) {
		$words = ['sixteen', 'fifteen', 'fourteen', 'thirteen', 'twelve', 'eleven', 'ten', 'nine', 'eight', 'seven', 'six', 'five', 'four', 'three', 'two', 'one'];
		$numbers = [16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1];
		return str_replace($numbers, $words, $input);
	}

endif;

/**
 * Determine if current page is an AMP endpoint.
 */
if(!function_exists(__NAMESPACE__ . '\\is_amp')):

	function is_amp() {

		/**
		 * Not ready yet.
		 */
		if(!did_action('parse_query')) {
			return false;
		}

		if(function_exists('is_amp_endpoint') && is_amp_endpoint()) {
			return true;
		}

		return false;

	}

endif;

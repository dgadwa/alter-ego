<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

/**
 * Chap_Composite class is used to provide
 * composites of shortcodes.
 *
 * Each composite extends this class by supplying
 * it's content.
 */
abstract class Chap_Composite {

	/**
	 * True if "shortcodes without prefix" option is enabled.
	 *
	 * @var boolean
	 */
	public $no_prefix;

	/**
	 * Custom prefix for shortcodes.
	 *
	 * @var string
	 */
	public $prefix;

	/**
	 * The prefix to use.
	 */
	public $p = '';

	/**
	 * The ID of the composite.
	 *
	 * @var string
	 */
	public $id;

	/**
	 * The display name of the composite.
	 *
	 * @var string
	 */
	public $name;

	/**
	 * The image of the composite.
	 *
	 * @var string
	 */
	public $image = '';

	/**
	 * The description of the composite.
	 *
	 * @var string
	 */
	public $description = '';

	/**
	 * The content of the composite.
	 *
	 * @var string
	 */
	public $content = '';

	/**
	 * Initialize the shortcode.
	 */
	public function __construct($id, $settings) {

		$this->id = $id;
		$this->name = $this->get_name();
		$this->prefix = $settings['prefix'];
		$this->no_prefix = $settings['no-prefix'];

		/**
		 * Use prefix.
		 */
		if(!$this->no_prefix) {

			if(!empty($this->prefix)) {
				/**
				 * Custom prefix.
				 */
				$this->p = $this->prefix;
			} else {
				/**
				 * Default prefix.
				 */
				$this->p = 'chap-';
			}

		}

		$this->load_content();

	}

	/**
	 * Display name for the component.
	 *
	 * @return string
	 */
	public function get_name() {
		if(isset($this->name)) {
			return $this->name;
		}
		$name = $this->id;
		$name = str_replace('slide-', '', $name);
		$name = str_replace('page-', '', $name);
		$name = str_replace('-', ' ', $name);
		$name = ucfirst($name);
		return $name;
	}

	/**
	 * Loads the content of the composite.
	 */
	public function load_content() {
		$this->content = $this->get_content();
	}

	/**
	 * Override to specify the content of the composite.
	 */
	public function get_content() {
		return $this->content;
	}

	/**
	 * Returns the image URL if it exists.
	 */
	public function get_image() {

		/**
		 * Manually specified image.
		 */
		if(!empty($this->image)) {
			return $this->image;
		}

		/**
		 * Find image from the plugin's images directory.
		 */
		$image = get_plugin_dir() . 'images/' . $this->id . '.png';
		if(file_exists($image)) {
			return get_plugin_url() . 'images/' . $this->id . '.png';
		}

	}

	/**
	 * Returns the modal tab the composite belongs to.
	 */
	public function get_tab() {

		$tabs = include plugin_dir_path(dirname(__FILE__)) . 'includes/modal-tabs-composite.php';

		foreach($tabs as $name => $composite) {
			if(in_array($this->id, $composite)) {
				return $name;
			}
		}

		return 'front-page';

	}

	/**
	 * Renders the admin UI for inserting this composite.
	 * Can be overridden when custom UI is necessary.
	 */
	public function render_ui() {
		include plugin_dir_path(dirname(__FILE__)) . 'admin/partials/chap-shortcodes-ui-composite.php';
	}

}

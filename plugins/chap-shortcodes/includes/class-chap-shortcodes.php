<?php

namespace Chap\Shortcodes;
use Chap\Helpers;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       http://chap.website
 * @since      1.0.0
 *
 * @package    Chap_Shortcodes
 * @subpackage Chap_Shortcodes/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @version    1.0.4
 * @package    Chap_Shortcodes
 * @subpackage Chap_Shortcodes/includes
 * @author     websevendev <websevendev@gmail.com>
 */
class Chap_Shortcodes {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Chap_Shortcodes_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $chap_shortcodes    The string used to uniquely identify this plugin.
	 */
	protected $chap_shortcodes;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Contains settings.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var array
	 */
	protected $settings;

	/**
	 * Contains all loaded shortcodes.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var array
	 */
	protected $shortcodes;

	/**
	 * Contains all composites.
	 *
	 * @since    1.0.0
	 * @access   public
	 * @var array
	 */
	public $composites;

	/**
	 * True if settings are already loaded.
	 *
	 * @var boolean
	 */
	protected $composites_loaded = false;

	/**
	 * True if settings are already loaded.
	 *
	 * @var boolean
	 */
	protected $settings_loaded = false;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {

		$this->chap_shortcodes = 'chap-shortcodes';
		$this->version = '1.0.0';

		/**
		 * Collect AMP styles and scripts to be loaded.
		 *
		 * @global $csc
		 * @var array
		 */
		$GLOBALS['csc']['amp-styles'] = [];
		$GLOBALS['csc']['amp-scripts'] = [];

		/**
		 * Track slider context.
		 */
		$GLOBALS['csc']['slider'] = false;

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Chap_Shortcodes_Loader. Orchestrates the hooks of the plugin.
	 * - Chap_Shortcodes_i18n. Defines internationalization functionality.
	 * - Chap_Shortcodes_Admin. Defines all hooks for the admin area.
	 * - Chap_Shortcodes_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		$path = plugin_dir_path(dirname(__FILE__));

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once $path . 'includes/class-chap-shortcodes-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once $path . 'includes/class-chap-shortcodes-i18n.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once $path . 'admin/class-chap-shortcodes-admin.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once $path . 'public/class-chap-shortcodes-public.php';

		/**
		 * Include helper functions.
		 */
		require_once $path . 'includes/helpers.php';

		/**
		 * Include the Parser class.
		 */
		require_once $path . 'includes/class-parser.php';

		/**
		 * Include the Chap_Shortcode class.
		 */
		require_once $path . 'includes/class-chap-shortcode.php';

		/**
		 * Include the Chap_Composite class.
		 */
		require_once $path . 'includes/class-chap-composite.php';

		/**
		 * Initialize loader.
		 */
		$this->loader = new Chap_Shortcodes_Loader();

	}

	/**
	 * Register Chap Shortcodes settings.
	 *
	 * @since    1.0.0
	 * @access   public
	 */
	public function init_settings() {

		/**
		 * Shortcode prefix setting.
		 */
		register_setting('chap-shortcodes', 'csc-prefix', [
			'default' => 'chap-',
		]);

		/**
		 * Use shortcodes without prefix setting.
		 */
		register_setting('chap-shortcodes', 'csc-no-prefix', [
			'default' => 1,
		]);

		/**
		 * Semantic UI CSS CDN setting.
		 */
		register_setting('chap-shortcodes', 'csc-sui-css', [
			'default' => '//cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.6/semantic.min.css',
		]);

		/**
		 * Semantic UI JS CDN setting.
		 */
		register_setting('chap-shortcodes', 'csc-sui-js', [
			'default' => '//cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.6/semantic.min.js',
		]);

		/**
		 * Swiper CSS CDN setting.
		 */
		register_setting('chap-shortcodes', 'csc-swiper-css', [
			'default' => '//cdnjs.cloudflare.com/ajax/libs/Swiper/3.4.2/css/swiper.min.css',
		]);

		/**
		 * Swiper JS CDN setting.
		 */
		register_setting('chap-shortcodes', 'csc-swiper-js', [
			'default' => '//cdnjs.cloudflare.com/ajax/libs/Swiper/3.4.2/js/swiper.min.js',
		]);

		/**
		 * Shortcodes in widgets setting.
		 */
		register_setting('chap-shortcodes', 'csc-shortcodes-in-widgets', ['default' => 1]);

		/**
		 * Shortcodes in menus setting.
		 */
		register_setting('chap-shortcodes', 'csc-shortcodes-in-menus', ['default' => 1]);

		/**
		 * Shortcodes in comments setting.
		 */
		register_setting('chap-shortcodes', 'csc-shortcodes-in-comments', ['default' => 0]);

		/**
		 * Shortcodes in excerpts setting.
		 */
		register_setting('chap-shortcodes', 'csc-shortcodes-in-excerpts', ['default' => 0]);

		/**
		 * Shortcodes in author bio.
		 */
		register_setting('chap-shortcodes', 'csc-shortcodes-in-bio', ['default' => 1]);

		/**
		 * Shortcodes in Contact Form 7 setting.
		 */
		register_setting('chap-shortcodes', 'csc-shortcodes-in-cf7', ['default' => 0]);

		/**
		 * Option to lazy load [image] shortcode images by default.
		 */
		register_setting('chap-shortcodes', 'csc-lazy-loading', ['default' => 0]);

		/**
		 * Social media links setting.
		 */
		register_setting('chap-social', 'csc-social', []);

	}

	/**
	 * Load Chap Shortcodes settings.
	 *
	 * @since    1.0.0
	 * @access   public
	 */
	public function load_settings() {

		if($this->settings_loaded) {
			return;
		}

		$this->settings = [];
		$this->settings['prefix'] = get_option('csc-prefix');
		$this->settings['no-prefix'] = get_option('csc-no-prefix', 1);

		if(get_option('csc-shortcodes-in-widgets', true)) {
			/**
			 * Enable shortcodes in widgets.
			 */
			add_filter('widget_title', 'do_shortcode');
			add_filter('widget_text', 'do_shortcode');
		}

		if(get_option('csc-shortcodes-in-menus', true)) {
			/**
			 * Enable shortcodes in menus.
			 */
			add_filter('nav_menu_item_title', 'do_shortcode');
		}

		if(get_option('csc-shortcodes-in-comments', true)) {
			/**
			 * Enable shortcodes in comments.
			 */
			add_filter('comment_text', 'do_shortcode');
		}

		if(get_option('csc-shortcodes-in-excerpts', true)) {
			/**
			 * Enable shortcodes in excerpts.
			 */
			add_filter('the_excerpt', 'do_shortcode');
			add_filter('get_the_excerpt', 'do_shortcode');
		}

		if(get_option('csc-shortcodes-in-bio', true)) {
			/**
			 * Enable shortcodes in author bio.
			 */
			add_filter('chap_author_bio', 'do_shortcode');
		}

		$this->settings_loaded = true;

	}

	/**
	 * Load shortcodes.
	 *
	 * @since    1.0.0
	 * @access   public
	 */
	public function load_shortcodes() {

		$this->shortcodes = [];

		foreach(glob(plugin_dir_path(dirname(__FILE__)) . 'includes/shortcodes/shortcode-*.php') as $shortcode) {

			require_once $shortcode;

			$id = str_replace(['shortcode-', '.php'], '', basename($shortcode));
			$class_name = __NAMESPACE__ . '\\Chap_Shortcode_' . ucfirst($id);

			$this->shortcodes[$id] = new $class_name($id, $this->settings);

		}

	}

	/**
	 * Order the shortcodes, so they will be displayed in
	 * a logical order in the admin 'Insert shortcode' modal.
	 *
	 * @since    1.0.0
	 * @access   public
	 */
	public function sort_shortcodes() {

		$order = include plugin_dir_path(dirname(__FILE__)) . 'includes/shortcodes-order.php';

		$sort = function($a, $b) use ($order) {
			return (array_search($a, $order) > array_search($b, $order));
		};

		uksort($this->shortcodes, $sort);

	}

	/**
	 * Load composites.
	 *
	 * @since    1.0.0
	 * @access   public
	 */
	public function load_composites() {
		/**
		 * Don't need to load composites on the front end.
		 */
		if(!is_admin()) {
			return;
		}

		/**
		 * Composites already loaded.
		 */
		if($this->composites_loaded) {
			return;
		}

		$composite_files = glob(plugin_dir_path(dirname(__FILE__)) . 'includes/composites/composite-*.php');

		/**
		 * Sort composite-name.php to be in front of composite-name-1.php
		 */
		$composite_files = array_map(function($val) {
			return str_replace('.php', '-0.php', $val);
		}, $composite_files);
		natsort($composite_files);
		$composite_files = array_map(function($val) {
			return str_replace('-0.php', '.php', $val);
		}, $composite_files);

		/**
		 * Load composite objects.
		 */
		$this->composites = [];
		foreach($composite_files as $composite) {
			require_once $composite;

			$id = str_replace(['composite-', '.php'], '', basename($composite));
			$class_name = __NAMESPACE__ . '\\Chap_Composite_' . str_replace('-', '_', $id);

			$this->composites[] = new $class_name($id, $this->settings);
		}

		$this->composites_loaded = count($this->composites);
	}

	/**
	 * Determine if current theme is Chap.
	 *
	 * @since  1.0.2
	 * @return boolean
	 */
	public static function current_theme_is_chap() {
		$current_theme = wp_get_theme();
		return in_array(true, [
			$current_theme->get('Name') === 'Chap',
			$current_theme->get('Name') === 'Chap Child',
			$current_theme->get('TextDomain') === 'chap',
		]);
	}

	/**
	 * Load Semantic UI files.
	 *
	 * @since    1.0.0
	 * @access   public
	 */
	public function load_semantic_ui() {
		/**
		 * Don't load if current theme is Chap.
		 */
		if(self::current_theme_is_chap()) {
			return;
		}

		$sui_css = get_option('csc-sui-css');
		if($sui_css !== false && !empty($sui_css)) {
			wp_enqueue_style('chap/sui', $sui_css, [], false, 'all');
		}

		$sui_js = get_option('csc-sui-js');
		if($sui_js !== false && !empty($sui_js)) {
			wp_enqueue_script('chap/js', $sui_js, ['jquery'], false, true);
		}
	}

	/**
	 * Load Swiper slider.
	 *
	 * @since    1.0.4
	 * @access   public
	 */
	public function load_swiper_assets() {
		/**
		 * Don't load if current theme is Chap.
		 */
		if(self::current_theme_is_chap()) {
			// $GLOBALS['csc']['has_slides'] = true; // Load from theme.
			return;
		}

		$swiper_css = get_option('csc-swiper-css');
		if($swiper_css !== false && !empty($swiper_css)) {
			wp_enqueue_style('chap/swiper', $swiper_css, [], false, 'all');
		}

		$swiper_js = get_option('csc-swiper-js');
		if($swiper_js !== false && !empty($swiper_js)) {
			wp_enqueue_script('chap/swiper/js', $swiper_js, ['jquery'], false, true);
		}
	}

	/**
	 * Load AMP styles.
	 *
	 * @since    1.0.2
	 * @access   public
	 */
	public function load_amp_styles() {
		if(!Helpers\is_amp()) {
			return;
		}
		if(!is_array($GLOBALS['csc']['amp-styles'])) {
			return;
		}
		foreach($GLOBALS['csc']['amp-styles'] as $id => $load) {
			if($load) {
				$amp_style = get_plugin_dir() . 'public/css/amp/' . $id . '.php';
				if(file_exists($amp_style)) {
					include $amp_style;
				}
			}
		}
	}

	/**
	 * Load AMP scripts.
	 *
	 * @since    1.0.2
	 * @access   public
	 */
	public function load_amp_scripts($scripts) {
		if(!Helpers\is_amp()) {
			return $scripts;
		}
		if(!is_array($GLOBALS['csc']['amp-scripts'])) {
			return $scripts;
		}
		foreach($GLOBALS['csc']['amp-scripts'] as $script => $version) {
			$script_src = 'https://cdn.ampproject.org/v0/' . esc_attr($script) . '-' . esc_attr($version) . '.js';
			$scripts[$script] = $script_src;
		}
		return $scripts;
	}

	/**
	 * Remove empty paragraphs from shortcodes.
	 *
	 * @see      https://gist.github.com/bitfade/4555047
	 * @since    1.0.0
	 * @access   public
	 */
	public function the_content_filter($content) {
		$loaded_shortcodes = [];

		foreach($this->shortcodes as $shortcode) {
			$loaded_shortcodes = array_merge($shortcode->ids, $loaded_shortcodes);
		}

		$block = join('|', $loaded_shortcodes);
		$rep = preg_replace("/(<p>)?\[($block)(\s[^\]]+)?\](<\/p>|<br \/>)?/", "[$2$3]", $content);
		$rep = preg_replace("/(<p>)?\[\/($block)](<\/p>|<br \/>)?/", "[/$2]", $rep);

		return $rep;
	}

	/**
	 * Render the Media buttons above WordPress editor.
	 *
	 * @since    1.0.1
	 * @access   public
	 */
	public function chap_shortcodes_media_buttons($editor) {
		include plugin_dir_path(dirname(__FILE__)) . 'admin/partials/chap-shortcodes-media-buttons.php';
	}

	/**
	 * Return the "Insert shortcode" button and modal.
	 *
	 * @since    1.0.0
	 * @access   public
	 */
	public function chap_shortcodes_insert_shortcode() {
		include_once plugin_dir_path(dirname(__FILE__)) . 'admin/partials/chap-shortcodes-insert-shortcode.php';
	}

	/**
	 * Return the "Insert composite" button and modal.
	 *
	 * @since    1.0.0
	 * @access   public
	 */
	public function chap_shortcodes_insert_composite() {
		include_once plugin_dir_path(dirname(__FILE__)) . 'admin/partials/chap-shortcodes-insert-composite.php';
	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Chap_Shortcodes_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new Chap_Shortcodes_i18n();

		$this->loader->add_action('plugins_loaded', $plugin_i18n, 'load_plugin_textdomain');

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new Chap_Shortcodes_Admin($this->get_chap_shortcodes(), $this->get_version());

		$this->loader->add_action('admin_enqueue_scripts', $plugin_admin, 'enqueue_styles');
		$this->loader->add_action('admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts');

		$this->loader->add_action('admin_menu', $plugin_admin, 'chap_shortcodes_settings_menu');
		$this->loader->add_action('media_buttons', $this, 'sort_shortcodes', 10);
		$this->loader->add_action('media_buttons', $this, 'chap_shortcodes_media_buttons', 15);
		$this->loader->add_action('media_buttons', $this, 'chap_shortcodes_insert_shortcode', 20);
		$this->loader->add_action('media_buttons', $this, 'chap_shortcodes_insert_composite', 20);

		/**
		 * Disable AMP customizer for Chap theme.
		 *
		 * Chap theme uses it's own theme options to
		 * customize AMP output, however themes are loaded
		 * after plugins, so there is no way to disable
		 * customizer from the theme itself. Doing it here
		 * since this plugin is bundled with the theme.
		 *
		 * @since 1.0.2
		 */
		if(self::current_theme_is_chap()) {
			add_filter('amp_customizer_is_enabled', '__return_false');
		}

	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

		$plugin_public = new Chap_Shortcodes_Public($this->get_chap_shortcodes(), $this->get_version());

		$this->loader->add_action('wp_enqueue_scripts', $plugin_public, 'enqueue_styles');
		$this->loader->add_action('wp_enqueue_scripts', $plugin_public, 'enqueue_scripts');
		$this->loader->add_action('wp_enqueue_scripts', $this, 'load_semantic_ui');
		$this->loader->add_action('wp_enqueue_scripts', $this, 'load_swiper_assets');
		$this->loader->add_action('amp_post_template_css', $this, 'load_amp_styles');
		$this->loader->add_action('chap_amp_component_scripts', $this, 'load_amp_scripts');

		$this->loader->add_action('init', $this, 'init_settings', 10);
		$this->loader->add_action('init', $this, 'load_settings', 20);
		$this->loader->add_action('init', $this, 'load_shortcodes', 30);
		$this->loader->add_action('init', $this, 'load_composites', 40);

		$this->loader->add_filter('the_content', $this, 'the_content_filter');

		$this->loader->add_filter('wpcf7_form_elements', $this, 'csc_wpcf7_form_elements');

	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_chap_shortcodes() {
		return $this->chap_shortcodes;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Chap_Shortcodes_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

	/**
	 * Access the composites externally.
	 *
	 * @since     1.0.0
	 * @return    array    All the loaded composites.
	 */
	public function get_chap_shortcodes_composites() {
		$this->load_settings();
		$this->load_composites();
		return $this->composites;
	}

	/**
	 * Access the Social Media list externally.
	 *
	 * @since     1.0.0
	 * @return    array    Social Medias used by [social] shortcode.
	 */
	public function get_chap_shortcodes_social() {
		return include plugin_dir_path(dirname(__FILE__)) . 'includes/social.php';
	}

	/**
	 * Access the array of loaded shortcodes externally.
	 *
	 * @since     1.0.0
	 * @return    array    Loaded shortcodes.
	 */
	public function get_loaded_shortcodes() {
		return $this->shortcodes;
	}

	/**
	 * Allow shortcodes in Contact Form 7.
	 *
	 * @since     1.0.3
	 * @return    $form    CF7 form content.
	 */
	public function csc_wpcf7_form_elements($form) {
		if(get_option('csc-shortcodes-in-cf7')) {
			$form = self::the_content_filter($form); // remove wpautop
			$form = do_shortcode($form); // parse shortcodes
		}
		return $form;
	}

}

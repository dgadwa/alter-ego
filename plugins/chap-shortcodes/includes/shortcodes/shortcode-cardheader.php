<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Shortcode_Cardheader extends Chap_Shortcode {

	public function __construct($id, $options) {

		$this->default_args = [
			'subheader' => '',
		];

		$this->description = esc_html__('Card header.', 'chap-shortcodes');

		$this->arg_docs = [
			'classes' => [
				'alignment' => ['center aligned', 'right aligned'],
			],
			'subheader' => esc_html__('Text under the header.', 'chap-shortcodes'),
		];

		$this->presets[esc_html__('Card header with a subheader', 'chap-shortcodes')] = [
			'subheader' => 'Subheader',
		];

		parent::__construct($id, $options);

	}

	public function render_shortcode() {

		$subheader = $this->has_arg('subheader') ? do_shortcode('[csc-subheader]' . $this->args['subheader'] . '[/csc-subheader]') : '';

		$this->add_class('header');
		$this->content = '<div class="ui massive header">' . $this->content . $subheader . '</div>';

		return '<div class="content">' . $this->html_tag('div') . '</div>';

	}

}

<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Shortcode_Menu extends Chap_Shortcode {

	public function __construct($id, $options) {

		$this->description = esc_html__('Menu. Use [item] shortcode to add menu items.', 'chap-shortcodes');

		$this->arg_docs = [
			'classes' => [
				'type' => ['primary', 'secondary', 'pointing', 'tabular', 'text', 'pagination'],
				'axis' => ['horizontal', 'vertical'],
				'count' => ['two item', 'three item', '...'],
				'size' => ['mini', 'tiny', 'small', 'medium', 'large', 'big', 'huge', 'massive'],
				'float' => ['left floated', 'right floated'],
				'icon' => ['icon', 'labeled icon'],
				'mobile' => ['stackable'],
				'other' => ['compact', 'inverted', 'evenly divided', 'fluid', 'borderless', 'fixed'],
			],
		];

		parent::__construct($id, $options);

	}

	public function render_shortcode() {

		$this->add_script('menu-dropdown');

		if($this->has_class('tabular')) {
			$this->add_amp_style('menu-tabular');
		}

		if($this->has_class('secondary')) {
			$this->add_amp_style('menu-secondary');
		}

		if($this->has_class('text')) {
			$this->add_amp_style('menu-text');
		}

		if($this->has_class('icon')) {
			$this->add_amp_style('menu-icon');
		}

		if($this->has_class('compact')) {
			$this->add_amp_style('menu-compact');
		}

		if($this->has_class('item')) {
			$this->add_amp_style('menu-evenly-divided');
		}

		if($this->has_class('attached')) {
			$this->add_amp_style('menu-attached');
		}

		$this->prepend_class('ui');
		$this->add_classes(['csc-menu', 'menu']);

		if(!isset($this->atts['role'])) {
			$this->atts['role'] = $this->has_class('vertical') ? 'menu' : 'menubar';
		}

		return $this->html_tag('div');

	}

}

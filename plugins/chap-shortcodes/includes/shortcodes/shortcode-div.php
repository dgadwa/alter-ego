<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Shortcode_Div extends Chap_Shortcode {

	public function __construct($id, $options) {

		$this->description = esc_html__('HTML &lt;div&gt; tag as a shortcode. Useful because inside shortcodes WP autop is disabled. Can be used as a substitute for any UI shortcode when it\'s needed to nest the same shortcode inside itself.', 'chap-shortcodes');

		$this->presets[esc_html__('With CSS classes', 'chap-shortcodes')] = [
			'class1',
			'class2',
		];

		$this->presets[esc_html__('Container shortcode alternative', 'chap-shortcodes')] = [
			'ui',
			'container',
		];

		$this->presets[esc_html__('List shortcode alternative', 'chap-shortcodes')] = [
			'ui',
			'list',
		];

		parent::__construct($id, $options);

	}

	public function render_shortcode() {

		return $this->html_tag('div');

	}

}

<?php

namespace Chap\Shortcodes;
use Chap\Helpers;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Shortcode_Statistic extends Chap_Shortcode {

	public function __construct($id, $options) {

		$this->default_args = [
			'label' => '',
			'pos' => 'after',
		];

		$this->description = esc_html__('A statistic emphasizes the current value of an attribute.', 'chap-shortcodes');

		$this->arg_docs = [
			'classes' => [
				'color' => ['red', 'green', 'blue', '...'],
				'size' => ['mini', 'tiny', 'small', 'large', 'huge'],
				'orientation' => ['horizontal'],
				'other' => ['inverted'],
			],
			'label' => esc_html__('Name of the attribute.', 'chap-shortcodes'),
			'pos' => esc_html__('Label position [before|after] (default: after).', 'chap-shortcodes'),
			'icon' => esc_html__('Name of the icon to use.', 'chap-shortcodes'),
			'image' => esc_html__('URL of the image to use.', 'chap-shortcodes'),
		];

		$this->presets[esc_html__('View count', 'chap-shortcodes')] = [
			'title' => 'Views',
		];

		parent::__construct($id, $options);

	}

	public function render_shortcode() {

		$this->prepend_class('ui');

		if($this->has_arg('icon')) {
			$icon = do_shortcode('[csc-icon ' . $this->args['icon'] . ']');
			$this->content = $icon . $this->content;
		}

		if($this->has_arg('image') && !Helpers\is_amp()) {
			$image = '<img class="ui circular inline image" src="' . $this->args['image'] . '" alt="' . $this->args['label'] . '" />';
			$this->content = $image . $this->content;
		}

		$value_classes = $this->has_class('text') ? 'text value' : 'value';
		$this->content = '<div class="' . $value_classes . '">' . $this->content . '</div>';

		if($this->has_arg('label')) {
			if($this->args['pos'] === 'before') {
				$this->content = '<div class="label">' . $this->args['label'] . '</div>'. $this->content;
			} else {
				$this->content .= '<div class="label">' . $this->args['label'] . '</div>';
			}
		}

		$this->add_class('statistic');

		return $this->html_tag('div');

	}

}

<?php

namespace Chap\Shortcodes;
use Chap\Helpers;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Shortcode_Code extends Chap_Shortcode {

	public function __construct($id, $options) {

		$this->default_args = [
			'title' => '',
			'language' => 'wiki',
			'show_language' => 'true',
		];

		$this->description = esc_html__('Highlighted code block.', 'chap-shortcodes');

		$this->arg_docs = [
			'language' => esc_html__('Language [html|css|scss|less|php|js|json|sql] (default: WP shortcode)', 'chap-shortcodes'),
			'title' => esc_html__('Title', 'chap-shortcodes'),
			'show_language' => esc_html__('Show language name in the title (default: true)', 'chap-shortcodes'),
		];

		$this->presets['HTML'] = [
			'language' => 'html',
		];

		$this->presets['PHP'] = [
			'language' => 'php',
		];

		parent::__construct($id, $options);

	}

	public function render_shortcode() {

		$this->add_style('prism');
		$this->add_script('prism');
		$this->add_style('code');
		$this->add_script('code');

		$this->add_amp_style('label');
		$this->add_amp_style('segment');
		$this->add_amp_style('demo');

		$this->raw_content = str_replace('<br />', '', $this->raw_content);
		$this->raw_content = esc_html($this->raw_content);

		/**
		 * Mark highlights.
		 */
		while(strpos($this->raw_content, '[highlight]') !== false) {
			$replace = Helpers\is_amp() ? [] : ['(((chap-mark-start)))', '(((chap-mark-end)))'];
			$this->raw_content = str_replace(
				['[highlight]', '[/highlight]'],
				$replace,
				$this->raw_content
			);
		}

		/**
		 * Small hack to use HTML syntax highlighting for shortcodes.
		 * Shortcodes' [ and ] are turned into < and >, then highlighted
		 * and then changed back to original, while keeping the highlights.
		 */
		if($this->args['language'] === 'wiki' && !Helpers\is_amp() && !$this->has_class('demo')) {
			$this->raw_content = str_replace(['&lt;', '&gt;'], ['{lt}', '{gt}'], $this->raw_content);
			$this->raw_content = str_replace(['[', ']'], ['&lt;', '&gt;'], $this->raw_content);
			$this->raw_content = str_replace(['&#91;', '&#93;'], ['&lt;', '&gt;'], $this->raw_content);
		}

		/**
		 * Attached class.
		 */
		$attached = $this->has_arg('title') ? ' bottom attached' : ($this->has_class('demo') ? ' ' . $this->args['classes'] : '');

		/**
		 * Language label.
		 */
		$label_text = $this->args['language'] == 'wiki' ? esc_html__('Shortcode', 'chap-shortcodes') : strtoupper($this->args['language']);
		$language_label = $this->args['show_language'] !== 'false' ? '<div class="detail">' . $label_text . '</div>' : '';

		ob_start();
		if($this->has_arg('title')):
		?>
		<div class="ui top attached clearing demo segment">
			<div class="ui top attached label">
				<?php echo esc_html($this->args['title']) . wp_kses_post($language_label); ?>
			</div>
		</div>
		<?php endif; ?>
		<div class="ui instructive<?php echo esc_attr($attached); ?> code paddingless segment">
			<pre class="language-<?php echo esc_attr($this->args['language']); ?>"><code class="language-<?php echo esc_attr($this->args['language']); ?>"><?php echo trim($this->raw_content); ?></code></pre>
		</div>
		<?php

		return ob_get_clean();

	}

}

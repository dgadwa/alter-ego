<?php

namespace Chap\Shortcodes;
use Chap\Helpers;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Shortcode_Step extends Chap_Shortcode {

	public function __construct($id, $options) {

		$this->default_args = [
			'title' => '',
		];

		$this->description = esc_html__('A statistic emphasizes the current value of an attribute.', 'chap-shortcodes');

		$this->arg_docs = [
			'classes' => [
				'state' => ['active', 'completed', 'disabled'],
			],
			'title' => esc_html__('Step title.', 'chap-shortcodes'),
			'icon' => esc_html__('Name of the icon to use.', 'chap-shortcodes'),
		];

		// $this->presets[esc_html__('Specified count')] = [
		// 	'three',
		// ];

		parent::__construct($id, $options);

	}

	public function render_shortcode() {

		$this->add_class('step');

		$this->content = '<div class="description">' . $this->content . '</div>';
		if($this->has_arg('title')) {
			$this->content = '<div class="title">' . $this->args['title'] . '</div>' . $this->content;
		}
		$this->content = '<div class="content">' . $this->content . '</div>';

		if($this->has_arg('icon')) {
			if(Helpers\is_amp() && $this->has_class('completed')) {
				$icon = do_shortcode('[csc-icon green check]');
			} else {
				$icon = do_shortcode('[csc-icon ' . $this->args['icon'] . ']');
			}
			$this->content = $icon . $this->content;
		}

		return $this->html_tag('div');

	}

}

<?php

namespace Chap\Shortcodes;
use Chap\Helpers;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Shortcode_Checkbox extends Chap_Shortcode {

	public function __construct($id, $options) {

		$this->default_args = [
			'type' => 'checkbox',
		];

		$this->description = '';

		$this->arg_docs = [
			'id' => esc_html__('ID of the checkbox.', 'chap-shortcodes'),
			'name' => esc_html__('Name of the checkbox.', 'chap-shortcodes'),
			'label' => esc_html__('Label text.', 'chap-shortcodes'),
			'value' => esc_html__('Checkbox value (for radio checkboxes).', 'chap-shortcodes'),
			'classes' => [
				'state' => ['checked', 'disabled'],
			],
			'required' => esc_html__('Set to "true" to mark as required field.', 'chap-shortcodes'),
		];

		// $this->presets['Preset'] = [
		// 	'class',
		// ];

		parent::__construct($id, $options);

	}

	/**
	 * Create a separate HTML checkbox input and remove atts from main container.
	 */
	protected function checkbox() {

		$atts = [
			'type' => $this->atts['type'],
			'tabindex' => '0',
			'classes' => ['hidden'],
		];
		unset($this->atts['type']);

		if($this->has_class('checked')) {
			$atts['checked'] = 'checked';
		}

		if($this->has_arg('id')) {
			$atts['id'] = $this->atts['id'];
			unset($this->atts['id']);
		}

		if($this->has_arg('name')) {
			$atts['name'] = $this->atts['name'];
			unset($this->atts['name']);
		}

		if($this->has_class('required') || $this->has_arg('required')) {
			$atts['required'] = '';
		}

		return Helpers\html_tag('input', $atts, true);

	}

	public function render_shortcode() {

		$this->add_script('checkbox');

		if($this->has_class('radio')) {
			$this->args['type'] = 'radio';
			$this->atts['type'] = 'radio';
		}

		$this->content = $this->checkbox();

		if($this->has_arg('label')) {
			$this->content .= '<label>' . $this->args['label'] . '</label>';
		}

		$this->prepend_class('ui');
		$this->add_class('checkbox');

		return $this->html_tag('div');

	}

}

<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Shortcode_Divider extends Chap_Shortcode {

	public function __construct($id, $options) {

		$this->has_content = false;

		$this->description = '';

		$this->arg_docs = [
			'classes' => [
				'variation' => ['hidden', 'segment', 'clearing', 'horizontal'], // vertical broken
			],
		];

		$this->presets[esc_html__('Hidden divider', 'chap-shortcodes')] = [
			'hidden',
		];

		$this->presets[esc_html__('Section divider', 'chap-shortcodes')] = [
			'section',
		];

		parent::__construct($id, $options);

	}

	public function render_shortcode() {

		$this->prepend_class('ui');
		$this->add_class('divider');

		return $this->html_tag('div');

	}

}

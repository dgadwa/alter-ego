<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Shortcode_Container extends Chap_Shortcode {

	public function __construct($id, $options) {

		$this->description = esc_html__('Contains page elements to a reasonable maximum width.', 'chap-shortcodes');

		$this->arg_docs = [
			'classes' => [
				'type' => ['text', 'fluid'],
				'alignment' => ['left aligned', 'right aligned', 'center aligned', 'justified'],
			],
		];

		$this->presets[esc_html__('Text container', 'chap-shortcodes')] = [
			'text',
		];

		$this->presets[esc_html__('Full width container', 'chap-shortcodes')] = [
			'fluid',
		];

		parent::__construct($id, $options);

	}

	public function render_shortcode() {

		$this->prepend_class('ui');
		$this->add_class('container');

		return $this->html_tag('div');

	}

}

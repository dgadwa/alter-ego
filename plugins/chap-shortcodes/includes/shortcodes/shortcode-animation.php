<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Shortcode_Animation extends Chap_Shortcode {

	public function __construct($id, $options) {

		$this->default_args = [
			'anim' => 'fade',
		];

		$this->arg_docs = [
			'anim' => esc_html__('Animation.', 'chap-shortcodes'),
			'anim-offset' => esc_html__('Change offset to trigger animations sooner or later (px).', 'chap-shortcodes'),
			'anim-duration' => esc_html__('Duration of animation (ms). Available options: 50-3000 with a step of 50.', 'chap-shortcodes'),
			'anim-easing' => esc_html__('Choose timing function to ease elements in different ways.', 'chap-shortcodes'),
			'anim-delay' => esc_html__('Delay animation (ms).', 'chap-shortcodes'),
			'anim-anchor' => esc_html__('Anchor element, whose offset will be counted to trigger animation instead of actual elements offset.', 'chap-shortcodes'),
			'anim-anchor-placement' => esc_html__('Anchor placement - which one position of element on the screen should trigger animation.', 'chap-shortcodes'),
			'anim-once' => esc_html__('Choose wheter animation should fire once, or every time you scroll up/down to element.', 'chap-shortcodes'),
			// 'options' => [
			// 	'&nbsp;anim' => ['fade', 'fade-up', 'fade-down', 'fade-left', 'fade-right', 'fade-up-right', 'fade-up-left', 'fade-down-right', 'fade-down-left', 'flip-up', 'flip-down', 'flip-left', 'flip-right', 'slide-up', 'slide-down', 'slide-left', 'slide-right', 'zoom-in', 'zoom-in-up', 'zoom-in-down', 'zoom-in-left', 'zoom-in-right', 'zoom-out', 'zoom-out-up', 'zoom-out-down', 'zoom-out-left', 'zoom-out-right'],
			// 	'&nbsp;anim-duration' => ['50', '100', '150', '...', '2900', '2950', '3000'],
			// 	'&nbsp;anim-easing' => ['linear', 'ease', 'ease-in', 'ease-out', 'ease-in-out', 'ease-in-back', 'ease-out-back', 'ease-in-out-back', 'ease-in-sine', 'ease-out-sine', 'ease-in-out-sine', 'ease-in-quad', 'ease-out-quad', 'ease-in-out-quad', 'ease-in-cubic', 'ease-out-cubic', 'ease-in-out-cubic', 'ease-in-quart', 'ease-out-quart', 'ease-in-out-quart', ],
			// 	'&nbsp;anim-delay' => ['50', '100', '150', '...', '2900', '2950', '3000'],
			// 	'&nbsp;anim-anchor' => ['#selector'],
			// 	'&nbsp;anim-anchor-placement' => ['top-bottom', 'top-center', 'top-top', 'center-bottom', 'center-center', 'center-top', 'bottom-bottom', 'bottom-center', 'bottom-top'],
			// 	'&nbsp;anim-once' => ['true (default)', 'false'],
			// ],
		];

		$this->description = esc_html__('Animated HTML &lt;div&gt; tag.', 'chap-shortcodes');

		// $this->presets[esc_html__('With CSS classes')] = [
		// 	'class1',
		// 	'class2',
		// ];

		parent::__construct($id, $options);

	}

	public function render_shortcode() {

		return $this->html_tag('div');

	}

}

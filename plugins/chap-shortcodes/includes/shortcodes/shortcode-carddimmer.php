<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Shortcode_Carddimmer extends Chap_Shortcode {

	public function __construct($id, $options) {

		$this->default_args = [
			'dimmer_classes' => '',
			'image' => '//placehold.it/150x150',
			'alt' => 'Dimmer image',
		];

		$this->description = esc_html__('Image in a card that shows content on hover.', 'chap-shortcodes');

		$this->arg_docs = [
			'image' => esc_html__('The url to the image.', 'chap-shortcodes'),
			'content' => esc_html__('Content to show on image hover.', 'chap-shortcodes'),
			'alt' => esc_html__('Image alt.', 'chap-shortcodes'),
		];

		parent::__construct($id, $options);

	}

	public function render_shortcode() {

		return do_shortcode('[csc-dimmer ' . $this->args['classes'] . ' image="' . $this->args['image'] . '" alt="' . $this->args['alt'] . '" dimmer="' . $this->args['dimmer_classes'] . '"]' . $this->content . '[/csc-dimmer]');

	}

}

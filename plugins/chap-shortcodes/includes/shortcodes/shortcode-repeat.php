<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Shortcode_Repeat extends Chap_Shortcode {

	public function __construct($id, $options) {

		$this->default_args = [
			'data' => '',
			'separator' => ',',
			'divider' => '|',
			'variable' => '%',
		];

		$this->description = esc_html__('Define data and repeat the contents.', 'chap-shortcodes');

		$this->arg_docs = [
			'data' => 'value1, value2, value3',
			'separator' => esc_html__('Custom separator symbol (default: ",").', 'chap-shortcodes'),
			'divider' => esc_html__('Custom divider symbol (default: "|").', 'chap-shortcodes'),
			'variable' => esc_html__('Custom variable enclosing symbols (default: "%").', 'chap-shortcodes'),
		];

		parent::__construct($id, $options);

	}

	public function render_shortcode() {

		$original_content = $this->raw_content;
		$this->content = '';

		$data = trim($this->args['data']);
		$data = str_replace(['<br>', '<br />', '<br/>'], '', $data);
		$data = preg_replace("/\r|\n/", '', $data);
		$data = rtrim($data, $this->args['separator']);
		$data = explode($this->args['separator'], $data);
		$data = array_map('trim', $data);

		foreach($data as $row) {
			$content = $original_content;
			$vars = explode($this->args['divider'], $row);
			$vars = array_map('trim', $vars);
			foreach($vars as $index => $var) {
				$content = str_replace($this->args['variable'] . $index . $this->args['variable'], $var, $content);
			}
			$this->content .= do_shortcode($content);
		}

		return $this->content;

	}

}

<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Shortcode_Grid extends Chap_Shortcode {

	public function __construct($id, $options) {

		$this->description = esc_html__('Grid for containing rows and columns.', 'chap-shortcodes');

		$this->arg_docs = [
			'classes' => [
				'columns' => ['equal width', 'two column', 'three column', '...'],
				'type' => ['celled', 'internally celled', 'divided', 'padded'],
				'alignment' => ['centered'],
				'mobile' => ['stackable', 'doubling'],
				'other' => ['reversed'],
			],
		];

		// $this->presets['Preset'] = [
		// 	'class',
		// ];

		parent::__construct($id, $options);

	}

	public function render_shortcode() {

		$this->add_amp_style('row');
		$this->add_amp_style('column');

		/**
		 * Add default classes if none set.
		 */
		if(count($this->atts['classes']) === 0) {
			$this->atts['classes'] = [
				'equal',
				'width',
				'stackable',
			];
		}

		$this->prepend_class('ui');
		$this->add_class('grid');

		return $this->html_tag('div');

	}

}

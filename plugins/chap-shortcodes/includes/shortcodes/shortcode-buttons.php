<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Shortcode_Buttons extends Chap_Shortcode {

	public function __construct($id, $options) {

		$this->description = esc_html__('Container for multiple buttons.', 'chap-shortcodes');

		$this->arg_docs = [
			'classes' => [
				'grouping' => ['horizontal', 'vertical'],
				'emphasis' => ['primary', 'secondary', 'basic', 'positive', 'negative'],
				'size' => ['mini', 'tiny', 'small', 'medium', 'large', 'big', 'huge', 'massive'],
				'color' => ['red', 'green', 'blue', '...'],
				'float' => ['left floated', 'right floated'],
				'other' => ['compact', 'inverted', 'fluid', 'active', 'disabled'],
			],
		];

		parent::__construct($id, $options);

	}

	public function render_shortcode() {

		$this->prepend_class('ui');
		$this->add_class('buttons');

		return $this->html_tag('div');

	}

}

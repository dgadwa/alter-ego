<?php

namespace Chap\Shortcodes;
use Chap\Helpers;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Shortcode_Button extends Chap_Shortcode {

	public function __construct($id, $options) {

		$this->default_args = [
			'href' => '',
			'icon' => '',
			'hover' => '',
			'hover_icon' => '',
		];

		$this->arg_docs = [
			'classes' => [
				'emphasis' => ['primary', 'secondary', 'basic', 'positive', 'negative'],
				'size' => ['mini', 'tiny', 'small', 'medium', 'large', 'big', 'huge', 'massive'],
				'color' => ['red', 'green', 'blue', '...'],
				'social' => ['facebook', 'twitter', 'google plus', 'vk', 'linkedin', 'instagram', 'youtube'],
				'float' => ['left floated', 'right floated'],
				'icon' => ['icon', 'right icon', 'labeled icon', 'right labeled icon'],
				'other' => ['compact', 'inverted', 'circular', 'fluid', 'active', 'disabled'],
			],
			'href' => esc_html__('The href/url/link argument can be used to specify a link.', 'chap-shortcodes'),
			'icon' => esc_html__('Name of the icon to use.', 'chap-shortcodes'),
			'hover' => esc_html__('Text to use on hover.', 'chap-shortcodes'),
			'hover_icon' => esc_html__('Name of the icon to use on hover.', 'chap-shortcodes'),
		];

		$this->description = '';

		$this->presets[esc_html__('Large primary button', 'chap-shortcodes')] = [
			'large',
			'primary',
		];

		$this->presets[esc_html__('Small secondary button', 'chap-shortcodes')] = [
			'small',
			'secondary',
		];

		parent::__construct($id, $options);

	}

	public function render_shortcode() {

		/**
		 * Dropdown buttons not supported on AMP.
		 */
		if(Helpers\is_amp() && $this->has_class('dropdown')) {
			return;
		}

		/* Acessibility:
		 * 1. The div is not keyboard-focusable by default, so you need to add `tabindex="0"`.
		 * 2. By default, the div does not trigger click on Enter/Space, so you need to add a keyup handler to make it do so.
		 * 3. Screen readers won't report the div as a button because they can't identify widgets based on how they look, so
		 * add `role="button"` as per the ARIA spec (only on first cup of coffee now, so I'm not providing a link.)
		 */

		$tag = 'div';

		// if($this->has_arg('type')) {
		// 	$tag = 'button';
		// }

		if(!$this->has_arg('href')) {
			$tag = 'button';
		}

		$icon = $this->has_arg('icon') ? do_shortcode('[csc-icon ' . $this->args['icon'] . ']') : '';

		$this->prepend_class('ui');

		if($this->has_arg('hover') || $this->has_arg('hover_icon')) {

			$hover_icon = $this->has_arg('hover_icon') ? '<i class="' . $this->args['hover_icon'] . ' icon"></i>' : '';
			$visible_content = '<div class="visible content">' . $icon . $this->content . '</div>';
			$hidden_content = '<div class="hidden content">' . $hover_icon . $this->args['hover'] . '</div>';

			$this->add_classes(['animated', 'button']);
			$this->content = $visible_content . $hidden_content;

			return $this->html_tag($tag);

		}

		$this->add_class('button');
		$this->content = $icon . $this->content;

		return $this->html_tag($tag);

	}

}

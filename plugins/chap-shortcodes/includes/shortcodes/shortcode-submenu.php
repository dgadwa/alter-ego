<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Shortcode_Submenu extends Chap_Shortcode {

	public function __construct($id, $options) {

		$this->description = esc_html__('Submenu, used inside menu.', 'chap-shortcodes');

		parent::__construct($id, $options);

	}

	public function render_shortcode() {

		$this->add_class('menu');

		if(!isset($this->atts['role'])) {
			$this->atts['role'] = 'menu';
		}

		return $this->html_tag('div');

	}

}

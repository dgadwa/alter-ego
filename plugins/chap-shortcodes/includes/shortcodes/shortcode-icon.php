<?php

namespace Chap\Shortcodes;
use Chap\Helpers;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Shortcode_Icon extends Chap_Shortcode {

	protected static $fa = [];

	public function __construct($id, $options) {

		$this->has_content = false;

		$this->description = esc_html__('Display Semantic UI icons.', 'chap-shortcodes');

		$this->arg_docs = [
			'classes' => [
				'size' => ['mini', 'tiny', 'small', 'medium', 'large', 'big', 'huge', 'massive'],
				'color' => ['red', 'green', 'blue', '...'],
				'flip' => ['horizontally flipped', 'vertically flipped'],
				'rotation' => ['clockwise rotated', 'counterclockwise rotated'],
				'other' => ['circular', 'bordered', 'disabled'],
				'name' => ['icon name'],
			],
		];

		self::maybe_load_amp_variables();

		parent::__construct($id, $options);

	}

	/**
	 * Load the icon map only if AMP and not loaded yet (ie. direct call to render method).
	 */
	protected static function maybe_load_amp_variables() {

		if(!count(self::$fa) && Helpers\is_amp()) {
			/**
			 * Map SUI icon class to Font Awesome.
			 */
			self::$fa = apply_filters('csc_font_awesome_remap_for_amp', [
				'add-to-calendar' => 'calendar-plus-o',
				'alarm' => 'bell',
				'alarm-mute' => 'bell-slash',
				'browser' => 'list-alt',
				'checked-calendar' => 'calendar-check-o',
				'delete-calendar' => 'calendar-minus-o',
				'external-square' => 'external-link-square',
				'external' => 'external-link',
				'find' => 'binoculars',
				'hand-pointer' => 'hand-pointer-o',
				'hourglass-empty' => 'hourglass-o',
				'hourglass-full' => 'hourglass',
				'idea' => 'lightbulb-o',
				'lab' => 'flask',
				'mail' => 'envelope',
				'options' => 'sliders',
				'payment' => 'credit-card',
				'privacy' => 'key',
				'protect' => 'shield',
				'registe' => 'registered',
				'remove-from-calendar' => 'calendar-minus-o',
				'setting' => 'cog',
				'settings' => 'cogs',
				'shop' => 'shopping-cart',
				'text-telephone' => 'phone',
				'cart' => 'shopping-cart',
				'add-to-cart' => 'cart-plus',
				'add-user' => 'user-plus',
				'call' => 'phone',
				'talk' => 'commenting',
				'configure' => 'wrench',
				'erase' => 'eraser',
				'external-share' => 'share-square-o',
				'hide' => 'eye-slash',
				'in-cart' => 'cart-arrow-down',
				'pin' => 'thumb-tack',
				'remove-bookmark' => 'bookmark-o',
				'remove-user' => 'user-times',
				'theme' => 'tint',
				'translate' => 'language',
				'unhide' => 'eye',
				'wait' => 'clock-o',
				'wizard' => 'magic',
				'write' => 'pencil',
				'announcement' => 'bullhorn',
				'birthday' => 'birthday-cake',
				'help' => 'question',
				'warning' => 'exclamation',
				'warning-sign' => 'exclamation-triangle',
				'doctor' => 'user-md',
				'handicap' => 'wheelchair',
				'spy' => 'user-secret',
				'student' => 'graduation-cap',
				'gay' => 'mars-double',
				'heterosexual' => 'venus-mars',
				'intergender' => 'transgender',
				'lesbian' => 'venus-double',
				'man' => 'mars',
				'non-binary-transgender' => 'mercury',
				'other-gender' => 'mars-stroke',
				'woman' => 'venus',
				'block-layout' => 'th-large',
				'grid-layout' => 'th',
				'list-layout' => 'th-list',
				'maximize' => 'arrows-alt',
				'resize' => 'arrows',
				'zoom' => 'search-plus',
				'zoom-out' => 'search-minus',
				'bar' => 'beer',
				'cocktail' => 'glass',
				'flag-checke' => 'flag-checkered',
				'hand-lizard' => 'hand-lizard-o',
				'hand-peace' => 'hand-peace-o',
				'hand-paper' => 'hand-paper-o',
				'hand-rock' => 'hand-rock-o',
				'hand-scissors' => 'hand-scissors-o',
				'hand-spock' => 'hand-spock-o',
				'law' => 'balance-scale',
				'lemon' => 'lemon-o',
				'lightning' => 'bolt',
				'moon' => 'moon-o',
				'puzzle' => 'puzzle-piece',
				'shipping' => 'truck',
				'soccer' => 'futbol-o',
				'sun' => 'sun-o',
				'travel' => 'briefcase',
				'treatment' => 'stethoscope',
				'world' => 'globe',
				'circle-notched' => 'circle-o-notch',
				'add' => 'plus',
				'remove' => 'times',
				'checkmark' => 'check',
				'checkmark-box' => 'check-square-o',
				'move' => 'arrows',
				'radio' => 'circle',
				'remove-circle' => 'times-circle',
				'selected-radio' => 'dot-circle-o',
				'newspaper' => 'newspaper-o',
				'sound' => 'headphones',
				'pointing-down' => 'hand-o-down',
				'pointing-left' => 'hand-o-left',
				'pointing-right' => 'hand-o-right',
				'pointing-up' => 'hand-o-up',
				'battery-low' => 'battery-quarter',
				'battery-high' => 'battery-three-quarters',
				'disk' => 'hdd',
				'game' => 'gamepad',
				'keyboard' => 'keyboard-o',
				'power' => 'power-off',
				'tooth-alt' => 'bluetooth-b',
				'tooth' => 'bluetooth',
				'fork' => 'code-fork',
				'empty-heart' => 'heart-o',
				'empty-star' => 'star-o',
				'meh' => 'meh-o',
				'smile' => 'smile-o',
				'frown' => 'frown-o',
				'closed-captioning' => 'cc',
				'mute' => 'microphone-slash',
				'record' => 'video-camera',
				'unmute' => 'microphone',
				'video-play' => 'play-circle',
				'emergency' => 'ambulance',
				'first-aid' => 'medkit',
				'food' => 'cutlery',
				'h' => 'h-square',
				'hospital' => 'hospital-o',
				'marker' => 'map-marker',
				'military' => 'fighter-jet',
				'sort-content-asc' => 'sort-amount-asc',
				'sort-content-desc' => 'sort-amount-desc',
				'attach' => 'paperclip',
				'content' => 'bars',
				'linkify' => 'link',
				'orde-list' => 'list-ol',
				'text-cursor' => 'i-cursor',
				'unlinkify' => 'chain-broken',
				'unorde-list' => 'list-ul',
				'lira' => 'try',
				'pound' => 'gbp',
				'american-express' => 'cc-amex',
				'cit-card' => 'credit-card',
				'diners-club' => 'cc-diners-club',
				'discover' => 'cc-discover',
				'japan-cit-bureau' => 'cc-jcb',
				'mastercard' => 'cc-mastercard',
				'paypal-card' => 'cc-paypal',
				'stripe' => 'cc-stripe',
				'visa' => 'cc-visa',
				'tie' => 'black-tie',
				'envira-gallery' => 'envira',
				'microsoft-edge' => 'edge',
				'optinmonster' => 'optin-monster',
				'pied-piper-hat' => 'pied-piper',
				'pocket' => 'get-pocket',
				'dit' => 'reddit',
				'dit-alien' => 'reddit-alien',
				'dit-square' => 'reddit-square',
				'wikipedia' => 'wikipedia-w',
				'right-quote' => 'quote-right',
				'left-quote' => 'quote-left',
				'quote' => 'quote-left',
				'sidebar' => 'bars',
				'dropdown' => 'caret-down',
				'right-arrow' => 'arrow-right',
				'left-arrow' => 'arrow-left',
				'right-chevron' => 'chevron-right',
				'left-chevron' => 'chevron-left',
				'right-angle' => 'angle-right',
				'left-angle' => 'angle-left',
				'notched-circle-loading' => 'spinner',
				'lightbulb' => 'lightbulb-o',
			]);
		}

	}

	/**
	 * Get Font Awesome class name based on Semantic UI class name.
	 */
	protected static function get_fa($class) {

		self::maybe_load_amp_variables();

		if(isset(self::$fa[$class])) {
			return self::$fa[$class];
		}

		if(isset(self::$fa[str_replace('-o', '', $class)])) {
			return self::$fa[str_replace('-o', '', $class)] . '-o';
		}

		if(isset(self::$fa[str_replace('-square', '', $class)])) {
			return self::$fa[str_replace('-square', '', $class)] . '-square';
		}

		if(isset(self::$fa[str_replace('-circle', '', $class)])) {
			return self::$fa[str_replace('-circle', '', $class)] . '-circle';
		}

		if(isset(self::$fa[str_replace('-alt', '', $class)])) {
			return self::$fa[str_replace('-alt', '', $class)] . '-alt';
		}

		if(isset(self::$fa[str_replace('-h', '', $class)])) {
			return self::$fa[str_replace('-h', '', $class)] . '-h';
		}

		if(isset(self::$fa[str_replace('-v', '', $class)])) {
			return self::$fa[str_replace('-v', '', $class)] . '-v';
		}

		return false;

	}

	public function render_shortcode() {

		if(Helpers\is_amp()) {

			$classes = $this->args['classes'];

			/**
			 * Remove SUI modifier classes.
			 */
			$classes = $classes === 'medium' ? $classes : str_replace([
				'circular', 'bordered', 'disabled', 'inverted',
				'mini', 'tiny', 'small', 'medium', 'large', 'big', 'huge', 'massive',
				'red', 'green', 'blue', 'orange', 'yellow', 'olive', 'teal', 'blue', 'violet', 'purple', 'pink', 'brown', 'grey', 'black',
				'horizontally flipped', 'vertically flipped',
				'counterclockwise rotated', 'clockwise rotated', 
				'top aligned', 'middle aligned', 'bottom aligned', 
			], '', $classes);

			$classes = trim($classes);

			if(strlen($classes) > 0) {

				$classes = str_replace(' ',          '-', $classes);
				$classes = str_replace('outline',    'o', $classes);
				$classes = str_replace('horizontal', 'h', $classes);
				$classes = str_replace('vertical',   'v', $classes);
				$classes = str_replace('alphabet',   'alpha', $classes);
				$classes = str_replace('ascending',  'asc', $classes);
				$classes = str_replace('descending', 'desc', $classes);
				$classes = str_replace(['alternate', 'alternative'],  'alt', $classes);

				$fa_class = self::get_fa($classes);
				if($fa_class) {
					$classes = $fa_class;
					/**
					 * Keep the .right class if it originally had it.
					 */
					if(strpos($this->args['classes'], 'right') !== false) {
						$classes .= ' right';
					}
				}

				return '<i class="fa fa-' . $classes . '" aria-hidden="true"></i>';

			}

		}

		$this->add_class('icon');

		if($this->has_arg('href')) {
			return '<a href="' . $this->args['href'] . '">' . $this->html_tag('i') . '</a>';
		}

		return $this->html_tag('i');

	}

}

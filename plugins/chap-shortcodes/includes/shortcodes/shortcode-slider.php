<?php

namespace Chap\Shortcodes;
use Chap\Helpers;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Shortcode_Slider extends Chap_Shortcode {

	public function __construct($id, $options) {

		$this->default_args = [
			'loop' => false,
			'direction' => 'horizontal',
			'effect' => 'slide',
			'speed' => 300,
			'autoplay' => 0,
			'autoplay_stop_on_last' => false,
			'autoplay_disable_on_interaction' => true,
			'navigation' => false,
			'pagination' => 'none',
			'pagination_clickable' => false,
			'scrollbar' => false,
			'auto_height' => false,
			'amp_height' => 320,
			'amp_type' => 'auto',

			'space_between' => 16,
			'slides_per_view' => 1,
			'slides_per_column' => 1,
			'slides_per_column_fill' => 'column',
			'slides_per_group' => 1,
			'slides_offset_before' => 0,
			'slides_offset_after' => 0,
			'centered_slides' => false,
			'vertical_alignment' => '',

			'grab_cursor' => false,
			'free_mode' => false,
			'mousewheel_control' => false,
			'keyboard_control' => false,
			'a11y' => true,
			'wrapper' => '',

			'config' => '{}',
		];

		$this->description = esc_html__('Wrapper for slides.', 'chap-shortcodes');

		$this->arg_docs = [
			'loop' => esc_html__('Set to "true" to loop slides.', 'chap-shortcodes'),
			'direction' => esc_html__('Slides direction [horizontal|vertical] (default: horizontal).', 'chap-shortcodes'),
			'effect' => esc_html__('Slides transition effect [slide|fade|cube|coverflow|flip] (default: slide).', 'chap-shortcodes'),
			'speed' => esc_html__('Duration of transition between slides (in ms) (default: 300).', 'chap-shortcodes'),
			'autoplay' => esc_html__('Delay between transitions (in ms). If this parameter is 0, auto play will be disabled (default: 0).', 'chap-shortcodes'),
			'autoplay_stop_on_last' => esc_html__('Stop autoplay when last slide is reached (default: false).', 'chap-shortcodes'),
			'autoplay_disable_on_interaction' => esc_html__('Stop autoplay when user interacts with the slider (default: true).', 'chap-shortcodes'),
			'navigation' => esc_html__('Set to "true" to show navigation arrows.', 'chap-shortcodes'),
			'pagination' => esc_html__('Pagination type to show [none|bullets|fraction|progress] (default: none).', 'chap-shortcodes'),
			'pagination_clickable' => esc_html__('Set to "true" to enable clicking on pagination button to transition to appropriate slide (bullets only) (default: false).', 'chap-shortcodes'),
			'scrollbar' => esc_html__('Set to "true" to show scrollbar.', 'chap-shortcodes'),
			'width' => esc_html__('Manually specify a width for the slider (px).', 'chap-shortcodes'),
			'height' => esc_html__('Manually specify a height for the slider (px).', 'chap-shortcodes'),
			'auto_height' => esc_html__('Set to "true" for the slider wrapper to adopt its height to the height of the currently active slide.', 'chap-shortcodes'),
			'amp_height' => esc_html__('Manually specify a height for for the slider on AMP pages (px) (default: 320).', 'chap-shortcodes'),
			'amp_type' => esc_html__('Manually specify AMP slider type [slides|carousel] (default: "auto"). In auto mode "carousel" is used when "slides_per_view" argument is set, how ever carousel mode does not function well with all content types so manual override can be used.', 'chap-shortcodes'),
			'color' => esc_html__('Custom color for slider controls (hex).', 'chap-shortcodes'),

			'space_between' => esc_html__('Distance between slides (px) (default: 16).', 'chap-shortcodes'),
			'slides_per_view' => esc_html__("Number of slides per view (slides visible at the same time in slider's container).", 'chap-shortcodes'),
			'slides_per_column' => esc_html__('Number of slides per column, for multirow layout.', 'chap-shortcodes'),
			'slides_per_column_fill' => esc_html__('Defines how slides should fill rows [column|row].', 'chap-shortcodes'),
			'slides_per_group' => esc_html__('Set numbers of slides to define and enable group sliding.', 'chap-shortcodes'),
			'slides_offset_before' => esc_html__('Additional slide offset in the beginning of the container (before all slides) (px).', 'chap-shortcodes'),
			'slides_offset_after' => esc_html__('Additional slide offset in the end of the container (after all slides) (px).', 'chap-shortcodes'),
			'centered_slides' => esc_html__('If true, then active slide will be centered, not always on the left side.', 'chap-shortcodes'),
			'vertical_alignment' => esc_html__('Vertical alignment of slides [top|middle|bottom] (default: top).', 'chap-shortcodes'),

			'grab_cursor' => esc_html__('Set to "true" to show grab cursor when hovering the slider (default: false).', 'chap-shortcodes'),
			'free_mode' => esc_html__('If true, then slides will not have fixed positions (default: false).', 'chap-shortcodes'),
			'mousewheel_control' => esc_html__('Set to "true" to enable navigation through slides using mouse wheel (default: false).', 'chap-shortcodes'),
			'keyboard_control' => esc_html__('Set to "true" to enable navigation through slides using arrow keys (default: false).', 'chap-shortcodes'),
			'a11y' => esc_html__('Set to "false" to disable keyboard accessibility that provides focusable navigation buttons and basic ARIA for screen readers (default: true).', 'chap-shortcodes'),
			'wrapper' => esc_html__('Additional classes for the slides wrapper.', 'chap-shortcodes'),

			'config' => esc_html__('Custom JavaScript object compatible with Swiper API (default: {}).', 'chap-shortcodes'),
		];

		// $this->presets[esc_html__('With CSS classes')] = [
		// 	'class1',
		// 	'class2',
		// ];

		parent::__construct($id, $options);

	}

	public function render_shortcode() {

		if(!Helpers\is_amp()) {
			$this->add_script('slider');
		}

		// Force an ID.
		if(!isset($this->atts['id'])) {
			$this->atts['id'] = 'csc-slider-' . uniqid();
		}

		// Responsive slides per view when it contains comma separated values
		$views = [];
		if(strpos($this->args['slides_per_view'], ',') !== -1) {
			$views = explode(',', $this->args['slides_per_view']);
			$views = array_map('trim', $views);
			$this->args['slides_per_view'] = reset($views);
		}

		// Compose wrapper classes, used for both AMP and non-AMP sliders.
		$vertical_alignment = strlen($this->args['vertical_alignment']) > 0 ? esc_attr($this->args['vertical_alignment']) . ' aligned ' : '';
		$wrapper_classes = trim($vertical_alignment . 'swiper-wrapper ' . $this->args['wrapper']);

		// Render AMP slider.
		if(Helpers\is_amp()) {
			/**
			 * Galleries are already converted to sliders by AMP plugin.
			 */
			if(strpos($this->raw_content, '[gallery') !== false) {
				return do_shortcode($this->raw_content);
			}

			$this->add_amp_script('amp-carousel');
			unset($this->atts);

			// AMP slider type, carousel or slides.
			if($this->args['amp_type'] === 'auto' || !in_array($this->args['amp_type'], ['slides', 'carousel'])) {
				$type = $this->args['slides_per_view'] > 1 ? 'carousel' : 'slides';
			} else {
				$type = $this->args['amp_type'];
			}

			$height = ($this->has_arg('height') && $this->args['amp_height'] == 320) ? $this->args['height'] : $this->args['amp_height'];
			// Atts from shortcode args.
			$this->atts = [
				'type' => $type,
				'layout' => 'fixed-height',
				'height' => $height,
			];

			// Manual atts for the slider element.
			$slider_atts = '';
			if((int)$this->args['autoplay'] > 0) {
				$slider_atts .= ' autoplay';
				$this->atts['delay'] = (int)$this->args['autoplay'];
			}
			if($this->bool_arg('loop')) {
				$slider_atts .= ' loop';
			}

			// Use wrapper classes.
			$this->add_class($wrapper_classes);

			// Render the slider and add manual atts.
			ob_start();
			echo $this->html_tag('amp-carousel');
			$slider = ob_get_clean();
			$slider = str_replace('<amp-carousel', '<amp-carousel' . $slider_atts, $slider);
			return $slider;
		}

		$this->add_class('swiper-container');
		$this->add_class('csc-swiper-container');

		// JS booleans
		$autoplay_stop_on_last = json_encode($this->bool_arg('autoplay_stop_on_last'));
		$autoplay_disable_on_interaction = json_encode($this->bool_arg('autoplay_disable_on_interaction'));
		$loop = json_encode($this->bool_arg('loop'));
		$auto_height = json_encode($this->bool_arg('auto_height'));
		$centered_slides = json_encode($this->bool_arg('centered_slides'));
		$grab_cursor = json_encode($this->bool_arg('grab_cursor'));
		$centered_slides = json_encode($this->bool_arg('centered_slides'));
		$free_mode = json_encode($this->bool_arg('free_mode'));
		$mousewheel_control = json_encode($this->bool_arg('mousewheel_control'));
		$keyboard_control = json_encode($this->bool_arg('keyboard_control'));
		$a11y = json_encode($this->bool_arg('a11y'));
		$pagination_clickable = json_encode($this->bool_arg('pagination_clickable'));

		// JS conditionals
		$width = $this->has_arg('width') ? 'width:parseInt(' . $this->args['width'] . '),' : '';
		$height = $this->has_arg('height') ? 'height:parseInt(' . $this->args['height'] . '),' : '';
		$pagination = in_array($this->args['pagination'], ['bullets', 'fraction', 'progress']) ? "pagination:'.csc-swiper-pagination'," : '';
		$pagination_type = in_array($this->args['pagination'], ['bullets', 'fraction', 'progress']) ? "paginationType:'" . $this->args['pagination'] . "'," : '';
		$navigation = $this->bool_arg('navigation') ? "nextButton:'.csc-swiper-button-next',prevButton:'.csc-swiper-button-prev'," : '';
		$scrollbar = $this->bool_arg('scrollbar') ? "scrollbar:'.csc-swiper-scrollbar'," : '';
		$slides_per_view = $this->args['slides_per_view'] === 'auto' ? "'auto'" : 'parseInt(' . (int)$this->args['slides_per_view'] . ')';

		// Remove WPAUTOP code from conf string
		$conf = str_replace([
			'<br>',
			'<br/>',
			'<br />',
			'<p>',
			'</p>',
		], '', $this->args['config']);

		// Create responsive conf from views
		if(count($views) === 3) {
			$view_conf = '{breakpoints:{1024:{slidesPerView:' . (int)$views[1] . '},425:{slidesPerView:' . (int)$views[2] . '}}}';
			$conf = 'Object.assign(' . $view_conf . ',' . $conf . ')';
		}

		// JS Swiper configuration
		$id = $this->atts['id'];
		$js = isset($GLOBALS['csc']['slider-obj']) ? '' : 'var csc_init_swipers = csc_init_swipers || [];';
		$GLOBALS['csc']['slider-obj'] = true;

		$js .= <<<JS
csc_init_swipers.push(
	function(){
		return Object.assign({
			csc_id:'{$this->atts["id"]}',
			direction:'{$this->args["direction"]}',
			effect:'{$this->args["effect"]}',
			{$width}
			{$height}
			{$pagination}
			{$pagination_type}
			{$navigation}
			{$scrollbar}
			speed:parseInt({$this->args['speed']}),
			autoplay:parseInt({$this->args['autoplay']}),
			paginationClickable:{$pagination_clickable},
			autoplayStopOnLast:{$autoplay_stop_on_last},
			autoplayDisableOnInteraction:{$autoplay_disable_on_interaction},
			loop:{$loop},
			autoHeight:{$auto_height},
			spaceBetween:parseInt({$this->args['space_between']}),
			slidesPerView:{$slides_per_view},
			slidesPerColumn:parseInt({$this->args['slides_per_column']}),
			slidesPerColumnFill:'{$this->args["slides_per_column_fill"]}',
			slidesPerGroup:parseInt({$this->args['slides_per_group']}),
			slidesOffsetBefore:parseInt({$this->args['slides_offset_before']}),
			slidesOffsetAfter:parseInt({$this->args['slides_offset_after']}),
			grabCursor:{$grab_cursor},
			centeredSlides:{$centered_slides},
			freeMode:{$free_mode},
			mousewheelControl:{$mousewheel_control},
			keyboardControl:{$keyboard_control},
			a11y:{$a11y}
		},{$conf});
	}
);
JS;
		wp_add_inline_script('chap/js', str_replace(["\r", "\n", "\t"], '', $js));

		// fix editor syntax highlighting broken by above JS string
		?>
		<?php

		$pagination = in_array($this->args['pagination'], ['bullets', 'fraction', 'progress']) ? '<div class="csc-swiper-pagination swiper-pagination"></div>' : '';
		$navigation = $this->bool_arg('navigation') ? '<div class="csc-swiper-button-prev swiper-button-prev"></div><div class="csc-swiper-button-next swiper-button-next"></div>' : '';
		$scrollbar = $this->bool_arg('scrollbar') ? '<div class="csc-swiper-scrollbar swiper-scrollbar"></div>' : '';

		$this->content = <<<HTML
<div class="{$wrapper_classes}">
	{$this->content}
</div>
{$pagination}
{$navigation}
{$scrollbar}
HTML;

		// Add slider dimensions inline.
		if($this->has_arg('width')) {
			$this->add_inline_style('width', (int)$this->args['width'] . 'px');
		}
		if($this->has_arg('height')) {
			$this->add_inline_style('height', (int)$this->args['height'] . 'px');
		}

		// Add inline CSS to modify slider control colors.
		if($this->has_arg('color')) {
			$color = str_replace('#', '', esc_attr($this->args['color']));
			$css = <<<CSS
<style type="text/css">
	#{$this->atts['id']} .swiper-pagination-bullets .swiper-pagination-bullet-active, 
	#{$this->atts['id']} .swiper-pagination-progress .swiper-pagination-progressbar {
		background: #{$color} !important;
	}
	#{$this->atts['id']} .swiper-button-next {
		background-image: url("data:image/svg+xml;charset=utf-8,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 27 44'%3E%3Cpath d='M27 22L5 44l-2.1-2.1L22.8 22 2.9 2.1 5 0l22 22z' fill='%23{$color}'/%3E%3C/svg%3E");
	}
	#{$this->atts['id']} .swiper-button-prev {
		background-image: url("data:image/svg+xml;charset=utf-8,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 27 44'%3E%3Cpath d='M0 22L22 0l2.1 2.1L4.2 22l19.9 19.9L22 44 0 22z' fill='%23{$color}'/%3E%3C/svg%3E");
	}
</style>
CSS;
			add_action('wp_footer', function() use ($css) {
				echo $css;
			});
		}

		$return = $this->html_tag('div');

		return $return;
	}

	/**
	 * Keep slider context.
	 */
	public function render($args, $content = '') {
		wp_enqueue_style('chap/swiper');
		wp_enqueue_script('chap/swiper');

		$GLOBALS['csc']['slider'] = true;

		/**
		 * When outputting a post in slider context, add slide classes.
		 */
		$post_class_filter = function($classes) {
			$classes[] = 'swiper-slide';
			$classes[] = 'csc-slide';
			return $classes;
		};
		add_filter('post_class', $post_class_filter);

		$this->parse_args($args, $this->default_args);
		$this->add_amp_style();
		$this->content = do_shortcode($content);
		$this->raw_content = $content;

		remove_filter('post_class', $post_class_filter);

		$GLOBALS['csc']['slider'] = false;

		return $this->render_shortcode();

	}

}

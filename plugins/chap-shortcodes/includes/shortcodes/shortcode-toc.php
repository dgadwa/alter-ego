<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

/**
 * @since 1.0.3
 */
class Chap_Shortcode_Toc extends Chap_Shortcode {

	protected static $root_level = 10;

	public function __construct($id, $options) {

		$this->default_args = [
			'title' => esc_html('Table of contents'),
			'header' => 'small center aligned',
			'classes' => 'vertical',
			'container' => 'basic padded right floated',
			'smooth' => 'true',
		];

		$this->description = esc_html('Wrapping post content in this will create a Table of contents menu from headers that have ID attributes.');

		$this->arg_docs = [
			'title' => esc_html__('Title above TOC menu (default: "Table of contents").', 'chap-shortcodes'),
			'header' => esc_html__('Custom classes for the title header (default: "small center aligned").', 'chap-shortcodes'),
			'classes' => esc_html__('Custom classes for the menu (default: "vertical").', 'chap-shortcodes'),
			'container' => esc_html__('Custom classes for the menu container segment (default: "basic padded right floated").', 'chap-shortcodes'),
			'smooth' => esc_html__('Smooth scrolling to anchor links (default: "true").', 'chap-shortcodes'),
		];

		// $this->presets['Preset'] = [
		// 	'class',
		// ];

		parent::__construct($id, $options);

	}

	protected function has_children($index, $level) {

		if($level > self::$root_level) {
			return false;
		}

		$next = isset($GLOBALS['csc-toc'][$index + 1]) ? $GLOBALS['csc-toc'][$index + 1] : false;

		if($next && $next['level'] > $level) {
			return true;
		}

		return false;

	}

	public function render_shortcode() {

		self::$root_level = 10;
		$GLOBALS['csc-toc'] = [];

		$this->content = do_shortcode($this->raw_content);

		if(count($GLOBALS['csc-toc']) > 0) {

			$toc = '[csc-segment ' . $this->args['container'] . ' toc]';

			if($this->has_arg('title')) {
				$toc .= '[csc-header ' . $this->args['header'] . ']' . $this->args['title'] . '[/csc-header]';
			}

			$toc .= '[csc-menu ' . $this->args['classes'] . ']';

			$close_menu = false;

			foreach($GLOBALS['csc-toc'] as $index => $item) {

				$level = $item['level'];

				if($level <= self::$root_level) {

					self::$root_level = $level;

					if($close_menu) {
						$toc .= '[/csc-submenu]';
						$toc .= '[/csc-div]';
						$close_menu = false;
					}

				}

				if(self::has_children($index, $level)) {

					$toc .= '[csc-div item]';
					$toc .= '[csc-header tiny marginless url="#' . $item['id'] . '"]' . $item['content'] . '[/csc-header]';
					$toc .= '[csc-submenu]';

					$close_menu = true;

				} else {

					$toc .= '[csc-item url="#' . $item['id'] . '"]' . $item['content'] . '[/csc-item]';

				}

			}

			if($close_menu) {
				$toc .= '[/csc-submenu]';
				$toc .= '[/csc-div]';
			}

			$toc .= '[/csc-menu]';
			$toc .= '[/csc-segment]';

			$this->content = do_shortcode($toc) . $this->content;

		}

		unset($GLOBALS['csc-toc']);

		if($this->args['smooth'] === 'true') {
			$this->add_script('toc');
		}

		return $this->content;

	}

}

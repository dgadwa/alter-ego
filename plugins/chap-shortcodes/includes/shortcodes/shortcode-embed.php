<?php

namespace Chap\Shortcodes;
use Chap\Helpers;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Shortcode_Embed extends Chap_Shortcode {

	public function __construct($id, $options) {

		$this->default_args = [
			'amp-iframe' => false,
			'amp-height' => 400,
		];

		$this->description = esc_html__('Embed content responsively.', 'chap-shortcodes');

		$this->arg_docs = [
			'source' => esc_html__('Source to use, if no source is provided it will be determined from the domain of a specified URL (youtube|vimeo).', 'chap-shortcodes'),
			'url' => esc_html__('URL to use for embed.', 'chap-shortcodes'),
			'image' => esc_html__('Link to placeholder image.', 'chap-shortcodes'),
			'icon' => esc_html__('Icon to use with placeholder.', 'chap-shortcodes'),
			'classes' => [
				'ratio' => ['4:3', '16:9', '21:9'],
			],
			'amp' => sprintf(
				esc_html__('(Optional) Alternative URL used on AMP pages, use an URL suitable for the %1$sWordPress &#91;embed&#93; shortcode%2$s.', 'chap-shortcodes'),
				'<a href="https://codex.wordpress.org/Embeds" target="_blank" rel="noopener">',
				'</a>'
			),
			'amp-iframe' => esc_html__('Set to "true" to force iFrame usage on AMP pages.', 'chap-shortcodes'),
			'amp-height' => esc_html__('AMP iFrame height (default: 400).', 'chap-shortcodes'),
		];

		$this->presets[esc_html__('YouTube', 'chap-shortcodes')] = [
			'source' => 'youtube',
			'id' => 'XSGBVzeBUbk',
		];

		$this->presets[esc_html__('YouTube link', 'chap-shortcodes')] = [
			'url' => 'https://www.youtube.com/watch?v=XSGBVzeBUbk',
		];

		parent::__construct($id, $options);

	}

	public function render_shortcode() {

		if(Helpers\is_amp()) {

			if($this->has_arg('url')) {
				$url = $this->args['url'];
			} elseif($this->has_arg('amp')) {
				$url = $this->args['amp'];
			} elseif($this->has_arg('source')) {
				$src = $this->args['source'];
				$id = $this->args['id'];
				switch($src) {
					case 'youtube':
						$url = 'https://www.youtube.com/watch?v=' . $id;
					break;
					case 'vimeo':
						$url = 'https://player.vimeo.com/video/' . $id;
					break;
				}
			}

			if(!isset($url)) {
				return;
			}
			$url = esc_url($url);

			if(function_exists('\\Chap\\Options\\get')) {
				$max_width = \Chap\Options\get('amp_content_max_width');
			} else {
				$max_width = 600;
			}
			$max_height = $this->args['amp-height'];

			$embed_content = '[embed]' . $url . '[/embed]';

			if(strpos($url, 'google.com/maps/embed') !== false || $this->args['amp-iframe'] === 'true') {
				$embed_content = <<<GMAPS
<iframe 
	src="{$url}" 
	width="{$max_width}" 
	height="{$max_height}" 
	frameborder="0" 
>
</iframe>
GMAPS;
			}

			$amp_content = new \AMP_Content(
				$embed_content,
				[
					'AMP_Twitter_Embed_Handler' => [],
					'AMP_YouTube_Embed_Handler' => [],
					'AMP_Instagram_Embed_Handler' => [],
					'AMP_Vine_Embed_Handler' => [],
					'AMP_Facebook_Embed_Handler' => [],
					'AMP_Gallery_Embed_Handler' => [],
				],
				[
					'AMP_Style_Sanitizer' => [],
					'AMP_Blacklist_Sanitizer' => [],
					'AMP_Img_Sanitizer' => [],
					'AMP_Video_Sanitizer' => [],
					'AMP_Audio_Sanitizer' => [],
					'AMP_Iframe_Sanitizer' => [
						'add_placeholder' => true,
					],
				],
				[
					'content_max_width' => $max_width,
				]
			);

			foreach($amp_content->get_amp_scripts() as $id => $src) {
				$this->add_amp_script($id);
			}

			return $amp_content->get_amp_content(); // AMP html, no kses.

		}

		$this->add_script('embed');

		$this->prepend_class('ui');
		$this->add_class('embed');

		if($this->has_arg('source')) {
			$this->atts['data-source'] = $this->args['source'];
		}

		if($this->has_arg('id')) {
			$this->atts['data-id'] = $this->args['id'];
			unset($this->atts['id']);
		}

		if($this->has_arg('url')) {

			/**
			 * Bybass "Refused to display in a frame because it set 'X-Frame-Options' to 'SAMEORIGIN'" error.
			 */
			$this->args['url'] = str_replace('watch?v=', 'v/', $this->args['url']);

			$this->atts['data-url'] = $this->args['url'];

			unset($this->atts['href']);
			unset($this->args['href']);

		}

		if($this->has_arg('icon')) {
			$this->atts['data-icon'] = $this->args['icon'];
		}

		if($this->has_arg('image')) {
			$this->atts['data-placeholder'] = $this->args['image'];
		}

		return $this->html_tag('div');

	}

}

<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Shortcode_Column extends Chap_Shortcode {

	public function __construct($id, $options) {

		$this->description = esc_html__('Grid column.', 'chap-shortcodes');

		$this->arg_docs = [
			'classes' => [
				'width' => ['two wide', 'four wide', '...', 'sixteen wide'],
				'float' => ['left floated', 'right floated'],
				'alignment' => ['left aligned', 'center aligned', 'right aligned'],
				'color' => ['red', 'green', 'blue', '...'],
			],
		];

		parent::__construct($id, $options);

	}

	public function render_shortcode() {

		$this->add_amp_style('grid');
		$this->add_amp_style('row');

		$this->add_class('column');

		return $this->html_tag('div');

	}

}

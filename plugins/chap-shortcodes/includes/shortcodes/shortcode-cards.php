<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Shortcode_Cards extends Chap_Shortcode {

	public function __construct($id, $options) {

		$this->description = esc_html__('Container for cards.', 'chap-shortcodes');

		$this->arg_docs = [
			'classes' => [
				'columns' => ['one', 'two', 'three', '...'],
				'color' => ['red', 'green', 'blue', '...'],
				'mobile' => ['stackable', 'doubling'],
			],
		];

		$this->presets[esc_html__('Specified count', 'chap-shortcodes')] = [
			'three',
		];

		parent::__construct($id, $options);

	}

	public function render_shortcode() {

		$this->prepend_class('ui');
		$this->add_class('cards');

		return $this->html_tag('div');

	}

}

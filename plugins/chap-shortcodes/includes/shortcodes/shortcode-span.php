<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Shortcode_Span extends Chap_Shortcode {

	public function __construct($id, $options) {

		$this->description = esc_html__('HTML &lt;span&gt; tag as a shortcode. Useful because inside shortcodes WP autop is disabled.', 'chap-shortcodes');

		$this->presets[esc_html__('With CSS classes', 'chap-shortcodes')] = [
			'class1',
			'class2',
		];

		parent::__construct($id, $options);

	}

	public function render_shortcode() {

		return $this->html_tag('span');

	}

}

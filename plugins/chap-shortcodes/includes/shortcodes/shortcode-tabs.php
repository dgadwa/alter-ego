<?php

namespace Chap\Shortcodes;
use Chap\Helpers;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Shortcode_Tabs extends Chap_Shortcode {

	public function __construct($id, $options) {

		$this->default_args = [
			'tab_classes' => '',
		];

		$this->description = '';

		$this->arg_docs = [
			'classes' => [
				'type' => ['primary', 'secondary', 'pointing', 'tabular', 'text', 'pagination'],
				'size' => ['mini', 'tiny', 'small', 'medium', 'large', 'big', 'huge', 'massive'],
				'alignment' => ['left aligned', 'right aligned'],
				'attachment' => ['top attached', 'bottom attached'],
			],
			'tab_classes' => esc_html__('Classes for all child items.', 'chap-shortcodes'),
		];

		$this->presets[esc_html__('Attached pointing tabs', 'chap-shortcodes')] = [
			'top',
			'attached',
			'pointing',
			'tab_classes' => 'bottom attached',
		];

		$this->presets[esc_html__('Attached tabular tabs', 'chap-shortcodes')] = [
			'top',
			'attached',
			'tabular',
			'tab_classes' => 'bottom attached',
		];

		$this->presets[esc_html__('Secondary pointing tabs', 'chap-shortcodes')] = [
			'secondary',
			'pointing',
			'tab_classes' => 'basic paddingless',
		];

		parent::__construct($id, $options);

	}

	public function render($args, $content = '') {

		// Start collecting tabs.
		$GLOBALS['csc']['tabs'] = [];

		$this->parse_args($args, $this->default_args);
		$this->add_amp_style();
		$this->content = do_shortcode($content);
		$this->raw_content = $content;

		if(Helpers\is_amp()) {
			$output = $this->amp_render_shortcode();
		} else {
			$output = $this->render_shortcode();
		}

		// Stop collecting tabs.
		unset($GLOBALS['csc']['tabs']);

		return $output;

	}

	public function render_shortcode() {

		/**
		 * Check that there are tabs.
		 */
		if(!isset($GLOBALS['csc']['tabs'])) {
			return;
		}

		$this->add_script('tabs');

		$this->prepend_class('ui');
		$this->add_class('csc-tabs');

		// Default to menu, but allow to use list if specified.
		if(!$this->has_class('list')) {
			$this->add_class('menu');
		}

		/**
		 * Find if any tab has been manually set active.
		 * If none is found the first tab will be set to active.
		 */
		$active_tab = 0;
		foreach($GLOBALS['csc']['tabs'] as $i => $tab) {
			if($tab['active']) {
				$active_tab = $i;
				break;
			}
		}

		/**
		 * Build the tab items menu.
		 */
		foreach($GLOBALS['csc']['tabs'] as $i => $tab) {

			/**
			 * Build tab item classes.
			 */
			$classes = [];
			if($i === 0 && $active_tab === 0) {
				$classes[] = 'active';
			}
			if(isset($tab['classes']) && !empty($tab['classes'])) {
				$classes[] = $tab['classes'];
			}
			$classes[] = 'item';

			/**
			 * Last right aligned item should have negative pixel.
			 */
			$styles = [];
			if(strpos($tab['classes'], 'right') !== false && $i === (count($GLOBALS['csc']['tabs']) - 1)) {
				$styles[] = 'margin-right:-1px';
			}

			/**
			 * Render the tab item.
			 */
			if(strpos($tab['classes'], 'disabled') !== false) {
				$this->content .= Helpers\html_tag('div', [
					'classes' => $classes,
					'style' => $styles,
				]) . do_shortcode($tab['title']) . '</div>';
			} else {
				$this->content .= Helpers\html_tag('a', [
					'href' => '#',
					'classes' => $classes,
					'rel' => 'nofollow',
					'data-tab' => $tab['id'],
					'style' => $styles,
				]) . do_shortcode($tab['title']) . '</a>';
			}

		}

		/**
		 * Render the tabs menu items with classes.
		 */
		$this->content = $this->html_tag('div');

		/**
		 * Center aligned menu needs to be wrapped in a container.
		 */
		if($this->has_classes(['center', 'aligned'])) {
			$this->content = do_shortcode('[csc-container center aligned]' . $this->content . '[/csc-container]');
		}

		/**
		 * Render tabs contents.
		 */
		foreach($GLOBALS['csc']['tabs'] as $i => $tab) {

			/**
			 * Build tab content container classes.
			 */
			$classes = ['ui'];
			if($i === $active_tab) {
				$classes[] = 'active';
			}
			if($this->has_arg('tab_classes')) {
				$classes = array_merge($classes, explode(' ', $this->args['tab_classes']));
			}
			$classes[] = 'tab';
			$classes[] = 'segment';

			/**
			 * Render the tab content container.
			 */
			if(strpos($tab['classes'], 'disabled') === false) {
				$this->content .= Helpers\html_tag('div', [
					'classes' => $classes,
					'data-tab' => $tab['id'],
				]) . $tab['content'] . '</div>';
			}

		}

		return $this->content;

	}

	public function amp_render_shortcode() {

		/**
		 * Check that there are tabs.
		 */
		if(!isset($GLOBALS['csc']['tabs'])) {
			return;
		}

		$this->add_amp_style('menu');
		$this->add_amp_script('amp-accordion');

		$this->prepend_class('ui');
		$this->add_class('csc-tabs');
		$this->add_class('menu');

		/**
		 * Find if any tab has been manually set active.
		 * If none is found the first tab will be set to active.
		 */
		$active_tab = 0;
		foreach($GLOBALS['csc']['tabs'] as $i => $tab) {
			if($tab['active']) {
				$active_tab = $i;
				break;
			}
		}

		/**
		 * Build the tab items menu.
		 */
		foreach($GLOBALS['csc']['tabs'] as $i => $tab) {

			/**
			 * Render the tab item.
			 */
			$section = Helpers\html_tag('h3', [
				'classes' => ['active', 'item'],
			]) . do_shortcode($tab['title']) . '</h3>';

			/**
			 * Render the tab content container.
			 */
			$section .= Helpers\html_tag('div', [
				'classes' => ['ui', 'basic', 'marginless', 'tab', 'segment'],
			]) . $tab['content'] . '</div>';

			/**
			 * Expand active tab.
			 */
			if(($i === 0 && $active_tab === 0) || ($i === $active_tab)) {
				$expanded = ' expanded';
			} else {
				$expanded = '';
			}

			/**
			 * Wrap in a section.
			 */
			$this->content .= '<section' . $expanded . ' >' . $section . '</section>';

		}

		/**
		 * Render amp-accordion.
		 */
		$this->content = Helpers\html_tag('amp-accordion', [
			'classes' => $this->atts['classes'],
		]) . $this->content . '</amp-accordion>';

		/**
		 * Disable session states.
		 */
		$this->content = str_replace('<amp-accordion', '<amp-accordion disable-session-states', $this->content);

		return $this->content;

	}

}

<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Shortcode_Segment extends Chap_Shortcode {

	public function __construct($id, $options) {

		$this->description = esc_html('A segment is used to create a grouping of related content.');

		$this->arg_docs = [
			'classes' => [
				'type' => ['basic', 'raised', 'stacked', 'piled', 'vertical'],
				'emphasis' => ['secondary', 'tertiary'],
				'color' => ['red', 'green', 'blue', '...'],
				'size' => ['mini', 'tiny', 'small', 'medium', 'large', 'big', 'huge', 'massive'],
				'padding' => ['padded', 'very padded'],
				'float' => ['left floated', 'right floated'],
				'alignment' => ['left aligned', 'center aligned', 'right aligned'],
				'attachment' => ['top attached', 'attached', 'bottom attached'],
				'other' => ['compact', 'circular', 'clearing', 'disabled', 'inverted', 'loading', 'fluid'],
			],
		];

		parent::__construct($id, $options);

	}

	public function render_shortcode() {

		/**
		 * Add z-index for piled segments.
		 */
		if($this->has_class('piled')) {
			$this->add_inline_style('z-index', '0');
		}

		$this->prepend_class('ui');
		$this->add_class('segment');

		return $this->html_tag('div');

	}

}

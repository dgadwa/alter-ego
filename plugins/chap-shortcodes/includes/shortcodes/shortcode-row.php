<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Shortcode_Row extends Chap_Shortcode {

	public function __construct($id, $options) {

		$this->description = esc_html__('Grid row.', 'chap-shortcodes');

		$this->arg_docs = [
			'classes' => [
				'columns' => ['two column', 'three column', '...'],
				'mobile' => ['stackable', 'doubling'],
			],
		];

		parent::__construct($id, $options);

	}

	public function render_shortcode() {

		$this->add_amp_style('grid');
		$this->add_amp_style('column');

		$this->add_class('row');

		return $this->html_tag('div');

	}

}

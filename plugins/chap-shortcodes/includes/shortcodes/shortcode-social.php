<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Shortcode_Social extends Chap_Shortcode {

	public function __construct($id, $options) {

		$this->has_content = false;

		$this->default_args = [
			'text' => true,
			'multiline' => false,
			'only' => '',
		];

		$this->description = esc_html__('Social buttons with links to social media or sharing a page.', 'chap-shortcodes');

		$this->arg_docs = [
			'supported' => 'facebook|twitter|google plus|linkedin|pinterest|vk|instagram|youtube',
			'classes' => [
				'size' => ['mini', 'tiny', 'small', 'medium', 'large', 'big', 'huge', 'massive'],
				'float' => ['left floated', 'right floated'],
				'icon' => ['icon', 'right icon', 'labeled icon', 'right labeled icon'],
				'other' => ['compact', 'inverted', 'circular', 'fluid', 'active', 'disabled'],
			],
			'text' => esc_html__('Show text (default: true)', 'chap-shortcodes'),
			'multiline' => esc_html__('Multiple lines (default: false)', 'chap-shortcodes'),
			'only' => esc_html__('List of social medias to render, pipe-separated.', 'chap-shortcodes'),
			'share' => esc_html__('Switches social media links to sharing links. Set to "true" to share the current page, or a custom link.', 'chap-shortcodes'),
			'title' => esc_html__('Social sharing title (default: current page title).', 'chap-shortcodes'),
			'image' => esc_html__('Social sharing image (default: current page featured image) (Pinterest only).', 'chap-shortcodes'),
		];

		$this->presets[esc_html__('No text', 'chap-shortcodes')] = [
			'text' => 'false',
		];

		$this->presets[esc_html__('Multiple lines', 'chap-shortcodes')] = [
			'multiline' => 'true',
		];

		$this->presets[esc_html__('Sharing links', 'chap-shortcodes')] = [
			'share' => 'true',
		];

		parent::__construct($id, $options);

	}

	public function render_shortcode() {

		$options = get_option('csc-social');
		if(!$options || count($options) < 1) {
			return;
		}

		$show_text = $this->args['text'] && $this->args['text'] !== 'false';
		$multiline = $this->args['multiline'] && $this->args['multiline'] !== 'false';

		/**
		 * Add icon class if no text is shown.
		 */
		if(!$show_text) {
			if(strpos($this->args['classes'], 'icon') === false) {
				$this->args['classes'] .= ' icon';
			}
		}

		/**
		 * Load Social Medias list to access the text.
		 */
		if($show_text) {
			global $plugin_chap_shortcodes;
			$social_medias = $plugin_chap_shortcodes->get_chap_shortcodes_social();
		}

		/**
		 * Only render these social medias.
		 */
		$only = [];
		if($this->has_arg('only')) {
			$only = explode('|', $this->args['only']);
		}

		foreach($options as $id => $link) {

			/**
			 * Don't render if not in "only" array.
			 */
			if(count($only) > 0) {
				if(!in_array($id, $only)) {
					continue;
				}
			}

			if($this->has_arg('share')) {

				if($this->args['share'] === 'true') {
					$link = get_permalink();
				} else {
					$link = $this->args['share'];
				}

				/**
				 * This social media doesn't have a share link.
				 */
				if(!isset($social_medias[$id]['share'])) {
					continue;
				}

				if(!$this->has_arg('image')) {
					$this->args['image'] = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full');
				}

				if(!$this->has_arg('title')) {
					$this->args['title'] = get_the_title();
				}

				$link = str_replace(
					[
						'{url}',
						'{img}',
						'{title}',
					],
					[
						urlencode($link),
						urlencode($this->args['image']),
						urlencode($this->args['title']),
					],
					$social_medias[$id]['share']
				);

			}

			if(!empty($link)) {

				$social_class = strpos($this->args['classes'], 'inverted') === false ? $id : '';
				$style = '';

				if($multiline) {
					$style = ' style="margin-bottom:.5em"';
				}

				$this->content .= '[csc-button ' . $this->args['classes'] . ' ' . $social_class . ' link="' . $link . '" target="_blank"' . $style . ']';
				$this->content .= '[csc-icon ' . $id . ']';

				if($show_text) {
					$this->content .= $social_medias[$id]['name'];
				}

				$this->content .= '[/csc-button]';

				if($multiline) {
					$this->content .= '<br />';
				}

			}

		}

		return do_shortcode($this->content);

	}

}

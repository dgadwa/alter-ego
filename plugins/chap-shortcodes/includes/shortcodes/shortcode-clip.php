<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Shortcode_Clip extends Chap_Shortcode {

	public function __construct($id, $options) {

		$this->default_args = [
			'clip' => 5,
		];

		$this->arg_docs = [
			'clip [ n ]' => esc_html__('Clip the top and bottom.', 'chap-shortcodes'),
			'clip [ n | 0 ]' => esc_html__('Clip the top.', 'chap-shortcodes'),
			'clip [ 0 | n ]' => esc_html__('Clip the bottom.', 'chap-shortcodes'),
		];

		$this->description = esc_html__('Clip elements.', 'chap-shortcodes');

		parent::__construct($id, $options);

	}

	public function render_shortcode() {

		return $this->html_tag('div');

	}

}

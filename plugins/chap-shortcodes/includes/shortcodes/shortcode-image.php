<?php

namespace Chap\Shortcodes;
use Chap\Helpers;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Shortcode_Image extends Chap_Shortcode {

	public function __construct($id, $options) {

		$this->default_args = [
			'alt' => '',
			'caption_classes' => '',
		];

		$this->description = esc_html__('Wrapper for WordPress HTML images.', 'chap-shortcodes');

		$this->arg_docs = [
			'classes' => [
				'size' => ['mini', 'tiny', 'small', 'medium', 'large', 'big', 'huge', 'massive'],
				'variation' => ['bordered', 'rounded', 'circular'],
				'alignment' => ['left aligned', 'center aligned', 'right aligned'],
				'float' => ['left floated', 'right floated'],
				'other' => ['fluid', 'disabled', 'spaced', 'avatar', 'lazyload'],
			],
			'src' => esc_html__('Optional: image link (image can also be specified inside the shortcode, using an HTML &lt;img&gt; tag).', 'chap-shortcodes'),
			'alt' => esc_html__('Optional: alternative text (only used when src argument is specified).', 'chap-shortcodes'),
			'caption' => esc_html__('Optional: image caption (only used when src argument is specified).', 'chap-shortcodes'),
			'caption_classes' => esc_html__('Optional: caption classes [center aligned|right aligned] (only used when src argument is specified).', 'chap-shortcodes'),
			'href' => esc_html__('The href/url/link argument can be used to specify a link. If the image has "zoomable" class then the link will be used as the full-size image.', 'chap-shortcodes'),
		];

		parent::__construct($id, $options);

	}

	/**
	 * Images must be wrapped in a segment for alignment.
	 */
	protected function align_image($image) {

		if(!$this->has_class('aligned')) {
			return $image;
		}

		$classes = [
			'ui',
			'basic',
			'paddingless',
		];

		if($this->has_class('left')) {
			$classes[] = 'left';
		} elseif($this->has_class('center')) {
			$classes[] = 'center';
		} elseif($this->has_class('right')) {
			$classes[] = 'right';
		}

		$classes[] = 'aligned';
		$classes[] = 'segment';

		return '<div class="' . join(' ', $classes) . '">' . $image . '</div>';

	}

	/**
	 * Wraps the caption text in div.ui.caption.
	 */
	protected function wrap_caption_text($content, $caption_args) {

		$classes = ['ui'];

		if(isset($caption_args['align'])) {

			switch($caption_args['align']) {
				case 'alignleft':
					$classes[] = 'left';
				break;
				case 'alignright':
					$classes[] = 'right';
				break;
				case 'aligncenter':
					$classes[] = 'center';
				break;
			}

			if($caption_args['align'] !== 'alignnone') {
				$classes[] = 'aligned';
			}

		}

		if(isset($caption_args['class'])) {
			$classes[] = $caption_args['class'];
		}

		$classes[] = 'caption';

		$classes = join(' ', $classes);

		if(strpos($content, '<') === 0) {
			/**
			 * Caption located after the image/html.
			 */
			if(strpos($content, '<a') !== false) {
				$content = str_replace('</a>', '<div class="' . $classes . '">', $content) . '</div></a>';
			} else {
				$content = str_replace('>', '><div class="' . $classes . '">', $content) . '</div>';
			}
		} else {
			/**
			 * Caption is located before the image/html.
			 */
			$pos = strpos($content, '<');
			if($pos !== false) {
				$content = '<div class="' . $classes . '">' . substr_replace($content, '</div><', $pos, 1);
			}
		}

		return $content;

	}

	public function render_shortcode() {
		/**
		 * Use lazy loading.
		 */
		$lazy = !Helpers\is_amp() && (get_option('csc-lazy-loading') == 1 || $this->has_class('lazyload'));
		$src_att = $lazy ? 'data-src' : 'src';

		/**
		 * Add a default alt text for placehold.it images.
		 */
		if(!$this->has_arg('alt') && $this->has_arg('src')) {
			if(strpos($this->args['src'], 'placehold.it') !== false) {
				$this->args['alt'] = esc_html__('Placeholder image', 'chap-shortcodes');
			}
		}

		$this->prepend_class('ui');
		$this->add_class('image');

		/**
		 * Image url specified in shortcode arguments.
		 */
		if($this->has_arg('src')) {
			$class = $lazy ? 'class="lazyload" ' : '';
			$alt = $this->has_arg('alt') ? ' alt="' . $this->args['alt'] . '"' : '';
			$this->content .= '<img ' . $class . $src_att . '="' . $this->args['src'] . '"' . $alt . ' />';
			if($lazy) {
				$this->content .= '<noscript><img src="' . $this->args['src'] . '"' . $alt . ' /></noscript>';
			}
			if($this->has_arg('caption')) {
				$this->content .= '<div class="ui ' . $this->args['caption_classes'] . ' caption">' . $this->args['caption'] . '</div>';
			}
			return $this->align_image($this->html_tag('div'));
		}

		/**
		 * Parse shortcode content to find caption shortcode.
		 */
		$shortcodes = Parser::parse_shortcodes($this->raw_content);
		$caption_args = [];

		/**
		 * Look for caption shortcode.
		 */
		foreach($shortcodes as $shortcode) {
			if($shortcode['name'] === 'caption') {
				/**
				 * Store caption arguments.
				 */
				if(isset($shortcode['attrs'])) {
					foreach($shortcode['attrs'] as $attrs) {
						foreach($attrs as $attr => $value) {
							$caption_args[$attr] = $value;
						}
					}
				}
				/**
				 * Caption shortcode content.
				 */
				$content = isset($shortcode['content']) ? $shortcode['content'] : '';
				/**
				 * Use caption content as shortcode content and wrap caption text.
				 */
				$this->raw_content = $this->wrap_caption_text($content, $caption_args);
				/**
				 * Only one caption can be used per image, exit the loop.
				 */
				break;
			}
		}

		/**
		 * Use original content.
		 */
		$this->content = $this->raw_content;

		/**
		 * Remove WP classes.
		 */
		$this->content = str_replace([
			'alignnone',
			'alignleft',
			'alignright',
			'aligncenter',
		], '', $this->content);

		/**
		 * Content contains a link.
		 */
		if(strpos($this->content, '<a') !== false) {
			/**
			 * Already is a link, don't use url from argument.
			 */
			unset($this->args['href']);
			/**
			 * Add current classes to the link.
			 */
			$parent_classes = join(' ', $this->atts['classes']);
			$parent_classes = str_replace('zoomable', '', $parent_classes);
			$this->content = str_replace('<a', '<a class="' . $parent_classes . '"', $this->content);
		}

		return $this->align_image($this->html_tag('div'));

	}

}

<?php

namespace Chap\Shortcodes;
use Chap\Helpers;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Shortcode_Header extends Chap_Shortcode {

	public function __construct($id, $options) {

		$this->default_args = [
			'tag' => '',
			'icon' => '',
			'image' => '',
			'image_size' => 'mini',
			'alt' => esc_html__('Header icon', 'chap-shortcodes'),
		];

		$this->description = '';

		$this->arg_docs = [
			'classes' => [
				'size' => ['tiny', 'small', 'medium', 'large', 'huge'],
				'color' => ['red', 'green', 'blue', '...'],
				'float' => ['left floated', 'right floated'],
				'alignment' => ['left aligned', 'right aligned', 'justified', 'center aligned'],
				'attachment' => ['top attached', 'attached', 'bottom attached'],
				'icon' => ['icon'],
				'other' => ['dividing', 'block', 'inverted', 'disabled'],
			],
			'icon' => esc_html__('Name of the icon to use.', 'chap-shortcodes'),
			'image' => esc_html__('Link to image to use.', 'chap-shortcodes'),
			'image_size' => esc_html__('Image size.', 'chap-shortcodes'),
			'alt' => esc_html__('Alternate text for the image.', 'chap-shortcodes'),
			'tag' => esc_html__('HTML header tag.', 'chap-shortcodes'),
		];

		$this->presets[esc_html__('HTML header tag', 'chap-shortcodes')] = [
			'tag' => 'h1',
		];

		$this->presets[esc_html__('Relative size header', 'chap-shortcodes')] = [
			'large',
		];

		parent::__construct($id, $options);

	}

	public function render_shortcode() {

		/**
		 * Add to Table of Contents.
		 *
		 * @since 1.0.3
		 */
		if($this->has_arg('id') && isset($GLOBALS['csc-toc'])) {
			$GLOBALS['csc-toc'][] = [
				'id' => $this->args['id'],
				'level' => $this->get_header_level(),
				'content' => $this->content,
			];
		}

		if($this->has_arg('icon')) {
			$icon = do_shortcode('[csc-icon ' . $this->args['icon'] . ']');
			$this->content = $icon . '<div class="content">' . $this->content . '</div>';
		}

		if($this->has_arg('image') && !Helpers\is_amp()) {
			$image_class = 'ui ' . $this->args['image_size'];
			$image_class .= ' image';
			$image = '<img class="' . $image_class . '" src="' . $this->args['image'] . '" alt="' . $this->args['alt'] . '" />';
			$this->content = $image . '<div class="content">' . $this->content . '</div>';
		}

		$this->prepend_class('ui');
		$this->add_class('header');

		/**
		 * Force an HTML tag for AMP pages.
		 */
		if(Helpers\is_amp() && !$this->has_arg('tag')) {
			if($this->has_class('huge')) {
				$this->args['tag'] = 'h1';
			} elseif($this->has_class('large')) {
				$this->args['tag'] = 'h2';
			} elseif($this->has_class('small')) {
				$this->args['tag'] = 'h4';
			} elseif($this->has_class('tiny')) {
				$this->args['tag'] = 'h5';
			} else {
				$this->args['tag'] = 'h3';
			}
		}

		if($this->has_arg('tag')) {
			return $this->html_tag($this->args['tag']);
		} elseif(!$this->has_arg('href') && !isset($this->args['role'])) {
			$this->atts['role'] = 'heading';
			$this->atts['aria-level'] = $this->get_header_level();
		}

		return $this->html_tag('div');

	}

	protected function get_header_level() {
		$level = 3;

		if($this->has_arg('tag') && in_array($this->args['tag'], ['h1', 'h2', 'h3', 'h4', 'h5', 'h6'])) {
			$level = intval(str_replace('h', '', $this->args['tag']));
		} elseif($this->has_class('huge')) {
			$level = 1;
		} elseif($this->has_class('large')) {
			$level = 2;
		} elseif($this->has_class('small')) {
			$level = 4;
		} elseif($this->has_class('tiny')) {
			$level = 5;
		}

		return $level;
	}

}

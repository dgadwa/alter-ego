<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Shortcode_Steps extends Chap_Shortcode {

	public function __construct($id, $options) {

		$this->description = esc_html__('Wrapper for multiple steps.', 'chap-shortcodes');

		$this->arg_docs = [
			'classes' => [
				'orintation' => ['vertical'],
				'responsive' => ['tablet stackable'],
				'attached' => ['top attached', 'attached', 'bottom attached'],
				'count' => ['one', 'two', 'three', '...'],
				'size' => ['mini', 'tiny', 'small', 'large', 'big', 'huge', 'massive'],
				'other' => ['fluid'],
			],
		];

		parent::__construct($id, $options);

	}

	public function render_shortcode() {

		$this->prepend_class('ui');
		$this->add_class('steps');

		return $this->html_tag('div');

	}

}

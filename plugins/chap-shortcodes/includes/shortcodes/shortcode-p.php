<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Shortcode_P extends Chap_Shortcode {

	public function __construct($id, $options) {

		$this->description = esc_html__('HTML &lt;p&gt; tag as a shortcode. Can be used to avoid WP autop.', 'chap-shortcodes');

		$this->presets[esc_html__('With CSS classes', 'chap-shortcodes')] = [
			'class1',
			'class2',
		];

		parent::__construct($id, $options);

	}

	public function render_shortcode() {

		return $this->html_tag('p');

	}

}

<?php

namespace Chap\Shortcodes;
use Chap\Helpers;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Shortcode_Item extends Chap_Shortcode {

	public function __construct($id, $options) {

		$this->default_args = [
			'href' => '',
			'alt' => esc_html__('List image', 'chap-shortcodes'),
		];

		$this->description = esc_html__('Menu or list item.', 'chap-shortcodes');

		$this->arg_docs = [
			'classes' => [
				'state' => ['active', 'disabled'],
				'menu' => ['dropdown'],
			],
			'href' => esc_html__('The href/url/link argument can be used to specify a link.', 'chap-shortcodes'),
			'icon' => esc_html__('Name of the icon to use.', 'chap-shortcodes'),
			'header' => esc_html__('Lists only: Header text.', 'chap-shortcodes'),
			'image' => esc_html__('Lists only: Image link to use.', 'chap-shortcodes'),
			'alt' => esc_html__('Lists only: Alternative text for the image.', 'chap-shortcodes'),
			'active' => esc_html('Wildcard pattern to match with the current URL. If match is found .active class is added.'),
		];

		$this->presets[esc_html__('Menu item', 'chap-shortcodes')] = [
			'href' => '#',
		];

		parent::__construct($id, $options);

	}

	/**
	 * Wrap item in link and unset link.
	 */
	protected function maybe_linkify($content) {
		if($this->has_arg('href')) {
			$target = $this->has_arg('target') ? ' target="' . $this->args['target'] . '"' : '';
			$content = '<a href="' . esc_url($this->args['href']) . '"' . $target . '>' . $content . '</a>';
			unset($this->args['href']);
			unset($this->args['target']);
		}
		return $content;
	}

	/**
	 * Wrap the content.
	 */
	protected function item_content($content) {

		$is_list = isset($GLOBALS['csc']['list']) && $GLOBALS['csc']['list'];
		$is_amp = Helpers\is_amp();

		if($is_amp && $is_list) {
			$content = $this->maybe_linkify($content);
		}

		return '<div class="middle aligned content">' . $content . '</div>';

	}

	public function render_shortcode() {

		$is_list = isset($GLOBALS['csc']['list']) && $GLOBALS['csc']['list'];
		$is_amp = Helpers\is_amp();

		if($this->has_arg('header')) {

			$header = '<div class="header">' . $this->args['header'] . '</div>';
			$description = '<div class="description">' . $this->content . '</div>';

			$this->content = $this->item_content($header . $description);

		}

		$prepend = '';

		if($this->has_arg('icon')) {
			$prepend = do_shortcode('[csc-icon ' . $this->args['icon'] . ']');
		} elseif($this->has_arg('image') && !$is_amp) {
			$prepend = '<img class="ui avatar image" src="' . esc_url($this->args['image']) . '" alt="' . esc_attr($this->args['alt']) . '" />';
		}

		if(!empty($prepend)) {
			if($this->has_arg('header')) {
				$this->content = $prepend . $this->content;
			} else {
				$this->content = $prepend . $this->item_content($this->content);
			}
		}

		$this->add_class('item');

		$tag = 'div';
		if($is_amp && $is_list) {
			// $tag = 'li';
			$this->content = $this->maybe_linkify($this->content);
		}

		if($is_amp && !$is_list && $this->has_class('disabled')) {
			unset($this->args['href']);
		}

		/**
		 * Maybe make active.
		 */
		if($this->has_arg('active') && !$this->has_class('active')) {
			global $wp;
			$home_url = rtrim(home_url(), '/');
			$current_url = rtrim(home_url(add_query_arg([], $wp->request)), '/');
			$pattern = str_replace(
				[
					'%home%',
					'%href%',
					'%url%',  // alias for href
					'%link%', // alias for href
				],
				[
					$home_url,
					$this->args['href'],
					$this->args['href'],
					$this->args['href'],
				],
				$this->args['active']
			);
			$pattern = rtrim($pattern, '/');
			if(fnmatch($pattern, $current_url)) {
				$this->add_class('active');
			}
		}

		/**
		 * Add ARIA role to div list items and menu items.
		 */
		if(!isset($this->atts['role'])) {
			if($is_list) {
				if($tag === 'div') {
					$this->atts['role'] = 'listitem';
				}
			} else {
				$this->atts['role'] = 'menuitem';
			}
		}

		/**
		 * Role attribute is not W3C compliant for <a> tags.
		 */
		if($is_list && !$this->has_arg('header') && $this->has_arg('href')) {
			unset($this->atts['role']);
		}

		return $this->html_tag($tag);

	}

}

<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Shortcode_Flag extends Chap_Shortcode {

	public function __construct($id, $options) {

		$this->description = esc_html__('A flag can use the two digit country code, the full name, or a common alias.', 'chap-shortcodes');

		$this->arg_docs = [
			'classes' => [
				'code' => ['au', 'ca', 'us', 'gb', '...'],
				'name' => ['australia', 'canada', 'united states', 'united kingdom', '...'],
				'alias' => ['united arab emirates', 'svalbard', 'america', '...'],
			],
		];

		parent::__construct($id, $options);

	}

	public function render_shortcode() {

		$this->add_class('flag');

		return $this->html_tag('i');

	}

}

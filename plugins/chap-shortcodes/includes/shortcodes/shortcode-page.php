<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Shortcode_Page extends Chap_Shortcode {

	public function __construct($id, $options) {

		$this->has_content = false;

		$this->default_args = [
			'wpautop' => 'true',
		];

		$this->description = esc_html__('Render the contents of a page.', 'chap-shortcodes');

		$this->arg_docs = [
			'id' => esc_html__('ID of the page to render.', 'chap-shortcodes'),
			'title' => esc_html__('Title of the page to render.', 'chap-shortcodes'),
			'path' => esc_html__('Path of the page to render.', 'chap-shortcodes'),
			'wpautop' => esc_html__('Automatically add paragraphs (default: true).', 'chap-shortcodes'),
		];

		$this->presets[esc_html__('Display page by ID', 'chap-shortcodes')] = [
			'1234',
		];

		$this->presets[esc_html__('Display page by title', 'chap-shortcodes')] = [
			'title' => 'Page title',
		];

		$this->presets[esc_html__('Display page by path', 'chap-shortcodes')] = [
			'path' => '/page-slug',
		];

		$this->presets[esc_html__('Disable WPAUTOP', 'chap-shortcodes')] = [
			'1234',
			'wpautop' => 'false',
		];

		parent::__construct($id, $options);

	}

	public function render_shortcode() {

		if(isset($this->args['title'])) {
			// Get by title
			$page = get_page_by_title($this->args['title']);
		} elseif(isset($this->args['path'])) {
			// Get by path
			$page = get_page_by_path($this->args['path']);
		} else {
			// Get by ID
			$id = isset($this->args['id']) ? (int)$this->args['id'] : (int)$this->args['classes'];
			if(!isset($id) || empty($id) || $id === 0) {
				return;
			}
			$page = get_post($id);
		}

		if(!isset($page->post_content)) {
			return;
		}

		$content = $page->post_content;
		if($this->args['wpautop'] === 'true' || $this->args['wpautop'] == 1) {
			$content = wpautop($page->post_content);
		}

		return do_shortcode($content);

	}

}

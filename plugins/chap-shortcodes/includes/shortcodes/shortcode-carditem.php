<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Shortcode_Carditem extends Chap_Shortcode {

	public function __construct($id, $options) {

		$this->default_args = [
			'icon' => '',
			'description' => '',
		];

		$this->description = esc_html__('Content inside a card.', 'chap-shortcodes');

		$this->arg_docs = [
			'icon' => esc_html__('Name of the icon to use.', 'chap-shortcodes'),
			'content' => esc_html__('Item text.', 'chap-shortcodes'),
			'description' => esc_html__('Subtext under the item.', 'chap-shortcodes'),
		];

		$this->presets[esc_html__('Item with an icon', 'chap-shortcodes')] = [
			'icon' => 'check',
		];

		$this->presets[esc_html__('Item with a description', 'chap-shortcodes')] = [
			'description' => 'Description',
		];

		parent::__construct($id, $options);

	}

	public function render_shortcode() {

		$icon = $this->has_arg('icon') ? '<i class="' . $this->args['icon'] . ' icon"></i>' : '';
		$meta = $this->has_arg('description') ? '<div class="meta">' . $this->args['description'] . '</div>' : '';

		$this->content = '<div class="header">' . $icon . $this->content . '</div>' . $meta;

		$this->add_class('content');

		return $this->html_tag('div');

	}

}

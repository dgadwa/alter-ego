<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Shortcode_Images extends Chap_Shortcode {

	public function __construct($id, $options) {

		$this->description = esc_html__('Wrapper for multiple images.', 'chap-shortcodes');

		$this->arg_docs = [
			'classes' => [
				'size' => ['mini', 'tiny', 'small', 'medium', 'large', 'big', 'huge', 'massive'],
			],
		];

		parent::__construct($id, $options);

	}

	public function render_shortcode() {

		$this->prepend_class('ui');
		$this->add_class('images');

		return $this->html_tag('div');

	}

}

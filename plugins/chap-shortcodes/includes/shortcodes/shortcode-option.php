<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Shortcode_Option extends Chap_Shortcode {

	public function __construct($id, $options) {

		$this->default_args = [
			'value' => '',
		];

		$this->description = esc_html__('Select option.', 'chap-shortcodes');

		$this->arg_docs = [
			'value' => esc_html__('Option value.', 'chap-shortcodes'),
			'classes' => [
				'state' => ['selected'],
			],
		];

		// $this->presets['Preset'] = [
		// 	'class',
		// ];

		parent::__construct($id, $options);

	}

	public function render_shortcode() {

		if($this->has_class('selected')) {
			$this->atts['selected'] = 'selected';
		}

		unset($this->atts['classes']);

		return $this->html_tag('option');

	}

}

<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Shortcode_Segments extends Chap_Shortcode {

	public function __construct($id, $options) {

		$this->description = esc_html__('Wrapper for combining multiple segments.', 'chap-shortcodes');

		parent::__construct($id, $options);

	}

	public function render_shortcode() {

		$this->prepend_class('ui');
		$this->add_class('segments');

		return $this->html_tag('div');

	}

}

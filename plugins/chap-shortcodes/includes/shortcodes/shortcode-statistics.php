<?php

namespace Chap\Shortcodes;
use Chap\Helpers;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Shortcode_Statistics extends Chap_Shortcode {

	public function __construct($id, $options) {

		$this->description = esc_html__('Wrapper for multiple statistics.', 'chap-shortcodes');

		$this->arg_docs = [
			'classes' => [
				'color' => ['red', 'green', 'blue', '...'],
				'orientation' => ['horizontal'],
				'count' => ['one', 'two', 'three', '...'],
			],
		];

		parent::__construct($id, $options);

	}

	public function render_shortcode() {

		/**
		 * Replace number classes with evenly divided in AMP.
		 */
		if(Helpers\is_amp() && count($this->atts['classes']) > 0) {
			$amp_classes = [];
			foreach($this->atts['classes'] as $i => $class) {
				$amp_classes[] = str_replace([
					'one',
					'two',
					'three',
					'four',
					'five',
					'six',
					'seven',
					'eight',
					'nine',
					'ten',
				], 'evenly divided', $class);
			}
			$this->atts['classes'] = $amp_classes;
		}

		$this->prepend_class('ui');
		$this->add_class('statistics');

		return $this->html_tag('div');

	}

}

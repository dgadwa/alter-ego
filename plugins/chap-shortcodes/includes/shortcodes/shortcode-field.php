<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Shortcode_Field extends Chap_Shortcode {

	public function __construct($id, $options) {

		$this->description = esc_html__('Form field.', 'chap-shortcodes');

		$this->arg_docs = [
			'classes' => [
				'state' => ['disabled', 'error'],
				'width' => ['three wide', 'ten wide', '...'],
				'other' => ['required'],
			],
			'label' => esc_html__('Label text for the field', 'chap-shortcodes'),
		];

		parent::__construct($id, $options);

	}

	public function render_shortcode() {

		$this->prepend_class('field');

		if($this->has_arg('label')) {
			$this->content = '<label>' . $this->args['label'] . '</label>' . $this->content;
		}

		return $this->html_tag('div');

	}

}

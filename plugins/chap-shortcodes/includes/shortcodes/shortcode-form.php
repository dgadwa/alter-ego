<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Shortcode_Form extends Chap_Shortcode {

	public function __construct($id, $options) {

		$this->default_args = [
			'method' => 'get',
		];

		$this->description = esc_html__('HTML form.', 'chap-shortcodes');

		$this->arg_docs = [
			'id' => 'ID of the form.',
			'name' => 'Name of the form.',
			'method' => 'Form method: get/post.',
			'action' => 'Form action.',
			'classes' => [
				'size' => ['mini', 'small', 'medium', 'large', 'big', 'huge', 'massive'],
				'fields' => ['equal width'],
				'state' => ['loading', 'success', 'error', 'warning'],
				'other' => ['inverted'],
			],
		];

		// $this->presets['Preset'] = [
		// 	'class',
		// ];

		parent::__construct($id, $options);

	}

	public function render_shortcode() {

		$this->prepend_class('ui');
		$this->add_class('form');

		return $this->html_tag('form');

	}

}

<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Shortcode_Address extends Chap_Shortcode {

	public function __construct($id, $options) {

		$this->description = esc_html__('HTML &lt;address&gt; tag as a shortcode.', 'chap-shortcodes');

		parent::__construct($id, $options);

	}

	public function render_shortcode() {

		return $this->html_tag('address');

	}

}

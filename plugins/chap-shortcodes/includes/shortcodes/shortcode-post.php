<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Shortcode_Post extends Chap_Shortcode {

	public function __construct($id, $options) {

		$this->has_content = false;

		$this->default_args = [
			'wpautop' => 'true',
		];

		$this->description = esc_html__('Render the contents of a post.', 'chap-shortcodes');

		$this->arg_docs = [
			'id' => esc_html__('ID of the post to render.', 'chap-shortcodes'),
			'title' => esc_html__('Title of the post to render.', 'chap-shortcodes'),
			'wpautop' => esc_html__('Automatically add paragraphs (default: true).', 'chap-shortcodes'),
		];

		$this->presets[esc_html__('Display post by ID', 'chap-shortcodes')] = [
			'1234',
		];

		$this->presets[esc_html__('Display post by title', 'chap-shortcodes')] = [
			'title' => 'Post title',
		];

		$this->presets[esc_html__('Disable WPAUTOP', 'chap-shortcodes')] = [
			'1234',
			'wpautop' => 'false',
		];

		parent::__construct($id, $options);

	}

	public function render_shortcode() {

		if(isset($this->args['title'])) {
			// Get by title
			$post = get_page_by_title($this->args['title'], 'OBJECT', 'post');
		} else {
			// Get by ID
			$id = isset($this->args['id']) ? (int)$this->args['id'] : (int)$this->args['classes'];
			if(!isset($id) || empty($id) || $id === 0) {
				return;
			}
			$post = get_post($id);
		}

		if(!isset($post->post_content)) {
			return;
		}

		$content = $post->post_content;
		if($this->args['wpautop'] === 'true' || $this->args['wpautop'] == 1) {
			$content = wpautop($post->post_content);
		}

		return do_shortcode($content);

	}

}

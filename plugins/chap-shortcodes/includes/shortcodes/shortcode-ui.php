<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Shortcode_Ui extends Chap_Shortcode {

	public function __construct($id, $options) {

		$this->description = esc_html__('Renders a basic HTML &lt;div&gt; tag (or &lt;a&gt;, if link is specified) with .ui class already prepended. Can be used as a substitute for any UI shortcode when it\'s needed to nest the same shortcode inside itself.', 'chap-shortcodes');

		$this->presets[esc_html__('With additional CSS classes', 'chap-shortcodes')] = [
			'class1',
			'class2',
		];

		$this->presets[esc_html__('Container shortcode alternative', 'chap-shortcodes')] = [
			'text',
			'container',
		];

		$this->presets[esc_html__('List shortcode alternative', 'chap-shortcodes')] = [
			'list',
		];

		parent::__construct($id, $options);

	}

	public function render_shortcode() {

		$this->prepend_class('ui');

		return $this->html_tag('div');

	}

}

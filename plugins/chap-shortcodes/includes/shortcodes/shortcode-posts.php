<?php

namespace Chap\Shortcodes;
use Chap\Helpers;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Shortcode_Posts extends Chap_Shortcode {

	protected $name;
	protected $pagination_index;
	protected $pagination_id;
	protected $query;

	public function __construct($id, $options) {

		$this->default_args = [
			'query' => 'posts_per_page=3',
			'template' => '',
			'next' => esc_html__('Older posts', 'chap-shortcodes'),
			'prev' => esc_html__('Newer posts', 'chap-shortcodes'),
			'pagination_alignment' => 'center',
			'pagination_menu' => '',
			'pagination_container' => '',
			'excerpt' => 55,
			'thumbnail' => 'medium',
			'header' => 'small',
			'meta' => true,
			'tags' => true,
			'read_more' => true,
		];

		$this->has_content = false;

		$this->description = esc_html__('Display WordPress posts.', 'chap-shortcodes');

		$this->arg_docs = [
			'classes' => [
				'type' => ['items', 'grid', 'cards'],
				'items' => ['compact', 'very compact', 'divided', 'relaxed', 'very relaxed'],
				'grid' => ['divided', 'vertically divided', 'celled', 'internally celled', 'relaxed', 'very relaxed'],
				'cards' => ['raised', 'stackable', 'doubling', 'centered', 'fluid', 'red', 'green', 'blue', '...'],
			],
			'query' => esc_html__('WordPress query string.', 'chap-shortcodes'),
			'template' => esc_html__('Custom template name.', 'chap-shortcodes'),
			'excerpt' => esc_html__('Custom excerpt length (number of words). Set to 0 to disable excerpts. (Default: 55)', 'chap-shortcodes'),
			'thumbnail' => esc_html__('Custom thumbnail image size (none|thumbnail|small|medium|large|full). (Default: medium)', 'chap-shortcodes'),
			'header' => esc_html__('Posts header size (tiny|small|medium|large|huge). (Default: small)', 'chap-shortcodes'),
			'meta' => esc_html__('Set to false to disable meta (date and author).', 'chap-shortcodes'),
			'tags' => esc_html__('Set to false to disable tags (categories, tags and sticky post indicator).', 'chap-shortcodes'),
			'read_more' => esc_html__('Set to false to disable the "Read more" link.', 'chap-shortcodes'),
			'pagination' => esc_html__('Set to true to enable pagination.', 'chap-shortcodes'),
			'pagination_alignment' => esc_html__('Pagination alignment (left|center|right).', 'chap-shortcodes'),
			'pagination_menu' => esc_html__('Additional classes for the pagination menu.', 'chap-shortcodes'),
			'next' => esc_html__('Pagination next posts link text (default: "Older posts").', 'chap-shortcodes'),
			'prev' => esc_html__('Pagination previous posts link text (default: "Newer posts").', 'chap-shortcodes'),
		];

		$this->presets[esc_html__('Posts with query', 'chap-shortcodes')] = [
			'items',
			'query' => 'category_name=category&posts_per_page=3&order=ASC',
		];

		$this->presets[esc_html__('Posts with pagination', 'chap-shortcodes')] = [
			'items',
			'query' => 'posts_per_page=5',
			'pagination' => 'true',
		];

		$this->name = $id;
		$this->pagination_index = 0;

		parent::__construct($id, $options);

	}

	public function render_shortcode() {

		if(isset($this->args['thumbnail_size'])) {
			$this->args['thumbnail'] = $this->args['thumbnail_size'];
		}

		$is_grid = $this->has_class('grid');
		$is_cards = $this->has_class('cards');
		$is_slider = $GLOBALS['csc']['slider'];

		// Load AMP styles.
		if(Helpers\is_amp()) {
			if($is_grid) {
				do_shortcode('[csc-grid]');
			}
			if($is_cards) {
				do_shortcode('[csc-cards]');
				do_shortcode('[csc-card]');
			}
		}

		/**
		 * Use custom thumbnail size.
		 */
		if($this->has_arg('thumbnail')) {
			$size = $this->args['thumbnail'];
			$thumbnail_filter = function() use ($size) {
				return $size;
			};
			add_filter('chap_option_loop_thumbnail_size', $thumbnail_filter);
		}

		/**
		 * Use custom header size.
		 */
		if($this->has_arg('header')) {
			$size = $this->args['header'];
			$header_filter = function() use ($size) {
				return $size;
			};
			add_filter('chap_option_loop_header_size', $header_filter);
		}

		/**
		 * Don't render meta.
		 */
		$render_meta = $this->args['meta'] === 'false' ? false : true;
		$render_meta_filter = function() use ($render_meta) {
			return $render_meta;
		};
		add_filter('chap_option_loop_render_entry_meta', $render_meta_filter);

		/**
		 * Use custom excerpt length.
		 */
		if($this->has_arg('excerpt')) {
			$length = intval($this->args['excerpt']);
			$excerpt_filter = function() use ($length) {
				return $length;
			};
			add_filter('excerpt_length', $excerpt_filter);
		}

		/**
		 * Don't render tags/extras.
		 */
		$render_tags = $this->args['tags'] === 'false' ? false : true;
		$render_tags_filter = function() use ($render_tags) {
			return $render_tags;
		};
		add_filter('chap_option_loop_render_extras', $render_tags_filter);

		/**
		 * Don't render read more.
		 */
		$render_read_more = $this->args['read_more'] === 'false' ? false : true;
		$render_read_more_filter = function() use ($render_read_more) {
			return $render_read_more;
		};
		add_filter('chap_option_loop_render_read_more', $render_read_more_filter);

		/**
		 * Never use justified for grid columns.
		 * Columns can be justified by using a .justified.grid istead.
		 */
		$grid_columns_justified_filter = function() {
			return false;
		};
		add_filter('chap_option_loop_grid_justified', $grid_columns_justified_filter);

		/**
		 * Get grid column count int from classes.
		 */
		if($is_grid) {
			for($i = 1; $i <= 16; $i++) {
				$class = Helpers\number_to_word($i) . ' column';
				if($this->has_class($class)) {
					$columns = $i;
					break;
				}
			}
			if(!isset($columns)) {
				$columns = 3;
				$this->prepend_class('column');
				$this->prepend_class('three');
			}
		}

		/**
		 * Add default cards count if none set.
		 */
		if($is_cards) {
			$has_count = false;
			for($i = 1; $i <= 16; $i++) {
				$class = Helpers\number_to_word($i);
				if($this->has_class($class)) {
					$has_count = true;
					break;
				}
			}
			if(!$has_count) {
				$this->prepend_class('three');
			}
		}

		/**
		 * Choose a default template.
		 */
		if(!$this->has_arg('template')) {
			if($is_grid) {
				$this->args['template'] = 'templates/loop-column';
			} elseif($is_cards) {
				$this->args['template'] = 'templates/loop-card';
			} else {
				$this->args['template'] = 'templates/loop-item';
			}
		}

		/**
		 * Pagination.
		 */
		if($this->has_arg('pagination')) {

			/**
			 * Increment the pagination index and assign the loop an ID.
			 */
			$this->pagination_index++;
			$this->pagination_id = $this->name . '_' . $this->pagination_index . '_page';
			if(!isset($this->atts['id'])) {
				$this->atts['id'] = $this->pagination_id;
			}

			/**
			 * Add paged query var to WP_Query.
			 */
			$page = isset($_GET[$this->pagination_id]) ? (int)$_GET[$this->pagination_id] : 1;
			$pagination = 'paged=' . $page;
			$this->args['query'] = strlen($this->args['query']) > 0 ? $this->args['query'] . '&' . $pagination : $pagination;

		}

		/**
		 * WP Query.
		 */
		$this->query = new \WP_Query($this->args['query']);

		/**
		 * Loop.
		 */
		if($this->query->have_posts()) {

			ob_start();

			$count = 0;

			if($is_grid && !$is_slider) {
				$GLOBALS['chap_grid']['columns'] = $columns;
				$GLOBALS['chap_grid']['rendered'] = 0;
				echo '<div class="row">';
			}

			while($this->query->have_posts()) {
				$count++;
				$this->query->the_post();
				get_template_part($this->args['template']);
			}

			if($is_grid && !$is_slider) {
				echo '</div>'; // Close .row

				// Reset to default.
				$GLOBALS['chap_grid']['columns'] = 3;
				$GLOBALS['chap_grid']['rendered'] = 0;
			}

			$this->content = ob_get_clean();

		}

		$this->prepend_class('ui');
		$this->add_class('loop');

		/**
		 * Backwards compatibility.
		 */
		if(!$is_grid && !$is_cards && !$this->has_class('items')) {
			$this->add_class('items');
		}

		/**
		 * Remove thumbnail size filter.
		 */
		if($this->has_arg('thumbnail')) {
			remove_filter('chap_option_loop_thumbnail_size', $thumbnail_filter);
		}

		/**
		 * Remove header size filter.
		 */
		if($this->has_arg('header')) {
			remove_filter('chap_option_loop_header_size', $header_filter);
		}

		/**
		 * Remove don't render meta filter.
		 */
		remove_filter('chap_option_loop_render_entry_meta', $render_meta_filter);

		/**
		 * Remove excerpt filter.
		 */
		if($this->has_arg('excerpt')) {
			remove_filter('excerpt_length', $excerpt_filter);
		}

		/**
		 * Remove don't render tags filter.
		 */
		remove_filter('chap_option_loop_render_extras', $render_tags_filter);

		/**
		 * Remove don't render read_more filter.
		 */
		remove_filter('chap_option_loop_render_read_more', $render_read_more_filter);

		/**
		 * Remove grid columns justified filter.
		 */
		remove_filter('chap_option_loop_grid_justified', $grid_columns_justified_filter);

		$pagination = '';
		if($this->has_arg('pagination') && $this->query->max_num_pages > 1) {
			/**
			 * Render as shortcode so dependancies such as
			 * AMP CSS for grids will be loaded automatically.
			 */
			$pagination = do_shortcode(
				'[csc-grid center aligned ' . $this->args['pagination_container'] . ']' .
					'[csc-column ' . $this->args['pagination_alignment'] . ' aligned]' .
						'[csc-menu ' . $this->args['pagination_menu'] . ' pagination]' .
							$this->get_next_page_link() .
							$this->get_previous_page_link() .
						'[/csc-menu]' .
					'[/csc-column]' .
				'[/csc-grid]'
			);
		}

		wp_reset_postdata();

		if($is_slider) {
			return $this->content;
		}

		return $this->html_tag('div') . $pagination;

	}

	protected function get_next_page_link() {

		$max_page = (int)$this->query->max_num_pages;
		if($max_page <= 1) {
			return;
		}

		$page = isset($_GET[$this->pagination_id]) ? (int)$_GET[$this->pagination_id] : 1;
		$next_page = $page + 1;
		if($next_page < 1 || $next_page > $max_page) {
			return;
		}

		return sprintf(
			'<a class="nav-next item" href="%1$s#%2$s">%3$s</a>',
			esc_url(add_query_arg([$this->pagination_id => $next_page])),
			esc_attr($this->atts['id']),
			$this->args['next']
		);

	}

	protected function get_previous_page_link() {

		$max_page = (int)$this->query->max_num_pages;
		if($max_page <= 1) {
			return;
		}

		$page = isset($_GET[$this->pagination_id]) ? (int)$_GET[$this->pagination_id] : 1;
		$prev_page = $page - 1;
		if($prev_page < 1 || $prev_page > $max_page) {
			return;
		}

		return sprintf(
			'<a class="nav-previous item" href="%1$s#%2$s">%3$s</a>',
			esc_url(add_query_arg([$this->pagination_id => $prev_page])),
			esc_attr($this->atts['id']),
			$this->args['prev']
		);

	}

}

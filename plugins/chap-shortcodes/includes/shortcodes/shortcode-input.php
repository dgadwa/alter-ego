<?php

namespace Chap\Shortcodes;
use Chap\Helpers;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Shortcode_Input extends Chap_Shortcode {

	public function __construct($id, $options) {

		$this->default_args = [
			'type' => 'text',
		];

		$this->description = esc_html__('Input field.', 'chap-shortcodes');

		$this->arg_docs = [
			'id' => esc_html__('ID of the input.', 'chap-shortcodes'),
			'type' => esc_html__('Input type (default: "text").', 'chap-shortcodes'),
			'name' => esc_html__('Name of the input.', 'chap-shortcodes'),
			'placeholder' => esc_html__('Input placeholder text.', 'chap-shortcodes'),
			'value' => esc_html__('Input value.', 'chap-shortcodes'),
			'classes' => [
				'size' => ['mini', 'small', 'medium', 'large', 'big', 'huge', 'massive'],
				'label' => ['labeled', 'right labeled', 'corner labeled', 'left corner labeled'],
				'icon' => ['icon', 'left icon'],
				'action' => ['action', 'left action'],
				'other' => ['styled', 'inverted', 'transparent', 'fluid', 'loading', 'focus', 'disabled', 'error'],
			],
			'icon' => esc_html__('Name of the icon to use.', 'chap-shortcodes'),
			'label' => esc_html__('Label text.', 'chap-shortcodes'),
			'required' => esc_html__('Set to "true" to mark as required field.', 'chap-shortcodes'),
		];

		// $this->presets['Preset'] = [
		// 	'class',
		// ];

		parent::__construct($id, $options);

	}

	/**
	 * Create a separate HTML input and remove atts from main container.
	 */
	protected function input() {

		$atts = [
			'type' => $this->atts['type'],
		];

		unset($this->atts['type']);

		if($this->has_arg('id')) {
			$atts['id'] = $this->atts['id'];
			unset($this->atts['id']);
		}

		if($this->has_arg('name')) {
			$atts['name'] = $this->atts['name'];
			unset($this->atts['name']);
		}

		if($this->has_arg('placeholder')) {
			$atts['placeholder'] = $this->atts['placeholder'];
			unset($this->atts['placeholder']);
		}

		if($this->has_arg('value')) {
			$atts['value'] = $this->atts['value'];
			unset($this->atts['value']);
		}

		if($this->has_class('required') && !$this->has_arg('required')) {
			$atts['required'] = '';
		}

		return Helpers\html_tag('input', $atts, true);

	}

	public function render_shortcode() {

		/**
		 * Render unstyled HTML input tag.
		 */
		if(count($this->atts['classes']) <= 0) {
			unset($this->atts['classes']);
			return $this->html_tag('input', true);
		}

		if($this->has_class_string('left action')) {
			$this->content = $this->content . $this->input();
		} else {
			$this->content = $this->input() . $this->content;
		}

		if($this->has_arg('icon')) {
			$this->content .= do_shortcode('[csc-icon ' . $this->args['icon'] . ']');
		}

		if($this->has_arg('label')) {
			$label = do_shortcode('[csc-label]' . $this->args['label'] . '[/csc-label]');
			if($this->has_class_string('right labeled')) {
				$this->content .= $label;
			} else {
				$this->content = $label . $this->content;
			}
		}

		$this->prepend_class('ui');
		$this->add_class('input');

		return $this->html_tag('div');

	}

}

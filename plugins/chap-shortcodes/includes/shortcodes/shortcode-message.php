<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Shortcode_Message extends Chap_Shortcode {

	public function __construct($id, $options) {

		$this->default_args = [
			'header' => '',
			'icon' => '',
		];

		$this->description = '';

		$this->arg_docs = [
			'classes' => [
				'type' => ['positive', 'negative', 'success', 'info', 'warning', 'error'],
				'size' => ['mini', 'tiny', 'small', 'medium', 'large', 'big', 'huge', 'massive'],
				'color' => ['red', 'green', 'blue', '...'],
				'attach' => ['top attached', 'bottom attached'],
				'other' => ['compact', 'floating'],
			],
			'icon' => esc_html__('Name of the icon to use.', 'chap-shortcodes'),
			'header' => esc_html__('Message header text.', 'chap-shortcodes'),
		];

		$this->presets[esc_html__('Message with a header', 'chap-shortcodes')] = [
			'header' => 'Header',
		];

		$this->presets[esc_html__('Icon message', 'chap-shortcodes')] = [
			'icon',
			'icon' => 'inbox',
		];

		parent::__construct($id, $options);

	}

	public function render_shortcode() {

		$this->prepend_class('ui');
		$this->add_class('message');

		$this->content = $this->content;

		if($this->has_arg('header')) {
			$this->content = '<div class="header">' . $this->args['header'] . '</div>' . $this->content;
		}

		$this->content = '<div class="content">' . $this->content . '</div>';

		if($this->has_arg('icon')) {
			$this->content = do_shortcode('[csc-icon ' . $this->args['icon'] . ']') . $this->content;
		}

		return $this->html_tag('div');

	}

}

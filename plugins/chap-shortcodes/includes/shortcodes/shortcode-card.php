<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Shortcode_Card extends Chap_Shortcode {

	public function __construct($id, $options) {

		$this->default_args = [
			'href' => '',
		];

		$this->arg_docs = [
			'classes' => [
				'emphasis' => ['raised'],
				'color' => ['red', 'green', 'blue', '...'],
				'other' => ['centered', 'fluid'],
			],
			'href' => esc_html__('The href/url/link argument can be used to specify a link.', 'chap-shortcodes'),
		];

		$this->description = esc_html__('A card displays site content in a manner similar to a playing card.', 'chap-shortcodes');

		$this->presets[esc_html__('Link card', 'chap-shortcodes')] = [
			'href' => '#',
		];

		parent::__construct($id, $options);

	}

	public function render_shortcode() {

		$this->add_class('card');

		if($this->has_arg('href')) {
			$this->prepend_class('link');
		}

		$this->prepend_class('ui');

		return $this->html_tag('div');

	}

}

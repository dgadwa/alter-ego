<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Shortcode_Fields extends Chap_Shortcode {

	public function __construct($id, $options) {

		$this->description = esc_html__('Form fields container.', 'chap-shortcodes');

		$this->arg_docs = [
			'classes' => [
				'count' => ['one', 'two', 'three', '...'],
				'other' => ['inline', 'grouped', 'equal width'],
			],
		];

		parent::__construct($id, $options);

	}

	public function render_shortcode() {

		$this->add_class('fields');

		return $this->html_tag('div');

	}

}

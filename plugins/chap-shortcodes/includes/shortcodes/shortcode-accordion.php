<?php

namespace Chap\Shortcodes;
use Chap\Helpers;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Shortcode_Accordion extends Chap_Shortcode {

	public function __construct($id, $options) {

		$this->description = esc_html__('Accordion wrapper. Use [content] shortcode to add content inside accordion.', 'chap-shortcodes');

		$this->arg_docs = [
			'classes' => [
				'style' => ['styled', 'inverted'],
				'width' => ['fluid'],
			],
		];

		$this->presets[esc_html__('Styled accordion', 'chap-shortcodes')] = [
			'styled',
		];

		$this->presets[esc_html__('Full width accordion', 'chap-shortcodes')] = [
			'fluid',
		];

		parent::__construct($id, $options);

	}

	public function render_shortcode() {

		$this->add_script('accordion');
		$this->add_amp_script('amp-accordion');

		$this->prepend_class('ui');
		$this->add_class('accordion');

		if(Helpers\is_amp()) {
			$output = $this->html_tag('amp-accordion');
			return str_replace('<amp-accordion', '<amp-accordion disable-session-states', $output);
		}

		return $this->html_tag('div');

	}

}

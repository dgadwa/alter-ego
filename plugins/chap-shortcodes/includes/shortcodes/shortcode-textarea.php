<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Shortcode_Textarea extends Chap_Shortcode {

	public function __construct($id, $options) {

		$this->default_args = [
			'rows' => '',
		];

		$this->description = '';

		$this->arg_docs = [
			'id' => esc_html__('ID of the textarea.', 'chap-shortcodes'),
			'name' => esc_html__('Name of the textarea.', 'chap-shortcodes'),
			'rows' => esc_html__('Textarea rows count.', 'chap-shortcodes'),
			'required' => esc_html__('Set to "true" to mark as required field.', 'chap-shortcodes'),
		];

		$this->presets[esc_html__('Short textarea', 'chap-shortcodes')] = [
			'rows' => '2',
		];

		parent::__construct($id, $options);

	}

	public function render_shortcode() {

		if($this->has_arg('rows')) {
			$this->atts['rows'] = $this->args['rows'];
		}

		if($this->has_class('required')) {
			$this->atts['required'] = '';
		}

		return $this->html_tag('textarea');

	}

}

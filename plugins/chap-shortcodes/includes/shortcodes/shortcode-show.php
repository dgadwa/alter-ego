<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Shortcode_Show extends Chap_Shortcode {

	public function __construct($id, $options) {

		$this->default_args = [
			'operator' => 'OR',
		];

		$this->description = esc_html__('Show contents under certain conditions.', 'chap-shortcodes');

		$this->arg_docs = [
			'on' => esc_html__('Match heyword [home|front|blog|singular|sticky|archive|search|rtl|404| wc|shop|cart|checkout|account].', 'chap-shortcodes'),
			'post_type' => esc_html__('Match post type [post|page|product|...].', 'chap-shortcodes'),
			'category' => esc_html__('Match a category name, slug or ID.', 'chap-shortcodes'),
			'tag' => esc_html__('Match current post tags.', 'chap-shortcodes'),
			'tax' => esc_html__('Match current taxonomy.', 'chap-shortcodes'),
			'author' => esc_html__('Match an author name, slug or ID.', 'chap-shortcodes'),
			'single' => esc_html__('Match a single post of any post type by name, slug or ID.', 'chap-shortcodes'),
			'page' => esc_html__('Match a page by name, slug or ID.', 'chap-shortcodes'),
			'path' => esc_html__('Match a path.', 'chap-shortcodes'),
			'wc_endpoint' => esc_html__('Match a WooCommerce endpoint slug [order-pay|order-received|view-order|edit-account|edit-address|lost-password|customer-logout|add-payment-method].', 'chap-shortcodes'),
			'classes' => [
				'AMP' => ['amp-only', 'amp-invisible'],
			],
			'operator' => esc_html__('Operator between multiple arguments [OR|AND|NOT] (default: OR).', 'chap-shortcodes'),
		];

		$this->presets[esc_html__('Show on front page', 'chap-shortcodes')] = [
			'on' => 'front',
		];

		parent::__construct($id, $options);

	}

	public function render_shortcode() {

		// https://codex.wordpress.org/condition_Tags
		// https://docs.woocommerce.com/document/condition-tags/

		$conditions = [];

		if($this->has_arg('on')) {
			$on = explode(',', $this->args['on']);
			if(count($on) > 0) {
				foreach($on as $location) {
					$condition = false;
					switch(trim($location)) {
						case 'home':
							$condition = is_home();
						break;
						case 'front':
							$condition = is_front_page();
						break;
						case 'blog':
							$condition = (is_front_page() && is_home());
						break;
						case 'singular':
							$condition = is_singular();
						break;
						case 'sticky':
							$condition = is_sticky();
						break;
						case 'archive':
							$condition = is_archive();
						break;
						case 'search':
							$condition = is_search();
						break;
						case '404':
							$condition = is_404();
						break;
						case 'rtl':
							$condition = is_rtl();
						break;
						case 'wc':
						case 'woocommerce':
							$condition = is_woocommerce();
						break;
						case 'shop':
							$condition = is_shop();
						break;
						case 'cart':
							$condition = is_cart();
						break;
						case 'checkout':
							$condition = is_checkout();
						break;
						case 'account':
							$condition = is_account_page();
						break;
					}
					$conditions['on'][] = $condition;
				}
			}
		}

		if($this->has_arg('post_type')) {
			$types = explode(',', $this->args['post_type']);
			if(count($types) > 0) {
				foreach($types as $type) {
					$conditions['post_type'][] = (get_post_type() == trim($type));
				}
			}
		}

		if($this->has_arg('category')) {
			$cats = explode(',', $this->args['category']);
			if(count($cats) > 0) {
				foreach($cats as $cat) {
					$conditions['category'][] = in_category(trim($cat));
				}
			}
		}

		if($this->has_arg('tag')) {
			$tags = explode(',', $this->args['tag']);
			if(count($tags) > 0) {
				foreach($tags as $tag) {
					$conditions['tag'][] = has_tag(trim($tag));
				}
			}
		}

		if($this->has_arg('tax')) {
			$taxonomies = explode(',', $this->args['tax']);
			if(count($taxonomies) > 0) {
				foreach($taxonomies as $tax) {
					$conditions['tax'][] = is_tax(trim($tax));
				}
			}
		}

		if($this->has_arg('author')) {
			$authors = explode(',', $this->args['author']);
			if(count($authors) > 0) {
				foreach($authors as $author) {
					$conditions['author'][] = is_author(trim($author));
				}
			}
		}

		if($this->has_arg('single')) {
			$singles = explode(',', $this->args['single']);
			if(count($singles) > 0) {
				foreach($singles as $single) {
					$conditions['single'][] = is_single(trim($single));
				}
			}
		}

		if($this->has_arg('page')) {
			$pages = explode(',', $this->args['page']);
			if(count($pages) > 0) {
				foreach($pages as $page) {
					$conditions['page'][] = is_page(trim($page));
				}
			}
		}

		if($this->has_arg('path')) {
			$paths = explode(',', $this->args['path']);
			if(count($paths) > 0) {
				foreach($paths as $path) {
					$page = get_page_by_path(trim($path));
					$conditions['path'][] = $page && (get_the_ID() == $page->ID);
				}
			}
		}

		if($this->has_arg('wc_endpoint')) {
			$endpoints = explode(',', $this->args['wc_endpoint']);
			if(count($endpoints) > 0) {
				foreach($endpoints as $endpoint) {
					$conditions['wc_endpoint'][] = is_wc_endpoint_url(trim($endpoint));
				}
			}
		}

		// Merge sets of conditions.
		$merged = [];
		foreach($conditions as $set) {
			$merged = array_merge($merged, $set);
		}

		// Operate on conditions.
		switch(strtolower($this->args['operator'])) {
			case 'or':
				if(in_array(true, $merged)) {
					return $this->content;
				}
			break;
			case 'and':
				foreach($conditions as $set) {
					if(!in_array(true, $set)) {
						return;
					}
				}
			break;
			case 'not':
				if(!in_array(true, $merged)) {
					return $this->content;
				}
			break;
		}

		return;

	}

}

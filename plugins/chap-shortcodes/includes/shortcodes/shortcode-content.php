<?php

namespace Chap\Shortcodes;
use Chap\Helpers;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Shortcode_Content extends Chap_Shortcode {

	public function __construct($id, $options) {

		$this->default_args = [
			'title' => '',
		];

		$this->arg_docs = [
			'classes' => [
				'state' => ['active'],
			],
			'title' => esc_html__('The title text.', 'chap-shortcodes'),
		];

		$this->description = esc_html__('Used to display accordion content.', 'chap-shortcodes');

		$this->presets[esc_html__('Content with title', 'chap-shortcodes')] = [
			'title' => 'Title',
		];

		parent::__construct($id, $options);

	}

	public function render_shortcode() {

		$icon = do_shortcode('[csc-icon dropdown]');

		if(Helpers\is_amp()) {

			$title = '<header class="title">' . $icon . $this->args['title'] . '</header>';
			$content = '<div class="content">' . $this->content . '</div>';
			$expanded = $this->has_class('active') ? ' expanded' : '';

			return '<section' . $expanded . '>' . $title . $content . '</section>';

		} else {

			$active = $this->has_class('active') ? ' active' : '';
			$title = '<div class="title' . $active . '">' . $icon . $this->args['title'] . '</div>';
			$content = '<div class="content' . $active . '">' . $this->content . '</div>';

			return $title . $content;

		}

	}

}

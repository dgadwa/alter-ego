<?php

namespace Chap\Shortcodes;
use Chap\Helpers;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Shortcode_List extends Chap_Shortcode {

	public function __construct($id, $options) {

		$this->default_args = [
			'icon' => '',
		];

		$this->description = esc_html__('List. Use [item] shortcode to add items.', 'chap-shortcodes');

		$this->arg_docs = [
			'classes' => [
				'type' => ['bulleted', 'ordered', 'link'],
				'axis' => ['horizontal', 'vertical'],
				'variation' => ['divided', 'celled', 'selection', 'animated', 'relaxed', 'very relaxed'],
				'size' => ['mini', 'tiny', 'small', 'medium', 'large', 'big', 'huge', 'massive'],
				'float' => ['left floated', 'right floated'],
				'other' => ['inverted'],
			],
		];

		parent::__construct($id, $options);

	}

	public function render_shortcode() {

		$this->prepend_class('ui');
		$this->add_class('list');

		if($this->has_class('bulleted')) {
			$this->add_amp_style('list-bulleted');
		}

		if($this->has_class('horizontal')) {
			$this->add_amp_style('list-horizontal');
		}

		if($this->has_class('link')) {
			$this->add_amp_style('list-link');
		}

		if($this->has_class('ordered')) {
			$this->add_amp_style('list-ordered');
		}

		if(!isset($this->atts['role'])) {
			$this->atts['role'] = 'list';
		}

		return $this->html_tag('div');

	}

	/**
	 * Let [item] shortcode know that list items are rendered, not menu items.
	 */
	public function render($args, $content = '') {

		$GLOBALS['csc']['list'] = true;

		$this->parse_args($args, $this->default_args);
		$this->add_amp_style();
		$this->content = do_shortcode($content);
		$this->raw_content = $content;

		$GLOBALS['csc']['list'] = false;

		return $this->render_shortcode();

	}

}

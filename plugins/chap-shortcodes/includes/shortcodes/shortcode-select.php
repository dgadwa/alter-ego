<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Shortcode_Select extends Chap_Shortcode {

	public function __construct($id, $options) {

		$this->default_args = [];

		$this->description = '';

		$this->arg_docs = [
			'id' => esc_html__('ID of the select.', 'chap-shortcodes'),
			'name' => esc_html__('Name of the select.', 'chap-shortcodes'),
			'classes' => [
				'type' => ['multiple'],
				'state' => ['disabled'],
			],
			'required' => esc_html__('Set to "true" to mark as required field.', 'chap-shortcodes'),
		];

		parent::__construct($id, $options);

	}

	public function render_shortcode() {

		$this->add_script('dropdown');

		if($this->has_class('multiple')) {
			$this->atts['multiple'] = '';
		}

		if($this->has_class('required')) {
			$this->atts['required'] = '';
		}

		$this->prepend_class('ui');
		$this->add_classes(['chap-sc', 'dropdown']);

		return $this->html_tag('select');

	}

}

<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Shortcode_Cardextra extends Chap_Shortcode {

	public function __construct($id, $options) {

		$this->description = esc_html__('Extra content inside a card.', 'chap-shortcodes');

		$this->arg_docs = [
			'classes' => [
				'type' => ['content'],
			],
			'content' => 'Extra content for the card.',
		];

		parent::__construct($id, $options);

	}

	public function render_shortcode() {

		$this->prepend_class('extra');

		return $this->html_tag('div');

	}

}

<?php

namespace Chap\Shortcodes;
use Chap\Helpers;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Shortcode_Testimonial extends Chap_Shortcode {

	public function __construct($id, $options) {

		$this->default_args = [
			'name' => '',
			'occupation' => '',
			'image' => '',
		];

		$this->description = '';

		$this->arg_docs = [
			'classes' => [
				'type' => ['basic', 'raised', 'stacked'],
				'emphasis' => ['secondary', 'tertiary'],
				'color' => ['red', 'green', 'blue', '...'],
				'size' => ['mini', 'tiny', 'small', 'medium', 'large', 'big', 'huge', 'massive'],
			],
			'name' => esc_html__('Name.', 'chap-shortcodes'),
			'occupation' => esc_html__('Occupation/subtext under name.', 'chap-shortcodes'),
			'image' => esc_html__('Image next to name.', 'chap-shortcodes'),
		];

		$this->presets[esc_html__('With name, occupation and image', 'chap-shortcodes')] = [
			'name' => 'John Doe',
			'occupation' => 'Company, CEO',
			'image' => '//placehold.it/150',
		];

		parent::__construct($id, $options);

	}

	public function render_shortcode() {

		$this->add_amp_style('segment');
		$this->add_amp_style('p');
		$this->add_amp_style('header');

		$this->content = do_shortcode('[csc-label huge right corner][csc-icon right quote][/csc-label]') . $this->content;

		$header = '';

		if($this->has_arg('name')) {
			if(Helpers\is_amp()) {
				$header .= '<div class="ui right aligned marginless bold text header">';
			} else {
				$inverted = $this->has_class('inverted') ? 'inverted' : 'default';
				$header .= '<div class="ui marginless right floated ' . $inverted . ' image header">';
				if($this->has_arg('image')) {
					$header .= '<img class="ui amp-invisible avatar image" src="' . $this->args['image'] . '" alt="' . $this->args['name'] . '" />';
				}
			}
			$header .= '<div class="content">';
			$header .= $this->args['name'];
			if($this->has_arg('occupation')) {
				$header .= '<div class="sub header">' . $this->args['occupation'] . '</div>';
			}
			$header .= '</div>';
			$header .= '</div>';
		}

		$this->content .= $header;

		$this->prepend_class('ui');
		$this->add_class('compact');
		$this->add_class('testimonial');
		$this->add_class('segment');

		/**
		 * Don't use name as a HTML attribute.
		 */
		unset($this->atts['name']);

		return $this->html_tag('div');

	}

}

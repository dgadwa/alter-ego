<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Shortcode_Cardtext extends Chap_Shortcode {

	public function __construct($id, $options) {

		$this->description = esc_html__('Text inside a card.', 'chap-shortcodes');

		$this->arg_docs = [
			'content' => 'Text.',
		];

		parent::__construct($id, $options);

	}

	public function render_shortcode() {

		$this->content = '<div class="description">' . $this->content . '</div>';

		$this->add_class('content');

		return $this->html_tag('div');

	}

}

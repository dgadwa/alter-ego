<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Shortcode_Tab extends Chap_Shortcode {

	public function __construct($id, $options) {

		$this->default_args = [
			'title' => '',
		];

		$this->description = esc_html__('Tab content inside [tabs] shortcode.', 'chap-shortcodes');

		$this->arg_docs = [
			'classes' => [
				'state' => ['active', 'disabled'],
				'position' => ['right'],
			],
			'title' => esc_html__('Tab title.', 'chap-shortcodes'),
		];

		$this->presets[esc_html__('Tab with title', 'chap-shortcodes')] = [
			'title' => 'Title',
		];

		parent::__construct($id, $options);

	}

	public function render_shortcode() {

		/**
		 * Check that tabs container is initialized.
		 */
		if(!isset($GLOBALS['csc']['tabs'])) {
			return;
		}

		/**
		 * Store the tab for rendering in [tabs] shortcode.
		 */
		$GLOBALS['csc']['tabs'][] = [
			'id' => uniqid(),
			'title' => $this->args['title'],
			'content' => $this->content,
			'active' => $this->has_class('active'),
			'classes' => $this->args['classes'],
		];

		return;

	}

}

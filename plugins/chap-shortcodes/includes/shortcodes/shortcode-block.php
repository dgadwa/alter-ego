<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Shortcode_Block extends Chap_Shortcode {

	public function __construct($id, $options) {

		$this->default_args = [
			'top' => '0',
			'right' => '0',
			'bottom' => '0',
			'left' => '0',
			'all' => '',
			'unit' => 'em',
		];

		$this->description = esc_html__('Block is an element designed to help separate content from it\'s neighbours by specific amounts.', 'chap-shortcodes');

		$this->arg_docs = [
			'top' => esc_html__('Top margin.', 'chap-shortcodes'),
			'right' => esc_html__('Right margin.', 'chap-shortcodes'),
			'bottom' => esc_html__('Bottom margin.', 'chap-shortcodes'),
			'left' => esc_html__('Left margin.', 'chap-shortcodes'),
			'all' => esc_html__('Even margin from all directions, overrides other arguments.', 'chap-shortcodes'),
			'unit' => esc_html__('Margin units (default: em).', 'chap-shortcodes'),
		];

		$this->presets[esc_html__('30px space from the top', 'chap-shortcodes')] = [
			'top' => '30',
			'unit' => 'px',
		];

		$this->presets[esc_html__('1em space in all directions', 'chap-shortcodes')] = [
			'all' => '1',
		];

		parent::__construct($id, $options);

	}

	public function render_shortcode() {

		if($this->has_arg('all')) {
			$this->add_inline_style('margin', $this->args['all'] . $this->args['unit']);
		} else {
			$top = $this->args['top'] . $this->args['unit'];
			$right = $this->args['right'] . $this->args['unit'];
			$bottom = $this->args['bottom'] . $this->args['unit'];
			$left = $this->args['left'] . $this->args['unit'];
			$this->add_inline_style('margin', $top . ' ' . $right . ' ' . $bottom . ' ' . $left);
		}

		$this->prepend_class('ui');
		$this->add_class('chap-block');
		return $this->html_tag('div');

	}

}

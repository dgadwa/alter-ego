<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Shortcode_Labels extends Chap_Shortcode {

	public function __construct($id, $options) {

		$this->description = esc_html__('Container for multiple labels.', 'chap-shortcodes');

		$this->arg_docs = [
			'classes' => [
				'type' => ['basic', 'tag', 'horizontal', 'floating', 'circular'],
				'size' => ['mini', 'tiny', 'small', 'medium', 'large', 'big', 'huge', 'massive'],
				'color' => ['red', 'green', 'blue', '...'],
			],
		];

		parent::__construct($id, $options);

	}

	public function render_shortcode() {

		$this->prepend_class('ui');
		$this->add_class('labels');

		return $this->html_tag('div');

	}

}

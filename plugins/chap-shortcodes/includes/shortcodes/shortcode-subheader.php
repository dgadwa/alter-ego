<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Shortcode_Subheader extends Chap_Shortcode {

	public function __construct($id, $options) {

		$this->description = esc_html__('Subheader, to be used inside headers.', 'chap-shortcodes');

		$this->arg_docs = [
			'classes' => [
				'letters' => ['normal'],
			],
		];

		parent::__construct($id, $options);

	}

	public function render_shortcode() {

		if(!$this->has_class('normal')) {
			$this->prepend_class('ui');
		}

		$this->add_classes(['sub', 'header']);

		return $this->html_tag('div');

	}

}

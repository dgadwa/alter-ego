<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Shortcode_Cardimage extends Chap_Shortcode {

	public function __construct($id, $options) {

		$this->default_args = [
			'alt' => '',
		];

		$this->description = esc_html__('Card image. Should contain an HTML image tag that can be inserted with the "Add media" button above the editor.', 'chap-shortcodes');

		$this->arg_docs = [
			'content' => esc_html__('HTML image tag.', 'chap-shortcodes'),
			'src' => esc_html__('Optional: image link (image can also be specified inside the shortcode, using an HTML &lt;img&gt; tag).', 'chap-shortcodes'),
			'alt' => esc_html__('Optional: alternative text (only used when src argument is specified).', 'chap-shortcodes'),
		];

		parent::__construct($id, $options);

	}

	public function render_shortcode() {

		if($this->has_arg('src')) {
			$this->content .= '<img src="' . $this->args['src'] . '" alt="' . $this->args['alt'] . '" />';
		}

		$this->add_class('image');

		return $this->html_tag('div');

	}

}

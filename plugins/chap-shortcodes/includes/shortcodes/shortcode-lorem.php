<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Shortcode_Lorem extends Chap_Shortcode {

	public function __construct($id, $options) {

		$this->default_args = [];

		$this->description = esc_html__('Dummy text.', 'chap-shortcodes');

		$this->arg_docs = [
			'classes' => [
				'paragraph length' => ['short', 'long'],
				'paragraph count' => ['1-50'],
			],
		];

		$this->presets[esc_html__('Three long paragraphs', 'chap-shortcodes')] = [
			'long',
			'3',
		];

		parent::__construct($id, $options);

	}

	public function render_shortcode() {

		$lorem = include get_plugin_dir() . '/includes/lorem.php';
		$paragraph_length = 3;
		$paragraphs = 1;

		/**
		 * Paragraph length from class text.
		 */
		if($this->has_class('short')) {

			$paragraph_length = 1;
			$this->args['classes'] = trim(str_replace('short', '', $this->args['classes']));

		} elseif($this->has_class('long')) {

			$paragraph_length = 5;
			$this->args['classes'] = trim(str_replace('long', '', $this->args['classes']));

		}

		if(!empty($this->args['classes'])) {

			$p = intval($this->args['classes']);
			if(is_int($p) && $p > 0) {
				$paragraphs = $p;
			}

		}

		/**
		 * Cap paragraphs.
		 */
		if($paragraphs > 50) {
			$paragraphs = 50;
		}

		/**
		 * Tag to use.
		 */
		$tag = 'div';

		/**
		 * Compose text.
		 */
		for($i = 0; $i < $paragraphs; $i++) {

			$text = '';

			for($j = $i; $j < $i + $paragraph_length; $j++) {
				$key = $i;
				if(isset($lorem[$key])) {
					$text .= $lorem[$key] . ' ';
				}
			}

			if($paragraphs === 1) {
				$this->content .= $text;
				$tag = 'p';
			} else {
				$this->content .= '<p>' . $text . '</p>';
			}

		}

		foreach($this->atts['classes'] as $key => $class) {
			if(in_array($class, ['short', 'long'])) {
				unset($this->atts['classes'][$key]);
			}
		}

		return $this->html_tag($tag);

	}

}

<?php

namespace Chap\Shortcodes;
use Chap\Helpers;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Shortcode_Label extends Chap_Shortcode {

	public function __construct($id, $options) {

		$this->default_args = [
			'alt' => '',
		];

		$this->description = esc_html__('A label displays content classification.', 'chap-shortcodes');

		$this->arg_docs = [
			'classes' => [
				'type' => ['basic', 'tag', 'horizontal', 'floating', 'circular'],
				'ribbon' => ['ribbon', 'right ribbon'],
				'corner' => ['left corner', 'right corner'],
				'attached' => ['top attached', 'bottom attached', 'right attached', 'left attached', 'bottom left attached', 'bottom right attached'],
				'pointing' => ['pointing', 'pointing below', 'left pointing', 'right pointing'],
				'float' => ['left floated', 'right floated'],
				'size' => ['mini', 'tiny', 'small', 'medium', 'large', 'big', 'huge', 'massive'],
				'color' => ['red', 'green', 'blue', '...'],
				'other' => ['disabled'],
			],
			'href' => esc_html__('The href/url/link argument can be used to specify a link.', 'chap-shortcodes'),
			'icon' => esc_html__('Name of the icon to use.', 'chap-shortcodes'),
			'image' => esc_html__('URL of the image to use.', 'chap-shortcodes'),
			'alt' => esc_html__('Alternative text for the image.', 'chap-shortcodes'),
			'detail' => esc_html__('Label detail text.', 'chap-shortcodes'),
		];

		parent::__construct($id, $options);

	}

	public function render_shortcode() {

		if($this->has_arg('image') && !Helpers\is_amp()) {
			$this->add_class('image');
			$this->content = '<img src="' . $this->args['image'] . '" alt="' . $this->args['alt'] . '" />' . $this->content;
		}

		if($this->has_arg('detail')) {
			$this->content = $this->content . '<div class="detail">' . $this->args['detail'] . '</div>';
		}

		if($this->has_arg('icon')) {
			if($this->args['icon'] === 'close') {
				$this->content = $this->content . do_shortcode('[csc-icon ' . $this->args['icon'] . ']');
			} else {
				$this->content = do_shortcode('[csc-icon ' . $this->args['icon'] . ']') . $this->content;
			}
		}

		$this->prepend_class('ui');
		$this->add_class('label');

		return $this->html_tag('div');

	}

}

<?php

namespace Chap\Shortcodes;
use Chap\Helpers;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Shortcode_Ad extends Chap_Shortcode {

	public function __construct($id, $options) {

		$this->description = esc_html__('A wrapper for third-party ad network content.', 'chap-shortcodes');

		$this->arg_docs = [
			'classes' => [
				'type' => ['rectangle', 'banner', 'leaderboard', 'skyscraper', 'panorama', 'netboard', 'billboard', 'button'],
				'size' => ['small', 'medium', 'large', 'half', 'half page', 'wide'],
				'variation' => ['vertical', 'centered', 'square', 'top', 'test'],
				'visibility' => ['mobile'],
			],
		];

		$this->presets[esc_html__('Medium rectangle ad', 'chap-shortcodes')] = [
			'medium',
			'rectangle',
		];

		$this->presets[esc_html__('Banner ad', 'chap-shortcodes')] = [
			'banner',
		];

		$this->presets[esc_html__('Leaderboard ad', 'chap-shortcodes')] = [
			'leaderboard',
		];

		$this->presets[esc_html__('Large rectangle ad', 'chap-shortcodes')] = [
			'large',
			'rectangle',
		];

		$this->presets[esc_html__('Half page ad', 'chap-shortcodes')] = [
			'half',
			'page',
		];

		parent::__construct($id, $options);

	}

	public function render_shortcode() {

		if(Helpers\is_amp()) {
			return;
		}

		$this->prepend_class('ui');
		$this->add_class('ad');

		return $this->html_tag('div');

	}

}

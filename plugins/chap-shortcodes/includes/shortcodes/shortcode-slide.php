<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Shortcode_Slide extends Chap_Shortcode {

	public function __construct($id, $options) {

		$this->description = esc_html__('Slide inside slider shortcode.', 'chap-shortcodes');

		// $this->presets[esc_html__('With CSS classes')] = [
		// 	'class1',
		// 	'class2',
		// ];

		parent::__construct($id, $options);

	}

	public function render_shortcode() {

		$this->add_class('swiper-slide');
		$this->add_class('csc-slide');

		return $this->html_tag('div');

	}

}

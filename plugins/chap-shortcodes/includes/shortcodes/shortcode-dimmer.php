<?php

namespace Chap\Shortcodes;
use Chap\Helpers;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Shortcode_Dimmer extends Chap_Shortcode {

	public function __construct($id, $options) {

		$this->default_args = [
			'dimmer' => '',
			// 'image' => '//placehold.it/150',
			'alt' => 'Dimmer image',
		];

		$this->description = esc_html__('Image that displays content on hover.', 'chap-shortcodes');

		$this->arg_docs = [
			'image' => esc_html__('The url to the image.', 'chap-shortcodes'),
			'alt' => esc_html__('Image alt.', 'chap-shortcodes'),
			'dimmer' => esc_html__('Dimmer classes.', 'chap-shortcodes'),
		];

		parent::__construct($id, $options);

	}

	public function render_shortcode() {

		if(Helpers\is_amp()) {
			$this->add_amp_script('amp-lightbox');
		} else {
			$this->add_script('dimmer');
		}

		$this->content = '<div class="center">' . $this->content . '</div>';

		$id = uniqid();
		if(Helpers\is_amp()) {
			$this->content = '<div class="content" on="tap:' . $id . '.close" role="button" tabindex="0">' . $this->content . '</div>';
			$this->content = '<amp-lightbox id="' . $id . '" layout="nodisplay">' . $this->content . '</amp-lightbox>';
			$this->atts['on'] = 'tap:' . $id;
			$this->atts['role'] = 'button';
			$this->atts['tabindex'] = 0;
		} else {
			$this->content = '<div class="content">' . $this->content . '</div>';
		}

		$this->prepend_class('ui');

		if($this->has_arg('image')) {

			$this->add_class('dimmable');
			$this->add_class('image');

			$dimmer_args = [];
			$dimmer_args['classes'][] = 'ui';
			if(strlen($this->args['dimmer']) > 0) {
				$dimmer_args['classes'][] = $this->args['dimmer'];
			}
			$dimmer_args['classes'][] = 'dimmer';

			$this->content = Helpers\html_tag('div', $dimmer_args) . $this->content . '</div>';
			$this->content = $this->content . '<img src="' . $this->args['image'] . '" alt="' . $this->args['alt'] . '" />';

		} else {

			$this->add_class('dimmer');

		}

		return $this->html_tag('div');

	}

}

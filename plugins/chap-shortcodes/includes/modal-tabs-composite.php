<?php

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

/**
 * Return the contents of each tab in
 * the admin "Insert Composite" modal.
 */

return [

	// 'front-page' => [], // Default tab

	'pages' => [
		'page-contact',
		'page-team',
		'page-team-2',
		'page-portfolio',
		'page-testimonials',
		'page-faq',
		'page-plans',
		'page-about',
		'page-services',
	],

	'slides' => [
		'slide-jumbotron-short',
		'slide-jumbotron-long',
		'slide-call-to-action',
		'slide-basic',
		'slide-basic-animated',
		'slide-product',
		'slide-newsletter-signup',
		'slide-search',
		'slide-form',
		'slide-piled-segment',
	],

	'misc' => [
		'plan-card',
		'plan-cards',
		'github-front-page',
		'slide-github',
	],

	// Hidden
	'wizard' => [
		'sample-page',
	],

];

<?php

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

/**
 * Return the contents of each tab in
 * the admin "Insert Shortcode" modal.
 */

return [
	'containers' => [
		'container',
		'accordion',
		'content',
		'tabs',
		'tab',
		'grid',
		'row',
		'column',
		'segments',
		'segment',
		'ad',
		'list',
		'item',
		'menu',
		'submenu',
		'image',
		'images',
		'buttons',
		'labels',
		'statistics',
		'steps',
		'block',
		'slider',
		'slide',
	],
	'card' => [
		'cards',
		'card',
		'cardheader',
		'cardimage',
		'carddimmer',
		'carditem',
		'cardtext',
	],
	'form' => [
		'form',
		'fields',
		'field',
		'input',
		'textarea',
		'checkbox',
		'select',
		'option',
	],
	'misc' => [
		'p',
		'div',
		'span',
		'ui',
		'lorem',
		'posts',
		'address',
		'code',
		'post',
		'page',
		'repeat',
		'animation',
		'clip',
		'show',
	],
	'dev' => [
		'docfile',
		'demo',
		'args',
		'composites',
	],
];

<?php

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

/**
 * Dummy text used with [lorem] shortcode.
 */

$lorem = [];

$lorem[] = <<<LOREM
Lorem ipsum dolor sit amet, dolor maiorum ius ei, sea omnesque verterem ne. Facer decore theophrastus ne est, ea vim quot fierent eloquentiam, in reque porro mei. Ex legere laoreet scripserit ius, partem soluta timeam eu vim. Te mea congue aperiam, at mei diceret dolorum molestiae. Placerat complectitur vim ex.
LOREM;

$lorem[] = <<<LOREM
Esse oportere temporibus at vix, has reque paulo appareat ad, an prima salutatus referrentur sea. Probo minimum eos ut, ea lobortis perpetua nec. Cum posse semper et. Graeci tamquam ut sit, an appareat adolescens eum. Novum elitr in quo, cum ea vocibus signiferumque. Solet antiopam mediocritatem ei per, an vim clita alterum verterem, utinam dignissim dissentiunt te quo.
LOREM;

$lorem[] = <<<LOREM
Ex pri doctus offendit, sint error dignissim mel ea, ex sed eius graeco timeam. At eam impedit adipisci accusata, impedit accusam mel ex, eam docendi albucius an. Ius detraxit menandri cu. Per falli veniam mentitum an, alii ancillae et has. Eu his quod tollit, mazim quaeque habemus usu ea.
LOREM;

$lorem[] = <<<LOREM
Consetetur signiferumque mei et, quo no dicit deleniti epicurei. Ex qui dolorum periculis, mea soluta persius et. Mea atqui facilisi id. Duo deserunt electram pertinacia et, ne cum ridens gubergren, dicunt animal hendrerit sea an. Has tota minimum reprehendunt eu.
LOREM;

$lorem[] = <<<LOREM
Oportere dignissim ea nam. Nam no fugit maluisset similique, nobis verterem mel et. Debet apeirian ei quo, autem quando pro no. Ne mandamus deterruisset eam. Paulo timeam dignissim sit no, mei ex possim scaevola torquatos, est an ullum maiestatis.
LOREM;

$lorem[] = <<<LOREM
Ne vim sapientem referrentur. Cu qui eleifend persequeris. Laudem commune honestatis no per, eam integre moderatius ex, cu quem brute vix. Duo suas virtute ei, mollis tritani sanctus eos te. Nulla laoreet vim ut, scaevola vivendum vis eu, ius accusamus democritum mediocritatem eu.
LOREM;

$lorem[] = <<<LOREM
At dicant nominavi ius, impetus euismod mel in, no atomorum splendide efficiendi pro. Augue numquam definitiones quo ei. Vix eu sumo accusata, per ut libris fabulas appellantur. Ne inani graece repudiare vis, populo vivendum perfecto te has.
LOREM;

$lorem[] = <<<LOREM
At eum epicuri persequeris contentiones, no mei vero elaboraret, cu omnes putant sapientem vel. Vis sanctus atomorum eu, no simul meliore deterruisset est. An suscipit sadipscing nam. Pri ullum bonorum disputationi in, ea primis epicuri antiopam pri, cum cibo elitr causae no. Sumo ponderum ad pro, etiam luptatum laboramus in eam, mea ludus possim mediocritatem ad.
LOREM;

$lorem[] = <<<LOREM
Ei aeque reprehendunt duo, te ludus invenire per, nibh ocurreret eu cum. No fabulas civibus disputationi eos, nec ei ipsum facilis interpretaris. Ut debet impedit vel, pro ex tollit sensibus. Eum semper epicurei molestiae ea.
LOREM;

$lorem[] = <<<LOREM
Paulo fuisset in duo. Dolorum qualisque deseruisse eam te. Est ea graeci nusquam, ex vix putent habemus copiosae. No quo facer primis. Inani argumentum vim ut, magna fabellas ne mea, pro mediocrem dignissim disputationi cu. Sed falli elitr laboramus te, audiam timeam docendi te eam.
LOREM;

$lorem[] = <<<LOREM
Eius populo aliquam vis id. Ignota graeci fuisset ei qui, eu sumo timeam intellegam sed. Sumo movet cu nec. Cu adhuc ponderum cum. Ex est nostro meliore accusata, oratio semper sea no.
LOREM;

$lorem[] = <<<LOREM
Sumo simul no nam. Pri numquam accusamus percipitur an, eum amet unum sonet ut. Duo id decore mandamus, in has magna hendrerit. Et viris elitr adipiscing qui. Nostro integre corpora ut sit, corpora nostrum adolescens pri id.
LOREM;

$lorem[] = <<<LOREM
Facilis mandamus dissentiet ut eos. Movet aliquando repudiandae no usu. Has legere ridens ancillae eu. Cu oporteat pertinax pro.
LOREM;

$lorem[] = <<<LOREM
Regione percipit signiferumque sit an, at putent saperet facilis nec. Feugiat inermis eum eu, ei sadipscing eloquentiam vituperatoribus nec. Vis te nihil euripidis philosophia. Cum commune percipitur ad, est veniam voluptaria id.
LOREM;

$lorem[] = <<<LOREM
Euismod civibus cotidieque ad mel, id sumo diceret postulant sea. Probatus voluptatibus ex vix. An inani tempor signiferumque nam, ne errem consetetur sed. Saperet elaboraret mel cu, eam te nostro volumus fastidii. Per mucius nominavi scribentur te, te per esse ipsum idque.
LOREM;

$lorem[] = <<<LOREM
Ei solet salutandi pertinacia sed, duis homero euripidis eum ex. Duo alii necessitatibus cu, at dolorum corrumpit gubergren vim. Quo ei porro cetero persecuti, an vivendo tincidunt has. Et debet invidunt definiebas mel, ei vim unum dicant appareat.
LOREM;

$lorem[] = <<<LOREM
Ea vis summo theophrastus. Mei eripuit fabellas consulatu et, per nulla ubique ex. Cu eos diceret honestatis reformidans. Te affert omnesque tractatos per, ut movet dicunt officiis sea, nec sonet soluta audiam at. Eu quidam offendit mei. Mea vocent appetere an, commodo complectitur ius ad.
LOREM;

$lorem[] = <<<LOREM
Soleat facilisis assueverit eum et, pro sale summo consectetuer et, te quo agam detraxit concludaturque. Case tantas assueverit usu id, justo nemore doctus ex mei. Rebum singulis eum at, eu vix consul aliquip. Has at novum causae definitionem, lorem option delenit mea cu. Qui ludus corpora ea. At ubique apeirian moderatius pri, affert mollis vix ex.
LOREM;

$lorem[] = <<<LOREM
Ubique similique theophrastus sed no, sit et omnes quaestio interpretaris, at eum copiosae eleifend. Quo paulo maiorum eu, eu deserunt mediocrem nec. Pri ea melius praesent vituperatoribus. Eam eu ornatus habemus lobortis. Putant liberavisse ad usu.
LOREM;

$lorem[] = <<<LOREM
Vim ei homero imperdiet. Nec solet postea possit at, pri debet voluptatibus ne. Ex eum lorem minim, mea cu aperiam assueverit. Nec ex porro ullamcorper mediocritatem, odio mollis accusam ut est, ea probatus splendide eum. Ea his aperiri oporteat dissentiet, in viris consectetuer mea.
LOREM;

$lorem[] = <<<LOREM
Placerat evertitur prodesset pro an. Vix nostrud fierent vivendum an, at sit nostrud sadipscing efficiantur. Per in aliquid detraxit, appetere definiebas persequeris has ex. At pro ubique prompta philosophia.
LOREM;

$lorem[] = <<<LOREM
Ne cum persius deseruisse, ius ferri euismod eu, ea eum nisl noster aliquam. Id nam veniam maiorum hendrerit, ut integre dissentiet theophrastus mea. Tollit impedit vulputate ex nam, vide modus nam ex, delenit vituperata ea duo. Cum facer petentium vituperatoribus ad, at eam mandamus patrioque voluptaria. Partem commune at pro, tota utamur mel eu, cum cibo offendit tractatos te. Audiam integre tincidunt id sit.
LOREM;

$lorem[] = <<<LOREM
Mea graecis reprehendunt id. Et veritus efficiendi consectetuer eam. Quot mundi et sed. Eu quo amet patrioque, at ipsum choro ius, id ius quod epicuri. Eirmod tincidunt democritum duo ne, qui an ludus veniam, mea iusto expetendis in. Ei velit reformidans pro.
LOREM;

$lorem[] = <<<LOREM
Alterum lobortis his ex, eam ei numquam pertinacia. Ei qui falli graecis, ei eum purto causae, assentior necessitatibus est te. Eu qui abhorreant argumentum. Agam accumsan euripidis ea mea, libris imperdiet qui te, sea ut habemus democritum liberavisse. His ei duis graeci apeirian, imperdiet iracundia persequeris at quo, zril gubergren adolescens ad usu.
LOREM;

$lorem[] = <<<LOREM
Eu case consetetur nec. Nam et inermis comprehensam, ubique habemus ea sed. Eum in gloriatur rationibus interpretaris, munere quaestio definitiones ea ius. Te molestie definiebas percipitur vim. Erant iudicabit te pri, case nusquam mnesarchum eam ne.
LOREM;

$lorem[] = <<<LOREM
Duo ad quod lorem fuisset, propriae appareat recusabo pro ne. Nulla vivendum duo in, eam te essent incorrupte dissentiunt, eu est dicta harum delicata. Zril consequuntur cu pri. Ad ignota viderer nec, est suscipit consetetur cu. Probo congue nusquam vim ut, noster instructior quo ea.
LOREM;

$lorem[] = <<<LOREM
In euismod scripserit quo, quis diceret postulant id vel. Tempor facilisis ocurreret ut sed. Reque invenire has cu. Ignota audire cotidieque cu sea, hinc voluptatibus ex nam. Te vis alia doctus cotidieque.
LOREM;

$lorem[] = <<<LOREM
Quot definitiones te vim, duo ut omnis conceptam. Mel ad esse cibo incorrupte, te dicam detraxit principes mel. Eum modo ubique repudiare te, eos cu quod justo, ut duo nibh scribentur. Qui esse accommodare eu, cu ferri animal phaedrum sit. Eos commodo dolorem fastidii id, ei doctus nonumes cum, eum no petentium intellegam. Facer conceptam at eam, graeco abhorreant mel ad, his id sint omittam. Mel at velit eligendi mediocrem.
LOREM;

$lorem[] = <<<LOREM
Blandit assentior intellegat no nam, ea eam phaedrum argumentum adversarium. Sit quod recusabo argumentum ex. Sed dolore commune mentitum cu, feugait similique intellegat nam eu, ea nostrum definitiones vis. Ei modo mundi dissentiet mea, democritum percipitur eos ei.
LOREM;

$lorem[] = <<<LOREM
Eam ridens delenit mandamus ea, liber omnes civibus ex ius, illum adipiscing cu sit. Sit erat soluta eu. Sea animal adipisci no. Quidam omnium ut mel. Ne mea soluta malorum maiestatis.
LOREM;

$lorem[] = <<<LOREM
Eos et iuvaret insolens vituperatoribus. Eu iracundia omittantur dissentiet eam, vis cu debet persequeris. Eam cu virtute diceret, quo ex veniam oblique menandri. Velit commune no per. At vel admodum delicata, cu erat meis reprehendunt usu.
LOREM;

$lorem[] = <<<LOREM
Usu ex sint ullum commune, mel suas epicuri ea. His ea idque interesset, sea ridens labores singulis ea. Luptatum erroribus at est, et veri clita eripuit mel. Sed possim indoctum partiendo te. Dicat urbanitas definitiones sea no. Per in suas tempor honestatis, ullum error dissentiunt ex nec.
LOREM;

$lorem[] = <<<LOREM
Has eu habeo hendrerit, aeterno mediocrem vel at, vim option petentium et. Habeo iriure id ius, mel at odio magna, dicat liber in mei. Sit no habeo utinam deseruisse. Te est timeam lobortis, nulla suscipit principes mel an. Mei ex cibo ferri deleniti, constituto interpretaris mel et. Eu sit vidit adipisci molestiae.
LOREM;

$lorem[] = <<<LOREM
Error albucius cu vix, ne partem altera possit nam. Has ex autem alterum, eum et error iisque alienum, te affert voluptatibus definitionem sea. Libris mollis democritum nec ad, eu vel quas disputationi. Justo oporteat consulatu et nec, quas mazim nec ex, per aperiri salutatus at. Cu eos nemore aliquid adversarium, saepe democritum abhorreant an mei.
LOREM;

$lorem[] = <<<LOREM
Decore fierent ullamcorper ne mel, ferri nusquam periculis ea sed. Mea in volutpat sententiae, vel ea illud ullamcorper, nam no novum mollis aperiri. Et sea diceret ponderum, cu causae definiebas mei, id vim offendit praesent. Tritani accommodare delicatissimi no per, vel in nibh dolorum offendit, mea ea vide docendi. Dicat aliquip vulputate an per, simul sanctus definitionem cu has. Elit postea cotidieque has ea, ex impedit antiopam sea. Ut quo semper accumsan, no ius tota imperdiet assueverit.
LOREM;

$lorem[] = <<<LOREM
Ius id splendide tincidunt. Ei duo justo possit, mei perfecto sadipscing at, scripta detracto vis ad. Mea oblique delicata eu, fugit consequat voluptatibus sea ut, meis oblique indoctum vim ex. Sumo exerci luptatum ad duo, ne quo ferri impetus instructior, eam quis adipisci ex.
LOREM;

$lorem[] = <<<LOREM
Ei nam mutat dicit meliore, id error zril nonumes quo. Cu congue aperiri vix, cu ius dicat ridens. Omnis euripidis pri ei, eu quo solum percipitur omittantur. Ferri tractatos his ei. Usu simul vidisse ne, ut eos omnis adipisci concludaturque. Sit eu dolorum qualisque contentiones.
LOREM;

$lorem[] = <<<LOREM
Te ius illud fabulas. Te dicunt regione prodesset vel, id vis homero splendide. Feugiat deserunt atomorum no vis, mea recusabo vulputate at. No nisl recteque consulatu duo, mei nonumes facilisi recteque no. No tale reque timeam per. Menandri maluisset has et, mel ea vide vituperata, sint graece luptatum ne has.
LOREM;

$lorem[] = <<<LOREM
Delicata hendrerit sea et, cum stet duis ullamcorper eu. Ei mel adhuc fugit, no pro reque iuvaret praesent. Nam vidit reprehendunt in, his etiam sonet id, duis voluptua legendos vim te. An sit iuvaret verterem, probo iriure expetenda in nam, et zril appellantur sit.
LOREM;

$lorem[] = <<<LOREM
Dicunt dissentiunt ut sit, mei cu regione iuvaret, usu ea rebum volumus partiendo. Et ubique possit omittam per, dico nemore noluisse an eum. Cum te ullum dicam epicurei. Ad sea copiosae quaestio, fuisset electram no vim, in qui alii dicit evertitur. Duo ea minim soluta percipit. Cu eam diam eleifend posidonium, mollis aeterno neglegentur eum no. Cu nec adhuc admodum menandri, verear menandri ne mea.
LOREM;

$lorem[] = <<<LOREM
Pri dico copiosae tacimates an, in usu augue veniam. Pri ex omnesque qualisque persequeris, mel quas epicuri offendit ne, officiis accommodare an mel. No illud affert aliquid per, et unum nibh iisque pro. Vide cetero philosophia vel ne, eu omnesque forensibus vix. Dicit laoreet ne mea, sit illud iriure no.
LOREM;

$lorem[] = <<<LOREM
Brute electram ius cu. At melius expetenda persecuti ius, noluisse percipitur est no, eu dictas possim eligendi his. Persius expetenda explicari id sit, quo illud simul persequeris ut. Ex mazim salutatus eam. Per et dico homero laudem. Semper consectetuer vix in, eu per liber detracto, natum noluisse an sea. Te sit debet numquam, no persius platonem suscipiantur vim.
LOREM;

$lorem[] = <<<LOREM
Et errem percipitur his. In nihil molestiae inciderint est, in mea affert vocibus copiosae. No iusto voluptaria constituto vix, usu an tamquam prodesset. Eum vidit dolorum id, et his elitr officiis delicatissimi.
LOREM;

$lorem[] = <<<LOREM
Mel ut maluisset dissentias voluptatibus. Mei alii clita perfecto ea, mea ei alienum conceptam, vel id dicat euismod reprimique. No nec labore discere, duo stet tation malorum ea, tempor nostrum voluptua no vim. Ad duo rebum vidisse, eum mucius persecuti at. Affert tantas aperiam cum in.
LOREM;

$lorem[] = <<<LOREM
Munere molestie electram no eos, nostro consetetur incorrupte his ut. Illum meliore salutandi has in. His illum munere ne, fabulas luptatum scribentur an mel. Mea ea persius voluptatum, posse nobis primis cu sed.
LOREM;

$lorem[] = <<<LOREM
Meis posse molestiae vix et, pri audire minimum tacimates eu. Quo dolores accusata facilisis te, ea partiendo vulputate per. No eum iusto aliquam necessitatibus, sale nobis cu sit. Labores tincidunt an his, blandit tacimates cu his. Sit salutatus appellantur et. Cu est copiosae disputando suscipiantur, et cum nobis veniam. Quot vitae no ius, ut augue legimus lucilius mei.
LOREM;

$lorem[] = <<<LOREM
Minimum verterem oportere per ea, putant iudicabit no vix. Et justo probatus omittantur sed, te pro sanctus molestiae, cu tempor accumsan lucilius mei. Cu vix congue fabulas repudiandae, ne equidem molestiae argumentum mei, at vel labore prompta. In alterum vivendum sapientem pri, diceret fabulas mediocrem pri eu, ad mea legimus dolorem insolens. Ad nihil vidisse tamquam sea, per id alia aliquip sapientem.
LOREM;

$lorem[] = <<<LOREM
An mea audiam tamquam, vix ne mucius iudicabit liberavisse. An eos ubique vocibus molestiae, cum at partem putent debitis, nec te eros consectetuer. Eu adhuc omittam sed, altera moderatius no has, eos debet accusam epicurei ea. Et diam aeque impetus mei, in postea regione sed, putent blandit ne ius. At pro dolores maiestatis interpretaris, brute mazim dignissim ea eos, ferri utinam vulputate ad eos.
LOREM;

$lorem[] = <<<LOREM
Cibo voluptaria at mel, te deserunt partiendo adolescens pri. Decore gubergren aliquando an eum. Et vis blandit perfecto. Tantas prodesset an mea, et pro omnes nonumes praesent. Mel cu magna rebum ullamcorper, vel possit ceteros invenire ea.
LOREM;

$lorem[] = <<<LOREM
Ne sea probo dicam legere, novum docendi duo eu. Nibh civibus euripidis cum in, eu qui pertinacia maiestatis. Sed voluptua neglegentur te, ei eam persius inimicus. Antiopam tractatos moderatius cum ad, at dicant dicunt ocurreret per. Mea soleat fuisset ea. Vis ut utamur verterem neglegentur. Integre detracto at quo, dignissim temporibus et ius, et mundi alterum invidunt eam.
LOREM;

return $lorem;

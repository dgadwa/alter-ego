<?php

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

/**
 * Return the logical order of shortcodes, for
 * displaying in the admin "Insert Shortcode" modal.
 */

return [
	// Elements
	'button',
	'icon',
	'header',
	'subheader',
	'divider',
	'message',
	'label',
	'testimonial',
	'social',
	'step',
	'flag',
	'embed',
	'statistic',
	'dimmer',
	'toc',

	// Containers
	'container',
	'grid',
	'row',
	'column',
	'segments',
	'segment',
	'images',
	'image',
	'tabs',
	'tab',
	'accordion',
	'content',
	'list',
	'item',
	'menu',
	'submenu',
	'buttons',
	'labels',
	'ad',
	'statistics',
	'steps',
	'block',

	// Card
	'cards',
	'card',
	'cardheader',
	'cardimage',
	'carddimmer',
	'carditem',
	'cardtext',

	// Form
	'form',
	'fields',
	'field',
	'input',
	'textarea',
	'checkbox',
	'select',
	'option',

	// Misc
	'posts',
	'post',
	'page',
	'repeat',
	'code',
	'div',
	'span',
	'p',
	'ui',
	'lorem',
	'posts',
	'address',
	'clip',
	'animation',
];

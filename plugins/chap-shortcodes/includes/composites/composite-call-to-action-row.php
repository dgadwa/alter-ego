<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Composite_call_to_action_row extends Chap_Composite {

	public function __construct($id, $options) {

		$this->description = esc_html__('Call to action row with headers and multiple choice buttons.', 'chap-shortcodes');

		parent::__construct($id, $options);

	}

	public function get_content() {

		$p = $this->p;

		$content = <<<STR
[{$p}segment big vertical stripe]
  [{$p}grid stackable container]
    [{$p}column nine wide middle aligned]
      [{$p}header huge blue normal marginless text]Ready to get started?[/{$p}header]
      [{$p}header huge normal marginless text]Get in touch, or create an account.[/{$p}header]
    [/{$p}column]
    [{$p}column seven wide right aligned middle aligned]
      [{$p}button massive compact primary top]Create an account[/{$p}button]
      [{$p}button massive compact primary basic]Contact sales[/{$p}button]
    [/{$p}column]
  [/{$p}grid]
[/{$p}segment]
STR;

		return $content;

	}

}

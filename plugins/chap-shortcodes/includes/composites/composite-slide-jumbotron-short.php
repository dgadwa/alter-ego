<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Composite_slide_jumbotron_short extends Chap_Composite {

	public function __construct($id, $options) {

		$this->description = esc_html__('Jumbotron with very short text (allowing for massive header size).', 'chap-shortcodes');

		parent::__construct($id, $options);

	}

	public function get_content() {

		$p = $this->p;

		$content = <<<STR
[header massive inverted marginless]Lorem[/header]
[header inverted]Lorem ipsum dolor sit amet.[/header]
[block top="0.5"]
  [button basic inverted]Secondary[/button]
  [button inverted right labeled marginless icon icon="right arrow"]Primary[/button]
[/block]
STR;

		return $content;

	}

}

<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Composite_plan_cards extends Chap_Composite {

	public function __construct($id, $options) {

		$this->description = esc_html__('Plan cards.', 'chap-shortcodes');

		parent::__construct($id, $options);

	}

	public function get_content() {

		$p = $this->p;

		$content = <<<STR
[{$p}cards three stackable]
  [{$p}card red url="#"]
    [{$p}cardheader center aligned subheader="€5/month per user"]Cheap package[/{$p}cardheader]
    [{$p}cardimage src="//placehold.it/300x120" alt="Cheap"]
      [{$p}label red ribbon]Discount[/{$p}label]
    [/{$p}cardimage]
    [{$p}carditem icon="check"]Eu nec esse splendide[/{$p}carditem]
    [{$p}carditem icon="check" description="At alii legendos complectitur has."]Libris facilisis in duo[/{$p}carditem]
    [{$p}carditem icon="red user"]3+ mel meis tollit[/{$p}carditem]
    [{$p}cardtext]Vero virtute ius ad, ut vel percipit inimicus expetendis. Eu nec esse splendide suscipiantur. Mea ut purto affert.[/{$p}cardtext]
    [{$p}cardheader center aligned]€9.99[/{$p}cardheader]
  [/{$p}card]
  [{$p}card green url="#"]
    [{$p}cardheader center aligned subheader="€15/month per user"]Basic package[/{$p}cardheader]
    [{$p}cardimage src="//placehold.it/300x120" alt="Basic"]
    [{$p}carditem icon="check" description="At alii legendos complectitur has."]Libris facilisis in duo[/{$p}carditem]
    [{$p}carditem icon="check"]Eu nec esse splendide[/{$p}carditem]
    [{$p}carditem icon="green user"]7+ mel meis tollit[/{$p}carditem]
    [{$p}cardtext]Vero virtute ius ad, ut vel percipit inimicus expetendis. Eu nec esse splendide suscipiantur. Mea ut purto affert.[/{$p}cardtext]
    [{$p}cardheader center aligned]€29.99[/{$p}cardheader]
  [/{$p}card]
  [{$p}card blue url="#"]
    [{$p}cardheader center aligned subheader="€250/month unlimited users"]VIP package[/{$p}cardheader]
    [{$p}label blue right corner icon="dollar"]
    [{$p}cardimage src="//placehold.it/300x120" alt="VIP"]
    [{$p}carditem icon="check"]Eu nec esse splendide[/{$p}carditem]
    [{$p}carditem icon="check" description="At alii legendos complectitur has."]Libris facilisis in duo[/{$p}carditem]
    [{$p}carditem icon="blue user"]10+ mel meis tollit[/{$p}carditem]
    [{$p}cardtext]Vero virtute ius ad, ut vel percipit inimicus expetendis. Eu nec esse splendide suscipiantur. Mea ut purto affert.[/{$p}cardtext]
    [{$p}cardheader center aligned subheader="Best value"]€59.99[/{$p}cardheader]
  [/{$p}card]
[/{$p}cards]
STR;

		return $content;

	}

}

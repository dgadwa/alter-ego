<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Composite_feature_columns extends Chap_Composite {

	public function __construct($id, $options) {

		$this->description = esc_html__('Feature columns with icons, text and buttons.', 'chap-shortcodes');

		parent::__construct($id, $options);

	}

	public function get_content() {

		$p = $this->p;

		$content = <<<STR
[{$p}segment feature alternate vertical stripe]
  [{$p}container three column center aligned divided relaxed stackable grid]
    [{$p}row]
      [{$p}column]
        [{$p}header large icon icon="code"]Donec iaculis[/{$p}header]
        [{$p}p]Vivamus vitae erat finibus, lacinia enim et, tincidunt nulla. Sed gravida velit eget magna luctus dignissim. Curabitur efficitur volutpat nisi, elementum vehicula velit.[/{$p}p]
        [{$p}button large]Morbi pharetra[/{$p}button]
      [/{$p}column]
      [{$p}column]
        [{$p}header large icon icon="upload"]Etiam suscipit[/{$p}header]
        [{$p}p]Suspendisse ac quam nulla. Curabitur feugiat tristique ligula in vulputate. Vivamus mi sapien, tempor id justo eget, feugiat varius augue. Sed gravida velit eget magna luctus dignissim.[/{$p}p]
        [{$p}button large primary]Fusce ac urna[/{$p}button]
      [/{$p}column]
      [{$p}column]
        [{$p}header large icon icon="paint brush"]Vestibulum[/{$p}header]
        [{$p}p]Quisque non est quis nulla condimentum blandit in vitae lorem. Donec iaculis malesuada magna rutrum molestie. Nam eget lobortis quam.[/{$p}p]
        [{$p}button large]Vestibulum pharetra[/{$p}button]
      [/{$p}column]
    [/{$p}row]
  [/{$p}container]
[/{$p}segment]
STR;

		return $content;

	}

}

<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Composite_jumbotron_text_image extends Chap_Composite {

	public function __construct($id, $options) {

		$this->description = esc_html('Jumbotron text and a dimmer image with an action button.');

		parent::__construct($id, $options);

	}

	public function get_content() {

		$p = $this->p;

		$content = <<<STR
[{$p}segment huge center aligned containing vertical stripe]
  [{$p}grid stackable very relaxed center aligned container]
    [{$p}column twelve wide]
      [{$p}header tag="h1" huge normal text]Lorem ipsum dolor sit amet[/{$p}header]
      [{$p}p]Quisque convallis libero in sapien pharetra tincidunt. Aliquam elit ante, malesuada id, tempor eu, gravida id, odio.[/{$p}p]
    [/{$p}column]
  [/{$p}grid]
  [{$p}block top="5" bottom="-9"]
    [{$p}dimmer image="//placehold.it/1024x400" alt="" dimmer="inverted"]
      [{$p}button massive secondary right labeled icon icon="right chevron"]Action button[/{$p}button]
    [/{$p}dimmer]
  [/{$p}block]
[/{$p}segment]
STR;

		return $content;

	}

}

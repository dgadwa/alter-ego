<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Composite_woocommerce_products extends Chap_Composite {

	public function __construct($id, $options) {

		$this->description = esc_html__('WooCommerce products display.', 'chap-shortcodes');

		parent::__construct($id, $options);

	}

	public function get_content() {

		$p = $this->p;

		$content = <<<STR
[{$p}segment vertical stripe]
  [{$p}grid very relaxed two column stackable container]
    [{$p}row two column]
      [{$p}column sixteen wide]
        [{$p}header large mca]On sale[/{$p}header]
        [sale_products per_page="5" columns="5"]
      [/{$p}column]
    [/{$p}row]
    [{$p}row]
      [{$p}column eight wide]
        [{$p}header large mca]Top rated products[/{$p}header]
        [top_rated_products per_page="2" columns="2"]
      [/{$p}column]
      [{$p}column eight wide]
        [{$p}header large mca]Best selling products[/{$p}header]
        [best_selling_products per_page="3" columns="3"]
      [/{$p}column]
    [/{$p}row]
  [/{$p}grid]
[/{$p}segment]
STR;

		return $content;

	}

}

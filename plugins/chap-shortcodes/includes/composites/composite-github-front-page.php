<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Composite_github_front_page extends Chap_Composite {

	public function __construct($id, $options) {

		$this->description = esc_html__('GitHub-inspired front page.', 'chap-shortcodes');

		parent::__construct($id, $options);

	}

	public function get_content() {

		$p = $this->p;

		$content = <<<STR
[{$p}segment huge vertical]
  [{$p}block top="3" bottom="3"]
    [{$p}container center aligned stackable grid]
      [{$p}column fifteen wide]
        [{$p}header huge paddingless]Welcome home, developers[/{$p}header]
        [{$p}header large text gray]GitHub fosters a fast, flexible, and collaborative development process that lets you work on your own or with others.[/{$p}header]
      [/{$p}column]
    [/{$p}container]
  [/{$p}block]
[/{$p}segment]

[{$p}segment feature paddingless vertical]
  [{$p}grid four column center aligned divided relaxed stackable]
    [{$p}row]
      [{$p}column padded alternate][{$p}image small divided src="//placehold.it/512" alt="Build"]
        [{$p}header marginless text normal]For everything you build[/{$p}header]
        [{$p}p]Host and manage your code on GitHub. You can keep your work private or share it with the world.[/{$p}p]
      [/{$p}column]
      [{$p}column padded alternate][{$p}image small divided src="//placehold.it/512" alt="Work"]
        [{$p}header marginless text normal]A better way to work[/{$p}header]
        [{$p}p]From hobbyists to professionals, GitHub helps developers simplify the way they build software.[/{$p}p]
      [/{$p}column]
      [{$p}column padded alternate][{$p}image small divided src="//placehold.it/512" alt="Projects"]
        [{$p}header marginless text normal]Millions of projects[/{$p}header]
        [{$p}p]GitHub is home to millions of open source projects. Try one out or get inspired to create your own.[/{$p}p]
      [/{$p}column]
      [{$p}column padded alternate][{$p}image small divided src="//placehold.it/512" alt="Platform"]
        [{$p}header marginless text normal]One platform, from start to finish[/{$p}header]
        [{$p}p]With hundreds of integrations, GitHub is flexible enough to be at the center of your development process.[/{$p}p]
      [/{$p}column]
    [/{$p}row]
  [/{$p}grid]
[/{$p}segment]


[{$p}segment vertical stripe]
  [{$p}container]
    [{$p}header huge]Who uses GitHub?[/{$p}header]
    [{$p}divider section]
    [{$p}grid three column stackable]
      [{$p}column]
        [{$p}header blue]Individuals[{$p}icon angle right][/{$p}header]
        [{$p}p text gray]Use GitHub to create a personal project, whether you want to experiment with a new programming language or host your life’s work.[/{$p}p]
      [/{$p}column]
      [{$p}column]
        [{$p}header orange]Communities[{$p}icon angle right][/{$p}header]
        [{$p}p text gray]GitHub hosts one of the largest collections of open source software. Create, manage, and work on some of today’s most influential technologies.[/{$p}p]
      [/{$p}column]
      [{$p}column]
        [{$p}header purple]Businesses[{$p}icon angle right][/{$p}header]
        [{$p}p text gray]Businesses of all sizes use GitHub to support their development process and securely build software.[/{$p}p]
      [/{$p}column]
    [/{$p}grid]
    [{$p}divider section]
    [{$p}block ui basic paddingless segment top="5" bottom="-9"]
      [{$p}grid stackable mobile reversed]
        [{$p}column twelve wide]
          [{$p}image src="//placehold.it/1500x500" style="box-shadow:0 0 20px rgba(0,0,0,0.15)" alt="NASA"]
        [/{$p}column]
        [{$p}column four wide middle aligned]
          [{$p}header small]GitHub is proud to host projects and organizations like <a href="#">NASA</a>.[/{$p}header]
        [/{$p}column]
      [/{$p}grid]
    [/{$p}block]
  [/{$p}container]
[/{$p}segment]

[{$p}segment alternate vertical stripe]
  [{$p}container]
    [{$p}ui center aligned segment]
      [{$p}grid padded stackable]
        [{$p}column middle aligned four wide style="border-right: 1px solid #d8dee2"]
          [{$p}button massive fluid positive]Sign up for GitHub[/{$p}button]
        [/{$p}column]
        [{$p}column twelve wide left aligned]
          [{$p}header large]Public projects are always free. Work together across unlimited private repositories for $7 / month.[/{$p}header]
        [/{$p}column]
      [/{$p}grid]
    [/{$p}ui]
  [/{$p}container]
[/{$p}segment]
STR;

		return $content;

	}

}

<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Composite_woocommerce_products_2 extends Chap_Composite {

	public function __construct($id, $options) {

		$this->description = esc_html__('WooCommerce categories and products display for front page.', 'chap-shortcodes');

		parent::__construct($id, $options);

	}

	public function get_content() {

		$p = $this->p;

		$content = <<<STR
[{$p}segment vertical stripe]
  [{$p}container]
    [{$p}header huge center aligned]Product categories[/{$p}header]
    [product_categories parent="0" number="4" columns="4"]
    [{$p}divider hidden section]
    [{$p}header huge center aligned]Top rated products[/{$p}header]
    [top_rated_products per_page="4" columns="4"]
    [{$p}divider hidden section]
    [{$p}header huge center aligned]Best selling products[/{$p}header]
    [best_selling_products per_page="4" columns="4"]
    [{$p}divider hidden section]
    [{$p}header huge center aligned]On sale[/{$p}header]
    [sale_products per_page="4" columns="4"]
  [/{$p}container]
[/{$p}segment]
STR;

		return $content;

	}

}

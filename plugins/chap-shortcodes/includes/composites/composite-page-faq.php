<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Composite_page_faq extends Chap_Composite {

	public function __construct($id, $options) {

		$this->name = esc_html__('FAQ', 'chap-shortcodes');
		$this->description = esc_html__('Frequently asked questions page.', 'chap-shortcodes');

		parent::__construct($id, $options);

	}

	public function get_content() {

		$p = $this->p;

		$content = <<<STR
[{$p}grid two column stackable]
  [{$p}column]
    [{$p}header]Clients[/{$p}header]
    [{$p}accordion]
      [{$p}content title="How do I notify the IRS my address has changed?"][{$p}lorem short][/{$p}content]
      [{$p}content title="What is a split refund?"][{$p}lorem short 2][/{$p}content]
      [{$p}content title="How do I know if I have to file quarterly individual estimated tax payments?"][{$p}lorem short][/{$p}content]
      [{$p}content title="What are the tax changes for this year?"][{$p}lorem short 2][/{$p}content]
    [/{$p}accordion]
  [/{$p}column]
  [{$p}column]
    [{$p}header]Businesses[/{$p}header]
    [{$p}accordion inverted segment]
      [{$p}content title="Leverage agile frameworks to provide a robust synopsis for high level overviews."][{$p}lorem short][/{$p}content]
      [{$p}content title="Bring to the table win-win survival strategies to ensure proactive domination."][{$p}lorem][/{$p}content]
      [{$p}content title="Capitalise on low hanging fruit to identify a ballpark value added activity to beta test."][{$p}lorem long][/{$p}content]
    [/{$p}accordion]
  [/{$p}column]
[/{$p}grid]

[{$p}divider hidden section]

[{$p}header large dividing]About Visas - The Basics[/{$p}header]
[{$p}header small]Visa Types[/{$p}header]
[{$p}accordion styled]
  [{$p}content title="What types of visas are available for people to come to the United States?"][{$p}lorem][/{$p}content]
[/{$p}accordion]
[{$p}header small]After Visa Issuance[/{$p}header]
[{$p}accordion styled]
  [{$p}content title="How do I read and understand my visa?"][{$p}lorem short][/{$p}content]
  [{$p}content title="My visa expires in 5 years, what does this mean?"][{$p}lorem short 2][/{$p}content]
[/{$p}accordion]
[{$p}header small]Visa Validity[/{$p}header]
[{$p}accordion styled]
  [{$p}content title="My old passport has already expired. My visa to travel to the United States is still valid but in my expired passport. Do I need to apply for a new visa with my new passport?"][{$p}lorem][/{$p}content]
  [{$p}content title="My visa will expire while I am in the United States. Is there a problem with that?"][{$p}lorem][/{$p}content]
  [{$p}content title="What are indefinite validity visas (Burroughs visas) and are they still valid?"][{$p}lorem][/{$p}content]
[/{$p}accordion]
[{$p}header small]Administrative Processing[/{$p}header]
[{$p}accordion styled]
  [{$p}content title="What is Administrative Processing?"][{$p}lorem][/{$p}content]
[/{$p}accordion]
STR;

		return $content;

	}

}

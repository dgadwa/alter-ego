<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Composite_paragraph_grid_2 extends Chap_Composite {

	public function __construct($id, $options) {

		$this->description = esc_html__('Paragraph grid with 3 columns.', 'chap-shortcodes');

		parent::__construct($id, $options);

	}

	public function get_content() {

		$p = $this->p;

		$content = <<<STR
[{$p}segment vertical stripe]
  [{$p}grid three columns tablet doubling stackable container]
    [{$p}column]
      [{$p}header tag="h3"]Curabitur luctus[/{$p}header]
      [{$p}p]Duis dignissim mauris vel nibh porttitor laoreet. Vivamus blandit nunc quis lorem scelerisque.[/{$p}p]
    [/{$p}column]
    [{$p}column]
      [{$p}header tag="h3"]Phasellus vitae elit[/{$p}header]
      [{$p}p]Nunc sit amet enim justo. Quisque sodales semper augue, vel tempor lectus tincidunt vel.[/{$p}p]
    [/{$p}column]
    [{$p}column]
      [{$p}header tag="h3"]Suspendisse bibendum[/{$p}header]
      [{$p}p]Ut pharetra hendrerit ipsum, non scelerisque magna placerat tristique. Etiam tellus nibh.[/{$p}p]
    [/{$p}column]
    [{$p}column]
      [{$p}header tag="h3"]Phasellus vitae elit[/{$p}header]
      [{$p}p]Nunc sit amet enim justo. Quisque sodales semper augue, vel tempor lectus tincidunt vel.[/{$p}p]
    [/{$p}column]
    [{$p}column]
      [{$p}header tag="h3"]Suspendisse bibendum[/{$p}header]
      [{$p}p]Ut pharetra hendrerit ipsum, non scelerisque magna placerat tristique. Etiam tellus nibh.[/{$p}p]
    [/{$p}column]
    [{$p}column]
      [{$p}header tag="h3"]Curabitur luctus[/{$p}header]
      [{$p}p]Duis dignissim mauris vel nibh porttitor laoreet. Vivamus blandit nunc quis lorem scelerisque.[/{$p}p]
    [/{$p}column]
  [/{$p}grid]
[/{$p}segment]
STR;

		return $content;

	}

}

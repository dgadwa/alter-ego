<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Composite_info_sections extends Chap_Composite {

	public function __construct($id, $options) {

		$this->description = esc_html__('Info on the left and four internally celled sections on the right.', 'chap-shortcodes');

		parent::__construct($id, $options);

	}

	public function get_content() {

		$p = $this->p;

		$content = <<<STR
[{$p}segment vertical stripe]
  [{$p}container]
    [{$p}ui very relaxed stackable grid]
      [{$p}div six wide middle aligned column]
        [{$p}header small display]It's Not All Header, Header, Header, you know.[/{$p}header]
        [{$p}p]Ut ultrices ultrices enim. Curabitur sit amet mauris. Morbi in dui quis est pulvinar ullamcorper. Nulla facilisi. Integer lacinia sollicitudin massa. Cras metus. Sed aliquet risus a tortor. Integer id quam. Morbi mi. Quisque nisl felis, venenatis tristique, dignissim in, ultrices sit amet, augue. Proin sodales libero eget ante.[/{$p}p]
        [{$p}p]Praesent mauris. Fusce nec tellus sed augue semper porta. Mauris massa. Vestibulum lacinia arcu eget nulla. Aenean quam. In scelerisque sem at dolor. Maecenas mattis. Sed convallis tristique sem.[/{$p}p]
        [{$p}button large primary]Click here to learn more[/{$p}button]
      [/{$p}div]
      [{$p}div ten wide column]
        [{$p}grid two column internally celled very relaxed stackable]
          [{$p}row]
            [{$p}column]
              [{$p}header left aligned icon tag="h3" icon="lightbulb"]Ideas that change the world[/{$p}header]
              [{$p}p]Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero. Sed cursus ante dapibus diam. Sed nisi. Nulla quis.[/{$p}p]
            [/{$p}column]
            [{$p}column]
              [{$p}header left aligned icon tag="h3" icon="users"]The passionate pursuit of users[/{$p}header]
              [{$p}p]Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Curabitur sodales ligula in libero. Sed dignissim.[/{$p}p]
            [/{$p}column]
          [/{$p}row]
          [{$p}row]
            [{$p}column]
              [{$p}header left aligned icon tag="h3" icon="settings"]An option is like a choice, but better[/{$p}header]
              [{$p}p]Mauris ipsum. Nulla metus metus, ullamcorper vel, tincidunt sed, euismod in, nibh. Quisque volutpat condimentum velit. Class aptent taciti sociosqu.[/{$p}p]
            [/{$p}column]
            [{$p}column]
              [{$p}header left aligned icon tag="h3" icon="handshake outline"]Best deal for both parties[/{$p}header]
              [{$p}p]Etiam ultrices. Suspendisse in justo eu magna luctus suscipit. Sed lectus. Integer euismod lacus luctus magna. Quisque cursus, metus vitae.[/{$p}p]
            [/{$p}column]
          [/{$p}row]
        [/{$p}grid]
      [/{$p}div]
    [/{$p}ui]
  [/{$p}container]
[/{$p}segment]
STR;

		return $content;

	}

}

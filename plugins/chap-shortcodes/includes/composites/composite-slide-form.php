<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Composite_slide_form extends Chap_Composite {

	public function __construct($id, $options) {

		$this->name = esc_html__('Sign up', 'chap-shortcodes');
		$this->description = esc_html__('Slide with a sign up form.', 'chap-shortcodes');

		parent::__construct($id, $options);

	}

	public function get_content() {

		$p = $this->p;

		$content = <<<STR
[{$p}grid two column stackable container]
  [{$p}column sixteen wide tablet nine wide computer left aligned middle aligned left floated]
    [{$p}header huge inverted]Built to Make Your Business Better[/{$p}header]
    [{$p}p inverted]In vel mi sit amet augue congue elementum. Morbi in ipsum sit amet pede facilisis laoreet. Donec lacus nunc, viverra nec, blandit vel.
[/{$p}p]
  [/{$p}column]
  [{$p}column six wide large screen only middle aligned right floated]
    [{$p}form big]
      [{$p}field][{$p}input type="text" placeholder="Pick a username"][/{$p}field]
      [{$p}field][{$p}input type="email" placeholder="Your email address"][/{$p}field]
      [{$p}field][{$p}input type="password" placeholder="Create a password"][/{$p}field]
      [{$p}field compact description offwhite text]Use at least one letter, one numeral, and seven characters.[/{$p}field]
      [{$p}field][{$p}button massive fluid positive]Sign up for the service[/{$p}button][/{$p}field]
      [{$p}field description offwhite text]By signing up, you agree to our <a class="white text" href="#tos">terms of service</a> and <a class="white text" href="#pp">privacy policy</a>. <br>We'll occasionally send you account related emails.[/{$p}field]
    [/{$p}form]
  [/{$p}column]
[/{$p}grid]
STR;

		return $content;

	}

}

<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Composite_page_contact extends Chap_Composite {

	public function __construct($id, $options) {

		$this->description = esc_html__('Contact page.', 'chap-shortcodes');

		parent::__construct($id, $options);

	}

	public function get_content() {

		$p = $this->p;

		$content = <<<STR
[{$p}grid two column stackable]
  [{$p}column]
    [{$p}form padded segment]
      [{$p}field label="Your name"]
        [{$p}input name="name"]
      [/{$p}field]
      [{$p}field label="Your e-mail"]
        [{$p}input name="email" type="email"]
      [/{$p}field]
      [{$p}field label="Subject"]
        [{$p}input name="subject"]
      [/{$p}field]
      [{$p}field label="Message"]
        [{$p}textarea name="message"]
      [/{$p}field]
      [{$p}button large primary right labeled icon icon="send" type="submit"]Send[/{$p}button]
    [/{$p}form]
    [{$p}button primary amp-only url="/contact"]Go to contact form[/{$p}button]
  [/{$p}column]
  [{$p}column]
    [{$p}p]Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam rutrum velit diam, eu mattis quam tempor sed. Duis volutpat dictum justo ullamcorper blandit. Cras nulla nisi, scelerisque sed felis quis, placerat congue tortor.[/{$p}p]
    [{$p}header]Address[/{$p}header]
    [{$p}address]Lorem ipsum
    Dolor sit 30
    343434 Amet road[/{$p}address]
    [{$p}header]Contact[/{$p}header]
    [{$p}list large]
      [{$p}item icon="phone"]+123 45 67 890[/{$p}item]
      [{$p}item icon="mail"]<a href="mailto:#">info@loremipsum.com</a>[/{$p}item]
      [{$p}item icon="linkify"]<a href="#">www.loremipsum.com</a>[/{$p}item]
      [{$p}item icon="twitter"]<a href="#">@loremipsum</a>[/{$p}item]
      [{$p}item icon="facebook"]<a href="#">loremipsum</a>[/{$p}item]
    [/{$p}list]
  [/{$p}column]
[/{$p}grid]
STR;

		return $content;

	}

}

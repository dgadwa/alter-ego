<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Composite_page_team_2 extends Chap_Composite {

	public function __construct($id, $options) {

		$this->name = esc_html__('Team (cards)', 'chap-shortcodes');
		$this->description = esc_html__('Team page, displaying team members as cards.', 'chap-shortcodes');

		parent::__construct($id, $options);

	}

	public function get_content() {

		$p = $this->p;

		$content = <<<STR
[{$p}header large mca]Main department[{$p}subheader normal]Suspendisse in justo eu magna luctus suscipit[/{$p}subheader][/{$p}header]
[{$p}cards three stackable]
  [{$p}card]
    [{$p}cardimage src="//placehold.it/512x512.png" /]
    [{$p}cardtext]
      [{$p}header]Elizabeth Lyons[{$p}subheader normal]Founder[/{$p}subheader][/{$p}header]
      [{$p}p]Ex legere laoreet scripserit ius, partem soluta timeam eu vim. Te mea congue aperiam.[/{$p}p]
    [/{$p}cardtext]
    [{$p}cardtext center aligned extra]
      [{$p}button circular facebook icon][{$p}icon facebook /][/{$p}button]
      [{$p}button circular twitter icon][{$p}icon twitter /][/{$p}button]
      [{$p}button circular linkedin icon][{$p}icon linkedin /][/{$p}button]
      [{$p}button circular youtube icon][{$p}icon youtube /][/{$p}button]
      [{$p}button circular secondary icon marginless][{$p}icon mail /][/{$p}button]
    [/{$p}cardtext]
  [/{$p}card]
  [{$p}card]
    [{$p}cardimage src="//placehold.it/512x512.png" /]
    [{$p}cardtext]
      [{$p}header]Terri Diaz[{$p}subheader normal]Joined in 2013[/{$p}subheader][/{$p}header]
      [{$p}p]Te mea congue aperiam, at mei diceret dolorum molestiae.[/{$p}p]
    [/{$p}cardtext]
    [{$p}cardtext center aligned extra]
      [{$p}button circular facebook icon][{$p}icon facebook /][/{$p}button]
      [{$p}button circular twitter icon][{$p}icon twitter /][/{$p}button]
      [{$p}button circular google plus icon][{$p}icon google plus /][/{$p}button]
      [{$p}button circular instagram icon][{$p}icon instagram /][/{$p}button]
      [{$p}button circular secondary icon marginless][{$p}icon mail /][/{$p}button]
    [/{$p}cardtext]
  [/{$p}card]
  [{$p}card]
    [{$p}cardimage src="//placehold.it/512x512.png" /]
    [{$p}cardtext]
      [{$p}header]Rebecca Slattery[{$p}subheader normal]Joined in 2016[/{$p}subheader][/{$p}header]
      [{$p}p]Mauris ipsum. Nulla metus metus, ullamcorper vel, tincidunt sed, euismod in, nibh.[/{$p}p]
    [/{$p}cardtext]
    [{$p}cardtext center aligned extra]
      [{$p}button circular pinterest icon][{$p}icon pinterest /][/{$p}button]
      [{$p}button circular twitter icon][{$p}icon twitter /][/{$p}button]
      [{$p}button circular google plus icon][{$p}icon google plus /][/{$p}button]
      [{$p}button circular youtube icon][{$p}icon youtube /][/{$p}button]
      [{$p}button circular secondary icon marginless][{$p}icon mail /][/{$p}button]
    [/{$p}cardtext]
  [/{$p}card]
[/{$p}cards]

[{$p}divider hidden section /]

[{$p}header large mca]Public relations department[{$p}subheader normal]Integer euismod lacus luctus magna tincidunt[/{$p}subheader][/{$p}header]
[{$p}cards three stackable]
  [{$p}card]
    [{$p}cardimage src="//placehold.it/512x512.png" /]
    [{$p}cardtext]
      [{$p}header]Travis Campbell[{$p}subheader normal]Co-founder[/{$p}subheader][/{$p}header]
      [{$p}p]Mauris ipsum. Nulla metus metus, ullamcorper vel, tincidunt sed, euismod in.[/{$p}p]
    [/{$p}cardtext]
    [{$p}cardtext center aligned extra]
      [{$p}button circular pinterest icon][{$p}icon pinterest /][/{$p}button]
      [{$p}button circular twitter icon][{$p}icon twitter /][/{$p}button]
      [{$p}button circular google plus icon][{$p}icon google plus /][/{$p}button]
      [{$p}button circular youtube icon][{$p}icon youtube /][/{$p}button]
      [{$p}button circular secondary icon marginless][{$p}icon mail /][/{$p}button]
    [/{$p}cardtext]
  [/{$p}card]
  [{$p}card]
    [{$p}cardimage src="//placehold.it/512x512.png" /]
    [{$p}cardtext]
      [{$p}header]Helen Flores[{$p}subheader normal]Joined in 2014[/{$p}subheader][/{$p}header]
      [{$p}p]Ex legere laoreet scripserit ius, partem soluta timeam eu vim. Te mea congue aperiam.[/{$p}p]
    [/{$p}cardtext]
    [{$p}cardtext center aligned extra]
      [{$p}button circular facebook icon][{$p}icon facebook /][/{$p}button]
      [{$p}button circular twitter icon][{$p}icon twitter /][/{$p}button]
      [{$p}button circular linkedin icon][{$p}icon linkedin /][/{$p}button]
      [{$p}button circular youtube icon][{$p}icon youtube /][/{$p}button]
      [{$p}button circular secondary icon marginless][{$p}icon mail /][/{$p}button]
    [/{$p}cardtext]
  [/{$p}card]
  [{$p}card]
    [{$p}cardimage src="//placehold.it/512x512.png" /]
    [{$p}cardtext]
      [{$p}header]Debby Bedoya[{$p}subheader normal]Joined in 2012[/{$p}subheader][/{$p}header]
      [{$p}p]Te mea congue aperiam, at mei diceret dolorum molestiae.[/{$p}p]
    [/{$p}cardtext]
    [{$p}cardtext center aligned extra]
      [{$p}button circular facebook icon][{$p}icon facebook /][/{$p}button]
      [{$p}button circular twitter icon][{$p}icon twitter /][/{$p}button]
      [{$p}button circular google plus icon][{$p}icon google plus /][/{$p}button]
      [{$p}button circular instagram icon][{$p}icon instagram /][/{$p}button]
      [{$p}button circular secondary icon marginless][{$p}icon mail /][/{$p}button]
    [/{$p}cardtext]
  [/{$p}card]
[/{$p}cards]
STR;

		return $content;

	}

}

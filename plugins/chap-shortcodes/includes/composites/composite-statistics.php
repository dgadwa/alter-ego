<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Composite_statistics extends Chap_Composite {

	public function __construct($id, $options) {

		$this->description = esc_html__('Four animated statistics.', 'chap-shortcodes');

		parent::__construct($id, $options);

	}

	public function get_content() {

		$p = $this->p;

		$content = <<<STR
[{$p}segment vertical stripe]
  [{$p}container]
    [{$p}grid center aligned]
      [{$p}row four column doubling]
        [{$p}column]
          [{$p}statistic massive label="Saves" anim="fade-down"]39[/{$p}statistic]
        [/{$p}column]
        [{$p}column]
          [{$p}statistic massive label="Signups" anim="fade-down" anim-delay="50"]525[/{$p}statistic]
        [/{$p}column]
        [{$p}column]
          [{$p}statistic massive label="Flights" icon="plane" anim="fade-down" anim-delay="100"]5[/{$p}statistic]
        [/{$p}column]
        [{$p}column]
          [{$p}statistic massive label="Members" icon="user" anim="fade-down" anim-delay="150"]23[/{$p}statistic]
        [/{$p}column]
      [/{$p}row]
    [/{$p}grid]
  [/{$p}container]
[/{$p}segment]
STR;

		return $content;

	}

}

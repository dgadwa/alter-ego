<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Composite_slide_search extends Chap_Composite {

	public function __construct($id, $options) {

		$this->description = esc_html__('Slide with a search form.', 'chap-shortcodes');

		parent::__construct($id, $options);

	}

	public function get_content() {

		$p = $this->p;

		$content = <<<STR
[{$p}container text]
  [{$p}segments stacked]
    [{$p}segment]
      [{$p}header large marginless icon icon="search"]
        Query our numerous posts
        [{$p}subheader]You might find something interesting[/{$p}subheader]
      [/{$p}header]
    [/{$p}segment]
    [{$p}segment primary secondary]
      [{$p}form big method="get" action="/"]
        [{$p}input type="hidden" name="post_type" value="post"][/{$p}input]
        [{$p}input fluid action name="s" type="text" placeholder="What would you like to read about?"]
          [{$p}button big primary type="submit"]Find posts[/{$p}button]
        [/{$p}input]
      [/{$p}form]
    [/{$p}segment]
  [/{$p}segments]
[/{$p}container]
STR;

		return $content;

	}

}

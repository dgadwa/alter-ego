<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Composite_sections extends Chap_Composite {

	public function __construct($id, $options) {

		$this->description = esc_html__('Full width sections with backgrounds.', 'chap-shortcodes');

		parent::__construct($id, $options);

	}

	public function get_content() {

		$p = $this->p;

		$content = <<<STR
[{$p}grid two column stackable]
  [{$p}segment basic inverted black marginless column]
    [{$p}block fluid text container big basic padded segment top="6" bottom="6" units="vw"]
      [{$p}header huge inverted tag="h3"]Nunc ut bibendum lorem[/{$p}header]
      [{$p}p inverted]Ex legere laoreet scripserit ius, partem soluta timeam eu vim. Te mea congue aperiam, at mei diceret dolorum molestiae. Placerat complectitur vim ex.[/{$p}p]
      [{$p}p inverted]Lorem ipsum dolor sit amet, dolor maiorum ius ei, sea omnesque verterem ne. Facer decore theophrastus ne est, ea vim quot fierent eloquentiam, in reque porro mei.[/{$p}p]
      [{$p}button big inverted]Read more[/{$p}button]
    [/{$p}block]
  [/{$p}segment]
  [{$p}segment basic inverted primary marginless column]
    [{$p}block fluid text container big basic padded segment top="6" bottom="6" units="vw"]
      [{$p}header huge tag="h3"]Partem soluta timeam[/{$p}header]
      [{$p}p]Lorem ipsum dolor sit amet, dolor maiorum ius ei, sea omnesque verterem ne. Facer decore theophrastus ne est, ea vim quot fierent eloquentiam, in reque porro mei.[/{$p}p]
      [{$p}p]Ex legere laoreet scripserit ius, partem soluta timeam eu vim. Te mea congue aperiam, at mei diceret dolorum molestiae. Placerat complectitur vim ex.[/{$p}p]
      [{$p}button big secondary]Read more[/{$p}button]
    [/{$p}block]
  [/{$p}segment]
[/{$p}grid]
STR;

		return $content;

	}

}

<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Composite_slide_call_to_action extends Chap_Composite {

	public function __construct($id, $options) {

		$this->description = esc_html__('Slide with a call to action.', 'chap-shortcodes');

		parent::__construct($id, $options);

	}

	public function get_content() {

		$p = $this->p;

		$content = <<<STR
[{$p}header inverted]Earn more from your apps the smart way[/{$p}header]
[{$p}button primary]Sign up for AdBob[/{$p}button]
[{$p}block top="0.2"]
  [{$p}header inverted tiny]
    [{$p}subheader normal]Or, <a href="#">request initial consultation</a>.[/{$p}subheader]
  [/{$p}header]
[/{$p}block]
STR;

		return $content;

	}

}

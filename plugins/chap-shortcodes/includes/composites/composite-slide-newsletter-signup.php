<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Composite_slide_newsletter_signup extends Chap_Composite {

	public function __construct($id, $options) {

		$this->description = esc_html__('Slide with a newsletter sign-up form.', 'chap-shortcodes');

		parent::__construct($id, $options);

	}

	public function get_content() {

		$p = $this->p;

		$content = <<<STR
[{$p}container text]
  [{$p}segments stacked]
    [{$p}segment padded transparent]
      [{$p}header large icon icon="send"]Newsletter signup[/{$p}header]
      [{$p}subheader text muted]We never send spam e-mail, promise![/{$p}subheader]
    [/{$p}segment]
    [{$p}segment primary secondary]
      [{$p}form big method="post" action="#"]
        [{$p}input fluid action name="email" type="email" placeholder="E-mail"]
          [{$p}button big primary type="submit"]Sign-up[/{$p}button]
        [/{$p}input]
      [/{$p}form]
    [/{$p}segment]
  [/{$p}segments]
[/{$p}container]
STR;

		return $content;

	}

}

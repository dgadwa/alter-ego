<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Composite_slide_product extends Chap_Composite {

	public function __construct($id, $options) {

		$this->description = esc_html__('Slide to display a product.', 'chap-shortcodes');

		parent::__construct($id, $options);

	}

	public function get_content() {

		$p = $this->p;

		$content = <<<STR
[{$p}grid equal width stackable container]
  [{$p}column computer only center aligned middle aligned]
    [{$p}image src="//placehold.it/400"]
  [/{$p}column]
  [{$p}column left aligned middle aligned]
    [{$p}list inverted]
      [{$p}item]
        [{$p}header]Lorem ipsum dolor sit amet[/{$p}header]
      [/{$p}item]
      [{$p}item]
        [{$p}segment basic paddingless inverted]
          [{$p}p]Lorem ipsum dolor sit amet, dolor maiorum ius ei, sea omnesque verterem ne. Facer decore ne est, ea vim quot fierent eloquentiam, in reque porro mei. Ex legere laoreet scripserit ius, partem soluta timeam eu vim.[/{$p}p]
        [/{$p}segment]
      [/{$p}item]
      [{$p}item]
        [{$p}button adjacent to]$555.55[/{$p}button]
        [{$p}button right floated right labeled positive icon icon="right arrow"]View product[/{$p}button]
      [/{$p}item]
    [/{$p}list]
  [/{$p}column]
[/{$p}grid]
STR;

		return $content;

	}

}

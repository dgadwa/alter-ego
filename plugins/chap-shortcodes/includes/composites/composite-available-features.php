<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Composite_available_features extends Chap_Composite {

	public function __construct($id, $options) {

		$this->description = esc_html__('Indicates available features.', 'chap-shortcodes');

		parent::__construct($id, $options);

	}

	public function get_content() {

		$p = $this->p;

		$content = <<<STR
[{$p}ui vertical stripe segment]
  [{$p}grid relaxed three columns tablet doubling stackable container]
    [{$p}column]
      [{$p}segment padded center aligned]
        [{$p}header large icon icon="green check"]Curabitur luctus[/{$p}header]
        [{$p}p]Duis dignissim mauris vel nibh porttitor laoreet. Vivamus blandit nunc quis lorem scelerisque.[/{$p}p]
      [/{$p}segment]
    [/{$p}column]
    [{$p}column]
      [{$p}segment padded center aligned]
        [{$p}header large icon icon="green check"]Phasellus vitae elit[/{$p}header]
        [{$p}p]Nunc sit amet enim justo. Quisque sodales semper augue, vel tempor lectus tincidunt vel.[/{$p}p]
      [/{$p}segment]
    [/{$p}column]
    [{$p}column]
      [{$p}segment padded center aligned]
        [{$p}header large icon icon="green check"]Suspendisse bibendum[/{$p}header]
        [{$p}p]Ut pharetra hendrerit ipsum, non scelerisque magna placerat tristique. Etiam tellus nibh.[/{$p}p]
      [/{$p}segment]
    [/{$p}column]
  [/{$p}grid]
[/{$p}ui]
STR;

		return $content;

	}

}

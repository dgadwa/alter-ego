<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Composite_page_plans extends Chap_Composite {

	public function __construct($id, $options) {

		$this->description = esc_html__('Plans selection page.', 'chap-shortcodes');

		parent::__construct($id, $options);

	}

	public function get_content() {

		$p = $this->p;

		$content = <<<STR
[{$p}divider hidden section amp-invisible]
[{$p}grid three columns stackable container]
  [{$p}column paddingless anim="fade-right"]
    [{$p}segments plan]
      [{$p}segment center aligned]
        [{$p}header large]
          Cheap package
          [{$p}subheader normal]€5/month per user[/{$p}subheader]
        [/{$p}header]
      [/{$p}segment]
      [{$p}segment paddingless amp-invisible]
        [{$p}image fluid src="//placehold.it/300x160" alt="Cheap"]
          [{$p}label red ribbon]Discount[/{$p}label]
        [/{$p}image]
      [/{$p}segment]
      [{$p}segment red padded]
        [{$p}header marginless]Suspendisse bibendum[/{$p}header]
        [{$p}p]Ut pharetra hendrerit ipsum, non scelerisque magna placerat tristique. Etiam tellus nibh.[/{$p}p]
      [/{$p}segment]
      [{$p}segment secondary center aligned]
        [{$p}button big basic red]Try now for free[/{$p}button]
      [/{$p}segment]
      [{$p}segment]
        [{$p}header icon="check"]Eu nec esse splendide[/{$p}header]
      [/{$p}segment]
      [{$p}segment]
        [{$p}header icon="check"]Libris facilisis in duo[/{$p}header]
      [/{$p}segment]
      [{$p}segment]
        [{$p}header icon="user"]10+ mel meis tollit[/{$p}header]
      [/{$p}segment]
    [/{$p}segments]
  [/{$p}column]
  [{$p}column paddingless]
    [{$p}segments large stacked raised featured plan]
      [{$p}segment center aligned]
        [{$p}header large]
          Basic package
          [{$p}subheader normal]€15/month per user[/{$p}subheader]
        [/{$p}header]
      [/{$p}segment]
      [{$p}segment paddingless amp-invisible]
        [{$p}image fluid src="//placehold.it/300x160" alt="Cheap"]
      [/{$p}segment]
      [{$p}segment primary padded]
        [{$p}header marginless]Suspendisse bibendum[/{$p}header]
        [{$p}p]Ut pharetra hendrerit ipsum, non scelerisque magna placerat tristique. Etiam tellus nibh.[/{$p}p]
      [/{$p}segment]
      [{$p}segment secondary center aligned]
        [{$p}button huge basic primary]Try now for free[/{$p}button]
      [/{$p}segment]
      [{$p}segment]
        [{$p}header icon="check"]Eu nec esse splendide[/{$p}header]
      [/{$p}segment]
      [{$p}segment]
        [{$p}header icon="check"]Libris facilisis in duo[/{$p}header]
      [/{$p}segment]
      [{$p}segment]
        [{$p}header icon="user"]15+ mel meis tollit[/{$p}header]
      [/{$p}segment]
    [/{$p}segments]
  [/{$p}column]
  [{$p}column paddingless anim="fade-left"]
    [{$p}segments]
      [{$p}segment center aligned]
        [{$p}header large]
          VIP package
          [{$p}subheader normal]€250/month unlimited users[/{$p}subheader]
        [/{$p}header]
      [/{$p}segment]
      [{$p}segment paddingless amp-invisible]
        [{$p}label large green right corner icon="star"]
        [{$p}image fluid src="//placehold.it/300x160" alt="Cheap"]
      [/{$p}segment]
      [{$p}segment green padded]
        [{$p}header marginless]Suspendisse bibendum[/{$p}header]
        [{$p}p]Ut pharetra hendrerit ipsum, non scelerisque magna placerat tristique. Etiam tellus nibh.[/{$p}p]
      [/{$p}segment]
      [{$p}segment secondary center aligned]
        [{$p}button big basic green]Try now for free[/{$p}button]
      [/{$p}segment]
      [{$p}segment]
        [{$p}header icon="check"]Eu nec esse splendide[/{$p}header]
      [/{$p}segment]
      [{$p}segment]
        [{$p}header icon="check"]Libris facilisis in duo[/{$p}header]
      [/{$p}segment]
      [{$p}segment]
        [{$p}header icon="user"]50+ mel meis tollit[/{$p}header]
      [/{$p}segment]
    [/{$p}segments]
  [/{$p}column]
[/{$p}grid]
STR;

		return $content;

	}

}

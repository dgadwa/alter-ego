<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Composite_page_about extends Chap_Composite {

	public function __construct($id, $options) {

		$this->description = esc_html__('About page.', 'chap-shortcodes');

		parent::__construct($id, $options);

	}

	public function get_content() {

		$p = $this->p;

		$content = <<<STR
[{$p}image medium rounded zoomable right floated src="//placehold.it/500x420" alt="Placeholder image" /]

[{$p}p]Sed ullamcorper ante eros, ut ultricies orci suscipit eget. Fusce ac urna nec turpis malesuada feugiat eu vel leo. Proin luctus enim sit amet sollicitudin blandit. Phasellus elementum est eget urna laoreet malesuada. Phasellus tincidunt sollicitudin arcu vitae rhoncus. Donec bibendum lectus ut urna rhoncus lobortis. Vestibulum pharetra fringilla dui at condimentum. Ut non metus in nulla bibendum tempor. In nunc lacus, commodo vel tempor ut, tristique at tortor. Integer erat odio, venenatis eget leo id, viverra maximus nibh. Quisque eget purus maximus, dapibus magna in, condimentum enim. Maecenas ac lorem hendrerit nisl lobortis porta eu in velit. Vestibulum massa felis, dapibus nec aliquam non, mollis vitae dui. Cras mollis varius facilisis. Nam tempor ac tellus quis blandit. Quisque sapien tellus, accumsan et consequat et, vestibulum nec dui.[/{$p}p]

[{$p}p]Vestibulum rhoncus fringilla tincidunt. Ut eleifend vehicula scelerisque. Fusce blandit, metus nec molestie pellentesque, felis ex elementum orci, sed molestie elit felis eu odio. Integer nulla lacus, mollis a condimentum a, elementum quis nulla. Maecenas bibendum, ipsum vitae volutpat mattis, arcu augue eleifend mi, et aliquam neque felis ac erat. Maecenas consequat libero sit amet lacus aliquet ultricies. Duis erat nisi, sollicitudin sed arcu et, viverra ultrices urna. Nam id sagittis est. Duis venenatis quis metus non ultrices. Suspendisse vel tortor vitae justo malesuada condimentum venenatis consequat dui. Mauris tincidunt congue orci, nec congue sem accumsan vel. Quisque tempus quis eros sit amet tempus. Vestibulum sed nisi ut nunc sagittis cursus. Donec eget lobortis libero. Aliquam id pellentesque dolor.[/{$p}p]

[{$p}divider section]

[{$p}grid equal width stackable]
  [{$p}column]
    [{$p}header large icon="briefcase"]What we do[/{$p}header]
    [{$p}p]Aliquam at ipsum diam. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Suspendisse a magna a tellus fermentum ultricies sit amet vitae nunc. Morbi id hendrerit enim. Sed erat ipsum, gravida vitae pellentesque eget, rutrum in elit. Fusce dictum egestas nisl, at porta tortor ullamcorper a.[/{$p}p]
  [/{$p}column]
  [{$p}column]
    [{$p}header large icon="bar chart"]How well we've done it[/{$p}header]
    [{$p}p]Nulla finibus quam sit amet massa interdum, et hendrerit arcu aliquam. Proin scelerisque nibh a quam dictum interdum. Nunc blandit a tortor facilisis maximus. Morbi pharetra nibh sit amet quam aliquet porttitor. Pellentesque vitae risus fermentum, mollis nunc eget, consequat metus.[/{$p}p]
    [{$p}p]Quisque non est quis nulla condimentum blandit in vitae lorem. Donec iaculis malesuada magna rutrum molestie. Nam eget lobortis quam. Suspendisse ac quam nulla. Curabitur feugiat tristique ligula in vulputate. Vivamus mi sapien, tempor id justo eget, feugiat varius augue. Praesent lobortis lectus metus. Nullam sed eleifend diam.[/{$p}p]
  [/{$p}column]
[/{$p}grid]
STR;

		return $content;

	}

}

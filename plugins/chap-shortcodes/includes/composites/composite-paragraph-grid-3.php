<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Composite_paragraph_grid_3 extends Chap_Composite {

	public function __construct($id, $options) {

		$this->description = esc_html__('Paragraph grid with icons and subheaders.', 'chap-shortcodes');

		parent::__construct($id, $options);

	}

	public function get_content() {

		$p = $this->p;

		$content = <<<STR
[{$p}segment vertical stripe]
  [{$p}grid relaxed three columns tablet doubling stackable container]
    [{$p}column]
      [{$p}header large icon="cloud"]Curabitur luctus[/{$p}header]
      [{$p}p]Duis dignissim mauris vel nibh porttitor laoreet. Vivamus blandit nunc quis lorem scelerisque.[/{$p}p]
    [/{$p}column]
    [{$p}column]
      [{$p}header large icon="comments"]Phasellus vitae elit[/{$p}header]
      [{$p}p]Nunc sit amet enim justo. Quisque sodales semper augue, vel tempor lectus tincidunt vel.[/{$p}p]
    [/{$p}column]
    [{$p}column]
      [{$p}header large icon="image"]Suspendisse bibendum[/{$p}header]
      [{$p}p]Ut pharetra hendrerit ipsum, non scelerisque magna placerat tristique. Etiam tellus nibh.[/{$p}p]
    [/{$p}column]
    [{$p}column]
      [{$p}header large icon="registered"]
        Phasellus vitae elit
        [{$p}subheader normal]Etiam tellus nibh[/{$p}subheader]
      [/{$p}header]
      [{$p}p]Nunc sit amet enim justo. Quisque sodales semper augue, vel tempor lectus tincidunt vel.[/{$p}p]
    [/{$p}column]
    [{$p}column]
      [{$p}header large icon="shopping bag"]
        Suspendisse bibendum
        [{$p}subheader normal]Vivamus blandit nunc[/{$p}subheader]
      [/{$p}header]
      [{$p}p]Ut pharetra hendrerit ipsum, non scelerisque magna placerat tristique. Etiam tellus nibh.[/{$p}p]
    [/{$p}column]
    [{$p}column]
      [{$p}header large icon="sitemap"]
        Curabitur luctus
        [{$p}subheader normal]Quisque sodales semper[/{$p}subheader]
      [/{$p}header]
      [{$p}p]Duis dignissim mauris vel nibh porttitor laoreet. Vivamus blandit nunc quis lorem scelerisque.[/{$p}p]
    [/{$p}column]
  [/{$p}grid]
[/{$p}segment]
STR;

		return $content;

	}

}

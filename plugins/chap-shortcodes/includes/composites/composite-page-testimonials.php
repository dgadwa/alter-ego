<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Composite_page_testimonials extends Chap_Composite {

	public function __construct($id, $options) {

		$this->description = esc_html__('Testimonials page.', 'chap-shortcodes');

		parent::__construct($id, $options);

	}

	public function get_content() {

		$p = $this->p;

		$content = <<<STR
[{$p}grid two column stackable]
  [{$p}column ten wide]
    [{$p}testimonial name="John Doe" occupation="Company, CEO" image="//placehold.it/150"][{$p}lorem short][/{$p}testimonial]
  [/{$p}column]
  [{$p}column ten wide right floated]
    [{$p}testimonial name="Johnson Doe" occupation="Company, CFO" image="//placehold.it/150"][{$p}lorem short][/{$p}testimonial]
  [/{$p}column]
  [{$p}column]
    [{$p}testimonial name="John Doeson" occupation="Company, CPU" image="//placehold.it/150"][{$p}lorem short][/{$p}testimonial]
  [/{$p}column]
  [{$p}column sixteen wide]
    [{$p}testimonial name="Johnny Doeb" occupation="Company, SEO" image="//placehold.it/150"][{$p}lorem long][/{$p}testimonial]
  [/{$p}column]
[/{$p}grid]
STR;

		return $content;

	}

}

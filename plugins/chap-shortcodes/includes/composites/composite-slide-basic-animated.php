<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Composite_slide_basic_animated extends Chap_Composite {

	public function __construct($id, $options) {

		$this->description = esc_html__('Basic slide with animations.', 'chap-shortcodes');

		parent::__construct($id, $options);

	}

	public function get_content() {

		$p = $this->p;

		$content = <<<STR
[{$p}header large inverted anim="slide-left" anim-delay="100"]Lorem ipsum dolor sit![/{$p}header]
[{$p}p inverted ellipsis anim="slide-left" anim-delay="200"]Lorem ipsum dolor sit amet, dolor maiorum ius ei, sea omnesque verterem ne. Facer decore theophrastus ne est, ea vim quot fierent eloquentiam, in reque porro mei. Ex legere laoreet scripserit ius, partem soluta timeam eu vim. Te mea congue aperiam, at mei diceret dolorum molestiae. Placerat complectitur vim ex.[/{$p}p]
[{$p}button inverted anim="slide-left" anim-delay="300"]Sed purus aliquet neque![/{$p}button]
STR;

		return $content;

	}

}

<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Composite_feature_columns_2 extends Chap_Composite {

	public function __construct($id, $options) {

		$this->description = esc_html__('Left aligned feature columns.', 'chap-shortcodes');

		parent::__construct($id, $options);

	}

	public function get_content() {

		$p = $this->p;

		$content = <<<STR
[{$p}segment vertical stripe]
  [{$p}grid middle aligned equal width very relaxed stackable container]
    [{$p}column]
      [{$p}header tag="h3" normal text left aligned icon icon="paint brush"]Etiam sed suscipit ex[/{$p}header]
      [{$p}p]Maecenas sollicitudin enim vitae ligula tempus, auctor elementum orci mattis. Aliquam erat volutpat. Proin vestibulum dui vel nibh faucibus, quis semper mauris posuere.[/{$p}p]
      [{$p}button]Cras gravida[{$p}icon right arrow][/{$p}button]
    [/{$p}column]
    [{$p}column]
      [{$p}header tag="h3" normal text left aligned icon icon="code"]Praesent at ex vitae lacus rutrum[/{$p}header]
      [{$p}p]Etiam porttitor dolor ante, non hendrerit dui aliquet eget. Nulla vitae aliquam dolor. In feugiat euismod purus, vel tincidunt nunc porta et. Nullam pulvinar finibus dapibus.[/{$p}p]
      [{$p}button]Vivamus[{$p}icon right arrow][/{$p}button]
    [/{$p}column]
  [/{$p}grid]
[/{$p}segment]
STR;

		return $content;

	}

}

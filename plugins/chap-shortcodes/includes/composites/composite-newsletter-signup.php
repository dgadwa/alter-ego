<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Composite_newsletter_signup extends Chap_Composite {

	public function __construct($id, $options) {

		$this->description = esc_html__('Newsletter sign-up form.', 'chap-shortcodes');

		parent::__construct($id, $options);

	}

	public function get_content() {

		$p = $this->p;

		$content = <<<STR
[{$p}segment alternate email vertical stripe]
  [{$p}container center aligned grid]
    [{$p}column]
      [{$p}header large]Lorem ipsum dolor[/{$p}header]
      [{$p}form method="post" action="#"]
        [{$p}input huge fluid action name="email" type="email" placeholder="E-mail"]
          [{$p}button huge secondary type="submit"]Sign-up[/{$p}button]
        [/{$p}input]
      [/{$p}form]
    [/{$p}column]
  [/{$p}container]
[/{$p}segment]
STR;

		return $content;

	}

}

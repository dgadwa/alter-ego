<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Composite_newsletter_signup_2 extends Chap_Composite {

	public function __construct($id, $options) {

		$this->description = esc_html__('Newsletter sign-up form.', 'chap-shortcodes');

		parent::__construct($id, $options);

	}

	public function get_content() {

		$p = $this->p;

		$content = <<<STR
[{$p}segment email vertical stripe]
  [{$p}container blue padded segment]
    [{$p}grid equal width]
      [{$p}column]
        [{$p}header large]Lorem ipsum dolor sit amet...[{$p}subheader normal]We never send spam e-mail, promise![/{$p}subheader][/{$p}header]
      [/{$p}column]
      [{$p}column middle aligned]
        [{$p}form method="post" action="#"]
          [{$p}input fluid action name="email" type="email" placeholder="E-mail"]
            [{$p}button primary type="submit"]Sign-up[/{$p}button]
          [/{$p}input]
        [/{$p}form]
      [/{$p}column]
    [/{$p}grid]
  [/{$p}container]
[/{$p}segment]
STR;

		return $content;

	}

}

<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Composite_paragraph_grid extends Chap_Composite {

	public function __construct($id, $options) {

		$this->description = esc_html__('Paragraph grid with 2 columns.', 'chap-shortcodes');

		parent::__construct($id, $options);

	}

	public function get_content() {

		$p = $this->p;

		$content = <<<STR
[{$p}segment vertical stripe]
  [{$p}grid two columns stackable container]
    [{$p}column]
      [{$p}header tag="h3"]Curabitur luctus[/{$p}header]
      [{$p}p]Duis dignissim mauris vel nibh porttitor laoreet. Vivamus blandit nunc quis lorem vestibulum scelerisque. Donec quis commodo lacus. Morbi sed consequat odio.[/{$p}p]
    [/{$p}column]
    [{$p}column]
      [{$p}header tag="h3"]Phasellus vitae elit[/{$p}header]
      [{$p}p]Nunc sit amet enim justo. Quisque sodales semper augue, vel tempor lectus tincidunt vel. Morbi elementum id metus id sodales.[/{$p}p]
    [/{$p}column]
    [{$p}column]
      [{$p}header tag="h3"]Suspendisse bibendum[/{$p}header]
      [{$p}p]Ut pharetra hendrerit ipsum, non scelerisque magna placerat tristique. Etiam tellus nibh, finibus quis efficitur sit amet, mattis quis urna.[/{$p}p]
    [/{$p}column]
    [{$p}column]
      [{$p}header tag="h3"]Vestibulum id nunc[/{$p}header]
      [{$p}p]Fusce id commodo neque. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.[/{$p}p]
    [/{$p}column]
  [/{$p}grid]
[/{$p}segment]
STR;

		return $content;

	}

}

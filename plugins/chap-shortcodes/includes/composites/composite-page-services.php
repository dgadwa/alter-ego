<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Composite_page_services extends Chap_Composite {

	public function __construct($id, $options) {

		$this->description = esc_html__('Page listing services in segments, with a slider of testimonials on the bottom.', 'chap-shortcodes');

		parent::__construct($id, $options);

	}

	public function get_content() {

		$p = $this->p;

		$content = <<<STR
[{$p}grid very relaxed stackable]
  [{$p}column ten wide left floated]
    [{$p}segment big primary padded]
      [{$p}header large left aligned icon icon="users"]Make Every Social Count[/{$p}header]
      [{$p}p]Lorem ipsum dolor sit amet, dolor maiorum ius ei, sea omnesque verterem ne. Facer decore theophrastus ne est, ea vim quot fierent eloquentiam, in reque porro mei.[/{$p}p]
      [{$p}button right labeled icon icon="right angle"]Learn more[/{$p}button]
    [/{$p}segment]
  [/{$p}column]
  [{$p}column ten wide right floated]
    [{$p}segment big grey padded]
      [{$p}header large left aligned icon icon="building"]Mild Green Enterprise Liquid[/{$p}header]
      [{$p}p]Ex legere laoreet scripserit ius, partem soluta timeam eu vim. Te mea congue aperiam, at mei diceret dolorum molestiae. Placerat complectitur vim ex.[/{$p}p]
      [{$p}button right labeled icon icon="right angle"]Learn more[/{$p}button]
    [/{$p}segment]
  [/{$p}column]
  [{$p}column ten wide left floated]
    [{$p}segment big primary padded]
      [{$p}header large left aligned icon icon="database"]The Database of a New Generation[/{$p}header]
      [{$p}p]Facer decore theophrastus ne est, ea vim quot porro mei. Ex legere laoreet scripserit ius, partem soluta timeam eu vim. Te mea congue aperiam, at mei diceret dolorum molestiae.[/{$p}p]
      [{$p}button right labeled icon icon="right angle"]Learn more[/{$p}button]
    [/{$p}segment]
  [/{$p}column]
  [{$p}column ten wide right floated]
    [{$p}segment big grey padded]
      [{$p}header large left aligned icon icon="table"]Does the Hard Returns, <br>So You Don't Have to[/{$p}header]
      [{$p}p]Ex legere laoreet scripserit ius, partem soluta timeam eu vim. Te mea congue aperiam, at mei diceret dolorum molestiae. Placerat complectitur vim ex.[/{$p}p]
      [{$p}button right labeled icon icon="right angle"]Learn more[/{$p}button]
    [/{$p}segment]
  [/{$p}column]
  [{$p}column ten wide left floated]
    [{$p}segment big primary padded]
      [{$p}header large left aligned icon icon="file"]The Sweet You Can Eat Between Meals Without Ruining Your File[/{$p}header]
      [{$p}p]Lorem ipsum dolor sit amet, dolor maiorum ius ei, sea omnesque verterem ne. Facer decore theophrastus ne est, ea vim quot fierent eloquentiam, in reque porro mei.[/{$p}p]
      [{$p}button right labeled icon icon="right angle"]Learn more[/{$p}button]
    [/{$p}segment]
  [/{$p}column]
  [{$p}column ten wide right floated]
    [{$p}segment big grey padded]
      [{$p}header large left aligned icon icon="dollar"]Money - Something For Everyone[/{$p}header]
      [{$p}p]Ex legere laoreet scripserit ius, partem soluta timeam eu vim. Te mea congue aperiam, at mei diceret dolorum molestiae. Placerat complectitur vim ex.[/{$p}p]
      [{$p}button right labeled icon icon="right angle"]Learn more[/{$p}button]
    [/{$p}segment]
  [/{$p}column]
[/{$p}grid]

[{$p}divider hidden section]
[{$p}divider hidden section]

[{$p}header large mca]What our clients say about our services...[/{$p}header]
[{$p}divider hidden amp-invisible]
[{$p}slider slides_per_view="2,1,1" scrollbar="true" space_between="30" grab_cursor="true" amp_type="slides" amp_height="320"]
  [{$p}slide]
    [{$p}testimonial primary name="John Doe" occupation="Company, CEO" image="//placehold.it/150"]
      [{$p}p]Ex legere laoreet scripserit ius, partem soluta timeam eu vim. Lorem ipsum dolor sit amet, dolor maiorum ius ei, sea omnesque verterem ne. Facer decore theophrastus ne est, ea vim quot fierent eloquentiam, in reque porro mei. Ex legere laoreet scripserit ius, partem soluta timeam eu vim. Te mea congue aperiam, at mei diceret dolorum molestiae. Placerat complectitur vim ex.[/{$p}p]
    [/{$p}testimonial]
  [/{$p}slide]
  [{$p}slide]
    [{$p}testimonial grey name="Johnson Doe" occupation="Organization, CEO" image="//placehold.it/150"]
      [{$p}p]Facer decore theophrastus ne est, ea vim quot fierent eloquentiam, in reque porro mei. Facer decore theophrastus ne est, ea vim quot fierent eloquentiam. Ex legere laoreet scripserit ius, partem soluta timeam eu vim.[/{$p}p]
    [/{$p}testimonial]
  [/{$p}slide]
  [{$p}slide]
    [{$p}testimonial primary name="John Doe" occupation="Company, CEO" image="//placehold.it/150"]
      [{$p}p]Lorem ipsum dolor sit amet, dolor maiorum ius ei, sea omnesque verterem ne. Facer decore theophrastus ne est, ea vim quot fierent eloquentiam, in reque porro mei. Ex legere laoreet scripserit ius, partem soluta timeam eu vim. Te mea congue aperiam, at mei diceret dolorum molestiae. Placerat complectitur vim ex.[/{$p}p]
    [/{$p}testimonial]
  [/{$p}slide]
  [{$p}slide]
    [{$p}testimonial grey name="Johnson Doe" occupation="Organization, CEO" image="//placehold.it/150"]
      [{$p}p]Facer decore theophrastus ne est, ea vim quot fierent eloquentiam, in reque porro mei. Te mea congue aperiam, at mei diceret dolorum molestiae. Placerat complectitur vim ex. Facer decore theophrastus ne est, ea vim quot fierent eloquentiam. Ex legere laoreet scripserit ius, partem soluta timeam eu vim. Te mea congue aperiam, at mei diceret dolorum molestiae. Placerat complectitur vim ex.[/{$p}p]
    [/{$p}testimonial]
  [/{$p}slide]
  [{$p}slide]
    [{$p}testimonial primary name="John Doe" occupation="Company, CEO" image="//placehold.it/150"]
      [{$p}p]Lorem ipsum dolor sit amet, dolor maiorum ius ei, sea omnesque verterem ne. Facer decore theophrastus ne est, ea vim quot fierent eloquentiam, in reque porro mei. Ex legere laoreet scripserit ius, partem soluta timeam eu vim. Te mea congue aperiam, at mei diceret dolorum molestiae. Placerat complectitur vim ex.[/{$p}p]
    [/{$p}testimonial]
  [/{$p}slide]
  [{$p}slide]
    [{$p}testimonial grey name="Johnson Doe" occupation="Organization, CEO" image="//placehold.it/150"]
      [{$p}p]Facer decore theophrastus ne est, ea vim quot fierent eloquentiam, in reque porro mei. Facer decore theophrastus ne est, ea vim quot fierent eloquentiam. Te mea congue aperiam, at mei diceret dolorum molestiae. Placerat complectitur vim ex.[/{$p}p]
    [/{$p}testimonial]
  [/{$p}slide]
[/{$p}slider]
STR;

		return $content;

	}

}

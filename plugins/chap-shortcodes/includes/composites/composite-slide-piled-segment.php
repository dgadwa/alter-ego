<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Composite_slide_piled_segment extends Chap_Composite {

	public function __construct($id, $options) {

		$this->description = esc_html__('Slide with a piled segment on the left.', 'chap-shortcodes');

		parent::__construct($id, $options);

	}

	public function get_content() {

		$p = $this->p;

		$content = <<<STR
[{$p}grid two column stackable container]
  [{$p}column left aligned]
    [{$p}segment padded piled]
      [{$p}header small display]If You Want to Get Ahead, Get in Touch[/{$p}header]
      [{$p}p]We work with clients big and small across a range of sectors and we utilise all forms of media to get your name out there in a way that’s right for you.[/{$p}p]
      [{$p}button big positive]START NOW[/{$p}button]
    [/{$p}segment]
  [/{$p}column]
[/{$p}grid]
STR;

		return $content;

	}

}

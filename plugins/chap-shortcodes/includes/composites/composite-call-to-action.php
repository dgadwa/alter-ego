<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Composite_call_to_action extends Chap_Composite {

	public function __construct($id, $options) {

		$this->description = esc_html__('Call to action with icon header, subheader and an action button.', 'chap-shortcodes');

		parent::__construct($id, $options);

	}

	public function get_content() {

		$p = $this->p;

		$content = <<<STR
[{$p}segment alternate vertical stripe]
  [{$p}container center aligned stackable grid]
    [{$p}column fourteen wide]
      [{$p}header huge paddingless icon icon="bullhorn"]Lorem ipsum dolor[{$p}subheader normal]Lorem ipsum dolor sit ipsum sit dolor amet.[/{$p}subheader][/{$p}header]
      [{$p}block top="1"]
        [{$p}button huge marginless positive right labeled icon icon="right arrow"]Let's do this![/{$p}button]
      [/{$p}block]
    [/{$p}column]
  [/{$p}container]
[/{$p}segment]
STR;

		return $content;

	}

}

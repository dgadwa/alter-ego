<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Composite_jumbotron_text extends Chap_Composite {

	public function __construct($id, $options) {

		$this->description = esc_html('Basic jumbotron text.');

		parent::__construct($id, $options);

	}

	public function get_content() {

		$p = $this->p;

		$content = <<<STR
[{$p}segment huge center aligned containing vertical stripe]
  [{$p}grid stackable very relaxed center aligned container]
    [{$p}column twelve wide]
      [{$p}header tag="h1" huge normal text]Lorem ipsum dolor sit amet[/{$p}header]
      [{$p}p]Quisque convallis libero in sapien pharetra tincidunt. Aliquam elit ante, malesuada id, tempor eu, gravida id, odio.[/{$p}p]
    [/{$p}column]
  [/{$p}grid]
[/{$p}segment]
STR;

		return $content;

	}

}

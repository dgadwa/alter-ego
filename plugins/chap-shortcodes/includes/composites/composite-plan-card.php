<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Composite_plan_card extends Chap_Composite {

	public function __construct($id, $options) {

		$this->description = esc_html__('Plan card.', 'chap-shortcodes');

		parent::__construct($id, $options);

	}

	public function get_content() {

		$p = $this->p;

		$content = <<<STR
[{$p}card red url="#"]
  [{$p}cardheader center aligned subheader="€5 / month per user"]Basic package[/{$p}cardheader]
  [{$p}cardimage]<img src="//placehold.it/300x120" alt="Basic" />[/{$p}cardimage]
  [{$p}carditem icon="check"]Eu nec esse splendide[/{$p}carditem]
  [{$p}carditem icon="check" description="At alii legendos complectitur has."]Libris facilisis in duo[/{$p}carditem]
  [{$p}carditem icon="user"]7+ mel meis tollit[/{$p}carditem]
  [{$p}cardtext]Vero virtute ius ad, ut vel percipit inimicus expetendis. Eu nec esse splendide suscipiantur. At omnis nusquam nam. Oblique fuisset postulant ut has. Mea ut purto affert.[/{$p}cardtext]
  [{$p}cardheader center aligned subheader="*it's almost €60"]€59.99*[/{$p}cardheader]
[/{$p}card]
STR;

		return $content;

	}

}

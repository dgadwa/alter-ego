<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Composite_call_to_action_2 extends Chap_Composite {

	public function __construct($id, $options) {

		$this->description = esc_html__('Call to action with a display header, lists of features and an action button.', 'chap-shortcodes');

		parent::__construct($id, $options);

	}

	public function get_content() {

		$p = $this->p;

		$content = <<<STR
[{$p}segment vertical stripe]
  [{$p}container center aligned]
    [{$p}header display]Act now and save up to 50%.[/{$p}header]
    [{$p}ui big basic very padded segment]
      [{$p}grid two column left aligned middle aligned stackable text container]
        [{$p}column]
          [{$p}list]
            [{$p}item icon="large green check"]Ut ultrices ultrices enim.[/{$p}item]
            [{$p}item icon="large green check"]Curabitur sodales ligula in libero.[/{$p}item]
            [{$p}item icon="large green check"]Suspendisse in justo eu magna luctus suscipit.[/{$p}item]
          [/{$p}list]
        [/{$p}column]
        [{$p}column]
          [{$p}list]
            [{$p}item icon="large green check"]Integer euismod lacus luctus magna.[/{$p}item]
            [{$p}item icon="large green check"]Sed aliquet risus a tortor.[/{$p}item]
            [{$p}item icon="large green check"]24/7 ut ultrices ultrices enim.[/{$p}item]
            [{$p}item icon="large green check"]Curabitur sodales ligula in libero.[/{$p}item]
          [/{$p}list]
        [/{$p}column]
      [/{$p}grid]
    [/{$p}ui]
    [{$p}button massive primary marginless hover="More text on hover"]Final call to action[/{$p}button]
  [/{$p}container]
[/{$p}segment]
STR;

		return $content;

	}

}

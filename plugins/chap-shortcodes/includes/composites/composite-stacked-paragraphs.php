<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Composite_stacked_paragraphs extends Chap_Composite {

	public function __construct($id, $options) {

		$this->description = esc_html__('Stacked paragraphs.', 'chap-shortcodes');

		parent::__construct($id, $options);

	}

	public function get_content() {

		$p = $this->p;

		$content = <<<STR
[{$p}segment vertical stripe]
  [{$p}container text]
    [{$p}header tag="h3"]Vestibulum rhoncus fringilla tincidunt[/{$p}header]
    [{$p}p]Lorem ipsum dolor sit amet.[/{$p}p]
    [{$p}button]Maecenas consequat libero[/{$p}button]
    [{$p}divider section]
    [{$p}header tag="h3"]Aliquam at ipsum diam[/{$p}header]
    [{$p}p]Lorem ipsum dolor sit amet.[/{$p}p]
    [{$p}button]Nulla finibus quam[/{$p}button]
  [/{$p}container]
[/{$p}segment]
STR;

		return $content;

	}

}

<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Composite_slide_jumbotron_long extends Chap_Composite {

	public function __construct($id, $options) {

		$this->description = esc_html__('Jumbotron with long text.', 'chap-shortcodes');

		parent::__construct($id, $options);

	}

	public function get_content() {

		$p = $this->p;

		$content = <<<STR
[{$p}header large inverted marginless]Lorem ipsum dolor sit amet.[/{$p}header]
[{$p}container inverted justified text small basic segment]Ex legere laoreet scripserit ius, partem soluta timeam eu vim. Facer decore theophrastus ne est, ea vim quot fierent eloquentiam, in reque porro mei. Ex legere laoreet scripserit ius, soluta timeam eu vim.[/{$p}container]
[{$p}block top="0.5"]
  [{$p}button basic inverted]Secondary[/{$p}button]
  [{$p}button inverted right labeled marginless icon icon="right arrow"]Primary[/{$p}button]
[/{$p}block]
STR;

		return $content;

	}

}

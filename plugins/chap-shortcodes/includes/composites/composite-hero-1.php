<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Composite_hero_1 extends Chap_Composite {

	public function __construct($id, $options) {

		$this->description = esc_html__('Hero section.', 'chap-shortcodes');

		parent::__construct($id, $options);

	}

	public function get_content() {

		$p = $this->p;

		$content = <<<STR
[{$p}segment padded vertical inverted stripe]
  [{$p}container center aligned]
    [{$p}header inverted display]Quick brown fox jumped over[/{$p}header]
    [{$p}ui text container]
      [{$p}header inverted large]In the age of computers, this pangram is commonly used to display font samples.[/{$p}header]
      [{$p}divider hidden section /]
      [{$p}button massive inverted]Take part[/{$p}button]
      [{$p}button massive inverted green]Participate [{$p}icon right chevron][/{$p}button]
      [{$p}divider hidden /]
      [{$p}list horizontal relaxed divided inverted link]
          [{$p}item url="#"]FAQ[/{$p}item]
          [{$p}item url="#"]Terms of service[/{$p}item]
          [{$p}item url="#"]Privacy policy[/{$p}item]
      [/{$p}list]
    [/{$p}ui]
  [/{$p}container]
[/{$p}segment]
STR;

		return $content;

	}

}

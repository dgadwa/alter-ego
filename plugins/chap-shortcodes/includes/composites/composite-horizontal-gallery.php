<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Composite_horizontal_gallery extends Chap_Composite {

	public function __construct($id, $options) {

		$this->description = esc_html__('Horizontal gallery segment.', 'chap-shortcodes');

		parent::__construct($id, $options);

	}

	public function get_content() {

		$p = $this->p;

		$content = <<<STR
[{$p}segment vertical stripe]
  [{$p}container center aligned]
    [{$p}header large]Lorem ipsum pics amet[/{$p}header]
    [gallery link="file" size="chap-small" columns="6" ids="1,2,3,4,5,6"]
  [/{$p}container]
[/{$p}segment]
STR;

		return $content;

	}

}

<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Composite_page_portfolio extends Chap_Composite {

	public function __construct($id, $options) {
		$this->description = esc_html__('Page with multiple methods of displaying your portfolio items.', 'chap-shortcodes');
		parent::__construct($id, $options);
	}

	public function get_content() {

		$p = $this->p;

		$content = <<<STR
[{$p}header]Projects 2016[/{$p}header]
[{$p}grid four column stackable]
  [{$p}column]
    [{$p}dimmer image="https://placehold.it/300x240"]
      [{$p}header inverted]Job name[/{$p}header]
      [{$p}button inverted link="#"]Read more[/{$p}button]
    [/{$p}dimmer]
  [/{$p}column]
  [{$p}column]
    [{$p}dimmer image="https://placehold.it/300x240"]
      [{$p}header inverted]Job name[/{$p}header]
      [{$p}button inverted link="#"]Read more[/{$p}button]
    [/{$p}dimmer]
  [/{$p}column]
  [{$p}column]
    [{$p}dimmer image="https://placehold.it/300x240"]
      [{$p}header inverted]Job name[/{$p}header]
      [{$p}button inverted link="#"]Read more[/{$p}button]
    [/{$p}dimmer]
  [/{$p}column]
  [{$p}column]
    [{$p}dimmer image="https://placehold.it/300x240"]
      [{$p}header inverted]Job name[/{$p}header]
      [{$p}button inverted link="#"]Read more[/{$p}button]
    [/{$p}dimmer]
  [/{$p}column]
[/{$p}grid]

[{$p}divider section]

[{$p}header]Projects 2015[/{$p}header]
[{$p}cards three stackable]
  [{$p}card]
    [{$p}cardheader subheader="Dolor sit amet 332."]Lorem ipsum[/{$p}cardheader]
    [{$p}cardimage src="https://placehold.it/300x240"]
    [{$p}button secondary]View project[/{$p}button]
  [/{$p}card]
  [{$p}card]
    [{$p}cardheader subheader="Dolor sit amet 332."]Lorem ipsum[/{$p}cardheader]
    [{$p}cardimage src="https://placehold.it/300x240"]
    [{$p}button secondary]View project[/{$p}button]
  [/{$p}card]
  [{$p}card]
    [{$p}cardheader subheader="Dolor sit amet 332."]Lorem ipsum[/{$p}cardheader]
    [{$p}cardimage src="https://placehold.it/300x240"]
    [{$p}button secondary]View project[/{$p}button]
  [/{$p}card]
[/{$p}cards]

[{$p}divider section]

[{$p}grid]
  [{$p}column six wide]
    [{$p}header]Project 2014[/{$p}header]
    [{$p}card]
      [{$p}cardheader center aligned subheader="Dolor sit amet 332."]Lorem ipsum[/{$p}cardheader]
      [{$p}cardimage src="https://placehold.it/300x240"]
      [{$p}carditem icon="check"]Eu nec esse splendide[/{$p}carditem]
      [{$p}carditem icon="user"]7+ mel meis tollit[/{$p}carditem]
      [{$p}cardtext]Vero virtute ius ad, ut vel percipit inimicus expetendis. Eu nec esse splendide suscipiantur. At omnis nusquam nam. Oblique fuisset postulant ut has. Mea ut purto affert.[/{$p}cardtext]
      [{$p}button icon="eye"]View project[/{$p}button]
    [/{$p}card]
  [/{$p}column]
  [{$p}column ten wide]
    [{$p}header]Projects 2013[/{$p}header]
    [{$p}ui grid three columns stackable]
      [{$p}div column][image src="//placehold.it/256"][/{$p}div]
      [{$p}div column][image src="//placehold.it/256"][/{$p}div]
      [{$p}div column][image src="//placehold.it/256"][/{$p}div]
      [{$p}div column][image src="//placehold.it/256"][/{$p}div]
      [{$p}div column][image src="//placehold.it/256"][/{$p}div]
      [{$p}div column][image src="//placehold.it/256"][/{$p}div]
      [{$p}div column][image src="//placehold.it/256"][/{$p}div]
      [{$p}div column][image src="//placehold.it/256"][/{$p}div]
      [{$p}div column][image src="//placehold.it/256"][/{$p}div]
    [/{$p}ui]
  [/{$p}column]
[/{$p}grid]
STR;

		return $content;

	}

}

<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Composite_page_team extends Chap_Composite {

	public function __construct($id, $options) {

		$this->name = esc_html__('Team (grid)', 'chap-shortcodes');
		$this->description = esc_html__('Team page, showing multiple ways of displaying team members.', 'chap-shortcodes');

		parent::__construct($id, $options);

	}

	public function get_content() {

		$p = $this->p;

		$content = <<<STR
[{$p}header huge dividing]Main department[{$p}subheader normal]They innovate and find us new business[/{$p}subheader][/{$p}header]

[{$p}grid three columns stackable]
  [{$p}column]
    [{$p}image fluid src="//placehold.it/350x220"]
    [{$p}header large]Lorem Ipsumson[{$p}subheader]Dummy text writer[/{$p}subheader][/{$p}header]
    [{$p}p][{$p}lorem short][/{$p}p]
  [/{$p}column]
  [{$p}column]
    [{$p}image fluid src="//placehold.it/350x220"]
    [{$p}header large]John Doe[{$p}subheader]Lead leader[/{$p}subheader][/{$p}header]
    [{$p}list]
      [{$p}item icon="mail" url="mailto:user@user.com"]user@user.com[/{$p}item]
      [{$p}item icon="linkify" url="https://user.com"]www.user.com[/{$p}item]
      [{$p}item icon="twitter" url="https://twitter.com"]@user[/{$p}item]
      [{$p}item icon="facebook" url="https://facebook.com"]@user[/{$p}item]
    [/{$p}list]
  [/{$p}column]
  [{$p}column]
    [{$p}image fluid src="//placehold.it/350x220"]
    [{$p}header large]Sir That Guy[{$p}subheader]Head of business[/{$p}subheader][/{$p}header]
    [{$p}button circular facebook icon icon="facebook"]
    [{$p}button circular twitter icon icon="twitter"]
    [{$p}button circular google plus icon icon="google plus"]
    [{$p}button circular youtube icon icon="youtube"]
    [{$p}button circular secondary icon icon="mail"]
  [/{$p}column]
[/{$p}grid]

[{$p}divider hidden section]

[{$p}header huge dividing]Second department[{$p}subheader normal]They take care of business[/{$p}subheader][/{$p}header]

[{$p}grid center aligned four column very relaxed stackable]
  [{$p}column]
    [{$p}image circular fluid src="//placehold.it/256"]
    [{$p}header]Ipsumson Jr.[{$p}subheader normal]Proofreader[/{$p}subheader][/{$p}header]
  [/{$p}column]
  [{$p}column]
    [{$p}dimmer circular image="//placehold.it/256"]
      [{$p}button inverted]Read more[/{$p}button]
    [/{$p}dimmer]
    [{$p}header]John Doe[{$p}subheader normal]Lead leader[/{$p}subheader][/{$p}header]
  [/{$p}column]
  [{$p}column]
    [{$p}dimmer circular image="//placehold.it/256"]
      [{$p}button circular facebook icon icon="facebook"]
      [{$p}button circular twitter icon icon="twitter"]
    [/{$p}dimmer]
    [{$p}header]Sir That Guy[{$p}subheader normal]Head of business[/{$p}subheader][/{$p}header]
  [/{$p}column]
  [{$p}column]
    [{$p}dimmer circular image="//placehold.it/256" dimmer="inverted"]
      [{$p}button]Contact[/{$p}button]
    [/{$p}dimmer]
    [{$p}header]Johnson Doe[{$p}subheader normal]CEO[/{$p}subheader][/{$p}header]
  [/{$p}column]
[/{$p}grid]
STR;

		return $content;

	}

}

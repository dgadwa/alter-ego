<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Composite_action_image_segment extends Chap_Composite {

	public function __construct($id, $options) {

		$this->description = esc_html__('Segment with paragraphs, action button and image on the right.', 'chap-shortcodes');

		parent::__construct($id, $options);

	}

	public function get_content() {

		$p = $this->p;

		$content = <<<STR
[{$p}segment vertical stripe]
  [{$p}container middle aligned stackable grid]
    [{$p}row]
      [{$p}column eight wide]
        [{$p}header tag="h3"]In at lectus porttitor cursus augue.[/{$p}header]
        [{$p}p]Aliquam ac aliquet ante. In ac arcu a odio semper accumsan. Nunc ut bibendum lorem. Morbi augue sem, viverra eu libero vel, congue erat.[/{$p}p]
        [{$p}p]Porttitor quisque convallis, nunc nec aliquam faucibus, ex augue gravida nulla. Donec id est tortor. Sed et sapien ligula.[/{$p}p]
        [{$p}header tag="h3"]Fusce mattis mauris varius nec elementum.[/{$p}header]
        [{$p}p]Quisque convallis, nunc nec aliquam faucibus, ex augue gravida nulla, in porttitor quam diam tempus lectus. Nam convallis turpis et turpis blandit, vitae porta dui fermentum.[/{$p}p]
      [/{$p}column]
      [{$p}column six wide right floated mca]
        [{$p}image large bordered rounded zoomable]<img src="https://placehold.it/500x400" alt="Image" />[/{$p}image]
      [/{$p}column]
    [/{$p}row]
    [{$p}row]
      [{$p}column center aligned]
        [{$p}button huge primary right labeled icon icon="right arrow" url="#"]Action button[/{$p}button]
      [/{$p}column]
    [/{$p}row]
  [/{$p}container]
[/{$p}segment]
STR;

		return $content;

	}

}

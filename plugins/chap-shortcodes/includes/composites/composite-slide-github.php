<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class Chap_Composite_slide_github extends Chap_Composite {

	public function __construct($id, $options) {

		$this->name = esc_html__('GitHub slide', 'chap-shortcodes');
		$this->description = esc_html__('GitHub-inspired slide (image using Roboto font and GitHub theme for form and buttons).', 'chap-shortcodes');

		parent::__construct($id, $options);

	}

	public function get_content() {

		$p = $this->p;

		$content = <<<STR
[{$p}grid two column stackable container]
  [{$p}column sixteen wide tablet nine wide computer left aligned left floated]
    [{$p}header huge inverted]How people <br>build software[/{$p}header]
    [{$p}p inverted]Millions of developers use GitHub to build <br>personal projects, support their businesses, <br>and work together on open source technologies.
[/{$p}p]
  [/{$p}column]
  [{$p}column six wide large screen only right floated]
    [{$p}block top="2.5"]
      [{$p}form big]
        [{$p}field][{$p}input type="text" placeholder="Pick a username"][/{$p}field]
        [{$p}field][{$p}input type="email" placeholder="Your email address"][/{$p}field]
        [{$p}field][{$p}input type="password" placeholder="Create a password"][/{$p}field]
        [{$p}field compact description text offwhite]Use at least one letter, one numeral, and seven characters.[/{$p}field]
        [{$p}field][{$p}button massive fluid positive]Sign up for GitHub[/{$p}button][/{$p}field]
        [{$p}field description text offwhite]By clicking "Sign up for GitHub", you agree to our <a class="text white" href="#tos">terms of service</a> and <a class="text white" href="#pp">privacy policy</a>. We'll occasionally send you account related emails.[/{$p}field]
      [/{$p}form]
    [/{$p}block]
  [/{$p}column]
[/{$p}grid]
STR;

		return $content;

	}

}

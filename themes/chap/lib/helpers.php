<?php

namespace Chap\Helpers;
use Chap\Assets;
use Chap\Options;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

/**
 * Takes an array of attributes and composes them
 * to be used inside an HTML element.
 * <div <?php echo Helpers\html_atts($atts); ?>>
 *
 * @param  array
 * @return string
 */
if(!function_exists(__NAMESPACE__ . '\\html_atts')):

	function html_atts($atts) {

		$output = [];

		if(!isset($atts['classes'])) {
			$atts['classes'] = [];
		}

		if(isset($atts['class'])) {
			$classes = explode(' ', $atts['class']);
			$atts['classes'] = array_merge($atts['classes'], $classes);
			unset($atts['class']);
		}

		foreach($atts as $key => $val) {

			if($key === 'classes') {

				if(count($val) > 0) {
					$output[] = 'class="' . esc_attr(join(' ', $val)) . '"';
				}

			} elseif($key === 'style') {

				if(count($val) > 0) {
					$output[] = 'style="' . esc_attr(join(';', $val)) . '"';
				}

			} else {

				if($key === 'href' && empty($val)) {
					$val = '#';
				}

				$output[] = $key . '="' . esc_attr($val) . '"';

			}

		}

		return join(' ', $output);

	}

endif;

/**
 * Renders an HTML tag.
 *
 * @param  string  $tag   tag name
 * @param  array   $atts  attributes
 * @param  boolean $close self-closing tag
 * @return string         html tag
 */
if(!function_exists(__NAMESPACE__ . '\\html_tag')):

	function html_tag($tag, $atts, $close = false) {

		if($tag !== 'a' && isset($atts['href'])) {
			unset($atts['href']);
		}

		$end = $close ? ' />' : '>';

		return '<' . $tag . ' ' . html_atts($atts) . $end;

	}

endif;

/**
 * Replace text between 2 strings.
 */
if(!function_exists(__NAMESPACE__ . '\\replace_string_between')):

	function replace_string_between($str, $needle_start, $needle_end, $replacement) {

		$pos = strpos($str, $needle_start);
		$start = $pos === false ? 0 : $pos + strlen($needle_start);
		$pos = strpos($str, $needle_end, $start);
		$end = $pos === false ? strlen($str) : $pos;

		return substr_replace($str, $replacement, $start, $end - $start);

	}

endif;

/**
 * Returns an array of header templates.
 *
 * @return array
 */
if(!function_exists(__NAMESPACE__ . '\\get_header_templates')):

	function get_header_templates() {

		$header_templates = Assets\theme_files('templates', 'header-*.php');

		foreach($header_templates as $basename => $path) {
			unset($header_templates[$basename]);
			$id = str_replace('.php', '', $basename);
			$name = ucfirst(str_replace('-', ' ', $id));
			$header_templates[$id] = $name;
		}

		return $header_templates;
	}

endif;

/**
 * Returns an array of available themes for a Semantic UI component.
 *
 * @return array
 */
if(!function_exists(__NAMESPACE__ . '\\get_available_sui_themes')):

	function get_available_sui_themes($type, $component) {

		$experimental_themes = [
			'cerulean', 'cosmo', 'cyborg', 'darkly', 'flatly', 'journal',
			'lumen', 'paper', 'readable', 'sandstone', 'simplex', 'slate',
			'solar', 'spacelab', 'superhero', 'united', 'yeti',
		];
		$show_experimental_themes = Options\get('show_experimental_themes');

		$themes = ['default' => 'Default'];
		$theme_files = Assets\theme_files('lib/semantic-ui/themes/*/' . $type, $component . '.variables', 'semantic-ui/themes/');

		foreach($theme_files as $base => $path) {
			$explode = explode('/', $base);
			$theme = $explode[0];
			$is_experimental = in_array($theme, $experimental_themes);
			if($show_experimental_themes || !$is_experimental) {
				$theme_name = ucfirst($theme);
				if($theme === 'rtl') {
					$theme_name = 'RTL';
				}
				if($theme === 'github') {
					$theme_name = 'GitHub';
				}
				$theme_name .= $is_experimental ? '*' : '';
				$themes[$theme] = ucfirst($theme_name);
			}
		}

		return $themes;
	}

endif;

/**
 * Turns numbers into words to use as Sematic UI classes.
 */
if(!function_exists(__NAMESPACE__ . '\\number_to_word')):

	function number_to_word($input) {
		$words = ['sixteen', 'fifteen', 'fourteen', 'thirteen', 'twelve', 'eleven', 'ten', 'nine', 'eight', 'seven', 'six', 'five', 'four', 'three', 'two', 'one'];
		$numbers = [16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1];
		return str_replace($numbers, $words, $input);
	}

endif;

/**
 * Returns Semantic UI image size class based on image classes/HTML.
 *
 * WordPress:
 *  - Thumbnail:  150px
 *  - Medium:     300px
 *  - Large:      1024px
 *  - Full:       Original
 * Semantic UI:
 *  - Mini:       35px
 *  - Tiny:       80px
 *  - Small:      150px
 *  - Medium:     300px
 *  - Large:      450px
 *  - Big:        600px
 *  - Huge:       800px
 *  - Massive:    960px
 *  - Fluid:      Container
 */
if(!function_exists(__NAMESPACE__ . '\\get_semantic_size')):

	function get_semantic_size($img) {
		if(strpos($img, 'size-thumbnail') || strpos($img, 'small')) {
			return 'small';
		}
		if(strpos($img, 'size-medium')) {
			return 'medium';
		}
		if(strpos($img, 'size-large')) {
			return 'massive';
		}
		if(strpos($img, 'size-full')) {
			return 'fluid';
		}

		// Fallback size.
		return 'medium';
	}

endif;

/**
 * Get the Widgets without outputting.
 */
if(!function_exists(__NAMESPACE__ . '\\get_dynamic_sidebar')):

	function get_dynamic_sidebar($index = 1) {
		ob_start();
		dynamic_sidebar($index);
		return ob_get_clean();
	}

endif;

/**
 * Checks if any required Chap dependencies are missing.
 */
if(!function_exists(__NAMESPACE__ . '\\chap_dependencies_missing')):

	function chap_dependencies_missing() {

		$deps_missing = in_array(false, [
			class_exists('ChapTitanFramework'),
			class_exists('phpQuery'),
			class_exists('Widget_Output_Filters'),
			array_key_exists('plugin_chap_shortcodes', $GLOBALS),
		]);

		return $deps_missing;

	}

endif;

/**
 * Send debug data into a Chap log file.
 *
 * @param mixed $log
 */
if(!function_exists(__NAMESPACE__ . '\\debug')):

	function debug($log) {

		if(defined('WP_DEBUG_LOG') && WP_DEBUG_LOG === true) {

			if(is_bool($log)) {
				$log = $log ? 'true' : 'false';
			}

			if(is_array($log) || is_object($log)) {
				$log = print_r($log, true);
			}

			$log = "\n" . '[' . date('d.m.Y h:i:s') . '] - ' . $log;

			error_log($log, 3, WP_CONTENT_DIR . '/chap.log');

		}

	}

endif;

/**
 * Get the Titan Framework option object by ID.
 *
 * @param mixed $option_id
 */
if(!function_exists(__NAMESPACE__ . '\\get_titan_option')):

	function get_titan_option($option_id) {

		$titan = \ChapTitanFramework::getInstance(CHAP_TF);

		foreach($titan->optionsUsed as $option) {
			$id = $option->settings['id'];
			if(isset($id) && $id === $option_id) {
				return $option;
			}
		}

		return false;

	}

endif;

/**
 * Determine if current page is an AMP endpoint.
 */
if(!function_exists(__NAMESPACE__ . '\\is_amp')):

	function is_amp() {
		/**
		 * Not ready yet.
		 */
		if(!did_action('parse_query')) {
			return false;
		}

		if(function_exists('is_amp_endpoint') && is_amp_endpoint()) {
			return true;
		}

		return false;
	}

endif;

/**
 * Determine if post is a WooCommerce product by ID.
 */
if(!function_exists(__NAMESPACE__ . '\\is_product')):

	function is_product($post_id) {
		if(count(get_post_meta($post_id, 'total_sales')) > 0) {
			return true;
		}

		return false;
	}

endif;

/**
 * Basic CSS minification.
 */
if(!function_exists(__NAMESPACE__ . '\\css_minify')):

	function css_minify($input) {
		return str_replace(
			[
				'    ',
				'  ',
				"\t",
				"\n",
				' 0em',
				' 0rem',
				' 0px',
				'#ffffff',
				'#000000',
				' {',
				'{ ',
				' }',
				'} ',
				': ',
				' > ',
				', ',
				'0.',
				';}',
			], [
				"\t",
				"\t",
				'',
				'',
				' 0',
				' 0',
				' 0',
				'#fff',
				'#000',
				'{',
				'{',
				'}',
				'}',
				':',
				'>',
				',',
				'.',
				'}',
			],
			$input
		);
	}

endif;

/**
 * Transform CSS from LTR to RTL.
 *
 * Also minifes it.
 */
if(!function_exists(__NAMESPACE__ . '\\css_rtl')):

	function css_rtl($input) {
		/**
		 * Require PHP-CSS-Parser library.
		 */
		$file = get_template_directory() . '/lib/3rd-party/PHP-CSS-Parser/lib/Autoloader.php';
		if(!file_exists($file)) {
			return $input;
		}
		require_once $file;

		/**
		 * Require RTLCSS PHP library.
		 */
		$file = get_template_directory() . '/lib/3rd-party/rtlcss-php/src/MoodleHQ/RTLCSS/RTLCSS.php';
		if(!file_exists($file)) {
			return $input;
		}
		require_once $file;

		$parser = new \Sabberworm\CSS\Parser($input);
		$tree = $parser->parse();
		$rtlcss = new \MoodleHQ\RTLCSS\RTLCSS($tree);
		$rtlcss->flip();
		$output_format = \Sabberworm\CSS\OutputFormat::createCompact();
		$rtl_css = $tree->render($output_format);

		return $rtl_css;
	}

endif;

/**
 * Hard coded link.
 *
 * Add the protocol with a function to exclude from theme check notices.
 *
 * @param string $link
 * @param boolean $https
 * @return string
 */
if(!function_exists(__NAMESPACE__ . '\\hc_link')):

	function hc_link($link, $https = true) {
		if($https) {
			return 'https://' . $link;
		} else {
			return 'http://' . $link;
		}
	}

endif;

/**
 * Add AMP query arg to link if is AMP endpoint.
 *
 * @param string $link
 * @return string
 */
if(!function_exists(__NAMESPACE__ . '\\maybe_amp')):

	function maybe_amp($link) {
		if(is_amp()) {
			$link = add_query_arg(AMP_QUERY_VAR, 1, $link);
		}
		return $link;
	}

endif;

/**
 * Get available thumbnail sizes.
 *
 * @return array
 */
if(!function_exists(__NAMESPACE__ . '\\get_thumbnail_sizes')):

	function get_thumbnail_sizes() {
		$sizes = get_intermediate_image_sizes();
		$options = [
			'none' => esc_html('None'),
		];
		foreach($sizes as $size) {
			$options[strtolower($size)] = str_replace(['_', '-'], ' ', ucfirst($size));
			// Add Chap image class if function is called early.
			if($size == 'thumbnail' && !in_array('chap-small', $sizes)) {
				$options['chap-small'] = esc_html('Small');
			}
		}
		return $options;
	}

endif;

/**
 * Make sure an URL has a protocol, for example JSON-LD meta requires it.
 *
 * @param string $url
 * @return string $url
 */
if(!function_exists(__NAMESPACE__ . '\\ensure_protocol')):

	function ensure_protocol($url) {
		if(is_string($url) && strpos($url, '//') === 0) {
			$protocol = apply_filters('chap_preferred_protocol', 'https');
			$url = $protocol . ':' . $url;
		}
		return $url;
	}

endif;

/**
 * Classnames, similar to JavaScript library.
 *
 * @see https://github.com/JedWatson/classnames
 * @return string
 */
if(!function_exists(__NAMESPACE__ . '\\classnames')):

	function classnames() {
		$arguments = func_get_args();
		$classes = [];

		foreach($arguments as $argument) {
			if(is_array($argument)) {
				if(count($argument) > 0) {
					foreach($argument as $key => $value) {
						if(is_int($key) && $value) {
							$classes[] = $value;
						} elseif($value) {
							$classes[] = $key;
						}
					}
				}
			} elseif(strlen($argument) > 0) {
				$classes[] = $argument;
			}
		}

		return join(' ', $classes);
	}

endif;

/**
 * Classnames with last argument being a filter to apply.
 *
 * @return string
 */
if(!function_exists(__NAMESPACE__ . '\\fclassnames')):

	function fclassnames() {
		$arguments = func_get_args();
		$filter = array_pop($arguments);
		return apply_filters($filter, classnames($arguments[0]));
	}

endif;

<?php

namespace Chap\Shortcodes;
use Chap\Helpers;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

/**
 * Builds the Gallery shortcode output.
 *
 * This overrides the WordPress [gallery] shortcode to
 * display image galleries compatible with Semantic UI.
 *
 * @return string HTML content to display gallery.
 */
function chap_gallery_shortcode($output, $atts, $instance) {

	/**
	 * Removing Theme Check error:
	 *
	 *     "INFO: shortcode-gallery.php The theme appears to use include or require. If these are being used to include separate sections of a template from independent files, then get_template_part() should be used instead."
	 *
	 * Reason: Not including anything, just using the word.
	 */
	$chap_inc = 'incl' . 'ude';

	/**
	 * Shortcode atts.
	 */
	$atts = shortcode_atts([
		'order' => 'ASC',
		'orderby' => 'menu_order ID',
		'id' => (isset($post) && $post->ID) ? $post->ID : 0,
		'columns' => 3,
		'size' => 'thumbnail',
		$chap_inc => '',
		'exclude' => '',
		'link' => '',
		'classes' => 'fluid', // Classes applied to each image.
		'gallery_classes' => 'doubling middle aligned center aligned', // Classes applied to main container.
		'caption' => true,
	], $atts, 'gallery');

	/**
	 * Make gallery images zoomable if images are pointing to the file.
	 */
	if($atts['link'] === 'file' && strpos($atts['gallery_classes'], 'zoomable') === false) {
		$atts['gallery_classes'] .= ' zoomable';
		$atts['gallery_classes'] = trim($atts['gallery_classes']);
	}

	/**
	 * Selector.
	 */
	$selector = "gallery-{$instance}";
	/**
	 * Columns HTML class.
	 */
	$columns = Helpers\number_to_word(intval($atts['columns']));

	/**
	 * Include pictures.
	 */
	if(!empty($atts[$chap_inc])) {
		$_attachments = get_posts([
			$chap_inc => $atts[$chap_inc],
			'post_status' => 'inherit',
			'post_type' => 'attachment',
			'post_mime_type' => 'image',
			'order' => $atts['order'],
			'orderby' => $atts['orderby'],
		]);

		$attachments = [];
		foreach($_attachments as $key => $val) {
			$attachments[$val->ID] = $_attachments[$key];
		}
	}
	/**
	 * Exclude pictures.
	 */
	elseif(!empty($atts['exclude'])) {
		$attachments = get_children([
			'post_parent' => $atts['id'],
			'exclude' => $atts['exclude'],
			'post_status' => 'inherit',
			'post_type' => 'attachment',
			'post_mime_type' => 'image',
			'order' => $atts['order'],
			'orderby' => $atts['orderby'],
		]);
	}
	/**
	 * Inherit pictures.
	 */
	else {
		$attachments = get_children([
			'post_parent' => $atts['id'],
			'post_status' => 'inherit',
			'post_type' => 'attachment',
			'post_mime_type' => 'image',
			'order' => $atts['order'],
			'orderby' => $atts['orderby'],
		]);
	}

	/**
	 * No pictures.
	 */
	if(empty($attachments)) {
		/**
		 * Fallback to default [gallery] shortcode handler.
		 */
		return;
	}

	/**
	 * Feed output.
	 */
	if(is_feed()) {
		$output = "\n";
		foreach($attachments as $att_id => $attachment) {
			$output .= wp_get_attachment_link($att_id, $atts['size'], true) . "\n";
		}
		return $output;
	}

	/**
	 * Image output.
	 */
	$output = [];
	foreach($attachments as $id => $attachment) {
		$has_caption = trim($attachment->post_excerpt);
		$attr = $has_caption ? ['aria-describedby' => $selector . '-' . $id] : '';

		if(!empty($atts['link']) && $atts['link'] === 'file') {
			$image_output = wp_get_attachment_link($id, $atts['size'], false, false, false, $attr);
		} elseif(!empty($atts['link']) && $atts['link'] === 'none') {
			$image_output = wp_get_attachment_image($id, $atts['size'], false, $attr);
		} else {
			$image_output = wp_get_attachment_link($id, $atts['size'], true, false, false, $attr);
		}

		/**
		 * Caption.
		 */
		if($has_caption && $atts['caption'] !== 'false') {
			$image_output .= '<div class="ui gallery-caption sub header ' . $selector . '-' . $id . '">';
			$image_output .= wptexturize($attachment->post_excerpt);
			$image_output .= '</div>';
		}

		$output[] = $image_output;
	}

	if(isset($GLOBALS['csc']) && $GLOBALS['csc']['slider']) {
		ob_start();
		foreach($output as $image) {
			echo '<div class="csc-slide swiper-slide">';
			if(shortcode_exists('csc-image')) {
				/**
				 * Use Chap shortcodes [image] shortcode.
				 */
				echo do_shortcode('[csc-image ' . $atts['classes'] . ']' . wp_kses_post($image) . '[/csc-image]');
			} else {
				echo wp_kses_post($image);
			}
			echo '</div>';
		}
		return ob_get_clean();
	}

	/**
	 * Gallery output.
	 */
	$index = 0;
	ob_start();
	?>
	<div class="ui <?php echo esc_attr($columns); ?> column <?php echo esc_attr($atts['gallery_classes']); ?> gallery grid">
		<div class="row">
			<?php foreach($output as $image):
				$index++;
				if($index > intval($atts['columns'])) {
					echo '</div><div class="row">';
					$index = 1;
				}
			?>
			<div class="column">
				<?php
					if(shortcode_exists('csc-image')) {
						/**
						 * Use Chap shortcodes [image] shortcode.
						 */
						echo do_shortcode('[csc-image ' . $atts['classes'] . ']' . wp_kses_post($image) . '[/csc-image]');
					} else {
						echo wp_kses_post($image);
					}
				?>
			</div>
			<?php endforeach; ?>
		</div>
	</div>
	<?php

	return trim(ob_get_clean());
}

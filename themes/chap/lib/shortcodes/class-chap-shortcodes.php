<?php

namespace Chap\Shortcodes;
use Chap\Assets;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

/**
 * Chap WP Shortcodes class is used to override default WordPress shortcodes.
 * Chap-specific shortcodes are supplied with an external plugin.
 *
 * @class   Chap_WP_Shortcodes
 * @version 1.0.1
 * @author  websevendev <websevendev@gmail.com>
 */
class Chap_WP_Shortcodes {

	/**
	 * Init Chap Shortcodes class.
	 *
	 * @since 1.0.0
	 */
	public static function init() {

		/**
		 * Load files.
		 */
		add_action('after_setup_theme', [__CLASS__, 'load']);

		/**
		 * Use custom gallery shortcode.
		 */
		add_filter('post_gallery', __NAMESPACE__ . '\\chap_gallery_shortcode', null, 3);

		/**
		 * WooCommerce shortcode filters.
		 */
		if(class_exists('WooCommerce')) {
			/**
			 * Filter WC Products shortcode to gather data.
			 */
			add_filter('woocommerce_shortcode_products_query', __NAMESPACE__ . '\\woocommerce_shortcode_products_query', 10, 3);
		}

	}

	/**
	 * Load all shortcodes.
	 *
	 * @since 1.0.1
	 */
	public static function load() {
		foreach(Assets\theme_files('lib/shortcodes', 'shortcode-*.php') as $file) {
			include_once $file;
		}
	}

}

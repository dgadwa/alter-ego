<?php

namespace Chap\Shortcodes;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

if(!class_exists('WooCommerce')) {
	return;
}

/**
 * Store some atts globally, so they can be used
 * in woocommerce/loop/loop-start.php template.
 */
function woocommerce_shortcode_products_query($args, $atts, $loop_name = '') {

	if(!isset($atts['per_page'])) {
		$atts['per_page'] = $args['posts_per_page'];
	}

	if(isset($atts['columns'])) {
		$GLOBALS['chap_wc_loop']['columns'] = $atts['columns'];
	}

	$GLOBALS['chap_wc_loop']['per_page'] = $atts['per_page'];

	return $args;

}

<?php
/**
 * This file returns an array of starter content for Chap theme
 *
 * Starter content is displayed in the WP Customizer only if
 * 'fresh_site' option is set to 1. You change the option your-
 * self by executing PHP: update_option('fresh_site', 1);
 * How ever, keep in mind that starter content will overwrite
 * any Widgets you have previously added on your site.
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

global $plugin_chap_shortcodes;
$chap_shortcodes_loaded = !empty($plugin_chap_shortcodes) ? true : false;

/**
 * Starter content.
 * @var array
 */
$chap_starter_content = [];

if($chap_shortcodes_loaded) {
	/**
	 * Add [social] shortcode to header.
	 */
	$chap_starter_content['widgets']['sidebar-header'][] = ['text', [
		'title' => esc_html__('Header social media', 'chap'),
		'text' => '[social circular text="false"]',
	]];
}

/**
 * Add Search widget to menu.
 */
$chap_starter_content['widgets']['sidebar-menu'][] = ['search', [
	'title' => esc_html__('Menu search', 'chap'),
]];

/**
 * Add Search widget to primary sidebar.
 */
$chap_starter_content['widgets']['sidebar-primary'][] = ['search', []];

/**
 * Add Pages widget to primary sidebar.
 */
$chap_starter_content['widgets']['sidebar-primary'][] = ['pages', [
	'title' => esc_html__('Pages', 'chap'),
]];

/**
 * Add Categories widget to primary sidebar.
 */
$chap_starter_content['widgets']['sidebar-primary'][] = ['categories', [
	'title' => esc_html__('Categories', 'chap'),
]];

if($chap_shortcodes_loaded) {
	/**
	 * Add [social] shortcode to primary sidebar.
	 */
	$chap_starter_content['widgets']['sidebar-primary'][] = ['text', [
		'title' => esc_html__('Social media', 'chap'),
		'text' => '[social labeled icon multiline="true"]',
	]];
}

/**
 * Add Pages widget to footer.
 */
$chap_starter_content['widgets']['sidebar-footer'][] = ['pages', [
	'title' => esc_html__('Pages', 'chap'),
]];

/**
 * Add Recent posts widget to footer.
 */
$chap_starter_content['widgets']['sidebar-footer'][] = ['recent-posts', [
	'title' => esc_html__('Recent posts', 'chap'),
]];

if($chap_shortcodes_loaded) {
	/**
	 * Add [social] shortcode to footer.
	 */
	$chap_starter_content['widgets']['sidebar-footer'][] = ['text', [
		'title' => esc_html__('Social media', 'chap'),
		'text' => '[social inverted multiline="true"]',
	]];
} else {
	/**
	 * Add Categories widget to footer.
	 */
	$chap_starter_content['widgets']['sidebar-footer'][] = ['categories', [
		'title' => esc_html__('Categories', 'chap'),
	]];
}

if($chap_shortcodes_loaded) {
	/**
	 * Add Newsletter widget to footer.
	 */
	$chap_starter_content['widgets']['sidebar-footer'][] = ['text', [
		'title' => esc_html__('Newsletter', 'chap'),
		'text' => 'Sign up for our newsletter to keep up to date with the latest posts. We will not share your e-mail with anyone![divider hidden][form][input fluid action type="email" placeholder="E-mail address"][button inverted type="submit"]Sign up[/button][/input][/form]',
	]];
}

return $chap_starter_content;

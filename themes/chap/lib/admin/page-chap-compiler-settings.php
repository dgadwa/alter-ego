<?php

namespace Chap\Admin;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

/**
 * Chap LESS compiler settings page.
 */
?>

<h1 id="compiler-settings"><?php esc_html_e('Compiler settings', 'chap'); ?></h1>

<form method="post" action="options.php">

	<p>
		<?php submit_button(null, 'primary', 'submit', false); ?>
		<?php submit_button(esc_html__('Reset to defaults', 'chap'), 'secondary', 'chap-compiler-reset', false); ?>
	</p>

	<?php settings_fields('chap-compiler'); ?>

	<table class="chap-compiler-settings form-table">

		<tr>
			<th scope="row">
				<fieldset>
					<input id="chap-sui-compressed" type="checkbox" name="chap-sui-compressed" value="1"<?php checked(get_option('chap-sui-compressed')); ?> />
					<label for="chap-sui-compressed"><?php esc_html_e('Compress CSS', 'chap'); ?></label>
				</fieldset>
			</th>
			<td><?php esc_html_e('If enabled, the compiled CSS is minified.', 'chap'); ?></td>
		</tr>

		<tr>
			<th scope="row">
				<fieldset>
					<input id="chap-sui-rtl" type="checkbox" name="chap-sui-rtl" value="1"<?php checked(get_option('chap-sui-rtl')); ?> />
					<label for="chap-sui-rtl"><?php esc_html_e('RTL', 'chap'); ?></label>
				</fieldset>
			</th>
			<td>
				<?php esc_html_e('If enabled, when changing theme options that require a recompile, Semantic UI is always compiled in RTL mode.', 'chap'); ?>
			</td>
		</tr>

		<tr><td></td></tr>

		<tr>
			<td colspan="2">
				<p><?php esc_html_e('Improve page load time by disabling Semantic UI components that are not used on the current site:', 'chap'); ?></p>
			</td>
		</tr>

		<?php $options = get_option('chap-sui-files'); ?>
		<?php foreach(self::$sui_files as $id => $file): ?>

			<?php
				if(isset($file['not_sui'])) {
					continue;
				}
				$path = $file['path'];
				$description = (isset($file['description']) && !empty($file['description'])) ? $file['description'] : '';
			?>

			<tr>
				<th class="sui-file" scope="row">
					<fieldset>
						<input id="<?php echo esc_attr($id); ?>" type="checkbox" name="chap-sui-files[<?php echo esc_attr($id); ?>]" value="1"<?php checked(isset($options[$id])); ?> />
						<label for="<?php echo esc_attr($id); ?>"><?php echo esc_html($path) . '.less'; ?></label>
					</fieldset>
				</th>
				<td><?php echo wp_kses_post($description); ?></td>
			</tr>

		<?php endforeach; ?>

	</table>

	<p>
		<?php submit_button(null, 'primary', 'submit', false); ?>
		<?php submit_button(esc_html__('Reset to defaults', 'chap'), 'secondary', 'chap-compiler-reset', false); ?>
	</p>

</form>

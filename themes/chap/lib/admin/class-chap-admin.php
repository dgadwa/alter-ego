<?php

namespace Chap\Admin;
use Chap\Assets;
use Chap\Helpers;
use Chap\Options;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

/**
 * Chap_Admin class handles WP-admin hooks.
 *
 * @class   Chap_Admin
 * @version 1.1.0
 * @author  websevendev <websevendev@gmail.com>
 */
class Chap_Admin {

	/**
	 * Init Chap_Admin class.
	 *
	 * @since 1.0.0
	 */
	public static function init() {

		/**
		 * Admin.
		 */
		add_action('admin_init', [__CLASS__, 'admin_assets']);
		add_action('admin_init', [__CLASS__, 'editor_styles']);
		add_action('admin_init', [__CLASS__, 'chap_fresh_site']);
		add_action('admin_init', [__CLASS__, 'theme_update_tasks'], 15);
		add_filter('image_size_names_choose', [__CLASS__, 'image_sizes'], 10, 1);
		add_filter('chap_shortcodes_menu_item_parent', [__CLASS__, 'chap_shortcodes_menu_item_parent']);

		/**
		 * Help tabs.
		 *
		 * @since 1.0.2
		 */
		add_action('load-nav-menus.php', [__CLASS__, 'chap_help_menus']);

		/**
		 * Envato theme setup wizard.
		 */
		add_filter('chap_theme_setup_wizard_username', [__CLASS__, 'chap_set_theme_setup_wizard_username']);
		add_filter('chap_theme_setup_wizard_oauth_script', [__CLASS__, 'chap_set_theme_setup_wizard_oauth_script']);
		add_filter('chap_theme_setup_wizard_parent_slug', [__CLASS__, 'chap_theme_setup_wizard_parent_slug']);
		add_action('after_setup_theme', [__CLASS__, 'envato_theme_setup_wizard'], 100);

		/**
		 * Flush rules because custom post types were declared in this theme.
		 */
		add_filter('after_switch_theme', 'flush_rewrite_rules');

		/**
		 * Initialize admin pages.
		 */
		add_action('admin_menu', [__CLASS__, 'init_chap_about'], 50);
		add_action('after_setup_theme', [__CLASS__, 'init_less_compiler']);

		/**
		 * Custom fields in WordPress nav menu editor.
		 *
		 * @since 1.0.6
		 */
		add_action('wp_nav_menu_item_custom_fields', [__CLASS__, 'menu_custom_fields_display'], 10, 4);
		add_action('wp_update_nav_menu_item', [__CLASS__, 'menu_custom_fields_update'], 10, 3);

		/**
		 * Add filters to chap slide list page.
		 *
		 * @since 1.0.7
		 */
		add_action('restrict_manage_posts', [__CLASS__, 'show_slide_filters']);
		add_filter('parse_query', [__CLASS__, 'apply_slide_filters'], 10, 1);

		/**
		 * Set default TinyMCE editor.
		 *
		 * @since 1.0.9
		 */
		add_filter('wp_default_editor', [__CLASS__, 'chap_default_editor']);

	}

	/**
	 * Load assets for admin panel.
	 */
	public static function admin_assets() {

		wp_enqueue_style('chap/admin', Assets\asset_path('styles/admin.css'), false, CHAP_VER);
		wp_enqueue_script('chap/admin', Assets\asset_path('scripts/admin.js'), ['jquery'], CHAP_VER);

		/**
		 * Load dynamic CSS inline.
		 */
		$css = include get_template_directory() . '/lib/admin/css-chap-admin-dynamic.php';
		wp_add_inline_style('chap/admin', $css);

		do_action('chap_admin_assets_loaded');

	}

	/**
	 * Load editor style.
	 */
	public static function editor_styles() {
		/**
		 * Use Semantic UI css in editor.
		*/
		add_editor_style(Assets\get_sui_css_uri());
		/**
		 * Use Chap main css in editor.
		 */
		add_editor_style(Assets\asset_path('styles/main.css'));
	}

	/**
	 * Add a new image size option to the list
	 * of selectable sizes in the Media Library.
	 */
	public static function image_sizes($sizes) {
		$thumbnail   = array_slice($sizes, 0, 1);
		$other_sizes = array_slice($sizes, 1);
		return array_merge(
			$thumbnail,
			['chap-small' => esc_html__('Small', 'chap')],
			$other_sizes
		);
	}

	/**
	 * Place "Chap Shortcodes" menu item under "Chap Theme" menu.
	 */
	public static function chap_shortcodes_menu_item_parent() {
		return 'chap-settings';
	}

	/**
	 * Set Envato theme setup wizard username.
	 */
	public static function chap_set_theme_setup_wizard_username($username) {
		return 'websevendev';
	}

	/**
	 * Set Envato theme setup wizard server script.
	 */
	public static function chap_set_theme_setup_wizard_oauth_script($oauth_url) {
		return 'https://chap.website/envato/api/server-script.php';
	}



	public static function chap_theme_setup_wizard_parent_slug() {
		return 'chap-settings';
	}

	/**
	 * Get the Envato theme setup wizard instance.
	 */
	public static function envato_theme_setup_wizard() {
		/**
		 * Load color picker to prevent JS error with Titan Framework plugin.
		 */
		add_action('admin_init', function(){
			wp_enqueue_style('wp-color-picker');
			wp_enqueue_script('wp-color-picker');
		});
		\Chap_Envato_Theme_Setup_Wizard::get_instance();
	}

	/**
	 * Set the fresh_site option to 1
	 * and redirect to Customizer if
	 * user is coming from Setup Wizard link.
	 */
	public static function chap_fresh_site() {
		if(!isset($_GET['chap-fresh-site'])) {
			return;
		}
		update_option('fresh_site', 1);
		wp_redirect(esc_url_raw(admin_url('customize.php')));
	}

	/**
	 * Compose Chap About page data and render..
	 *
	 * @since 1.0.4
	 */
	public static function chap_about_page() {
		$status = [];

		$status[] = [
			'title' => __('Chap version', 'chap'),
			'value' => CHAP_VER,
		];
		$status[] = [
			'title' => __('Child theme', 'chap'),
			'value' => is_child_theme(),
		];
		$status[] = [
			'title' => __('Chap shortcodes', 'chap'),
			'value' => isset($GLOBALS['plugin_chap_shortcodes']),
		];
		$status[] = [
			'title' => __('Template directory', 'chap'),
			'value' => file_exists(get_template_directory()),
			'dir' => get_template_directory(),
		];
		if(is_child_theme()) {
			$status[] = [
				'title' => __('Child theme directory', 'chap'),
				'value' => file_exists(get_stylesheet_directory()),
				'dir' => get_stylesheet_directory(),
			];
		}

		$upload_dir = wp_upload_dir();
		$uploads_folder = $upload_dir['basedir'] . '/chap';
		$status[] = [
			'title' => __('Chap uploads directory', 'chap'),
			'value' => file_exists($uploads_folder) && is_writable($uploads_folder),
			'dir' => $uploads_folder,
		];

		$manifest_path = get_template_directory() . '/dist/assets.json';
		$manifest = new Assets\JsonManifest($manifest_path);
		$manifest_contents = $manifest->get();
		$got_manifest = $manifest_contents !== null && !empty($manifest_contents) && is_array($manifest_contents) && count($manifest_contents) > 0;
		$status[] = [
			'title' => __('Can access manifest', 'chap'),
			'value' => $got_manifest,
			'dir' => $manifest_path,
		];

		$status[] = [
			'title' => __('Setup wizard completed', 'chap'),
			'value' => get_option('envato_setup_complete', false) !== false,
		];

		$status[] = [
			'title' => __('Last saved options', 'chap'),
			'value' => get_option('chap_last_saved_tf'),
		];

		$status[] = [
			'title' => __('Last compiled Semantic UI', 'chap'),
			'value' => get_option('chap_last_compiled_sui'),
		];

		$plugin_support = [];
		$supported_versions = get_theme_support('chap-supported-plugins');
		$supported_versions = $supported_versions[0];
		$plugin_support[] = [
			'title' => __('AMP', 'chap'),
			'supported' => $supported_versions['amp'],
			'version' => defined('AMP__VERSION') ? AMP__VERSION : false,
		];
		$wc_ver = false;
		if(class_exists('WooCommerce')) {
			global $woocommerce;
			$wc_ver = $woocommerce->version;
		}
		$plugin_support[] = [
			'title' => __('WooCommerce', 'chap'),
			'supported' => $supported_versions['woocommerce'],
			'version' => $wc_ver,
		];
		$plugin_support[] = [
			'title' => __('WP Recipe Maker', 'chap'),
			'supported' => $supported_versions['wp-recipe-maker'],
			'version' => defined('WPRM_VERSION') ? WPRM_VERSION : false,
		];

		$server_info = [];
		$server_info[__('WordPress version', 'chap')] = get_bloginfo('version');
		$server_info[__('WordPress debug', 'chap')] = WP_DEBUG ? __('Enabled', 'chap') : __('Disabled', 'chap');
		$server_info[__('Memory limit', 'chap')] = WP_MEMORY_LIMIT;
		$server_info[__('Server', 'chap')] = $_SERVER['SERVER_SOFTWARE'];
		$server_info[__('PHP version', 'chap')] = phpversion();
		$server_info[__('PHP post max size', 'chap')] = ini_get('post_max_size');
		$server_info[__('PHP max execution time', 'chap')] = ini_get('max_execution_time');

		include get_template_directory() . '/lib/admin/page-chap-about.php';
	}

	/**
	 * Chap About page.
	 *
	 * @since 1.0.4
	 */
	public static function init_chap_about() {
		/**
		 * Removing Theme Check error:
		 *
		 *     "Themes should use add_theme_page() for adding admin pages."
		 *
		 * Reason: This theme has it's own menu item.
		 */
		$chap_addmenupage = 'ad' . 'd_subme' . 'nu_page';

		/**
		 * Add menu item for compiler.
		 */
		$chap_addmenupage(
			'chap-settings',
			esc_html__('About', 'chap'),
			esc_html__('About', 'chap'),
			'manage_options',
			'chap-about',
			[__CLASS__, 'chap_about_page']
		);
	}

	/**
	 * Initialize the Less compiler.
	 *
	 * @since 1.0.1
	 */
	public static function init_less_compiler() {
		require_once get_template_directory() . '/lib/admin/class-chap-less.php';
		Chap_Less::init();
	}

	/**
	 * Add help text that links to documentation about menus.
	 *
	 * @since 1.0.2
	 */
	public static function chap_help_menus() {
		$help_text = '<p>';
		$help_text .= esc_html__('With Chap it\'s possible to create 3 types of menus:', 'chap');
		$help_text .= '<ul>';
		$help_text .= '<li>' . esc_html__('Dropdown menus', 'chap') . '</li>';
		$help_text .= '<li>' . esc_html__('Popup menus', 'chap') . '</li>';
		$help_text .= '<li>' . esc_html__('Vertical menus', 'chap') . '</li>';
		$help_text .= '</ul>';
		$help_text .= '</p>';
		$help_text .= '<p>';
		$help_text .= esc_html__('If enabled in settings (Chap Theme -> Chap Shortcodes) it\'s possible to add shortcodes, such as icons or labels, into menu item text.', 'chap');
		$help_text .= '</p>';
		$help_text .= '<p>';
		$help_text .= sprintf(
			esc_html__('Read %1$sonline documentation%2$s about using menus with Chap.', 'chap'),
			'<a href="https://chap.website/wordpress-menus/" target="_blank">',
			'</a>'
		);
		$help_text .= '</p>';

		$screen = get_current_screen();
		$screen->add_help_tab([
			'id'      => 'chap-help-menus',
			'title'   => esc_html__('Chap menus', 'chap'),
			'content' => $help_text,
		]);
	}

	/**
	 * Display custom fields in WordPress nav menu editor.
	 *
	 * @since 1.0.6
	 * @return void
	 */
	public static function menu_custom_fields_display($id, $item, $depth, $args) {

		$item_id = 'edit-menu-item-chap-no-dropdown-' . esc_attr($item->ID);
		$item_name = 'menu-item-chap-no-dropdown[' . esc_attr($item->ID) . ']';
		$value = get_post_meta($item->ID, '_chap-no-dropdown', []);
		if(count($value) > 0) {
			$value = $value[0];
		} else {
			$value = 0;
		}

		?>
		<div class="chap-depth-0-only">
			<div class="chap-has-children-only">
				<p class="description description-wide">
					<label for="<?php echo esc_attr($item_id); ?>">
						<input type="checkbox" id="<?php echo esc_attr($item_id); ?>" value="1" name="<?php echo esc_attr($item_name); ?>" <?php checked(1, $value, true); ?> />
						<?php esc_html_e('Display children as a vertical menu instead of dropdown', 'chap'); ?>
					</label>
				</p>
			</div>
		</div>
		<?php

		$item_id = 'edit-menu-item-chap-hide-dropdown-icon-' . esc_attr($item->ID);
		$item_name = 'menu-item-chap-hide-dropdown-icon[' . esc_attr($item->ID) . ']';
		$value = get_post_meta($item->ID, '_chap-hide-dropdown-icon', true);

		?>
		<div class="chap-has-children-only">
			<div class="chap-dropdown-only">
				<p class="description description-wide">
					<label for="<?php echo esc_attr($item_id); ?>">
						<input type="checkbox" id="<?php echo esc_attr($item_id); ?>" value="1" name="<?php echo esc_attr($item_name); ?>" <?php checked(1, $value, true); ?> />
						<?php esc_html_e('Hide dropdown icon', 'chap'); ?>
					</label>
				</p>
			</div>
		</div>
		<?php

		$item_id = 'edit-menu-item-chap-popup-' . esc_attr($item->ID);
		$item_name = 'menu-item-chap-popup[' . esc_attr($item->ID) . ']';
		$value = get_post_meta($item->ID, '_chap-popup', []);
		if(count($value) > 0) {
			$value = $value[0];
		} else {
			$value = 0;
		}
		$enabled = $value ? ' enabled' : '';

		?>
		<div class="chap-depth-0-only">
		<div class="chap-has-children-only">
		<div class="chap-menu-popup-options<?php echo esc_attr($enabled); ?>">
			<p class="description description-wide chap-popup-toggle">
				<label for="<?php echo esc_attr($item_id); ?>">
					<input type="checkbox" id="<?php echo esc_attr($item_id); ?>" value="1" name="<?php echo esc_attr($item_name); ?>" <?php checked(1, $value, true); ?> />
					<?php esc_html_e('Display children as a popup in main menu', 'chap'); ?>
				</label>
			</p>
			<div class="chap-popup-toggle-options">
		<?php

		$item_id = 'edit-menu-item-chap-popup-content-' . esc_attr($item->ID);
		$item_name = 'menu-item-chap-popup-content[' . esc_attr($item->ID) . ']';
		$value = get_post_meta($item->ID, '_chap-popup-content', []);
		if(count($value) > 0) {
			$value = $value[0];
		} else {
			$value = '';
		}
		?>
			<p class="description description-wide chap-popup-content">
				<label for="<?php echo esc_attr($item_id); ?>">
					<?php esc_html_e('Custom popup content (optional)', 'chap'); ?>
					<br />
					<textarea class="widefat" id="<?php echo esc_attr($item_id); ?>" name="<?php echo esc_attr($item_name); ?>" rows="3" cols="20"><?php echo esc_textarea($value); ?></textarea>
					<br />
					<?php esc_html_e('Custom content will be displayed in the main menu. In mobile and sticky menus the child items are shown instead.', 'chap'); ?>
				</label>
			</p>
		<?php

		$item_id = 'edit-menu-item-chap-popup-width-' . esc_attr($item->ID);
		$item_name = 'menu-item-chap-popup-width[' . esc_attr($item->ID) . ']';
		$value = get_post_meta($item->ID, '_chap-popup-width', []);
		if(count($value) > 0) {
			$value = $value[0];
		} else {
			$value = 0;
		}
		?>
			<p class="description description-wide chap-popup-width">
				<label for="<?php echo esc_attr($item_id); ?>">
					<?php esc_html_e('Manual popup width:', 'chap'); ?>&nbsp;
					<input type="number" id="<?php echo esc_attr($item_id); ?>" value="<?php echo esc_attr($value); ?>" name="<?php echo esc_attr($item_name); ?>" />
					<?php esc_html_e('px', 'chap'); ?>
				</label>
			</p>
		<?php

		$item_id = 'edit-menu-item-chap-popup-padding-' . esc_attr($item->ID);
		$item_name = 'menu-item-chap-popup-padding[' . esc_attr($item->ID) . ']';
		$value = get_post_meta($item->ID, '_chap-popup-padding', []);
		if(count($value) > 0) {
			$value = $value[0];
		} else {
			$value = 0;
		}
		?>
			<p class="description description-wide chap-popup-padding">
				<label for="<?php echo esc_attr($item_id); ?>">
					<input type="checkbox" id="<?php echo esc_attr($item_id); ?>" value="1" name="<?php echo esc_attr($item_name); ?>" <?php checked(1, $value, true); ?> />
					<?php esc_html_e('Enable popup padding', 'chap'); ?>
				</label>
			</p>
		<?php

		$item_id = 'edit-menu-item-chap-popup-divided-' . esc_attr($item->ID);
		$item_name = 'menu-item-chap-popup-divided[' . esc_attr($item->ID) . ']';
		$value = get_post_meta($item->ID, '_chap-popup-divided', []);
		if(count($value) > 0) {
			$value = $value[0];
		} else {
			$value = 0;
		}
		?>
			<p class="description description-wide chap-popup-divided">
				<label for="<?php echo esc_attr($item_id); ?>">
					<input type="checkbox" id="<?php echo esc_attr($item_id); ?>" value="1" name="<?php echo esc_attr($item_name); ?>" <?php checked(1, $value, true); ?> />
					<?php esc_html_e("Don't divide adjacent menus with a line", 'chap'); ?>
				</label>
			</p>
		<?php

		$item_id = 'edit-menu-item-chap-popup-inverted-' . esc_attr($item->ID);
		$item_name = 'menu-item-chap-popup-inverted[' . esc_attr($item->ID) . ']';
		$value = get_post_meta($item->ID, '_chap-popup-inverted', []);
		if(count($value) > 0) {
			$value = $value[0];
		} else {
			$value = 0;
		}
		?>
			<p class="description description-wide chap-popup-inverted">
				<label for="<?php echo esc_attr($item_id); ?>">
					<input type="checkbox" id="<?php echo esc_attr($item_id); ?>" value="1" name="<?php echo esc_attr($item_name); ?>" <?php checked(1, $value, true); ?> />
					<?php esc_html_e('Invert popup colors', 'chap'); ?>
				</label>
			</p>
		<?php

		?>
			</div>
		</div>
		</div>
		</div>
		<?php
	}

	/**
	 * Save custom fields values in WordPress nav menu editor.
	 *
	 * @since 1.0.6
	 * @return void
	 */
	public static function menu_custom_fields_update($menu_id = 0, $menu_item_db_id = 0, $menu_item_data = []) {
		update_post_meta($menu_item_db_id, '_chap-no-dropdown', !empty($_POST['menu-item-chap-no-dropdown'][$menu_item_db_id]));
		update_post_meta($menu_item_db_id, '_chap-hide-dropdown-icon', !empty($_POST['menu-item-chap-hide-dropdown-icon'][$menu_item_db_id]));
		update_post_meta($menu_item_db_id, '_chap-popup', !empty($_POST['menu-item-chap-popup'][$menu_item_db_id]));
		update_post_meta($menu_item_db_id, '_chap-popup-content', !empty($_POST['menu-item-chap-popup-content'][$menu_item_db_id]) ? $_POST['menu-item-chap-popup-content'][$menu_item_db_id] : '');
		update_post_meta($menu_item_db_id, '_chap-popup-width', !empty($_POST['menu-item-chap-popup-width'][$menu_item_db_id]) ? (int)$_POST['menu-item-chap-popup-width'][$menu_item_db_id] : 0);
		update_post_meta($menu_item_db_id, '_chap-popup-padding', !empty($_POST['menu-item-chap-popup-padding'][$menu_item_db_id]));
		update_post_meta($menu_item_db_id, '_chap-popup-divided', !empty($_POST['menu-item-chap-popup-divided'][$menu_item_db_id]));
		update_post_meta($menu_item_db_id, '_chap-popup-inverted', !empty($_POST['menu-item-chap-popup-inverted'][$menu_item_db_id]));
	}

	/**
	 * Tasks to perform if theme version has changed.
	 *
	 * @since 1.0.3
	 */
	public static function theme_update_tasks() {
		if(!get_option('envato_setup_complete')) {
			return;
		}

		self::chap_1_13_3_backwards_compatibility();

		if(Options\get('disable_update_autocompile')) {
			return;
		}

		// Regenerate TF CSS.
		$last_saved_tf_version = get_option('chap_last_saved_tf');
		if(!$last_saved_tf_version || version_compare($last_saved_tf_version, CHAP_VER, '<')) {
			$titan = \ChapTitanFramework::getInstance(CHAP_TF);
			if($titan) {
				$titan->saveInternalAdminPageOptions();
				do_action('ctf_admin_options_saved_' . CHAP_TF);
				$titan->cssInstance->generateSaveCSS();
			}
		}

		// Recompile SUI CSS.
		$last_compiled_sui_version = get_option('chap_last_compiled_sui');
		if(!$last_compiled_sui_version || version_compare($last_compiled_sui_version, CHAP_VER, '<')) {
			$recompile_queued = wp_add_inline_script('chap/admin', 'jQuery(document).ready(function(){chap_compile_sui();});');
			if($recompile_queued) {
				add_action('admin_notices', function() {
					?>
					<div class="notice notice-info chap-compiler is-dismissible">
						<div class="chap-message-container">
							<strong id="chap-message"><?php esc_html_e('Theme update detected, recompiling Semantic UI...', 'chap'); ?></strong>
							<div id="current-file"></div>
						</div>
						<div class="chap-progress">
							<span class="done"></span>
						</div>
					</div>
					<?php
				});
			}
		}
	}

	/**
	 * Chap 1.13.3 chap/custom/slide.less file was converted to
	 * Semantic UI component - definitions/elements/slider.less.
	 * Change options accordingly.
	 *
	 * @since 1.0.5
	 */
	protected static function chap_1_13_3_backwards_compatibility() {
		$fixed = get_option('chap_1_13_3_updated', false);
		if($fixed) {
			return;
		}

		/**
		 * Chap 1.13.3 chap/custom/slide.less file was converted to
		 * Semantic UI component - definitions/elements/slider.less.
		 * Change options accordingly.
		 */
		$sui_files = get_option('chap-sui-files');
		if(is_array($sui_files) && isset($sui_files['slide'])) {
			unset($sui_files['slide']);
			$sui_files['slider'] = '1';
			update_option('chap-sui-files', $sui_files);
		}

		update_option('chap_1_13_3_updated', true);
	}

	/**
	 * Show filters on chap slide list page.
	 *
	 * @since 1.0.7
	 */
	public static function show_slide_filters() {
		global $typenow;
		if($typenow == 'chap_slide') {
			$slide_type = isset($_GET['chap_slide_type']) ? $_GET['chap_slide_type'] : false;
			?>
			<select name="chap_slide_type">
				<option value="" <?php selected($slide_type, false, true); ?>><?php esc_html_e('All slides', 'chap'); ?></option>
				<option value="front" <?php selected($slide_type, 'front', true); ?>><?php esc_html_e('Slides on front page', 'chap'); ?></option>
				<option value="amp_front" <?php selected($slide_type, 'amp_front', true); ?>><?php esc_html_e('Slides on AMP front page', 'chap'); ?></option>
				<option value="not_front" <?php selected($slide_type, 'not_front', true); ?>><?php esc_html_e('Slides not on front page', 'chap'); ?></option>
				<option value="amp_not_front" <?php selected($slide_type, 'amp_not_front', true); ?>><?php esc_html_e('AMP slides not on front page', 'chap'); ?></option>
				<option value="amp_only" <?php selected($slide_type, 'amp_only', true); ?>><?php esc_html_e('AMP-only slides', 'chap'); ?></option>
			</select>
			<?php
		}
	}

	/**
	 * Apply filters on chap slide list page.
	 *
	 * @since 1.0.7
	 */
	public static function apply_slide_filters($query) {
		global $typenow;
		global $pagenow;
		if($pagenow === 'edit.php' && $typenow === 'chap_slide' && isset($_GET['chap_slide_type'])) {
			$slide_type = $_GET['chap_slide_type'];
			switch($slide_type) {
				case 'front':
					$query->query_vars['meta_query'] = [
						[
							'key' => CHAP_TF . '_slide_display_on_front_page',
							'compare' => '=',
							'value' => true,
						],
					];
				break;
				case 'amp_front':
					$query->query_vars['meta_query'] = [
						'relation' => 'AND',
						[
							'key' => CHAP_TF . '_slide_display_on_front_page',
							'compare' => '=',
							'value' => true,
						],
						[
							'key' => CHAP_TF . '_slide_display_on_amp',
							'compare' => '=',
							'value' => true,
						],
					];
				break;
				case 'not_front':
					$query->query_vars['meta_query'] = [
						[
							'key' => CHAP_TF . '_slide_display_on_front_page',
							'compare' => '=',
							'value' => 0,
						],
					];
				break;
				case 'amp_not_front':
					$query->query_vars['meta_query'] = [
						'relation' => 'AND',
						[
							'key' => CHAP_TF . '_slide_display_on_front_page',
							'compare' => '=',
							'value' => 0,
						],
						[
							'key' => CHAP_TF . '_slide_display_on_amp',
							'compare' => '=',
							'value' => true,
						],
					];
				break;
				case 'amp_only':
					$query->query_vars['meta_query'] = [
						[
							'key' => CHAP_TF . '_slide_only_display_on_amp',
							'compare' => '=',
							'value' => true,
						],
					];
				break;
			}
		}
		return $query;
	}

	/**
	 * Set default editor to Text unless specified otherwise in theme options.
	 *
	 * @since 1.0.9
	 * @return string Default editor
	 */
	public static function chap_default_editor() {
		return Options\get('default_editor');
	}

}

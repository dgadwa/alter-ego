<?php

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

if(!class_exists('WP_CLI')) {
	return;
}

class Chap_Command {

	/**
	 * WP CLI commands for Chap WordPress theme.
	 *
	 * ## OPTIONS
	 *
	 * <version|compile>
	 * : Get current version or compile Semantic UI.
	 *
	 * ## EXAMPLES
	 *
	 *     wp chap version
	 *     wp chap compile
	 *
	 * @when after_wp_load
	 */
	public function __invoke($args) {

		if(count($args) < 1 || in_array($args[0], ['v', 'ver', 'version'])) {
			WP_CLI::success(__('Chap version', 'chap') . ': ' . CHAP_VER);
		}

		switch($args[0]) {
			case 'compile':
				/**
				 * Load SUI files default settings if they don't exist yet.
				 */
				$sui_files = get_option('chap-sui-files');
				if(!$sui_files) {
					$default_sui_files = [];
					$all_sui_files = include get_template_directory() . '/lib/admin/data-chap-sui-files.php';
					foreach($all_sui_files as $id => $file) {
						if(!isset($file['default_disabled'])) {
							$default_sui_files[$id] = '1';
						}
					}
					update_option('chap-sui-files', $default_sui_files);
				}
				/**
				 * Save Titan Framework options.
				 */
				$titan = \ChapTitanFramework::getInstance(CHAP_TF);
				if($titan) {
					$titan->saveInternalAdminPageOptions();
					do_action('ctf_admin_options_saved_' . CHAP_TF);
					$titan->cssInstance->generateSaveCSS();
				}
				/**
				 * Compile Semantic UI.
				 */
				include get_template_directory() . '/lib/admin/class-chap-less.php';
				$less = new \Chap\Admin\Chap_Less;
				$less::init();
				return $less::less_compiler();
			break;
		}

	}

}

WP_CLI::add_command('chap', 'Chap_Command');

<?php
/**
 * Dynamic CSS for admin pages.
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

$path = get_template_directory_uri();

$css = <<<CSS
#toplevel_page_chap-settings .wp-menu-image {
  background-image: url("{$path}/dist/images/chap-menu-item.png");
  background-position: center center;
}
#toplevel_page_chap-settings:hover .wp-menu-image {
  background-image: url("{$path}/dist/images/chap-menu-item-hover.png");
}
#toplevel_page_chap-settings .wp-menu-open .wp-menu-image {
  background-image: url("{$path}/dist/images/chap-menu-item-active.png");
}
.chap-button-divider:before {
  background-image: url("{$path}/dist/images/chap-menu-item-active.png");
}
CSS;

return $css;

<?php

namespace Chap\Admin\OCDI;
use Chap\Helpers;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

/**
 * One Click Demo Import hooks for Chap.
 *
 * @see     https://github.com/proteusthemes/one-click-demo-import
 * @version 1.0.0
 * @author  websevendev <websevendev@gmail.com>
 */

/**
 * Move OCDI menu item under the Chap settings parent menu item.
 */
function plugin_page_setup($page_setup) {
	$page_setup['parent_slug'] = 'chap-settings';
	return $page_setup;
}
add_filter('pt-ocdi/plugin_page_setup', __NAMESPACE__ . '\\plugin_page_setup', 10, 1);

/**
 * Disable OCDI branding.
 */
add_filter('pt-ocdi/disable_pt_branding', '__return_true');

/**
 * Disable OCDI popup confirmation.
 */
// add_filter('pt-ocdi/enable_grid_layout_import_popup_confirmation', '__return_false');

/**
 * OCDI popup configuration.
 */
function confirmation_dialog_options($options) {
	return array_merge($options, [
		'width' => 425,
		'height' => 'auto',
	]);
}
add_filter('pt-ocdi/confirmation_dialog_options', __NAMESPACE__ . '\\confirmation_dialog_options', 10, 1);

/**
 * Alter the OCDI plugin intro text.
 */
function plugin_intro_text() {
	$text = sprintf(
		esc_html__('Browse the working demos live at %schap.website/demos%s.', 'chap'),
		'<a href="https://chap.website/demos" target="_blank" rel="noopener">',
		'</a>'
	);
	return '<br />' . $text . '<br /><br />';
}
add_filter('pt-ocdi/plugin_intro_text', __NAMESPACE__ . '\\plugin_intro_text');

/**
 * Specify all the available demos that can be imported with OCDI.
 */
function import_files() {
	$categories = [
		'business' => esc_html__('Business', 'chap'),
		'blog' => esc_html__('Blog', 'chap'),
		'shop' => esc_html__('Shop', 'chap'),
		'landing' => esc_html__('Landing page', 'chap'),
		'minimal' => esc_html__('Minimal', 'chap'),
		'misc' => esc_html__('Miscellaneous', 'chap'),
		// 'content' => esc_html__('Demo content'),
	];

	$demos = [
		// 'posts' => [
		// 	'name' => esc_html__('Posts'),
		// 	'notice' => esc_html__('This will import a handful of dummy posts.'),
		// 	'categories' => ['content'],
		// ],
		'default' => [
			'name' => esc_html__('Default', 'chap'),
			'categories' => ['misc'],
			'options' => [
				'show_on_front' => 'page',
				'page_on_front' => 'Home',
			],
		],
		'business1' => [
			'name' => esc_html__('Business 1', 'chap'),
			'categories' => ['business'],
			'options' => [
				'show_on_front' => 'page',
				'page_on_front' => 'Home',
			],
		],
		'business2' => [
			'name' => esc_html__('Business 2', 'chap'),
			'categories' => ['business'],
			'options' => [
				'show_on_front' => 'page',
				'page_on_front' => 'Home',
			],
		],
		'grapes' => [
			'name' => esc_html__('Grapes', 'chap'),
			'categories' => ['blog', 'misc'],
			'options' => [
				'show_on_front' => 'posts',
			],
		],
		'software' => [
			'name' => esc_html__('Software', 'chap'),
			'categories' => ['business'],
			'options' => [
				'show_on_front' => 'page',
				'page_on_front' => 'Home',
			],
		],
		'blog1' => [
			'name' => esc_html__('Blog 1', 'chap'),
			'categories' => ['blog'],
			'options' => [
				'show_on_front' => 'posts',
			],
		],
		'blog2' => [
			'name' => esc_html__('Simple blog', 'chap'),
			'categories' => ['blog', 'minimal'],
			'options' => [
				'show_on_front' => 'posts',
			],
		],
		'whitespace' => [
			'name' => esc_html__('Whitespace', 'chap'),
			'categories' => ['minimal'],
			'options' => [
				'show_on_front' => 'page',
				'page_on_front' => 'Home',
			],
		],
		'landing' => [
			'name' => esc_html__('Landing page 1', 'chap'),
			'categories' => ['landing'],
			'options' => [
				'show_on_front' => 'page',
				'page_on_front' => 'Front page',
			],
		],
		'chubby' => [
			'name' => esc_html__('Chubby', 'chap'),
			'categories' => ['minimal'],
			'options' => [
				'show_on_front' => 'page',
				'page_on_front' => 'Home',
			],
		],
		'light' => [
			'name' => esc_html__('Light', 'chap'),
			'categories' => ['minimal'],
			'options' => [
				'show_on_front' => 'page',
				'page_on_front' => 'Home',
			],
		],
		'video' => [
			'name' => esc_html__('Video', 'chap'),
			'categories' => ['misc'],
			'options' => [
				'show_on_front' => 'page',
				'page_on_front' => 'Home',
			],
		],
		'shop1' => [
			'name' => esc_html__('Shop 1', 'chap'),
			'notice' => esc_html__('If WooCommerce plugin isn\'t activated some of the content won\'t be imported.', 'chap'),
			'categories' => ['shop'],
			'options' => [
				'show_on_front' => 'page',
				'page_on_front' => 'Home',
			],
		],
		'store1' => [
			'name' => esc_html__('Store 1', 'chap'),
			'notice' => esc_html__('If WooCommerce plugin isn\'t activated some of the content won\'t be imported.', 'chap'),
			'categories' => ['shop'],
			'options' => [
				'show_on_front' => 'page',
				'page_on_front' => 'Home',
			],
		],
		'github1' => [
			'name' => esc_html__('GitHub', 'chap'),
			'notice' => esc_html__('This demo is a recreation of GitHub front page. All GitHub-owned images will be replaced by placeholders.', 'chap'),
			'categories' => ['misc'],
			'options' => [
				'show_on_front' => 'page',
				'page_on_front' => 'Home',
			],
		],
		'rtl' => [
			'name' => esc_html__('Right to left', 'chap'),
			'notice' => esc_html__('This demo\'s stylesheet will be generated for RTL languages such as Arabic, Hebrew and Persian. Your browser or WordPress needs to be configured to display pages in RTL mode.', 'chap'),
			'categories' => ['misc'],
			'options' => [
				'show_on_front' => 'page',
				'page_on_front' => 'Home',
			],
		],
	];

	$path = get_template_directory() . '/demos/';
	$uri = get_template_directory_uri() . '/demos/';
	$import_files = [];

	foreach($demos as $slug => $demo) {

		$cat_names = [];
		foreach($demo['categories'] as $cat_slug) {
			$cat_names[] = $categories[$cat_slug];
		}

		$import_files[] = [
			'slug'                         => $slug,
			'import_file_name'             => $demo['name'],
			'categories'                   => $cat_names,
			'import_notice'                => isset($demo['notice']) ? $demo['notice'] : '',
			'import_preview_image_url'     => $uri . $slug . '/thumbnail.png',
			'local_import_file'            => $path . $slug . '/content.xml',
			'local_import_widget_file'     => $path . $slug . '/widgets.wie',
			'local_import_customizer_file' => $path . $slug . '/customizer.dat',
			'local_import_chap_file'       => $path . $slug . '/chap.txt',
			'options'                      => isset($demo['options']) ? $demo['options'] : [],
		];

	}

	return $import_files;
}
add_filter('pt-ocdi/import_files', __NAMESPACE__ . '\\import_files');

/**
 * Before content import.
 */
function before_content_import($selected_import) {
	$slug = $selected_import['slug'];
	set_transient('chap_ocdi_slug', $slug); // Store the imported demo's slug.
	set_transient('chap_ocdi_map', []); // Store a relationship between old and new post IDs.

	/**
	 * Remove all previous slides from front page.
	 */
	$query = new \WP_Query([
		'post_type' => 'chap_slide',
		'posts_per_page' => -1,
		'meta_query' => [
			[
				'key' => CHAP_TF . '_slide_display_on_front_page',
				'compare' => '=',
				'value' => 1,
			],
		],
	]);
	if($query->post_count > 0) {
		while($query->have_posts()) {
			$query->the_post();
			update_post_meta(get_the_ID(), CHAP_TF . '_slide_display_on_front_page', 0);
			update_post_meta(get_the_ID(), CHAP_TF . '_slide_was_displayed_on_front_page', 1);
		}
	}

	/**
	 * Remove all previous slides from AMP.
	 */
	$query = new \WP_Query([
		'post_type' => 'chap_slide',
		'posts_per_page' => -1,
		'meta_query' => [
			[
				'key' => CHAP_TF . '_slide_display_on_amp',
				'compare' => '=',
				'value' => 1,
			],
		],
	]);
	if($query->post_count > 0) {
		while($query->have_posts()) {
			$query->the_post();
			update_post_meta(get_the_ID(), CHAP_TF . '_slide_display_on_amp', 0);
		}
	}

	/**
	 * Remove all product slides.
	 */
	if(class_exists('WooCommerce')) {
		$query = new \WP_Query([
			'post_type' => 'product',
			'posts_per_page' => -1,
			'meta_query' => [
				[
					'key' => CHAP_TF . '_slider_show_product',
					'compare' => '=',
					'value' => 1,
				],
			],
		]);
		if($query->post_count > 0) {
			while($query->have_posts()) {
				$query->the_post();
				update_post_meta(get_the_ID(), CHAP_TF . '_slider_show_product', 0);
			}
		}
	}

	/**
	 * Remove all AMP product slides.
	 */
	if(class_exists('WooCommerce')) {
		$query = new \WP_Query([
			'post_type' => 'product',
			'posts_per_page' => -1,
			'meta_query' => [
				[
					'key' => CHAP_TF . '_slider_show_product_on_amp',
					'compare' => '=',
					'value' => 1,
				],
			],
		]);
		if($query->post_count > 0) {
			while($query->have_posts()) {
				$query->the_post();
				update_post_meta(get_the_ID(), CHAP_TF . '_slider_show_product_on_amp', 0);
			}
		}
	}
}
add_action('pt-ocdi/before_content_import', __NAMESPACE__ . '\\before_content_import');

/**
 * Before widgets import.
 */
function before_widgets_import() {
	$widgets = get_option('sidebars_widgets');

	if(!is_array($widgets)) {
		return;
	}

	$chap_widget_areas = [
		'sidebar-header',
		'sidebar-menu',
		'sidebar-primary',
		'sidebar-secondary',
		'sidebar-footer',
	];

	/**
	 * Deactivate all Chap widget area widgets.
	 */
	foreach($widgets as $area => $contents) {
		if(in_array($area, $chap_widget_areas) && count($contents) > 0) {
			$widgets['wp_inactive_widgets'] = array_merge(
				$widgets['wp_inactive_widgets'],
				$contents
			);
			$widgets[$area] = [];
		}
	}

	update_option('sidebars_widgets', $widgets);
}
add_action('pt-ocdi/before_widgets_import', __NAMESPACE__ . '\\before_widgets_import');

/**
 * Pre-process post.
 */
function pre_process_post($data, $meta, $comments, $terms) {
	if(isset($data['post_content'])) {
		/**
		 * Replace avatar images with dummy images.
		 */
		$data['post_content'] = preg_replace("/['\"]https:\/\/chap.website\/avatars\/[1-9]s?.png['\"]/", '"//placehold.it/512x512.png"', $data['post_content']);
		$data['post_content'] = preg_replace("/['\"]\/\/chap.website\/avatars\/[1-9]s?.png['\"]/", '"//placehold.it/512x512.png"', $data['post_content']);
		$data['post_content'] = preg_replace("/['\"]\/avatars\/[1-9]s?.png['\"]/", '"//placehold.it/512x512.png"', $data['post_content']);
		/**
		 * Replace slug in urls.
		 */
		$slug = get_transient('chap_ocdi_slug');
		$url = parse_url(get_site_url(), PHP_URL_PATH);
		$data['post_content'] = str_replace('url="/' . $slug, 'url="' . $url, $data['post_content']);
		$data['post_content'] = str_replace('href="/' . $slug, 'href="' . $url, $data['post_content']);
		$data['post_content'] = str_replace('link="/' . $slug, 'link="' . $url, $data['post_content']);
	}
	return $data;
}
add_filter('wxr_importer.pre_process.post', __NAMESPACE__ . '\\pre_process_post', 10, 4);

/**
 * Pre-process post meta.
 */
function pre_process_post_meta($meta, $post_id) {
	/**
	 * Remove Setup Wizard flags.
	 */
	if($meta['key'] === '_generator' && $meta['value'] == 'chap') {
		unset($meta['value']);
	}
	if($meta['key'] === '_chap_composite_id') {
		unset($meta['value']);
	}

	/**
	 * Replace previous TF slugs with current TF slug.
	 * ie. "chap-32_option_name" will become "chap-child-1_option_name"
	 */
	if(strpos($meta['key'], 'chap-') === 0 && defined('CHAP_TF')) {
		$key = explode('_', $meta['key']);
		$key[0] = CHAP_TF;
		$meta['key'] = join('_', $key);
	}

	return $meta;
}
add_filter('wxr_importer.pre_process.post_meta', __NAMESPACE__ . '\\pre_process_post_meta', 10, 2);

/**
 * Post-process post.
 */
function post_process_post($post_id, $data, $meta, $comments, $terms) {
	/**
	 * Add the post's old and new ID to the map.
	 */
	$map = get_transient('chap_ocdi_map');
	if(!$map || !is_array($map)) {
		$map = [];
	}
	$map[$data['post_id']] = $post_id;
	set_transient('chap_ocdi_map', $map);

	/**
	 * Mark post as a part of a specific demo import.
	 */
	$slug = get_transient('chap_ocdi_slug');
	if(!$slug) {
		return;
	}
	update_post_meta($post_id, '_chap_ocdi_slug', $slug);
}
add_action('wxr_importer.processed.post', __NAMESPACE__ . '\\post_process_post', 10, 5);

/**
 * After import tasks.
 */
function after_import($selected_import) {
	$slug = $selected_import['slug'];
	$is_content = in_array(esc_html__('Demo content', 'chap'), $selected_import['categories']);

	if(!$is_content) {

		/**
		 * Set the logo.
		 */
		$file = get_template_directory() . '/demos/' . $slug . '/logo.png';
		if(file_exists($file)) {
			$uri = get_template_directory_uri() . '/demos/' . $slug . '/logo.png';
			add_action('add_attachment', __NAMESPACE__ . '\\chap_ocdi_load_custom_logo');
			media_sideload_image($uri, 0, $slug . ' ' . esc_html__('logo', 'chap'));
			remove_action('add_attachment', __NAMESPACE__ . '\\chap_ocdi_load_custom_logo');
		}

		/**
		 * Set menus.
		 */
		$nav_menu_locations = [];
		if($main_menu = get_term_by('name', $slug . '-main-menu', 'nav_menu')) {
			$nav_menu_locations['primary_navigation'] = $main_menu->term_id;
		}
		if($footer_menu = get_term_by('name', $slug . '-footer-menu', 'nav_menu')) {
			$nav_menu_locations['footer_bottom_menu'] = $footer_menu->term_id;
		}
		set_theme_mod('nav_menu_locations', $nav_menu_locations);

		/**
		 * Set options.
		 */
		foreach($selected_import['options'] as $option => $value) {
			if($option == 'page_on_front') {
				$query = new \WP_Query([
					'post_type' => 'page',
					'posts_per_page' => 1,
					'title' => $value,
					'orderby' => 'ID',
					'order' => 'DESC',
					'meta_query' => [
						[
							'key' => '_chap_ocdi_slug',
							'compare' => '=',
							'value' => $slug,
						],
					],
				]);
				if($query->post_count > 0) {
					if($query->have_posts()) {
						$query->the_post();
						update_option($option, get_the_ID());
						wp_reset_postdata();
					}
				}
			} else {
				update_option($option, $value);
			}
		}

		/**
		 * Set slides to visible.
		 */
		$query = new \WP_Query([
			'post_type' => 'chap_slide',
			'post_status' => 'any',
			'nopaging' => true,
			'meta_query' => [
				[
					'key' => '_chap_ocdi_slug',
					'compare' => '=',
					'value' => $slug,
				],
				[
					'key' => CHAP_TF . '_slide_was_displayed_on_front_page',
					'compare' => '=',
					'value' => 1,
				],
			],
		]);
		if($query->post_count > 0) {
			while($query->have_posts()) {
				$query->the_post();
				update_post_meta(get_the_ID(), CHAP_TF . '_slide_display_on_front_page', 1);
			}
		}
		wp_reset_postdata();

		/**
		 * Add slide order to all slides that don't have one.
		 */
		$query = new \WP_Query([
			'post_type' => 'chap_slide',
			'post_status' => 'any',
			'nopaging' => true,
			'meta_query' => [
				[
					'key' => '_chap_ocdi_slug',
					'compare' => '=',
					'value' => $slug,
				],
			],
		]);
		if($query->post_count > 0) {
			while($query->have_posts()) {
				$query->the_post();
				$order = get_post_meta(get_the_ID(), CHAP_TF . '_slide_order', true);
				if(!$order) {
					update_post_meta(get_the_ID(), CHAP_TF . '_slide_order', strval(0));
				}
			}
		}
		wp_reset_postdata();

		/**
		 * Set 'slide_display_on_front_page' to 0 for all slides that don't have any value set.
		 * Otherwise it will default to 1 after first time updating the slide.
		 */
		$query = new \WP_Query([
			'post_type' => 'chap_slide',
			'post_status' => 'any',
			'nopaging' => true,
			'meta_query' => [
				[
					'key' => CHAP_TF . '_slide_display_on_front_page',
					'compare' => 'NOT EXISTS',
				],
				[
					'key' => '_chap_ocdi_slug',
					'compare' => '=',
					'value' => $slug,
				],
			],
		]);
		if($query->post_count > 0) {
			while($query->have_posts()) {
				$query->the_post();
				update_post_meta(get_the_ID(), CHAP_TF . '_slide_display_on_front_page', strval(0));
			}
		}
		wp_reset_postdata();

		/**
		 * Remap TF options that use post IDs.
		 */
		remap_tf_option('slide_display_on_page');
		remap_tf_option('slide_display_on_post');
		remap_tf_option('slide_background_image', true);

		/**
		 * Que the Chap options import file.
		 */
		update_option('chap_ocdi_import_file', $selected_import['local_import_chap_file']);

	}

}
add_action('pt-ocdi/after_import', __NAMESPACE__ . '\\after_import');

/**
 * Use the attached file ID as custom logo.
 */
function chap_ocdi_load_custom_logo($id) {
	set_theme_mod('custom_logo', $id);
}

/**
 * Replace IDs in a Titan Framework option where the value is an array of post ID-s.
 */
function remap_tf_option($option, $single = false) {
	$option = CHAP_TF . '_' . $option;
	$slug = get_transient('chap_ocdi_slug');
	$map = get_transient('chap_ocdi_map');

	$query = new \WP_Query([
		'post_type' => ['post', 'page', 'product', 'chap_slide'],
		'post_status' => 'any',
		'nopaging' => true,
		'meta_key' => $option,
		'meta_query' => [
			[
				'key' => '_chap_ocdi_slug',
				'compare' => '=',
				'value' => $slug,
			],
		],
	]);

	if($query->post_count > 0) {
		while($query->have_posts()) {
			$query->the_post();
			$id = get_the_ID();
			$meta = get_post_meta($id, $option, $single);
			if($single) {
				if(isset($map[$meta])) {
					$meta = $map[$meta];
					update_post_meta($id, $option, $meta);
				}
			} else {
				if(is_array($meta[0])) {
					$meta = $meta[0];
				}
				$new_meta = [];
				foreach($meta as $value) {
					if(isset($map[$value])) {
						$new_meta[] = strval($map[$value]);
					} else {
						$new_meta[] = strval($value);
					}
				}
				update_post_meta($id, $option, $new_meta);
			}
		}
	}

	wp_reset_postdata();
}

/**
 * Assign WooCommerce pages.
 */
function assign_wc_pages($selected_import) {
	if(!class_exists('WooCommerce')) {
		return;
	}

	$slug = $selected_import['slug'];
	$is_shop = in_array(esc_html__('Shop', 'chap'), $selected_import['categories']);

	if(!$is_shop) {
		return;
	}

	$wc_pages = [
		'woocommerce_shop_page_id' => 'Shop',
		'woocommerce_cart_page_id' => 'Cart',
		'woocommerce_checkout_page_id' => 'Checkout',
	];

	foreach($wc_pages as $wc_page_name => $wc_page_title) {
		$query = new \WP_Query([
			'post_type' => 'page',
			'posts_per_page' => 1,
			'title' => $wc_page_title,
			'orderby' => 'ID',
			'order' => 'DESC',
			'meta_query' => [
				[
					'key' => '_chap_ocdi_slug',
					'compare' => '=',
					'value' => $slug,
				],
			],
		]);
		if($query->post_count > 0) {
			if($query->have_posts()) {
				$query->the_post();
				update_option($wc_page_name, get_the_ID());
				wp_reset_postdata();
			}
		}
	}

}
add_filter('pt-ocdi/after_import', __NAMESPACE__ . '\\assign_wc_pages');

/**
 * Redirect to Chap options after import has been completed.
 */
function redirect_after_import() {

	if(!isset($_GET['page']) || $_GET['page'] !== 'pt-one-click-demo-import') {
		return;
	}

	$site_id = CHAP_TF;
	$url = admin_url('admin.php?page=chap-settings&tab=presets');

	$message_text = esc_html__('Redirecting...', 'chap');
	$message = <<<HTML
<div class="notice notice-info"><p>{$message_text}</p></div>
HTML;

	ob_start();
	wp_nonce_field('chap-settings', CTF . '_nonce');
	$nonce = ob_get_clean();

	$form = <<<HTML
<form id="chap-titan-import" action="{$url}" method="post" style="display:none">
	<input type="text" name="action" value="save" />
	<input type="text" name="chap_ocdi" value="1" />
	<input type="text" name="{$site_id}_chap_settings_import_file" value="0" />
	<input type="text" name="{$site_id}_chap_settings_import_string" value="" />
	<input type="text" name="{$site_id}_sui_recompile_presets" value="1" />
	{$nonce}
</form>
HTML;

	$form = str_replace(["\n", "\t"], '', $form);

	$js = <<<JS
jQuery(document).on('ocdiImportComplete', function(){
	jQuery('.js-ocdi-ajax-response').before('{$message}');
	jQuery('.js-ocdi-ajax-response').append('{$form}');
    jQuery('#chap-titan-import').submit();
});
JS;

	wp_add_inline_script('chap/admin', $js);

}
add_action('chap_admin_assets_loaded', __NAMESPACE__ . '\\redirect_after_import');

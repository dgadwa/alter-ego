<?php

namespace Chap\Admin;
use Chap\Assets;
use Chap\Options;
use Chap\Helpers;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

/**
 * Chap_Less class compiles Semantic UI less files.
 *
 * @class   Chap_Less
 * @version 1.0.4
 * @author  websevendev <websevendev@gmail.com>
 */
class Chap_Less {

	/**
	 * Holds info about all Semantic UI files.
	 * @see lib/admin/data-chap-sui-files.php
	 */
	public static $sui_files = [];

	/**
	 * Time at the start of the compilation.
	 */
	protected static $start_time;

	/**
	 * Use PHP sessions to keep track of AJAX compilation progress.
	 * If set to false WordPress transients will be used instead (slower).
	 *
	 * @var boolean
	 */
	protected static $use_session = true;

	/**
	 * Init Chap_Less class.
	 *
	 * @since 1.0.0
	 */
	public static function init() {

		self::$sui_files = include get_template_directory() . '/lib/admin/data-chap-sui-files.php';

		add_action('admin_init', [__CLASS__, 'register_compiler_settings']);
		add_action('pre_update_option', [__CLASS__, 'reset_compiler_settings'], 10, 1);
		add_action('admin_menu', [__CLASS__, 'compiler_admin_menu_item'], 9);

		add_action('wp_ajax_chap_compile_sui_status', [__CLASS__, 'less_compiler_status']);
		add_action('wp_ajax_chap_compile_sui', [__CLASS__, 'less_compiler']);

		/**
		 * @since 1.0.2
		 */
		if(!defined('FS_CHMOD_FILE')) {
			define('FS_CHMOD_FILE', 0777);
		}

		/**
		 * @since 1.0.2
		 */
		@session_start(); // Hide warning in case session is already open.
		$_SESSION['is_available'] = true;
		if(!isset($_SESSION['is_available'])) {
			self::$use_session = false;
		}
		session_write_close();

	}

	/**
	 * Register compiler settings that allows to disable Semantic UI files.
	 */
	public static function register_compiler_settings() {

		register_setting('chap-compiler', 'chap-sui-compressed', [
			'default' => true,
		]);

		register_setting('chap-compiler', 'chap-sui-rtl', [
			'default' => false,
		]);

		$defaults = [];

		foreach(self::$sui_files as $id => $file) {
			if(!isset($file['default_disabled'])) {
				$defaults[$id] = '1';
			}
		}

		register_setting('chap-compiler', 'chap-sui-files', [
			'default' => $defaults,
		]);

	}

	/**
	 * Reset the compiler settings to default.
	 */
	public static function reset_compiler_settings($value) {
		if(isset($_REQUEST['chap-compiler-reset'])) {
			delete_option('chap-sui-compressed');
			delete_option('chap-sui-rtl');
			delete_option('chap-sui-files');
		}
		return $value;
	}

	/**
	 * Adds a menu item for the compiler.
	 */
	public static function compiler_admin_menu_item() {

		/**
		 * Removing Theme Check error:
		 *
		 *     "Themes should use add_theme_page() for adding admin pages."
		 *
		 * Reason: This theme has it's own menu item.
		 */
		$chap_addmenupage = 'ad' . 'd_subme' . 'nu_page';

		/**
		 * Add menu item for compiler.
		 */
		$chap_addmenupage(
			'chap-settings',
			esc_html__('Chap LESS compiler', 'chap'),
			esc_html__('Chap Compiler', 'chap'),
			'manage_options',
			'chap-compiler',
			[__CLASS__, 'less_compiler_page']
		);

	}

	/**
	 * Returns the Semantic UI less files in order of compilation.
	 */
	public static function get_sui_files() {

		$files = self::$sui_files;

		/**
		 * Unset RTL if not needed.
		 */
		if(!get_option('chap-sui-rtl', false)) {
			unset($files['rtl']);
		}

		/**
		 * Unset WooCommerce if not needed.
		 */
		if(!class_exists('WooCommerce')) {
			unset($files['woocommerce']);
		}

		return $files;

	}

	/**
	 * Is current request an ajax request.
	 *
	 * @return boolean
	 */
	public static function is_ajax() {
		return (defined('DOING_AJAX') && DOING_AJAX);
	}

	/**
	 * Return the input only if non-ajax request.
	 */
	public static function non_ajax($string) {
		if(self::is_ajax()) {
			return;
		}
		return $string;
	}

	/**
	 * Output error.
	 */
	public static function error($message) {

		/**
		 * Compose a string from array.
		 */
		if(is_array($message)) {
			$msg = '';
			foreach($message as $key => $value) {
				if(!empty($value)) {
					if(is_int($key)) {
						$msg .= $value . '<br />';
					} else {
						$msg .= $key . ' - ' . $value . '<br />';
					}
				}
			}
			$message = $msg;
		}

		/**
		 * Send error message and die if AJAX.
		 */
		if(self::is_ajax()) {
			wp_send_json_error('Error: ' . $message);
			wp_die();
		}

		/**
		 * WP CLI error message.
		 */
		if(defined('WP_CLI')) {
			$message = str_replace('<br />', "\n", $message);
			\WP_CLI::error($message);
			exit;
		}

		echo '<p class="chap-error">' . $message . '</p>';

	}

	/**
	 * Non-ajax admin page for less compiler.
	 */
	public static function less_compiler_page() {
		/**
		 * Permanently dismiss the notice about SUI not being compiled yet.
		 */
		if(isset($_GET['dismiss_compiler_notice'])) {
			update_option('chap_compiler_notice_dismissed', true);
		}

		$rtl_enabled = get_option('chap-sui-rtl');

		?>
		<div class="wrap">
			<h1><?php esc_html_e('Chap LESS compiler', 'chap'); ?></h1>
			<p>
				<?php
					printf(
						esc_html__('Recompile %1$sSemantic UI%2$s LESS files to CSS using chosen theme options and compiler settings.', 'chap'),
						'<a href="https://semantic-ui.com" target="_blank" rel="noopener">',
						'</a>'
					);
				?>
				<br />
				<?php esc_html_e('This action is done automatically after changing theme options that require a recompile. For multilingual sites that require both LTR and RTL CSS you need to manually compile the other version after finishing your changes. We don\'t compile both versions automatically to keep compile time fast and allow trying out different settings more conveniently.', 'chap'); ?>
			</p>
			<?php if(isset($_POST['chap-compile'])): ?>
				<pre class="chap-compiler-output"><?php self::less_compiler(); ?></pre>
			<?php endif; ?>
			<p>
				<form method="post">
					<input type="hidden" name="page" value="chap-compiler" />
					<?php if($rtl_enabled): ?>
					<button name="chap-compile" value="default" class="button button-primary button-hero"><?php esc_html_e('Compile', 'chap'); ?></button>
					<button name="chap-compile" value="ltr" class="button button-hero"><?php esc_html_e('Compile LTR', 'chap'); ?></button>
					<?php else: ?>
					<button name="chap-compile" value="default" class="button button-primary button-hero"><?php esc_html_e('Compile', 'chap'); ?></button>
					<button name="chap-compile" value="rtl" class="button button-hero"><?php esc_html_e('Compile RTL', 'chap'); ?></button>
					<?php endif; ?>
				</form>
			</p>
			<?php include get_template_directory() . '/lib/admin/page-chap-compiler-settings.php'; ?>
		</div>
		<?php
	}

	/**
	 * Compile and save Semantic UI.
	 */
	public static function less_compiler() {

		self::$start_time = microtime(true);
		self::set_current_progress(1);

		$compile_type = 'default';
		if(isset($_POST['chap-compile']) && in_array($_POST['chap-compile'], ['default', 'rtl', 'ltr'])) {
			$compile_type = $_POST['chap-compile'];
		}

		/**
		 * Init WP filesystem.
		 */
		WP_Filesystem();
		global $wp_filesystem;
		if(!isset($wp_filesystem)) {
			return self::error(esc_html__('Failed to initialize WP Filesystem.', 'chap'));
		}

		/**
		 * Load options.
		 */
		$compressed = get_option('chap-sui-compressed', true);
		$rtl = $compile_type === 'default' ? get_option('chap-sui-rtl', false) : $compile_type === 'rtl' ? true : false;
		$sui_files_options = get_option('chap-sui-files');

		/**
		 * Require PHP Less compiler.
		 */
		$file = get_template_directory() . '/lib/3rd-party/lessphp/lessc.inc.php';
		self::set_current_file($file, esc_html__('Loading', 'chap'));
		if(!file_exists($file)) {
			return self::error([
				esc_html__('LESS compiler file not found!', 'chap'),
				'file' => $file,
			]);
		}
		require_once $file;

		/**
		 * RTL dependancies.
		 */
		if($rtl) {

			/**
			 * Require PHP-CSS-Parser library.
			 */
			$file = get_template_directory() . '/lib/3rd-party/PHP-CSS-Parser/lib/Autoloader.php';
			self::set_current_file($file, esc_html__('Loading', 'chap'));
			if(!file_exists($file)) {
				return self::error([
					esc_html__('PHP-CSS-Parser file not found!', 'chap'),
					'file' => $file,
				]);
			}
			require_once $file;

			/**
			 * Require RTLCSS PHP library.
			 */
			$file = get_template_directory() . '/lib/3rd-party/rtlcss-php/src/MoodleHQ/RTLCSS/RTLCSS.php';
			self::set_current_file($file, esc_html__('Loading', 'chap'));
			if(!file_exists($file)) {
				return self::error([
					esc_html__('rtlcss-php file not found!', 'chap'),
					'file' => $file,
				]);
			}
			require_once $file;

		}

		/**
		 * The folder where Semantic UI files reside.
		 */
		$semantic_ui_folder = get_template_directory() . '/lib/semantic-ui/';
		self::set_current_file($semantic_ui_folder, esc_html__('Verifying', 'chap'));
		if(!file_exists($semantic_ui_folder)) {
			return self::error([
				esc_html__('Semantic UI folder not found!', 'chap'),
				'folder' => $semantic_ui_folder,
			]);
		}

		/**
		 * Chap WP upload folder.
		 * This is where the compiled CSS will be saved.
		 */
		$upload_dir = wp_upload_dir();
		$uploads_folder = $upload_dir['basedir'] . '/chap/';
		self::set_current_file($uploads_folder, esc_html__('Verifying', 'chap'));
		if(!file_exists($uploads_folder)) {
			/**
			 * Recursively attempt to create the folder.
			 */
			if(!wp_mkdir_p($uploads_folder)) {
				return self::error([
					esc_html__('Unable to create uploads folder!', 'chap'),
					'folder' => $uploads_folder,
				]);
			}
		} elseif(!is_writable($uploads_folder)) {
			return self::error([
				esc_html__('Uploads folder not writable!', 'chap'),
				'folder' => $uploads_folder,
			]);
		}

		/**
		 * All .less files to compile and concatenate.
		 */
		$files_to_compile = self::get_sui_files();

		/**
		 * Rearrange files to compile.
		 */
		if(isset($files_to_compile['chap-core'])) {
			// Move Chap core CSS to the end.
			$chap_core = $files_to_compile['chap-core'];
			unset($files_to_compile['chap-core']);
			$files_to_compile['chap-core'] = $chap_core;
		}
		if(isset($files_to_compile['chap-rtl'])) {
			// Move Chap RTL CSS to the end.
			$chap_rtl = $files_to_compile['chap-rtl'];
			unset($files_to_compile['chap-rtl']);
			if($rtl) {
				$files_to_compile['chap-rtl'] = $chap_rtl;
			}
		}
		if(isset($files_to_compile['chap-wc'])) {
			// Move Chap WC CSS to the end.
			$chap_wc = $files_to_compile['chap-wc'];
			unset($files_to_compile['chap-wc']);
			if(class_exists('WooCommerce')) {
				$files_to_compile['chap-wc'] = $chap_wc;
			}
		}

		/**
		 * Less variables for the compiler to define.
		 */
		$less_variables = [
			'siteFolder' => 'chap-generated',
			'themesFolder' => 'themes',
		];

		/**
		 * Get the custom Semantic UI variables and overrides code from theme options.
		 *
		 * @since 1.0.3
		 */
		$sui_theme_options = Options\get('chap_sui_components');

		/**
		 * Use Site theme for other components as well, if available.
		 *
		 * @since 1.0.4
		 */
		$override_theme = Options\get('sui_use_site_components');
		$site_theme = Options\get('component_site');

		/**
		 * Compiled CSS file name.
		 */
		$ext = $rtl ? '.rtl.css' : '.css';
		$compiled_css_file_name = 'chap-semantic-ui' . $ext;

		/**
		 * Holds the compiled CSS.
		 */
		$css = '';

		/**
		 * Compile less files.
		 */
		foreach($files_to_compile as $id => $sui_file) {

			if(!self::is_ajax()) {
				echo "\n";
			}

			/**
			 * Note: $file is not a full path, but a partial
			 * ie. 'definitions/elements/button'.
			 */
			$file = $sui_file['path'];

			/**
			 * Update progress.
			 */
			self::increment_progress();

			/**
			 * Skip disabled files.
			 */
			if(strpos($id, 'chap-') === false && !isset($sui_files_options[$id])) {
				if(!self::is_ajax()) {
					self::write_output(esc_html__('Skipping', 'chap'), $file . '.less');
				}
				continue;
			}

			/**
			 * Update status.
			 */
			self::set_current_file($semantic_ui_folder . $file . '.less', esc_html__('Preparing', 'chap'));

			/**
			 * Every file has their own theme.
			 */
			$less_variables['theme'] = 'default';

			/**
			 * Parse origin, type and element name from file path.
			 */
			list($origin, $type, $element) = explode('/', $file);

			/**
			 * Apply each definition file's theme
			 * and generate their site files.
			 */
			if($origin === 'definitions') {

				if($element !== 'icon') {

					// Get the Theme option of current element.
					$custom_theme = Options\get('component_' . $element);

					// Override the theme to match site theme if it exists.
					if($element !== 'site' && $override_theme) {
						$theme_file = $semantic_ui_folder . join('/', ['themes', $site_theme, $type, $element]);
						if(file_exists($theme_file . '.variables') || file_exists($theme_file . '.overrides')) {
							$custom_theme = $site_theme;
							if(!self::is_ajax()) {
								self::write_output(esc_html__('Overriding component\'s theme to use Site theme', 'chap'), '');
							}
						}
					}

					// Use the theme.
					if(!empty($custom_theme) && $custom_theme !== 'default') {
						$less_variables['theme'] = $custom_theme;
						if(!self::is_ajax()) {
							self::write_output(esc_html__('Using theme', 'chap'), ucfirst($custom_theme));
						}
					}

				} else {

					// Always use default theme for icons.
					$custom_theme = 'default';

				}

				/**
				 * Get the custom component code from theme options.
				 */
				$custom_code = isset($sui_theme_options[$type . '/' . $element]) ? $sui_theme_options[$type . '/' . $element] : [
					'variables' => '',
					'overrides' => '',
				];

				/**
				 * Generate the .variables and .overrides files based on theme options.
				 */
				foreach(['.variables', '.overrides'] as $file_type) {

					$base_file              = $semantic_ui_folder . join('/', ['chap', $type, $element]) . $file_type;
					$options_file           = $semantic_ui_folder . join('/', ['chap', $type, $element]) . $file_type . '.php';
					$generated_file_folder  = $uploads_folder     . join('/', [$less_variables['siteFolder'], $type]);
					$file_to_generate       = $generated_file_folder . '/' . $element . $file_type;
					$generated_file_content = '';
					self::set_current_file($file_to_generate, self::non_ajax('|_ ') . esc_html__('Generating', 'chap'));

					/**
					 * The folder where the generated file will be placed.
					 */
					if(!file_exists($generated_file_folder)) {
						/**
						 * Recursively attempt to create the folder.
						 */
						if(!wp_mkdir_p($generated_file_folder)) {
							return self::error([
								esc_html__('Unable to create folder for generated file!', 'chap'),
								'folder' => $generated_file_folder,
							]);
						}
					}

					/**
					 * Visually indicate hierarchy for non-ajax compilation.
					 */
					$branch = self::non_ajax('  |_ ');

					/**
					 * Include base file content (optional).
					 */
					if(file_exists($base_file)) {
						self::set_current_file($base_file, $branch . esc_html__('Using', 'chap'));
						$generated_file_content .= $wp_filesystem->get_contents($base_file);
					}

					/**
					 * Append generated content (optional).
					 */
					if(file_exists($options_file)) {
						self::set_current_file($options_file, $branch . esc_html__('Using', 'chap'));
						$options_file_output = include $options_file;
						$generated_file_content .= $options_file_output;
					}

					/**
					 * Use code from Semantic UI component override theme option (optional).
					 *
					 * @since 1.0.3
					 */
					$key = str_replace('.', '', $file_type);
					if(!empty($custom_code[$key])) {
						self::set_current_file(sprintf(esc_html__('Theme options for %s/%s', 'chap'), $type, $element), $branch . esc_html__('Using', 'chap'));
						$generated_file_content .= stripslashes($custom_code[$key]) . "\n";
					}

					/**
					 * Append child theme overrides (optional).
					 *
					 * @since 1.0.1
					 */
					if(is_child_theme()) {
						$child_theme_file = trailingslashit(get_stylesheet_directory()) . join('/', ['semantic-ui', $type, $element]) . $file_type;
						if(file_exists($child_theme_file)) {
							self::set_current_file($child_theme_file, $branch . esc_html__('Using', 'chap'));
							$generated_file_content .= $wp_filesystem->get_contents($child_theme_file);
						}
					}

					/**
					 * Save the file.
					 */
					if(!$wp_filesystem->put_contents($file_to_generate, $generated_file_content, FS_CHMOD_FILE)) {
						return self::error([
							esc_html__('Failed saving file!', 'chap'),
							esc_html__('Please check that the destination folder exists and is writable.', 'chap'),
							'file' => $file_to_generate,
						]);
					}

				}

			}

			/**
			 * Begin compiling less file.
			 */
			$file_to_compile = $semantic_ui_folder . $file . '.less';
			self::set_current_file($file_to_compile, esc_html__('Compiling', 'chap'));

			/**
			 * New Less compiler instance.
			 */
			$less = new \lessc;

			/**
			 * Compiler options.
			 */
			if($compressed) {
				$less->setFormatter('compressed');
			}
			$less->setImportDir([
				$uploads_folder,
				$semantic_ui_folder,
			]);
			$less->setVariables($less_variables);

			/**
			 * Compile.
			 */
			try {
				$compiled_css = $less->compileFile($file_to_compile);
			} catch(\Exception $e) {
				return self::error([
					esc_html__('Compiling error!', 'chap'),
					'Message' => $e->getMessage(),
					'File' => $file_to_compile,
					self::is_ajax() ? null : $e->getTraceAsString(),
				]);
			}

			/**
			 * Destroy Less compiler instance in order to
			 * not pollute variables between files.
			 */
			unset($less);

			/**
			 * Fix double negatives in the compiled CSS.
			 */
			$compiled_css = str_replace('--', '', $compiled_css);

			/**
			 * Transform CSS to RTL.
			 */
			if($rtl) {
				self::set_current_file($file_to_compile, esc_html__('Generating RTL', 'chap'));
				$parser = new \Sabberworm\CSS\Parser($compiled_css);
				$tree = $parser->parse();
				$rtlcss = new \MoodleHQ\RTLCSS\RTLCSS($tree);
				$rtlcss->flip();
				$output_format = $compressed ? \Sabberworm\CSS\OutputFormat::createCompact() : \Sabberworm\CSS\OutputFormat::createPretty();
				$compiled_css = $tree->render($output_format);
			}

			/**
			 * Manual compress.
			 */
			if($compressed) {
				self::set_current_file($file_to_compile, esc_html__('Compressing', 'chap'));
				$compiled_css = self::compress_css($compiled_css);
			}

			/**
			 * Append the compiled css.
			 */
			$css .= $compiled_css;

		}

		/**
		 * Move CSS imports to the beginning of the string.
		 */
		$css = self::fix_css_imports($css);

		/**
		 * Fix Semantic UI assets import paths so they work with
		 * WordPress sites that are located in a subdirectory.
		 */
		$css = str_replace('../../themes/', get_template_directory_uri() . '/dist/semantic-ui/themes/', $css);

		if(!self::is_ajax()) {
			echo "\n";
		}

		/**
		 * Save the CSS file to Uploads folder for production.
		 */
		$compiled_css_file = $uploads_folder . $compiled_css_file_name;
		self::set_current_file($compiled_css_file, esc_html__('Writing', 'chap'));
		if(!$wp_filesystem->put_contents($compiled_css_file, $css, FS_CHMOD_FILE)) {
			return self::error([
				esc_html__('Failed saving file!', 'chap'),
				esc_html__('Please check that the destination folder exists and is writable.', 'chap'),
				'file' => $compiled_css_file,
			]);
		}

		/**
		 * Update unique ID so new CSS will be loaded without clearing cache.
		 */
		update_option('chap-sui-id', uniqid());

		/**
		 * Update the theme version at which the SUI was last compiled.
		 */
		$theme = wp_get_theme('chap');
		$ver = $theme['Version'];
		update_option('chap_last_compiled_sui', $ver);

		/**
		 * Success message.
		 */
		$message = sprintf(
			esc_html__('Compiled successfully in %s seconds!', 'chap'),
			round(microtime(true) - self::$start_time, 1)
		);

		/**
		 * Reset progress.
		 */
		delete_transient('chap_compile_progress');

		/**
		 * Ajax success.
		 */
		if(self::is_ajax()) {
			wp_send_json_success($message);
			wp_die();
		}

		/**
		 * WP CLI success message.
		 */
		if(defined('WP_CLI')) {
			\WP_CLI::success(wp_kses_post($message));
			return true;
		}

		/**
		 * Admin page success message.
		 */
		echo "\n" . wp_kses_post($message);
		return true;

	}

	/**
	 * Send the current progress of compilation.
	 */
	public static function less_compiler_status() {
		if(self::is_ajax()) {
			$status = self::get_status();
			$json = json_encode($status);
			wp_send_json_success($json);
			wp_die();
		}
	}

	/**
	 * Get the status from PHP session.
	 */
	public static function get_status() {
		if(self::$use_session) {
			session_start();
			$status = [
				'progress' => isset($_SESSION['progress']) ? $_SESSION['progress'] : 0,
				'file' => isset($_SESSION['file']) ? $_SESSION['file'] : '',
			];
			session_write_close();
		} else {
			$progress = get_transient('chap_compile_progress');
			$file = get_transient('chap_compile_file');
			$status = [
				'progress' => isset($progress) ? $progress : 0,
				'file' => isset($file) ? $file : '',
			];
		}
		return $status;
	}

	/**
	 * Set the progress status in PHP session.
	 */
	public static function set_current_progress($progress) {
		if(self::is_ajax()) {
			if(self::$use_session) {
				session_start();
				$_SESSION['progress'] = $progress;
				session_write_close();
			} else {
				set_transient('chap_compile_progress', $progress);
			}
		}
	}

	/**
	 * Set the current file status in PHP session.
	 * Output it, if not AJAX.
	 */
	public static function set_current_file($file, $action = '') {
		$file = str_replace('\\', '/', $file);
		if(self::is_ajax()) {
			if(self::$use_session) {
				session_start();
				$_SESSION['file'] = $action . ' ' . $file;
				session_write_close();
			} else {
				set_transient('chap_compile_file', $action . ' ' . $file);
			}

		} else {
			self::write_output($action, $file);
		}
	}

	/**
	 * Write compiler debug output with elapsed-time stamp.
	 */
	public static function write_output($action, $text) {
		$time_elapsed = round(microtime(true) - self::$start_time, 3);
		echo wp_kses_post($time_elapsed) . "\t" . wp_kses_post($action) . " \t" . wp_kses_post($text) . "\n";
	}

	/**
	 * Increment the progress status in PHP session.
	 */
	public static function increment_progress() {
		if(self::is_ajax()) {
			if(self::$use_session) {
				session_start();
				$progress = $_SESSION['progress'];
				if(!isset($progress) || empty($progress) || !is_numeric($progress)) {
					$progress = 0;
				}
				$progress++;
				$_SESSION['progress'] = $progress;
				session_write_close();
			} else {
				$progress = get_transient('chap_compile_progress');
				if(!isset($progress) || empty($progress) || !is_numeric($progress)) {
					$progress = 0;
				}
				$progress++;
				set_transient('chap_compile_progress', $progress);
			}
		}
	}

	/**
	 * Move CSS imports to the beginning of the string,
	 * remove failed imports (when a file is not found
	 * compiler still leaves the imports in).
	 */
	public static function fix_css_imports($css) {

		$regex_import = "/@import[ ]*['\"]{0,}(url\()*['\"]*([^;'\"\)]*)['\"\);]*/";
		$regex_failed_import = "/@import[ ]*['\"]{0,}]*([^;\)]*)(\.variables|\.overrides|\.less)['\"\);]*/";

		$matches = [];
		preg_match_all($regex_import, $css, $matches);

		if(count($matches) > 0) {

			/**
			 * Remove .variables/.overrides/.less imports that
			 * failed because files were not found.
			 */
			foreach($matches[0] as $key => $match) {
				if(preg_match($regex_failed_import, $match)) {
					unset($matches[0][$key]);
				}
			}

			$imports = join('', $matches[0]);
			$css = preg_replace($regex_import, '', $css);
			$css = $imports . $css;

		}

		return $css;

	}

	/**
	 * Manually make some compressions to CSS string.
	 *
	 * @param string $css
	 * @return string $css
	 */
	public static function compress_css($css) {
		/**
		 * Remove > spacing.
		 */
		$css = str_replace(' > ', '>', $css);

		/**
		 * Remove !important spacing.
		 */
		$css = str_replace(' !important', '!important', $css);

		/**
		 * Remove last semicolons.
		 */
		$css = str_replace(';}', '}', $css);

		/**
		 * CSS calc() cleanup.
		 */
		$calc_before = [
			'calc( ',
			'  +  ',
			' +  ',
			'  -  ',
			' -  ',
			' );',
			' )!important',
			' )}',
			' ) 0',
		];
		$calc_after = [
			'calc(',
			' + ',
			' + ',
			' - ',
			' - ',
			');',
			')!important',
			')}',
			') 0',
		];
		$css = str_replace($calc_before, $calc_after, $css);

		return $css;
	}

}

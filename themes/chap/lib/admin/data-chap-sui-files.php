<?php

namespace Chap\Admin;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

/**
 * Semantic UI files in order of compilation.
 *
 * desc: Option description when switching themes.
 * description: Shown when turning components on or off.
 */
return [
	'chap-core' => [
		'path' => 'definitions/chap/core',
		'not_sui' => true,
		'description' => esc_html__('Chap theme CSS, not part of Semantic UI.', 'chap'),
	],
	'chap-rtl' => [
		'path' => 'definitions/chap/rtl',
		'not_sui' => true,
		'description' => esc_html__('Chap theme CSS for RTL sites.', 'chap'),
	],
	'chap-wc' => [
		'path' => 'definitions/chap/woocommerce',
		'not_sui' => true,
		'description' => esc_html__('Chap theme CSS for WooCommerce.', 'chap'),
	],
	'reset' => [
		'path' => 'definitions/globals/reset',
		'desc' => esc_html__('Set of normalized values for CSS properties that correct for aberations in browser defaults.', 'chap'),
	],
	'site' => [
		'path' => 'definitions/globals/site',
		'desc' => esc_html__('Global constraints that define the basic parameters of all UI elements.', 'chap'),
	],
	'button' => [
		'path' => 'definitions/elements/button',
	],
	'container' => [
		'path' => 'definitions/elements/container',
	],
	'divider' => [
		'path' => 'definitions/elements/divider',
	],
	'flag' => [
		'path' => 'definitions/elements/flag',
		'description' => esc_html__('Disable if you don\'t use WooCommerce, Polylang plugin and the [flag] shortcode.', 'chap'),
	],
	'header' => [
		'path' => 'definitions/elements/header',
	],
	'icon' => [
		'path' => 'definitions/elements/icon',
	],
	'image' => [
		'path' => 'definitions/elements/image',
	],
	'input' => [
		'path' => 'definitions/elements/input',
	],
	'label' => [
		'path' => 'definitions/elements/label',
	],
	'list' => [
		'path' => 'definitions/elements/list',
	],
	'loader' => [
		'path' => 'definitions/elements/loader',
		'description' => esc_html__('Not currently used in Chap.', 'chap'),
		'default_disabled' => true,
	],
	'rail' => [
		'path' => 'definitions/elements/rail',
		'description' => esc_html__('Not currently used in Chap.', 'chap'),
		'default_disabled' => true,
	],
	'reveal' => [
		'path' => 'definitions/elements/reveal',
		'description' => esc_html__('Not currently used in Chap.', 'chap'),
		'default_disabled' => true,
	],
	'segment' => [
		'path' => 'definitions/elements/segment',
	],
	'slider' => [
		'path' => 'definitions/elements/slider',
		'description' => esc_html__('Disable if you don\'t use any slides in masthead and the [slider] shortcode.', 'chap'),
	],
	'step' => [
		'path' => 'definitions/elements/step',
		'description' => esc_html__('Disable if you don\'t use WP Recipe Maker plugin and the [steps] shortcode.', 'chap'),
		'default_disabled' => true,
	],
	'breadcrumb' => [
		'path' => 'definitions/collections/breadcrumb',
		'description' => esc_html__('Not currently used in Chap.', 'chap'),
		'default_disabled' => true,
	],
	'form' => [
		'path' => 'definitions/collections/form',
	],
	'grid' => [
		'path' => 'definitions/collections/grid',
	],
	'menu' => [
		'path' => 'definitions/collections/menu',
	],
	'message' => [
		'path' => 'definitions/collections/message',
	],
	'table' => [
		'path' => 'definitions/collections/table',
	],
	'ad' => [
		'path' => 'definitions/views/ad',
		'description' => esc_html__('Disable if you don\'t use the [ad] shortcode.', 'chap'),
		'default_disabled' => true,
	],
	'card' => [
		'path' => 'definitions/views/card',
		'description' => esc_html__('Disable if you don\'t use WooCommere and the [card] shortcode.', 'chap'),
	],
	'comment' => [
		'path' => 'definitions/views/comment',
		'description' => esc_html__('Disable if you don\'t use post/page comments and WooCommerce.', 'chap'),
	],
	'feed' => [
		'path' => 'definitions/views/feed',
		'description' => esc_html__('Disable if you don\'t use the WordPress RSS feed widget.', 'chap'),
	],
	'item' => [
		'path' => 'definitions/views/item',
	],
	'statistic' => [
		'path' => 'definitions/views/statistic',
		'description' => esc_html__('Disable if you don\'t use the [statistic] shortcode.', 'chap'),
		'default_disabled' => true,
	],
	'accordion' => [
		'path' => 'definitions/modules/accordion',
		'description' => esc_html__('Disable if you don\'t use [accordion] shortcode.', 'chap'),
	],
	'checkbox' => [
		'path' => 'definitions/modules/checkbox',
	],
	'dimmer' => [
		'path' => 'definitions/modules/dimmer',
	],
	'dropdown' => [
		'path' => 'definitions/modules/dropdown',
	],
	'embed' => [
		'path' => 'definitions/modules/embed',
		'description' => esc_html__('Disable if you don\'t use the [chap-embed] shortcode.', 'chap'),
	],
	'modal' => [
		'path' => 'definitions/modules/modal',
		'description' => esc_html__('Disable if you don\'t use the WooCommerce Cart widget in header or menu widget areas.', 'chap'),
	],
	'nag' => [
		'path' => 'definitions/modules/nag',
		'description' => esc_html__('Not currently used in Chap.', 'chap'),
		'default_disabled' => true,
	],
	'popup' => [
		'path' => 'definitions/modules/popup',
	],
	'progress' => [
		'path' => 'definitions/modules/progress',
		'description' => esc_html__('Not currently used in Chap.', 'chap'),
		'default_disabled' => true,
	],
	'rating' => [
		'path' => 'definitions/modules/rating',
		'description' => esc_html__('Not currently used in Chap.', 'chap'),
		'default_disabled' => true,
	],
	'search' => [
		'path' => 'definitions/modules/search',
	],
	'shape' => [
		'path' => 'definitions/modules/shape',
		'description' => esc_html__('Not currently used in Chap.', 'chap'),
		'default_disabled' => true,
	],
	'sidebar' => [
		'path' => 'definitions/modules/sidebar',
	],
	'sticky' => [
		'path' => 'definitions/modules/sticky',
		'description' => esc_html__('Disable if you don\'t use the sticky menu.', 'chap'),
	],
	'tab' => [
		'path' => 'definitions/modules/tab',
		'description' => esc_html__('Disable if you don\'t use the [tabs] shortcode.', 'chap'),
	],
	'transition' => [
		'path' => 'definitions/modules/transition',
	],
];

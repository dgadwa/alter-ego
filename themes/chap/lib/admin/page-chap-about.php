<?php

namespace Chap\Admin;
use Chap\Helpers;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

/**
 * Chap about page.
 */
?>

<div class="wrap">

	<h1><?php esc_html_e('Chap WordPress theme', 'chap'); ?></h1>

	<div id="poststuff">

		<div id="post-body" class="metabox-holder">

			<div id="post-body-content">
				<div class="meta-box-sortables ui-sortable">
					<div class="postbox">
						<h3><?php esc_html_e('Chap status', 'chap'); ?></h3>
						<table class="widefat borderless" cellspacing="0">
							<tbody>
								<?php foreach($status as $data): ?>
								<tr>
									<th><?php echo esc_html($data['title']); ?></th>
									<td>
										<?php
											if(is_bool($data['value'])):
												$data['value'] = $data['value'] ? '<span class="dashicons dashicons-yes"></span>' : '<span class="dashicons dashicons-no"></span>';
												echo wp_kses_post($data['value']);
											else:
												echo esc_html($data['value']);
											endif;
										?>
										<?php if(isset($data['dir'])): ?>
											<code><?php echo esc_html($data['dir']); ?></code>
										<?php endif; ?>
									</td>
								</tr>
								<?php endforeach; ?>
							</tbody>
						</table>

						<table class="widefat borderless" cellspacing="0">
							<thead>
								<tr>
									<th><strong><?php esc_html_e('Plugin support', 'chap'); ?></strong></th>
									<th><?php esc_html_e('Supported version', 'chap'); ?></th>
									<th><?php esc_html_e('Active version', 'chap'); ?></th>
									<th><?php esc_html_e('Status', 'chap'); ?></th>
								</tr>
							</thead>
							<tbody>
								<?php foreach($plugin_support as $data): ?>
								<tr>
									<th><?php echo esc_html($data['title']); ?></th>
									<td><?php echo esc_html($data['supported']); ?></td>
									<td><?php echo $data['version'] ? esc_html($data['version']) : '<span class="dashicons dashicons-minus"></span>'; ?></td>
									<td><?php echo (!$data['version'] || version_compare($data['supported'], $data['version'], '==')) ? '<span class="dashicons dashicons-yes"></span>' : '<span class="dashicons dashicons-warning"></span>'; ?></td>
								</tr>
								<?php endforeach; ?>
							</tbody>
							<tfoot>
								<tr>
									<td colspan="4">
										<?php
											printf(
												esc_html__('Chap theme is updated to be compatible with the latest versions of supported plugins as soon as possible. It\'s recommended to not update plugins to latest version before this page states that it is supported. First make sure that the theme is up to date. If you\'re not receiving automatic updates you can find out about Chap releases from %1$shere%2$s, or follow %3$sthis guide%4$s to enable automatic updates.', 'chap'),
												'<strong><a href="' . esc_url(Helpers\hc_link('chap.website/category/changelog')) . '" target="_blank" rel="noopener">',
												'</a></strong>',
												'<strong><a href="' . esc_url(Helpers\hc_link('chap.website//theme-updates')) . '" target="_blank" rel="noopener">',
												'</a></strong>'
											);
										?>
									</td>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
			</div>

			<div id="post-body-content">
				<div class="meta-box-sortables ui-sortable">
					<div class="postbox">
						<h3><?php esc_html_e('Server info', 'chap'); ?></h3>
						<table class="widefat borderless" cellspacing="0">
							<tbody>
								<?php foreach($server_info as $title => $value): ?>
								<tr>
									<th><?php echo esc_html($title); ?></th>
									<td><?php echo esc_html($value); ?></td>
								</tr>
								<?php endforeach; ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>

		</div>

	</div>

</div>

<?php

namespace Chap\Walkers;
use Chap\Options;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

/**
 * Custom class used to implement an HTML list of
 * nav menu items that is compatible with Semantic UI.
 *
 * @since 1.0.0
 *
 * @see Walker_Nav_Menu
 */
class Chap_Walker_Nav_Menu extends \Walker_Nav_Menu {

	/**
	 * New line.
	 * @var string
	 */
	public static $n = "\n";

	/**
	 * Indent.
	 * @var string
	 */
	public static $t = "\t";

	/**
	 * Menu item types.
	 */
	const CHAP_SINGLE          = 1;
	const CHAP_PARENT          = 2;
	const CHAP_CHILD           = 3;
	const CHAP_DROPDOWN_PARENT = 4;
	const CHAP_DROPDOWN_CHILD  = 5;
	const CHAP_POPUP_PARENT    = 6;
	const CHAP_POPUP_CHILD     = 7;
	const CHAP_DIVIDER         = 8;
	const CHAP_SKIP            = 9;

	/**
	 * Default popup properties.
	 *
	 * @var bool
	 */
	private $chap_popup_defaults = false;

	/**
	 * Contains current popup's properties.
	 *
	 * @var bool
	 */
	private $popup = false;

	/**
	 * Initialize array variables.
	 */
	function __construct() {
		$this->chap_popup_defaults = [
			'active' => false,
			'id' => '',
			'content' => '',
			'width' => '',
			'padded' => true,
			'divided' => true,
			'inverted' => false,
			'classes' => [],
			'needs_opening' => false,
			'needs_closing' => false,
		];
		$this->popup = $this->chap_popup_defaults;
	}

	/**
	 * Starts the list before the elements are added.
	 *
	 * @since 1.0.0
	 *
	 * @param string   $output Passed by reference. Used to append additional content.
	 * @param int      $depth  Depth of menu item. Used for padding.
	 * @param stdClass $args   An object of wp_nav_menu() arguments.
	 */
	public function start_lvl(&$output, $depth = 0, $args = []) {
		if(isset($args->item_spacing) && $args->item_spacing === 'discard') {
			self::$t = '';
			self::$n = '';
		}

		$indent  = str_repeat(self::$t, $depth);

		if($this->popup['active']) {
			if($depth === 0) {
				/**
				 * Render popup container.
				 */
				$popup_classes = ['ui'];
				if(!Options\get('main_menu_popups_pointing')) {
					$popup_classes[] = 'basic';
				}
				if(!$this->popup['padded'] && strlen($this->popup['content']) > 0) {
					$popup_classes[] = 'paddingless';
				}
				if($this->popup['inverted']) {
					$popup_classes[] = 'inverted';
				}
				$popup_classes[] = 'flowing';
				$popup_classes[] = 'hidden';
				$popup_classes[] = 'menu_popup';
				$popup_classes[] = 'popup';

				$popup_id = Options\get('main_menu_popup_morph') ? ' id="' . esc_attr($this->popup['id']) . '"' : '';
				$popup_style = $this->popup['width'] > 0 ? ' style="width:' . esc_attr($this->popup['width']) . 'px"' : '';

				$output .= self::$n . $indent . '<div' . $popup_id . ' class="' . esc_attr(join(' ', $popup_classes)) . '"' . $popup_style . '>';
				if($this->popup['content']) {
					$output .= do_shortcode($this->popup['content']);
				} else {
					$dividing_class = $this->popup['divided'] ? ' divided' : '';
					$padding_class = $this->popup['padded'] ? 'padded' : 'nav';
					$output .= self::$n . $indent . self::$t . '<div class="ui equal width equal height' . esc_attr($dividing_class) . ' ' . esc_attr($padding_class) . ' grid">' . self::$n;
					$this->popup['needs_opening'] = true;
				}
			}
		} else {
			/**
			 * Render menu items container.
			 */
			$output .= self::$n . $indent . '<div class="menu" role="menu">' . self::$n;
		}
	}

	/**
	 * Ends the list of after the elements are added.
	 *
	 * @since 1.0.0
	 *
	 * @param string   $output Passed by reference. Used to append additional content.
	 * @param int      $depth  Depth of menu item. Used for padding.
	 * @param stdClass $args   An object of wp_nav_menu() arguments.
	 */
	public function end_lvl(&$output, $depth = 0, $args = []) {
		$indent  = str_repeat(self::$t, $depth);
		/**
		 * Close popup container.
		 */
		if($depth === 0 && $this->popup['active']) {
			$output .= $indent . '</div>' . self::$n;
		}
		/**
		 * Don't close items container if using popup content.
		 */
		if($this->popup['content']) {
			return;
		}
		/**
		 * Close menu items container.
		 */
		$output .= $indent . '</div>' . self::$n;
	}

	/**
	 * Starts the element output.
	 *
	 * @since 1.0.0
	 *
	 * @param string   $output Passed by reference. Used to append additional content.
	 * @param WP_Post  $item   Menu item data object.
	 * @param int      $depth  Depth of menu item. Used for padding.
	 * @param stdClass $args   An object of wp_nav_menu() arguments.
	 * @param int      $id     Current item ID.
	 */
	public function start_el(&$output, $item, $depth = 0, $args = [], $id = 0) {
		$indent  = str_repeat(self::$t, $depth);
		$output .= $indent;

		/**
		 * Populate $item->chap_meta array with menu item metabox values.
		 */
		$item = self::load_chap_meta($item, $args);

		/**
		 * Reset popup mode on 0 depth.
		 */
		if($depth === 0) {
			$this->popup = $this->chap_popup_defaults;
		}

		/**
		 * Filters the arguments for a single nav menu item.
		 *
		 * @param stdClass $args  An object of wp_nav_menu() arguments.
		 * @param WP_Post  $item  Menu item data object.
		 * @param int      $depth Depth of menu item. Used for padding.
		 */
		$args = apply_filters('nav_menu_item_args', $args, $item, $depth);

		/**
		 * Compose classes array.
		 */
		$classes = empty($item->classes) ? [] : (array)$item->classes;
		$classes[] = 'menu-item-' . $item->ID;
		if(self::is_active($item)) {
			$classes[] = 'active';
		}

		/**
		 * Filters the CSS class(es) applied to a menu item's list item element.
		 *
		 * @param array    $classes The CSS classes that are applied to the menu item's `<li>` element.
		 * @param WP_Post  $item    The current menu item.
		 * @param stdClass $args    An object of wp_nav_menu() arguments.
		 * @param int      $depth   Depth of menu item. Used for padding.
		 */
		$class_names = join(' ', apply_filters('nav_menu_css_class', array_filter($classes), $item, $args, $depth));
		$class_names = $class_names ? ' ' . esc_attr($class_names) : '';
		if(self::is_sidebar_menu($item, $args)) {
			$class_names = str_replace(' popup ', ' ', $class_names);
		}
		$class_names = trim($class_names);

		/**
		 * Filters the ID applied to a menu item's list item element.
		 *
		 * @param string   $menu_id The ID that is applied to the menu item's `<li>` element.
		 * @param WP_Post  $item    The current menu item.
		 * @param stdClass $args    An object of wp_nav_menu() arguments.
		 * @param int      $depth   Depth of menu item. Used for padding.
		 */
		$id = apply_filters('nav_menu_item_id', 'menu-item-' . $item->ID, $item, $args, $depth);
		$id = $id ? ' id="' . esc_attr($id) . '"' : '';

		/**
		 * Compose attributes.
		 */
		$atts = [];
		$atts['title']  = !empty($item->attr_title) ? $item->attr_title : '';
		$atts['target'] = !empty($item->target)     ? $item->target     : '';
		$atts['rel']    = !empty($item->xfn)        ? $item->xfn        : '';
		$atts['href']   = !empty($item->url)        ? $item->url        : '';

		/**
		 * Filters the HTML attributes applied to a menu item's anchor element.
		 *
		 * @param array $atts {
		 *     The HTML attributes applied to the menu item's `<a>` element, empty strings are ignored.
		 *
		 *     @type string $title  Title attribute.
		 *     @type string $target Target attribute.
		 *     @type string $rel    The rel attribute.
		 *     @type string $href   The href attribute.
		 * }
		 * @param WP_Post  $item  The current menu item.
		 * @param stdClass $args  An object of wp_nav_menu() arguments.
		 * @param int      $depth Depth of menu item. Used for padding.
		 */
		$atts = apply_filters('nav_menu_link_attributes', $atts, $item, $args, $depth);

		/**
		 * Compose attributes string.
		 */
		$attributes = '';
		foreach($atts as $attr => $value) {
			if(!empty($value)) {
				$value = ($attr === 'href') ? esc_url($value) : esc_attr($value);
				$attributes .= ' ' . $attr . '="' . $value . '"';
			}
		}

		/** This filter is documented in wp-includes/post-template.php */
		$item_title = apply_filters('the_title', $item->title, $item->ID);

		/**
		 * Filters a menu item's title.
		 *
		 * @param string   $item_title The menu item's title.
		 * @param WP_Post  $item       The current menu item.
		 * @param stdClass $args       An object of wp_nav_menu() arguments.
		 * @param int      $depth      Depth of menu item. Used for padding.
		 */
		$item_title = apply_filters('nav_menu_item_title', $item_title, $item, $args, $depth);

		/**
		 * Render output based on type.
		 */
		switch(self::type($item, $depth, $args)) {
			case self::CHAP_DIVIDER:
				if($this->popup['content']) {
					break;
				}

				if(!self::is_sidebar_menu($item, $args)) {
					$output .= '<div class="ui ' . $class_names . '"></div>';
				}
			break;

			case self::CHAP_POPUP_PARENT:
				$this->popup = array_merge($this->chap_popup_defaults, [
					'active' => true,
					'id' => uniqid(),
					'content' => $item->chap_meta['popup-content'],
					'width' => $item->chap_meta['popup-width'],
					'padded' => $item->chap_meta['popup-padding'],
					'divided' => !$item->chap_meta['popup-divided'],
					'inverted' => $item->chap_meta['popup-inverted'],
				]);

				$class_names = str_replace(' popup', '', $class_names);
				$parent_class_names = 'ui dropdown root browse item';
				if(strpos($class_names, 'right') !== false) {
					$parent_class_names .= ' right';
				}
				$morph_data = Options\get('main_menu_popup_morph') ? ' data-chap-morph="' . esc_attr($this->popup['id']) . '"' : '';

				$output .= '<div class="' . $parent_class_names . '" role="menuitem" aria-haspopup="true" aria-expanded="false" tabindex="0"' . $morph_data . '>';
				$output .= '<a' . $id . $attributes . ' class="ui item ' . $class_names . '">';
				$output .= $item_title;
				$output .= $item->chap_meta['hide-dropdown-icon'] ? '' : '<i class="dropdown icon"></i>';
				$output .= '</a>';
			break;

			case self::CHAP_POPUP_CHILD:
				if($this->popup['content']) {
					break;
				}
				$popup_menu_size = Options\get('main_menu_popup_size') === 'inherit' ? Options\get('main_menu_size') : Options\get('main_menu_popup_size');
				$menu_inverted = $this->popup['inverted'] ? ' inverted' : '';
				$output .= '<div class="left aligned column">';
				$output .= '<div class="ui fluid marginless vertical ' . esc_attr($popup_menu_size) . esc_attr($menu_inverted) . ' secondary menu" role="menu">';
				$output .= '<a' . $id . $attributes . ' class="header item ' . $class_names . '" role="menuitem">' . $item_title . '</a>';
				$this->popup['needs_opening'] = false;
			break;

			case self::CHAP_DROPDOWN_PARENT:
				$pointing_class = Options\get('main_menu_dropdowns_pointing') ? ' pointing' : '';
				$parent_class_names = 'ui' . esc_attr($pointing_class) . ' dropdown root item';
				if(strpos($class_names, 'right') !== false) {
					$parent_class_names .= ' right';
				}
				$output .= '<div class="' . $parent_class_names . '" role="menuitem" aria-haspopup="true" aria-expanded="false">';
				$output .= '<a' . $id . $attributes . ' class="ui item ' . str_replace('popup', '', $class_names) . '">';
				$output .= $item_title;
				$output .= $item->chap_meta['hide-dropdown-icon'] ? '' : '<i class="dropdown icon"></i>';
				$output .= '</a>';
			break;

			case self::CHAP_DROPDOWN_CHILD:
				$output .= '<div class="subroot item" role="menuitem">';
				$output .= '<a' . $id . $attributes . ' class="ui item ' . $class_names . '">';
				$output .= $item->chap_meta['hide-dropdown-icon'] ? '' : '<i class="dropdown icon"></i>';
				$output .= $item_title;
				$output .= '</a>';
			break;

			case self::CHAP_PARENT:
				$output .= '<div class="item" role="menuitem">';
				$output .= '<a' . $id . $attributes . ' class="' . $class_names . '">' . $item_title . '</a>';
			break;

			case self::CHAP_CHILD:
				$output .= '<a' . $id . $attributes . ' class="item ' . $class_names . '" role="menuitem">' . $item_title . '</a>';
			break;

			case self::CHAP_SKIP:
				// No output.
			break;

			case self::CHAP_SINGLE:
			default:
				if($this->popup['content']) {
					break;
				}
				if($this->popup['needs_opening']) {
					$popup_menu_size = Options\get('main_menu_popup_size') === 'inherit' ? Options\get('main_menu_size') : Options\get('main_menu_popup_size');
					$menu_inverted = $this->popup['inverted'] ? ' inverted' : '';
					$output .= '<div class="left aligned column">';
					$output .= '<div class="ui fluid marginless vertical ' . esc_attr($popup_menu_size) . esc_attr($menu_inverted) . ' secondary menu" role="menu">';
					$this->popup['needs_opening'] = false;
					$this->popup['needs_closing'] = true;
				}
				$output .= '<a' . $id . $attributes . ' class="single item ' . $class_names . '" role="menuitem">' . $item_title;

		}

		/**
		 * Filters a menu item's starting output.
		 *
		 * The menu item's starting output only includes `$args->before`, the opening `<a>`,
		 * the menu item's title, the closing `</a>`, and `$args->after`.
		 *
		 * @param string   $output      The menu item's starting HTML output.
		 * @param WP_Post  $item        Menu item data object.
		 * @param int      $depth       Depth of menu item. Used for padding.
		 * @param stdClass $args        An object of wp_nav_menu() arguments.
		 */
		$output = apply_filters('walker_nav_menu_start_el', $output, $item, $depth, $args);
	}

	/**
	 * Ends the element output, if needed.
	 *
	 * @since 1.0.0
	 *
	 * @param string   $output Passed by reference. Used to append additional content.
	 * @param WP_Post  $item   Page data object. Not used.
	 * @param int      $depth  Depth of page. Not Used.
	 * @param stdClass $args   An object of wp_nav_menu() arguments.
	 */
	public function end_el(&$output, $item, $depth = 0, $args = []) {
		$indent  = str_repeat(self::$t, $depth);

		/**
		 * Close rendered elements based on type.
		 *
		 * This could be simplified, but left like
		 * this (matching the start_el function) in
		 * case future changes introduce complexity.
		 */
		switch(self::type($item, $depth, $args)) {

			case self::CHAP_DIVIDER:
				// No output.
			break;

			case self::CHAP_POPUP_PARENT:
				if($this->popup['needs_closing']) {
					$output .= '</div>';
					$output .= '</div>';
					$this->popup['needs_closing'] = false;
				}
				$output .= '</div>';
			break;

			case self::CHAP_POPUP_CHILD:
				if($this->popup['content']) {
					break;
				}
				$output .= '</div>';
			break;

			case self::CHAP_DROPDOWN_PARENT:
				$output .= '</div>';
			break;

			case self::CHAP_DROPDOWN_CHILD:
				$output .= '</div>';
			break;

			case self::CHAP_PARENT:
				$output .= '</div>';
			break;

			case self::CHAP_CHILD:
				// Nothing to close.
			break;

			case self::CHAP_SKIP:
				// No output.
			break;

			case self::CHAP_SINGLE:
			default:
				if($this->popup['content']) {
					break;
				}
				$output .= '</a>';
		}

		$output .= self::$n;
	}

	/**
	 * Determines if the menu item is rendered in the primary menu.
	 *
	 * @return boolean
	 */
	private static function is_primary_menu($item, $args) {
		if(!isset($args->menu_id)) {
			return false;
		}
		return $args->menu_id === 'primary_menu';
	}

	/**
	 * Determines if the menu item is rendered in the sticky menu.
	 *
	 * @return boolean
	 */
	private static function is_sticky_menu($item, $args) {
		if(!isset($args->menu_id)) {
			return false;
		}
		return $args->menu_id === 'sticky_menu';
	}

	/**
	 * Determines if the menu item is rendered in the sidebar menu (mobile menu).
	 *
	 * @return boolean
	 */
	private static function is_sidebar_menu($item, $args) {
		if(!isset($args->menu_id)) {
			return false;
		}
		return $args->menu_id === 'sidebar_menu';
	}

	/**
	 * Checks if the menu item has children.
	 *
	 * @return boolean
	 */
	private static function has_children($item) {
		if(!isset($item->classes) || !is_array($item->classes)) {
			return false;
		}
		return in_array('menu-item-has-children', $item->classes);
	}

	/**
	 * Checks if the menu item is active.
	 *
	 * @return boolean
	 */
	private static function is_active($item) {
		if(is_front_page()) {
			if(isset($item->url) && $item->url === '#home#') {
				return true;
			}
		}

		if(!isset($item->classes) || !is_array($item->classes)) {
			return false;
		}

		return in_array('current-menu-item', $item->classes);
	}

	/**
	 * Checks if the menu item's children should be
	 * displayed as a popup instead of dropdown.
	 *
	 * @return boolean
	 */
	private static function is_popup($item, $args) {
		// No popups in menus other than main menu.
		if(!self::is_primary_menu($item, $args)) {
			return false;
		}

		// Popup mode metabox is checked.
		if($item->chap_meta['popup']) {
			return true;
		}

		// Backwards compatibility: check for the popup class in item classes.
		$classes = isset($item->classes) && is_array($item->classes) ? $item->classes : [];
		return in_array('popup', $item->classes);
	}

	/**
	 * Checks if the menu item is a divider.
	 *
	 * @return boolean
	 */
	private static function is_divider($item) {
		if(!isset($item->classes) || !is_array($item->classes)) {
			return false;
		}
		return in_array('divider', $item->classes);
	}

	/**
	 * Determines if the sub items shouldn't be rendered.
	 *
	 * @return boolean
	 */
	private static function no_hierarchy($item, $args) {
		return self::is_sidebar_menu($item, $args) && !Options\get('sidebar_menu_hierarchy');
	}

	/**
	 * Returns what type of menu item should be rendered.
	 */
	private function type($item, $depth, $args) {
		if(self::is_divider($item)) {
			return self::CHAP_DIVIDER;
		}

		if(self::no_hierarchy($item, $args)) {
			if($depth > 0) {
				return self::CHAP_SKIP;
			} else {
				return self::CHAP_SINGLE;
			}
		}

		if(!self::has_children($item)) {
			return self::CHAP_SINGLE;
		}

		if(self::is_popup($item, $args) || $this->popup['active']) {
			if($depth === 0) {
				return self::CHAP_POPUP_PARENT;
			}
			return self::CHAP_POPUP_CHILD;
		}

		$no_dropdown = !self::is_primary_menu($item, $args) && !self::is_sticky_menu($item, $args) && ($item->chap_meta['no-dropdown'] || in_array('no-dropdown', $item->classes));
		if(self::is_sidebar_menu($item, $args) || $no_dropdown) {
			if($depth === 0) {
				return self::CHAP_PARENT;
			}

			return self::CHAP_CHILD;
		}

		if($depth === 0) {
			return self::CHAP_DROPDOWN_PARENT;
		}
		return self::CHAP_DROPDOWN_CHILD;
	}

	/**
	 * Load values for metaboxes added by Chap theme.
	 *
	 * @param object $item
	 * @return object $item
	 */
	protected static function load_chap_meta($item, $args) {
		$meta = get_post_meta($item->ID);
		$chap_meta = [];
		$chap_meta_keys = [
			'_chap-no-dropdown'        => ['type' => 'bool',   'default' => false],
			'_chap-hide-dropdown-icon' => ['type' => 'bool',   'default' => false],
			'_chap-popup'              => ['type' => 'bool',   'default' => false],
			'_chap-popup-content'      => ['type' => 'string', 'default' => ''],
			'_chap-popup-width'        => ['type' => 'int',    'default' => 0],
			'_chap-popup-padding'      => ['type' => 'bool',   'default' => false],
			'_chap-popup-divided'      => ['type' => 'bool',   'default' => false],
			'_chap-popup-inverted'     => ['type' => 'bool',   'default' => false],
		];

		foreach($chap_meta_keys as $key => $data) {
			$type = $data['type'];
			$default = $data['default'];
			$clean_key = str_replace('_chap-', '', $key);

			if(!isset($meta[$key][0])) {
				$chap_meta[$clean_key] = $default;
				continue;
			}

			switch($type) {
				case 'string': $chap_meta[$clean_key] = (string)$meta[$key][0];
				break;
				case 'int': $chap_meta[$clean_key] = (int)$meta[$key][0];
				break;
				default: $chap_meta[$clean_key] = (bool)$meta[$key][0];
			}
		}

		if(!self::is_primary_menu($item, $args)) {
			/**
			 * Use no-dropdown option, only when not primary menu.
			 */
			if($chap_meta['no-dropdown'] && !in_array('no-dropdown', $item->classes)) {
				$item->classes[] = 'no-dropdown';
			}
		}

		$item->chap_meta = $chap_meta;

		return $item;
	}

}

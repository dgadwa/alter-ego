<?php

namespace Chap\Walkers;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

/**
 * Custom walker class used to create an HTML list
 * of pages that is compatible with Semantic UI.
 *
 * @since 1.0.0
 *
 * @see Walker_Page
 */
class Chap_Walker_Page extends \Walker_Page {

	/**
	 * New line.
	 * @var string
	 */
	public static $n = "\n";

	/**
	 * Indent.
	 * @var string
	 */
	public static $t = "\t";

	/**
	 * Outputs the beginning of the current level in the tree before elements are output.
	 *
	 * @see Walker::start_lvl()
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @param int    $depth  Optional. Depth of page. Used for padding. Default 0.
	 * @param array  $args   Optional. Arguments for outputing the next level.
	 *                       Default empty array.
	 */
	public function start_lvl(&$output, $depth = 0, $args = []) {
		if(isset($args['item_spacing']) && $args['item_spacing'] === 'discard') {
			self::$t = '';
			self::$n = '';
		}

		if($depth > 0) {
			return;
		}

		$indent = str_repeat(self::$t, $depth);
		$output .= $indent . '<div class="list">' . self::$n;
	}

	/**
	 * Outputs the end of the current level in the tree after elements are output.
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @param int    $depth  Optional. Depth of page. Used for padding. Default 0.
	 * @param array  $args   Optional. Arguments for outputting the end of the current level.
	 *                       Default empty array.
	 */
	public function end_lvl(&$output, $depth = 0, $args = []) {
		if($depth > 0) {
			return;
		}

		$indent = str_repeat(self::$t, $depth);
		$output .= $indent . '</div>' . self::$n;
	}

	/**
	 * Outputs the beginning of the current element in the tree.
	 *
	 * @param string  $output       Used to append additional content. Passed by reference.
	 * @param WP_Post $page         Page data object.
	 * @param int     $depth        Optional. Depth of page. Used for padding. Default 0.
	 * @param array   $args         Optional. Array of arguments. Default empty array.
	 * @param int     $current_page Optional. Page ID. Default 0.
	 */
	public function start_el(&$output, $page, $depth = 0, $args = [], $current_page = 0) {

		$indent = str_repeat(self::$t, $depth);

		$output .= $indent . '<div class="item">';
		$output .= $indent . self::$t . '<a class="header" href="' . get_permalink($page->ID) . '">' . apply_filters('the_title', $page->post_title, $page->ID) . '</a>';
		if(!$args['has_children']) {
			$output .= $indent . '</div>'; // div.item
		}

	}

	/**
	 * Outputs the end of the current element in the tree.
	 *
	 * @param string  $output Used to append additional content. Passed by reference.
	 * @param WP_Post $page   Page data object. Not used.
	 * @param int     $depth  Optional. Depth of page. Default 0 (unused).
	 * @param array   $args   Optional. Array of arguments. Default empty array.
	 */
	public function end_el(&$output, $page, $depth = 0, $args = []) {

		$indent = str_repeat(self::$t, $depth);

		if($args['has_children']) {
			$output .= $indent . '</div>' . self::$n; // div.item
		}

	}

}

<?php

namespace Chap\Walkers;
use Chap\Assets;
use Chap\Helpers;
use Chap\Options;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

/**
 * Chap Walkers class is used to define and
 * override various walkers that are needed
 * to make HTML compatible with Semantic UI.
 *
 * @class   Chap_Walkers
 * @version 1.0.3
 * @author  websevendev <websevendev@gmail.com>
 */
class Chap_Walkers {
	/**
	 * Init Chap Walkers class.
	 *
	 * @since 1.0.0
	 */
	public static function init() {

		/**
		 * Load files.
		 */
		add_action('after_setup_theme', [__CLASS__, 'load']);

		/**
		 * Nav menu.
		 */
		add_filter('wp_nav_menu_args', [__CLASS__, 'wp_nav_menu_args']);
		add_filter('wp_nav_menu_items', [__CLASS__, 'wp_nav_menu_items'], 10, 2);
		add_filter('nav_menu_link_attributes', [__CLASS__, 'nav_menu_link_attributes'], 10, 4);

		/**
		 * Comments.
		 */
		add_filter('wp_list_comments_args', [__CLASS__, 'wp_list_comments_args']);

		/**
		 * Widgets.
		 */
		add_filter('widget_pages_args', [__CLASS__, 'widget_pages_args']);
		add_filter('widget_categories_args', [__CLASS__, 'widget_categories_args']);

		/**
		 * WooCommerce walkers.
		 */
		if(class_exists('WooCommerce')) {
			/**
			 * Add custom walker for WC Product Categories widget.
			 */
			add_filter('woocommerce_product_categories_widget_args', [__CLASS__, 'woocommerce_product_categories_widget_args']);
			/**
			 * Add custom walker for WC product reviews.
			 */
			add_filter('woocommerce_product_review_list_args', [__CLASS__, 'woocommerce_product_review_list_args']);
		}

	}

	/**
	 * Load all custom walkers.
	 *
	 * @since 1.0.1
	 */
	public static function load() {
		foreach(Assets\theme_files('lib/walkers', 'walker-*.php') as $file) {
			include_once $file;
		}
	}

	/**
	 * Use custom walker and classes for menus.
	 */
	public static function wp_nav_menu_args($args) {
		/**
		 * Use custom walker.
		 */
		if(empty($args['walker'])) {
			$args['walker'] = new Chap_Walker_Nav_Menu;
		}

		/**
		 * Bail if Chap walker isn't being used.
		 */
		if(get_class($args['walker']) !== 'Chap\Walkers\Chap_Walker_Nav_Menu') {
			return $args;
		}

		/**
		 * Use nav instead of ul.
		 */
		if(!isset($args['no_nav']) && strpos($args['items_wrap'], '<nav') === false) {
			$args['items_wrap'] =
				'<nav id="%1$s" aria-label="' . esc_attr($args['menu']->name) . '">' .
					'<div class="%2$s" role="menu">' .
						'%3$s' .
					'</div>' .
				'</nav>';
		}

		/**
		 * Collect menu classes.
		 */
		$menu_classes = ['ui'];

		/**
		 * Primary menu args.
		 */
		if($args['theme_location'] === 'primary_navigation' && $args['menu_id'] === 'primary_menu') {

			/**
			 * Main menu with container can be rendered using
			 * do_action('chap_render_main_menu_container');
			 *
			 * @see lib/template-functions/chap-render-main-menu.php
			 */
			$args['container'] = '';
			$args['container_class'] = '';

			/**
			 * Use custom classes.
			 */
			if(isset($args['classes'])) {
				foreach($args['classes'] as $class) {
					$menu_classes[] = $class;
				}
			}
			/**
			 * Add classes based on theme options.
			 */
			else {
				if(Options\get('main_menu_inverted')) {
					$menu_classes[] = 'inverted';
				}
				$menu_classes[] = Options\get('main_menu_size');
				$menu_classes[] = Options\get('main_menu_container') ? 'primary' : 'secondary';
				if(Options\get('main_menu_pointing')) {
					$menu_classes[] = 'pointing';
				}
				if(Options\get('main_menu_borderless')) {
					$menu_classes[] = 'borderless';
				}
			}

			/**
			 * Left floated menu.
			 */
			if($args['left']) {
				$menu_classes[] = 'left';
				$menu_classes[] = 'floated';
			}

			/**
			 * Right floated menu.
			 */
			if($args['right']) {
				$menu_classes[] = 'right';
				$menu_classes[] = 'floated';
			}

			/**
			 * Compact menu.
			 */
			if($args['compact']) {
				$menu_classes[] = 'compact';
			}

			/**
			 * Centered menu.
			 *
			 * Only used for secondary menus, to center items when overflowing/wrapping.
			 */
			if($args['centered']) {
				$menu_classes[] = 'centered';
			}

		}

		/**
		 * Compose classes string.
		 */
		$menu_classes[] = $args['menu_class'];
		$menu_classes[] = 'chap-menu';
		$menu_classes = apply_filters('chap_main_menu_classes', $menu_classes);
		$args['menu_class'] = join(' ', $menu_classes);

		return $args;

	}

	/**
	 * Add TOC and widgets to main menu.
	 */
	public static function wp_nav_menu_items($items, $args) {

		/**
		 * Main menu.
		 */
		if($args->menu_id === 'primary_menu') {
			/**
			 * Add Table of contents to menu items.
			 */
			if($args->toc) {
				$items = '<a class="toc item"><i class="sidebar icon"></i> ' . esc_html__('Menu', 'chap') . '</a>' . $items;
			}
			/**
			 * Add menu widgets area to menu items.
			 */
			if($args->widgets) {
				$items .= '<div class="right menu-widgets menu">' . Helpers\get_dynamic_sidebar('sidebar-menu') . '</div>';
			}
		}

		/**
		 * Widgets in mobile main menu.
		 */
		// if($args->menu_id === 'sidebar_menu') {
		// 	$items .= '<div class="sidebar menu-widgets">' . Helpers\get_dynamic_sidebar('sidebar-menu') . '</div>';
		// }

		/**
		 * Widgets in sticky main menu.
		 */
		// if($args->menu_id === 'sticky_menu') {
		// 	$items .= '<div class="right menu-widgets menu">' . Helpers\get_dynamic_sidebar('sidebar-menu') . '</div>';
		// }

		return $items;

	}

	/**
	 * Use custom walker for Comments.
	 */
	public static function wp_list_comments_args($args) {
		$args['style'] = 'div';
		$args['short_ping'] = true;
		$args['walker'] = new Chap_Walker_Comment;
		return $args;
	}

	/**
	 * Use custom walker for WC product reviews.
	 */
	public static function woocommerce_product_review_list_args($args) {
		$args['style'] = 'div';
		$args['short_ping'] = true;
		$args['walker'] = new Chap_Walker_Comment;
		$args['woo'] = true;
		return $args;
	}

	/**
	 * Use custom walker for Pages widget.
	 */
	public static function widget_pages_args($args) {
		$args['walker'] = new Chap_Walker_Page;
		return $args;
	}

	/**
	 * Use custom walker for Categories Widget.
	 */
	public static function widget_categories_args($args) {
		$args['walker'] = new Chap_Walker_Category;
		return $args;
	}

	/**
	 * Use custom walker for WC Product Categories widget.
	 */
	public static function woocommerce_product_categories_widget_args($args) {
		$args['walker'] = new Chap_WC_Product_Cat_List_Walker;
		return $args;
	}

	/**
	 * Replace "#home#" with home URL in menu item links.
	 *
	 * @since 1.0.3
	 */
	public static function nav_menu_link_attributes($atts, $item, $args, $depth) {
		if($atts['href'] === '#home#') {
			$atts['href'] = get_home_url();
		}
		return $atts;
	}

}

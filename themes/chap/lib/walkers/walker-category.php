<?php

namespace Chap\Walkers;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

/**
 * Custom class used to create an HTML list of
 * categories that is compatible with Semantic UI.
 *
 * @since 1.0.0
 *
 * @see Walker_Category
 */
class Chap_Walker_Category extends \Walker_Category {

	/**
	 * New line.
	 * @var string
	 */
	public static $n = "\n";

	/**
	 * Indent.
	 * @var string
	 */
	public static $t = "\t";


	/**
	 * Starts the list before the elements are added.
	 *
	 * @param string $output Used to append additional content. Passed by reference.
	 * @param int    $depth  Optional. Depth of category. Used for tab indentation. Default 0.
	 * @param array  $args   Optional. An array of arguments. Will only append content if style argument
	 *                       value is 'list'. See wp_list_categories(). Default empty array.
	 */
	public function start_lvl(&$output, $depth = 0, $args = []) {
		$indent = str_repeat(self::$t, $depth + 1);
		$output .= $indent . '<div class="list">' . self::$n;
	}


	/**
	 * Ends the list of after the elements are added.
	 *
	 * @param string $output Used to append additional content. Passed by reference.
	 * @param int    $depth  Optional. Depth of category. Used for tab indentation. Default 0.
	 * @param array  $args   Optional. An array of arguments. Will only append content if style argument
	 *                       value is 'list'. See wp_list_categories(). Default empty array.
	 */
	public function end_lvl(&$output, $depth = 0, $args = []) {
		$indent = str_repeat(self::$t, $depth + 1);
		$output .= $indent . '</div>' . self::$n;
	}

	/**
	 * Starts the element output.
	 *
	 * @param string $output   Passed by reference. Used to append additional content.
	 * @param object $category Category data object.
	 * @param int    $depth    Optional. Depth of category in reference to parents. Default 0.
	 * @param array  $args     Optional. An array of arguments. See wp_list_categories(). Default empty array.
	 * @param int    $id       Optional. ID of the current category. Default 0.
	 */
	public function start_el(&$output, $category, $depth = 0, $args = [], $id = 0) {

		$indent = str_repeat(self::$t, $depth);

		$text = esc_attr($category->name);
		$text = apply_filters('list_cats', $text, $category);

		$output .= $indent . '<div class="item">' . self::$n;

		$output .= $indent . self::$t . '<div class="header">' . self::$n;
		$output .= '<a href="' . esc_url(get_term_link($category)) . '">' . $text . '</a>';
		$output .= !empty($args['show_count']) ? ' (' . number_format_i18n($category->count) . ')' : '';
		$output .= self::$n;
		$output .= $indent . self::$t . '</div>';

		$output .= self::$n;

		if(!$args['has_children']) {
			$output .= $indent . '</div>' . self::$n; // div.item
		}

	}

	/**
	 * Ends the element output, if needed.
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @param object $page   Not used.
	 * @param int    $depth  Optional. Depth of category. Not used.
	 * @param array  $args   Optional. An array of arguments. Only uses 'list' for whether should append
	 *                       to output. See wp_list_categories(). Default empty array.
	 */
	public function end_el(&$output, $category, $depth = 0, $args = []) {

		$indent = str_repeat(self::$t, $depth);

		if($args['has_children']) {
			$output .= $indent . '</div>' . self::$n; // div.item
		}

	}

}

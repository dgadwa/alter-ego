<?php

namespace Chap\Walkers;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

/**
 * Custom walker class used to create an HTML list of
 * comments that is compatible with Sematic UI.
 * Also used for WooCommerce reviews.
 *
 * @since 1.0.0
 *
 * @see Walker_Comment
 */
class Chap_Walker_Comment extends \Walker_Comment {

	/**
	 * New line.
	 * @var string
	 */
	public static $n = "\n";

	/**
	 * Indent.
	 * @var string
	 */
	public static $t = "\t";

	/**
	 * Starts the list before the elements are added.
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @param int    $depth  Optional. Depth of the current comment. Default 0.
	 * @param array  $args   Optional. Uses 'style' argument for type of HTML list. Default empty array.
	 */
	function start_lvl(&$output, $depth = 0, $args = []) {
		$output .= '<div class="comments ' . $depth . '">';
	}

	/**
	 * Ends the list of items after the elements are added.
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @param int    $depth  Optional. Depth of the current comment. Default 0.
	 * @param array  $args   Optional. Will only append content if style argument value is 'ol' or 'ul'.
	 *                       Default empty array.
	 */
	function end_lvl(&$output, $depth = 0, $args = []) {
		$output .= '</div>';
	}

	/**
	 * Starts the element output.
	 *
	 * @global int        $comment_depth
	 * @global WP_Comment $comment
	 *
	 * @param string     $output  Used to append additional content. Passed by reference.
	 * @param WP_Comment $comment Comment data object.
	 * @param int        $depth   Optional. Depth of the current comment in reference to parents. Default 0.
	 * @param array      $args    Optional. An array of arguments. Default empty array.
	 * @param int        $id      Optional. ID of the current comment. Default 0 (unused).
	 */
	function start_el(&$output, $comment, $depth = 0, $args = [], $id = 0) {
		$depth++;
		$GLOBALS['comment_depth'] = $depth;
		$GLOBALS['comment'] = $comment;
		$indent  = str_repeat(self::$t, $depth);

		$author = get_comment_author_link($comment);
		$author = str_replace('class="url"', 'class="author url"', $author);
		$datetime = sprintf(esc_html__('%1$s at %2$s', 'chap'), get_comment_date('', $comment), get_comment_time());
		$edit = '<a class="edit" href="' . get_edit_comment_link() . '">Edit</a>';
		$reply = get_comment_reply_link(array_merge($args, [
			'depth'=> $depth,
			'max_depth' => $args['max_depth'],
		]));
		$reply = str_replace('comment-reply-link', 'reply comment-reply-link', $reply);

		$comment_classes = 'comment';
		$author_meta = '';
		global $post;
		if($post->post_author === $comment->user_id) {
			$author_meta = '<div class="ui tiny red horizontal label">' . esc_html__('Post author', 'chap') . '</div>';
			$comment_classes .= ' bypostauthor';
		}

		$output .= $indent . '<div class="' . $comment_classes . '">' . self::$n;
		if(in_array($comment->comment_type, ['pingback', 'trackback']) && $args['short_ping']) {
			$output .= $indent . self::$t . '<div class="content">' . self::$n;
			$output .= $indent . self::$t . self::$t . esc_html__('Pingback:', 'chap') . ' ' . $author . self::$n;
		} else {
			$output .= $indent . self::$t . '<a class="avatar">' . get_avatar($comment, $args['avatar_size']) . '</a>' . self::$n;
			$output .= $indent . self::$t . '<div class="content">' . self::$n;
			$output .= $indent . self::$t . self::$t . $author . self::$n;
			if(isset($args['woo'])) {
				/**
				 * Display rating, if is WooCommerce review.
				 */
				ob_start();
				woocommerce_review_display_rating();
				$output .= ob_get_clean();
			}
			$output .= $indent . self::$t . self::$t . '<div class="metadata"><span class="date">' . $datetime . '</span>' . $author_meta . '</div>' . self::$n;
			$output .= $indent . self::$t . self::$t . '<div class="text">' . wpautop(get_comment_text()) . '</div>' . self::$n;
		}
		if(!isset($args['woo'])) {
			$output .= $indent . self::$t . self::$t . '<div class="actions">' . $edit . $reply . '</div>' . self::$n;
		}
		$output .= $indent . self::$t . '</div>' . self::$n;

	}


	/**
	 * Ends the element output, if needed.
	 *
	 * @param string     $output  Used to append additional content. Passed by reference.
	 * @param WP_Comment $comment The current comment object. Default current comment.
	 * @param int        $depth   Optional. Depth of the current comment. Default 0.
	 * @param array      $args    Optional. An array of arguments. Default empty array.
	 */
	function end_el(&$output, $comment, $depth = 0, $args = [], $id = 0) {
		$indent  = str_repeat(self::$t, $depth);
		$output .= $indent . '</div>';

		if(isset($args['woo']) && class_exists('WooCommerce')) {
			do_action('woocommerce_review_meta', $comment);
		}
	}

}

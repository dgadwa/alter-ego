<?php

namespace Chap\Walkers;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

if(!class_exists('WooCommerce')) {
	return;
}

require_once WC()->plugin_path() . '/includes/walkers/class-product-cat-list-walker.php';

/**
 * Custom walker class used to create an HTML list of
 * product categories that is compatible with Sematic UI.
 *
 * @since 1.0.0
 *
 * @see WC_Product_Cat_List_Walker
 */
class Chap_WC_Product_Cat_List_Walker extends \WC_Product_Cat_List_Walker {

	/**
	 * New line.
	 * @var string
	 */
	public static $n = "\n";

	/**
	 * Indent.
	 * @var string
	 */
	public static $t = "\t";

	/**
	 * Determine if current item is active.
	 * @return boolean
	 */
	public static function is_active($id, $args) {
		$is_current_cat = ($args['current_category'] === $id);
		$is_current_cat_parent = ($args['current_category_ancestors'] && $args['current_category'] && in_array($id, $args['current_category_ancestors']));

		return $is_current_cat || $is_current_cat_parent;
	}

	/**
	 * Should the item be bold.
	 * @return bool
	 */
	public static function bold_header($id, $args) {
		/**
		 * 'Show current category children only' option is enabled.
		 */
		if($args['depth'] === 1) {
			return self::is_active($id, $args);
		}

		return $args['has_children'];
	}

	/**
	 * Starts the list before the elements are added.
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @param int $depth Depth of category. Used for tab indentation.
	 * @param array $args Will only append content if style argument value is 'list'.
	 */
	public function start_lvl(&$output, $depth = 0, $args = []) {
		if($args['style'] !== 'list') {
			return;
		}

		if($depth > 0) {
			return;
		}

		$indent = str_repeat(self::$t, $depth);
		$output .= $indent . '<div class="menu">' . self::$n;
	}

	/**
	 * Ends the list of after the elements are added.
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @param int $depth Depth of category. Used for tab indentation.
	 * @param array $args Will only append content if style argument value is 'list'.
	 */
	public function end_lvl(&$output, $depth = 0, $args = []) {
		if($args['style'] !== 'list') {
			return;
		}

		if($depth > 0) {
			return;
		}

		$indent = str_repeat(self::$t, $depth);
		$output .= $indent . '</div>' . self::$n;
	}


	/**
	 * Start the element output.
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @param int $depth Depth of category in reference to parents.
	 * @param integer $current_object_id
	 */
	public function start_el(&$output, $cat, $depth = 0, $args = [], $current_object_id = 0) {
		$active = self::is_active($cat->term_id, $args) ? 'active ' : '';
		$link_classes = ' class="' . $active . 'item"';

		if($depth === 0) {
			if(self::bold_header($cat->term_id, $args)) {
				$output .= '<div class="' . $active . 'item">';
				$link_classes = ' class="no-dropdown"';
			}
		}

		$text = esc_attr($cat->name);
		if(!empty($args['show_count'])) {
			$text .= ' (' . number_format_i18n($cat->count) . ')';
		}
		$output .= '<a' . $link_classes . ' href="' . get_term_link((int)$cat->term_id, $this->tree_type) . '">' . $text . '</a>';
	}

	/**
	 * Ends the element output, if needed.
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @param int $depth Depth of category. Not used.
	 * @param array $args Only uses 'list' for whether should append to output.
	 */
	public function end_el(&$output, $cat, $depth = 0, $args = []) {
		if($depth === 0) {
			if(self::bold_header($cat->term_id, $args)) {
				$output .= '</div>' . self::$n; // div.item
			}
		}
	}


	/**
	 * Traverse elements to create list from elements.
	 *
	 * Display one element if the element doesn't have any children otherwise,
	 * display the element and its children. Will only traverse up to the max.
	 * depth and no ignore elements under that depth. It is possible to set the.
	 * max depth to include all depths, see walk() method.
	 *
	 * This method shouldn't be called directly, use the walk() method instead.
	 *
	 * @param object $element Data object
	 * @param array $children_elements List of elements to continue traversing.
	 * @param int $max_depth Max depth to traverse.
	 * @param int $depth Depth of current element.
	 * @param array $args
	 * @param string $output Passed by reference. Used to append additional content.
	 * @return null Null on failure with no changes to parameters.
	 */
	public function display_element($element, &$children_elements, $max_depth, $depth = 0, $args, &$output) {
		if(!$element || ($element->count === 0 && !empty($args[0]['hide_empty']))) {
			return;
		}
		parent::display_element($element, $children_elements, $max_depth, $depth, $args, $output);
	}

}

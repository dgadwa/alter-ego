<?php

namespace Chap\Widgets;
use phpQuery;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

if(!class_exists('WooCommerce')) {
	return;
}

/**
 * WooCommerce Rating Filter widget.
 */
function filter_woocommerce_rating_filter($widget_output, $props, $sidebar_id, $widget_type) {
	$dom = phpQuery::newDocumentHTML($widget_output);

	pq('ul > li > a')->addClass('ui header');

	return $dom->getDocument();
}
add_filter('chap_widget_woocommerce_rating_filter', __NAMESPACE__ . '\\filter_woocommerce_rating_filter', 10, 4);

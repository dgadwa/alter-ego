<?php

namespace Chap\Widgets;
use phpQuery;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

/**
 * Text widget with [instagram-feed] shortcode.
 *
 * @see https://wordpress.org/plugins/instagram-feed/
 */
function filter_instagram_feed($widget_output, $props, $sidebar_id) {

	if(!strpos($widget_output, 'id="sb_instagram"')) {
		return $widget_output;
	}

	$dom = phpQuery::newDocumentHTML($widget_output);

	/**
	 * Add SUI classes to "Load more" button.
	 */
	pq('.sbi_load_btn')->addClass('ui button');
	/**
	 * Add SUI classes to "Follow on Instagram" button.
	 */
	pq('.sbi_follow_btn > a')->addClass('ui labeled instagram icon button');
	/**
	 * Replace Font Awesome classes with SUI icon classes for Instagram icon.
	 */
	pq('.sbi_follow_btn > a > i')->removeClass('fa')->removeClass('fa-instagram')->addClass('instagram icon');

	return $dom->getDocument();

}
add_filter('chap_widget_text', __NAMESPACE__ . '\\filter_instagram_feed', 10, 3);

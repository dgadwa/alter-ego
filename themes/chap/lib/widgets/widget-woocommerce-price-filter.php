<?php

namespace Chap\Widgets;
use phpQuery;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

if(!class_exists('WooCommerce')) {
	return;
}

/**
 * WooCommerce Price filter widget.
 */
function filter_woocommerce_price_filter($widget_output, $props) {
	$dom = phpQuery::newDocumentHTML($widget_output);

	pq('button')
	->removeClass('button')
	->addClass('ui tiny primary labeled icon button')
	->append('<i class="filter icon"></i>');

	pq('.price_label')
	->addClass('ui adjacent to tiny button');

	return $dom->getDocument();
}
add_filter('chap_widget_woocommerce_price_filter', __NAMESPACE__ . '\\filter_woocommerce_price_filter', 10, 2);

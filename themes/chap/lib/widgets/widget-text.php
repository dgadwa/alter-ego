<?php

namespace Chap\Widgets;
use phpQuery;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

/**
 * Text widget filter.
 */
// function filter_text($widget_output, $props, $sidebar_id) {
// 	return $widget_output;
// }
// add_filter('chap_widget_text', __NAMESPACE__ . '\\filter_text', 9, 3);

/**
 * Add checkbox option to disable content filters.
 */
add_filter('in_widget_form', function($widget, $return, $instance){
	if(version_compare($GLOBALS['wp_version'], '4.8', '<')) {
		return;
	}

	if($widget->id_base === 'text') {
		$field_id = $widget->get_field_id('chap_disable_filters');
		$field_name = $widget->get_field_name('chap_disable_filters');
		$field_value = isset($instance['chap_disable_filters']) ? $instance['chap_disable_filters'] : '';
		?><p>
			<input type="checkbox" class="checkbox" id="<?php echo esc_attr($field_id); ?>" name="<?php echo esc_attr($field_name); ?>" <?php checked(1, $field_value); ?> />
			<label for="<?php echo esc_attr($field_id); ?>"><?php esc_html_e('Disable content filters', 'chap') . $field_value; ?></label>
		</p><?php
	}
}, 10, 3);

/**
 * Save the content filters checkbox option.
 */
add_filter('widget_update_callback', function($instance, $new_instance){
	if(version_compare($GLOBALS['wp_version'], '4.8', '<')) {
		return $instance;
	}

	if(isset($instance['text']) && isset($instance['filter'])) {
		if(isset($new_instance['chap_disable_filters'])) {
			$instance['chap_disable_filters'] = 1;
		} else {
			$instance['chap_disable_filters'] = 0;
		}
	}

	return $instance;
}, 10, 2);

/**
 * Store the initial text, before any filters are applied.
 */
add_filter('widget_text_content', function($text, $instance){
	if($instance['chap_disable_filters']) {
		$GLOBALS['chap']['text_widget_content'] = $text;
	}
	return $text;
}, 1, 2);

/**
 * Restore the initial text.
 */
add_filter('widget_text_content', function($text, $instance){
	if($instance['chap_disable_filters'] && isset($GLOBALS['chap']['text_widget_content'])) {
		if(get_option('csc-shortcodes-in-widgets')) {
			$text = do_shortcode($GLOBALS['chap']['text_widget_content']);
		} else {
			$text = $GLOBALS['chap']['text_widget_content'];
		}
		unset($GLOBALS['chap']['text_widget_content']);
	}
	return $text;
}, 999, 2);

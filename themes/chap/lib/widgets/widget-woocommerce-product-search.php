<?php

namespace Chap\Widgets;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

if(!class_exists('WooCommerce')) {
	return;
}

// Using filter from ./widget-search.php
add_filter('chap_widget_woocommerce_product_search', __NAMESPACE__ . '\\filter_search', 10, 3);

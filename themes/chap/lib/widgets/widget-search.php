<?php

namespace Chap\Widgets;
use Chap\Options;
use phpQuery;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

/**
 * Search form widget.
 */
function filter_search($widget_output, $props, $sidebar_id) {

	$is_menu = ($sidebar_id === 'sidebar-menu');
	$is_header = ($sidebar_id === 'sidebar-header');

	if(!$is_menu && !$is_header) {
		return $widget_output;
	}

	$dom = phpQuery::newDocumentHTML($widget_output);

	pq('form')->removeClass('fluid')
			  ->removeClass('left')
			  ->removeClass('action')
			  ->removeClass('input');

	$inverted = Options\get('main_menu_inverted');
	if($inverted) {
		pq('form')->addClass('inverted');
	}

	$container = Options\get('main_menu_container');
	if($is_menu && $container) {
		pq('form')->addClass('transparent');
	}

	if($is_header) {
		pq('form')->addClass('marginless');
	}

	$size = Options\get('main_menu_size');
	if(!in_array($size, ['mini', 'tiny', 'small'])) {
		$size = 'medium'; // Cap size, it gets too big in larger menus.
	}

	pq('form')->addClass($size)
	->addClass('input');

	pq('form > button')->remove();

	/**
	 * Remove link class, because it's not a link item, but an input.
	 */
	if($is_menu) {
		pq('.menu-widget')->removeClass('link');
	}

	return $dom->getDocument();
}
add_filter('chap_widget_search', __NAMESPACE__ . '\\filter_search', 10, 3);

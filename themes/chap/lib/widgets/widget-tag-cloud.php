<?php

namespace Chap\Widgets;
use Chap\Options;
use phpQuery;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

/**
 * Tag cloud widget.
 */
function filter_tag_cloud($widget_output, $props) {
	$dom = phpQuery::newDocumentHTML($widget_output);

	pq('.tagcloud')->addClass('ui');
	pq('.tagcloud a')->addClass('ui label');

	// Size from theme options.
	$size = Options\get('widget_tag_cloud_tag_size');
	if($size && $size !== 'auto') {
		pq('.tagcloud')->addClass($size);
		pq('.tagcloud a')->removeAttr('style');
	}

	// Color from theme options.
	$color = Options\get('widget_tag_cloud_tag_color');
	if($color && $color != 'default') {
		pq('.tagcloud')->addClass($color);
	}

	pq('.tagcloud')->addClass('labels');

	return $dom->getDocument();
}
add_filter('chap_widget_tag_cloud', __NAMESPACE__ . '\\filter_tag_cloud', 10, 2);

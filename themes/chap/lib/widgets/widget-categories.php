<?php

namespace Chap\Widgets;
use phpQuery;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

/**
 * Categories widget.
 * Uses custom walker, only need to alter the main container.
 */
function filter_categories($widget_output, $props, $sidebar_id) {
	/**
	 * No modifications if is displayed as dropdown.
	 * Add JavaScript to initialize the dropdown.
	 */
	if(strpos($widget_output, '</select>') !== false) {
		global $chap_theme;
		if(!isset($chap_theme['category_widget_js'])) {
			$home = home_url();
			$js = <<<JS
document.addEventListener('chap_ready', function(){
	var chap_init_category_dropdowns = function(){
		var $ = jQuery;
		$('.widget select[name="cat"]').each(function(){
			var chap_dropdown = $($(this).clone().wrap('<p>').parent().html());
			$(this).replaceWith(chap_dropdown);
			chap_dropdown.find('option[value="-1"]').val('');
			chap_dropdown.addClass('search').dropdown({
				forceSelection: false,
				onChange: function(value) {
					if(!value) {
						return;
					}
					location.href = "{$home}/?cat=" + value;
				},
			}).addClass('fluid');
		});
	};
	wp.hooks.addAction('chap_init', chap_init_category_dropdowns);
});
JS;
			$chap_theme['category_widget_js'] = wp_add_inline_script('chap/js', $js);
		}
		return $widget_output;
	}

	$dom = phpQuery::newDocumentHTML($widget_output);

	pq('ul')->after('<div class="ui fluid ' . $props['direction'] . ' ' . $props['inverted'] . ' ' . $props['container'] . '" role="list"></div>');

	foreach(pq('ul > .item') as $item) {
		pq('.ui.list')->append($item);
	}
	pq('.ui.list div.item > div.list')->attr('role', 'list');
	pq('.ui.list div.item')->attr('role', 'listitem');

	// Remove child items in footer.
	if($sidebar_id === 'sidebar-footer') {
		pq('.item > .list')->remove();
	}

	pq('ul')->remove();

	return $dom->getDocument();

}
add_filter('chap_widget_categories', __NAMESPACE__ . '\\filter_categories', 10, 3);

<?php

namespace Chap\Widgets;
use phpQuery;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

/**
 * Custom HTML widget filter.
 */
function filter_custom_html($widget_output, $props, $sidebar_id) {
	/**
	 * Custom HTML widgets in menu should not be .link.item's by default.
	 * The Text widget can be used instead to get a .link.item.
	 */
	if($sidebar_id === 'sidebar-menu') {
		$dom = phpQuery::newDocumentHTML($widget_output);
		pq('.menu-widget')->removeClass('link');
		return $dom->getDocument();
	}

	return $widget_output;
}
add_filter('chap_widget_custom_html', __NAMESPACE__ . '\\filter_custom_html', 9, 3);

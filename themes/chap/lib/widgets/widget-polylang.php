<?php

namespace Chap\Widgets;
use phpQuery;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

/**
 * Polylang language selector widget.
 */
function filter_polylang($widget_output, $props, $sidebar_id) {

	/**
	 * Polylang widget must be displayed as a dropdown.
	 *
	 */
	if(strpos($widget_output, '</select>') !== false) {

		$dom = phpQuery::newDocumentHTML($widget_output);

		pq('.widget_polylang select')->addClass('chap-polylang invisible');

		if($sidebar_id === 'sidebar-header') {
			pq('.widget_polylang select')->addClass('ui top right pointing dropdown icon button');
		}

		if($sidebar_id === 'sidebar-menu') {
			pq('.menu-widget')->removeClass('link')->removeClass('item');
			pq('.widget_polylang select')->addClass('top right dropdown item');
		}

		if($sidebar_id === 'sidebar-primary' || $sidebar_id === 'sidebar-secondary') {
			pq('.widget_polylang select')->addClass('ui floating dropdown icon button');
		}

		if($sidebar_id === 'sidebar-footer') {
			pq('.widget_polylang select')->addClass('ui floating dropdown icon button');
		}

		wp_enqueue_style('chap/polylang', get_template_directory_uri() . '/lib/widgets/widget-polylang.css', ['chap']);
		wp_enqueue_script('chap/polylang', get_template_directory_uri() . '/lib/widgets/widget-polylang.js', ['chap/js'], null, true);

		return $dom->getDocument();
	}

	return $widget_output;

}
add_filter('chap_widget_polylang', __NAMESPACE__ . '\\filter_polylang', 10, 3);

<?php

namespace Chap\Widgets;
use phpQuery;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

if(!class_exists('WooCommerce')) {
	return;
}

/**
 * WooCommerce Layered Nav Filters widget.
 */
function filter_woocommerce_layered_nav_filters($widget_output, $props) {
	$dom = phpQuery::newDocumentHTML($widget_output);

	pq('li a')->addClass('ui label')->append('<i class="close icon"></i>');
	pq('ul')->after('<div class="chap_layered_nav_filters"></div>');
	foreach(pq('li > a') as $a) {
		pq('.chap_layered_nav_filters')->append($a);
	}
	pq('ul')->remove();

	return $dom->getDocument();
}
add_filter('chap_widget_woocommerce_layered_nav_filters', __NAMESPACE__ . '\\filter_woocommerce_layered_nav_filters', 10, 2);

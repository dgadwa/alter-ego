<?php

namespace Chap\Widgets;
use phpQuery;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

if(!class_exists('WooCommerce')) {
	return;
}

/**
 * WooCommerce monster widget.
 */
function filter_wc_monster($widget_output, $props, $sidebar_id, $widget_type) {

	$dom = phpQuery::newDocumentHTML($widget_output);

	$filters = [
		'widget_shopping_cart' => 'chap_widget_woocommerce_widget_cart',
		'widget_product_search' => 'chap_widget_woocommerce_product_search',
		'widget_product_categories' => 'chap_widget_woocommerce_product_categories',
		'widget_product_tag_cloud' => 'chap_widget_woocommerce_product_tag_cloud',
		'widget_products' => 'chap_widget_woocommerce_products',
		'widget_recent_reviews' => 'chap_widget_woocommerce_recent_reviews',
		'widget_top_rated_products' => 'chap_widget_woocommerce_top_rated_products',
		'widget_layered_nav' => 'chap_widget_woocommerce_layered_nav',
		'widget_layered_nav_filters' => 'chap_widget_woocommerce_layered_nav_filters',
	];

	foreach(pq('.widget') as $section) {

		$section = pq($section);
		$html = $section->html();

		foreach($filters as $class => $filter) {
			if($section->hasClass($class)) {
				$html = apply_filters($filter, $html, $props, $sidebar_id, $widget_type);
				break;
			}
		}

		$section->html($html);

	}

	return $dom->getDocument();

}
add_filter('chap_widget_wc-monster', __NAMESPACE__ . '\\filter_wc_monster', 10, 4);

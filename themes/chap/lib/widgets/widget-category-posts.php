<?php

namespace Chap\Widgets;
use phpQuery;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

/**
 * Category posts widget
 * @link https://wordpress.org/plugins/category-posts/
 */
function filter_category_posts($widget_output, $props) {
	$dom = phpQuery::newDocumentHTML($widget_output);

	pq('ul')->addClass('ui fluid ' . $props['direction'] . ' ' . $props['inverted'] . ' ' . $props['container']);
	pq('li')->removeClass('cat-post-item')->addClass('item');
	pq('li > a.post-title')->removeClass('cat-post-title')->addClass('header');
	pq('li > p.post-date')->removeClass('cat-post-date')->addClass('description');
	pq('li > span.post-date')->addClass('description');

	return $dom->getDocument();
}
add_filter('chap_widget_category-posts', __NAMESPACE__ . '\\filter_category_posts', 10, 2);

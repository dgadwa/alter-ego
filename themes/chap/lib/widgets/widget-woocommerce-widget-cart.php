<?php

namespace Chap\Widgets;
use Chap\Options;
use phpQuery;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

if(!class_exists('WooCommerce')) {
	return;
}

/**
 * WooCommerce Cart widget.
 */
function filter_woocommerce_widget_cart($widget_output, $props, $sidebar_id) {

	$is_menu   = ($sidebar_id === 'sidebar-menu');
	$is_header = ($sidebar_id === 'sidebar-header');

	if(!$is_menu && !$is_header) {
		return $widget_output;
	}

	$dom = phpQuery::newDocumentHTML($widget_output);

	/**
	 * Wrap the cart in a modal.
	 */
	$html = '';
	$cart_item_count = WC()->cart->get_cart_contents_count();

	/**
	 * Render cart button in header widget area.
	 */
	if($is_header) {
		$label = is_rtl() ? 'right labeled' : 'labeled';
		$html  = '<div class="ui primary cart_widget ' . $label . ' icon button">';
		$html .= '<i class="cart icon"></i>';
		$html .= '<span class="chap-cart-item-count">' . $cart_item_count . '</span>';
		$html .= '</div>';
		/**
		 * Wrap in div to mimic buttons in textwidgets.
		 */
		$html = '<div>' . $html . '</div>';
	}

	/**
	 * Render cart icon with text in main menu widget area.
	 */
	if($is_menu) {
		$count_class = $cart_item_count > 0 ? '' : ' invisible';
		$html  = '<i class="cart icon"></i> ' . esc_html__('Cart', 'chap');
		$html .= '<div class="ui tiny red floating chap-cart-item-count label' . $count_class . '">';
		$html .= $cart_item_count;
		$html .= '</div>';
	}

	/**
	 * Modal actions.
	 */
	$actions  = '<div class="actions">';
	$actions .= '<a href="' . esc_url(wc_get_cart_url()) . '" class="ui primary labeled icon button"><i class="cart icon"></i>' . esc_html__('Cart', 'chap') . '</a>';
	$actions .= '<a href="' . esc_url(wc_get_checkout_url()) . '" class="ui positive right labeled icon button checkout"><i class="arrow right icon"></i>' . esc_html__('Checkout', 'chap') . '</a>';
	$actions .= '</div>';

	/**
	 * Create modal.
	 */
	pq('.widget_shopping_cart_content')->before($html);
	pq('.widget_shopping_cart_content')->wrap('<div class="content"></div>');
	pq('.content')->wrap('<div class="ui cart_widget small modal hidden"></div>');
	pq('.cart_widget.modal')->prepend('<i class="close icon"></i><div class="header">' . esc_html__('Cart', 'chap') . '</div>');
	pq('.cart_widget.modal')->append($actions);

	return $dom->getDocument();
}
add_filter('chap_widget_woocommerce_widget_cart', __NAMESPACE__ . '\\filter_woocommerce_widget_cart', 10, 3);

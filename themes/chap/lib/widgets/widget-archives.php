<?php

namespace Chap\Widgets;
use phpQuery;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

/**
 * Archives widget.
 */
function filter_archives($widget_output, $props) {
	/**
	 * Add JavaScript to initialize the dropdown.
	 */
	if(strpos($widget_output, '</select>') !== false) {
		global $chap_theme;
		if(!isset($chap_theme['archive_widget_js'])) {
			$js = <<<JS
document.addEventListener('chap_ready', function(){
	var chap_init_archive_dropdowns = function(){
		var $ = jQuery;
		$('.widget select[name="archive-dropdown"]').each(function(){
			$(this).addClass('search').dropdown({forceSelection: false}).addClass('fluid');
		});
	};
	wp.hooks.addAction('chap_init', chap_init_archive_dropdowns);
});
JS;
			$chap_theme['archive_widget_js'] = wp_add_inline_script('chap/js', $js);
		}
		return $widget_output;
	}

	$dom = phpQuery::newDocumentHTML($widget_output);

	pq('ul')->addClass('ui fluid ' . $props['direction'] . ' ' . $props['inverted'] . ' ' . $props['container']);
	pq('li')->addClass('item');
	pq('li')->wrapInner('<div class="header"></div>');

	return $dom->getDocument();
}
add_filter('chap_widget_archives', __NAMESPACE__ . '\\filter_archives', 10, 2);

<?php

namespace Chap\Widgets;
use phpQuery;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

/**
 * Recent comments widget.
 */
function filter_recent_comments($widget_output, $props) {
	$dom = phpQuery::newDocumentHTML($widget_output);

	pq('ul#recentcomments')->addClass('ui fluid ' . $props['direction'] . ' ' . $props['inverted'] . ' ' . $props['container']);
	pq('li.recentcomments')->addClass('item')->wrapInner('<div class="header"></div>');

	return $dom->getDocument();
}
add_filter('chap_widget_recent-comments', __NAMESPACE__ . '\\filter_recent_comments', 10, 2);

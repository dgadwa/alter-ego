<?php

namespace Chap\Widgets;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

// Using filter from ./widget-recent-posts.php
add_filter('chap_widget_meta', __NAMESPACE__ . '\\filter_recent_posts', 10, 2);

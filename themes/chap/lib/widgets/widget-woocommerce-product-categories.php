<?php

namespace Chap\Widgets;
use phpQuery;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

if(!class_exists('WooCommerce')) {
	return;
}

/**
 * WooCommerce Product Categories widget.
 * Uses custom walker, only need to alter the main container.
 */
function filter_woocommerce_product_categories($widget_output, $props, $sidebar_id) {
	/**
	 * Add JavaScript to initialize the dropdown.
	 */
	if(strpos($widget_output, '</select>') !== false) {
		global $chap_theme;
		if(!isset($chap_theme['wc_product_cat_widget_js'])) {
			$js = <<<JS
document.addEventListener('chap_ready', function(){
	var chap_init_wc_product_cat_dropdowns = function(){
		var $ = jQuery;
		$('.widget select[name="product_cat"]').each(function(){
			$(this).addClass('search').dropdown({forceSelection: false}).addClass('fluid');
			$(this).addClass('dropdown_product_cat');
		});
	};
	wp.hooks.addAction('chap_init', chap_init_wc_product_cat_dropdowns);
});
JS;
			$chap_theme['wc_product_cat_widget_js'] = wp_add_inline_script('chap/js', $js);
		}
		return $widget_output;
	}

	if($sidebar_id === 'sidebar-header' || $sidebar_id === 'sidebar-menu') {
		// Not supported.
		return '';
	}

	$selector = '.list';

	if($sidebar_id === 'sidebar-primary' || $sidebar_id === 'sidebar-secondary') {
		$props['container'] = 'menu';
		$selector = '.menu';
	}

	$dom = phpQuery::newDocumentHTML($widget_output);

	$role = $selector === '.menu' ? 'menu' : ($selector === '.list' ? 'list' : '');
	$role_att = $role ? ' role="' . $role . '"' : '';

	pq('ul')->after('<div class="ui fluid ' . $props['direction'] . ' ' . $props['inverted'] . ' ' . $props['container'] . '"' . $role_att . '></div>');

	foreach(pq('ul > .item') as $item) {
		pq('.ui' . $selector)->append($item);
	}

	// Remove child items in footer.
	if($sidebar_id === 'sidebar-footer') {

		foreach(pq('.ui.tiny.header') as $parent) {
			pq('.ui' . $selector)->prepend($parent);
		}

		pq('.ui.tiny.header')
		->removeClass('ui')
		->removeClass('tiny')
		->removeClass('header')
		->addClass('item');

		pq('div.item')->remove();

	}

	pq('ul')->remove();

	if($role === 'menu') {
		pq('.ui' . $selector . ' .item')->attr('role', 'menuitem');
		pq('.ui' . $selector . ' .item .menu')->attr('role', 'menu');
	} elseif($role === 'list') {
		pq('.ui' . $selector . ' .item')->attr('role', 'listitem');
	}

	return $dom->getDocument();

}
add_filter('chap_widget_woocommerce_product_categories', __NAMESPACE__ . '\\filter_woocommerce_product_categories', 10, 3);

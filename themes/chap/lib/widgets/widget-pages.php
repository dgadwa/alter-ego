<?php

namespace Chap\Widgets;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

// Using filter from ./widget-categories.php
add_filter('chap_widget_pages', __NAMESPACE__ . '\\filter_categories', 10, 4);

<?php

namespace Chap\Widgets;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

if(!class_exists('WooCommerce')) {
	return;
}

// Using filter from ./widget-tag-cloud.php
add_filter('chap_widget_woocommerce_product_tag_cloud', __NAMESPACE__ . '\\filter_tag_cloud', 10, 2);

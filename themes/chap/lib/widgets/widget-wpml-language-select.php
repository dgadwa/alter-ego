<?php

namespace Chap\Widgets;
use phpQuery;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

if(!class_exists('SitePressLanguageSwitcher')) {
	return;
}

/**
 * WPML language select widget.
 */
function filter_icl_lang_sel_widget($widget_output, $props, $sidebar_id) {
	$dom = phpQuery::newDocumentHTML($widget_output);

	/**
	 * Create a selection dropdown.
	 */
	pq('#lang_sel')->after('<div class="ui fluid chap_wpml selection dropdown"></div>');
	pq('.chap_wpml')->append('<div class="current_language"></div>');
	pq('.chap_wpml')->append('<i class="dropdown icon"></i>');
	pq('.chap_wpml')->append('<div class="menu"></div>');

	/**
	 * Add the first link as the selected language.
	 */
	pq('.chap_wpml .current_language')->html(pq('#lang_sel a:first')->html());

	/**
	 * Move the dropdown items into menu.
	 */
	foreach(pq('#lang_sel > ul > li > ul > li > a') as $item) {
		pq('.chap_wpml .menu')->append($item);
	}

	/**
	 * Add item class to menu items.
	 */
	pq('.chap_wpml .menu > a')->addClass('item');

	/**
	 * Don't use fluid when language select
	 * is being output with an action.
	 */
	if($sidebar_id === 'action') {
		pq('.chap_wpml')->removeClass('fluid');
	}

	/**
	 * In menu widget area make a dropdown item.
	 */
	if($sidebar_id === 'sidebar-menu') {
		pq('.menu-widget')->removeClass('link')
						  ->removeClass('item')
						  ->addClass('ui')
						  ->addClass('dropdown')
						  ->addClass('item')
						  ->html(pq('.chap_wpml')->html());
	} else {
		/**
		 * Otherwise clean up original widget.
		 */
		pq('#lang_sel')->remove();

		/**
		 * And add JavaScript.
		 */
		$js = <<<JS
jQuery(document).ready(function(){
	jQuery('.chap_wpml').dropdown();
});
JS;
		wp_add_inline_script('chap/js', $js);
	}

	return $dom->getDocument();
}
add_filter('chap_widget_icl_lang_sel_widget', __NAMESPACE__ . '\\filter_icl_lang_sel_widget', 10, 3);

/**
 * Use widget filter to override WPML language selector action.
 */
function chap_wpml_add_language_selector() {
	ob_start();
	do_action('wpml_add_language_selector');
	$language_selector = ob_get_clean();
	echo filter_icl_lang_sel_widget($language_selector, [], 'action');
}
add_action('chap_wpml_add_language_selector', __NAMESPACE__ . '\\chap_wpml_add_language_selector');

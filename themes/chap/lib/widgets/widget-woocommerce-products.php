<?php

namespace Chap\Widgets;
use phpQuery;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

if(!class_exists('WooCommerce')) {
	return;
}

/**
 * WooCommerce Products widget.
 */
function filter_woocommerce_products($widget_output, $props, $sidebar_id, $widget_type) {
	$classes = 'product_list_widget';
	if($widget_type === 'woocommerce_top_rated_products') {
		$classes = 'product_rating ' . $classes;
	}

	$dom = phpQuery::newDocumentHTML($widget_output);

	pq('ul')->after('<div class="ui ' . $props['inverted'] . ' ' . $classes . ' list"></div>');
	foreach(pq('ul > .item') as $item) {
		pq('.ui.list')->append($item);
	}
	pq('ul')->remove();

	return $dom->getDocument();
}
add_filter('chap_widget_woocommerce_products', __NAMESPACE__ . '\\filter_woocommerce_products', 10, 4);

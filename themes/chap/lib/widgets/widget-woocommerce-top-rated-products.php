<?php

namespace Chap\Widgets;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

if(!class_exists('WooCommerce')) {
	return;
}

// Using filter from ./widget-woocommerce-products.php
add_filter('chap_widget_woocommerce_top_rated_products', __NAMESPACE__ . '\\filter_woocommerce_products', 10, 4);

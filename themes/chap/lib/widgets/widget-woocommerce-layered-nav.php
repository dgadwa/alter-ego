<?php

namespace Chap\Widgets;
use phpQuery;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

if(!class_exists('WooCommerce')) {
	return;
}

/**
 * WooCommerce Layered Nav widget.
 */
function filter_woocommerce_layered_nav($widget_output, $props) {
	/**
	 * Add JavaScript to initialize the dropdown.
	 */
	if(strpos($widget_output, '</select>') !== false) {
		if(strpos($widget_output, 'type="submit"') !== false) {
			$widget_output = str_replace('multiple="multiple"', '', $widget_output);
		} else {
			$widget_output = str_replace('<select ', '<select multiple="multiple" ', $widget_output);
		}
		global $chap_theme;
		if(!isset($chap_theme['wc_layered_nav_dropdown'])) {
			global $wp;
			$shop_url = rtrim(get_permalink(wc_get_page_id('shop')), '/');
			$selected = isset($_GET['filter_color']) ? esc_js(wp_unslash($_GET['filter_color'])) : '';
			$js = <<<JS
document.addEventListener('chap_ready', function(){
	var chap_init_wc_layered_nav_dropdown = function(){
		var $ = jQuery;
		var selected = '{$selected}';
		$('.widget.woocommerce.widget_layered_nav').each(function(){
			var select = $(this).find('select');
			var multiple = !$(this).find('button[type="submit"]').length;
			var settings = {
				forceSelection: false,
				onChange: function(value) {
					if(!value) {
						return;
					}
					$(this).dropdown('hide');
					location.href = value.length ? '{$shop_url}?filter_color=' + value : '{$shop_url}';
				},
			};
			select.addClass('ui fluid select');
			if(multiple) {
				select.addClass('multiple');
			} else {
				$(this).find('button[type="submit"]').hide();
				settings.placeholder = '';
			}
			select.dropdown(settings);
		});
	};
	wp.hooks.addAction('chap_init', chap_init_wc_layered_nav_dropdown);
});
JS;
			$chap_theme['wc_layered_nav_dropdown'] = wp_add_inline_script('chap/js', $js);
		}
		return $widget_output;
	}

	$dom = phpQuery::newDocumentHTML($widget_output);

	// Add Semantic UI classes.
	pq('li a')->addClass('item');
	pq('li.chosen a')->addClass('active');
	pq('span.count')->addClass('ui horizontal label');

	// Create a replacement container for ul.
	pq('ul')->after('<div class="chap_layered_nav"></div>');
	pq('.chap_layered_nav')->addClass('ui fluid vertical ' . $props['inverted'] . ' menu');

	// Move the count into the link element.
	foreach(pq('li > span') as $span) {
		// Remove parenthesis around count.
		$span->textContent = str_replace(['(', ')'], '', $span->textContent);

		pq($span)->prev()->append($span);
	}

	// Move the link elements to the replacement container.
	foreach(pq('li > a') as $a) {
		pq('.chap_layered_nav')->append($a);
	}

	// Get rid of the old container.
	pq('ul')->remove();

	return $dom->getDocument();
}
add_filter('chap_widget_woocommerce_layered_nav', __NAMESPACE__ . '\\filter_woocommerce_layered_nav', 10, 2);

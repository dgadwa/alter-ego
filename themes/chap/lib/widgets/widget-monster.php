<?php

namespace Chap\Widgets;
use phpQuery;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

/**
 * Monster widget.
 */
function filter_monster($widget_output, $props, $sidebar_id, $widget_type) {

	$dom = phpQuery::newDocumentHTML($widget_output);

	$filters = [
		'widget_archive' => 'chap_widget_archives',
		'widget_calendar' => 'chap_widget_calendar',
		'widget_categories' => 'chap_widget_categories',
		'widget_pages' => 'chap_widget_pages',
		'widget_meta' => 'chap_widget_meta',
		'widget_recent_comments' => 'chap_widget_recent-comments',
		'widget_recent_entries' => 'chap_widget_recent-posts',
		'widget_rss' => 'chap_widget_rss',
		'widget_search' => 'chap_widget_search',
		'widget_tag_cloud' => 'chap_widget_tag_cloud',
		'widget_nav_menu' => 'chap_widget_nav_menu',
	];

	foreach(pq('.widget') as $section) {

		$section = pq($section);
		$html = $section->html();

		foreach($filters as $class => $filter) {
			if($section->hasClass($class)) {
				$html = apply_filters($filter, $html, $props, $sidebar_id, $widget_type);
				break;
			}
		}

		$section->html($html);

	}

	return $dom->getDocument();

}
add_filter('chap_widget_monster', __NAMESPACE__ . '\\filter_monster', 10, 4);

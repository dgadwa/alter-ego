<?php

namespace Chap\Widgets;
use phpQuery;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

/**
 * Calendar widget.
 */
function filter_calendar($widget_output, $props) {
	$dom = phpQuery::newDocumentHTML($widget_output);

	pq('caption')->addClass('ui top attached ' . $props['inverted'] . ' header');
	pq('table')->addClass('ui small compact bottom attached celled ' . $props['inverted'] . ' unstackable table');
	pq('td.pad')->addClass('borderless');
	pq('#next')->addClass('borderless right aligned');

	// Fix W3C error.
	pq('tfoot')->insertAfter('tbody');

	return $dom->getDocument();
}
add_filter('chap_widget_calendar', __NAMESPACE__ . '\\filter_calendar', 10, 2);

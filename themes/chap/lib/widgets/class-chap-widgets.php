<?php

namespace Chap\Widgets;
use Chap\Assets;
use Chap\Helpers;
use Chap\Options;
use phpQuery;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

/**
 * Chap Widgets class which uses the filter_widgets plugin
 * to override widgets HTML to better suit Semantic UI.
 *
 * @class   Chap_Widgets
 * @version 1.0.2
 * @author  websevendev <websevendev@gmail.com>
 */
class Chap_Widgets {

	/**
	 * Footer widget rows.
	 *
	 * @var array
	 */
	public static $footer_rows = [];

	/**
	 * Init Chap Widgets class.
	 *
	 * @since 1.0.0
	 */
	public static function init() {

		/**
		 * Load files.
		 */
		add_action('after_setup_theme', [__CLASS__, 'load']);

		/**
		 * Filter widgets output.
		 */
		add_filter('widget_output', [__CLASS__, 'filter_widgets'], 10, 4);

		/**
		 * Load footer configuration.
		 */
		add_action('wp', [__CLASS__, 'load_footer_config']);

	}

	/**
	 * Load all widget filters.
	 *
	 * @since 1.0.1
	 */
	public static function load() {
		foreach(Assets\theme_files('lib/widgets', 'widget-*.php') as $file) {
			include_once $file;
		}
	}

	/**
	 * Load footer configuration.
	 *
	 * @since 1.0.2
	 */
	public static function load_footer_config() {
		$rows_string = Options\get('footer_widget_rows');
		if($rows_string) {
			$rows = explode(',', $rows_string);
			$rows = array_map('trim', $rows);
			$rows = array_map('intval', $rows);
			if(count($rows) > 0) {
				self::$footer_rows = $rows;
			}
		}
	}

	/**
	 * Filter widgets output.
	 *
	 * @since  1.0.0
	 * @param  string $widget_output Widget output.
	 * @param  string $widget_type   Widget type.
	 * @param  string $widget_id     Widget ID.
	 * @param  string $sidebar_id    Sidebar ID.
	 * @return string
	 */
	public static function filter_widgets($widget_output, $widget_type, $widget_id, $sidebar_id) {

		$props = self::get_widget_props($sidebar_id);

		$widget_output = self::global_filter($widget_output, $props, $sidebar_id, $widget_type);

		/**
		 * Disable phpQuery warnings.
		 * @see http://stackoverflow.com/questions/6090667/php-domdocument-errors-warnings-on-html5-tags
		 */
		libxml_use_internal_errors(true);
		$widget_output = apply_filters('chap_widget_' . $widget_type, $widget_output, $props, $sidebar_id, $widget_type);
		libxml_clear_errors();

		$widget_output = self::global_filter_after($widget_output, $props, $sidebar_id, $widget_type);

		// var_dump($widget_type);
		return $widget_output;

	}

	/**
	 * Return widgets properties based on
	 * which widget area they reside in.
	 *
	 * @param  string $sidebar_id
	 * @return array
	 */
	public static function get_widget_props($sidebar_id) {

		/**
		 * Default props.
		 */
		$props = [
			'inverted'  => 'default', // Light or dark color scheme.
			'container' => 'bulletless list', // Widget container.
			'direction' => 'vertical', // Content direction.
		];

		/**
		 * Assign props based on Widget area location.
		 */
		if($sidebar_id === 'sidebar-header' || $sidebar_id == 'sidebar-menu') {

			$props['inverted']  = Options\get('header_inverted') ? 'inverted' : 'default';
			$props['container'] = 'divided list';
			$props['direction'] = 'horizontal';

		} elseif($sidebar_id === 'sidebar-footer') {

			$props['inverted']  = Options\get('footer_inverted') ? 'inverted' : 'default';
			$props['container'] = 'bulletless link list';

		}

		return apply_filters('chap_widget_props', $props);

	}

	/**
	 * Changes that are applied to all widgets.
	 *
	 * @param  string $widget_output
	 * @param  array $props Properties based on widget area.
	 * @return string
	 */
	public static function global_filter($widget_output, $props, $sidebar_id, $widget_type) {
		/**
		 * For widgets without headers add an invisible
		 * header with widget's id, for W3C compliance.
		 */
		if(strpos($widget_output, '<section') !== false) {

			$header_tag = '';

			if($sidebar_id === 'sidebar-footer') {
				$header_tag = 'h4';
				if(strpos($widget_output, '<' . $header_tag) !== false) {
					return $widget_output;
				}
			} else {
				$header_tag = 'h2';
				if(strpos($widget_output, '<' . $header_tag) !== false) {
					return $widget_output;
				}
			}

			if(!empty($header_tag)) {
				$dom = phpQuery::newDocumentHTML($widget_output);
				pq('section')->prepend('<' . $header_tag . ' class="invisible screen-reader-text">' . $widget_type . '</' . $header_tag . '>');
				return $dom->getDocument();
			}

		}

		return $widget_output;
	}

	/**
	 * Changes that are applied to all widgets after all DOM manipulation is done.
	 *
	 * @since 1.0.2
	 * @param  string $widget_output
	 * @param  array $props Properties based on widget area.
	 * @return string
	 */
	public static function global_filter_after($widget_output, $props, $sidebar_id, $widget_type) {
		/**
		 * Apply custom column widths for footer widgets.
		 *
		 * @since 1.0.2
		 */
		if($sidebar_id === 'sidebar-footer') {
			global $chap_theme;
			$current_index = $chap_theme['footer_widget'];

			if(in_array($current_index, self::$footer_rows)) {
				$widget_output = '</div><div class="row">' . $widget_output;
			}

			$chap_theme['footer_widget']++;
		}

		return $widget_output;
	}

}

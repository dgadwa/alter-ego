<?php

namespace Chap\Widgets;
use Chap\Helpers;
use phpQuery;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

/**
 * RSS widget.
 */
function filter_rss($widget_output, $props) {
	$dom = phpQuery::newDocumentHTML($widget_output);

	// Feed header
	pq('a.rsswidget:first')->replaceWith('<i class="rss icon"></i>');
	pq('a.rsswidget')->addClass('content');

	// Feed content
	pq('ul')->addClass('ui bulletless ' . $props['inverted'] . ' feed list');
	pq('ul > li > a.rsswidget')->removeClass('content');
	pq('ul > li')->addClass('event')->wrapInner('<div class="content"></div>');
	pq('.rssSummary')->addClass('summary');
	pq('.rss-date')->addClass('date');
	pq('cite')->addClass('user');

	return $dom->getDocument();
}
add_filter('chap_widget_rss', __NAMESPACE__ . '\\filter_rss', 10, 2);

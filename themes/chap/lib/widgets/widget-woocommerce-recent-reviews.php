<?php

namespace Chap\Widgets;
use phpQuery;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

if(!class_exists('WooCommerce')) {
	return;
}

/**
 * WooCommerce Recent Reviews widget.
 */
function filter_woocommerce_recent_reviews($widget_output, $props, $sidebar_id, $widget_type) {
	$dom = phpQuery::newDocumentHTML($widget_output);

	pq('ul')->after('<div class="ui ' . $props['inverted'] . ' product_rating product_list_widget list"></div>');
	pq('li')->wrapInner('<div class="item"></div>');
	pq('li .star-rating')->wrap('<div class="right floated content"></div>');
	// .attachment-shop_thumbnail deprecated since WC ~3.3.0 (?)
	pq('.attachment-shop_thumbnail, .attachment-woocommerce_thumbnail')->addClass('ui avatar left floated marginless image');
	pq('img.woocommerce-placeholder')->addClass('ui avatar left floated marginless bordered image');

	foreach(pq('a > img') as $img) {
		pq($img)->parent()->before($img);
	}

	pq('a')->addClass('header')
	->wrapInner('<span class="product-title"></span>')
	->wrap('<div class="left floated middle aligned content"></div>');

	foreach(pq('.reviewer') as $span) {
		pq($span)->prev()->prev()->append($span);
	}

	pq('.reviewer')->wrap('<div class="left floated sub header"></div>');

	foreach(pq('ul > li > .item') as $item) {
		pq('.ui.list')->append($item);
	}

	pq('ul')->remove();

	return $dom->getDocument();
}
add_filter('chap_widget_woocommerce_recent_reviews', __NAMESPACE__ . '\\filter_woocommerce_recent_reviews', 10, 4);

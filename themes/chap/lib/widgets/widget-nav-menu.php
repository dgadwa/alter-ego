<?php

namespace Chap\Widgets;
use Chap\Options;
use phpQuery;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

/**
 * Determine menu position based on which sidebar
 * it is in and where that sidebar is located.
 */
function get_menu_position($sidebar_id) {

	$menu_position = 'left';
	$primary_sidebar_position = Options\get('sidebar_primary_position');

	if($sidebar_id === 'sidebar-primary') {

		$menu_position = $primary_sidebar_position;

	} elseif($sidebar_id === 'sidebar-secondary') {

		if($primary_sidebar_position === 'left') {
			$menu_position = 'right';
		} else {
			$menu_position = 'left';
		}

	}

	return $menu_position;

}

/**
 * Custom menu widget.
 */
function filter_nav_menu($widget_output, $props, $sidebar_id) {

	$menu_position = get_menu_position($sidebar_id);

	if($sidebar_id !== 'sidebar-footer') {
		$props['container'] = 'fluid menu';
	}

	$menu_classes = apply_filters('chap_widget_nav_menu_classes', $props['direction'] . ' ' . $props['inverted'] . ' ' . $props['container']);
	remove_all_filters('chap_widget_nav_menu_classes');

	$dom = phpQuery::newDocumentHTML($widget_output);

	pq('.chap-menu')->removeClass('menu')->addClass($menu_classes);

	if($menu_position === 'right') {
		// Prevent menu from going offscreen by opening on the wrong side.
		pq('.chap-menu .dropdown.item .menu')->addClass('left');
		pq('.chap-menu .dropdown.icon')->addClass('left');
	} else {
		pq('.chap-menu .dropdown.icon')
		->removeClass('dropdown')
		->removeClass('icon')
		->addClass('right caret dropdown icon');
	}

	// Can't display dropdowns in footer.
	if($sidebar_id === 'sidebar-footer') {
		pq('.chap-menu .dropdown.item .menu')->remove();
		pq('.chap-menu .dropdown.icon')->remove();
	}

	return $dom->getDocument();

}
add_filter('chap_widget_nav_menu', __NAMESPACE__ . '\\filter_nav_menu', 10, 3);

/**
 * Add CSS classes option to the custom menu widget.
 */
add_filter('in_widget_form', function($widget, $return, $instance) {
	if($widget->id_base === 'nav_menu') {
		$field_id = $widget->get_field_id('chap_classes');
		$field_name = $widget->get_field_name('chap_classes');
		$field_value = isset($instance['chap_classes']) ? $instance['chap_classes'] : '';
		?><p>
			<label for="<?php echo esc_attr($field_id); ?>"><?php esc_html_e('Custom CSS classes', 'chap'); ?>:</label>
			<input type="text" class="widefat" id="<?php echo esc_attr($field_id); ?>" name="<?php echo esc_attr($field_name); ?>" value="<?php echo esc_attr($field_value); ?>" />
		</p><?php
	}
}, 10, 3);

/**
 * Save the custom CSS classes option.
 */
add_filter('widget_update_callback', function($instance, $new_instance) {
	if(isset($new_instance['nav_menu']) && !empty($new_instance['chap_classes'])) {
		$instance['chap_classes'] = esc_attr($new_instance['chap_classes']);
	}
	return $instance;
}, 10, 2);

/**
 * Pass the custom CSS classes option value to the widget filter.
 */
add_filter('widget_nav_menu_args', function($nav_menu_args, $nav_menu, $args, $instance) {
	if(isset($instance['chap_classes'])) {
		$classes = trim($instance['chap_classes']);
		if(strlen($classes) < 1) {
			return $nav_menu_args;
		}
		add_filter('chap_widget_nav_menu_classes', function($menu_classes) use ($classes) {
			$menu_classes = $classes;
			return $menu_classes;
		});
	}
	return $nav_menu_args;
}, 10, 4);

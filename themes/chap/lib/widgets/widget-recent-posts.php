<?php

namespace Chap\Widgets;
use phpQuery;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

/**
 * Recent posts widget.
 */
function filter_recent_posts($widget_output, $props) {
	$dom = phpQuery::newDocumentHTML($widget_output);

	pq('ul')->addClass('ui fluid ' . $props['direction'] . ' ' . $props['inverted'] . ' ' . $props['container']);
	pq('li')->addClass('item');
	pq('li > a')->addClass('header');
	pq('li > span.post-date')->addClass('description');

	return $dom->getDocument();
}
add_filter('chap_widget_recent-posts', __NAMESPACE__ . '\\filter_recent_posts', 10, 2);

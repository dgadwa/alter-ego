<?php

namespace Chap\Assets;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

/**
 * Get paths for assets.
 */
class JsonManifest {

	private $manifest;

	public function __construct($manifest_path) {
		if(file_exists($manifest_path)) {
			include_once ABSPATH . 'wp-admin/includes/file.php';
			\WP_Filesystem();
			global $wp_filesystem;
			$this->manifest = json_decode($wp_filesystem->get_contents($manifest_path), true);
		} else {
			$this->manifest = [];
		}
	}

	public function get() {
		return $this->manifest;
	}

	public function get_path($key = '', $default = null) {
		$collection = $this->manifest;

		if(is_null($key)) {
			return $collection;
		}

		if(isset($collection[$key])) {
			return $collection[$key];
		}

		foreach(explode('.', $key) as $segment) {
			if(!isset($collection[$segment])) {
				return $default;
			} else {
				$collection = $collection[$segment];
			}
		}

		return $collection;
	}

}

/**
 * Returns the asset path of specified file.
 *
 * @param  string $filename File name.
 * @return string           Asset path.
 */
function asset_path($filename) {
	$dist_path = get_template_directory_uri() . '/dist/';
	$directory = dirname($filename) . '/';
	$file = basename($filename);

	static $manifest;
	if(empty($manifest)) {
		$manifest_path = get_template_directory() . '/dist/' . 'assets.json';
		$manifest = new JsonManifest($manifest_path);
	}

	$get = $manifest->get();
	if(is_array($get) && array_key_exists($file, $get)) {
		return $dist_path . $directory . $get[$file];
	} else {
		return $dist_path . $directory . $file;
	}
}

/**
 * Looks up files from theme root directory that match the selector.
 * Then looks for child theme overrides.
 *
 * @param  string $dir           Subdirectory relative to (child)theme root.
 * @param  string $selector      File name selector.
 * @param  string $key_cutoff    Where in the path the "basename" should begin.
 * @return array                 Array of basename => file.
 */
function theme_files($dir, $selector, $key_cutoff = '') {
	$dir = !empty($dir) ? $dir . DIRECTORY_SEPARATOR : '';
	$files = [];
	$theme_root = get_template_directory();
	$child_root = get_stylesheet_directory();

	/**
	 * Collect the files matching the selector from specified directory.
	 */
	$glob = glob($theme_root . DIRECTORY_SEPARATOR . $dir . $selector, GLOB_BRACE);
	foreach($glob as $file) {
		if(strlen($key_cutoff) > 0) {
			$explode = explode($key_cutoff, $file);
			$key = end($explode);
		} else {
			$key = basename($file);
		}
		$files[$key] = $file;
	}

	// Rewrite Semantic UI path for child themes.
	$dir = str_replace('lib/semantic-ui', 'semantic-ui', $dir);

	/**
	 * Override files if using Child theme.
	 */
	if($child_root !== $theme_root) {
		$glob = glob($child_root . DIRECTORY_SEPARATOR . $dir . $selector, GLOB_BRACE);
		foreach($glob as $file) {
			if(strlen($key_cutoff) > 0) {
				$explode = explode($key_cutoff, $file);
				$key = end($explode);
			} else {
				$key = basename($file);
			}
			$files[$key] = $file;
		}
	}

	return $files;
}

/**
 * Returns a path to a theme file which may be overriden
 * by a matching file in a child theme directory.
 *
 * @param  string $dir      Subdirectory relative to (child)theme root.
 * @param  string $filename File name. May be a glob.
 * @return string           Path to theme file.
 */
function theme_file($dir, $filename) {
	$files = theme_files($dir, $filename);
	if(count($files) < 1) {
		return '';
	}

	/**
	 * Return the path to first file in array.
	 */
	foreach($files as $file) {
		return $file;
	}
}

/**
 * Returns the URI to Semantic UI CSS file.
 *
 * @param  string $rtl RTL suffix.
 * @return string SUI CSS URI.
 */
function get_sui_css_uri($rtl = '') {
	/**
	 * Generated Semantic UI path.
	 */
	$wp_upload  = wp_upload_dir();
	$name       = 'chap-semantic-ui';
	$file       = '/chap/' . $name . $rtl . '.css';

	if(file_exists($wp_upload['basedir'] . $file)) {
		/**
		 * Use generated Semantic UI.
		 */
		$uri = $wp_upload['baseurl'] . $file;
	} else {
		/**
		 * Use default bundled SUI CSS.
		 */
		$uri = asset_path('styles/semantic.css');
		/**
		 * Notify user that Semantic UI is not yet compiled.
		 */
		if(
			is_admin()
			&& get_option('envato_setup_complete', false)
			&& !get_option('chap_compiler_notice_dismissed', false)
			&& (!isset($_GET['page']) || $_GET['page'] !== 'chap-compiler')
		) {
			add_action('admin_notices', function() {
				?>
				<div class="notice notice-warning is-dismissible">
					<p>
					<?php
						printf(
							esc_html__('Semantic UI CSS is not yet compiled, fallback is being used - not recommended. %1$sGo to compiler%2$s', 'chap'),
							'<a href="' . admin_url('admin.php?page=chap-compiler&dismiss_compiler_notice=true') . '">',
							'</a>'
						);
					?>
					</p>
				</div>
				<?php
			});
		}
	}

	return apply_filters('chap_sui_css_uri', $uri);
}

<?php

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

require_once AMP__DIR__ . '/includes/embeds/class-amp-gallery-embed.php';

/**
 * Class Chap_AMP_Gallery_Embed_Handler
 */
class Chap_AMP_Gallery_Embed_Handler extends AMP_Gallery_Embed_Handler {

	/**
	 * Gallery shortcode size argument.
	 *
	 * @var string
	 */
	private $gallery_size = 'thumbnail';

	public function shortcode($attr) {
		/**
		 * Store the gallery shortcode size argument.
		 */
		if(isset($attr['size'])) {
			$this->gallery_size = $attr['size'];
		}

		return parent::shortcode($attr);
	}

	public function render($args) {
		/**
		 * Use smaller gallery height when using smaller images.
		 */
		switch($this->gallery_size) {
			case 'thumbnail':
			case 'chap-small':
				$this->args['height'] = 150;
			break;
			case 'medium':
				$this->args['height'] = 300;
			break;
			default:
				$this->args['height'] = $this->DEFAULT_HEIGHT;
		}

		return parent::render($args);
	}

}

/**
 * Use the Chap Gallery embed handler instead of normal one.
 */
add_filter('amp_content_embed_handlers', function($handlers) {
	if(isset($handlers['AMP_Gallery_Embed_Handler'])) {
		unset($handlers['AMP_Gallery_Embed_Handler']);
	}
	$handlers['Chap_AMP_Gallery_Embed_Handler'] = [];

	return $handlers;
});

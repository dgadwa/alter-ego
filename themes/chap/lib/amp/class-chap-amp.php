<?php

namespace Chap\AMP;
use Chap\Assets;
use Chap\Helpers;
use Chap\Options;
use Chap\Titles;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

/**
 * Chap_AMP class handles rendering AMP pages.
 *
 * @class   Chap_AMP
 * @version 1.2.0
 * @since   1.9.0
 * @author  websevendev <websevendev@gmail.com>
 */
class Chap_AMP {

	/**
	 * Styles to load for the AMP page.
	 */
	protected static $styles = [];

	/**
	 * Collect WordPress Recipe Maker metadata.
	 *
	 * @var array
	 */
	protected static $wprm_meta = [];

	/**
	 * Init Chap_AMP class.
	 */
	public static function init() {
		/**
		 * AMP plugin required.
		 *
		 * @see https://wordpress.org/plugins/amp
		 * @see https://github.com/Automattic/amp-wp
		 */
		if(!defined('AMP__VERSION')) {
			return;
		}

		/**
		 * Load files.
		 */
		add_action('after_setup_theme', [__CLASS__, 'load']);

		/**
		 * Default styles.
		 */
		self::$styles = [
			'base'       => true,
			'header'     => true,
			'sidebar'    => true,
			'container'  => true,
			'button'     => true,
			'loop'       => true,
			'pagination' => true,
			'footer'     => true,
			'generic'    => true,
			'table'      => true,
			'helpers'    => true,
		];

		/**
		 * AMP hooks.
		 */
		add_action('wp', [__CLASS__, 'add_amp_support'], 5);
		add_filter('amp_pre_get_permalink', [__CLASS__, 'amp_get_permalink']);
		add_filter('amp_post_template_file', [__CLASS__, 'amp_post_template_file'], 10, 3);
		add_filter('amp_post_template_data', [__CLASS__, 'amp_post_template_data']);
		add_filter('amp_post_template_analytics', [__CLASS__, 'amp_google_analytics']);
		add_filter('amp_post_template_analytics', [__CLASS__, 'amp_parsely_analytics']);
		add_filter('amp_content_sanitizers', [__CLASS__, 'amp_content_sanitizers']);
		add_filter('amp_post_status_default_enabled', [__CLASS__, 'amp_post_status_default_enabled'], 10, 2);

		/**
		 * Style filters.
		 */
		add_filter('chap_amp_styles', [__CLASS__, 'amp_styles']);
		add_filter('chap_amp_header_font', [__CLASS__, 'amp_header_font']);
		add_filter('chap_amp_page_font', [__CLASS__, 'amp_page_font']);
		add_filter('chap_amp_link_underline', [__CLASS__, 'amp_link_underline']);

		/**
		 * Template hooks.
		 */
		add_action('chap_amp_load_css', [__CLASS__, 'chap_amp_load_css']);
		add_action('chap_amp_header', [__CLASS__, 'chap_amp_header']);
		add_action('chap_amp_footer', [__CLASS__, 'chap_amp_footer']);
		add_action('chap_amp_thumbnail', [__CLASS__, 'build_post_thumbnail']);
		add_action('chap_amp_ad_slot', [__CLASS__, 'chap_amp_ad_slot']);
		add_filter('the_content', [__CLASS__, 'chap_insert_content_ads']);
		add_action('chap_amp_render_brand', [__CLASS__, 'chap_amp_render_brand']);
		add_filter('chap_amp_prev_post_link', [__CLASS__, 'amp_prev_post_link']);
		add_filter('chap_amp_next_post_link', [__CLASS__, 'amp_next_post_link']);
		add_filter('chap_amp_prev_posts_link', [__CLASS__, 'amp_prev_posts_link']);
		add_filter('chap_amp_next_posts_link', [__CLASS__, 'amp_next_posts_link']);
		add_filter('chap_amp_home_url_path', [__CLASS__, 'amp_home_url_path']);

		/**
		 * Use fallback image for posts, pages and products.
		 */
		add_filter('chap_amp_post_meta', [__CLASS__, 'use_fallback_image']);
		add_filter('chap_amp_page_meta', [__CLASS__, 'use_fallback_image']);
		add_filter('chap_amp_product_meta', [__CLASS__, 'use_fallback_image']);

		/**
		 * Add AMP query arg to links.
		 */
		add_filter('term_link', [__CLASS__, 'term_link']);

		/**
		 * WordPress Recipe Maker plugin integratin.
		 */
		add_filter('wprm_recipe_metadata', [__CLASS__, 'add_wprm_recipe_schema'], 10, 2);

		/**
		 * Don't remove original JSON-LD meta when SEO Framework Articles extension is active.
		 *
		 * SEO Framework often invalidates the data, for example, if article image is missing,
		 * and ends up outputting nothing.
		 * Duplicate structured data should be OK.
		 *
		 * @see https://www.littlestreamsoftware.com/articles/existing-duplicate-structured-data-treated-google/
		 */
		add_filter('the_seo_framework_remove_amp_articles', '__return_false');

	}

	/**
	 * Load AMP libraries.
	 *
	 * @since 1.1.5
	 */
	public static function load() {
		/**
		 * Load embed handlers.
		 */
		foreach(Assets\theme_files('lib/amp/embeds', 'class-chap-*-embed.php') as $file) {
			include_once $file;
		}
	}

	/**
	 * Override AMP permalink to always use ?amp=1 due to pretty URLs being unreliable.
	 *
	 * @return string AMP permalink.
	 */
	public static function amp_get_permalink($post_id) {
		// TODO: blog URL may be different.
		if(is_front_page() || is_home() || $post_id === 0) {
			return add_query_arg(AMP_QUERY_VAR, 1, get_home_url());
		}
		return add_query_arg(AMP_QUERY_VAR, 1, get_permalink($post_id));
	}

	/**
	 * Add AMP support for search and archives.
	 */
	public static function add_amp_support() {
		if(is_admin()) {
			return;
		}

		$is_shop = class_exists('WooCommerce') && is_shop();

		/**
		 * Handle rendering archives that may be empty.
		 */
		if(in_array(true, [
			is_home(),
			is_search(),
			is_archive(),
			$is_shop,
		])) {
			if(Helpers\is_amp()) {
				/**
				 * AMP plugin won't render if post with queried object's ID doesn't exist.
				 *
				 * Set the queried object id to first post in the archive.
				 *
				 * @since 1.1.3
				 */
				$post_id = get_queried_object_id();
				$post = get_post($post_id);
				if(!$post) {
					global $wp_query;
					if(count($wp_query->posts) > 0) {
						foreach($wp_query->posts as $post) {
							$wp_query->queried_object_id = $post->ID;
							continue;
						}
					} else {
						/**
						 * Query for any post and set it as global post to make AMP
						 * plugin render pages like search results with no results.
						 */
						$single_query = new \WP_Query([
							'posts_per_page' => 1,
							'ignore_sticky_posts' => true,
						]);
						if(count($single_query->posts) > 0) {
							foreach($single_query->posts as $any_post) {
								global $post;
								$post = $any_post;
								continue;
							}
						}
					}
				}

				/**
				 * Manually render AMP page.
				 */
				if(in_array(true, [
					!get_queried_object(),
					is_archive(),
					$is_shop,
				])) {
					/**
					 * Need to call amp_render_post() directly
					 * due to check for get_queried_object() added
					 * in AMP plugin 0.6.
					 */
					add_action('template_redirect', function() {
						amp_render_post(null);
						exit;
					});
				} else {
					/**
					 * Call render method manually.
					 */
					amp_prepare_render();
				}
			} else {
				/**
				 * Fix amphtml links on pages that don't work with AMP plugin.
				 */
				if(in_array(true, [
					(is_home() && !is_front_page()),
					is_archive(),
					is_search(),
					$is_shop,
				])) {
					add_filter('amp_frontend_show_canonical', '__return_false');
					add_action('wp_head', function() {
						global $wp;
						$args = [];
						if(is_search() && isset($_GET['s'])) {
							$args['s'] = esc_attr($_GET['s']);
						}
						$args[AMP_QUERY_VAR] = 1;
						$amp_url = home_url(add_query_arg($args, $wp->request));
						printf('<link rel="amphtml" href="%s">', esc_url($amp_url));
					});
				}

				/**
				 * Not an AMP endpoint, perform frontend actions.
				 */
				amp_add_frontend_actions();
			}
		}
	}

	/**
	 * Enable/disable AMP support for specific posts or pages.
	 *
	 * @since 1.2.0
	 * @param bool $default_enabled
	 * @param WP_Post $post
	 * @return bool
	 */
	public static function amp_post_status_default_enabled($default_enabled, $post) {
		/**
		 * Enable front page.
		 */
		if(in_array($post->ID, [
			(int)get_option('page_on_front'),
			(int)get_option('page_for_posts'),
		])) {
			return true;
		}

		/**
		 * Disable unsupported WooCommerce pages.
		 */
		if(class_exists('WooCommerce')) {
			if(in_array(true, [
				is_cart(),
				is_checkout(),
				is_account_page(),
			])) {
				return false;
			}
		}

		return $default_enabled;
	}

	/**
	 * Use custom template files for post types.
	 * Load additional styles for the templates.
	 */
	public static function amp_post_template_file($file, $type, $post) {
		$template = '';

		/**
		 * Try to override main template.
		 */
		if(basename($file) === 'single.php' || basename($file) === 'page.php') {

			if(is_front_page() && !is_home()) {

				$template = Assets\theme_file('amp', 'home-page.php');
				// self::$styles['article'] = true;

			} elseif(is_home()) {

				$template = Assets\theme_file('amp', 'home.php');

			} elseif(is_search()) {

				$template = Assets\theme_file('amp', 'search.php');

			} elseif(is_archive()) {

				$template = Assets\theme_file('amp', 'archive.php');

			} elseif($template = Assets\theme_file('amp', 'single-' . $post->post_type . '.php')) {

				/**
				 * Try to load single-{post_type}.php template.
				 * When successful load style of the post_type and article.
				 */
				self::$styles[$post->post_type] = true;

			}

		}

		if(strlen($template) > 0) {
			$file = $template;
		}

		$template_used = basename($file);
		$load_article_styles = (strpos($template_used, 'single-') !== false) || in_array($template_used, [
			'home-page.php',
			'single.php',
			'page.php',
		]);
		$load_article_styles = apply_filters('chap_amp_load_article_styles', $load_article_styles, $template_used);
		if($load_article_styles) {
			self::$styles['article'] = true;
		}

		return $file;
	}

	/**
	 * AMP post template data.
	 *
	 * @since 1.0.3
	 */
	public static function amp_post_template_data($data) {
		/**
		 * Build metadata.
		 */
		$data['metadata'] = self::amp_metadata($data['metadata']);

		/**
		 * Load fonts.
		 */
		unset($data['font_urls']);
		$header_fonts = Helpers\get_titan_option('sui_header_font')->getGoogleFontURLs();
		$page_fonts = Helpers\get_titan_option('sui_font')->getGoogleFontURLs();
		$font_urls = array_merge($header_fonts, $page_fonts);
		foreach($font_urls as $key => $font_url) {
			if(strpos($font_url, 'http')) {
				$data['font_urls'][$key] = $font_url;
			} else {
				$data['font_urls'][$key] = str_replace('//', 'https://', $font_url);
			}
		}
		$data['font_urls']['FontAwesome'] = 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css';

		/**
		 * Global components.
		 */
		$data['amp_component_scripts']['amp-sidebar'] = 'https://cdn.ampproject.org/v0/amp-sidebar-0.1.js';
		$data['amp_component_scripts']['amp-fit-text'] = 'https://cdn.ampproject.org/v0/amp-fit-text-0.1.js';
		$data['amp_component_scripts']['amp-accordion'] = 'https://cdn.ampproject.org/v0/amp-accordion-0.1.js';
		$data['amp_component_scripts']['amp-form'] = 'https://cdn.ampproject.org/v0/amp-form-0.1.js';

		/**
		 * @global $chap_theme
		 * @see class-chap.php
		 */
		global $chap_theme;
		if($chap_theme['has_slides']) {
			$data = self::build_slider($data);
		}

		/**
		 * Load ad assets.
		 */
		if(in_array(true, [
			Options\get('amp_ad_slot_1_enabled'),
			Options\get('amp_ad_slot_2_enabled'),
			Options\get('amp_ad_slot_3_enabled'),
		])) {
			$data['amp_component_scripts']['amp-ad'] = 'https://cdn.ampproject.org/v0/amp-ad-0.1.js';
			self::$styles['ads'] = true;
		}

		/**
		 * Load social sharing assets.
		 */
		if(!is_home() && !is_front_page() && in_array(get_post_type(), Options\get('amp_social_post_types'))) {
			$data['amp_component_scripts']['amp-social-share'] = 'https://cdn.ampproject.org/v0/amp-social-share-0.1.js';
			self::$styles['social'] = true;
		}

		/**
		 * Load RTL styles.
		 */
		if(is_rtl()) {
			self::$styles['rtl'] = true;
		}

		/**
		 * Strings.
		 */
		$data['comments_link_text'] = esc_html__('Leave a comment', 'chap');

		/**
		 * Accept components.
		 */
		$data['amp_component_scripts'] = apply_filters('chap_amp_component_scripts', $data['amp_component_scripts']);

		/**
		 * Fix canonical URL, it's wrong for archives due to AMP plugin not supporting it yet.
		 */
		global $wp;
		$data['canonical_url'] = esc_url(home_url(remove_query_arg(AMP_QUERY_VAR, $wp->request)));
		if(is_search() && isset($_GET['s'])) {
			$data['canonical_url'] = add_query_arg('s', esc_attr($_GET['s']), $data['canonical_url']);
		}

		return $data;
	}

	/**
	 * Get JSON-LD schema by ID.
	 *
	 * @param  int $id Post ID.
	 * @return array
	 */
	public static function get_schema_by_id($id) {
		if(Helpers\is_product($id)) {
			return self::get_product_schema($id);
		}

		$post_schema = self::get_post_schema($id);
		$post_type = get_post_type($id);

		if($post_type === 'page') {
			$post_schema['@type'] = 'Article';
		}

		return $post_schema;
	}

	/**
	 * Get post JSON-LD schema by ID.
	 *
	 * @param  int   $id Post ID.
	 * @return array
	 */
	public static function get_post_schema($id) {
		$schema = [
			'@context' => 'http://schema.org',
			'@type' => 'BlogPosting',
			'mainEntityOfPage' => get_permalink($id),
			'publisher' => [
				'@type' => 'Organization',
				'name' => get_bloginfo('name'),
			],
			'headline' => get_the_title($id),
			'datePublished' => date('c', get_the_date('U', $id)),
			'dateModified' => date('c', get_post_modified_time('U', false, $id)),
		];

		$post_author = get_userdata(get_post_field('post_author', $id));
		if(isset($post_author->display_name)) {
			$schema['author'] = [
				'@type' => 'Person',
				'name' => $post_author->display_name,
			];
		}

		$site_icon_url = apply_filters('amp_site_icon_url', function_exists('get_site_icon_url') ? get_site_icon_url(32) : '');
		if($site_icon_url) {
			$schema['publisher']['logo'] = [
				'@type' => 'ImageObject',
				'url' => Helpers\ensure_protocol($site_icon_url),
				'height' => 32,
				'width' => 32,
			];
		}

		$post_image_src = wp_get_attachment_image_src(get_post_thumbnail_id($id), 'full');
		if(is_array($post_image_src)) {
			$schema['image'] = [
				'@type' => 'ImageObject',
				'url' => Helpers\ensure_protocol($post_image_src[0]),
				'width' => $post_image_src[1],
				'height' => $post_image_src[2],
			];
		}

		return apply_filters('chap_amp_post_meta', $schema);
	}

	/**
	 * Use fallback image for posts in schema.
	 */
	public static function use_fallback_image($schema) {
		if(isset($schema['image'])) {
			return $schema;
		}

		$fallback_image = Options\get('amp_fallback_image');
		if($fallback_image) {
			$fallback_image_src = wp_get_attachment_image_src($fallback_image, 'full');
			if(is_array($fallback_image_src)) {
				$schema['image'] = [
					'@type' => 'ImageObject',
					'url' => Helpers\ensure_protocol($fallback_image_src[0]),
					'width' => $fallback_image_src[1],
					'height' => $fallback_image_src[2],
				];
			}
		}

		return $schema;
	}

	/**
	 * Get WC product JSON-LD schema.
	 *
	 * @param  int   $id
	 * @param  array $image_object
	 * @return array
	 */
	public static function get_product_schema($id, $image_object = false) {
		$product = wc_get_product($id);
		if(!isset($product->id)) {
			return [];
		}

		$schema = [
			'@context' => 'http://schema.org/',
			'@type' => 'Product',
			'mainEntityOfPage' => get_the_permalink(),
			'url' => get_the_permalink(),
			'name' => $product->get_title(),
			'description' => wp_trim_excerpt(),
			'sku' => $product->get_sku(),
			'weight' => $product->get_weight(),
			'offers' => [
				'@type' => 'Offer',
				'priceCurrency' => get_woocommerce_currency(),
				'price' => $product->get_price(),
				'availability' => 'http://schema.org/' . ($product->is_in_stock() ? 'InStock' : 'OutOfStock'),
			],
		];

		if($image_object) {
			$schema['image'] = $image_object;
		}

		if($product->has_dimensions()) {
			$schema['width'] = $product->get_width();
			$schema['height'] = $product->get_height();
		}

		if($product->has_weight()) {
			$schema['weight'] = $product->get_weight();
		}

		if(get_option('woocommerce_enable_review_rating') === 'yes' && $product->get_rating_count() > 0) {
			$schema['aggregateRating'] = [
				'@type' => 'AggregateRating',
				'ratingValue' => $product->get_average_rating(),
				'ratingCount' => $product->get_rating_count(),
				'reviewCount' => $product->get_review_count(),
			];
		}

		return apply_filters('chap_amp_product_meta', $schema);
	}

	/**
	 * Returns an ItemList schema for all the posts on the current page.
	 *
	 * @since 1.1.4
	 * @return array
	 */
	public static function get_itemlist_schema() {
		$itemlist = [];

		global $wp;
		$url = home_url(add_query_arg([], $wp->request));

		$position = 1;
		while(have_posts()) {
			the_post();
			$id = get_the_ID();
			$post_meta = self::get_schema_by_id($id);
			$post_meta['url'] = add_query_arg(AMP_QUERY_VAR, 1, $url) . '#' . basename(get_permalink());

			if(!isset($post_meta['image'])) {
				$thumbnail_url = get_the_post_thumbnail_url($id);
				if($thumbnail_url) {
					$post_meta['image'] = Helpers\ensure_protocol($thumbnail_url);
				}
			}

			$itemlist[] = [
				'@type' => 'ListItem',
				'position' => $position,
				'item' => $post_meta,
			];

			$position++;
		}

		return $itemlist;
	}

	/**
	 * Adds metadata of WPRM recipe to post template head.
	 *
	 * @since 1.1.4
	 * @param array $metadata
	 * @param object $recipe
	 * @return void
	 */
	public static function add_wprm_recipe_schema($metadata, $recipe) {
		add_action('amp_post_template_head', function() use ($metadata) {
			if(!class_exists('WPRM_Metadata')) {
				return;
			}

			$wprm_meta = new \WPRM_Metadata;
			if(isset($metadata['image'])) {
				$metadata['image'] = Helpers\ensure_protocol($metadata['image']);
			}
			$metadata = $wprm_meta::sanitize_metadata($metadata);

			echo '<script type="application/ld+json">' . wp_json_encode($metadata) . '</script>';
		});

		return $metadata;
	}

	/**
	 * Modify JSON-LD metadata based on AMP route.
	 *
	 * @param  array $metadata
	 * @return array
	 */
	protected static function amp_metadata($metadata) {
		/**
		 * Make sure publisher logo image url has a protocol.
		 */
		if(isset($metadata['publisher']['logo']['url'])) {
			$metadata['publisher']['logo']['url'] = Helpers\ensure_protocol($metadata['publisher']['logo']['url']);
		}

		/**
		 * Make sure image url has a protocol.
		 */
		if(isset($metadata['image']['url'])) {
			$metadata['image']['url'] = Helpers\ensure_protocol($metadata['image']['url']);
		}

		/**
		 * WooCommerce product schema.
		 */
		if(function_exists('is_product') && is_product()) {
			return self::get_product_schema(get_the_ID(), $metadata['image']);
		}

		/**
		 * Search results schema.
		 */
		if(is_search()) {
			$metadata['@context'] = 'http://schema.org/';
			$metadata['@type'] = 'SearchResultsPage';
			$metadata['mainEntity'] = [
				'@type' => 'ItemList',
				'name' => Titles\title(),
				'itemListOrder' => 'http://schema.org/ItemListOrderAscending',
				'itemListElement' => self::get_itemlist_schema(),
			];
			return apply_filters('chap_amp_search_meta', $metadata);
		}

		/**
		 * Page schema.
		 */
		if(get_post_type() === 'page') {
			$metadata['@type'] = 'Article';
			return apply_filters('chap_amp_page_meta', $metadata);
		}

		/**
		 * Loop schema.
		 */
		if(is_archive() || is_home()) {
			$meta = [
				'@context' => 'http://schema.org/',
				'@type' => 'ItemList',
				'itemListOrder' => 'http://schema.org/Unordered',
				'mainEntityOfPage' => esc_url(home_url(add_query_arg([], $GLOBALS['wp']->request))),
				'name' => Titles\title(),
				'itemListElement' => self::get_itemlist_schema(),
			];

			return apply_filters('chap_amp_archive_meta', $meta);
		}

		return apply_filters('chap_amp_post_meta', $metadata);
	}

	/**
	 * Add Google analytics.
	 */
	public static function amp_google_analytics($analytics) {
		if(!is_array($analytics)) {
			$analytics = [];
		}

		$account = Options\get('amp_ga_id');
		if(empty($account) || !preg_match("/(UA|YT|MO)-\d+-\d+/i", $account)) {
			return $analytics; // Google Analytics ID not set or invalid.
		}

		/**
		* @see https://developers.google.com/analytics/devguides/collection/amp-analytics
		*/
		$analytics['chap-googleanalytics'] = [
			'type' => 'googleanalytics',
			'attributes' => [],
			'config_data' => [
				'vars' => [
					'account' => $account,
				],
				'triggers' => [
					'trackPageview' => [
						'on' => 'visible',
						'request' => 'pageview',
					],
				],
			],
		];

		return $analytics;
	}

	/**
	 * Add Parse.ly analytics.
	 */
	public static function amp_parsely_analytics($analytics) {
		if(!is_array($analytics)) {
			$analytics = [];
		}

		$api_key = Options\get('amp_parsely_api_key');
		if(empty($api_key)) {
			return $analytics;
		}

		/**
		 * @see https://www.parsely.com/docs/integration/tracking/google-amp.html
		 */
		$analytics['chap-parsely'] = [
			'type' => 'parsely',
			'attributes' => [],
			'config_data' => [
				'vars' => [
					'apikey' => $api_key,
				],
			],
		];

		return $analytics;
	}

	/**
	 * Find the AMP template and include it.
	 */
	protected static function locate_template($name, $amp) {
		$template = Assets\theme_file('amp', $name . '.php');
		if(file_exists($template)) {
			include $template;
		}
	}

	/**
	 * Output all of AMP CSS.
	 */
	public static function chap_amp_load_css($amp) {
		ob_start();
		/**
		 * Load main styles, that have been activated.
		 */
		$amp->load_parts(['style']);
		/**
		 * Load styles hooked to amp_post_template_css action.
		 */
		do_action('amp_post_template_css', $amp);
		/**
		 * Load custom AMP CSS from theme options.
		 */
		echo get_option('chap_amp_compiled_css', '');
		$css = ob_get_clean();

		$css = str_replace('!important', '', $css); // !important not allowed in AMP.

		if(is_rtl()) {
			echo Helpers\css_rtl($css);
		} else {
			echo Helpers\css_minify($css);
		}
	}

	/**
	 * Render the beginning HTML of all AMP templates.
	 *
	 * This template also loads the header bar, slider and ad slot 1 templates.
	 */
	public static function chap_amp_header($amp) {
		self::locate_template('html-start', $amp);
	}

	/**
	 * Render the closing HTML of all AMP templates.
	 *
	 * This template also loads the footer template.
	 */
	public static function chap_amp_footer($amp) {
		self::locate_template('html-end', $amp);
	}

	/**
	 * Render brand image.
	 */
	public static function chap_amp_render_brand() {

		$custom_logo_id = get_theme_mod('custom_logo');

		if($custom_logo_id && Options\get('brand_show_logo')) {

			$logo = wp_get_attachment_image($custom_logo_id, 'chap-small', false, [
				'class' => 'custom-logo',
				'alt' => esc_html(get_bloginfo('name')),
			]);

			$sanitized_logo = false;

			if(strpos($logo, '.svg') !== false) {

				/**
				 * SVG's get width and height as 1, determine
				 * the real size and make replacements.
				 */
				if(is_array($src = wp_get_attachment_image_src($custom_logo_id))) {

					if($xml = simplexml_load_file($src[0])) {

						$atts = $xml->attributes();
						$width = (int)$atts->width;
						$height = (int)$atts->height;
						$have_width = (is_int($width) && $width > 0);
						$have_height = (is_int($height) && $height > 0);

						if($have_width && $have_height) {

							$max_width = 150;
							$ratio = $width / $max_width;
							$width = $max_width;
							$height = round($height / $ratio);

							$sanitized_logo[0] = str_replace(
								[
									'<img',
									'/>',
									'width="1"',
									'height="1"',
								],
								[
									'<amp-img',
									'></amp-img>',
									'width="' . $width . '"',
									'height="' . $height . '"',
								],
								$logo
							);

						}

					}

				}

			} else {

				$sanitized_logo = \AMP_Content_Sanitizer::sanitize(
					$logo,
					['AMP_Img_Sanitizer' => []],
					['content_max_width' => 150]
				);

			}

			if(is_array($sanitized_logo)) {
				echo($sanitized_logo[0]); // AMP html, no kses.
			}

		}

	}

	/**
	 * Render an Ad slot.
	 *
	 * @param  int $id Ad slot ID.
	 */
	public static function chap_amp_ad_slot($id) {

		if(!is_int($id)) {
			return;
		}

		$enabled = Options\get('amp_ad_slot_' . $id . '_enabled');

		if(!$enabled) {
			return;
		}

		$markup = Options\get('amp_ad_slot_' . $id);

		echo($markup); // AMP html, no kses.

	}

	/**
	 * Insert AMP ads to content.
	 */
	public static function chap_insert_content_ads($content) {
		$ad_markers = apply_filters('chap_amp_ad_markers', ['<!-- amp-ad -->', '<!--amp-ad-->']);

		if(!Helpers\is_amp()) {
			return str_replace($ad_markers, '', $content);
		}

		$enabled = Options\get('amp_ad_slot_2_enabled');

		if(!$enabled) {
			return $content;
		}

		ob_start();
		do_action('chap_amp_ad_slot', 2);
		$ad = ob_get_clean();

		return str_replace($ad_markers, $ad, $content);
	}

	/**
	 * Output post thumbnail AMP HTML.
	 *
	 * @param  int    $post_id
	 * @param  string $size
	 */
	public static function build_post_thumbnail($post_id, $size = 'thumbnail') {
		$featured_html = get_the_post_thumbnail($post_id, $size);
		if(!$featured_html) {
			return;
		}

		$featured_id = get_post_thumbnail_id($post_id);
		$featured_image = get_post($featured_id);

		list($sanitized_html, $featured_scripts, $featured_styles) = \AMP_Content_Sanitizer::sanitize(
			$featured_html,
			['AMP_Img_Sanitizer' => []],
			['content_max_width' => 100]
		);

		echo($sanitized_html); // AMP html, no kses.
	}

	/**
	 * Build the slider.
	 *
	 * @since 1.1.0
	 */
	public static function build_slider($data) {
		/**
		 * Render the slider.
		 */
		ob_start();
		do_action('chap_render_slider');
		$slider = ob_get_clean();

		/**
		 * Sanitize the slider.
		 */
		list($sanitized_slider, $scripts, $styles) = \AMP_Content_Sanitizer::sanitize(
			$slider,
			[
				'AMP_Style_Sanitizer' => [],
				'AMP_Blacklist_Sanitizer' => [],
				'AMP_Img_Sanitizer' => [],
				'AMP_Video_Sanitizer' => [],
				'AMP_Audio_Sanitizer' => [],
			]
		);

		/**
		 * Add scripts.
		 */
		$data['amp_component_scripts']['amp-carousel'] = 'https://cdn.ampproject.org/v0/amp-carousel-0.1.js';
		/**
		 * Add styles.
		 */
		self::$styles['slider'] = true;
		/**
		 * Add inline styles.
		 */
		foreach($styles as $key => $value) {
			$data['post_amp_styles'][$key] = $value;
		}

		/**
		 * Output the slider.
		 *
		 * @see /amp/slider.php
		 */
		add_action('chap_render_amp_slider', function() use ($sanitized_slider) {
			echo $sanitized_slider;
		});

		return $data;
	}

	/**
	 * Add or remove styles based on AMP route.
	 *
	 * @param  array $styles Modified styles.
	 * @return array $styles Styles to use.
	 */
	public static function amp_styles($styles) {
		$styles = array_merge(self::$styles, $styles);
		return $styles;
	}

	/**
	 * Use header font from theme options.
	 *
	 * @param  string $header_font CSS font.
	 * @return string              CSS font.
	 */
	public static function amp_header_font($font) {
		$header_font = Options\get('sui_header_font');
		if($header_font) {
			if($header_font['font-type'] === 'google') {
				$header_font = "'" . $header_font['font-family'] . "', 'Helvetica Neue', Arial, Helvetica, sans-serif";
			} else {
				$header_font = $header_font['font-family'];
			}

			return $header_font;
		}

		return $font;
	}

	/**
	 * Use page font from theme options.
	 *
	 * @param  string $page_font CSS font.
	 * @return string            CSS font.
	 */
	public static function amp_page_font($font) {
		$page_font = Options\get('sui_font');
		if($page_font) {
			if($page_font['font-type'] === 'google') {
				$page_font = "'" . $page_font['font-family'] . "', 'Helvetica Neue', Arial, Helvetica, sans-serif";
			} else {
				$page_font = $page_font['font-family'];
			}

			return $page_font;
		}

		return $font;
	}

	/**
	 * Get link underline CSS from theme options.
	 *
	 * @return array
	 */
	public static function amp_link_underline($underline) {
		$link_underline = Options\get('sui_link_underline');
		switch($link_underline) {
			case 'hover':
				$underline = 'none';
				$underline_hover = 'underline';
			break;
			case 'always':
				$underline = 'underline';
				$underline_hover = 'underline';
			break;
			default:
				$underline = 'none';
				$underline_hover = 'none';
		}

		return [
			'underline' => $underline,
			'underline_hover' => $underline_hover,
		];
	}

	/**
	 * Adds AMP query var to HTML anchor tag links.
	 *
	 * Link must be of format: <a href="link" rel="">...</a>
	 * or a different anchor can be specified.
	 *
	 * Functions that return links in that format:
	 * get_previous_post_link(), get_next_post_link(),
	 * get_previous_posts_link(), get_next_posts_link().
	 *
	 * @param string $link
	 * @param string $anchor
	 */
	protected static function add_amp_query_var($link, $anchor = '" rel=') {
		if(strpos($link, $anchor) !== false) {
			return str_replace($anchor, '?' . AMP_QUERY_VAR . '=1' . $anchor, $link);
		}
		return $link;
	}

	/**
	 * Wrapper for get_previous_post_link() that adds icon and AMP query var.
	 *
	 * @return mixed
	 */
	public static function amp_prev_post_link($in_same_term = false, $amp = true) {
		$dir = is_rtl() ? 'right' : 'left';
		$link = get_previous_post_link('%link', '<i class="fa fa-chevron-' . esc_attr($dir) . '"></i>&nbsp;%title', $in_same_term);
		if($link) {
			if($amp) {
				return self::add_amp_query_var($link);
			} else {
				return $link;
			}
		}

		return false;
	}

	/**
	 * Wrapper for get_next_post_link() that adds icon and AMP query var.
	 *
	 * @return mixed
	 */
	public static function amp_next_post_link($in_same_term = false, $amp = true) {
		$dir = is_rtl() ? 'left' : 'right';
		$link = get_next_post_link('%link', '%title&nbsp;<i class="fa fa-chevron-' . esc_attr($dir) . '"></i>', $in_same_term);
		if($link) {
			if($amp) {
				return self::add_amp_query_var($link);
			} else {
				return $link;
			}
		}

		return false;
	}

	/**
	 * Wrapper for get_previous_posts_link() that adds icon and AMP query var.
	 *
	 * @return mixed
	 */
	public static function amp_prev_posts_link($label = '', $amp = true) {
		$link = get_previous_posts_link('<i class="fa fa-chevron-left"></i>&nbsp;' . $label);
		if($link) {
			if($amp) {
				return self::add_amp_query_var($link);
			} else {
				return $link;
			}
		}

		return false;
	}

	/**
	 * Wrapper for get_next_posts_link() that adds icon and AMP query var.
	 *
	 * @return mixed
	 */
	public static function amp_next_posts_link($label = '', $amp = true) {
		$link = get_next_posts_link($label . '&nbsp;<i class="fa fa-chevron-right"></i>');
		if($link) {
			if($amp) {
				return self::add_amp_query_var($link);
			} else {
				return $link;
			}
		}

		return false;
	}

	/**
	 * The home URL path on AMP pages.
	 *
	 * @return string
	 */
	public static function amp_home_url_path($path) {
		return $path . '?' . AMP_QUERY_VAR . '=1';
	}

	/**
	 * Add AMP query var to term links.
	 *
	 * @since 1.1.2
	 * @param string $link
	 * @return string
	 */
	public static function term_link($link) {
		return Helpers\maybe_amp($link);
	}

	/**
	 * Temporarily add Blacklist Sanitizer back after AMP 0.5 update,
	 * further theme updates need to be made to accomodate the change.
	 *
	 * @since 1.1.3
	 * @return array
	 */
	public static function amp_content_sanitizers($sanitizers) {
		$sanitizers['AMP_Blacklist_Sanitizer'] = [];
		return $sanitizers;
	}

}

<?php

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

/**
 * Select pages control.
 */
class ChapTitanFrameworkOptionChapSelectPages extends ChapTitanFrameworkOptionSelectPages {

	function __construct($settings, $owner) {
		parent::__construct($settings, $owner);
	}

	/**
	 * Fix options.
	 */
	public function create_select_options() {

		parent::create_select_options();

		/**
		 * Remove ' - Select - ' option.
		 */
		unset($this->settings['options']['']);

	}

	/**
	 * Initialize the select2 field
	 *
	 * @return void
	 */
	public function init_select_script() {
		$placeholder = esc_html__('Select pages...', 'chap');
		?>
		<script>
		jQuery(document).ready(function(){
			if(jQuery().select2) {
				jQuery('.tf-chap-select-pages select').select2({placeholder: '<?php echo esc_attr($placeholder); ?>'});
			}
		});
		</script>
		<?php
	}

}

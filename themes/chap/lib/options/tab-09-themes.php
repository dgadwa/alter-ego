<?php

namespace Chap\Options;
use Chap\Helpers;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

$tab = $panel->createTab(['name' => esc_html__('Themes', 'chap')]);

$tab->createOption([
	'name' => esc_html__('Semantic UI themes', 'chap'),
	'desc' => esc_html__('This page allows to switch between various Semantic UI component themes.', 'chap'),
	'type' => 'heading',
]);

$tab->createOption([
	'id' => 'show_experimental_themes',
	'name' => esc_html__('Experimental themes', 'chap'),
	'desc' => sprintf(
		/* Translators: 1, 2: HTML anchor tags, 3: HTML new line tag, 4, 5: HTML anchor tags. */
		esc_html__('Show additional themes from %1$sSemantic-UI-Forest%2$s. %3$sThese themes are not optimized to be used with Chap and may require manual configuration using the %4$sSemantic UI component editor%5$s.', 'chap'),
		'<a href="http://semantic-ui-forest.com/themes/" target="_blank" rel="noopener">',
		'</a>',
		'<br />',
		'<a href="' . esc_url(admin_url('admin.php?page=chap-settings&tab=sui')) . '" target="_blank" rel="noopener">',
		'</a>'
	),
	'type' => 'checkbox',
	'default' => false,
]);

$sui_files = include get_template_directory() . '/lib/admin/data-chap-sui-files.php';
$skip_components = [
	'container', 'flag', 'header', 'image', 'rail', 'reveal',
	'grid', 'ad', 'dimmer', 'embed', 'nag', 'search', 'shape',
	'sidebar', 'sticky', 'tab', 'transition', 'reset', 'rating',
	'chap-core', 'chap-rtl', 'chap-wc',
];
$prev_type = 'globals';
foreach($sui_files as $component => $data) {
	list($origin, $type) = explode('/', $data['path']);
	if($origin === 'chap' || !isset($type)) {
		continue;
	}
	if(in_array($component, $skip_components)) {
		continue;
	}

	// Create subheader.
	if($type !== $prev_type) {
		$tab->createOption([
			'id' => 'header-' . $type,
			'name' => ucfirst($type),
			'type' => 'heading',
		]);
		$prev_type = $type;
	}

	// Create option.
	$tab->createOption([
		'id' => 'component_' . $component,
		'name' => isset($data['name']) ? $data['name'] : ucfirst($component),
		'desc' => isset($data['desc']) ? $data['desc'] : '',
		'type' => $component === 'site' ? 'radio' : 'select',
		'options' => (isset($_GET['tab']) && $_GET['tab'] === 'themes') ? Helpers\get_available_sui_themes($type, $component) : [],
		'default' => 'default',
	]);

	// Add an option to site component.
	if($component === 'site') {
		$tab->createOption([
			'id' => 'sui_use_site_components',
			'name' => esc_html__('Override', 'chap'),
			'desc' => esc_html__('Use chosen site theme for all components, if available.', 'chap'),
			'type' => 'checkbox',
			'default' => false,
		]);
	}

}

$tab->createOption([
	'name' => esc_html__('Compilation', 'chap'),
	'desc' => sprintf(
		/* Translators: HTML anchor tag to compiler settings page. */
		esc_html__('Compiler can be configured on the %1$scompiler settings%2$s page.', 'chap'),
		'<a href="' . esc_url(admin_url('admin.php?page=chap-compiler')) . '">',
		'</a>'
	),
	'type' => 'heading',
]);

$tab->createOption([
	'id' => 'sui_recompile_themes',
	'name' => esc_html__('Recompile', 'chap'),
	'desc' => esc_html__('Recompile Semantic UI after saving options on this page.', 'chap'),
	'type' => 'checkbox',
	'default' => true,
]);

$tab->createOption(['type' => 'save']);

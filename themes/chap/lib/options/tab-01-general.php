<?php

namespace Chap\Options;
use Chap\Assets;
use Chap\Helpers;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

$tab = $panel->createTab(['name' => esc_html__('General', 'chap')]);

$tab->createOption([
	'name' => esc_html__('Sidebars', 'chap'),
	'desc' => esc_html__('Configure sidebars default behavior.', 'chap') .
		'<br />' .
		esc_html__('Can be overridden for specific pages by choosing a different template while editing the page.', 'chap'),
	'type' => 'heading',
]);

$tab->createOption([
	'id' => 'primary_sidebar_width',
	'name' => esc_html__('Primary sidebar width', 'chap'),
	'type' => 'number',
	'min' => 1,
	'max' => 8,
	'step' => 1,
	'unit' => ' / 16',
	'default' => 4,
]);

$tab->createOption([
	'id' => 'secondary_sidebar_width',
	'name' => esc_html__('Secondary sidebar width', 'chap'),
	'type' => 'number',
	'min' => 1,
	'max' => 8,
	'step' => 1,
	'unit' => ' / 16',
	'default' => 4,
]);

$tab->createOption([
	'id' => 'sidebar_primary_position',
	'name' => esc_html__('Primary sidebar position', 'chap'),
	'type' => 'radio',
	'horizontal' => true,
	'options' => [
		'left' => esc_html__('Left', 'chap'),
		'right' => esc_html__('Right', 'chap'),
	],
	'default' => 'right',
]);

$tab->createOption([
	'id' => 'sidebar_secondary_enable',
	'name' => esc_html__('Enable secondary sidebar', 'chap'),
	'desc' => esc_html__('Show two sidebars.', 'chap'),
	'type' => 'checkbox',
	'default' => false,
]);

$tab->createOption([
	'id' => 'sidebar_disable_front',
	'name' => esc_html__('Disable on front page', 'chap'),
	'desc' => esc_html__("Don't show any sidebars on front page.", 'chap'),
	'type' => 'checkbox',
	'default' => false,
]);

$tab->createOption([
	'id' => 'sidebar_disable_all',
	'name' => esc_html__('Disable globally', 'chap'),
	'desc' => esc_html__("Don't show any sidebars (can be overridden with templates).", 'chap'),
	'type' => 'checkbox',
	'default' => false,
]);

$tab->createOption([
	'name' => esc_html__('Loop type', 'chap'),
	'desc' => esc_html__('Choose how to display posts in loops such as blog page, category archives, author archives and search results.', 'chap'),
	'type' => 'heading',
]);

$tab->createOption([
	'id' => 'loop_override_front_page',
	'name' => esc_html__('Blog page', 'chap'),
	'desc' => sprintf(
		esc_html__('Enable %1$sblog mode%2$s for the main loop.', 'chap'),
		'<strong>',
		'</strong>'
	),
	'image' => Assets\asset_path('images/options/blog_mode.png'),
	'image_size' => 'tiny',
	'help' => sprintf(
		/* Translators: Semantic UI divider. */
		esc_html__('Blog mode displays a full width featured image and full post content until the %2$s<!--more-->%3$s tag is specified.%1$sThis setting applies to the page specified as "Posts page" in %4$sReading Settings%5$s or front page when it\'s set to "Your latest posts".%1$sIf enabled then the loop settings that follow will be ignored on blog page.', 'chap'),
		'<div class="ui divider"></div>',
		'<code>',
		'</code>',
		'<a href="' . esc_url(admin_url('options-reading.php')) . '">',
		'</a>'
	),
	'type' => 'checkbox',
	'default' => false,
]);

$tab->createOption([
	'id' => 'loop_type',
	'name' => esc_html__('Display type', 'chap'),
	'type' => 'chap-radio-image',
	'options' => [
		'items' => [
			'desc' => esc_html__('Items', 'chap'),
			'image' => Assets\asset_path('images/options/loop_items.png'),
		],
		'grid' => [
			'desc' => esc_html__('Grid', 'chap'),
			'image' => Assets\asset_path('images/options/loop_grid.png'),
		],
		'cards' => [
			'desc' => esc_html__('Cards', 'chap'),
			'image' => Assets\asset_path('images/options/loop_cards.png'),
		],
	],
	'default' => 'items',
]);

$tab->createOption([
	'id' => 'loop_columns',
	'name' => esc_html__('Columns', 'chap'),
	'visible' => [
		'loop_type' => ['grid', 'cards'],
	],
	'type' => 'number',
	'min' => 1,
	'max' => 6,
	'step' => 1,
	'default' => 3,
]);

$tab->createOption([
	'id' => 'loop_grid_justified',
	'name' => esc_html__('Text', 'chap'),
	'desc' => esc_html__('Justified', 'chap'),
	'visible' => [
		'loop_type' => ['grid'],
	],
	'type' => 'checkbox',
	'default' => true,
]);

$tab->createOption([
	'id' => 'loop_spacing',
	'name' => esc_html__('Spacing', 'chap'),
	'help' => esc_html__('Specifies the amount of spacing between loop items.', 'chap'),
	'visible' => [
		'loop_type' => ['items', 'grid'],
	],
	'type' => 'radio',
	'horizontal' => true,
	'options' => [
		'default' => esc_html__('None', 'chap'),
		'relaxed' => esc_html__('Relaxed', 'chap'),
		'very relaxed' => esc_html__('Very relaxed', 'chap'),
	],
	'default' => 'very relaxed',
]);

$tab->createOption([
	'id' => 'loop_padding',
	'name' => esc_html__('Padding', 'chap'),
	'help' => esc_html__('Specifies the amount of padding in grid cells.', 'chap'),
	'visible' => [
		'loop_type' => ['grid'],
	],
	'type' => 'radio',
	'horizontal' => true,
	'options' => [
		'default' => esc_html__('None', 'chap'),
		'padded' => esc_html__('Padded', 'chap'),
		'very padded' => esc_html__('Very padded', 'chap'),
	],
	'default' => 'default',
]);

$tab->createOption([
	'id' => 'loop_separation',
	'name' => esc_html__('Separation', 'chap'),
	'help' => esc_html__('Specifies how to separate loop items.', 'chap') . '<div class="ui divider"></div>' . esc_html__('"Celled" and "Internally celled" options work with grids only.', 'chap'),
	'visible' => [
		'loop_type' => ['items', 'grid'],
	],
	'type' => 'radio',
	'horizontal' => true,
	'options' => [
		'default' => esc_html__('None', 'chap'),
		'divided' => esc_html__('Divided', 'chap'),
		'celled' => esc_html__('Celled', 'chap'),
		'internally celled' => esc_html__('Internally celled', 'chap'),
	],
	'default' => 'divided',
]);

$tab->createOption([
	'name' => esc_html__('Loop item', 'chap'),
	'desc' => esc_html__('Choose what content to display in loop items.', 'chap'),
	'type' => 'heading',
]);

$tab->createOption([
	'id' => 'excerpt_length',
	'name' => esc_html__('Excerpt length', 'chap'),
	'type' => 'number',
	'min' => 0,
	'max' => 500,
	'step' => 1,
	'unit' => esc_html__('words', 'chap'),
	'default' => 55,
]);

$tab->createOption([
	'id' => 'loop_thumbnail_size',
	'name' => esc_html__('Thumbnail size', 'chap'),
	'type' => 'select',
	'options' => Helpers\get_thumbnail_sizes(),
	'default' => 'medium',
]);

$tab->createOption([
	'id' => 'loop_header_size',
	'name' => esc_html__('Header size', 'chap'),
	'type' => 'select',
	'options' => [
		'tiny' => esc_html__('Tiny', 'chap'),
		'small' => esc_html__('Small', 'chap'),
		'medium' => esc_html__('Medium', 'chap'),
		'large' => esc_html__('Large', 'chap'),
		'huge' => esc_html__('Huge', 'chap'),
	],
	'default' => 'medium',
]);

$tab->createOption([
	'id' => 'loop_thumbnail_max_height',
	'name' => esc_html__('Thumbnail max height', 'chap'),
	'visible' => [
		'loop_type' => ['grid', 'cards'],
	],
	'type' => 'number',
	'min' => 0,
	'max' => 50,
	'step' => 1,
	'unit' => 'em',
	'default' => 20,
	'css' => '.cards.loop article a.image, .grid.loop article a.image { max-height: valueem; }',
]);

$tab->createOption([
	'id' => 'loop_render_entry_meta',
	'name' => esc_html__('Show meta', 'chap'),
	'desc' => esc_html__('Display post date and author below title.', 'chap'),
	'type' => 'checkbox',
	'default' => true,
]);

$tab->createOption([
	'id' => 'loop_render_extras',
	'name' => esc_html__('Show tags', 'chap'),
	'desc' => esc_html__('Display categories, tags and sticky post indicator.', 'chap'),
	'type' => 'checkbox',
	'default' => true,
]);

$tab->createOption([
	'id' => 'loop_render_read_more',
	'name' => esc_html__('Show read more', 'chap'),
	'desc' => esc_html__('Display read more link.', 'chap'),
	'type' => 'checkbox',
	'default' => true,
]);

$tab->createOption([
	'name' => esc_html__('Pagination', 'chap'),
	'type' => 'heading',
]);

$tab->createOption([
	'id' => 'pagination_type',
	'name' => esc_html__('Pagination type', 'chap'),
	'type' => 'radio',
	'horizontal' => true,
	'options' => [
		'normal' => esc_html__('Normal', 'chap'),
		'paged' => esc_html__('Paged', 'chap'),
	],
	'default' => 'normal',
]);

$tab->createOption([
	'id' => 'pagination_alignment',
	'name' => esc_html__('Pagination alignment', 'chap'),
	'help' => esc_html__('Applies to pagination of posts, search results, comments, products, etc.', 'chap'),
	'type' => 'radio',
	'horizontal' => true,
	'options' => [
		'left' => esc_html__('Left', 'chap'),
		'center' => esc_html__('Center', 'chap'),
		'right' => esc_html__('Right', 'chap'),
	],
	'default' => 'center',
]);

$tab->createOption([
	'name' => esc_html__('Posts', 'chap'),
	'type' => 'heading',
]);

$tab->createOption([
	'id' => 'order_after_post',
	'name' => esc_html__('After post content', 'chap'),
	'desc' => esc_html__('Reorder or toggle content shown after posts.', 'chap'),
	'type' => 'sortable',
	'options' => [
		'post_pagination' => esc_html('Pagination (for posts with multiple pages)'),
		'post_meta' => esc_html('Post meta (categories and tags)'),
		'post_author' => esc_html('Post author info'),
		'after_post_content' => esc_html('After post content (editable in the "Code" tab)'),
		'post_navigation' => esc_html('Post navigation (links to previous/next posts)'),
		'post_comments' => esc_html('Comments'),
	],
	'default' => [
		'post_pagination',
		'post_meta',
		'post_author',
		'after_post_content',
		'post_navigation',
		'post_comments',
	],
]);

$tab->createOption([
	'name' => esc_html__('Pages', 'chap'),
	'type' => 'heading',
]);

$tab->createOption([
	'id' => 'order_after_page',
	'name' => esc_html__('After page content', 'chap'),
	'desc' => esc_html__('Reorder or toggle content shown after pages.', 'chap'),
	'type' => 'sortable',
	'options' => [
		'post_pagination' => esc_html('Pagination (for posts with multiple pages)'),
		'after_page_content' => esc_html('After page content (editable in the "Code" tab)'),
		'page_comments' => esc_html('Comments'),
	],
	'default' => [
		'post_pagination',
		'after_page_content',
		'page_comments',
	],
]);

$tab->createOption([
	'name' => esc_html__('Widgets', 'chap'),
	'type' => 'heading',
]);

$break_headers = <<<CSS
.sidebar-widget,
.footer-widget {
	& > .header {
		word-wrap: break-word;
		word-break: break-all;
	}
}
CSS;

$tab->createOption([
	'id' => 'break_widget_headers',
	'name' => esc_html__('Widget headers', 'chap'),
	'desc' => esc_html__('Break long words in widget headers.', 'chap'),
	'type' => 'checkbox',
	'default' => false,
	'css' => $break_headers,
]);

$tab->createOption([
	'id' => 'header_widgets_button_size',
	'name' => esc_html__('Header widget buttons', 'chap'),
	'desc' => esc_html__('A normalized size for all buttons in the header widget area.', 'chap'),
	'type' => 'number',
	'min' => 0.5,
	'max' => 1.5,
	'step' => 0.05,
	'unit' => 'rem',
	'default' => 0.8,
	'css' => '.header-widgets.menu .ui.button { font-size: valuerem!important; }',
]);

$tab->createOption([
	'id' => 'widget_tag_cloud_tag_size',
	'name' => esc_html__('Tag cloud tag size', 'chap'),
	'type' => 'select',
	'options' => [
		'auto' => esc_html__('Auto', 'chap'),
		'mini' => esc_html__('Mini', 'chap'),
		'tiny' => esc_html__('Tiny', 'chap'),
		'small' => esc_html__('Small', 'chap'),
		'medium' => esc_html__('Medium', 'chap'),
		'large' => esc_html__('Large', 'chap'),
		'big' => esc_html__('Big', 'chap'),
		'huge' => esc_html__('Huge', 'chap'),
		'massive' => esc_html__('Massive', 'chap'),
	],
	'default' => 'auto',
]);

$tab->createOption([
	'id' => 'widget_tag_cloud_tag_color',
	'name' => esc_html__('Tag cloud tag color', 'chap'),
	'type' => 'select',
	'options' => [
		'default' => esc_html__('Default', 'chap'),
		'red' => esc_html__('Red', 'chap'),
		'orange' => esc_html__('Orange', 'chap'),
		'yellow' => esc_html__('Yellow', 'chap'),
		'olive' => esc_html__('Olive', 'chap'),
		'green' => esc_html__('Green', 'chap'),
		'teal' => esc_html__('Teal', 'chap'),
		'blue' => esc_html__('Blue', 'chap'),
		'violet' => esc_html__('Violet', 'chap'),
		'purple' => esc_html__('Purple', 'chap'),
		'pink' => esc_html__('Pink', 'chap'),
		'brown' => esc_html__('Brown', 'chap'),
		'grey' => esc_html__('Grey', 'chap'),
		'black' => esc_html__('Black', 'chap'),
	],
	'default' => 'default',
]);

$tab->createOption([
	'name' => esc_html__('Loading screen', 'chap'),
	'type' => 'heading',
]);

$tab->createOption([
	'id' => 'enable_loading_screen',
	'name' => esc_html__('Enable', 'chap'),
	'desc' => esc_html__('Show a loading screen until all page content is loaded.', 'chap'),
	'type' => 'checkbox',
	'default' => false,
]);

$tab->createOption([
	'id' => 'enable_loading_front_page_only',
	'name' => esc_html__('Front page', 'chap'),
	'desc' => esc_html__('Show the loading screen only on front page.', 'chap'),
	'type' => 'checkbox',
	'default' => false,
]);

$tab->createOption([
	'id' => 'enable_loading_screen_brand',
	'name' => esc_html__('Logo', 'chap'),
	'desc' => esc_html__('Show brand logo image on the loading screen.', 'chap'),
	'type' => 'checkbox',
	'default' => true,
]);

$tab->createOption([
	'id' => 'loading_screen_loader_color',
	'name' => esc_html__('Loader color', 'chap'),
	'desc' => esc_html__('The color of the loading indicator.', 'chap'),
	'type' => 'color',
	'default' => '#ffffff',
]);

$tab->createOption([
	'id' => 'loading_screen_background_color',
	'name' => esc_html__('Background color', 'chap'),
	'desc' => esc_html__('The background color of the loading screen.', 'chap'),
	'type' => 'color',
	'default' => '#1b1c1d',
]);

$tab->createOption([
	'id' => 'loading_screen_delay',
	'name' => esc_html__('Delay', 'chap'),
	'desc' => sprintf(
		/* Translators: HTML code tags. */
		esc_html__('Delay the removal of loading screen to leave other actions attached to %1$sDOMContentLoaded%2$s event time to execute.', 'chap'),
		'<code>',
		'</code>'
	),
	'type' => 'number',
	'min' => 0,
	'max' => 2500,
	'step' => 50,
	'unit' => 'ms',
	'default' => 100,
]);

$tab->createOption([
	'name' => esc_html__('Miscellaneous', 'chap'),
	'type' => 'heading',
]);

$tab->createOption([
	'id' => 'outdated_browser_warning',
	'name' => esc_html__('Outdated browser warning', 'chap'),
	'desc' => esc_html__("Show a warning when user's browser isn't compatible with this theme.", 'chap'),
	'type' => 'checkbox',
	'default' => true,
]);

$tab->createOption([
	'id' => 'default_editor',
	'name' => esc_html__('Default editor mode', 'chap'),
	'help' => esc_html__('This theme typically uses a lot shortcodes in post content and they are indented to keep them easily readable.', 'chap') .
		'<div class="ui divider"></div>' .
		esc_html__('The Visual editor mode removes indentation from shortcodes and therefore is not recommended.', 'chap'),
	'image' => Assets\asset_path('images/options/editor_text_mode.png'),
	'type' => 'radio',
	'horizontal' => true,
	'options' => [
		'tinymce' => esc_html__('Visual', 'chap'),
		'html' => esc_html__('Text', 'chap'),
	],
	'default' => 'html',
]);

$tab->createOption([
	'id' => 'hide_custom_fields',
	'name' => esc_html__('Hide custom fields', 'chap'),
	'desc' => esc_html__('Hide custom metabox fields below all post types (custom metaboxes are for advanced users).', 'chap'),
	'type' => 'checkbox',
	'default' => true,
]);

$tab->createOption([
	'id' => 'enable_aos',
	'name' => esc_html__('Enable AOS', 'chap'),
	'desc' => sprintf(
		/* Translators: HTML anchor tag to animation documantation. */
		esc_html__('Enable Animate on Scroll library. This allows to %1$sanimate content%2$s when user scrolls down the page.', 'chap'),
		'<a href="https://chap.website/animation/" target="_blank" rel="noopener">',
		'</a>'
	),
	'type' => 'checkbox',
	'default' => false,
]);

$tab->createOption([
	'id' => 'show_chap_icon',
	'name' => esc_html__('Show Chap icon', 'chap'),
	'desc' => esc_html__('Display a link to Chap settings in the WordPress admin bar.', 'chap'),
	'type' => 'checkbox',
	'default' => true,
]);

$tab->createOption([
	'id' => 'relocate_customize_link',
	'name' => esc_html__('Relocate Customize', 'chap'),
	'desc' => esc_html__('Relocate the "Customize" link in the WordPress admin bar to the "Site name" dropdown menu to conserve space. Customizer is not used to customize this theme.', 'chap'),
	'type' => 'checkbox',
	'default' => true,
]);

$tab->createOption([
	'id' => 'disable_update_autocompile',
	'name' => esc_html__('Disable autocompile', 'chap'),
	'desc' => esc_html__("Don't automatically recompile Semantic UI when theme version is changed.", 'chap'),
	'type' => 'checkbox',
	'default' => false,
]);

if(class_exists('WP_Recipe_Maker')):
	$tab->createOption([
		'id' => 'disable_wprm_styles',
		'name' => esc_html__('Disable WPRM styles', 'chap'),
		'desc' => esc_html__("Don't enqueue WordPress Recipe Maker plugin's main stylesheet (the stylesheet is not needed when only using Chap WPRM templates for recipes).", 'chap'),
		'type' => 'checkbox',
		'default' => false,
	]);
endif;

$tab->createOption([
	'name' => esc_html__('Plugins', 'chap'),
	'desc' => esc_html__('Disabling a plugin allows you to manually remove it and be no longer notified about installing it as a required plugin.', 'chap'),
	'type' => 'heading',
]);

$tab->createOption([
	'id' => 'disable_ocdi',
	'name' => esc_html__('Disable OCDI', 'chap'),
	'desc' => esc_html__('Disable the One Click Demo Import plugin, used for demo importing.', 'chap'),
	'type' => 'checkbox',
	'default' => false,
]);

$tab->createOption([
	'id' => 'disable_envato_market',
	'name' => esc_html__('Disable Envato Market', 'chap'),
	'desc' => esc_html__('Disable the Envato Market plugin, used for theme updates.', 'chap'),
	'type' => 'checkbox',
	'default' => false,
]);

$tab->createOption([
	'id' => 'disable_chap_shortcodes',
	'name' => esc_html__('Disable Chap Shortcodes', 'chap'),
	'desc' => esc_html__('Disable the Chap Shortcodes plugin (not recommended, this plugin is designed as a core part of this theme).', 'chap'),
	'type' => 'checkbox',
	'default' => false,
]);

$tab->createOption(['type' => 'save']);

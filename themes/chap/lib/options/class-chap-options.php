<?php

namespace Chap\Options;
use Chap\Assets;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

/**
 * Chap_Options class handles Chap theme options.
 *
 * @class   Chap_Options
 * @version 1.0.2
 * @since   1.3.0
 * @author  websevendev <websevendev@gmail.com>
 */
class Chap_Options {

	/**
	 * Initialize Chap_Options class.
	 *
	 * @since 1.0.0
	 */
	public static function init() {

		/**
		 * Load files.
		 */
		add_action('after_setup_theme', [__CLASS__, 'load']);

		/**
		 * Admin bar modifications.
		 *
		 * @since 1.0.1
		 */
		add_action('admin_bar_menu', [__CLASS__, 'chap_admin_bar_menu_item'], 35);
		add_action('wp_enqueue_scripts', [__CLASS__, 'chap_admin_bar_menu_item_css']);
		add_action('admin_enqueue_scripts', [__CLASS__, 'chap_admin_bar_menu_item_css']);

		add_action('admin_bar_menu', [__CLASS__, 'chap_admin_bar_amp_menu_item'], 40);
		add_action('wp_enqueue_scripts', [__CLASS__, 'chap_admin_bar_amp_menu_item_css']);

		add_action('wp_before_admin_bar_render', [__CLASS__, 'relocate_customize_link']);

		/**
		 * Show notices.
		 */
		add_action('ctf_admin_page_table_start_' . CHAP_TF, [__CLASS__, 'chap_ocdi_import_message']);
		add_action('admin_notices', [__CLASS__, 'chap_tf_sass_error_message']);

		/**
		 * Trigger Semantic UI compilation after saving theme options.
		 */
		add_action('admin_init', [__CLASS__, 'chap_sui_compile_js']);
		add_action('ctf_admin_page_table_start_' . CHAP_TF, [__CLASS__, 'chap_sui_compile_ui']);

		/**
		 * Filter Google Font url.
		 */
		add_filter('ctf_enqueue_google_webfont_' . CHAP_TF, [__CLASS__, 'chap_google_fonts_variants'], 10, 2);

		/**
		 * Update "last saved" version after saving options.
		 */
		add_filter('ctf_admin_options_saved_' . CHAP_TF, [__CLASS__, 'update_version']);

	}

	/**
	 * Load Titan Framework admin panel, tabs, options, metaboxes.
	 */
	public static function load() {

		if(!class_exists('ChapTitanFramework')) {
			return;
		}

		/**
		 * Get the Options Framework instance.
		 */
		$titan = self::get_titan();

		/**
		 * Create a Menu item in the Admin panel.
		 */
		$panel = $titan->createAdminPanel([
			'id' => 'chap-settings',
			'name' => esc_html__('Chap Theme', 'chap'),
			'icon' => 'none',
		]);

		/**
		 * Load all controls, tabs and metaboxes.
		 *
		 * Note: Tab file names are prefixed with a number to load them in
		 * a logical order for displaying. However, the order matters for
		 * generating custom CSS in options as well. CSS variables become
		 * available only after their related option is defined. For example
		 * in tab-07-slider we can use $main_menu_bottom_margin that is defined
		 * in tab-04-menus but we can't use $slider_container_top_margin in menus tab.
		 */
		foreach(Assets\theme_files('lib/options', '{control,tab,metabox}-*.php') as $file) {
			include_once $file;
		}

	}

	/**
	 * Returns the Titan Framework instance.
	 */
	public static function get_titan() {
		if(!class_exists('ChapTitanFramework')) {
			return false;
		}
		return \ChapTitanFramework::getInstance(CHAP_TF);
	}

	/**
	 * Add an admin bar menu item linking to Chap settings page.
	 */
	public static function chap_admin_bar_menu_item() {
		if(!get('show_chap_icon')) {
			return;
		}

		global $wp_admin_bar;
		$wp_admin_bar->add_menu([
			'id' => 'chap-settings',
			'title' => esc_html__('Chap', 'chap'),
			'href' => admin_url('admin.php?page=chap-settings'),
		]);
	}

	/**
	 * Add CSS that displays the Chap admin bar menu item icon.
	 *
	 * @since 1.0.1
	 */
	public static function chap_admin_bar_menu_item_css() {
		if(!get('show_chap_icon')) {
			return;
		}

		$chap_item = Assets\asset_path('images/chap-menu-item.png');
		$chap_item_hover = Assets\asset_path('images/chap-menu-item-hover.png');

		$left = is_rtl() ? '3px' : '-7px';
		$css = <<<CSS
#wp-admin-bar-chap-settings .ab-item:before {
	content: url("{$chap_item}");
	top: -4px;
	left: {$left};
	width: 28px;
	margin-right: 0;
}
#wp-admin-bar-chap-settings .ab-item:hover:before,
#wp-admin-bar-chap-settings .ab-item:focus:before,
#wp-admin-bar-chap-settings .ab-item:active:before {
	content: url("{$chap_item_hover}");
}
CSS;
		wp_add_inline_style('admin-bar', $css);
	}

	/**
	 * Add an admin bar menu item linking to AMP page.
	 *
	 * @since 1.0.2
	 */
	public static function chap_admin_bar_amp_menu_item() {
		if(is_admin()) {
			return;
		}
		if(!get('amp_enabled')) {
			return;
		}
		if(!get('amp_show_admin_link')) {
			return;
		}

		global $wp_admin_bar;
		$wp_admin_bar->add_menu([
			'id' => 'chap-amp-link',
			'title' => '',
			'href' => add_query_arg(AMP_QUERY_VAR, 1),
		]);
	}

	/**
	 * Add CSS that displays the AMP menu item icon.
	 *
	 * @since 1.0.2
	 */
	public static function chap_admin_bar_amp_menu_item_css() {
		if(is_admin()) {
			return;
		}
		if(!get('amp_enabled')) {
			return;
		}
		if(!get('amp_show_admin_link')) {
			return;
		}

		$amp_item = Assets\asset_path('images/chap-amp-menu-item-gray.png');
		$amp_item_hover = Assets\asset_path('images/chap-amp-menu-item-gray-hover.png');

		$left = is_rtl() ? '3px' : '-7px';
		$css = <<<CSS
#wp-admin-bar-chap-amp-link .ab-item:before {
	content: url("{$amp_item}");
	top: -5px;
	left: {$left};
	width: 21px;
	margin-right: 0;
}
#wp-admin-bar-chap-amp-link .ab-item:hover:before,
#wp-admin-bar-chap-amp-link .ab-item:focus:before,
#wp-admin-bar-chap-amp-link .ab-item:active:before {
	content: url("{$amp_item_hover}");
}
CSS;
		wp_add_inline_style('admin-bar', $css);
	}

	/**
	 * Move the "Customize" link in WP Admin Bar to a submenu.
	 *
	 * @since 1.0.1
	 */
	public static function relocate_customize_link() {
		if(!get('relocate_customize_link')) {
			return;
		}

		global $wp;
		global $wp_admin_bar;
		$current_url = home_url(add_query_arg([], $wp->request));
		$wp_admin_bar->remove_menu('customize');
		$wp_admin_bar->add_node([
			'id' => 'chap-customize-relocated',
			'title' => esc_html__('Customize', 'chap'),
			'parent' => 'site-name',
			'href' => add_query_arg('url', urlencode(esc_url($current_url)), admin_url('customize.php')),
		]);
	}

	/**
	 * Determine if Semantic UI should be recompiled.
	 */
	public static function should_compile() {
		$tab = isset($_GET['tab']) ? $_GET['tab'] : false;
		$message = isset($_GET['message']) ? $_GET['message'] : false;

		if(in_array($tab, ['styles', 'themes', 'sui', 'presets']) && in_array($message, ['saved', 'reset'])) {
			return self::get_titan()->getOption('sui_recompile_' . $tab);
		}

		return false;
	}

	/**
	 * Add JavaScript that triggers Semantic UI compilation.
	 */
	public static function chap_sui_compile_js() {
		if(self::should_compile()) {
			/**
			 * Trigger the recompile function.
			 */
			wp_add_inline_script('chap/admin', 'jQuery(document).ready(function(){chap_compile_sui();});');
		}
	}

	/**
	 * Render admin notice about Semantic UI compilation.
	 */
	public static function chap_sui_compile_ui() {
		if(!self::should_compile()) {
			return;
		}
		echo get_chap_sui_compile_ui();
	}

	/**
	 * Render admin notice about OCDI import.
	 */
	public static function chap_ocdi_import_message() {
		if(!get_option('chap_did_ocdi_import') || isset($_POST['chap_ocdi'])) {
			return;
		}

		update_option('chap_did_ocdi_import', 0);

		?>
		<div class="notice notice-success chap-ocdi">
			<p>
				<strong><?php esc_html_e('Demo content imported!', 'chap'); ?></strong><br />
				<?php esc_html_e('You have been redirected here to automatically recompile Semantic UI using the new theme options.', 'chap'); ?><br />
				<span class="chap-before-compilation"><?php esc_html_e('Your website is ready once the compilation finishes.', 'chap'); ?></span>
				<span class="chap-after-compilation"><a class="button button-large button-primary" href="<?php echo esc_url(home_url()); ?>"><?php esc_html_e('View your new website', 'chap'); ?></a></span>
			</p>
		</div>
		<?php
	}

	/**
	 * Display SASS compilation error.
	 *
	 * @since 1.0.1
	 */
	public static function chap_tf_sass_error_message() {
		$tab = isset($_GET['tab']) ? $_GET['tab'] : false;
		$message = isset($_GET['message']) ? $_GET['message'] : false;
		if(!in_array($message, ['saved', 'reset'])) {
			return;
		}
		$error = get_option('chap_tf_sass_error');
		if(!$error) {
			return;
		}
		?>
		<div class="notice notice-error notice-sass is-dismissible">
			<div class="chap-message-container">
				<strong><?php esc_html_e('Compiling styles based on theme options produced an error:', 'chap'); ?></strong>
				<pre><?php echo esc_html($error); ?></pre>
			</div>
		</div>
		<?php
		update_option('chap_tf_sass_error', false);
	}

	/**
	 * Add Semantic UI Google Fonts variants.
	 */
	public static function chap_google_fonts_variants($font_url, $font_name) {

		/**
		 * Default SUI font variants.
		 */
		$variants = [
			'400',
			'700',
			'400italic',
			'700italic',
		];

		/**
		 * Split the variants from the URL.
		 */
		list($left, $right) = explode(':', $font_url);
		list($initial_variants, $subsets) = explode('&', $right);

		$initial_variants = explode(',', $initial_variants);
		$variants = array_merge($initial_variants, $variants);
		$variants = array_unique($variants);

		/**
		 * Add slide header font weight.
		 */
		$slide_header_font_weight = get('sui_slide_header_font_weight');
		if(!in_array($slide_header_font_weight, $variants)) {
			$variants[] = $slide_header_font_weight;
			$variants[] = $slide_header_font_weight . 'italic';
		}

		$subsets = explode(',', $subsets);
		$subsets = apply_filters('chap_google_fonts_subsets', $subsets, $font_name);
		$variants = apply_filters('chap_google_fonts_variants', $variants, $font_name);

		/**
		 * Compose new url with our variants.
		 */
		$font_url = $left . ':' . join(',', $variants) . '&' . join(',', $subsets);

		return $font_url;

	}

	/**
	 * Update the version of Chap theme at last TF settings save.
	 */
	public static function update_version() {
		$theme = wp_get_theme('chap');
		$ver = $theme['Version'];
		update_option('chap_last_saved_tf', $ver);
	}

}

/**
 * Titan Framework namespace.
 * Will consist of theme folder name and multisite ID.
 */
if(!defined('CHAP_TF')) {
	define('CHAP_TF', get_option('stylesheet') . '-' . get_current_blog_id());
}

/**
 * Function to get Titan Framework option values.
 */
function get($option, $post_id = null) {
	if(!class_exists('ChapTitanFramework')) {
		return;
	}
	$value = \ChapTitanFramework::getInstance(CHAP_TF)->getOption($option, $post_id);
	return apply_filters('chap_option_' . $option, $value, $post_id);
}

/**
 * Returns the UI that shows progress about compiling Semantic UI.
 *
 * @return string
 */
function get_chap_sui_compile_ui() {
	ob_start(); ?>
	<div class="notice notice-info chap-compiler is-dismissible">
		<div class="chap-message-container">
			<strong id="chap-message"><?php esc_html_e('Recompiling Semantic UI...', 'chap'); ?></strong>
			<div id="current-file"></div>
		</div>
		<div class="chap-progress">
			<span class="done"></span>
		</div>
	</div>
	<?php return ob_get_clean();
}

<?php

namespace Chap\Options;
use Chap\Assets;
use Chap\Helpers;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

$tab = $panel->createTab(['name' => esc_html__('Footer', 'chap')]);

$tab->createOption([
	'name' => esc_html__('Footer settings', 'chap'),
	'type' => 'heading',
]);

$tab->createOption([
	'id' => 'footer_inverted',
	'name' => esc_html__('Inverted', 'chap'),
	'desc' => esc_html__('Invert footer colors.', 'chap'),
	'help' => esc_html__('Inverted colors stand out better on darker backgrounds.', 'chap'),
	'type' => 'checkbox',
	'default' => true,
]);

$tab->createOption([
	'id' => 'footer',
	'name' => esc_html__('Background', 'chap'),
	'type' => 'background',
	'options' => [
		'color' => [
			'alpha' => true,
			'default' => '#1b1c1d',
		],
		'color_2' => [
			'alpha' => true,
		],
		'opacity' => [
			'default' => 0.5,
		],
	],
	'selector' => '.full.height > .ui.footer.segment',
]);

$tab->createOption([
	'id' => 'footer_box',
	'name' => esc_html__('Box model', 'chap'),
	'image' => Assets\asset_path('images/options/box_model_footer.png'),
	'type' => 'box',
	'box' => [
		'margin',
		'border',
		'padding',
	],
	'selector' => '.full.height > .ui.footer.segment',
	'default' => [
		'padding' => [
			'top' => [
				'value' => 5,
				'units' => 'em',
			],
			'bottom' => [
				'value' => 5,
				'units' => 'em',
			],
		],
	],
]);

$tab->createOption([
	'id' => 'footer_top_angle',
	'name' => esc_html__('Top angle', 'chap'),
	'desc' => esc_html__('This option allows the footer to have an angled edge on the top.', 'chap'),
	'image' => Assets\asset_path('images/options/footer_angle.png'),
	'type' => 'number',
	'min' => -24,
	'max' => 24,
	'step' => 1,
	'default' => 0,
]);

$tab->createOption([
	'name' => esc_html__('Footer widgets', 'chap'),
	'type' => 'heading',
]);

$tab->createOption([
	'id' => 'footer_alignment',
	'name' => esc_html__('Alignment', 'chap'),
	'type' => 'radio',
	'horizontal' => true,
	'options' => [
		'left' => esc_html__('Left', 'chap'),
		'center' => esc_html__('Center', 'chap'),
		'right' => esc_html__('Right', 'chap'),
	],
	'default' => 'center',
]);

$tab->createOption([
	'id' => 'footer_widgets_spacing',
	'name' => esc_html__('Spacing', 'chap'),
	'type' => 'radio',
	'horizontal' => true,
	'options' => [
		'default' => esc_html__('None', 'chap'),
		'relaxed' => esc_html__('Relaxed', 'chap'),
		'very relaxed' => esc_html__('Very relaxed', 'chap'),
	],
	'default' => 'very relaxed',
]);

$tab->createOption([
	'id' => 'footer_widgets_separation',
	'name' => esc_html__('Separation', 'chap'),
	'type' => 'radio',
	'horizontal' => true,
	'options' => [
		'default' => esc_html__('None', 'chap'),
		'divided' => esc_html__('Divided', 'chap'),
		'celled' => esc_html__('Celled', 'chap'),
		'internally celled' => esc_html__('Internally celled', 'chap'),
	],
	'default' => 'divided',
]);

$tab->createOption([
	'id' => 'footer_widget_rows',
	'name' => esc_html__('Rows', 'chap'),
	'desc' => esc_html__('Comma separated numbers after which widgets a new row should be created.', 'chap'),
	'example' => esc_html__('Example', 'chap') . ': 2,4,6 - ' . esc_html__('will create a new row after the 2nd, 4th and 6th widget.', 'chap'),
	'type' => 'text',
	'default' => '',
]);

$tab->createOption([
	'name' => esc_html__('Columns', 'chap'),
	'type' => 'custom',
	'custom' => sprintf(
		esc_html__('Widget columns are equal width by default, but can be customized by using a third party plugin, %1$sread more%2$s.', 'chap'),
		'<a href="' . Helpers\hc_link('chap.website/customizing-footer-columns/') . '" target="_blank" rel="noopener">',
		'</a>'
	),
]);

$tab->createOption([
	'name' => esc_html__('Footer image', 'chap'),
	'desc' => esc_html__('Small image displayed between the footer widgets and footer menu/text.', 'chap'),
	'type' => 'heading',
]);

$tab->createOption([
	'id' => 'footer_image',
	'name' => esc_html__('Image', 'chap'),
	'type' => 'upload',
	'size' => 'small',
]);

$tab->createOption([
	'id' => 'footer_image_size',
	'name' => esc_html__('Size', 'chap'),
	'type' => 'select',
	'options' => [
		'mini' => esc_html__('Mini', 'chap'),
		'small' => esc_html__('Small', 'chap'),
		'medium' => esc_html__('Medium', 'chap'),
	],
	'default' => 'mini',
]);

$tab->createOption([
	'id' => 'footer_image_link',
	'name' => esc_html__('Link', 'chap'),
	'type' => 'text',
	'default' => get_home_url(),
]);

$tab->createOption([
	'name' => esc_html__('Custom footer content', 'chap'),
	'type' => 'heading',
]);

global $plugin_chap_shortcodes;
$shortcodes_loaded = !empty($plugin_chap_shortcodes) ? true : false;
if($shortcodes_loaded) {
	$footer_text_default = sprintf(
		/* Translators: HTML anchor tag to Chap website. */
		'%1$s' . "\n" . '  [item]' . esc_html__('Powered by %2$sChap%3$s WordPress theme.', 'chap') . '[/item]' . "\n" . '%4$s',
		'[list small vertical inverted link]',
		'<a href="https://chap.website" target="_blank" rel="noopener">',
		'</a>',
		'[/list]'
	);
} else {
	$footer_text_default = sprintf(
		/* Translators: HTML anchor tag to Chap website. */
		'%1s' . "\n" . '  <div class="item">' . esc_html__('Powered by %2$sChap%3$s WordPress theme.', 'chap') . '</div>' . "\n" . '%4$s',
		'<div class="ui small vertical inverted link list">',
		'<a href="https://chap.website" target="_blank" rel="noopener">',
		'</a>',
		'</div>'
	);
}

$tab->createOption([
	'id' => 'footer_text',
	'name' => esc_html__('Footer text content', 'chap'),
	'desc' => esc_html__('Text displayed at the bottom of the footer.', 'chap'),
	'type' => 'editor',
	'wpautop' => false,
	'default' => $footer_text_default,
]);

$tab->createOption([
	'id' => 'footer_text_alignment',
	'name' => esc_html__('Footer text content alignment', 'chap'),
	'type' => 'radio',
	'horizontal' => true,
	'options' => [
		'left' => esc_html__('Left', 'chap'),
		'center' => esc_html__('Center', 'chap'),
		'right' => esc_html__('Right', 'chap'),
	],
	'default' => 'center',
]);

$tab->createOption(['type' => 'save']);

/**
 * Create CSS for the footer based on options.
 */
function chap_tf_footer_css() {
	$titan = \ChapTitanFramework::getInstance(CHAP_TF);
	$angle = round(intval($titan->getOption('footer_top_angle')));
	if($angle != 0) {
		$clip_left = $angle < 0 ? abs($angle) / 2 : 0;
		$clip_right = $angle > 0 ? $angle / 2 : 0;
		$titan->createCSS('
			.ui.footer.segment {
				clip-path: polygon(
					0 calc(0% + ' . $clip_left . 'vw),
					100% calc(0% + ' . $clip_right . 'vw),
					100% 100%,
					0 100%
				);
			}
		');
	}
}
add_filter('customize_save_after',               __NAMESPACE__ . '\\chap_tf_footer_css', 6);
add_filter('ctf_admin_options_saved_' . CHAP_TF, __NAMESPACE__ . '\\chap_tf_footer_css', 6);

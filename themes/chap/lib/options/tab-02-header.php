<?php

namespace Chap\Options;
use Chap\Assets;
use Chap\Helpers;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

$tab = $panel->createTab(['name' => esc_html__('Header', 'chap')]);

$tab->createOption([
	'name' => esc_html__('Header template', 'chap'),
	'desc' => sprintf(
		/* Translators: HTML anchor tag to info about header templates. */
		esc_html__('Choose a default header template. %1$sClick here%2$s for more info about header templates and how add custom templates.', 'chap'),
		'<a href="https://chap.website/custom-header-templates" target="_blank" rel="noopener">',
		'</a>'
	),
	'type' => 'heading',
]);

$header_templates = Helpers\get_header_templates();
$header_template_notes = [
	'header-centered-menu' => sprintf(
		esc_html__('This header can\'t leave much room for menus with lots of items. %1$sThe brand and header widgets containers on the either side of the menu have been allocated an equal static width. %1$sThe width can be changed in the %4$sSUI%5$s tab by editing %2$s@chapHeaderCenteredMenuSideColumnsWidth%3$s variable in the %4$sChap - Core%5$s component.', 'chap'),
		'<div class="ui divider"></div>',
		'<code>',
		'</code>',
		'<strong>',
		'</strong>'
	),
	'header-grid' => sprintf(
		esc_html__('For this header the brand container is allocated a static width.%1$s The width can be changed in the %4$sSUI%5$s tab by editing %2$s@chapHeaderGridBrandContainerWidth%3$s variable in the %4$sChap - Core%5$s component.', 'chap'),
		'<div class="ui divider"></div>',
		'<code>',
		'</code>',
		'<strong>',
		'</strong>'
	),
];
$header_template_images = [];
foreach($header_templates as $slug => $name) {
	if(!in_array($slug, [
		'header-branded',
		'header-branded-compact-menu',
		'header-centered-brand',
		'header-centered-menu',
		'header-grid',
		'header-landing',
		'header-landing-compact-menu',
		'header-landing-centered-menu',
		'header-landing-right-menu',
		'header-landing-widgets',
	])) {
		$image = Assets\asset_path('images/header-templates-2/header-none.png');
	} else {
		$image = Assets\asset_path('images/header-templates-2/' . $slug . '.png');
	}
	$header_template_images[$slug] = [
		'desc' => $name,
		'image' => $image,
		'help' => isset($header_template_notes[$slug]) ? $header_template_notes[$slug] : '',
	];
}

$tab->createOption([
	'id' => 'header_default_template',
	'name' => esc_html__('Default header template', 'chap'),
	'desc' => sprintf(
		esc_html__('%1$s%2$sPlease note%3$s: the amount of menu items a header can contain varies based on the chosen template and menu size. Even if all items fit on widescreen monitor, please resize the window to make sure they fit on smaller resolutions as well, before mobile menu kicks in. %1$sThe breakpoint at which mobile menu is triggered can be edited in the %2$sSUI%3$s tab by changing the %4$s@chapMobileMenuBreakpoint%5$s variable in the %2$sChap - Core%3$s component. Overflow behaviour can be configured in the %2$sMenus%3$s tab.', 'chap'),
		'<br />',
		'<strong>',
		'</strong>',
		'<code>',
		'</code>'
	),
	'help' => sprintf(
		esc_html__('
			Header templates are template files that define the look of the header.
			%1$sHeader template files can be copied to the child theme folder to make manual changes in they don\'t meet your design requirements.
			%1$sPlease feel free to contact support if you need help achieving a specific custom result.
		', 'chap'),
		'<div class="ui divider"></div>'
	),
	'type' => 'chap-radio-image',
	'hide_others' => true,
	'options' => $header_template_images,
	'default' => 'header-branded',
]);

$tab->createOption([
	'name' => esc_html__('Header settings', 'chap'),
	'type' => 'heading',
]);

$tab->createOption([
	'id' => 'header_inverted',
	'name' => esc_html__('Inverted', 'chap'),
	'desc' => esc_html__('Invert header colors.', 'chap'),
	'help' => sprintf(
		esc_html__('
			Inverted colors stand out better on darker backgrounds.
			%1$sThis option applies to general things displayed in the header, such as masthead title and text in header widgets.
			%1$sBrand and main menu have their separate options for inverting the colors.
		', 'chap'),
		'<div class="ui divider"></div>'
	),
	'type' => 'checkbox',
	'default' => true,
]);

$tab->createOption([
	'id' => 'header',
	'name' => esc_html__('Background', 'chap'),
	'type' => 'background',
	'options' => [
		'color' => [
			'alpha' => true,
			'default' => '#1b1c1d',
		],
		'color_2' => [
			'alpha' => true,
		],
		'opacity' => [
			'default' => 0.5,
		],
	],
	'selector' => '.full.height > .ui.masthead.segment',
]);

$tab->createOption([
	'id' => 'header_background_full_size',
	'name' => esc_html__('Background image file', 'chap'),
	'help' => sprintf(
		esc_html__('Default background image size is "large" (maximum %2$s1024x1024%3$s).', 'chap') . '%1$s' .
		esc_html__('Enabling this option uses the full native image size.', 'chap') . '%1$s' .
		esc_html__('This option is disabled by default to discourage unknowingly using too large images like %2$s5200x4000%3$s that slow down page loading.', 'chap') . '%1$s' .
		esc_html__('Before enabling this option make sure the image size is something managable like %2$s1920x1080%3$s or %2$s1280x720%3$s depending on the quality you need.', 'chap') . '%1$s' .
		esc_html__('Image size can be altered in the %4$sWordPress Media Library%5$s.', 'chap'),
		'<div class="ui divider"></div>',
		'<code>',
		'</code>',
		'<a href="' . esc_url(admin_url('upload.php')) . '" target="_blank" rel="noopener">',
		'</a>'
	),
	'desc' => esc_html__('Use full size image file.', 'chap'),
	'type' => 'checkbox',
	'default' => false,
]);

/**
 * Deprecated option.
 */
$tab->createOption([
	'id' => 'header_remove_bottom_border',
	'name' => esc_html__('Bottom border', 'chap'),
	'desc' => esc_html__('Remove masthead bottom border.', 'chap'),
	'type' => 'checkbox',
	'default' => false,
	'hidden' => true,
]);

/**
 * Replaces header_remove_bottom_border.
 */
$tab->createOption([
	'id' => 'header_box',
	'name' => esc_html__('Box model', 'chap'),
	'image' => Assets\asset_path('images/options/box_model_header.png'),
	'type' => 'box',
	'box' => [
		'margin',
		'border',
		'padding',
	],
	'selector' => '.full.height > .ui.masthead.segment',
	'css' => '.full.height > .ui.masthead.segment + main:not(.column) { margin-top: -#{$marginBottom}; }',
	'default' => [
		'margin' => [
			'bottom' => [
				'value' => 3,
				'units' => 'rem',
			],
		],
		'border' => [
			'bottom' => get('header_remove_bottom_border') ? [] : [
				'value' => 1,
				'units' => 'px',
				'style' => 'solid',
				'color' => 'rgba(34, 36, 38, .15)',
			],
		],
		'padding' => [
			'top' => [
				'value' => 1,
				'units' => 'em',
			],
		],
	],
]);

$tab->createOption([
	'id' => 'header_bottom_angle',
	'name' => esc_html__('Bottom angle', 'chap'),
	'desc' => esc_html__('This option allows the header to have an angled edge on the bottom.', 'chap'),
	'image' => Assets\asset_path('images/options/header_angle.png'),
	'type' => 'number',
	'min' => -24,
	'max' => 24,
	'step' => 1,
	'default' => 0,
]);

$tab->createOption([
	'name' => esc_html__('Header background video', 'chap'),
	'type' => 'heading',
]);

$tab->createOption([
	'id' => 'header_background_video',
	'name' => esc_html__('Video', 'chap'),
	'desc' => esc_html__('Supported formats: mp4, webm', 'chap'),
	'type' => 'file',
]);

$tab->createOption([
	'id' => 'header_background_video_on_front_page',
	'name' => esc_html__('Video on front page', 'chap'),
	'desc' => esc_html__('Show background video only on front page.', 'chap'),
	'type' => 'checkbox',
	'default' => true,
]);

$tab->createOption([
	'id' => 'header_background_video_mobile',
	'name' => esc_html__('Video on mobile', 'chap'),
	'desc' => esc_html__('Play background video on small screens (not recommended).', 'chap'),
	'type' => 'checkbox',
	'default' => false,
]);

$tab->createOption([
	'name' => esc_html__('Masthead', 'chap'),
	'type' => 'heading',
]);

$tab->createOption([
	'id' => 'title_in_masthead',
	'name' => esc_html__('Page title in masthead', 'chap'),
	'desc' => esc_html__('Choose where to display the title on content pages.', 'chap'),
	'type' => 'chap-radio-image',
	'options' => [
		'enabled' => [
			'desc' => esc_html__('Enabled', 'chap'),
			'image' => Assets\asset_path('images/options/title_in_masthead_1.png'),
		],
		'disabled' => [
			'desc' => esc_html__('Disabled', 'chap'),
			'image' => Assets\asset_path('images/options/title_in_masthead_2.png'),
		],
	],
	'default' => 'enabled',
]);

$tab->createOption([
	'name' => esc_html__('Masthead title', 'chap'),
	'desc' => esc_html__('Changes only apply for titles displayed in the masthead.', 'chap'),
	'type' => 'heading',
]);

$tab->createOption([
	'id' => 'masthead_title_size',
	'name' => esc_html__('Size', 'chap'),
	'type' => 'select',
	'options' => [
		'tiny' => esc_html__('Tiny', 'chap'),
		'small' => esc_html__('Small', 'chap'),
		'medium' => esc_html__('Medium', 'chap'),
		'large' => esc_html__('Large', 'chap'),
		'huge' => esc_html__('Huge', 'chap'),
	],
	'default' => 'large',
]);

$tab->createOption([
	'id' => 'masthead_title_alignment',
	'name' => esc_html__('Alignment', 'chap'),
	'type' => 'chap-radio-image',
	'options' => [
		'left' => [
			'desc' => esc_html__('Left', 'chap'),
			'image' => Assets\asset_path('images/options/masthead_title_alignment_1.png'),
		],
		'center' => [
			'desc' => esc_html__('Center', 'chap'),
			'image' => Assets\asset_path('images/options/masthead_title_alignment_2.png'),
		],
		'right' => [
			'desc' => esc_html__('Right', 'chap'),
			'image' => Assets\asset_path('images/options/masthead_title_alignment_3.png'),
		],
	],
	'default' => 'center',
]);

/**
 * Deprecated option.
 */
$tab->createOption([
	'id' => 'masthead_title_margin',
	'name' => esc_html__('Masthead title vertical margin', 'chap'),
	'desc' => esc_html__('Greater margin makes for a taller header.', 'chap'),
	'type' => 'number',
	'min' => 0,
	'max' => 10,
	'step' => 0.5,
	'unit' => 'em',
	'default' => 3,
	// 'css' => '.masthead.segment .ui.title.container { margin: valueem 0; }',
	'hidden' => true,
]);

/**
 * Replaces masthead_title_margin.
 */
$tab->createOption([
	'id' => 'masthead_title_box',
	'name' => esc_html__('Box model', 'chap'),
	'image' => Assets\asset_path('images/options/box_model_masthead_title.png'),
	'type' => 'box',
	'box' => [
		'margin',
		'border',
		'padding',
	],
	'params' => [
		'margin' => ['top', 'bottom'],
		'border' => ['top', 'right', 'bottom', 'left'],
		'padding' => ['top', 'right', 'bottom', 'left'],
	],
	'selector' => '.full.height > .ui.masthead.segment > .ui.title.container',
	'default' => [
		'margin' => [
			'top' => [
				'value' => get('masthead_title_margin'),
				'units' => 'em',
			],
			'bottom' => [
				'value' => get('masthead_title_margin'),
				'units' => 'em',
			],
		],
	],
]);

$tab->createOption(['type' => 'save']);

/**
 * Create CSS for the header based on options.
 */
function chap_tf_header_css() {
	$titan = \ChapTitanFramework::getInstance(CHAP_TF);
	$angle = round(intval($titan->getOption('header_bottom_angle')));
	if($angle != 0) {
		$clip_left = $angle < 0 ? abs($angle) / 2 : 0;
		$clip_right = $angle > 0 ? $angle / 2 : 0;
		if(apply_filters('chap_use_new_header_clip_method', true)) {
			$page_background = $titan->getOption('sui_page_background_color');
			$titan->createCSS('
				.ui.masthead.segment > .ui.masthead-clip.segment {
					width: 100%;
					height: ' . abs($clip_left + $clip_right) . 'vw;
					position: absolute;
					bottom: -1px;
					padding: 0;
					border: 0;
					background: ' . $page_background . ';
					clip-path: polygon(
						0 0,
						100% 0,
						100% calc(100% - ' . $clip_right . 'vw),
						0 calc(100% - ' . $clip_left . 'vw)
					);
					transform: rotateX(180deg) rotateY(180deg);
				}
			');
		} else {
			$titan->createCSS('
				.ui.masthead.segment {
					clip-path: polygon(
						0 0,
						100% 0,
						100% calc(100% - ' . $clip_right . 'vw),
						0 calc(100% - ' . $clip_left . 'vw)
					);
				}
			');
		}
	}
}
add_filter('customize_save_after',               __NAMESPACE__ . '\\chap_tf_header_css', 5);
add_filter('ctf_admin_options_saved_' . CHAP_TF, __NAMESPACE__ . '\\chap_tf_header_css', 5);

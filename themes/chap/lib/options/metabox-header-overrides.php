<?php

/**
 * Metabox for choosing a header template.
 */

namespace Chap\Options;
use Chap\Helpers;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

$header_options_post_types = apply_filters('chap_header_overrides_post_types', ['page', 'post', 'product']);

$metaBox = $titan->createMetaBox([
	'name' => esc_html__('Header overrides', 'chap'),
	'post_type' => $header_options_post_types,
	'priority' => 'low',
]);

$metaBox->createOption([
	'id' => 'header_template',
	'name' => esc_html__('Override header template', 'chap'),
	'type' => 'select',
	'options' => array_merge(
		['default' => esc_html__('Default', 'chap')],
		Helpers\get_header_templates()
	),
	'default' => 'default',
]);

$metaBox->createOption([
	'id' => 'enable_custom_header_background',
	'name' => esc_html__('Override background', 'chap'),
	'desc' => esc_html__('Use a custom header background for this page.', 'chap'),
	'type' => 'checkbox',
	'default' => false,
]);

/**
 * Used to store background CSS.
 */
$metaBox->createOption([
	'id' => 'custom_header_background_css',
	'type' => 'code',
	'lang' => 'css',
	'enqueue' => true,
	'compiled' => true,
	'hidden' => true,
]);

$custom_header_background = $metaBox->createOption([
	'id' => 'custom_header',
	'name' => esc_html__('Custom background', 'chap'),
	'visible' => [
		'enable_custom_header_background' => [true],
	],
	'type' => 'background',
	'options' => [
		'color' => [
			'alpha' => true,
		],
		'color_2' => [
			'alpha' => true,
		],
		'opacity' => [
			'default' => 0.5,
		],
	],
	'selector' => '.full.height > .ui.masthead.segment',
]);

/**
 * Store background CSS.
 */
add_action('save_post', function($postID, $post = null) use ($header_options_post_types, $custom_header_background) {
	$post_type = get_post_type($postID);
	if(!in_array($post_type, $header_options_post_types)) {
		return;
	}

	$enabled = get('enable_custom_header_background', $postID);
	if($enabled) {
		$class_prefix = $post_type === 'page' ? 'page-id-' : 'postid-';
		$custom_header_background->settings['selector'] = 'body.' . esc_attr($class_prefix . $postID) . ' .full.height > .ui.masthead.segment';
		$css = $custom_header_background->generateCSSCode('', $custom_header_background, $postID);
		$css = Helpers\css_minify($css);
		update_post_meta($postID, CHAP_TF . '_custom_header_background_css', $css);
	} else {
		update_post_meta($postID, CHAP_TF . '_custom_header_background_css', '');
	}
}, 15, 2);

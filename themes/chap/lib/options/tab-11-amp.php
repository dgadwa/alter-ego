<?php

namespace Chap\Options;
use Chap\Assets;
use Chap\Helpers;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

$tab = $panel->createTab(['name' => esc_html__('AMP', 'chap')]);

$tab->createOption([
	'id' => 'amp_enabled',
	'name' => '<img src="' . esc_url(Assets\asset_path('images/options/logo_amp.png')) . '" alt="' . esc_html__('AMP logo', 'chap') . '" />',
	'desc' => esc_html__('Enable Accelerated Mobile Pages.', 'chap'),
	'type' => 'checkbox',
	'default' => defined('AMP__VERSION') ? true : false,
]);

if(!get('amp_enabled') || !defined('AMP__VERSION')) {

	$tab->createOption([
		'name' => esc_html__('What is AMP?', 'chap'),
		'desc' => sprintf(
			/* Translators: HTML new line tags. */
			esc_html__('The AMP Project is an open-source initiative aiming to make the web better for all. %1$sThe project enables the creation of websites and ads that are consistently fast, %1$sbeautiful and high-performing across devices and distribution platforms.', 'chap'),
			'<br />'
		) .
		'<br /><br />' .
		'<a class="button button-primary" href="https://www.ampproject.org/learn/overview/" target="_blank" rel="noopener">' .
			esc_html__('Learn more about AMP', 'chap') .
		'</a>' .
		'<br /><br />',
		'type' => 'heading',
	]);

	if(get('amp_enabled') && !defined('AMP__VERSION')) {
		$tab->createOption([
			'name' => esc_html__('AMP plugin required', 'chap'),
			'desc' => esc_html__('AMP options will appear here once the AMP plugin is installed and activated.', 'chap') . '<br /><br />',
			'type' => 'heading',
		]);
	}

	$tab->createOption(['type' => 'save']);

	return;

}

$tab->createOption([
	'name' => esc_html__('How do I see the AMP pages?', 'chap'),
	'desc' => sprintf(
		/* Translators: HTML code tags. */
		esc_html__('To see the AMP version of a page, add %1$s?amp=1%2$s to the end of the URL.', 'chap'),
		'<code>',
		'</code>'
	) .
	'<br />' . esc_html__('AMP is supported for front page, chosen post types, pages, search results, archives, WooCommerce shop and products.', 'chap') . '<br />' .
	'<br />' . esc_html__('Due to AMP CSS length limitations only essential styles are dynamically loaded based on the shortcodes that are used.', 'chap') . '<br />' .
	sprintf(
		/* Translators: HTML anchor tag to CSS helper classes documentation, HTML code tags. */
		esc_html__('All content may not be suitable for AMP pages, using %1$sCSS helper classes%2$s %3$s.amp-only%4$s and %3$s.amp-invisible%4$s can help tailor your content to be presentable in both formats.', 'chap'),
		'<a href="' . Helpers\hc_link('chap.website/css-helper-classes') . '" target="_blank" rel="noopener">',
		'</a>',
		'<code>',
		'</code>'
	),
	'type' => 'heading',
]);

$tab->createOption([
	'name' => esc_html__('General', 'chap'),
	'type' => 'heading',
]);

$tab->createOption([
	'id' => 'amp_main_menu_links_redirect',
	'name' => esc_html__('AMP menu links', 'chap'),
	'desc' => esc_html__('Links in AMP menu redirect to main website.', 'chap'),
	'help' => sprintf(
		esc_html__('When this option is disabled links in AMP main menu will include %1$s?amp=1%2$s suffix, keeping users on AMP version of the website when they navigate to other pages.', 'chap'),
		'<code>',
		'</code>'
	),
	'type' => 'checkbox',
	'default' => true,
]);

$tab->createOption([
	'id' => 'amp_show_non_amp_version_link',
	'name' => esc_html__('Link to non-AMP version', 'chap'),
	'desc' => esc_html__('Show a link in the footer to non-AMP version of the current page.', 'chap'),
	'type' => 'checkbox',
	'default' => false,
]);

$tab->createOption([
	'id' => 'amp_main_menu_menu_separate',
	'name' => esc_html__('Use separate menu', 'chap'),
	'desc' => esc_html__("Don't use main menu items for AMP menu.", 'chap'),
	'help' => esc_html__('When this option is enabled you can choose a different menu from the WordPress menu editor to be used for the AMP menu.', 'chap'),
	'type' => 'checkbox',
	'default' => false,
]);

$tab->createOption([
	'id' => 'amp_footer_text',
	'name' => esc_html__('Footer text', 'chap'),
	'type' => 'editor',
	'wpautop' => false,
	'collapsed' => true,
	'default' => '<a href="' . Helpers\hc_link('chap.website') . '" target="_blank" rel="noopener">Powered by Chap WordPress theme</a>',
]);

$tab->createOption([
	'id' => 'amp_fallback_image',
	'name' => esc_html__('Fallback image', 'chap'),
	'desc' => sprintf(
		/* Translators: HTML new line tag, HTML strong tags. */
		esc_html__('Google requires an image for an AMP page with structured data to appear in a carousel of rich results in mobile search. %1$sBy default the featured image is used. In case it doesn\'t exist, you can specify a fallback image %2$sat least 696px wide%3$s, for example your site/brand logo.', 'chap'),
		'<br />',
		'<strong>',
		'</strong>'
	),
	'type' => 'upload',
]);

$tab->createOption([
	'id' => 'amp_show_admin_link',
	'name' => esc_html__('Admin bar link', 'chap'),
	'desc' => esc_html__('Show a link in the WordPress admin bar to AMP version of the current page.', 'chap'),
	'type' => 'checkbox',
	'default' => true,
]);

$tab->createOption([
	'name' => esc_html__('Social sharing', 'chap'),
	'type' => 'heading',
]);

$tab->createOption([
	'id' => 'amp_social_post_types',
	'name' => esc_html__('Post types', 'chap'),
	'desc' => esc_html__('Enable social sharing buttons for these post types.', 'chap'),
	'type' => 'multicheck-post-types',
	'default' => [
		'post',
		'product',
	],
]);

$tab->createOption([
	'id' => 'amp_social_share',
	'name' => esc_html__('Social networks', 'chap'),
	'desc' => esc_html__('Show social sharing button for these networks.', 'chap'),
	'type' => 'multicheck',
	'options' => [
		'email' => esc_html__('Email', 'chap'),
		'facebook' => esc_html__('Facebook', 'chap'),
		'twitter' => esc_html__('Twitter', 'chap'),
		'gplus' => esc_html__('Google Plus', 'chap'),
		'linkedin' => esc_html__('LinkedIn', 'chap'),
		'pinterest' => esc_html__('Pinterest', 'chap'),
		'tumblr' => esc_html__('Tumblr', 'chap'),
		'whatsapp' => esc_html__('WhatsApp', 'chap'),
	],
	'default' => [
		'email',
		'facebook',
		'twitter',
		'gplus',
	],
]);

$tab->createOption([
	'id' => 'amp_social_facebook_app_id',
	'name' => esc_html__('Facebook app ID', 'chap'),
	'desc' => sprintf(
		/* Translators: HTML anchor tag to Facebook Share dialog documantation. */
		esc_html__('Facebook %1$sshare dialog%2$s requires and app ID.', 'chap'),
		'<a href="https://developers.facebook.com/docs/sharing/reference/share-dialog" target="_blank" rel="noopener">',
		'</a>'
	),
	'type' => 'text',
]);

$tab->createOption([
	'name' => esc_html__('Analytics', 'chap'),
	'type' => 'heading',
]);

$tab->createOption([
	'id' => 'amp_ga_id',
	'name' => esc_html__('Google Analytics ID', 'chap'),
	'desc' => esc_html__('Insert your GA ID to track pageviews on AMP pages.', 'chap'),
	'type' => 'text',
	'placeholder' => 'UA-XXXXX-Y',
]);

$tab->createOption([
	'id' => 'amp_parsely_api_key',
	'name' => esc_html__('Parse.ly API key', 'chap'),
	'desc' => esc_html__('Insert your Parse.ly analytics platform API key.', 'chap'),
	'type' => 'text',
]);

$tab->createOption([
	'name' => esc_html__('Advertisement', 'chap'),
	'desc' => sprintf(
		/* Translators: HTML anchor tag to AMP ad network demos. */
		esc_html__('Look up AMP markup for your Ad network %1$shere%2$s.', 'chap'),
		'<a href="https://github.com/ampproject/amphtml/tree/master/ads" target="_blank" rel="noopener">',
		'</a>'
	),
	'type' => 'heading',
]);

$ad_placeholder = <<<AMP
<amp-ad
    width="320"
    height="100"
    type="adsense"
    data-ad-client="xx-yyy-0000000000000000"
    data-ad-slot="0000000000"
    class="centered">
</amp-ad>
AMP;

$ad_placeholder_2 = <<<AMP
<amp-ad
    width="300"
    height="250"
    type="adsense"
    data-ad-client="xx-yyy-0000000000000000"
    data-ad-slot="0000000000">
</amp-ad>
AMP;

$tab->createOption([
	'id' => 'amp_ad_slot_1_enabled',
	'name' => esc_html__('Ad slot 1', 'chap'),
	'desc' => esc_html__('Displayed below the header.', 'chap'),
	'type' => 'enable',
	'default' => false,
]);

$tab->createOption([
	'id' => 'amp_ad_slot_1',
	'type' => 'textarea',
	'default' => $ad_placeholder,
]);

$tab->createOption([
	'id' => 'separator-1',
	'type' => 'custom',
	'custom' => '<hr />',
]);

$tab->createOption([
	'id' => 'amp_ad_slot_2_enabled',
	'name' => esc_html__('Ad slot 2', 'chap'),
	'desc' => sprintf(
		/* Translators: HTML code tags. */
		esc_html__('Replaces all instances of %1$s<!--amp-ad-->%2$s in post content.', 'chap'),
		'<code>',
		'</code>'
	),
	'type' => 'enable',
	'default' => false,
]);

$tab->createOption([
	'id' => 'amp_ad_slot_2',
	'type' => 'textarea',
	'default' => $ad_placeholder_2,
]);

$tab->createOption([
	'id' => 'separator-2',
	'type' => 'custom',
	'custom' => '<hr />',
]);

$tab->createOption([
	'id' => 'amp_ad_slot_3_enabled',
	'name' => esc_html__('Ad slot 3', 'chap'),
	'desc' => esc_html__('Displayed above the footer.', 'chap'),
	'type' => 'enable',
	'default' => false,
]);

$tab->createOption([
	'id' => 'amp_ad_slot_3',
	'type' => 'textarea',
	'default' => $ad_placeholder,
]);

$tab->createOption([
	'name' => esc_html__('Styles', 'chap'),
	'type' => 'heading',
]);

$tab->createOption([
	'id' => 'amp_content_max_width',
	'name' => esc_html__('Content maximum width', 'chap'),
	'type' => 'number',
	'min' => 320,
	'max' => 1024,
	'step' => 5,
	'unit' => 'px',
	'default' => 600,
]);

$tab->createOption([
	'id' => 'amp_slider_height',
	'name' => esc_html__('Slider height', 'chap'),
	'type' => 'number',
	'min' => 100,
	'max' => 1000,
	'step' => 10,
	'unit' => 'px',
	'default' => 320,
]);

$tab->createOption([
	'id' => 'amp_dark_text_color',
	'name' => esc_html__('Dark text color', 'chap'),
	'type' => 'color',
	'alpha' => true,
	'default' => 'rgba(0, 0, 0, 0.87)',
]);

$tab->createOption([
	'id' => 'amp_light_text_color',
	'name' => esc_html__('Light text color', 'chap'),
	'type' => 'color',
	'alpha' => true,
	'default' => 'rgba(255, 255, 255, 0.9)',
]);

$tab->createOption([
	'id' => 'amp_muted_text_color',
	'name' => esc_html__('Muted text color', 'chap'),
	'type' => 'color',
	'alpha' => true,
	'default' => 'rgba(0, 0, 0, 0.6)',
]);

$tab->createOption([
	'id' => 'amp_button_font_bold',
	'name' => esc_html__('Button font weight', 'chap'),
	'desc' => esc_html__('Bold', 'chap'),
	'type' => 'checkbox',
	'default' => true,
]);

$tab->createOption([
	'name' => esc_html__('Override styles', 'chap'),
	'desc' => esc_html__('Leave blank to inherit main website options.', 'chap'),
	'type' => 'heading',
]);

$tab->createOption([
	'id' => 'amp_primary_color',
	'name' => esc_html__('Primary color', 'chap'),
	'type' => 'color',
]);

$tab->createOption([
	'id' => 'amp_secondary_color',
	'name' => esc_html__('Secondary color', 'chap'),
	'type' => 'color',
]);

$tab->createOption([
	'id' => 'amp_header_background_color',
	'name' => esc_html__('Header background color', 'chap'),
	'type' => 'color',
]);

$tab->createOption([
	'id' => 'amp_header_text_color',
	'name' => esc_html__('Header text color', 'chap'),
	'type' => 'color',
]);

$tab->createOption([
	'id' => 'amp_slider_background_color',
	'name' => esc_html__('Slider background color', 'chap'),
	'type' => 'color',
]);

$tab->createOption([
	'id' => 'amp_page_background_color',
	'name' => esc_html__('Page background color', 'chap'),
	'type' => 'color',
]);

$tab->createOption([
	'id' => 'amp_page_text_color',
	'name' => esc_html__('Page text color', 'chap'),
	'type' => 'color',
]);

$tab->createOption([
	'id' => 'amp_link_color',
	'name' => esc_html__('Link color', 'chap'),
	'type' => 'color',
]);

$tab->createOption([
	'id' => 'amp_footer_background_color',
	'name' => esc_html__('Footer background color', 'chap'),
	'type' => 'color',
]);

$tab->createOption([
	'id' => 'amp_footer_text_color',
	'name' => esc_html__('Footer text color', 'chap'),
	'type' => 'color',
]);

$tab->createOption([
	'id' => 'amp_custom_css',
	'name' => esc_html__('Additional CSS/SASS for AMP pages', 'chap'),
	'type' => 'code',
	'lang' => 'css',
	'enqueue' => false,
]);

$tab->createOption(['type' => 'save']);

/**
 * Compile the custom CSS.
 */
function compile_custom_amp_css() {
	$compiled_css = '';
	$custom_css = get('amp_custom_css');
	if(strlen($custom_css) > 0) {
		include_once get_template_directory() . '/lib/3rd-party/scssphp/scss.inc.php';
		$scss = new \ChapTitanSCSSCompiler();
		$scss->setFormatter('chaptitanscss_formatter_compressed');
		try {
			$compiled_css = $scss->compile($custom_css);
		} catch(\Exception $e) {
			$compiled_css = '';
			update_option('chap_tf_sass_error', $e->getMessage());
		}
	}
	update_option('chap_amp_compiled_css', $compiled_css);
}
add_filter('customize_save_after',               __NAMESPACE__ . '\\compile_custom_amp_css', 5);
add_filter('ctf_admin_options_saved_' . CHAP_TF, __NAMESPACE__ . '\\compile_custom_amp_css', 5);

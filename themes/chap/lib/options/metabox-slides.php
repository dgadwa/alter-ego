<?php

/**
 * Chap slide options metaboxes.
 */

namespace Chap\Options;
use Chap\Helpers;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

/**
 * Slide location metabox.
 */
$metaBox = $titan->createMetaBox([
	'name' => esc_html__('Slide location', 'chap'),
	'post_type' => 'chap_slide',
	'context' => 'side',
	'priority' => 'low',
]);

$metaBox->createOption([
	'id' => 'slide_display_on_front_page',
	'desc' => esc_html__('Front page', 'chap'),
	'type' => 'checkbox',
	'default' => true,
]);

$metaBox->createOption([
	'id' => 'slide_display_on_page',
	'name' => esc_html__('Pages', 'chap'),
	'type' => 'chap-select-pages',
	'multiple' => true,
]);

$metaBox->createOption([
	'id' => 'slide_display_on_post',
	'name' => esc_html__('Posts', 'chap'),
	'type' => 'chap-select-posts',
	'multiple' => true,
]);

$metaBox->createOption([
	'id' => 'slide_display_on_amp',
	'desc' => esc_html__('Show on AMP pages', 'chap'),
	'type' => 'checkbox',
	'default' => false,
]);

$metaBox->createOption([
	'id' => 'slide_only_display_on_amp',
	'desc' => esc_html__('Show only on AMP pages', 'chap'),
	'type' => 'checkbox',
	'default' => false,
]);

/**
 * Slide options metabox.
 */
$types = ['chap_slide'];
if(class_exists('WooCommerce')) {
	$types[] = 'product';
}
$postID = null;
global $pagenow;
if($pagenow === 'post.php') {
	$postID = isset($_GET['post']) ? (int)$_GET['post'] : null;
}
$metaBox = $titan->createMetaBox([
	'name' => esc_html__('Slide options', 'chap'),
	'post_type' => $types,
]);

$metaBox->createOption([
	'id' => 'slide_full_width',
	'name' => esc_html__('Full width', 'chap'),
	'help' => sprintf(
		esc_html__('When this option is selected the slide contents won\'t be restrained to a maximum width.', 'chap') . '%1$s' .
		esc_html__('This allows a background image or color to cover the entire slider area.', 'chap') . '%1$s' .
		esc_html__('Wrap the slide contents in %2$s[container]%3$s shortcode to keep the text contained in the center.', 'chap') . '%1$s' .
		esc_html__('%2$s[container left aligned]%3$s or %2$s[container right aligned]%3$s can be used to align it\'s contents.', 'chap'),
		'<div class="ui divider"></div>',
		'<code>',
		'</code>'
	),
	'desc' => sprintf(
		esc_html__('Remove slider container.', 'chap'),
		'<br />'
	),
	'type' => 'checkbox',
	'default' => false,
	'hidden' => get_post_type($postID) !== 'chap_slide',
]);

$metaBox->createOption([
	'id' => 'slide_alignment',
	'name' => esc_html__('Alignment', 'chap'),
	'type' => 'radio',
	'horizontal' => true,
	'options' => [
		'left' => esc_html__('Left', 'chap'),
		'center' => esc_html__('Center', 'chap'),
		'right' => esc_html__('Right', 'chap'),
	],
	'default' => 'center',
	'hidden' => get_post_type($postID) !== 'chap_slide',
]);

$metaBox->createOption([
	'id' => 'slide_vertical_alignment',
	'name' => esc_html__('Vertical alignment', 'chap'),
	'type' => 'radio',
	'horizontal' => true,
	'options' => [
		'top' => esc_html__('Top', 'chap'),
		'middle' => esc_html__('Middle', 'chap'),
		'bottom' => esc_html__('Bottom', 'chap'),
	],
	'default' => 'middle',
	'hidden' => get_post_type($postID) !== 'chap_slide',
]);

if(class_exists('WooCommerce')):

	$metaBox->createOption([
		'id' => 'slider_show_product',
		'name' => esc_html__('Slider', 'chap'),
		'desc' => esc_html__('Show product slide in the front page slider.', 'chap'),
		'type' => 'checkbox',
		'default' => false,
		'hidden' => get_post_type($postID) !== 'product',
	]);

	$metaBox->createOption([
		'id' => 'slider_show_product_on_amp',
		'name' => esc_html__('AMP slider', 'chap'),
		'desc' => esc_html__('Show product slide in the front page slider on AMP pages.', 'chap'),
		'type' => 'checkbox',
		'default' => false,
		'hidden' => get_post_type($postID) !== 'product',
	]);

endif;

/**
 * Used to store background CSS.
 */
$metaBox->createOption([
	'id' => 'slide_background_css',
	'type' => 'code',
	'lang' => 'css',
	'enqueue' => false,
	'hidden' => true,
]);

$background_option = $metaBox->createOption([
	'id' => 'slide',
	'name' => esc_html__('Background', 'chap'),
	'type' => 'background',
	'options' => [
		'color' => [
			'alpha' => true,
		],
		'color_2' => [
			'alpha' => true,
		],
		'opacity' => [
			'default' => 1,
		],
	],
	'selector' => '#chap_slide',
]);

$metaBox->createOption([
	'id' => 'slide_order',
	'name' => esc_html__('Slide order', 'chap'),
	'help' => esc_html__('Reducing the order number makes the slide appear earlier in the slider.', 'chap'),
	'type' => 'number',
	'min' => -1000,
	'max' => 1000,
	'step' => 1,
	'default' => 0,
	'hidden' => get_post_type($postID) !== 'chap_slide',
]);

if(class_exists('WooCommerce')):

	$metaBox->createOption([
		'id' => 'product_slide_button_text',
		'name' => esc_html__('Override button text', 'chap'),
		'help' => esc_html__('This option allows to set custom text for the primary action button in product slides.', 'chap') .
				'<div class="ui divider"></div>' .
				esc_html('When left empty "View product" text from "chap" language namespace is used.'),
		'type' => 'text',
		'hidden' => get_post_type($postID) !== 'product',
	]);

	$metaBox->createOption([
		'id' => 'product_slide_order',
		'name' => esc_html__('Slide order', 'chap'),
		'help' => esc_html__('Reducing the order number makes the slide appear earlier than other product slides in the slider.', 'chap'),
		'type' => 'number',
		'min' => -1000,
		'max' => 1000,
		'step' => 1,
		'default' => 100,
		'hidden' => get_post_type($postID) !== 'product',
	]);

endif;

/**
 * Store background CSS.
 */
add_action('save_post', function($postID, $post = null) use ($background_option) {
	$post_type = get_post_type($postID);
	if(!in_array($post_type, ['chap_slide', 'product'])) {
		return;
	}
	if($post_type === 'product') {
		$background_option->settings['selector'] = str_replace('#chap_slide', '#product', $background_option->settings['selector']);
	}

	$background_option->settings['selector'] = $background_option->settings['selector'] . '-slide-' . esc_attr($postID);
	$css = $background_option->generateCSSCode('', $background_option, $postID);
	$css = Helpers\css_minify($css);
	update_post_meta($postID, CHAP_TF . '_slide_background_css', $css);
}, 15, 2);

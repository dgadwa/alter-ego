<?php

namespace Chap\Options;
use Chap\Assets;
use Chap\Helpers;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

$tab = $panel->createTab(['name' => esc_html__('SUI', 'chap')]);

$tab->createOption([
	'name' => '<img src="' . esc_url(Assets\asset_path('images/options/logo_sui.png')) . '" alt="' . esc_html__('Semantic UI logo', 'chap') . '" />',
	'type' => 'custom',
]);

$tab->createOption([
	'name' => esc_html__('Edit Semantic UI components', 'chap'),
	'desc' => sprintf(
		/* Translators: HTML anchor tag to Less documentation. */
		esc_html__('This is an advanced feature that allows full control over Semantic UI components. The language needs to be valid %1$sLESS%2$s or CSS.', 'chap'),
		'<a href="http://lesscss.org/3.x/#overview" target="_blank" rel="noopener">',
		'</a>'
	) . '<br />' . sprintf(
		/* Translators: HTML anchor tags. */
		esc_html__('More information about Semantic UI theming: %1$sChap%4$s, %2$sSemantic-UI.com%4$s, %3$sLearnSemantic.com%4$s.', 'chap'),
		'<a href="https://chap.website/semantic-ui-theming/" target="_blank" rel="noopener">',
		'<a href="https://semantic-ui.com/usage/theming.html" target="_blank" rel="noopener">',
		'<a href="http://learnsemantic.com/developing/customizing.html" target="_blank" rel="noopener">',
		'</a>'
	),
	'type' => 'heading',
]);

unset($component);

/**
 * Create dropdown select for Semantic UI component's,
 * only if user is currently in SUI Titan Framework tab.
 */
if(isset($_GET['tab']) && $_GET['tab'] === 'sui') {

	// Populate SUI slugs.
	$sui_files = include get_template_directory() . '/lib/admin/data-chap-sui-files.php';
	$sui_slugs = [];
	foreach($sui_files as $id => $sui_file) {
		$path = str_replace('definitions/', '', $sui_file['path']);
		if($path == 'chap/woocommerce' && !class_exists('WooCommerce')) {
			continue;
		}
		$sui_slugs[] = $path;
	}

	// Create selectable component options.
	$components = [];
	foreach($sui_slugs as $slug) {
		list($type, $element) = explode('/', $slug);
		$components[$slug] = ucfirst($type) . ' - ' . str_replace(['Rtl', 'Woocommerce'], ['RTL', 'WooCommerce'], ucfirst($element));
	}

	// Component editing selection.
	$tab->createOption([
		'id' => 'sui_component_edit',
		'name' => esc_html__('Select component to edit', 'chap'),
		'type' => 'select',
		'options' => $components,
		'default' => 'chap/core',
	]);

	// Update current component from GET.
	if(isset($_GET['component']) && in_array($_GET['component'], $sui_slugs)) {
		$component = $_GET['component'];
		$titan = \ChapTitanFramework::getInstance(CHAP_TF);
		foreach($titan->optionsUsed as $option) {
			$id = $option->settings['id'];
			if($id === 'sui_component_edit') {
				$option->setValue($component);
			}
		}
		$titan->saveInternalAdminPageOptions();
	}

} else {

	// Option without populated values, for pages other than SUI editor.
	$tab->createOption([
		'id' => 'sui_component_edit',
		'name' => esc_html__('Select component to edit', 'chap'),
		'type' => 'select',
		'options' => [],
		'default' => 'chap/core',
	]);

}

$tab->createOption([
	'id' => 'sui_ajax_save',
	'name' => esc_html__('AJAX save', 'chap'),
	'desc' => esc_html__('Save and recompile without refreshing the page.', 'chap'),
	'label' => esc_html__('Save Changes', 'chap'),
	'wait_label' => esc_html__('Saving...', 'chap'),
	'type' => 'ajax-button',
	'action' => 'sui_ajax_save',
	'class' => 'button-primary',
	'data_filter_callback' => 'chap_save_sui_ajax',
	'success_callback' => 'chap_compile_sui_ajax_cb',
]);

if(!isset($component)) {
	$component = get('sui_component_edit');
}
$tab->createOption([
	'id' => 'chap_sui_components',
	'name' => esc_html($component),
	'desc' => esc_html__('Note: variables that already have theme options have a comment next to them. Overriding them here manually may not work, the theme option should be used instead.', 'chap'),
	'type' => 'sui-code',
	'component' => $component,
	'default' => [],
]);

$tab->createOption([
	'id' => 'sui_ajax_save_2',
	'name' => esc_html__('AJAX save', 'chap'),
	'desc' => esc_html__('Save and recompile without refreshing the page.', 'chap'),
	'label' => esc_html__('Save Changes', 'chap'),
	'wait_label' => esc_html__('Saving...', 'chap'),
	'type' => 'ajax-button',
	'action' => 'sui_ajax_save',
	'class' => 'button-primary',
	'data_filter_callback' => 'chap_save_sui_ajax',
	'success_callback' => 'chap_compile_sui_ajax_cb',
]);

$tab->createOption([
	'name' => esc_html__('Compilation', 'chap'),
	'desc' => sprintf(
		/* Translators: HTML anchor tag to compiler settings page. */
		esc_html__('Compiler can be configured on the %1$scompiler settings%2$s page.', 'chap'),
		'<a href="' . esc_url(admin_url('admin.php?page=chap-compiler')) . '">',
		'</a>'
	),
	'type' => 'heading',
]);

$tab->createOption([
	'id' => 'sui_recompile_sui',
	'name' => esc_html__('Recompile', 'chap'),
	'desc' => esc_html__('Recompile Semantic UI after saving options on this page.', 'chap'),
	'type' => 'checkbox',
	'default' => true,
]);

$tab->createOption(['type' => 'save']);

/**
 * AJAX save.
 */
function wp_ajax_sui_ajax_save() {
	if(empty($_POST['nonce']) || !wp_verify_nonce($_POST['nonce'], 'ctf-ajax-button')) {
		wp_send_json_error(esc_html__('Security check failed, please refresh the page and try again.', 'chap'));
	}

	$titan = \ChapTitanFramework::getInstance(CHAP_TF);
	foreach($titan->optionsUsed as $option) {
		$id = $option->settings['id'];
		if($id === 'chap_sui_components') {
			$value = $option->getValue();
			$value[$_POST['sui_component']] = [
				'variables' => $_POST['variables_content'],
				'overrides' => $_POST['overrides_content'],
			];
			$option->setValue($value);
		}
	}
	$titan->saveInternalAdminPageOptions();

	wp_send_json_success([
		'status' => 1,
		'message' => esc_html__('Compiling...', 'chap'),
		'ui' => get_chap_sui_compile_ui(),
	]);
}
add_action('wp_ajax_sui_ajax_save', __NAMESPACE__ . '\\wp_ajax_sui_ajax_save');

<?php

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

/**
 * Code editor for Semantic UI components.
 */
class ChapTitanFrameworkOptionSuiCode extends ChapTitanFrameworkOptionCode {

	protected $editors = [];

	function __construct($settings, $owner) {

		parent::__construct($settings, $owner);
		$this->settings['enqueue']  = false;
		$this->settings['height']  = 320;
		remove_action('customize_controls_enqueue_scripts', [$this, 'loadAdminScripts']);
		remove_filter('ctf_generate_css_code_' . $this->getOptionNamespace(), [$this, 'generateCSSCode'], 10, 2);
		remove_filter('wp_head', [$this, 'printCSSForPagesAndPosts'], 100);
		remove_filter('wp_footer', [$this, 'printJS'], 100);
		remove_filter('wp_footer', [$this, 'printJSForPagesAndPosts'], 101);

		// Load compontents theme.
		$component_parts = explode('/', $this->settings['component']);
		$component_name = $component_parts[1];
		$this->settings['component_theme'] = \Chap\Options\get('component_' . $component_name);
		if(empty($this->settings['component_theme'])) {
			$this->settings['component_theme'] = 'default';
		}

		// Define editors.
		if($this->settings['component'] !== 'globals/site') {
			$this->editors[] = 'site_variables';
		}
		$this->editors[] = 'component_variables';
		if($this->settings['component_theme'] !== 'default') {
			$this->editors[] = 'theme_variables';
		}
		$this->editors[] = 'variables';
		$this->editors[] = 'component';
		$this->editors[] = 'component_overrides';
		if($this->settings['component_theme'] !== 'default') {
			$this->editors[] = 'theme_overrides';
		}
		$this->editors[] = 'overrides';

	}

	/**
	 * Displays the option for admin pages and meta boxes.
	 */
	public function display() {
		ob_start();
		$this->echoOptionHeaderBare();
		$header = ob_get_clean();
		echo str_replace('<td', '<td colspan="2"', $header);
		?>
		<script>
		var chap_edited_sui_component = false;
		var chap_confirm_navigation_message = '<?php esc_attr_e('Current changes are not saved, are you sure you wish to navigate away?', 'chap'); ?>';
		jQuery(document).ready(function($){

			var editors = ['<?php echo join("', '", $this->editors); ?>'];

			editors.forEach(function(slug){
				var editor_id = slug + '_editor';
				var container = jQuery('#' + editor_id);
				container.width(container.parent().width()).height(<?php echo esc_attr($this->settings['height']); ?>);

				var editor = ace.edit(editor_id);
				container.css('width', 'auto');
				editor.setValue(container.siblings('textarea#' + slug + '_content').val());
				editor.setTheme('ace/theme/<?php echo esc_attr($this->settings['theme']); ?>');
				editor.getSession().setMode('ace/mode/less');
				editor.setShowPrintMargin(false);
				editor.setShowFoldWidgets(false);
				editor.setFontSize(14);
				editor.renderer.setScrollMargin(10, 10);
				// editor.setHighlightActiveLine(false);
				editor.gotoLine(1);
				editor.session.setUseWorker(false);

				if(slug !== 'variables' && slug !== 'overrides') {
					editor.setReadOnly(true);
				}

				editor.getSession().on('change', function(e){
					$(editor.container).siblings('textarea#' + slug + '_content').val(editor.getValue());
					chap_edited_sui_component = true;
				});

			});

		});
		</script>
		<?php

		/**
		 * Init WP filesystem.
		 */
		WP_Filesystem();
		global $wp_filesystem;

		$current_value = $this->getValue();
		$sui = get_template_directory() . '/lib/semantic-ui/';

		foreach($this->editors as $editor) {

			$read_only = true;
			$value = false;
			$description = '';
			switch($editor) {
				case 'site_variables':
					$theme = 'default';
					$name = 'globals/site.variables';
					$display_name = $theme . '/' . $name;
					$description = esc_html__('Global base variables.', 'chap');
				break;
				case 'component_variables':
					$theme = 'default';
					$name = $this->settings['component'] . '.variables';
					$display_name = $theme . '/' . $name;
					$description = esc_html__('Variables specific for the component.', 'chap');
				break;
				case 'theme_variables':
					$theme = $this->settings['component_theme'];
					$name = $this->settings['component'] . '.variables';
					$display_name = $theme . '/' . $name;
					$description = esc_html__('Variables overriden and added by the component theme.', 'chap');
				break;
				case 'variables':
					$display_name = $this->settings['component'] . '.variables';
					$value = $this->getValueForEditing($this->settings['component'], 'variables');
					$description = esc_html__('Override variables or create new ones.', 'chap');
					$read_only = false;
				break;
				case 'component':
					$theme = 'default';
					$name = $this->settings['component'] . '.less';
					$display_name = $theme . '/' . $name;
					$description = esc_html__('LESS code for the component.', 'chap');
				break;
				case 'component_overrides':
					$theme = 'default';
					$name = $this->settings['component'] . '.overrides';
					$display_name = $theme . '/' . $name;
					$description = esc_html__('LESS code overrides for the component.', 'chap');
				break;
				case 'theme_overrides':
					$theme = $this->settings['component_theme'];
					$name = $this->settings['component'] . '.overrides';
					$display_name = $theme . '/' . $name;
					$description = esc_html__('Additional LESS code by the component theme.', 'chap');
				break;
				case 'overrides':
					$display_name = $this->settings['component'] . '.overrides';
					$value = $this->getValueForEditing($this->settings['component'], 'overrides');
					$description = esc_html__('Append your own LESS code for the component.', 'chap');
					$read_only = false;
				break;
			}
			// Populate the value from file.
			if($read_only && is_bool($value) && !$value) {
				if($editor === 'component') {
					$file = $sui . join('/', ['definitions', $name]);
				} else {
					$file = $sui . join('/', ['themes', $theme, $name]);
				}
				if(file_exists($file)) {
					$value = $wp_filesystem->get_contents($file);
				} else {
					$value = esc_html__('File not found', 'chap') . ': ' . esc_textarea($file);
				}
			}

			if(!empty($description)) {
				$read_only_class = $read_only ? ' read-only' : '';
				if($this->settings['component'] === 'chap/core' && $editor === 'component_variables') {
					$read_only_class .= ' open-by-default';
				}
				echo '<div class="chap-ace-editor-header' . $read_only_class . '">';
				echo '<strong>' . esc_html($display_name) . '</strong>: ' . esc_html($description);
				if($read_only) {
					echo '<span class="read-only">' . esc_html__('Read only', 'chap') . '</span>';
				}
				echo '</div>';
			}

			printf(
				'<div id="%s"></div>',
				esc_attr($editor . '_editor')
			);

			// Invisible textarea with editor value
			printf(
				'<textarea name="%s" id="%s" style="display:none">%s</textarea>',
				$read_only ? '' : esc_attr($editor . '_content'),
				esc_attr($editor . '_content'),
				esc_textarea($value)
			);

		}

		/**
		 * Which component is currently edited.
		 */
		printf(
			'<input type="hidden" name="sui_component" id="sui_component" value="%s" />',
			esc_attr($this->settings['component'])
		);

		$this->echoOptionFooter();
	}

	/**
	 * Combine variables and overrides to single option.
	 */
	public function cleanValueForSaving($value) {

		// Value from import.
		if(!empty($value)) {
			return $value;
		}

		// Nothing to save.
		if(!isset($_POST['variables_content']) && !isset($_POST['overrides_content'])) {
			return $value;
		}

		// Value from saving.
		$variables = $_POST['variables_content'];
		$overrides = $_POST['overrides_content'];
		$value = $this->getValue();
		$value[$this->settings['component']] = [
			'variables' => $variables,
			'overrides' => $overrides,
		];

		return $value;

	}

	/**
	 * Provide value as array.
	 */
	public function cleanValueForGetting($value) {
		if(is_array($value)) {
			return $value;
		}
		return stripslashes($value);
	}

	/**
	 * Provide the value of a components file type for editor.
	 */
	protected function getValueForEditing($component, $file_type) {
		$value = $this->getValue();
		if(!$value || empty($value)) {
			return '';
		}
		if(!isset($value[$component])) {
			return '';
		}
		if(!isset($value[$component][$file_type])) {
			return '';
		}
		return stripslashes($value[$component][$file_type]);
	}

	/**
	 * Disable customzer control.
	 */
	public function registerCustomizerControl($wp_customize, $section, $priority = 1) {}

}

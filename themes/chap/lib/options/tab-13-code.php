<?php

namespace Chap\Options;
use Chap\Assets;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

$tab = $panel->createTab(['name' => esc_html__('Code', 'chap')]);

$tab->createOption([
	'id' => 'global_custom_css',
	'name' => esc_html__('Global CSS/Sass', 'chap') .
	'<br /><br />' .
	'<img src="' . esc_url(Assets\asset_path('images/options/logo_sass.png')) . '" alt="' . esc_html__('Sass logo', 'chap') . '" />',
	'desc' => '<a id="tf-sass-vars-toggle" href="#" rel="nofollow">' . esc_html__('Available Sass variables', 'chap') . '</a>' .
	'<span class="dashicons dashicons-arrow-right"></span>' .
	'<ul id="tf-sass-vars" class="hidden">' .
		'<li><code>$header_background_color</code> - ' . esc_html__('Header background color', 'chap') . '</li>' .
		'<li><code>$footer_background_color</code> - ' . esc_html__('Footer background color', 'chap') . '</li>' .
		'<li><code>$sui_primary_color</code> - ' . esc_html__('Primary color', 'chap') . '</li>' .
		'<li><code>$sui_secondary_color</code> - ' . esc_html__('Secondary color', 'chap') . '</li>' .
		'<li><code>$sui_border_radius</code> - ' . esc_html__('Border radius', 'chap') . '</li>' .
		'<li><code>$sui_link_color</code> - ' . esc_html__('Link color', 'chap') . '</li>' .
		'<li><code>$sui_page_background_color</code> - ' . esc_html__('Page background color', 'chap') . '</li>' .
		'<li><code>$sui_page_text_color</code> - ' . esc_html__('Page text color', 'chap') . '</li>' .
		'<li><code>$sui_input_background</code> - ' . esc_html__('Form inputs background color', 'chap') . '</li>' .
	'</ul>',
	'type' => 'code',
	'lang' => 'css',
]);

$tab->createOption([
	'id' => 'global_custom_javascript',
	'name' => esc_html__('Global JavaScript', 'chap') .
	'<br /><br />' .
	'<img src="' . esc_url(Assets\asset_path('images/options/logo_js.png')) . '" alt="' . esc_html__('JavaScript logo', 'chap') . '" />',
	'type' => 'code',
	'lang' => 'javascript',
]);

$tab->createOption([
	'name' => esc_html__('Content', 'chap'),
	'desc' => esc_html__('Append additional content after every page or post.', 'chap'),
	'type' => 'heading',
]);

$tab->createOption([
	'id' => 'after_post_content',
	'name' => esc_html__('After post content', 'chap'),
	'desc' => esc_html__('Append content to every post.', 'chap'),
	'type' => 'editor',
	'wpautop' => false,
	'collapsed' => true,
]);

$tab->createOption([
	'id' => 'after_page_content',
	'name' => esc_html__('After page content', 'chap'),
	'desc' => esc_html__('Append content to every page.', 'chap'),
	'type' => 'editor',
	'wpautop' => false,
	'collapsed' => true,
]);

$tab->createOption([
	'id' => 'after_page_content_front_page_disabled',
	'name' => esc_html__('Front page', 'chap'),
	'desc' => esc_html__("Don't append content on front page.", 'chap'),
	'type' => 'checkbox',
	'default' => true,
]);

$tab->createOption([
	'name' => esc_html__('Template content', 'chap'),
	'desc' => esc_html__('Append additional content in various theme locations.', 'chap'),
	'type' => 'heading',
]);

$tab->createOption([
	'id' => 'before_header_content',
	'name' => esc_html__('Before header content', 'chap'),
	'desc' => esc_html__('Add content before the header.', 'chap'),
	'type' => 'editor',
	'wpautop' => false,
	'collapsed' => true,
]);

$tab->createOption([
	'id' => 'before_masthead_content',
	'name' => esc_html__('Before masthead content', 'chap'),
	'desc' => esc_html__('Add content above the slider/masthead title.', 'chap'),
	'type' => 'editor',
	'wpautop' => false,
	'collapsed' => true,
]);

$tab->createOption([
	'id' => 'after_masthead_content',
	'name' => esc_html__('After masthead content', 'chap'),
	'desc' => esc_html__('Add content below the slider/masthead title.', 'chap'),
	'type' => 'editor',
	'wpautop' => false,
	'collapsed' => true,
]);

$tab->createOption([
	'id' => 'before_footer_content',
	'name' => esc_html__('Before footer content', 'chap'),
	'desc' => esc_html__('Add content before the footer.', 'chap'),
	'type' => 'editor',
	'wpautop' => false,
	'collapsed' => true,
]);

$tab->createOption([
	'id' => 'after_footer_content',
	'name' => esc_html__('After footer content', 'chap'),
	'desc' => esc_html__('Add content below the footer.', 'chap'),
	'type' => 'editor',
	'wpautop' => false,
	'collapsed' => true,
]);

$tab->createOption([
	'name' => esc_html__('Metaboxes', 'chap'),
	'desc' => esc_html__('If you don\'t wish to pollute the global namespace with code that is only meant for specific pages or posts you can enable metaboxes for custom CSS or JavaScript for certain post types and add the code while editing these individual posts.', 'chap'),
	'type' => 'heading',
]);

$tab->createOption([
	'id' => 'custom_css',
	'name' => esc_html__('Custom CSS metabox', 'chap'),
	'desc' => esc_html__('Enable custom CSS metabox for these post types.', 'chap'),
	'type' => 'multicheck-post-types',
	'default' => [],
]);

$tab->createOption([
	'id' => 'custom_javascript',
	'name' => esc_html__('Custom JavaScript metabox', 'chap'),
	'desc' => esc_html__('Enable custom JavaScript metabox for these post types.', 'chap'),
	'type' => 'multicheck-post-types',
	'default' => [],
]);

$tab->createOption(['type' => 'save']);

<?php

namespace Chap\Options;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

$tab = $panel->createTab(['name' => esc_html__('Images', 'chap')]);

$tab->createOption([
	'name' => esc_html__('Featured images', 'chap'),
	'type' => 'heading',
]);

$tab->createOption([
	'id' => 'featured_image_type',
	'name' => esc_html__('Default position', 'chap'),
	'desc' => esc_html__('Choose how to display featured images by default (can be overriden for specific posts/pages).', 'chap'),
	'help' => esc_html__("Hidden image won't be shown on the post page, but will be used as a thumbnail in archives.", 'chap'),
	'type' => 'radio',
	'options' => [
		'full' => esc_html__('Full width', 'chap'),
		'left' => esc_html__('Left corner', 'chap'),
		'right' => esc_html__('Right corner', 'chap'),
		'hidden' => esc_html__('Hidden', 'chap'),
	],
	'default' => 'right',
]);


$tab->createOption([
	'id' => 'featured_image_min_height',
	'name' => esc_html__('Min-height', 'chap'),
	'desc' => esc_html__('Applies for full width featured images only.', 'chap'),
	'type' => 'number',
	'min' => 0,
	'max' => 100,
	'step' => 1,
	'unit' => 'vw',
	'default' => 26,
	'css' => 'main article .ui.featured-image.segment { min-height: valuevw; }',
]);

$tab->createOption([
	'name' => esc_html__('Viewer.js', 'chap'),
	'desc' => esc_html__('Image viewer.', 'chap'),
	'type' => 'heading',
]);

$tab->createOption([
	'id' => 'viewer_enabled',
	'name' => esc_html__('Enable Viewer.js', 'chap'),
	'desc' => sprintf(
		/* Translators: HTML anchor tag to Viewer.js demo page. */
		esc_html__('Use %1$sViewer.js%2$s lightbox to show large versions of images.', 'chap'),
		'<a href="' . esc_url('https://fengyuanchen.github.io/viewerjs/') . '" target="_blank" rel="noopener">',
		'</a>'
	),
	'type' => 'checkbox',
	'default' => true,
]);

$tab->createOption([
	'id' => 'viewer_navbar',
	'name' => esc_html__('Navbar', 'chap'),
	'desc' => esc_html__('Show the navbar (small thumbnails of all available images in the gallery).', 'chap'),
	'type' => 'checkbox',
	'default' => true,
]);

$tab->createOption([
	'id' => 'viewer_title',
	'name' => esc_html__('Title', 'chap'),
	'desc' => esc_html__("Show the current image's name and dimensions.", 'chap'),
	'type' => 'checkbox',
	'default' => false,
]);

$tab->createOption([
	'id' => 'viewer_toolbar',
	'name' => esc_html__('Toolbar', 'chap'),
	'desc' => esc_html__('Show the toolbar.', 'chap'),
	'type' => 'checkbox',
	'default' => true,
]);

$tab->createOption([
	'id' => 'viewer_tooltip',
	'name' => esc_html__('Tooltip', 'chap'),
	'desc' => esc_html__('Show the tooltip with image ratio (percentage) when zooming in or out.', 'chap'),
	'type' => 'checkbox',
	'default' => true,
]);

$tab->createOption([
	'id' => 'viewer_movable',
	'name' => esc_html__('Movable', 'chap'),
	'desc' => esc_html__('Enable to move the image.', 'chap'),
	'type' => 'checkbox',
	'default' => true,
]);

$tab->createOption([
	'id' => 'viewer_zoomable',
	'name' => esc_html__('Zoombale', 'chap'),
	'desc' => esc_html__('Enable to zoom the image.', 'chap'),
	'type' => 'checkbox',
	'default' => true,
]);

$tab->createOption([
	'id' => 'viewer_rotatable',
	'name' => esc_html__('Rotatable', 'chap'),
	'desc' => esc_html__('Enable to rotate the image.', 'chap'),
	'type' => 'checkbox',
	'default' => true,
]);

$tab->createOption([
	'id' => 'viewer_transition',
	'name' => esc_html__('Transition', 'chap'),
	'desc' => esc_html__('Enable CSS3 Transition for some special elements.', 'chap'),
	'type' => 'checkbox',
	'default' => true,
]);

$tab->createOption([
	'id' => 'viewer_fullscreen',
	'name' => esc_html__('Fullscreen', 'chap'),
	'desc' => esc_html__('Enable to request full screen when play is pressed.', 'chap'),
	'type' => 'checkbox',
	'default' => true,
]);

$tab->createOption([
	'id' => 'viewer_interval',
	'name' => esc_html__('Interval', 'chap'),
	'desc' => esc_html__('Interval of each image when playing. 0 disables autoplay.', 'chap'),
	'type' => 'number',
	'min' => 0,
	'max' => 1000 * 30,
	'step' => 1000,
	'unit' => 'ms',
	'default' => 5000,
]);

$tab->createOption([
	'id' => 'viewer_keyboard',
	'name' => esc_html__('Keyboard', 'chap'),
	'desc' => esc_html__('Enable keyboard support.', 'chap'),
	'type' => 'checkbox',
	'default' => true,
]);

$tab->createOption(['type' => 'save']);

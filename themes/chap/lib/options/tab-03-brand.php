<?php

namespace Chap\Options;
use Chap\Assets;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

$tab = $panel->createTab(['name' => esc_html__('Brand', 'chap')]);

$tab->createOption([
	'name' => esc_html__('Brand text', 'chap'),
	'desc' => sprintf(
		esc_html__('Site title and tagline can be modified in the %1$sGeneral Settings%2$s.', 'chap'),
		'<a href="' . esc_url(admin_url('options-general.php')) . '" target="_blank">',
		'</a>'
	),
	'type' => 'heading',
]);

$tab->createOption([
	'id' => 'brand_show_site_title',
	'name' => esc_html__('Site title', 'chap'),
	'desc' => esc_html__('Show site title in brand.', 'chap'),
	'type' => 'checkbox',
	'default' => true,
]);

$tab->createOption([
	'id' => 'brand_show_tagline',
	'name' => esc_html__('Site tagline', 'chap'),
	'desc' => esc_html__('Show tagline in brand.', 'chap'),
	'type' => 'checkbox',
	'default' => true,
]);

$tab->createOption([
	'id' => 'brand_text_size',
	'name' => esc_html__('Text size', 'chap'),
	'type' => 'select',
	'options' => [
		'tiny' => esc_html__('Tiny', 'chap'),
		'small' => esc_html__('Small', 'chap'),
		'medium' => esc_html__('Medium', 'chap'),
		'large' => esc_html__('Large', 'chap'),
		'huge' => esc_html__('Huge', 'chap'),
	],
	'default' => 'large',
]);

$tab->createOption([
	'id' => 'brand_inverted',
	'name' => esc_html__('Inverted', 'chap'),
	'desc' => esc_html__('Invert brand text colors.', 'chap'),
	'help' => esc_html__('Inverted colors stand out better on darker backgrounds.', 'chap'),
	'type' => 'checkbox',
	'default' => true,
]);

$tab->createOption([
	'name' => esc_html__('Logo', 'chap'),
	'desc' => sprintf(
		esc_html__('Logo image can be set in the %sCustomizer%s.', 'chap'),
		'<a href="' . esc_url(admin_url('customize.php?autofocus[section]=title_tagline&autofocus[control]=custom_logo')) . '" target="_blank">',
		'</a>'
	),
	'type' => 'heading',
]);

$tab->createOption([
	'id' => 'brand_show_logo',
	'name' => esc_html__('Image', 'chap'),
	'desc' => esc_html__('Show logo image in brand.', 'chap'),
	'type' => 'checkbox',
	'default' => false,
]);

$tab->createOption([
	'id' => 'brand_logo_position',
	'name' => esc_html__('Image position', 'chap'),
	'desc' => esc_html__('Logo image position relative to the site title and/or tagline text.', 'chap'),
	'type' => 'chap-radio-image',
	'options' => [
		'left' => [
			'desc' => esc_html__('Left', 'chap'),
			'image' => Assets\asset_path('images/options/brand_logo_left.png'),
		],
		'above' => [
			'desc' => esc_html__('Above', 'chap'),
			'image' => Assets\asset_path('images/options/brand_logo_above.png'),
		],
		'right' => [
			'desc' => esc_html__('Right', 'chap'),
			'image' => Assets\asset_path('images/options/brand_logo_right.png'),
		],
	],
	'default' => 'left',
]);

$tab->createOption([
	'id' => 'brand_logo_size',
	'name' => esc_html__('Image size modifier', 'chap'),
	'desc' => esc_html__('Set to 0 to use original image size.', 'chap'),
	'type' => 'number',
	'unit' => 'em',
	'min' => 0,
	'max' => 15,
	'step' => 0.25,
	'default' => 3,
]);

$tab->createOption([
	'name' => esc_html__('Mobile behaviour', 'chap'),
	'desc' => esc_html__('These settings kick in at mobile screen size.', 'chap'),
	'type' => 'heading',
]);

$tab->createOption([
	'id' => 'brand_mobile_center_text',
	'name' => esc_html__('Center text', 'chap'),
	'desc' => esc_html__('Center brand text on mobile.', 'chap'),
	'type' => 'checkbox',
	'default' => false,
]);

$tab->createOption([
	'id' => 'brand_mobile_hide_text',
	'name' => esc_html__('Hide text', 'chap'),
	'desc' => esc_html__('Hide brand text on mobile.', 'chap'),
	'type' => 'checkbox',
	'default' => false,
]);

$tab->createOption([
	'id' => 'brand_mobile_hide_logo',
	'name' => esc_html__('Hide logo', 'chap'),
	'desc' => esc_html__('Hide logo image on mobile.', 'chap'),
	'type' => 'checkbox',
	'default' => false,
]);

$tab->createOption([
	'name' => esc_html__('Brand container', 'chap'),
	'type' => 'heading',
]);

$tab->createOption([
	'id' => 'brand_size',
	'name' => esc_html__('Size', 'chap'),
	'desc' => esc_html__('Controls the base size of all brand contents.', 'chap'),
	'type' => 'select',
	'options' => [
		'mini' => esc_html__('Mini', 'chap'),
		'tiny' => esc_html__('Tiny', 'chap'),
		'small' => esc_html__('Small', 'chap'),
		'medium' => esc_html__('Medium', 'chap'),
		'large' => esc_html__('Large', 'chap'),
		'big' => esc_html__('Big', 'chap'),
		'huge' => esc_html__('Huge', 'chap'),
		'massive' => esc_html__('Massive', 'chap'),
	],
	'default' => 'big',
]);

$tab->createOption([
	'id' => 'brand_box',
	'name' => esc_html__('Box model', 'chap'),
	'image' => Assets\asset_path('images/options/box_model_brand.png'),
	'type' => 'box',
	'box' => [
		'margin',
		'border',
		'padding',
	],
	'selector' => '.ui.masthead.segment .ui.branding .ui.brand.segment',
]);

$tab->createOption([
	'name' => esc_html__('Branding container', 'chap'),
	'type' => 'heading',
]);

/**
 * Deprecated option.
 */
$tab->createOption([
	'id' => 'brand_container_top_margin',
	'name' => esc_html__('Spacing from top', 'chap'),
	'desc' => esc_html__('Space above brand container.', 'chap'),
	'type' => 'number',
	'unit' => 'em',
	'min' => -5,
	'max' => 5,
	'step' => 0.5,
	'default' => 0,
	// 'css' => '.ui.branding.container, .ui.branding.flex-grid { margin-top: valueem; }',
	'hidden' => true,
]);

/**
 * Replaces brand_container_top_margin.
 */
$tab->createOption([
	'id' => 'brand_container_box',
	'name' => esc_html__('Box model', 'chap'),
	'image' => Assets\asset_path('images/options/box_model_branding.png'),
	'type' => 'box',
	'box' => [
		'margin',
		'border',
		'padding',
	],
	'params' => [
		'margin' => ['top', 'bottom'],
		'border' => ['top', 'right', 'bottom', 'left'],
		'padding' => ['top', 'right', 'bottom', 'left'],
	],
	'selector' => '.ui.masthead.segment .ui.branding',
	'default' => [
		'margin' => [
			'top' => [
				'value' => get('brand_container_top_margin'),
				'units' => 'em',
			],
		],
	],
]);

$tab->createOption([
	'id' => 'brand_container_bottom_margin',
	'name' => esc_html__('Spacing from menu', 'chap'),
	'desc' => esc_html__('Space between brand container and main menu.', 'chap'),
	'help' => esc_html__('Only applies for header templates where the main menu container is located below the branding container.', 'chap'),
	'type' => 'number',
	'unit' => 'em',
	'min' => -5,
	'max' => 5,
	'step' => 0.5,
	'default' => 1,
	'css' => '.ui.branding.container + .ui.mainmenu.container { margin-top: valueem; }',
]);

$tab->createOption(['type' => 'save']);

/**
 * Create CSS for the brand based on options.
 */
function chap_tf_brand_css() {

	$titan = \ChapTitanFramework::getInstance(CHAP_TF);

	$size = (int)$titan->getOption('brand_logo_size');
	if($size <= 0) {
		$titan->createCSS('
			.ui.brand.segment > .ui.brand.header > img.ui.custom-logo.image {
				width: auto;
			}
		');
	} else {
		$titan->createCSS('
			.ui.brand.segment > .ui.brand.header > img.ui.custom-logo.image {
				width: ' . esc_attr($size) . 'em;
			}
		');
	}

}
add_filter('customize_save_after',               __NAMESPACE__ . '\\chap_tf_brand_css', 6);
add_filter('ctf_admin_options_saved_' . CHAP_TF, __NAMESPACE__ . '\\chap_tf_brand_css', 6);

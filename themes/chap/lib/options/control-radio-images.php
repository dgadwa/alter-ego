<?php

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

/**
 * Radio image select, with an option to have a description for each image.
 */
class ChapTitanFrameworkOptionChapRadioImage extends ChapTitanFrameworkOption {

	function __construct($settings, $owner) {
		if(!isset($settings['hide_others'])) {
			$settings['hide_others'] = false;
		}
		parent::__construct($settings, $owner);
	}

	/*
	 * Display for options and meta
	 */
	public function display() {
		if(empty($this->settings['options'])) {
			return;
		}
		if($this->settings['options'] == []) {
			return;
		}

		$this->echoOptionHeader();

		// Get the correct value, since we are accepting indices in the default setting
		$value = $this->getValue();
		$template = '<label class="tf-radio-image" id="%s"%s>' .
						'%s' .
						'<input id="%s" type="radio" name="%s" value="%s" %s/>' .
						'<img src="%s" />' .
					'</label>';

		// print the images
		foreach($this->settings['options'] as $key => $data) {

			if($value == '') {
				$value = $key;
			}

			unset($description);
			if(is_array($data)) {
				$description = isset($data['desc']) ? $data['desc'] : '';
				$help = isset($data['help']) ? $data['help'] : '';
				$imageURL = $data['image'];
			} else {
				$imageURL = $data;
			}
			if($help) {
				$help_icon = $this->get_option_help_icon($key, $help);
				$help_popup = $this->get_option_help_popup($key, $help);
			} else {
				$help_icon = '';
				$help_popup = '';
			}
			$desc = isset($description) ?  $help_popup . '<p class="description">' . esc_html($description) . $help_icon . '</p>' : '';

			$style = $this->settings['hide_others'] && strlen(checked($value, $key, false)) < 1 ? ' style="display:none"' : '';

			printf($template,
				$this->getID() . $key,
				$style,
				$desc,
				$this->getID() . $key,
				$this->getID(),
				esc_attr($key),
				checked($value, $key, false),
				esc_attr($imageURL)
			);

		}

		if($this->settings['hide_others']) {
			echo '<br /><div id="chap-tf-show-others" class="button button-primary">' . esc_html__('Show all', 'chap') . '</div>';
		}

		$this->echoOptionFooter();
	}

	public function get_option_help_icon($key, $help) {
		if(!$key || !$help) {
			return '';
		}

		ob_start();
		?>
		<span class="ctf-popup dashicons dashicons-warning" id="help-opt-<?php echo esc_attr($key); ?>" onclick="return false;"></span>
		<?php
		return ob_get_clean();
	}

	public function get_option_help_popup($key, $help) {
		if(!$key || !$help) {
			return '';
		}

		ob_start();
		?>
		<div class="ui small popup" id="help-opt-<?php echo esc_attr($key); ?>-content">
			<?php echo wp_kses_post($help); ?>
		</div>
		<?php
		return ob_get_clean();
	}

}

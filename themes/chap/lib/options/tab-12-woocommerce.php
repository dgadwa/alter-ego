<?php

namespace Chap\Options;
use Chap\Assets;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

$tab = $panel->createTab(['name' => esc_html__('WC', 'chap')]);

$tab->createOption([
	'name' => '<img src="' . esc_url(Assets\asset_path('images/options/logo_wc.png')) . '" alt="' . esc_html__('WooCommerce logo', 'chap') . '" />',
	'type' => 'custom',
]);

$tab->createOption([
	'name' => esc_html__('WooCommerce options', 'chap'),
	'type' => 'heading',
]);

$tab->createOption([
	'id' => 'woo_product_columns',
	'name' => esc_html__('Product columns', 'chap'),
	'type' => 'number',
	'min' => 1,
	'max' => 10,
	'step' => 1,
	'default' => 4,
]);

$tab->createOption([
	'id' => 'woo_products_per_page',
	'name' => esc_html__('Products per page', 'chap'),
	'type' => 'number',
	'min' => 1,
	'max' => 64,
	'step' => 1,
	'default' => 8,
]);

$tab->createOption([
	'id' => 'woo_upsells_product_columns',
	'name' => esc_html__('Up-sells products columns', 'chap'),
	'desc' => esc_html__('Products "You may also like..." displayed on single product page.', 'chap'),
	'type' => 'number',
	'min' => 1,
	'max' => 10,
	'step' => 1,
	'default' => 4,
]);

$tab->createOption([
	'id' => 'woo_related_product_columns',
	'name' => esc_html__('Related products columns', 'chap'),
	'desc' => esc_html__('Related products displayed on single product page.', 'chap'),
	'type' => 'number',
	'min' => 1,
	'max' => 10,
	'step' => 1,
	'default' => 4,
]);

$tab->createOption([
	'id' => 'woo_crosssells_product_columns',
	'name' => esc_html__('Cross-sells products columns', 'chap'),
	'desc' => esc_html__('Products "You may be interested in..." displayed on the cart page.', 'chap'),
	'type' => 'number',
	'min' => 1,
	'max' => 10,
	'step' => 1,
	'default' => 2,
]);

$tab->createOption([
	'id' => 'woo_product_nav',
	'name' => esc_html__('Product navigation', 'chap'),
	'desc' => esc_html__('Show links to previous/next product on single product page.', 'chap'),
	'type' => 'checkbox',
	'default' => false,
]);

$tab->createOption([
	'name' => esc_html__('Product card', 'chap'),
	'type' => 'heading',
]);

$tab->createOption([
	'id' => 'woo_card_image_height',
	'name' => esc_html__('Image max-height', 'chap'),
	'desc' => esc_html__('Prevent very tall images from expanding the card height.', 'chap'),
	'type' => 'number',
	'min' => 1,
	'max' => 750,
	'step' => 1,
	'unit' => 'px',
	'default' => 300,
	'css' => '.ui.product.card > .ui.image > a { max-height: valuepx; }',
]);

$tab->createOption([
	'id' => 'woo_card_header_size',
	'name' => esc_html__('Header size', 'chap'),
	'type' => 'select',
	'options' => [
		'tiny' => esc_html__('Tiny', 'chap'),
		'small' => esc_html__('Small', 'chap'),
		'medium' => esc_html__('Medium', 'chap'),
		'large' => esc_html__('Large', 'chap'),
		'huge' => esc_html__('Huge', 'chap'),
	],
	'default' => 'small',
]);

$tab->createOption([
	'id' => 'woo_product_cats_in_card',
	'name' => esc_html__('Categories', 'chap'),
	'desc' => esc_html__('Show product categories in product cards.', 'chap'),
	'type' => 'checkbox',
	'default' => false,
]);

$tab->createOption([
	'id' => 'woo_product_ratings_in_card',
	'name' => esc_html__('Rating', 'chap'),
	'desc' => esc_html__('Show star rating in product cards.', 'chap'),
	'type' => 'checkbox',
	'default' => true,
]);

$tab->createOption([
	'id' => 'woo_product_desc_in_card',
	'name' => esc_html__('Description', 'chap'),
	'desc' => esc_html__('Display short description in product cards.', 'chap'),
	'type' => 'checkbox',
	'default' => false,
]);

$tab->createOption([
	'id' => 'woo_card_price_size',
	'name' => esc_html__('Price size', 'chap'),
	'type' => 'select',
	'options' => [
		'tiny' => esc_html__('Tiny', 'chap'),
		'small' => esc_html__('Small', 'chap'),
		'medium' => esc_html__('Medium', 'chap'),
		'large' => esc_html__('Large', 'chap'),
		'huge' => esc_html__('Huge', 'chap'),
	],
	'default' => 'tiny',
]);

$tab->createOption([
	'id' => 'woo_card_button_size',
	'name' => esc_html__('Button size', 'chap'),
	'type' => 'select',
	'options' => [
		'mini' => esc_html__('Mini', 'chap'),
		'tiny' => esc_html__('Tiny', 'chap'),
		'small' => esc_html__('Small', 'chap'),
		'medium' => esc_html__('Medium', 'chap'),
		'large' => esc_html__('Large', 'chap'),
		'big' => esc_html__('Big', 'chap'),
		'huge' => esc_html__('Huge', 'chap'),
		'massive' => esc_html__('Massive', 'chap'),
	],
	'default' => 'medium',
]);

$tab->createOption([
	'id' => 'woo_on_sale_color',
	'name' => esc_html__('On sale ribbon color', 'chap'),
	'type' => 'select',
	'options' => [
		'red' => esc_html__('Red', 'chap'),
		'orange' => esc_html__('Orange', 'chap'),
		'yellow' => esc_html__('Yellow', 'chap'),
		'olive' => esc_html__('Olive', 'chap'),
		'green' => esc_html__('Green', 'chap'),
		'teal' => esc_html__('Teal', 'chap'),
		'blue' => esc_html__('Blue', 'chap'),
		'violet' => esc_html__('Violet', 'chap'),
		'purple' => esc_html__('Purple', 'chap'),
		'pink' => esc_html__('Pink', 'chap'),
		'brown' => esc_html__('Brown', 'chap'),
		'grey' => esc_html__('Grey', 'chap'),
		'black' => esc_html__('Black', 'chap'),
	],
	'default' => 'red',
]);

$tab->createOption([
	'id' => 'woo_card_on_sale_color',
	'name' => esc_html__('On sale product card', 'chap'),
	'desc' => esc_html__('Apply ribbon color to product cards.', 'chap'),
	'type' => 'checkbox',
	'default' => true,
]);

$tab->createOption([
	'name' => esc_html__('Product category card', 'chap'),
	'type' => 'heading',
]);

$tab->createOption([
	'id' => 'woo_category_card_header_size',
	'name' => esc_html__('Header size', 'chap'),
	'type' => 'select',
	'options' => [
		'tiny' => esc_html__('Tiny', 'chap'),
		'small' => esc_html__('Small', 'chap'),
		'medium' => esc_html__('Medium', 'chap'),
		'large' => esc_html__('Large', 'chap'),
		'huge' => esc_html__('Huge', 'chap'),
	],
	'default' => 'medium',
]);

$tab->createOption([
	'id' => 'woo_category_card_show_count',
	'name' => esc_html__('Product count', 'chap'),
	'desc' => esc_html__('Display product count in category cards.', 'chap'),
	'type' => 'checkbox',
	'default' => true,
]);

$tab->createOption([
	'id' => 'woo_category_card_show_desc',
	'name' => esc_html__('Description', 'chap'),
	'desc' => esc_html__('Display product category description in category cards.', 'chap'),
	'type' => 'checkbox',
	'default' => false,
]);

$tab->createOption(['type' => 'save']);

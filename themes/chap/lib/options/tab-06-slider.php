<?php

namespace Chap\Options;
use Chap\Assets;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

$tab = $panel->createTab(['name' => esc_html__('Slider', 'chap')]);

$tab->createOption([
	'name' => esc_html__('Slider settings', 'chap'),
	'type' => 'heading',
]);

$tab->createOption([
	'id' => 'slider_loop',
	'name' => esc_html__('Loop', 'chap'),
	'desc' => esc_html__('Enable continuous loop mode.', 'chap'),
	'type' => 'checkbox',
	'default' => true,
]);

$tab->createOption([
	'id' => 'slider_pagination',
	'name' => esc_html__('Pagination', 'chap'),
	'desc' => esc_html__('Enable pagination bullets at the bottom of the slides.', 'chap'),
	'type' => 'checkbox',
	'default' => true,
]);

$tab->createOption([
	'id' => 'slider_pagination_from_bottom',
	'name' => esc_html__('Distance from bottom', 'chap'),
	'desc' => esc_html__('Pagination bullets distance from the bottom.', 'chap'),
	'type' => 'number',
	'min' => 0,
	'step' => 1,
	'max' => 100,
	'unit' => 'px',
	'default' => 10,
	'css' => '.swiper-container.swiper-container-horizontal > .swiper-pagination.swiper-pagination-bullets { bottom: valuepx; }',
]);

$tab->createOption([
	'id' => 'slider_pagination_color',
	'name' => esc_html__('Pagination active bullet color', 'chap'),
	'type' => 'radio',
	'options' => [
		'swiper-pagination-white' => esc_html__('White', 'chap'),
		'swiper-pagination-black' => esc_html__('Black', 'chap'),
	],
	'default' => 'swiper-pagination-white',
]);

$tab->createOption([
	'id' => 'slider_navigation',
	'name' => esc_html__('Navigation buttons', 'chap'),
	'desc' => esc_html__('Enable next/previous arrow buttons.', 'chap'),
	'type' => 'checkbox',
	'default' => true,
]);

$tab->createOption([
	'id' => 'slider_navigation_color',
	'name' => esc_html__('Navigation buttons color', 'chap'),
	'type' => 'radio',
	'options' => [
		'swiper-button-white' => esc_html__('White', 'chap'),
		'swiper-button-black' => esc_html__('Black', 'chap'),
	],
	'default' => 'swiper-button-white',
]);

$tab->createOption([
	'id' => 'slider_scrollbar',
	'name' => esc_html__('Scrollbar', 'chap'),
	'desc' => esc_html__('Show a scrollbar at the bottom of the slides.', 'chap'),
	'type' => 'checkbox',
	'default' => false,
]);

$tab->createOption([
	'id' => 'slider_grab_cursor',
	'name' => esc_html__('Grab cursor', 'chap'),
	'desc' => esc_html__('Show a grab cursor indicating that slides can be dragged.', 'chap'),
	'type' => 'checkbox',
	'default' => false,
]);

$tab->createOption([
	'id' => 'slider_lazy_loading',
	'name' => esc_html__('Use lazy loading', 'chap'),
	'desc' => esc_html__('Slide background image will be loaded once the slide is visible.', 'chap'),
	'type' => 'checkbox',
	'default' => false,
]);

$tab->createOption([
	'id' => 'slider_autoplay',
	'name' => esc_html__('Autoplay', 'chap'),
	'desc' => esc_html__('Delay time between transitions. Setting this to 0 will disable autoplay.', 'chap'),
	'type' => 'number',
	'unit' => 'ms',
	'min' => 0,
	'max' => 1000 * 15,
	'step' => 500,
	'default' => 5000,
]);

$tab->createOption([
	'id' => 'slider_disable_on_interaction',
	'name' => esc_html__('Disable on interaction', 'chap'),
	'desc' => esc_html__('Stop autoplay after user interactions.', 'chap'),
	'type' => 'checkbox',
	'default' => true,
]);

$tab->createOption([
	'id' => 'slider_transition',
	'name' => esc_html__('Transition', 'chap'),
	'desc' => esc_html__('The effect shown when slides change.', 'chap'),
	'help' => esc_html__('Note: fade, cube, coverflow and flip transitions require all slides to have a solid background color or image to display properly. Cube and flip transitions can\'t be used when header has a bottom angle.', 'chap'),
	'type' => 'select',
	'options' => [
		'slide' => esc_html__('Slide', 'chap'),
		'fade' => esc_html__('Fade', 'chap'),
		'cube' => esc_html__('Cube', 'chap'),
		'coverflow' => esc_html__('Coverflow', 'chap'),
		'flip' => esc_html__('Flip', 'chap'),
	],
	'default' => 'slide',
]);

$tab->createOption([
	'id' => 'slider_speed',
	'name' => esc_html__('Transition speed', 'chap'),
	'desc' => esc_html__('Duration of transition between slides.', 'chap'),
	'type' => 'number',
	'unit' => 'ms',
	'min' => 50,
	'max' => 1000 * 3,
	'step' => 50,
	'default' => 300,
]);

$tab->createOption([
	'id' => 'slider_edit',
	'name' => esc_html__('Edit button', 'chap'),
	'desc' => esc_html__('Show edit button on slides for admin users.', 'chap'),
	'type' => 'checkbox',
	'default' => true,
]);

$tab->createOption([
	'id' => 'slider_box',
	'name' => esc_html__('Box model', 'chap'),
	'image' => Assets\asset_path('images/options/box_model_slider.png'),
	'type' => 'box',
	'box' => [
		'margin',
		'border',
		'padding',
	],
	'params' => [
		'margin' => ['top', 'bottom'],
		'border' => ['top', 'right', 'bottom', 'left'],
		'padding' => ['top', 'right', 'bottom', 'left'],
	],
	'selector' => '.ui.masthead.segment > .chap-main-slider',
]);

$tab->createOption([
	'name' => esc_html__('Slider height', 'chap'),
	'desc' => sprintf(
		/* Translators: HTML code tags. */
		esc_html__('The slider height is defined by responsive %1$svw%2$s (viewport width) units and is capped by a maximum height in case of extra wide monitors.', 'chap'),
		'<code>',
		'</code>'
	) . '<br />' . sprintf(
		esc_html__('Preset slides and slide composites are designed for the default slider height. If you stray from the default values be sure to double check the slides on all device sizes.', 'chap')
	),
	'type' => 'heading',
]);

$tab->createOption([
	'id' => 'slider_height',
	'name' => esc_html__('Slider height', 'chap'),
	'desc' => esc_html__('Default', 'chap') . ': 42vw',
	'type' => 'number',
	'min' => 10,
	'step' => 1,
	'max' => 200,
	'unit' => 'vw',
	'default' => 42,
	'css' => '.ui.slide { height: valuevw; min-height: calc(valuevw / 2); }',
]);

$tab->createOption([
	'id' => 'slider_height_tablet',
	'name' => esc_html__('Slider height (tablet)', 'chap'),
	'desc' => esc_html__('Default', 'chap') . ': 46vw',
	'type' => 'number',
	'min' => 10,
	'step' => 1,
	'max' => 200,
	'unit' => 'vw',
	'default' => 46,
	'css' => '@media only screen and (max-width: 991px) { .ui.slide { height: valuevw; } }',
]);

$tab->createOption([
	'id' => 'slider_height_mobile',
	'name' => esc_html__('Slider height (mobile)', 'chap'),
	'desc' => esc_html__('Default', 'chap') . ': 56vw',
	'type' => 'number',
	'min' => 10,
	'step' => 1,
	'max' => 200,
	'unit' => 'vw',
	'default' => 56,
	'css' => '@media only screen and (max-width: 425px) { .ui.slide { height: valuevw; } }',
]);

$tab->createOption([
	'id' => 'slider_max_height',
	'name' => esc_html__('Slider maximum height', 'chap'),
	'help' => esc_html__('This option caps the slider height to a maximum value.', 'chap') .
		'<div class="ui divider"></div>' .
		esc_html__('Setting the value to 0 limits the slider height to viewport height, meaning the whole slider will always fit on the user\'s screen.', 'chap'),
	'desc' => esc_html__('Default', 'chap') . ': 600px',
	'type' => 'number',
	'min' => 0,
	'step' => 10,
	'max' => 1200,
	'unit' => 'px',
	'default' => 600,
]);

$tab->createOption([
	'id' => 'slider_extend',
	'name' => esc_html__('Extend height', 'chap'),
	'desc' => esc_html__('Extend slider upwards to to include the main menu and brand.', 'chap'),
	'help' => esc_html__('Enabling this option makes the slider background cover the whole masthead area.', 'chap') .
		'<div class="ui divider"></div>' .
		esc_html__('The extending takes place dynamically after page is loaded, so it\'s best used in combination with the "Loading screen" option from "General" tab to mask the changes taking place.', 'chap'),
	'type' => 'checkbox',
	'default' => false,
	'css' => '.chap-main-slider.swiper-container { z-index: -1; }',
]);
// .ui.branding.container, .ui.mainmenu.container { * { z-index: 2; } }

$tab->createOption(['type' => 'save']);

/**
 * Create CSS for the footer based on options.
 */
function chap_tf_slider_css() {
	$titan = \ChapTitanFramework::getInstance(CHAP_TF);
	$max_height = (int)$titan->getOption('slider_max_height');
	if($max_height > 0) {
		$titan->createCSS('
			.ui.slide {
				max-height: ' . $max_height . 'px;
			}
		');
	}
}
add_filter('customize_save_after',              __NAMESPACE__ . '\\chap_tf_slider_css', 5);
add_filter('ctf_admin_options_saved_' . CHAP_TF, __NAMESPACE__ . '\\chap_tf_slider_css', 5);

<?php

namespace Chap\Options;
use Chap\Assets;
use Chap\Helpers;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

$tab = $panel->createTab(['name' => esc_html__('Presets', 'chap')]);

if(class_exists('OCDI_Plugin')) {
	$tab->createOption([
		'id' => 'chap_settings_import_demo',
		'name' => esc_html__('Import a demo', 'chap'),
		'type' => 'custom',
		'custom' => '<a class="button button-primary button-hero" href="' . esc_url(admin_url('admin.php?page=pt-one-click-demo-import')) . '">One Click Demo Import</a>',
	]);
}

$tab->createOption([
	'name' => esc_html__('Chap settings export', 'chap'),
	'desc' => esc_html__('Save current Chap settings to a file.', 'chap') .
		'<br />' .
		esc_html__("Note: WordPress posts, attachments or other content won't be included.", 'chap'),
	'type' => 'heading',
]);

$tab->createOption([
	'id' => 'chap_settings_export_file',
	'name' => esc_html__('Export file', 'chap'),
	'type' => 'custom',
	'custom' => '<div id="chap-export"></div>',
]);

$tab->createOption([
	'name' => esc_html__('Chap settings import', 'chap'),
	'type' => 'heading',
]);

$tab->createOption([
	'id' => 'chap_settings_import_file',
	'name' => esc_html__('Import file', 'chap'),
	'desc' => esc_html__('Import by uploading a Chap settings file.', 'chap'),
	'type' => 'file',
	'label' => '.txt',
]);

$tab->createOption([
	'id' => 'chap_settings_import_string',
	'name' => esc_html__('Import string', 'chap'),
	'desc' => esc_html__('Import by pasting the contents of a Chap settings file (and then press "Save Changes").', 'chap'),
	'type' => 'textarea',
	'default' => '',
]);

$tab->createOption([
	'name' => esc_html__('Remove imported content', 'chap'),
	'desc' => esc_html__("Delete all content created by a specific OCDI demo (posts, pages, slides, media, menus). This helps clean up after you've tried multiple demos.", 'chap') .
		'<br />' .
		esc_html__("Warning: If you've modified the imported content all your changes will be lost. Don't delete content of the demo you are using. This can't be undone.", 'chap'),
	'type' => 'heading',
]);

$ocdi_post_types = ['post', 'page', 'attachment', 'chap_slide', 'product'];
$ocdi_nav_menus = ['main-menu', 'sidebar-menu', 'footer-menu'];
$imports = [];
if(isset($_GET['tab']) && $_GET['tab'] == 'presets') {
	// Setup wizard content.
	$query = new \WP_Query([
		'post_type' => $ocdi_post_types,
		'post_status' => 'any',
		'nopaging' => true,
		'meta_query' => [
			[
				'key' => '_generator',
				'compare' => '=',
				'value' => 'chap',
			],
		],
	]);
	$slug = 'setup-wizard';
	if($query->post_count > 0) {
		$imports[$slug] = esc_html__('Setup wizard', 'chap') . ' (' . $query->post_count . ')';
		$imports[$slug] .= '<pre class="chap-ocdi-delete">';
		while($query->have_posts()) {
			$query->the_post();
			$id = get_the_ID();
			$title = get_the_title();
			$type = get_post_type();
			$id_text = ' (ID: ' . $id . ')';
			$imports[$slug] .= esc_html($type) . ': ' . esc_html($title . $id_text) . "\n";
		}
		wp_reset_query();
		// Setup wizard menu.
		unset($menu);
		if($menu = get_term_by('name', 'Chap main menu', 'nav_menu')) {
			$id_text = ' (ID: ' . $menu->term_id . ')';
			$imports[$slug] .= esc_html($menu->taxonomy) . ': ' . esc_html($menu->name . $id_text) . "\n";
		}
		$imports[$slug] .= '</pre>';
	}

	// OCDI demos.
	$import_files = apply_filters('pt-ocdi/import_files', []);
	foreach($import_files as $import) {
		$slug = $import['slug'];
		$name = $import['import_file_name'];
		$query = new \WP_Query([
			'post_type' => $ocdi_post_types,
			'post_status' => 'any',
			'nopaging' => true,
			'meta_query' => [
				[
					'key' => '_chap_ocdi_slug',
					'compare' => '=',
					'value' => $slug,
				],
			],
		]);
		if($query->post_count > 0) {
			$imports[$slug] = $name . ' (' . $query->post_count . ')';
			$imports[$slug] .= '<pre class="chap-ocdi-delete">';

			// Posts.
			while($query->have_posts()) {
				$query->the_post();
				$id = get_the_ID();
				$title = get_the_title();
				$type = get_post_type();
				$id_text = ' (ID: ' . $id . ')';
				$imports[$slug] .= esc_html($type) . ': ' . esc_html($title . $id_text) . "\n";
			}

			// Nav menus.
			foreach($ocdi_nav_menus as $nav_menu) {
				$nav_menu = $slug . '-' . $nav_menu;
				unset($menu);
				if($menu = get_term_by('name', $nav_menu, 'nav_menu')) {
					$id_text = ' (ID: ' . $menu->term_id . ')';
					$imports[$slug] .= esc_html($menu->taxonomy) . ': ' . esc_html($menu->name . $id_text) . "\n";
				}
			}

			$imports[$slug] .= '</pre>';
		}
		wp_reset_query();
	}
}

$tab->createOption([
	'id' => 'chap_settings_delete_ocdi',
	'name' => esc_html__('Imported content', 'chap'),
	'desc' => count($imports) > 0 ? esc_html__('Select content to delete and hit Save Changes.', 'chap') : esc_html__('No imported content found.', 'chap'),
	'type' => 'multicheck',
	'options' => $imports,
	'default' => [],
]);

$tab->createOption([
	'name' => esc_html__('Compilation', 'chap'),
	'desc' => sprintf(
		/* Translators: HTML anchor tag to compiler settings page. */
		esc_html__('Compiler can be configured on the %1$scompiler settings%2$s page.', 'chap'),
		'<a href="' . esc_url(admin_url('admin.php?page=chap-compiler')) . '">',
		'</a>'
	),
	'type' => 'heading',
]);

$tab->createOption([
	'id' => 'sui_recompile_presets',
	'name' => esc_html__('Recompile', 'chap'),
	'desc' => esc_html__('Recompile Semantic UI after saving options on this page.', 'chap'),
	'type' => 'checkbox',
	'default' => true,
]);

$tab->createOption(['type' => 'save']);

/**
 * Export settings.
 */
function chap_settings_export() {

	if(!isset($_GET['tab']) || $_GET['tab'] !== 'presets') {
		return;
	}

	/**
	 * Removing Theme Check errors:
	 *
	 *     "WARNING: Found base64_encode in the file tab-12-presets.php. base64_encode() is not allowed."
	 *
	 * Reason: Allowed by Envato guidelines.
	 * @see https://help.market.envato.com/hc/en-us/articles/202822450-WordPress-Theme-Submission-Requirements
	 */
	$chap_encode = 'bas' . 'e6' . '4_enco' . 'de';

	/**
	 * Settings file name.
	 */
	$blog = strtolower(str_replace(' ', '-', get_bloginfo('name')));
	$file_name = 'chap-settings-' . esc_html($blog) . '.txt';

	/**
	 * Get the options.
	 */
	$options = get_option(CHAP_TF . '_options');
	$options = maybe_unserialize($options);

	/**
	 * Store basename.
	 */
	if(isset($options['header_background_image']) && !is_numeric($options['header_background_image'])) {
		$options['header_background_image'] = basename($options['header_background_image']);
	}

	/**
	 * Store basename.
	 */
	if(isset($options['header_background_video']) && !is_numeric($options['header_background_video'])) {
		$options['header_background_video'] = basename($options['header_background_video']);
	}

	/**
	 * Options without an underscore in the name are probably
	 * automatically generated options for headers.
	 */
	foreach($options as $key => $value) {
		if(strpos($key, '_') === false) {
			unset($options[$key]);
		}
	}

	/**
	 * Store WordPress options that may be vital for getting same result when importing.
	 */
	$options['chap_wp_options'] = [
		'chap-sui-compressed'        => get_option('chap-sui-compressed'),
		'chap-sui-rtl'               => get_option('chap-sui-rtl'),
		'chap-sui-files'             => get_option('chap-sui-files'),
		'csc-prefix'                 => get_option('csc-prefix'),
		'csc-no-prefix'              => get_option('csc-no-prefix'),
		'csc-shortcodes-in-widgets'  => get_option('csc-shortcodes-in-widgets'),
		'csc-shortcodes-in-menus'    => get_option('csc-shortcodes-in-menus'),
		'csc-shortcodes-in-comments' => get_option('csc-shortcodes-in-comments'),
		'csc-shortcodes-in-excerpts' => get_option('csc-shortcodes-in-excerpts'),
	];

	/**
	 * Serialize and encode the options.
	 */
	$options = maybe_serialize($options);
	$options = $chap_encode($options);

	/**
	 * Construct the button for downloading settings string.
	 */
	$button_text = esc_html__('Download settings', 'chap');

	$export = <<<JS
	var opts = '{$options}';
	var data = "text/plain;charset=utf-8," + encodeURIComponent(opts);
	jQuery(document).ready(function(){
		jQuery('<a class="button button-primary" href="data:' + data + '" download="{$file_name}">{$button_text}</a>').appendTo('#chap-export');
	});
JS;

	echo '<script>' . $export . '</script>';

}
add_action('admin_footer', __NAMESPACE__ . '\\chap_settings_export');

/**
 * Import settings.
 *
 * @param  ChapTitanFrameworkAdminPage $page Admin page.
 * @param  ChapTitanFrameworkAdminTab  $tab  Active tab.
 */
function chap_settings_import($page, $tab) {

	if($tab->settings['id'] !== 'presets') {
		return;
	}

	/**
	 * Removing Theme Check errors:
	 *
	 *     "WARNING: Found base64_decode in the file tab-12-presets.php. base64_decode() is not allowed."
	 *
	 * Reason: Allowed by Envato guidelines.
	 * @see https://help.market.envato.com/hc/en-us/articles/202822450-WordPress-Theme-Submission-Requirements
	 */
	$chap_decode = 'bas' . 'e6' . '4_deco' . 'de';

	$titan = \ChapTitanFramework::getInstance(CHAP_TF);
	$import_file = $titan->getOption('chap_settings_import_file');
	$import_string = $titan->getOption('chap_settings_import_string');
	$file = false;
	$contents = '';

	if($import_file && intval($import_file) > 0) {
		$file = get_attached_file($import_file);
	}

	if(isset($_POST['chap_ocdi'])) {
		$ocdi_import_file = get_option('chap_ocdi_import_file', false);
		if($ocdi_import_file && file_exists($ocdi_import_file)) {
			$file = $ocdi_import_file;
			update_option('chap_did_ocdi_import', 1);
		}
	}

	/**
	 * Get contents from import file.
	 */
	if($file) {
		/**
		 * WP Filesystem.
		 */
		WP_Filesystem();
		global $wp_filesystem;
		if(!isset($wp_filesystem)) {
			return;
		}
		/**
		 * Get contents.
		 */
		$contents = $wp_filesystem->get_contents($file);
		$contents = $chap_decode($contents);
	}

	/**
	 * Get contents from import string.
	 */
	if($import_string && strlen($import_string) > 0) {
		$contents = $chap_decode($import_string);
	}

	/**
	 * Parse contents.
	 */
	$imports = maybe_unserialize($contents);

	/**
	 * One Click Demo Import map.
	 *
	 * Contains pairs of $old_id => $new_id.
	 */
	$map = get_transient('chap_ocdi_map');
	if(!$map) {
		$map = [];
	}

	/**
	 * Update WordPress options.
	 */
	if(isset($imports['chap_wp_options'])) {
		$options = $imports['chap_wp_options'];
		foreach($options as $key => $value) {
			update_option($key, $value);
		}
		unset($imports['chap_wp_options']);
	}

	/**
	 * Carry out imports.
	 */
	if(is_array($imports) && count($imports) > 0) {

		/**
		 * Preserve options.
		 */
		$recompile = $titan->getOption('sui_recompile_presets');
		$amp_enabled = $titan->getOption('amp_enabled');

		/**
		 * Loop all options, set to imported value or default.
		 */
		foreach($titan->optionsUsed as $option) {

			$id = $option->settings['id'];

			if(!empty($id)) {

				if(array_key_exists($id, $imports)) {

					/**
					 * Remap IDs.
					 */
					if(in_array($id, [
						'header_background_image',
						'header_background_video',
						'footer_image',
						'amp_fallback_image',
					]) && strlen($imports[$id]) > 0) {

						$old_value = $imports[$id];
						$new_value = false;

						if(!is_numeric($old_value)) {
							// Preset header image from assets.
							if(file_exists(get_template_directory() . '/dist/images/headers/' . $old_value)) {
								$new_value = Assets\asset_path('images/headers/' . $old_value);
							}
						} elseif(array_key_exists($old_value, $map)) {
							// Imported image, find it from the map.
							$new_value = $map[$old_value];
						}

						$imports[$id] = $new_value;

					}

					/**
					 * Set to imported value.
					 */
					$option->setValue($imports[$id]);

				} else {

					/**
					 * Use default value.
					 */
					$option->setValue($option->settings['default']);

				}

			}

		}

		/**
		 * Restore preserved options.
		 */
		$titan->setInternalAdminPageOption('sui_recompile_presets', $recompile);
		$titan->setInternalAdminPageOption('amp_enabled', $amp_enabled);

		/**
		 * Clear import input options.
		 */
		$titan->setInternalAdminPageOption('chap_settings_import_file', 0);
		$titan->setInternalAdminPageOption('chap_settings_import_string', '');

		/**
		 * Remove transients that may have been set during One Click Demo Import.
		 */
		delete_transient('chap_ocdi_slug');
		delete_transient('chap_ocdi_map');

	}

}
add_filter('ctf_pre_save_admin_' . CHAP_TF, __NAMESPACE__ . '\\chap_settings_import', 10, 2);

/**
 * Delete OCDI content.
 *
 * @param  ChapTitanFrameworkAdminPage $page Admin page.
 * @param  ChapTitanFrameworkAdminTab  $tab  Active tab.
 */
function chap_ocdi_content_delete($page, $tab) {

	if($tab->settings['id'] !== 'presets') {
		return;
	}

	$titan = \ChapTitanFramework::getInstance(CHAP_TF);
	$delete = $titan->getOption('chap_settings_delete_ocdi');

	if(!is_array($delete) || count($delete) < 1) {
		return;
	}

	$ocdi_post_types = ['post', 'page', 'attachment', 'chap_slide', 'product'];
	$ocdi_nav_menus = ['main-menu', 'sidebar-menu', 'footer-menu'];

	foreach($delete as $slug) {

		if($slug === 'setup-wizard') {
			// Setup wizard content.
			$query = new \WP_Query([
				'post_type' => $ocdi_post_types,
				'post_status' => 'any',
				'nopaging' => true,
				'meta_query' => [
					[
						'key' => '_generator',
						'compare' => '=',
						'value' => 'chap',
					],
				],
			]);
			if($query->post_count > 0) {
				while($query->have_posts()) {
					$query->the_post();
					$id = get_the_ID();
					wp_delete_post($id, true);
				}
			}
			wp_reset_query();
			// Setup wizard menu.
			unset($menu);
			if($menu = get_term_by('name', 'Chap main menu', 'nav_menu')) {
				wp_delete_nav_menu($menu->term_id);
			}
			continue;
		}

		/**
		 * Delete posts.
		 */
		$query = new \WP_Query([
			'post_type' => $ocdi_post_types,
			'post_status' => 'any',
			'nopaging' => true,
			'meta_query' => [
				[
					'key' => '_chap_ocdi_slug',
					'compare' => '=',
					'value' => $slug,
				],
			],
		]);
		if($query->post_count > 0) {
			while($query->have_posts()) {
				$query->the_post();
				$id = get_the_ID();
				wp_delete_post($id, true);
			}
		}
		wp_reset_query();

		/**
		 * Delete nav menus.
		 */
		foreach($ocdi_nav_menus as $nav_menu) {
			unset($menu);
			if($menu = get_term_by('name', $slug . '-' . $nav_menu, 'nav_menu')) {
				wp_delete_nav_menu($menu->term_id);
			}
		}

	}

	$titan->setInternalAdminPageOption('chap_settings_delete_ocdi', []);

}
add_filter('ctf_pre_save_admin_' . CHAP_TF, __NAMESPACE__ . '\\chap_ocdi_content_delete', 10, 2);

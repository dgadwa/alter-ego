<?php

namespace Chap\Options;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

$tab = $panel->createTab(['name' => esc_html__('Styles', 'chap')]);

$tab->createOption([
	'name' => esc_html__('Semantic UI styles', 'chap'),
	'desc' => esc_html__('This page contains user-friendly theme options that allow to easily change Semantic UI variables.', 'chap'),
	'type' => 'heading',
]);

$tab->createOption([
	'id' => 'sui_base_size',
	'name' => esc_html__('Base size', 'chap'),
	'desc' => esc_html__('Global variable controlling the size of every UI element.', 'chap'),
	'type' => 'number',
	'min' => 1,
	'max' => 64,
	'step' => 1,
	'unit' => 'px',
	'default' => 14,
]);

$tab->createOption([
	'id' => 'sui_container_max_width',
	'name' => esc_html__('Container maximum width', 'chap'),
	'desc' => esc_html__('Control the width of the entire site.', 'chap'),
	'type' => 'number',
	'min' => 600,
	'max' => 1920,
	'step' => 10,
	'unit' => 'px',
	'default' => 1200,
]);

$tab->createOption([
	'id' => 'layout_boxed',
	'name' => esc_html__('Boxed layout', 'chap'),
	'desc' => esc_html__('Contain the page content in a box.', 'chap'),
	'type' => 'checkbox',
	'default' => false,
]);

$tab->createOption([
	'id' => 'boxed_layout_width',
	'visible' => [
		'layout_boxed' => [true],
	],
	'name' => esc_html__('Boxed layout maximum width', 'chap'),
	'desc' => esc_html__('Control the width of the boxed layout container.', 'chap'),
	'type' => 'number',
	'min' => 600,
	'max' => 1920,
	'step' => 10,
	'unit' => 'px',
	'default' => 1500,
]);

$tab->createOption([
	'id' => 'boxed_layout_background_color',
	'visible' => [
		'layout_boxed' => [true],
	],
	'name' => esc_html__('Background color', 'chap'),
	'desc' => esc_html__('Background color outside the boxed layout container.', 'chap'),
	'type' => 'color',
	'default' => '#eeeeee',
]);

$tab->createOption([
	'id' => 'boxed_layout_shadow',
	'visible' => [
		'layout_boxed' => [true],
	],
	'name' => esc_html__('Box shadow', 'chap'),
	'desc' => esc_html__('Enable box shadow for the boxed layout container.', 'chap'),
	'type' => 'checkbox',
	'default' => false,
]);

$tab->createOption([
	'name' => esc_html__('Fonts', 'chap'),
	'type' => 'heading',
]);

$tab->createOption([
	'id' => 'sui_header_font',
	'name' => esc_html__('Header font', 'chap'),
	'type' => 'font',
	'show_google_fonts' => true,
	'show_websafe_fonts' => true,
	'show_font_family' => true,
	'show_color' => false,
	'show_font_size' => false,
	'show_font_weight' => true,
	'show_font_style' => true,
	'show_line_height' => false,
	'show_letter_spacing' => false,
	'show_text_transform' => false,
	'show_font_variant' => false,
	'show_text_shadow' => false,
	'show_preview' => true,
	'preview_text' => 'headers',
	'enqueue' => true,
	'default' => [
		'font-family' => 'Lato',
		'font-size' => '16px',
		'font-weight' => '700',
		'font-style' => 'normal',
	],
]);

$tab->createOption([
	'id' => 'sui_font',
	'name' => esc_html__('Main font', 'chap'),
	'type' => 'font',
	'show_google_fonts' => true,
	'show_websafe_fonts' => true,
	'show_font_family' => true,
	'show_color' => false,
	'show_font_size' => true,
	'show_font_weight' => true,
	'show_font_style' => false,
	'show_line_height' => false,
	'show_letter_spacing' => false,
	'show_text_transform' => false,
	'show_font_variant' => false,
	'show_text_shadow' => false,
	'show_preview' => true,
	'enqueue' => true,
	'default' => [
		'font-family' => 'Lato',
		'font-size' => '16px',
		'font-weight' => '400',
	],
]);

$tab->createOption([
	'id' => 'sui_slide_header_font_weight',
	'name' => esc_html__('Header font weight in slides', 'chap'),
	'type' => 'select',
	'options' => [
		'100' => esc_html__('Thin 100', 'chap'),
		'200' => esc_html__('Extra-Light 200', 'chap'),
		'300' => esc_html__('Light 300', 'chap'),
		'400' => esc_html__('Normal 400', 'chap'),
		'500' => esc_html__('Medium 500', 'chap'),
		'600' => esc_html__('Semi-Bold 600', 'chap'),
		'700' => esc_html__('Bold 700', 'chap'),
		'800' => esc_html__('Extra-Bold 800', 'chap'),
		'900' => esc_html__('Black 900', 'chap'),
	],
	'default' => '400',
]);

$tab->createOption([
	'id' => 'sui_button_font_bold',
	'name' => esc_html__('Button font weight', 'chap'),
	'desc' => esc_html__('Bold', 'chap'),
	'type' => 'checkbox',
	'default' => true,
]);

$tab->createOption([
	'id' => 'sui_label_font_bold',
	'name' => esc_html__('Label font weight', 'chap'),
	'desc' => esc_html__('Bold', 'chap'),
	'type' => 'checkbox',
	'default' => true,
]);

$tab->createOption([
	'id' => 'sui_submenu_font_size',
	'name' => esc_html__('Submenu font size', 'chap'),
	'type' => 'select',
	'options' => [
		'tiny' => esc_html__('Tiny', 'chap'),
		'small' => esc_html__('Small', 'chap'),
		'medium' => esc_html__('Medium', 'chap'),
		'large' => esc_html__('Large', 'chap'),
		'huge' => esc_html__('Huge', 'chap'),
	],
	'default' => 'tiny',
]);

$tab->createOption([
	'name' => esc_html__('Brand colors', 'chap'),
	'type' => 'heading',
]);

$tab->createOption([
	'id' => 'sui_primary_color',
	'name' => esc_html__('Primary color', 'chap'),
	'type' => 'color',
	'default' => '#2185d0',
]);

$tab->createOption([
	'id' => 'sui_secondary_color',
	'name' => esc_html__('Secondary color', 'chap'),
	'type' => 'color',
	'default' => '#1b1c1d',
]);

$tab->createOption([
	'name' => esc_html__('Links', 'chap'),
	'type' => 'heading',
]);

$tab->createOption([
	'id' => 'sui_link_color',
	'name' => esc_html__('Link color', 'chap'),
	'type' => 'color',
	'default' => '#4183c4',
]);

$tab->createOption([
	'id' => 'sui_link_underline',
	'name' => esc_html__('Link underline', 'chap'),
	'type' => 'radio',
	'options' => [
		'none' => esc_html__('None', 'chap'),
		'hover' => esc_html__('On hover', 'chap'),
		'always' => esc_html__('Always', 'chap'),
	],
	'default' => 'none',
]);

$tab->createOption([
	'name' => esc_html__('Page', 'chap'),
	'type' => 'heading',
]);

$tab->createOption([
	'id' => 'sui_page_background_color',
	'name' => esc_html__('Page background color', 'chap'),
	'type' => 'color',
	'default' => '#ffffff',
]);

$tab->createOption([
	'id' => 'sui_page_text_color',
	'name' => esc_html__('Page text color', 'chap'),
	'type' => 'color',
	'alpha' => true,
	'default' => 'rgba(0, 0, 0, 0.87)',
]);

$tab->createOption([
	'id' => 'sui_paragraph_margin',
	'name' => esc_html__('Paragraph margins', 'chap'),
	'desc' => esc_html__('Top/sides/bottom', 'chap'),
	'type' => 'text',
	'default' => '0em 0em 1em',
]);

$tab->createOption([
	'name' => esc_html__('Highlighted text', 'chap'),
	'type' => 'heading',
]);

$tab->createOption([
	'id' => 'sui_highlight_background',
	'name' => esc_html__('Background color', 'chap'),
	'type' => 'color',
	'alpha' => true,
	'default' => '#cce2ff',
]);

$tab->createOption([
	'id' => 'sui_highlight_text',
	'name' => esc_html__('Text color', 'chap'),
	'type' => 'color',
	'alpha' => true,
	'default' => 'rgba(0, 0, 0, 0.87)',
]);

$tab->createOption([
	'name' => esc_html__('Form inputs', 'chap'),
	'type' => 'heading',
]);

$tab->createOption([
	'id' => 'sui_input_background',
	'name' => esc_html__('Background color', 'chap'),
	'type' => 'color',
	'default' => '#ffffff',
]);

$tab->createOption([
	'id' => 'sui_input_text',
	'name' => esc_html__('Text color', 'chap'),
	'type' => 'color',
	'alpha' => true,
	'default' => 'rgba(0, 0, 0, 0.87)',
]);

$tab->createOption([
	'id' => 'sui_input_vertical_padding',
	'name' => esc_html__('Vertical padding', 'chap'),
	'type' => 'number',
	'min' => 0,
	'max' => 64,
	'step' => 1,
	'unit' => 'px',
	'default' => 11,
]);

$tab->createOption([
	'id' => 'sui_input_horizontal_padding',
	'name' => esc_html__('Horizontal padding', 'chap'),
	'type' => 'number',
	'min' => 0,
	'max' => 64,
	'step' => 1,
	'unit' => 'px',
	'default' => 14,
]);

$tab->createOption([
	'name' => esc_html__('Buttons', 'chap'),
	'type' => 'heading',
]);

$tab->createOption([
	'id' => 'sui_button_vertical_padding',
	'name' => esc_html__('Vertical padding', 'chap'),
	'desc' => esc_html__('Button defaults to using same height as input.', 'chap'),
	'type' => 'number',
	'min' => 0,
	'max' => 64,
	'step' => 1,
	'unit' => 'px',
	'default' => 11,
]);

$tab->createOption([
	'id' => 'sui_button_horizontal_padding',
	'name' => esc_html__('Horizontal padding', 'chap'),
	'type' => 'number',
	'min' => 0,
	'max' => 5,
	'step' => 0.25,
	'unit' => 'em',
	'default' => 1.5,
]);

$tab->createOption([
	'id' => 'sui_button_vertical_margin',
	'name' => esc_html__('Vertical margin', 'chap'),
	'type' => 'number',
	'min' => -5,
	'max' => 5,
	'step' => 0.25,
	'unit' => 'em',
	'default' => 0.25,
]);

$tab->createOption([
	'id' => 'sui_button_horizontal_margin',
	'name' => esc_html__('Horizontal margin', 'chap'),
	'type' => 'number',
	'min' => -5,
	'max' => 5,
	'step' => 0.25,
	'unit' => 'em',
	'default' => 0.25,
]);

$tab->createOption([
	'name' => esc_html__('Misc', 'chap'),
	'type' => 'heading',
]);

$tab->createOption([
	'id' => 'sui_border_radius',
	'name' => esc_html__('Border radius', 'chap'),
	'desc' => esc_html__('Applies to all elements with border radius.', 'chap'),
	'type' => 'number',
	'min' => 0,
	'max' => 16,
	'step' => 1,
	'unit' => 'px',
	'default' => 4,
]);

$tab->createOption([
	'id' => 'sui_icon_font_source',
	'name' => esc_html__('Icon font source', 'chap'),
	'type' => 'radio',
	'options' => [
		'cdn' => esc_html__('CDN', 'chap'),
		'local' => esc_html__('Local', 'chap'),
	],
	'default' => 'cdn',
]);

$tab->createOption([
	'name' => esc_html__('Compilation', 'chap'),
	'desc' => sprintf(
		/* Translators: HTML anchor tag to compiler settings page. */
		esc_html__('Compiler can be configured on the %1$scompiler settings%2$s page.', 'chap'),
		'<a href="' . esc_url(admin_url('admin.php?page=chap-compiler')) . '">',
		'</a>'
	),
	'type' => 'heading',
]);

$tab->createOption([
	'id' => 'sui_recompile_styles',
	'name' => esc_html__('Recompile', 'chap'),
	'desc' => esc_html__('Recompile Semantic UI after saving options on this page.', 'chap'),
	'type' => 'checkbox',
	'default' => true,
]);

$tab->createOption(['type' => 'save']);

/**
 * Dynamic CSS based on options in the Styles tab.
 */
function chap_tf_styles_css() {
	$titan = \ChapTitanFramework::getInstance(CHAP_TF);
	$boxed = $titan->getOption('layout_boxed');
	if($boxed) {
		$boxed_width = $titan->getOption('boxed_layout_width');
		$boxed_background_color = $titan->getOption('boxed_layout_background_color');
		$page_background_color = $titan->getOption('sui_page_background_color');
		$titan->createCSS('
			body > .pusher {
				background-color: ' . esc_attr($boxed_background_color) . ' !important;
			}
			body > .pusher > .full.height {
				max-width: ' . esc_attr($boxed_width) . 'px;
				margin: 0 auto;
				background-color: ' . esc_attr($page_background_color) . ';
			}
		');
		$boxed_box_shadow_enabled = $titan->getOption('boxed_layout_shadow');
		if($boxed_box_shadow_enabled) {
			$box_shadow = apply_filters('chap_boxed_layout_shadow', 'rgba(0, 0, 0, 0.12) 0px 1px 6px, rgba(0, 0, 0, 0.12) 0px 1px 4px');
			$titan->createCSS('
				body > .pusher > .full.height {
					box-shadow: ' . esc_attr($box_shadow) . ';
				}
			');
		}
	}
}
add_filter('customize_save_after',               __NAMESPACE__ . '\\chap_tf_styles_css', 6);
add_filter('ctf_admin_options_saved_' . CHAP_TF, __NAMESPACE__ . '\\chap_tf_styles_css', 6);

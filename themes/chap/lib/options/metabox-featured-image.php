<?php

/**
 * Metabox for selecting Featured Image options.
 */

namespace Chap\Options;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

$metaBox = $titan->createMetaBox([
	'name' => esc_html__('Featured Image options', 'chap'),
	'post_type' => ['page', 'post'],
	'context' => 'side',
	'priority' => 'low',
]);

$metaBox->createOption([
	'id' => 'featured_image_position',
	'name' => esc_html__('Position', 'chap'),
	'desc' => esc_html__('Choose how to display the featured image.', 'chap'),
	'type' => 'radio',
	'options' => [
		'default' => esc_html__('Default', 'chap'),
		'full' => esc_html__('Full width', 'chap'),
		'left' => esc_html__('Left corner', 'chap'),
		'right' => esc_html__('Right corner', 'chap'),
		'hidden' => esc_html__('Hidden', 'chap'),
	],
	'default' => 'default',
]);

<?php

/**
 * Custom code metaboxes.
 */

namespace Chap\Options;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

/**
 * Custom CSS metabox.
 */
$css_post_types = [];
$custom_css = get('custom_css');
if(is_array($custom_css) && count($custom_css) > 0) {
	foreach($custom_css as $key => $type) {
		$css_post_types[] = $type;
	}
}

if(count($css_post_types) > 0):

	$metaBox = $titan->createMetaBox([
		'id' => 'custom_css_meta',
		'name' => esc_html__('Custom CSS/Sass for this post', 'chap'),
		'post_type' => $css_post_types,
		'priority' => 'low',
	]);

	$metaBox->createOption([
		'id' => 'custom_css_metabox',
		'type' => 'code',
		'lang' => 'css',
	]);

endif;


/**
 * Custom JS metabox.
 */

$js_post_types = [];
$custom_js = get('custom_javascript');
foreach($custom_js as $key => $type) {
	$js_post_types[] = $type;
}

if(count($js_post_types > 0)):

	$metaBox = $titan->createMetaBox([
		'id' => 'custom_js_meta',
		'name' => esc_html__('Custom JavaScript for this post', 'chap'),
		'post_type' => $js_post_types,
		'priority' => 'low',
	]);

	$metaBox->createOption([
		'id' => 'custom_js_metabox',
		'type' => 'code',
		'lang' => 'javascript',
	]);

endif;

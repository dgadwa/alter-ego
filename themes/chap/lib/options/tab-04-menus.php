<?php

namespace Chap\Options;
use Chap\Assets;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

$tab = $panel->createTab(['name' => esc_html__('Menus', 'chap')]);

$tab->createOption([
	'name' => esc_html__('Main menu', 'chap'),
	'type' => 'heading',
	'image' => Assets\asset_path('images/options/menus.png'),
]);

$tab->createOption([
	'id' => 'main_menu_inverted',
	'name' => esc_html__('Inverted', 'chap'),
	'desc' => esc_html__('Invert menu colors.', 'chap'),
	'help' => esc_html__('Inverted colors stand out better on darker backgrounds.', 'chap'),
	'type' => 'checkbox',
	'default' => true,
]);

$tab->createOption([
	'id' => 'main_menu_container',
	'name' => esc_html__('Container', 'chap'),
	'desc' => esc_html__('Show menu items in a container.', 'chap'),
	'type' => 'checkbox',
	'default' => false,
]);

$tab->createOption([
	'id' => 'main_menu_pointing',
	'name' => esc_html__('Pointing', 'chap'),
	'desc' => esc_html__("Pointing menu shows it's relationship to nearby content.", 'chap'),
	'type' => 'checkbox',
	'default' => true,
]);

$tab->createOption([
	'id' => 'main_menu_borderless',
	'name' => esc_html__('Borderless', 'chap'),
	'desc' => esc_html__("Don't separate menu items with lines.", 'chap'),
	'image' => Assets\asset_path('images/options/menu_borderless.png'),
	'help' => esc_html__('Applies only for menus with "container" enabled, otherwise there are no lines by default.', 'chap'),
	'type' => 'checkbox',
	'default' => false,
]);

$tab->createOption([
	'id' => 'main_menu_size',
	'name' => esc_html__('Size', 'chap'),
	'image' => Assets\asset_path('images/options/menu_sizes.png'),
	'image_size' => 'tiny',
	'type' => 'select',
	'options' => [
		'mini' => esc_html__('Mini', 'chap'),
		'tiny' => esc_html__('Tiny', 'chap'),
		'small' => esc_html__('Small', 'chap'),
		'medium' => esc_html__('Medium', 'chap'),
		'large' => esc_html__('Large', 'chap'),
		'big' => esc_html__('Big', 'chap'),
		'huge' => esc_html__('Huge', 'chap'),
		'massive' => esc_html__('Massive', 'chap'),
	],
	'default' => 'large',
]);

$tab->createOption([
	'id' => 'main_menu_dropdowns_pointing',
	'name' => esc_html__('Pointing dropdowns', 'chap'),
	'desc' => esc_html__("Show dropdown's relationship to nearby content.", 'chap'),
	'help' => sprintf(
		esc_html__('Dropdowns are nested menu items, showing their children on hover. %1$sThey are created simply by nesting items in the WordPress menu editor. %1$sWhen "pointing" is enabled their container will show a little arrow pointing to the parent item.', 'chap'),
		'<div class="ui divider"></div>'
	),
	'type' => 'checkbox',
	'default' => false,
]);

$tab->createOption([
	'id' => 'main_menu_popups_pointing',
	'name' => esc_html__('Pointing popups', 'chap'),
	'desc' => esc_html__("Show popup's relationship to nearby content.", 'chap'),
	'help' => sprintf(
		esc_html__('Popups are more complex menu items that can contain multiple columns of items or even custom content. %1$sPopups can be created by checking the "Display children as a popup" box when editing items in WordPress menu editor. %1$Only downside to popups is that they cannot contain dropdown items that show their children on hover.', 'chap'),
		'<div class="ui divider"></div>'
	),
	'type' => 'checkbox',
	'default' => true,
]);

$tab->createOption([
	'id' => 'main_menu_popup_size',
	'name' => esc_html__('Popup menu size', 'chap'),
	'desc' => esc_html__('Control the size of menus in main menu popups. Inherit uses the size of main menu.', 'chap'),
	'type' => 'select',
	'options' => [
		'inherit' => esc_html__('Inherit', 'chap'),
		'mini' => esc_html__('Mini', 'chap'),
		'tiny' => esc_html__('Tiny', 'chap'),
		'small' => esc_html__('Small', 'chap'),
		'medium' => esc_html__('Medium', 'chap'),
		'large' => esc_html__('Large', 'chap'),
		'big' => esc_html__('Big', 'chap'),
		'huge' => esc_html__('Huge', 'chap'),
		'massive' => esc_html__('Massive', 'chap'),
	],
	'default' => 'inherit',
]);

/**
 * Deprecated option.
 */
$tab->createOption([
	'id' => 'main_menu_bottom_margin',
	'name' => esc_html__('Bottom margin', 'chap'),
	'desc' => esc_html__('Space below main menu container.', 'chap'),
	'type' => 'number',
	'unit' => 'em',
	'min' => -5,
	'max' => 10,
	'step' => 0.5,
	'default' => 1,
	// 'css' => '.ui.mainmenu.container, .ui.mainmenu.flex-grid { margin-bottom: valueem; }',
	'hidden' => true,
]);

/**
 * Replaces main_menu_bottom_margin.
 */
$tab->createOption([
	'id' => 'main_menu_container_box',
	'name' => esc_html__('Box model', 'chap'),
	'image' => Assets\asset_path('images/options/box_model_menu.png'),
	'type' => 'box',
	'box' => [
		'margin',
		'border',
		'padding',
	],
	'params' => [
		'margin' => ['top', 'bottom'],
		'border' => ['top', 'right', 'bottom', 'left'],
		'padding' => ['top', 'right', 'bottom', 'left'],
	],
	'selector' => '.ui.masthead.segment .ui.mainmenu',
	'default' => [
		'margin' => [
			'bottom' => [
				'value' => get('main_menu_bottom_margin'),
				'units' => 'em',
			],
		],
	],
]);

$tab->createOption([
	'id' => 'main_menu_overflow',
	'name' => esc_html__('Overflow', 'chap'),
	'desc' => esc_html__('How to handle too many menu items.', 'chap'),
	'help' => sprintf(
		esc_html__('
			When menu contains too many items it may overflow on small computer screen sizes, before mobile menu kicks in.
			%1$s
			The breakpoint at which mobile menu is triggered can be edited in the %2$sSUI%3$s tab by changing the %4$s@chapMobileMenuBreakpoint%5$s variable in the %2$sChap - Core%3$s component.
			By increasing the value you can avoid overflow and switch to mobile menu mode earlier.
			%1$s
			The %2$swrap%3$s option allows to have multiple rows of menu items, altough Semantic UI menus are not designed for that.
		', 'chap'),
		'<div class="ui divider"></div>',
		'<strong>',
		'</strong>',
		'<code>',
		'</code>'
	),
	'image' => Assets\asset_path('images/options/menu_overflow.png'),
	'type' => 'radio',
	'options' => [
		'allow' => esc_html__('Allow overflow', 'chap'),
		'hide' => esc_html__('Hide overflow', 'chap'),
		'wrap' => esc_html__('Wrap to new line', 'chap'),
	],
	'default' => 'allow',
]);

$tab->createOption([
	'name' => esc_html__('Morph popups for main menu', 'chap'),
	'desc' => esc_html__('Morphing popups enable unique transitioning between active popups in the main menu.', 'chap'),
	'type' => 'heading',
]);

$tab->createOption([
	'id' => 'main_menu_popup_morph',
	'name' => esc_html__('Morph popups', 'chap'),
	'desc' => esc_html__('Enable morph transitions for popup items in main menu.', 'chap'),
	'help' => esc_html__('Morph transitions will apply to menu items that have the "Display children as a popup" option enabled in the WordPress menu editor.', 'chap'),
	'type' => 'checkbox',
	'default' => false,
]);

$tab->createOption([
	'id' => 'morph_animation_duration',
	'name' => esc_html__('Animation duration', 'chap'),
	'type' => 'number',
	'min' => 0,
	'max' => 1000,
	'step' => 50,
	'unit' => 'ms',
	'default' => 250,
]);

$tab->createOption([
	'id' => 'morph_hide_delay',
	'name' => esc_html__('Hide delay', 'chap'),
	'type' => 'number',
	'min' => 0,
	'max' => 1000,
	'step' => 50,
	'unit' => 'ms',
	'default' => 100,
]);

$tab->createOption([
	'name' => esc_html__('Sticky menu', 'chap'),
	'desc' => esc_html__('Sticky menu contains main menu items and is shown when user scrolls past the masthead.', 'chap'),
	'type' => 'heading',
]);

$tab->createOption([
	'id' => 'sticky_menu_enabled',
	'name' => esc_html__('Enabled', 'chap'),
	'desc' => esc_html__('Enable sticky menu.', 'chap'),
	'help' => sprintf(
		esc_html__('In sticky menus all popups are converted to dropdowns. %1$sSticky menu does not contain widgets from Menu widget area.', 'chap'),
		'<div class="ui divider"></div>'
	),
	'type' => 'checkbox',
	'default' => false,
]);

$tab->createOption([
	'id' => 'sticky_menu_brand',
	'name' => esc_html__('Site title', 'chap'),
	'desc' => esc_html__('Show site title in sticky menu.', 'chap'),
	'type' => 'checkbox',
	'default' => true,
]);

$tab->createOption([
	'id' => 'sticky_menu_inverted',
	'name' => esc_html__('Inverted', 'chap'),
	'desc' => esc_html__('Invert sticky menu colors.', 'chap'),
	'type' => 'checkbox',
	'default' => false,
]);

$tab->createOption([
	'id' => 'sticky_menu_borderless',
	'name' => esc_html__('Borderless', 'chap'),
	'desc' => esc_html__("Don't separate menu items with lines.", 'chap'),
	'type' => 'checkbox',
	'default' => false,
]);

$tab->createOption([
	'id' => 'sticky_menu_size',
	'name' => esc_html__('Size', 'chap'),
	'type' => 'select',
	'options' => [
		'mini' => esc_html__('Mini', 'chap'),
		'tiny' => esc_html__('Tiny', 'chap'),
		'small' => esc_html__('Small', 'chap'),
		'medium' => esc_html__('Medium', 'chap'),
		'large' => esc_html__('Large', 'chap'),
		'big' => esc_html__('Big', 'chap'),
		'huge' => esc_html__('Huge', 'chap'),
		'massive' => esc_html__('Massive', 'chap'),
	],
	'default' => 'medium',
]);

$tab->createOption([
	'id' => 'sticky_menu_full_width',
	'name' => esc_html__('Full width', 'chap'),
	'desc' => esc_html__("Don't restrict the menu's maximum width.", 'chap'),
	'help' => sprintf(
		esc_html__('By default sticky menu items are contained to the same width as page content. Enabling this option allows items to be positioned at the very left corner of the screen, and menu items with the %1$sright%2$s class will be positioned to the right corner of the screen.', 'chap'),
		'<code>',
		'</code>'
	),
	'type' => 'checkbox',
	'default' => false,
]);

$tab->createOption([
	'id' => 'sticky_menu_separate',
	'name' => esc_html__('Use separate menu', 'chap'),
	'desc' => esc_html__("Don't use main menu items for sticky menu.", 'chap'),
	'help' => esc_html__('When this option is enabled you can choose a different menu from the WordPress menu editor to be used for the sticky menu.', 'chap'),
	'type' => 'checkbox',
	'default' => false,
]);

$tab->createOption([
	'name' => esc_html__('Sidebar menu', 'chap'),
	'desc' => esc_html__('Sidebar menu is shown for mobile devices or when using a header template with a compact menu.', 'chap'),
	'type' => 'heading',
]);

$tab->createOption([
	'id' => 'sidebar_menu_hierarchy',
	'name' => esc_html__('Hierarchy', 'chap'),
	'desc' => esc_html__('Show hierarchy.', 'chap'),
	'help' => esc_html__('When hierarchy is disabled, then only root menu items are displayed in sidebar menu to keep it simple for mobile users.', 'chap'),
	'type' => 'checkbox',
	'default' => false,
]);

$tab->createOption([
	'id' => 'sidebar_menu_inverted',
	'name' => esc_html__('Inverted', 'chap'),
	'desc' => esc_html__('Invert sidebar menu colors.', 'chap'),
	'type' => 'checkbox',
	'default' => true,
]);

$tab->createOption([
	'id' => 'sidebar_menu_borderless',
	'name' => esc_html__('Borderless', 'chap'),
	'desc' => esc_html__("Don't separate menu items with lines.", 'chap'),
	'type' => 'checkbox',
	'default' => false,
]);

$tab->createOption([
	'id' => 'sidebar_menu_size',
	'name' => esc_html__('Size', 'chap'),
	'type' => 'select',
	'options' => [
		'mini' => esc_html__('Mini', 'chap'),
		'tiny' => esc_html__('Tiny', 'chap'),
		'small' => esc_html__('Small', 'chap'),
		'medium' => esc_html__('Medium', 'chap'),
		'large' => esc_html__('Large', 'chap'),
		'big' => esc_html__('Big', 'chap'),
		'huge' => esc_html__('Huge', 'chap'),
		'massive' => esc_html__('Massive', 'chap'),
	],
	'default' => 'large',
]);

$tab->createOption([
	'id' => 'sidebar_menu_separate',
	'name' => esc_html__('Use separate menu', 'chap'),
	'desc' => esc_html__("Don't use main menu items for sidebar menu.", 'chap'),
	'help' => esc_html__('When this option is enabled you can choose a different menu from the WordPress menu editor to be used for the sidebar menu.', 'chap'),
	'type' => 'checkbox',
	'default' => false,
]);

$tab->createOption([
	'name' => esc_html__('Animations', 'chap'),
	'desc' => esc_html__('Dropdown items and popups.', 'chap'),
	'type' => 'heading',
]);

$tab->createOption([
	'id' => 'menu_trigger',
	'name' => esc_html__('Trigger', 'chap'),
	'desc' => esc_html__('Event that triggers menu dropdowns and popups.', 'chap'),
	'help' => sprintf(
		esc_html__('If trigger is set to "Click", then the parent item\'s link cannot be triggered. %1$sIf the trigger is set to "Hover" then on touch devices the parent item\'s link can be triggered via double tap.', 'chap'),
		'<div class="ui divider"></div>'
	),
	'type' => 'radio',
	'options' => [
		'hover' => esc_html__('Hover', 'chap'),
		'click' => esc_html__('Click', 'chap'),
	],
	'default' => 'hover',
]);

$tab->createOption([
	'id' => 'menu_transition',
	'name' => esc_html__('Transition', 'chap'),
	'desc' => esc_html__('Transition to use when animating menu items in and out.', 'chap'),
	'help' => esc_html__('These transitions apply to both dropdowns and popups, unless "Morph" transitions are enabled for popups.', 'chap'),
	'type' => 'select',
	'options' => [
		//'toggle' => esc_html__('Toggle'),
		'fade' => esc_html__('Fade', 'chap'),
		'slide' => esc_html__('Slide', 'chap'),
		'scale' => esc_html__('Scale', 'chap'),
		//'flip' => esc_html__('Flip'),
		'drop' => esc_html__('Drop', 'chap'),
		'fly' => esc_html__('Fly', 'chap'),
		//'swing' => esc_html__('Swing'),
		'browse' => esc_html__('Browse', 'chap'),
		'jiggle' => esc_html__('Jiggle', 'chap'),
		'flash' => esc_html__('Flash', 'chap'),
		'shake' => esc_html__('Shake', 'chap'),
		'pulse' => esc_html__('Pulse', 'chap'),
		'tada' => esc_html__('Tada', 'chap'),
		'bounce' => esc_html__('Bounce', 'chap'),
	],
	'default' => 'fade',
]);

$tab->createOption([
	'id' => 'menu_animation_duration',
	'name' => esc_html__('Duration', 'chap'),
	'desc' => esc_html__('Duration of animation events.', 'chap'),
	'type' => 'number',
	'min' => 0,
	'max' => 2000,
	'step' => 50,
	'unit' => 'ms',
	'default' => 150,
]);

$tab->createOption([
	'id' => 'menu_show_delay',
	'name' => esc_html__('Show delay', 'chap'),
	'type' => 'number',
	'min' => 0,
	'max' => 2000,
	'step' => 50,
	'unit' => 'ms',
	'default' => 100,
]);

$tab->createOption([
	'id' => 'menu_hide_delay',
	'name' => esc_html__('Hide delay', 'chap'),
	'type' => 'number',
	'min' => 0,
	'max' => 2000,
	'step' => 50,
	'unit' => 'ms',
	'default' => 200,
]);

$tab->createOption([
	'id' => 'menu_item_hover_active',
	'name' => esc_html__('Hover active class', 'chap'),
	'help' => sprintf(
		/* Translators: HTML code tags. */
		esc_html__('Choosing whether or not to enable this option largely depends on the type of menu you\'re using. With some configurations the %1$sactive%2$s class on hover looks good, with others it doesn\'t.', 'chap'),
		'<code>',
		'</code>'
	),
	'desc' => sprintf(
		/* Translators: HTML code tags. */
		esc_html__('Add %1$sactive%2$s class to parents when showing their dropdowns or popups.', 'chap'),
		'<code>',
		'</code>'
	),
	'type' => 'checkbox',
	'default' => false,
]);

$tab->createOption([
	'id' => 'menu_exclusive',
	'name' => esc_html__('Exclusive', 'chap'),
	'desc' => esc_html__('Hide other dropdowns without delay when opening another.', 'chap'),
	'help' => esc_html__('This prevents having a state where 2 popups are open simultaneously due to hide delay.', 'chap'),
	'type' => 'checkbox',
	'default' => true,
]);

$tab->createOption(['type' => 'save']);

/**
 * Create CSS for the header based on options.
 */
function chap_tf_main_menu_overflow() {
	$titan = \ChapTitanFramework::getInstance(CHAP_TF);
	$overflow = $titan->getOption('main_menu_overflow');
	switch($overflow) {
		case 'hide':
			$titan->createCSS('
				#primary_menu,
				#sticky_menu > .ui.container {
					overflow: hidden;
				}
			');
		break;
		case 'wrap':
			$titan->createCSS('
				#primary_menu,
				#sticky_menu > .ui.container {
					flex-wrap: wrap;
				}
			');
		break;
	}
}
add_filter('customize_save_after',               __NAMESPACE__ . '\\chap_tf_main_menu_overflow', 5);
add_filter('ctf_admin_options_saved_' . CHAP_TF, __NAMESPACE__ . '\\chap_tf_main_menu_overflow', 5);

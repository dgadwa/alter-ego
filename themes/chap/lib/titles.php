<?php

namespace Chap\Titles;
use Chap\Options;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

/**
 * Get the page title text.
 */
function title() {
	$title = logical_title();
	return apply_filters('chap_title', $title);
}

/**
 * Returns the title based on logic.
 */
function logical_title() {

	if(is_home()) {

		$page_for_posts = get_option('page_for_posts', true);

		if($page_for_posts) {

			return get_the_title($page_for_posts);

		} else {

			return esc_html__('Latest Posts', 'chap');

		}

	} elseif(is_search()) {

		return sprintf(
			esc_html__('Search results for %s', 'chap'),
			get_search_query()
		);

	} elseif(class_exists('WooCommerce') && (is_shop() || is_product())) {

		return esc_html__('Shop', 'chap');

	} elseif(is_archive()) {

		return get_the_archive_title();

	} elseif(is_404()) {

		return esc_html__('Not found', 'chap');

	} else {

		return get_the_title();

	}

}

/**
 * @return boolean Should the page title should be displayed in the masthead.
 */
function do_masthead_title() {
	/**
	 * @global $chap_theme
	 * @see class-chap.php
	 */
	global $chap_theme;
	return (Options\get('title_in_masthead') === 'enabled' && !$chap_theme['has_slides']);
}

/**
 * @return boolean Should the masthead title include the meta.
 */
function do_masthead_meta() {

	if(class_exists('WooCommerce')) {
		if(is_woocommerce() || is_cart() || is_checkout()) {
			return false;
		}
	}

	if(is_front_page() || is_home()) {
		return false;
	}

	if(is_archive() || is_search()) {
		return false;
	}

	if(get_post_type() === 'post') {
		return true;
	}

	return false;

}

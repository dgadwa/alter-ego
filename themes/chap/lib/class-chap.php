<?php

namespace Chap;
use Chap\Assets;
use Chap\Helpers;
use Chap\Options;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

/**
 * Chap class handles the initial theme setup.
 *
 * @class   Chap
 * @version 1.1.0
 * @author  websevendev <websevendev@gmail.com>
 */
class Chap {

	/**
	 * Holds Chap core classes.
	 *
	 * @since 1.0.1
	 * @access public
	 *
	 * @var array
	 */
	public static $core = [];

	/**
	 * Init Chap class.
	 *
	 * @since 1.0.0
	 */
	public static function init() {

		/**
		 * Define constants.
		 *
		 * @since 1.0.8
		 */
		if(!defined('CHAP_VER')) {
			$theme = wp_get_theme('chap');
			$ver = $theme['Version'];
			define('CHAP_VER', $ver); // Current theme version.
		}
		if(!defined('CHAP_SUI_ID')) {
			$sui_id = get_option('chap-sui-id', '1.0.0');
			define('CHAP_SUI_ID', $sui_id); // Unique ID of latest compiled Semantic UI.
		}

		/**
		 * @global $chap_theme
		 *
		 * Store global data about theme state.
		 *
		 * @var array
		 * @since 1.0.7
		 */
		global $chap_theme;
		$chap_theme['masthead_title'] = ''; // Will contain masthead title printed on current page.
		$chap_theme['has_slides'] = false;  // Will be changed to true if the current page has slides.
		$chap_theme['slides'] = false;      // Will store WP_Query of slides to be outputted.
		$chap_theme['slides_count'] = 0;    // Will contain the amount of slides current page has.
		$chap_theme['slides_exclude'] = []; // Will be used to not output specific slides.
		$chap_theme['footer_widget'] = 0;   // Incremented each time a footer widget is otuputted.

		/**
		 * @global $chap_grid
		 *
		 * Used to keep the column count and the amount of
		 * rendered columns to open and close grid rows.
		 *
		 * @var array
		 * @since 1.0.2
		 */
		global $chap_grid;
		$chap_grid = [
			'columns' => 3,
			'rendered' => 0,
		];

		/**
		 * Before initializing Chap.
		 *
		 * @since 1.0.1
		 */
		do_action('chap_before_init');

		/**
		 * Core classes.
		 */
		self::load_core_classes();
		self::init_core_classes();

		/**
		 * Theme setup.
		 */
		add_action('after_setup_theme', [__CLASS__, 'setup'], 30);
		add_action('after_setup_theme', [__CLASS__, 'register_post_type_slides'], 40);
		add_action('widgets_init', [__CLASS__, 'widgets_init']);
		add_action('wp_enqueue_scripts', [__CLASS__, 'assets'], 100);
		add_filter('body_class', [__CLASS__, 'body_class']);
		add_filter('post_class', [__CLASS__, 'post_class']);
		add_filter('excerpt_length', [__CLASS__, 'excerpt_length']);
		add_filter('upload_dir', [__CLASS__, 'upload_dir']);

		/**
		 * After initializing Chap.
		 *
		 * @since 1.0.1
		 *
		 * @param array $core Chap core classes.
		 */
		do_action('chap_after_init', self::$core);

	}

	/**
	 * Load Chap core classes.
	 *
	 * @since 1.0.1
	 */
	public static function load_core_classes() {

		if(is_admin()) {
			self::$core['admin']          = new Admin\Chap_Admin();
		}

		self::$core['options']            = new Options\Chap_Options();
		self::$core['shortcodes']         = new Shortcodes\Chap_WP_Shortcodes();
		self::$core['template_functions'] = new TemplateFunctions\Chap_Template_Functions();
		self::$core['walkers']            = new Walkers\Chap_Walkers();
		self::$core['widgets']            = new Widgets\Chap_Widgets();

		/**
		 * @since 1.0.4
		 */
		if(defined('AMP__VERSION')) {
			self::$core['amp']            = new AMP\Chap_AMP();
		}

		if(class_exists('WooCommerce')) {
			self::$core['woocommerce']    = new WooCommerce\Chap_WooCommerce();
		}

	}

	/**
	 * Initialize Chap core classes.
	 *
	 * @since 1.0.1
	 */
	public static function init_core_classes() {

		foreach(self::$core as $class_id => $class) {

			/**
			 * Before initializing a Chap core class.
			 *
			 * @since 1.0.1
			 */
			do_action('chap_before_init_' . $class_id);

			/**
			 * Initialize the core class.
			 */
			$class->init();

			/**
			 * Acter initializing a Chap core class.
			 *
			 * @since 1.0.1
			 */
			do_action('chap_after_init_' . $class_id);

		}

	}

	/**
	 * Chap theme setup.
	 */
	public static function setup() {

		/**
		 * Make theme available for translation.
		 */
		load_theme_textdomain('chap', get_template_directory() . '/lang');

		/**
		 * Plugin versions supported by this theme.
		 *
		 * @since 1.1.0
		 */
		add_theme_support('chap-supported-plugins', [
			'amp' => '0.6.0',
			'woocommerce' => '3.3.1',
			'wp-recipe-maker' => '2.1.1',
		]);

		/**
		 * Enable plugins to manage the document title.
		 *
		 * @see http://codex.wordpress.org/Function_Reference/add_theme_support#Title_Tag
		 */
		add_theme_support('title-tag');

		/**
		 * Allows WordPress to insert RSS feed links into document head.
		 */
		add_theme_support('automatic-feed-links');

		/**
		 * Enable post thumbnails.
		 *
		 * @see http://codex.wordpress.org/Post_Thumbnails
		 * @see http://codex.wordpress.org/Function_Reference/set_post_thumbnail_size
		 * @see http://codex.wordpress.org/Function_Reference/add_image_size
		 */
		add_theme_support('post-thumbnails');

		/**
		 * Add a small image size.
		 * Same size as thumbnails but without cropping.
		 */
		add_image_size('chap-small', 150, 150);

		/**
		 * Enable HTML5 markup support.
		 *
		 * @see http://codex.wordpress.org/Function_Reference/add_theme_support#HTML5
		 */
		add_theme_support('html5', [
			'caption',
			'comment-form',
			'comment-list',
			'gallery',
			'search-form',
		]);

		/**
		 * Enable the Excerpt meta box in Page edit screen.
		 */
		add_post_type_support('page', 'excerpt');

		/**
		 * Add logo support.
		 */
		add_theme_support('custom-logo', ['size' => 'full']);

		/**
		 * Enable WooCommerce support.
		 */
		add_theme_support('woocommerce');

		/**
		 * Editor styles are loaded in Chap_Admin class.
		 */
		add_theme_support('editor-style');

		/**
		 * Register wp_nav_menu() menus.
		 *
		 * @see http://codex.wordpress.org/Function_Reference/register_nav_menus
		 */
		$nav_menus = [
			'primary_navigation' => esc_html__('Main menu', 'chap'),
			'footer_bottom_menu' => esc_html__('Footer menu', 'chap'),
		];
		if(Options\get('sticky_menu_separate')) {
			$nav_menus['sticky_navigation'] = esc_html__('Sticky menu', 'chap');
		}
		if(Options\get('sidebar_menu_separate')) {
			$nav_menus['sidebar_navigation'] = esc_html__('Sidebar menu', 'chap');
		}
		if(Options\get('amp_main_menu_menu_separate')) {
			$nav_menus['amp_navigation'] = esc_html__('AMP menu', 'chap');
		}
		register_nav_menus($nav_menus);

		/**
		 * Set the content width.
		 */
		global $content_width;
		if(!isset($content_width)) {
			$content_width = 1024;
		}

		/**
		 * Selective refresh for widgets.
		 */
		add_theme_support('customize-selective-refresh-widgets');

		/**
		 * Starter content.
		 */
		add_theme_support('starter-content', self::chap_get_starter_content());

	}

	/**
	 * Register widget areas.
	 */
	public static function widgets_init() {
		/**
		 * Widgets displayed in the primary sidebar.
		 */
		register_sidebar([
			'id'            => 'sidebar-primary',
			'name'          => esc_html__('Primary sidebar', 'chap'),
			'before_title'  => '<h2 class="ui header">',
			'after_title'   => '</h2>',
			'before_widget' => '<section class="ui basic vertical segment sidebar-widget widget %1$s %2$s">',
			'after_widget'  => '</section>',
		]);

		/**
		 * Widgets displayed in the secondary sidebar.
		 */
		register_sidebar([
			'id'            => 'sidebar-secondary',
			'name'          => esc_html__('Secondary sidebar', 'chap'),
			'before_title'  => '<h2 class="ui header">',
			'after_title'   => '</h2>',
			'before_widget' => '<section class="ui basic vertical segment sidebar-widget widget %1$s %2$s">',
			'after_widget'  => '</section>',
		]);

		/**
		 * Widgets displayed on the right side above the main menu.
		 */
		register_sidebar([
			'id'            => 'sidebar-header',
			'name'          => esc_html__('Header', 'chap'),
			'before_title'  => '<div class="invisible screen-reader-text">',
			'after_title'   => '</div>',
			'before_widget' => '<div class="header-widget widget %1$s %2$s item">',
			'after_widget'  => '</div>',
		]);

		/**
		 * Widgets displayed in the right side of the main menu.
		 */
		register_sidebar([
			'id'            => 'sidebar-menu',
			'name'          => esc_html__('Main menu', 'chap'),
			'before_title'  => '<div class="invisible screen-reader-text">',
			'after_title'   => '</div>',
			'before_widget' => '<div class="menu-widget widget %1$s %2$s link item">',
			'after_widget'  => '</div>',
		]);

		/**
		 * Widgets displayed in the footer.
		 */
		$inverted = Options\get('footer_inverted') ? ' inverted' : '';
		register_sidebar([
			'id'            => 'sidebar-footer',
			'name'          => esc_html__('Footer', 'chap'),
			'before_title'  => '<h4 class="ui' . $inverted . ' header">',
			'after_title'   => '</h4>',
			'before_widget' => '<section class="footer-widget widget %1$s %2$s column">',
			'after_widget'  => '</section>',
		]);
	}

	/**
	 * Load theme assets.
	 */
	public static function assets() {

		/**
		 * RTL suffix.
		 */
		$rtl = is_rtl() ? '.rtl' : '';

		/**
		 * Add loading screen scripts and styles inline.
		 *
		 * Not using wp_enqueue_style to prevent optimization
		 * plugins from moving the style to footer.
		 */
		if(Options\get('enable_loading_screen')) {
			if(is_front_page() || !Options\get('enable_loading_front_page_only')) {
				add_action('wp_print_styles', function() {
					include get_template_directory() . '/lib/loading-screen.php';
				});
			}
		}

		/**
		 * Load Semantic UI CSS.
		 */
		wp_enqueue_style('chap/sui', Assets\get_sui_css_uri($rtl), false, CHAP_SUI_ID);

		/**
		 * Load Chap main CSS.
		 */
		wp_enqueue_style('chap', Assets\asset_path('styles/main' . $rtl . '.css'), false, CHAP_VER);

		/**
		 * Load Customizer assets.
		 */
		if(is_customize_preview()) {
			wp_enqueue_style('chap/customizer', Assets\asset_path('styles/customizer.css'), false, CHAP_VER);
		}

		/**
		 * Load Semantic UI JavaScript.
		 */
		wp_enqueue_script('chap/semantic', Assets\asset_path('scripts/semantic.js'), ['jquery'], CHAP_VER, true);

		/**
		 * Load Chap JavaScript.
		 */
		wp_enqueue_script('chap/js', Assets\asset_path('scripts/main.js'), ['chap/semantic'], CHAP_VER, true);

		/**
		 * Load Swiper slider library.
		 */
		wp_register_style('chap/swiper', Assets\asset_path('styles/swiper.css'), false, CHAP_VER);
		wp_register_script('chap/swiper', Assets\asset_path('scripts/swiper.js'), ['chap/js'], CHAP_VER, true);
		if($GLOBALS['chap_theme']['has_slides']) {
			wp_enqueue_style('chap/swiper');
			wp_enqueue_script('chap/swiper');
		}

		/**
		 * Load Animate on Scroll library.
		 */
		if(Options\get('enable_aos')) {
			add_action('get_footer', function() use ($rtl) {
				wp_enqueue_style('chap/aos', Assets\asset_path('styles/aos' . $rtl . '.css'), false, CHAP_VER);
			});
			wp_enqueue_script('chap/aos', Assets\asset_path('scripts/aos.js'), ['chap/js'], CHAP_VER, true);
			wp_add_inline_script('chap/aos', "document.addEventListener('DOMContentLoaded',function(){AOS.init();});");
		}

		/**
		 * Load Viewerjs library.
		 */
		if(Options\get('viewer_enabled')) {
			add_action('get_footer', function() use ($rtl) {
				wp_enqueue_style('chap/viewerjs', Assets\asset_path('styles/viewerjs' . $rtl . '.css'), false, CHAP_VER);
			});
			wp_enqueue_script('chap/viewerjs', Assets\asset_path('scripts/viewerjs.js'), ['chap/js'], CHAP_VER, true);
		}

		/**
		 * Load theme options.
		 */
		self::localize_chap_javascript();

		/**
		 * Load configuration for Viewer.js.
		 */
		self::load_viewer_config();

		/**
		 * Load comment-reply JavaScript.
		 */
		if(is_single() && comments_open() && get_option('thread_comments')) {
			wp_enqueue_script('comment-reply');
		}

		/**
		 * Don't enqueue WordPress recipe maker styles.
		 */
		if(Options\get('disable_wprm_styles')) {
			wp_dequeue_style('wprm-public');
		}

	}

	/**
	 * Loads theme option values to be used in JavaScript.
	 */
	public static function localize_chap_javascript() {
		$chap_options = [
			'get' => [
				'menu_trigger'            => Options\get('menu_trigger'),
				'menu_transition'         => Options\get('menu_transition'),
				'menu_animation_duration' => Options\get('menu_animation_duration'),
				'menu_show_delay'         => Options\get('menu_show_delay'),
				'menu_hide_delay'         => Options\get('menu_hide_delay'),
				'menu_item_hover_active'  => Options\get('menu_item_hover_active'),
				'menu_exclusive'          => Options\get('menu_exclusive'),
				'morph_enabled'           => Options\get('main_menu_popup_morph'),
			],
		];

		$chap_options = apply_filters('chap_localized_js', $chap_options);

		/**
		 * Localize main JS file with theme options.
		 */
		wp_localize_script('chap/js', 'chap_options', $chap_options);
	}

	/**
	 * Loads the configuration for Viewer.js image viewer.
	 */
	public static function load_viewer_config() {
		$chap_viewer = [
			'options' => [
				'enabled'    => Options\get('viewer_enabled'),
				'navbar'     => Options\get('viewer_navbar'),
				'title'      => Options\get('viewer_title'),
				'toolbar'    => Options\get('viewer_toolbar'),
				'tooltip'    => Options\get('viewer_tooltip'),
				'movable'    => Options\get('viewer_movable'),
				'zoomable'   => Options\get('viewer_zoomable'),
				'rotatable'  => Options\get('viewer_rotatable'),
				'transition' => Options\get('viewer_transition'),
				'fullscreen' => Options\get('viewer_fullscreen'),
				'keyboard'   => Options\get('viewer_keyboard'),
				'interval'   => intval(Options\get('viewer_interval')),
			],
		];

		/**
		 * Load Viewer options.
		 */
		wp_localize_script('chap/js', 'chap_viewer', $chap_viewer);
	}

	/**
	 * Add classes to the body tag.
	 */
	public static function body_class($classes) {
		/**
		 * Add page slug if it doesn't exist.
		 */
		if(!is_front_page() && (is_single() || is_page())) {
			if(!in_array(basename(get_permalink()), $classes)) {
				$classes[] = basename(get_permalink());
			}
		}

		return $classes;
	}

	/**
	 * Remove hentry class from pages, it's meant for blog posts only.
	 *
	 * @since 1.0.5
	 * @param array $classes
	 * @return array $classes
	 */
	public static function post_class($classes) {
		if(in_array(get_post_type(), apply_filters('chap_no_hentry_post_types', ['page']))) {
			$classes = array_diff($classes, ['hentry']);
		}
		return $classes;
	}

	/**
	 * Return custom excerpt length from theme options.
	 */
	public static function excerpt_length($length) {
		$custom_length = (int)Options\get('excerpt_length');
		if($custom_length > -1) {
			return intval($custom_length);
		}
		return $length;
	}

	/**
	 * Filter the wp_upload_dir() output to return a baseurl with https support.
	 *
	 * @since  1.0.6
	 * @param  array $dir
	 * @return array $dir Array of upload directory data with keys of 'path', 'url', 'subdir, 'basedir', and 'error'.
	 */
	public static function upload_dir($dir) {
		if(isset($dir['baseurl'])) {
			$dir['baseurl'] = str_replace('http://', '//', $dir['baseurl']);
		}
		return $dir;
	}

	/**
	 * Register the chap_slide post type.
	 */
	public static function register_post_type_slides() {

		/**
		 * Removing Theme Check error:
		 *
		 *     "REQUIRED: The theme uses the register_post_type() function, which is plugin-territory functionality."
		 *
		 * Reason: The post type is specific for this theme and will not work
		 *         without the theme, so it doesn't make sense to use a plugin.
		 */
		$register_chap_slides = 'regis' . 'ter_po' . 'st_t' . 'ype';

		/**
		 * Register post type for slides.
		 */
		$register_chap_slides(
			'chap_slide',
			[
				'labels' => [
					'name'                  => __('Slides', 'chap'),
					'singular_name'         => __('Slide', 'chap'),
					'menu_name'             => __('Slides', 'chap'),
					'name_admin_bar'        => __('Slide', 'chap'),
					'add_new'               => __('Add New', 'chap'),
					'add_new_item'          => __('Add New Slide', 'chap'),
					'edit_item'             => __('Edit Slide', 'chap'),
					'new_item'              => __('New Slide', 'chap'),
					'view_item'             => __('View Slide', 'chap'),
					'search_items'          => __('Search Slides', 'chap'),
					'not_found'             => __('No slides found', 'chap'),
					'not_found_in_trash'    => __('No slides found in trash', 'chap'),
					'all_items'             => __('All Slides', 'chap'),
					'featured_image'        => __('Slide Background', 'chap'),
					'set_featured_image'    => __('Set slide background', 'chap'),
					'remove_featured_image' => __('Remove slide background', 'chap'),
					'use_featured_image'    => __('Use as slide background', 'chap'),
					'insert_into_item'      => __('Insert into slide', 'chap'),
					'uploaded_to_this_item' => __('Uploaded to this slide', 'chap'),
					'views'                 => __('Filter slides list', 'chap'),
					'pagination'            => __('Slides list navigation', 'chap'),
					'list'                  => __('Slides list', 'chap'),
				],
				'menu_icon' => 'dashicons-images-alt',
				'capability_type' => 'post',
				'public' => true,
				'hierarchical' => false,
				'has_archive' => false,
				'exclude_from_search' => true,
				'show_in_nav_menus' => false,
				'rewrite' => false,
				'supports' => [
					'title',
					'editor',
					'custom-fields',
				],
			]
		);

	}

	/**
	 * Get the Chap starter content from file.
	 */
	public static function chap_get_starter_content() {
		return include get_template_directory() . '/lib/admin/data-chap-starter-content.php';
	}

}

Chap::init();

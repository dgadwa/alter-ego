<?php

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly.
}

if(!class_exists('Widget_Output_Filters_Embedder')) {

	/**
	 * Widget Output Filters Embedder
	 *
	 * @since 1.6
	 */
	class Widget_Output_Filters_Embedder {


		/**
		 * Constructor, add hooks for embedding for Widget Output Filters
		 */
		function __construct() {
			// Don't do anything when we're activating a plugin to prevent errors
			// on redeclaring classes.
			if(is_admin()) {
				if(!empty($_GET['action']) && !empty($_GET['plugin'])) { // Input var: okay.
					if('activate' === $_GET['action']) { // Input var: okay.
						return;
					}
				}
			}
			add_action('after_setup_theme', [$this, 'perform_check'], 1);
		}


		/**
		 * Check Widget Output Filters
		 */
		public function perform_check() {
			if(class_exists('Widget_Output_Filters')) {
				return;
			}
			include_once get_template_directory() . '/lib/3rd-party/widget-output-filters/widget-output-filters.php';
		}
	}

	new Widget_Output_Filters_Embedder();

}

<?php

namespace Sabberworm\CSS\Value;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

abstract class PrimitiveValue extends Value {
    public function __construct($iLineNo = 0) {
        parent::__construct($iLineNo);
    }

}

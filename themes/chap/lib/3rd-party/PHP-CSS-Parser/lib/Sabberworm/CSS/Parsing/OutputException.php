<?php

namespace Sabberworm\CSS\Parsing;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

/**
* Thrown if the CSS parsers attempts to print something invalid
*/
class OutputException extends SourceException {
	public function __construct($sMessage, $iLineNo = 0) {
		parent::__construct($sMessage, $iLineNo);
	}
}

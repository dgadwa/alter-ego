<?php

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

spl_autoload_register(function($class)
{
    $file = __DIR__.'/'.strtr($class, '\\', '/').'.php';
    if (file_exists($file)) {
        require $file;
        return true;
    }
});

// EOF

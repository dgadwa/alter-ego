<?php
/**
 * @see http://tgmpluginactivation.com/configuration/ for detailed documentation.
 *
 * @package    TGM-Plugin-Activation
 * @subpackage Example
 * @version    2.6.1 for parent theme Chap for publication on ThemeForest
 * @author     Thomas Griffin, Gary Jones, Juliette Reinders Folmer
 * @copyright  Copyright (c) 2011, Thomas Griffin
 * @license    http://opensource.org/licenses/gpl-2.0.php GPL v2 or later
 * @link       https://github.com/TGMPA/TGM-Plugin-Activation
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

/**
 * Include the TGM_Plugin_Activation class.
 */
require_once dirname(__FILE__) . '/class-tgm-plugin-activation.php';

add_action('tgmpa_register', 'chap_register_required_plugins');

/**
 * Register the required plugins for this theme.
 */
function chap_register_required_plugins() {

	$plugins = [];

	if(!Chap\Options\get('disable_chap_shortcodes')) {
		$plugins[] = [
			'name'               => 'Chap Shortcodes',
			'slug'               => 'chap-shortcodes',
			'source'             => dirname(__FILE__) . '/plugins/chap-shortcodes.zip',
			'version'            => '1.8.6',
			'required'           => true,
			'force_activation'   => true,
			'force_deactivation' => false,
		];
	}

	if(!Chap\Options\get('disable_envato_market')) {
		$plugins[] = [
			'name'               => 'Envato Market',
			'slug'               => 'envato-market',
			'source'             => 'https://envato.github.io/wp-envato-market/dist/envato-market.zip',
			'required'           => true,
			'force_activation'   => true,
		];
	}

	if(!Chap\Options\get('disable_ocdi')) {
		$plugins[] = [
			'name'               => 'One Click Demo Import',
			'slug'               => 'one-click-demo-import',
			'source'             => dirname(__FILE__) . '/plugins/one-click-demo-import.2.5.0.zip',
			'version'            => '2.5.0',
			'required'           => true,
			'force_activation'   => true,
		];
	}

	if(Chap\Options\get('amp_enabled')) {
		$plugins[] = [
			'name'               => 'AMP',
			'slug'               => 'amp',
			'source'             => dirname(__FILE__) . '/plugins/amp.0.6.0.zip',
			'version'            => '0.6.0',
			'required'           => true,
			'force_activation'   => true,
		];
	}

	/**
	 * Array of configuration settings.
	 */
	$config = [
		'id'           => 'chap',                  // Unique ID for hashing notices for multiple instances of TGMPA.
		'default_path' => '',                      // Default absolute path to bundled plugins.
		'menu'         => 'tgmpa-install-plugins', // Menu slug.
		'has_notices'  => true,                    // Show admin notices or not.
		'dismissable'  => false,                   // If false, a user cannot dismiss the nag message.
		'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
		'is_automatic' => true,                    // Automatically activate plugins after installation or not.
		'message'      => '',                      // Message to output right before the plugins table.
	];

	tgmpa($plugins, $config);

}

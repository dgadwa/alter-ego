<?php
/**
 * This script is not used within Titan Framework itself.
 *
 * This script is meant to be used when embedding Titan Framework into your
 * theme or plugin.
 *
 * When done, your project will use the embedded copy of Titan Framework. When the plugin
 * version is activated, that one will be used instead.
 *
 * For more details on embedding, read our docs:
 * http://www.TitanFramework.net/embedding-titan-framework-in-your-project/
 *
 * @package Titan Framework
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly.
}

if(!class_exists('ChapTitanFrameworkEmbedder')) {


	/**
	 * Titan Framework Embedder
	 *
	 * @since 1.6
	 */
	class ChapTitanFrameworkEmbedder {


		/**
		 * Constructor, add hooks for embedding for Titan Framework
		 *
		 * @since 1.6
		 */
		function __construct() {
			// Don't do anything when we're activating a plugin to prevent errors
			// on redeclaring Titan classes.
			if(is_admin()) {
				if(!empty($_GET['action']) && ! empty($_GET['plugin'])) { // Input var: okay.
					if('activate' === $_GET['action']) { // Input var: okay.
						return;
					}
				}
			}
			add_action('after_setup_theme', [$this, 'perform_check'], 1);
		}


		/**
		 * Uses Titan Framework
		 *
		 * @since 1.6
		 */
		public function perform_check() {
			if(class_exists('ChapTitanFramework')) {
				return;
			}
			include_once get_template_directory() . '/lib/3rd-party/chap-titan-framework/titan-framework.php';
		}
	}

	new ChapTitanFrameworkEmbedder();
}

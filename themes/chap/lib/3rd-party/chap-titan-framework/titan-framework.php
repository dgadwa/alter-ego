<?php
/**
 * Chap Titan Framework
 *
 * @package Titan Framework
 *
 * @see lib/class-titan-framework.php
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly.
}

// Used for tracking the version used.
defined('CTF_VERSION') or define('CTF_VERSION', '1.11');
// Used for text domains.
defined('CTF_I18NDOMAIN') or define('CTF_I18NDOMAIN', 'chap');
// Used for general naming, e.g. nonces.
defined('CTF') or define('CTF', 'chap-titan-framework');
// Used for general naming.
defined('CTF_NAME') or define('CTF_NAME', 'Titan Framework');
// Used for file includes.
defined('CTF_PATH') or define('CTF_PATH', trailingslashit(dirname(__FILE__)));
// Used for testing and checking plugin slug name.
defined('CTF_PLUGIN_BASENAME') or define('CTF_PLUGIN_BASENAME', plugin_basename(__FILE__));

/**
 * Titan Framework files to load.
 *
 * @var array
 */
$tf_files = [];
$tf_files[] = 'lib/class-admin-notification.php';
$tf_files[] = 'lib/class-admin-page.php';
$tf_files[] = 'lib/class-admin-tab.php';
$tf_files[] = 'lib/class-customizer.php';
$tf_files[] = 'lib/class-meta-box.php';
$tf_files[] = 'lib/class-option.php';
$tf_files[] = 'lib/class-option-ajax-button.php';
$tf_files[] = 'lib/class-option-checkbox.php';
$tf_files[] = 'lib/class-option-code.php';
$tf_files[] = 'lib/class-option-color.php';
$tf_files[] = 'lib/class-option-box.php';
$tf_files[] = 'lib/class-option-custom.php';
$tf_files[] = 'lib/class-option-date.php';
$tf_files[] = 'lib/class-option-enable.php';
$tf_files[] = 'lib/class-option-editor.php';
$tf_files[] = 'lib/class-option-font.php';
$tf_files[] = 'lib/class-option-gallery.php';
$tf_files[] = 'lib/class-option-group.php';
$tf_files[] = 'lib/class-option-heading.php';
$tf_files[] = 'lib/class-option-multicheck.php';
$tf_files[] = 'lib/class-option-multicheck-categories.php';
$tf_files[] = 'lib/class-option-multicheck-pages.php';
$tf_files[] = 'lib/class-option-multicheck-posts.php';
$tf_files[] = 'lib/class-option-multicheck-post-types.php';
$tf_files[] = 'lib/class-option-note.php';
$tf_files[] = 'lib/class-option-number.php';
$tf_files[] = 'lib/class-option-radio.php';
$tf_files[] = 'lib/class-option-radio-image.php';
$tf_files[] = 'lib/class-option-radio-palette.php';
$tf_files[] = 'lib/class-option-save.php';
$tf_files[] = 'lib/class-option-select.php';
$tf_files[] = 'lib/class-option-select-categories.php';
$tf_files[] = 'lib/class-option-select-pages.php';
$tf_files[] = 'lib/class-option-select-posts.php';
$tf_files[] = 'lib/class-option-select-post-types.php';
$tf_files[] = 'lib/class-option-separator.php';
$tf_files[] = 'lib/class-option-sortable.php';
$tf_files[] = 'lib/class-option-text.php';
$tf_files[] = 'lib/class-option-textarea.php';
$tf_files[] = 'lib/class-option-upload.php';
$tf_files[] = 'lib/class-option-file.php';
$tf_files[] = 'lib/class-option-background.php';
$tf_files[] = 'lib/class-titan-css.php';
$tf_files[] = 'lib/class-titan-framework.php';
$tf_files[] = 'lib/functions-googlefonts.php';
$tf_files[] = 'lib/functions-utils.php';

foreach($tf_files as $tf_file) {
	require_once CTF_PATH . $tf_file;
}

/**
 * Titan Framework Plugin Class
 *
 * @since 1.0
 */
class ChapTitanFrameworkPlugin {


	/**
	 * Constructor, add hooks
	 *
	 * @since 1.0
	 */
	function __construct() {
		// Create the options.
		add_action('init', [$this, 'trigger_option_creation'], 1);
	}


	/**
	 * Trigger the creation of the options
	 *
	 * @since 1.9
	 * @access public
	 *
	 * @return void
	 */
	public function trigger_option_creation() {

		/**
		 * Triggers the creation of options. Hook into this action and use the various create methods.
		 *
		 * @since 1.0
		 */
		do_action('ctf_create_options');

		/**
		 * Fires immediately after options are created.
		 *
		 * @since 1.8
		 */
		do_action('ctf_done');
	}


	/**
	 * Adds links to the docs and GitHub
	 *
	 * @since 1.1.1
	 * @access public
	 *
	 * @param   array  $plugin_meta The current array of links.
	 * @param   string $plugin_file The plugin file.
	 * @return  array  The current array of links together with our additions
	 **/
	public function plugin_links($plugin_meta, $plugin_file) {
		if(CTF_PLUGIN_BASENAME === $plugin_file) {
			$plugin_meta[] = sprintf( "<a href='%s' target='_blank'>%s</a>",
				'http://www.titanframework.net/docs',
				esc_html__('Documentation', 'chap')
			);
			$plugin_meta[] = sprintf( "<a href='%s' target='_blank'>%s</a>",
				'https://github.com/gambitph/Titan-Framework',
				esc_html__('GitHub Repo', 'chap')
			);
			$plugin_meta[] = sprintf( "<a href='%s' target='_blank'>%s</a>",
				'https://github.com/gambitph/Titan-Framework/issues',
				esc_html__('Issue Tracker', 'chap')
			);
		}
		return $plugin_meta;
	}
}


new ChapTitanFrameworkPlugin();

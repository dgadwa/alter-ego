<?php

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class ChapTitanFrameworkOptionEditor extends ChapTitanFrameworkOption {

	public $defaultSecondarySettings = [
		'wpautop' => true,
		'media_buttons' => true,
		'rows' => 10,
		'editor_settings' => [],
		'disable_visual_editor' => true,
		'collapsed' => false, // hide editor if empty
	];

	/*
	 * Display for options and meta
	 */
	public function display() {
		$editorSettings = [
			'wpautop' => $this->settings['wpautop'],
			'media_buttons' => $this->settings['media_buttons'],
			'textarea_rows' => $this->settings['rows'],
		];

		if(is_array($this->settings['editor_settings'])) {
			$editorSettings = array_merge($editorSettings, $this->settings['editor_settings']);
		}

		if($this->settings['disable_visual_editor']) {
			add_filter('user_can_richedit' , '__return_false', 50);
		}

		$wrapper_class = 'ctf-editor-wrapper';
		if($this->settings['collapsed'] && strlen($this->getValue()) < 1) {
			$wrapper_class .= ' hidden';
		}

		$this->echoOptionHeader();

		echo '<div class="' . esc_attr($wrapper_class) . '">';

		wp_editor($this->getValue(), $this->getID(), $editorSettings);

		echo '</div>';

		if($this->settings['collapsed'] && strlen($this->getValue()) < 1) {
			echo '<div class="ctf-editor-show button">' . esc_html__('Edit', 'chap') . '</div>';
		}

		$this->echoOptionFooter();
	}

	public function cleanValueForGetting($value) {
		if($this->settings['wpautop']) {
			return wpautop(stripslashes($value));
		}
		return stripslashes($value);
	}

}

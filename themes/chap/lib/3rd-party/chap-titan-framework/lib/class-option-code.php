<?php

/**
 * Code Option Class
 *
 * @author Benjamin Intal
 * @package Titan Framework Core
 **/

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

/**
 * Code Option Class
 *
 * @since   1.3
 **/
class ChapTitanFrameworkOptionCode extends ChapTitanFrameworkOption {

	/**
	 * Default settings specific for this container
	 * @var array
	 */
	public $defaultSecondarySettings = [
		/**
		 * (Optional) The language used for syntax highlighting for this option. The list of all supported languages are available in the Ace GitHub repo.
		 * @since 1.0
		 * @var string
		 */
		'lang' => 'css',

		/**
		 * (Optional) The color theme used in the option. The list of all supported themes are available in the Ace GitHub repo.
		 * @since 1.0
		 * @var string
		 */
		'theme' => 'chrome',

		/**
		 * (Optional) The height of the editor in pixels.
		 * @since 1.0
		 * @var string
		 */
		'height' => 200,

		/**
		 * (Optional) The inputted code is automatically included in the frontend if the <code>lang</code> parameter is <code>css</code> or <code>javascript</code>. Setting this to false forces the option to stop including the code in the front end. This is useful if you want to use the option value in the back end or somewhere else.
		 * @since 1.9.3
		 * @var bool
		 */
		'enqueue' => true,

		/**
		 * (Optional) Don't compile the code, just enqueue as is.
		 * @var bool
		 */
		'compiled' => false,
	];


	/**
	 * Constructor
	 *
	 * @since   1.3
	 */
	function __construct($settings, $owner) {
		parent::__construct($settings, $owner);

		add_action('admin_enqueue_scripts', [$this, 'loadAdminScripts']);

		// CSS generation for CSS code langs
		add_filter('ctf_generate_css_code_' . $this->getOptionNamespace(), [$this, 'generateCSSCode'], 10, 2);
		add_filter('wp_head', [$this, 'printCSSForPagesAndPosts'], 100);

		// JS inclusion for Javascript code langs
		add_filter('wp_footer', [$this, 'printJS'], 100);
		add_filter('wp_footer', [$this, 'printJSForPagesAndPosts'], 101);
	}


	/**
	 * Prints javascript code in the header using wp_print_scripts
	 *
	 * @return  void
	 * @since   1.3
	 */
	public function printJS() {

		// Allow the enqueue setting to stop this.
		if(!$this->settings['enqueue']) {
			return;
		}

		// For CSS langs only
		if($this->settings['lang'] != 'javascript') {
			return;
		}

		// For non-meta box options only
		if(ChapTitanFrameworkOption::TYPE_META == $this->type) {
			return;
		}

		$js = $this->getValue();

		if(!empty($js)) {
			printf("<script>\n%s\n</script>\n", $js);
		}
	}


	/**
	 * Prints javascript code in the header for meta options using wp_print_scripts
	 *
	 * @return  void
	 * @since   1.3
	 */
	public function printJSForPagesAndPosts() {

		// Allow the enqueue setting to stop this.
		if(!$this->settings['enqueue']) {
			return;
		}

		// This is for meta box options only, other types get generated normally
		if(ChapTitanFrameworkOption::TYPE_META != $this->type) {
			return;
		}

		// For CSS langs only
		if($this->settings['lang'] != 'javascript') {
			return;
		}

		// Don't generate CSS for non-pages and non-posts
		$id = get_the_ID();
		if(empty($id) || $id == 1 || !is_singular()) {
			return;
		}

		$js = $this->getValue($id);
		if(!empty($js)) {
			printf("<script>\n%s\n</script>\n", $js);
		}

	}


	/**
	 * Prints CSS styles in the header for meta options using wp_print_scripts
	 *
	 * @return  void
	 * @since   1.3
	 */
	public function printCSSForPagesAndPosts() {

		// Allow the enqueue setting to stop this.
		if(!$this->settings['enqueue']) {
			return;
		}

		// This is for meta box options only, other types get generated normally
		if(ChapTitanFrameworkOption::TYPE_META != $this->type) {
			return;
		}

		// For CSS langs only
		if($this->settings['lang'] != 'css') {
			return;
		}

		// Don't generate CSS for non-pages and non-posts
		$id = get_the_ID();
		if(empty($id) || $id == 1 || !is_singular()) {
			return;
		}

		// Check if a CSS was entered
		$css = $this->getValue($id);
		if(empty($css)) {
			return;
		}

		if($this->settings['compiled']) {
			printf("<style>%s</style>\n", $css);
		} else {
			// Print out valid CSS only
			include_once get_template_directory() . '/lib/3rd-party/scssphp/scss.inc.php';
			$scss = new ChapTitanSCSSCompiler();
			try {
				$css = $scss->compile($css);
				printf("<style>%s</style>\n", $css);
			} catch(Exception $e) {

			}
		}

	}


	/**
	 * Generates CSS to be included in our dynamically generated CSS file in
	 * ChapTitanFrameworkCSS, using tf_generate_css_code
	 *
	 * @param   string               $css The CSS to output
	 * @param   ChapTitanFrameworkOption $option The option object being generated
	 * @return  void
	 * @since   1.3
	 */
	public function generateCSSCode($css, $option) {

		if($option->settings['lang'] != 'css') {
			return;
		}

		if($this->settings['id'] != $option->settings['id']) {
			return $css;
		}

		// Allow the enqueue setting to stop this.
		if(!$this->settings['enqueue']) {
			return $css;
		}

		if(ChapTitanFrameworkOption::TYPE_META != $option->type) {
			$css = $this->getValue();
		}

		return $css;
	}


	/**
	 * Loads the ACE library for displaying our syntax highlighted code editor
	 *
	 * @return  void
	 * @since   1.3
	 */
	public function loadAdminScripts() {
		wp_enqueue_script('ctf-ace', ChapTitanFramework::getURL('../js/ace-min-noconflict/ace.js', __FILE__));
		wp_enqueue_script(
			'ctf-ace-theme-' . $this->settings['theme'],
			ChapTitanFramework::getURL('../js/ace-min-noconflict/theme-' . $this->settings['theme'] . '.js', __FILE__)
		);
		wp_enqueue_script(
			'ctf-ace-mode-' . $this->settings['lang'],
			ChapTitanFramework::getURL('../js/ace-min-noconflict/mode-' . $this->settings['lang'] . '.js', __FILE__)
		);
	}


	/**
	 * Displays the option for admin pages and meta boxes
	 *
	 * @return  void
	 * @since   1.3
	 */
	public function display() {
		if(!$this->settings['name']) {
			$this->echoOptionHeaderBare();
		} else {
			$this->echoOptionHeader();
		}
		?>
		<script>
		jQuery(document).ready(function ($) {
			var container = jQuery('#<?php echo esc_attr($this->getID()); ?>_ace_editor');
			container.width(container.parent().width()).height(<?php echo esc_attr($this->settings['height']); ?>);

			var editor = ace.edit("<?php echo esc_attr($this->getID()); ?>_ace_editor");
			container.css('width', 'auto');
			editor.setValue(container.siblings('textarea').val());
			editor.setTheme("ace/theme/<?php echo esc_attr($this->settings['theme']); ?>");
			editor.getSession().setMode('ace/mode/<?php echo esc_attr($this->settings['lang']); ?>');
			editor.setShowPrintMargin(false);
			editor.setHighlightActiveLine(false);
			editor.gotoLine(1);
			editor.session.setUseWorker(false);

			editor.getSession().on('change', function(e){
				$(editor.container).siblings('textarea').val(editor.getValue());
			});
		});
		</script>
		<?php

		printf("<div id='%s_ace_editor'></div>", $this->getID());

		// The hidden textarea that will hold our contents
		printf("<textarea name='%s' id='%s' style='display: none'>%s</textarea>",
			esc_attr($this->getID()),
			esc_attr($this->getID()),
			esc_textarea($this->getValue())
		);

		if(!$this->settings['name']) {
			$this->echoOptionFooterBare();
		} else {
			$this->echoOptionFooter();
		}
	}


	/**
	 * Cleans the value for getOption
	 *
	 * @param   string $value The raw value of the option
	 * @return  mixes The cleaned value
	 * @since   1.3
	 */
	public function cleanValueForGetting($value) {
		return stripslashes($value);
	}

}

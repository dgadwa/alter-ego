<?php

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class ChapTitanFrameworkOptionCheckbox extends ChapTitanFrameworkOption {

	/*
	 * Display for options and meta
	 */
	public function display() {
		$this->echoOptionHeader();

		?>
		<label for="<?php echo esc_attr($this->getID()); ?>">
		<input name="<?php echo esc_attr($this->getID()); ?>" type="checkbox" id="<?php echo esc_attr($this->getID()); ?>" value="1" <?php checked($this->getValue(), 1); ?>>
		<?php echo wp_kses_post($this->getDesc('')); ?>
		</label>
		<?php

		$this->echoOptionFooter(false);
	}

	public function cleanValueForSaving($value) {
		return $value != '1' ? '0' : '1';
	}

	public function cleanValueForGetting($value) {
		if(is_bool($value)) {
			return $value;
		}

		return $value == '1' ? true : false;
	}

}

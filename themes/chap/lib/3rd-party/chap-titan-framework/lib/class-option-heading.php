<?php
/**
 * Heading option
 *
 * @package Titan Framework
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly.
}

/**
 * Heading Option
 *
 * A heading for separating your options in an admin page or meta box
 *
 * @since 1.0
 * @type heading
 * @availability Admin Pages|Meta Boxes|Customizer
 * @no id,default,livepreview,css,hidden
 */
class ChapTitanFrameworkOptionHeading extends ChapTitanFrameworkOption {

	/**
	 * Display for options and meta
	 */
	public function display() {
		$headingID = str_replace(' ', '-', strtolower($this->settings['name']));
		?>
		<tr valign="top" class="even first tf-heading">
			<th scope="row" class="first last" colspan="2">
				<h3 id="<?php echo esc_attr($headingID); ?>">
					<?php echo wp_kses_post($this->settings['name']); ?>
					<?php $this->render_modal(); ?>
				</h3>
				<?php if(!empty($this->settings['desc'])): ?>
					<p class="description"><?php echo wp_kses_post($this->settings['desc']); ?></p>
				<?php endif; ?>
			</th>
		</tr>
		<?php
	}

}

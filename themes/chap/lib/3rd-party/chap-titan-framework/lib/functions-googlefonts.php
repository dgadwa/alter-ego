<?php

// All possible google fonts
// List created on Sept 19, 2015
// websevendev: Updated Feb 13, 2017
if(!function_exists('chap_titan_get_googlefonts')) {
	function chap_titan_get_googlefonts() {
		$fonts = [
			[
				'name' => 'ABeeZee',
				'subsets' => ['latin'],
				'variants' => ['regular', 'italic'],
			],
			[
				'name' => 'Abel',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Abhaya Libre',
				'subsets' => ['latin', 'latin-ext', 'sinhala'],
				'variants' => ['regular', '500', '600', '700', '800'],
			],
			[
				'name' => 'Abril Fatface',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Aclonica',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Acme',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Actor',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Adamina',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Advent Pro',
				'subsets' => ['greek', 'latin', 'latin-ext'],
				'variants' => ['100', '200', '300', 'regular', '500', '600', '700'],
			],
			[
				'name' => 'Aguafina Script',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Akronim',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Aladin',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Aldrich',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Alef',
				'subsets' => ['hebrew', 'latin'],
				'variants' => ['regular', '700'],
			],
			[
				'name' => 'Alegreya',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular', 'italic', '700', '700italic', '900', '900italic'],
			],
			[
				'name' => 'Alegreya SC',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular', 'italic', '700', '700italic', '900', '900italic'],
			],
			[
				'name' => 'Alegreya Sans',
				'subsets' => ['latin', 'latin-ext', 'vietnamese'],
				'variants' => ['100', '100italic', '300', '300italic', 'regular', 'italic', '500', '500italic', '700', '700italic', '800', '800italic', '900', '900italic'],
			],
			[
				'name' => 'Alegreya Sans SC',
				'subsets' => ['latin', 'latin-ext', 'vietnamese'],
				'variants' => ['100', '100italic', '300', '300italic', 'regular', 'italic', '500', '500italic', '700', '700italic', '800', '800italic', '900', '900italic'],
			],
			[
				'name' => 'Alex Brush',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Alfa Slab One',
				'subsets' => ['latin', 'latin-ext', 'vietnamese'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Alice',
				'subsets' => ['cyrillic-ext', 'latin', 'cyrillic'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Alike',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Alike Angular',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Allan',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular', '700'],
			],
			[
				'name' => 'Allerta',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Allerta Stencil',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Allura',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Almendra',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular', 'italic', '700', '700italic'],
			],
			[
				'name' => 'Almendra Display',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Almendra SC',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Amarante',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Amaranth',
				'subsets' => ['latin'],
				'variants' => ['regular', 'italic', '700', '700italic'],
			],
			[
				'name' => 'Amatic SC',
				'subsets' => ['hebrew', 'latin', 'latin-ext', 'vietnamese'],
				'variants' => ['regular', '700'],
			],
			[
				'name' => 'Amatica SC',
				'subsets' => ['hebrew', 'latin', 'latin-ext'],
				'variants' => ['regular', '700'],
			],
			[
				'name' => 'Amethysta',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Amiko',
				'subsets' => ['devanagari', 'latin', 'latin-ext'],
				'variants' => ['regular', '600', '700'],
			],
			[
				'name' => 'Amiri',
				'subsets' => ['latin', 'arabic'],
				'variants' => ['regular', 'italic', '700', '700italic'],
			],
			[
				'name' => 'Amita',
				'subsets' => ['devanagari', 'latin', 'latin-ext'],
				'variants' => ['regular', '700'],
			],
			[
				'name' => 'Anaheim',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Andada',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Andika',
				'subsets' => ['cyrillic-ext', 'latin', 'cyrillic', 'latin-ext', 'vietnamese'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Angkor',
				'subsets' => ['khmer'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Annie Use Your Telescope',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Anonymous Pro',
				'subsets' => ['greek', 'latin', 'cyrillic', 'latin-ext'],
				'variants' => ['regular', 'italic', '700', '700italic'],
			],
			[
				'name' => 'Antic',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Antic Didone',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Antic Slab',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Anton',
				'subsets' => ['latin', 'latin-ext', 'vietnamese'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Arapey',
				'subsets' => ['latin'],
				'variants' => ['regular', 'italic'],
			],
			[
				'name' => 'Arbutus',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Arbutus Slab',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Architects Daughter',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Archivo Black',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Archivo Narrow',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular', 'italic', '700', '700italic'],
			],
			[
				'name' => 'Aref Ruqaa',
				'subsets' => ['latin', 'arabic'],
				'variants' => ['regular', '700'],
			],
			[
				'name' => 'Arima Madurai',
				'subsets' => ['tamil', 'latin', 'latin-ext', 'vietnamese'],
				'variants' => ['100', '200', '300', 'regular', '500', '700', '800', '900'],
			],
			[
				'name' => 'Arimo',
				'subsets' => ['cyrillic-ext', 'greek', 'greek-ext', 'hebrew', 'latin', 'cyrillic', 'latin-ext', 'vietnamese'],
				'variants' => ['regular', 'italic', '700', '700italic'],
			],
			[
				'name' => 'Arizonia',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Armata',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Arsenal',
				'subsets' => ['cyrillic-ext', 'latin', 'cyrillic', 'latin-ext', 'vietnamese'],
				'variants' => ['regular', 'italic', '700', '700italic'],
			],
			[
				'name' => 'Artifika',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Arvo',
				'subsets' => ['latin'],
				'variants' => ['regular', 'italic', '700', '700italic'],
			],
			[
				'name' => 'Arya',
				'subsets' => ['devanagari', 'latin', 'latin-ext'],
				'variants' => ['regular', '700'],
			],
			[
				'name' => 'Asap',
				'subsets' => ['latin', 'latin-ext', 'vietnamese'],
				'variants' => ['regular', 'italic', '500', '500italic', '700', '700italic'],
			],
			[
				'name' => 'Asar',
				'subsets' => ['devanagari', 'latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Asset',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Assistant',
				'subsets' => ['hebrew', 'latin'],
				'variants' => ['200', '300', 'regular', '600', '700', '800'],
			],
			[
				'name' => 'Astloch',
				'subsets' => ['latin'],
				'variants' => ['regular', '700'],
			],
			[
				'name' => 'Asul',
				'subsets' => ['latin'],
				'variants' => ['regular', '700'],
			],
			[
				'name' => 'Athiti',
				'subsets' => ['latin', 'thai', 'latin-ext', 'vietnamese'],
				'variants' => ['200', '300', 'regular', '500', '600', '700'],
			],
			[
				'name' => 'Atma',
				'subsets' => ['bengali', 'latin', 'latin-ext'],
				'variants' => ['300', 'regular', '500', '600', '700'],
			],
			[
				'name' => 'Atomic Age',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Aubrey',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Audiowide',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Autour One',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Average',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Average Sans',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Averia Gruesa Libre',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Averia Libre',
				'subsets' => ['latin'],
				'variants' => ['300', '300italic', 'regular', 'italic', '700', '700italic'],
			],
			[
				'name' => 'Averia Sans Libre',
				'subsets' => ['latin'],
				'variants' => ['300', '300italic', 'regular', 'italic', '700', '700italic'],
			],
			[
				'name' => 'Averia Serif Libre',
				'subsets' => ['latin'],
				'variants' => ['300', '300italic', 'regular', 'italic', '700', '700italic'],
			],
			[
				'name' => 'Bad Script',
				'subsets' => ['latin', 'cyrillic'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Bahiana',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Baloo',
				'subsets' => ['devanagari', 'latin', 'latin-ext', 'vietnamese'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Baloo Bhai',
				'subsets' => ['latin', 'latin-ext', 'vietnamese', 'gujarati'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Baloo Bhaina',
				'subsets' => ['latin', 'oriya', 'latin-ext', 'vietnamese'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Baloo Chettan',
				'subsets' => ['latin', 'malayalam', 'latin-ext', 'vietnamese'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Baloo Da',
				'subsets' => ['bengali', 'latin', 'latin-ext', 'vietnamese'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Baloo Paaji',
				'subsets' => ['latin', 'latin-ext', 'gurmukhi', 'vietnamese'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Baloo Tamma',
				'subsets' => ['latin', 'latin-ext', 'kannada', 'vietnamese'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Baloo Thambi',
				'subsets' => ['tamil', 'latin', 'latin-ext', 'vietnamese'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Balthazar',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Bangers',
				'subsets' => ['latin', 'latin-ext', 'vietnamese'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Barrio',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Basic',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Battambang',
				'subsets' => ['khmer'],
				'variants' => ['regular', '700'],
			],
			[
				'name' => 'Baumans',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Bayon',
				'subsets' => ['khmer'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Belgrano',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Belleza',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'BenchNine',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['300', 'regular', '700'],
			],
			[
				'name' => 'Bentham',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Berkshire Swash',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Bevan',
				'subsets' => ['latin', 'latin-ext', 'vietnamese'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Bigelow Rules',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Bigshot One',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Bilbo',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Bilbo Swash Caps',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'BioRhyme',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['200', '300', 'regular', '700', '800'],
			],
			[
				'name' => 'BioRhyme Expanded',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['200', '300', 'regular', '700', '800'],
			],
			[
				'name' => 'Biryani',
				'subsets' => ['devanagari', 'latin', 'latin-ext'],
				'variants' => ['200', '300', 'regular', '600', '700', '800', '900'],
			],
			[
				'name' => 'Bitter',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular', 'italic', '700'],
			],
			[
				'name' => 'Black Ops One',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Bokor',
				'subsets' => ['khmer'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Bonbon',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Boogaloo',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Bowlby One',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Bowlby One SC',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Brawler',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Bree Serif',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Bubblegum Sans',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Bubbler One',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Buda',
				'subsets' => ['latin'],
				'variants' => ['300'],
			],
			[
				'name' => 'Buenard',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular', '700'],
			],
			[
				'name' => 'Bungee',
				'subsets' => ['latin', 'latin-ext', 'vietnamese'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Bungee Hairline',
				'subsets' => ['latin', 'latin-ext', 'vietnamese'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Bungee Inline',
				'subsets' => ['latin', 'latin-ext', 'vietnamese'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Bungee Outline',
				'subsets' => ['latin', 'latin-ext', 'vietnamese'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Bungee Shade',
				'subsets' => ['latin', 'latin-ext', 'vietnamese'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Butcherman',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Butterfly Kids',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Cabin',
				'subsets' => ['latin', 'latin-ext', 'vietnamese'],
				'variants' => ['regular', 'italic', '500', '500italic', '600', '600italic', '700', '700italic'],
			],
			[
				'name' => 'Cabin Condensed',
				'subsets' => ['latin', 'latin-ext', 'vietnamese'],
				'variants' => ['regular', '500', '600', '700'],
			],
			[
				'name' => 'Cabin Sketch',
				'subsets' => ['latin'],
				'variants' => ['regular', '700'],
			],
			[
				'name' => 'Caesar Dressing',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Cagliostro',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Cairo',
				'subsets' => ['latin', 'latin-ext', 'arabic'],
				'variants' => ['200', '300', 'regular', '600', '700', '900'],
			],
			[
				'name' => 'Calligraffitti',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Cambay',
				'subsets' => ['devanagari', 'latin', 'latin-ext'],
				'variants' => ['regular', 'italic', '700', '700italic'],
			],
			[
				'name' => 'Cambo',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Candal',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Cantarell',
				'subsets' => ['latin'],
				'variants' => ['regular', 'italic', '700', '700italic'],
			],
			[
				'name' => 'Cantata One',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Cantora One',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Capriola',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Cardo',
				'subsets' => ['greek', 'greek-ext', 'latin', 'latin-ext'],
				'variants' => ['regular', 'italic', '700'],
			],
			[
				'name' => 'Carme',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Carrois Gothic',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Carrois Gothic SC',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Carter One',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Catamaran',
				'subsets' => ['tamil', 'latin', 'latin-ext'],
				'variants' => ['100', '200', '300', 'regular', '500', '600', '700', '800', '900'],
			],
			[
				'name' => 'Caudex',
				'subsets' => ['greek', 'greek-ext', 'latin', 'latin-ext'],
				'variants' => ['regular', 'italic', '700', '700italic'],
			],
			[
				'name' => 'Caveat',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular', '700'],
			],
			[
				'name' => 'Caveat Brush',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Cedarville Cursive',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Ceviche One',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Changa',
				'subsets' => ['latin', 'latin-ext', 'arabic'],
				'variants' => ['200', '300', 'regular', '500', '600', '700', '800'],
			],
			[
				'name' => 'Changa One',
				'subsets' => ['latin'],
				'variants' => ['regular', 'italic'],
			],
			[
				'name' => 'Chango',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Chathura',
				'subsets' => ['telugu', 'latin'],
				'variants' => ['100', '300', 'regular', '700', '800'],
			],
			[
				'name' => 'Chau Philomene One',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular', 'italic'],
			],
			[
				'name' => 'Chela One',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Chelsea Market',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Chenla',
				'subsets' => ['khmer'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Cherry Cream Soda',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Cherry Swash',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular', '700'],
			],
			[
				'name' => 'Chewy',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Chicle',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Chivo',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['300', '300italic', 'regular', 'italic', '700', '700italic', '900', '900italic'],
			],
			[
				'name' => 'Chonburi',
				'subsets' => ['latin', 'thai', 'latin-ext', 'vietnamese'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Cinzel',
				'subsets' => ['latin'],
				'variants' => ['regular', '700', '900'],
			],
			[
				'name' => 'Cinzel Decorative',
				'subsets' => ['latin'],
				'variants' => ['regular', '700', '900'],
			],
			[
				'name' => 'Clicker Script',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Coda',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular', '800'],
			],
			[
				'name' => 'Coda Caption',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['800'],
			],
			[
				'name' => 'Codystar',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['300', 'regular'],
			],
			[
				'name' => 'Coiny',
				'subsets' => ['tamil', 'latin', 'latin-ext', 'vietnamese'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Combo',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Comfortaa',
				'subsets' => ['cyrillic-ext', 'greek', 'latin', 'cyrillic', 'latin-ext', 'vietnamese'],
				'variants' => ['300', 'regular', '700'],
			],
			[
				'name' => 'Coming Soon',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Concert One',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Condiment',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Content',
				'subsets' => ['khmer'],
				'variants' => ['regular', '700'],
			],
			[
				'name' => 'Contrail One',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Convergence',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Cookie',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Copse',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Corben',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular', '700'],
			],
			[
				'name' => 'Cormorant',
				'subsets' => ['cyrillic-ext', 'latin', 'cyrillic', 'latin-ext', 'vietnamese'],
				'variants' => ['300', '300italic', 'regular', 'italic', '500', '500italic', '600', '600italic', '700', '700italic'],
			],
			[
				'name' => 'Cormorant Garamond',
				'subsets' => ['cyrillic-ext', 'latin', 'cyrillic', 'latin-ext', 'vietnamese'],
				'variants' => ['300', '300italic', 'regular', 'italic', '500', '500italic', '600', '600italic', '700', '700italic'],
			],
			[
				'name' => 'Cormorant Infant',
				'subsets' => ['cyrillic-ext', 'latin', 'cyrillic', 'latin-ext', 'vietnamese'],
				'variants' => ['300', '300italic', 'regular', 'italic', '500', '500italic', '600', '600italic', '700', '700italic'],
			],
			[
				'name' => 'Cormorant SC',
				'subsets' => ['cyrillic-ext', 'latin', 'cyrillic', 'latin-ext', 'vietnamese'],
				'variants' => ['300', 'regular', '500', '600', '700'],
			],
			[
				'name' => 'Cormorant Unicase',
				'subsets' => ['cyrillic-ext', 'latin', 'cyrillic', 'latin-ext', 'vietnamese'],
				'variants' => ['300', 'regular', '500', '600', '700'],
			],
			[
				'name' => 'Cormorant Upright',
				'subsets' => ['latin', 'latin-ext', 'vietnamese'],
				'variants' => ['300', 'regular', '500', '600', '700'],
			],
			[
				'name' => 'Courgette',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Cousine',
				'subsets' => ['cyrillic-ext', 'greek', 'greek-ext', 'hebrew', 'latin', 'cyrillic', 'latin-ext', 'vietnamese'],
				'variants' => ['regular', 'italic', '700', '700italic'],
			],
			[
				'name' => 'Coustard',
				'subsets' => ['latin'],
				'variants' => ['regular', '900'],
			],
			[
				'name' => 'Covered By Your Grace',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Crafty Girls',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Creepster',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Crete Round',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular', 'italic'],
			],
			[
				'name' => 'Crimson Text',
				'subsets' => ['latin'],
				'variants' => ['regular', 'italic', '600', '600italic', '700', '700italic'],
			],
			[
				'name' => 'Croissant One',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Crushed',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Cuprum',
				'subsets' => ['cyrillic-ext', 'latin', 'cyrillic', 'latin-ext', 'vietnamese'],
				'variants' => ['regular', 'italic', '700', '700italic'],
			],
			[
				'name' => 'Cutive',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Cutive Mono',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Damion',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Dancing Script',
				'subsets' => ['latin', 'latin-ext', 'vietnamese'],
				'variants' => ['regular', '700'],
			],
			[
				'name' => 'Dangrek',
				'subsets' => ['khmer'],
				'variants' => ['regular'],
			],
			[
				'name' => 'David Libre',
				'subsets' => ['hebrew', 'latin', 'latin-ext', 'vietnamese'],
				'variants' => ['regular', '500', '700'],
			],
			[
				'name' => 'Dawning of a New Day',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Days One',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Dekko',
				'subsets' => ['devanagari', 'latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Delius',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Delius Swash Caps',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Delius Unicase',
				'subsets' => ['latin'],
				'variants' => ['regular', '700'],
			],
			[
				'name' => 'Della Respira',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Denk One',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Devonshire',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Dhurjati',
				'subsets' => ['telugu', 'latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Didact Gothic',
				'subsets' => ['cyrillic-ext', 'greek', 'greek-ext', 'latin', 'cyrillic', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Diplomata',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Diplomata SC',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Domine',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular', '700'],
			],
			[
				'name' => 'Donegal One',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Doppio One',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Dorsa',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Dosis',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['200', '300', 'regular', '500', '600', '700', '800'],
			],
			[
				'name' => 'Dr Sugiyama',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Droid Sans',
				'subsets' => ['latin'],
				'variants' => ['regular', '700'],
			],
			[
				'name' => 'Droid Sans Mono',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Droid Serif',
				'subsets' => ['latin'],
				'variants' => ['regular', 'italic', '700', '700italic'],
			],
			[
				'name' => 'Duru Sans',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Dynalight',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'EB Garamond',
				'subsets' => ['cyrillic-ext', 'latin', 'cyrillic', 'latin-ext', 'vietnamese'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Eagle Lake',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Eater',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Economica',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular', 'italic', '700', '700italic'],
			],
			[
				'name' => 'Eczar',
				'subsets' => ['devanagari', 'latin', 'latin-ext'],
				'variants' => ['regular', '500', '600', '700', '800'],
			],
			[
				'name' => 'Ek Mukta',
				'subsets' => ['devanagari', 'latin', 'latin-ext'],
				'variants' => ['200', '300', 'regular', '500', '600', '700', '800'],
			],
			[
				'name' => 'El Messiri',
				'subsets' => ['latin', 'cyrillic', 'arabic'],
				'variants' => ['regular', '500', '600', '700'],
			],
			[
				'name' => 'Electrolize',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Elsie',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular', '900'],
			],
			[
				'name' => 'Elsie Swash Caps',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular', '900'],
			],
			[
				'name' => 'Emblema One',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Emilys Candy',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Engagement',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Englebert',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Enriqueta',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular', '700'],
			],
			[
				'name' => 'Erica One',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Esteban',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Euphoria Script',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Ewert',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Exo',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['100', '100italic', '200', '200italic', '300', '300italic', 'regular', 'italic', '500', '500italic', '600', '600italic', '700', '700italic', '800', '800italic', '900', '900italic'],
			],
			[
				'name' => 'Exo 2',
				'subsets' => ['latin', 'cyrillic', 'latin-ext'],
				'variants' => ['100', '100italic', '200', '200italic', '300', '300italic', 'regular', 'italic', '500', '500italic', '600', '600italic', '700', '700italic', '800', '800italic', '900', '900italic'],
			],
			[
				'name' => 'Expletus Sans',
				'subsets' => ['latin'],
				'variants' => ['regular', 'italic', '500', '500italic', '600', '600italic', '700', '700italic'],
			],
			[
				'name' => 'Fanwood Text',
				'subsets' => ['latin'],
				'variants' => ['regular', 'italic'],
			],
			[
				'name' => 'Farsan',
				'subsets' => ['latin', 'latin-ext', 'vietnamese', 'gujarati'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Fascinate',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Fascinate Inline',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Faster One',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Fasthand',
				'subsets' => ['khmer'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Fauna One',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Federant',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Federo',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Felipa',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Fenix',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Finger Paint',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Fira Mono',
				'subsets' => ['cyrillic-ext', 'greek', 'greek-ext', 'latin', 'cyrillic', 'latin-ext'],
				'variants' => ['regular', '500', '700'],
			],
			[
				'name' => 'Fira Sans',
				'subsets' => ['cyrillic-ext', 'greek', 'greek-ext', 'latin', 'cyrillic', 'latin-ext', 'vietnamese'],
				'variants' => ['100', '100italic', '200', '200italic', '300', '300italic', 'regular', 'italic', '500', '500italic', '600', '600italic', '700', '700italic', '800', '800italic', '900', '900italic'],
			],
			[
				'name' => 'Fira Sans Condensed',
				'subsets' => ['cyrillic-ext', 'greek', 'greek-ext', 'latin', 'cyrillic', 'latin-ext', 'vietnamese'],
				'variants' => ['100', '100italic', '200', '200italic', '300', '300italic', 'regular', 'italic', '500', '500italic', '600', '600italic', '700', '700italic', '800', '800italic', '900', '900italic'],
			],
			[
				'name' => 'Fira Sans Extra Condensed',
				'subsets' => ['cyrillic-ext', 'greek', 'greek-ext', 'latin', 'cyrillic', 'latin-ext', 'vietnamese'],
				'variants' => ['100', '100italic', '200', '200italic', '300', '300italic', 'regular', 'italic', '500', '500italic', '600', '600italic', '700', '700italic', '800', '800italic', '900', '900italic'],
			],
			[
				'name' => 'Fjalla One',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Fjord One',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Flamenco',
				'subsets' => ['latin'],
				'variants' => ['300', 'regular'],
			],
			[
				'name' => 'Flavors',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Fondamento',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular', 'italic'],
			],
			[
				'name' => 'Fontdiner Swanky',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Forum',
				'subsets' => ['cyrillic-ext', 'latin', 'cyrillic', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Francois One',
				'subsets' => ['latin', 'latin-ext', 'vietnamese'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Frank Ruhl Libre',
				'subsets' => ['hebrew', 'latin', 'latin-ext'],
				'variants' => ['300', 'regular', '500', '700', '900'],
			],
			[
				'name' => 'Freckle Face',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Fredericka the Great',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Fredoka One',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Freehand',
				'subsets' => ['khmer'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Fresca',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Frijole',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Fruktur',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Fugaz One',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'GFS Didot',
				'subsets' => ['greek'],
				'variants' => ['regular'],
			],
			[
				'name' => 'GFS Neohellenic',
				'subsets' => ['greek'],
				'variants' => ['regular', 'italic', '700', '700italic'],
			],
			[
				'name' => 'Gabriela',
				'subsets' => ['cyrillic-ext', 'latin', 'cyrillic'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Gafata',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Galada',
				'subsets' => ['bengali', 'latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Galdeano',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Galindo',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Gentium Basic',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular', 'italic', '700', '700italic'],
			],
			[
				'name' => 'Gentium Book Basic',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular', 'italic', '700', '700italic'],
			],
			[
				'name' => 'Geo',
				'subsets' => ['latin'],
				'variants' => ['regular', 'italic'],
			],
			[
				'name' => 'Geostar',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Geostar Fill',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Germania One',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Gidugu',
				'subsets' => ['telugu', 'latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Gilda Display',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Give You Glory',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Glass Antiqua',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Glegoo',
				'subsets' => ['devanagari', 'latin', 'latin-ext'],
				'variants' => ['regular', '700'],
			],
			[
				'name' => 'Gloria Hallelujah',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Goblin One',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Gochi Hand',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Gorditas',
				'subsets' => ['latin'],
				'variants' => ['regular', '700'],
			],
			[
				'name' => 'Goudy Bookletter 1911',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Graduate',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Grand Hotel',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Gravitas One',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Great Vibes',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Griffy',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Gruppo',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Gudea',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular', 'italic', '700'],
			],
			[
				'name' => 'Gurajada',
				'subsets' => ['telugu', 'latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Habibi',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Halant',
				'subsets' => ['devanagari', 'latin', 'latin-ext'],
				'variants' => ['300', 'regular', '500', '600', '700'],
			],
			[
				'name' => 'Hammersmith One',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Hanalei',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Hanalei Fill',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Handlee',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Hanuman',
				'subsets' => ['khmer'],
				'variants' => ['regular', '700'],
			],
			[
				'name' => 'Happy Monkey',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Harmattan',
				'subsets' => ['latin', 'arabic'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Headland One',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Heebo',
				'subsets' => ['hebrew', 'latin'],
				'variants' => ['100', '300', 'regular', '500', '700', '800', '900'],
			],
			[
				'name' => 'Henny Penny',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Herr Von Muellerhoff',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Hind',
				'subsets' => ['devanagari', 'latin', 'latin-ext'],
				'variants' => ['300', 'regular', '500', '600', '700'],
			],
			[
				'name' => 'Hind Guntur',
				'subsets' => ['telugu', 'latin', 'latin-ext'],
				'variants' => ['300', 'regular', '500', '600', '700'],
			],
			[
				'name' => 'Hind Madurai',
				'subsets' => ['tamil', 'latin', 'latin-ext'],
				'variants' => ['300', 'regular', '500', '600', '700'],
			],
			[
				'name' => 'Hind Siliguri',
				'subsets' => ['bengali', 'latin', 'latin-ext'],
				'variants' => ['300', 'regular', '500', '600', '700'],
			],
			[
				'name' => 'Hind Vadodara',
				'subsets' => ['latin', 'latin-ext', 'gujarati'],
				'variants' => ['300', 'regular', '500', '600', '700'],
			],
			[
				'name' => 'Holtwood One SC',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Homemade Apple',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Homenaje',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'IM Fell DW Pica',
				'subsets' => ['latin'],
				'variants' => ['regular', 'italic'],
			],
			[
				'name' => 'IM Fell DW Pica SC',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'IM Fell Double Pica',
				'subsets' => ['latin'],
				'variants' => ['regular', 'italic'],
			],
			[
				'name' => 'IM Fell Double Pica SC',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'IM Fell English',
				'subsets' => ['latin'],
				'variants' => ['regular', 'italic'],
			],
			[
				'name' => 'IM Fell English SC',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'IM Fell French Canon',
				'subsets' => ['latin'],
				'variants' => ['regular', 'italic'],
			],
			[
				'name' => 'IM Fell French Canon SC',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'IM Fell Great Primer',
				'subsets' => ['latin'],
				'variants' => ['regular', 'italic'],
			],
			[
				'name' => 'IM Fell Great Primer SC',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Iceberg',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Iceland',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Imprima',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Inconsolata',
				'subsets' => ['latin', 'latin-ext', 'vietnamese'],
				'variants' => ['regular', '700'],
			],
			[
				'name' => 'Inder',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Indie Flower',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Inika',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular', '700'],
			],
			[
				'name' => 'Inknut Antiqua',
				'subsets' => ['devanagari', 'latin', 'latin-ext'],
				'variants' => ['300', 'regular', '500', '600', '700', '800', '900'],
			],
			[
				'name' => 'Irish Grover',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Istok Web',
				'subsets' => ['cyrillic-ext', 'latin', 'cyrillic', 'latin-ext'],
				'variants' => ['regular', 'italic', '700', '700italic'],
			],
			[
				'name' => 'Italiana',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Italianno',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Itim',
				'subsets' => ['latin', 'thai', 'latin-ext', 'vietnamese'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Jacques Francois',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Jacques Francois Shadow',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Jaldi',
				'subsets' => ['devanagari', 'latin', 'latin-ext'],
				'variants' => ['regular', '700'],
			],
			[
				'name' => 'Jim Nightshade',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Jockey One',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Jolly Lodger',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Jomhuria',
				'subsets' => ['latin', 'latin-ext', 'arabic'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Josefin Sans',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['100', '100italic', '300', '300italic', 'regular', 'italic', '600', '600italic', '700', '700italic'],
			],
			[
				'name' => 'Josefin Slab',
				'subsets' => ['latin'],
				'variants' => ['100', '100italic', '300', '300italic', 'regular', 'italic', '600', '600italic', '700', '700italic'],
			],
			[
				'name' => 'Joti One',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Judson',
				'subsets' => ['latin', 'latin-ext', 'vietnamese'],
				'variants' => ['regular', 'italic', '700'],
			],
			[
				'name' => 'Julee',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Julius Sans One',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Junge',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Jura',
				'subsets' => ['cyrillic-ext', 'greek', 'latin', 'cyrillic', 'latin-ext'],
				'variants' => ['300', 'regular', '500', '600'],
			],
			[
				'name' => 'Just Another Hand',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Just Me Again Down Here',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Kadwa',
				'subsets' => ['devanagari', 'latin'],
				'variants' => ['regular', '700'],
			],
			[
				'name' => 'Kalam',
				'subsets' => ['devanagari', 'latin', 'latin-ext'],
				'variants' => ['300', 'regular', '700'],
			],
			[
				'name' => 'Kameron',
				'subsets' => ['latin'],
				'variants' => ['regular', '700'],
			],
			[
				'name' => 'Kanit',
				'subsets' => ['latin', 'thai', 'latin-ext', 'vietnamese'],
				'variants' => ['100', '100italic', '200', '200italic', '300', '300italic', 'regular', 'italic', '500', '500italic', '600', '600italic', '700', '700italic', '800', '800italic', '900', '900italic'],
			],
			[
				'name' => 'Kantumruy',
				'subsets' => ['khmer'],
				'variants' => ['300', 'regular', '700'],
			],
			[
				'name' => 'Karla',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular', 'italic', '700', '700italic'],
			],
			[
				'name' => 'Karma',
				'subsets' => ['devanagari', 'latin', 'latin-ext'],
				'variants' => ['300', 'regular', '500', '600', '700'],
			],
			[
				'name' => 'Katibeh',
				'subsets' => ['latin', 'latin-ext', 'arabic'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Kaushan Script',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Kavivanar',
				'subsets' => ['tamil', 'latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Kavoon',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Kdam Thmor',
				'subsets' => ['khmer'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Keania One',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Kelly Slab',
				'subsets' => ['latin', 'cyrillic', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Kenia',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Khand',
				'subsets' => ['devanagari', 'latin', 'latin-ext'],
				'variants' => ['300', 'regular', '500', '600', '700'],
			],
			[
				'name' => 'Khmer',
				'subsets' => ['khmer'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Khula',
				'subsets' => ['devanagari', 'latin', 'latin-ext'],
				'variants' => ['300', 'regular', '600', '700', '800'],
			],
			[
				'name' => 'Kite One',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Knewave',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Kotta One',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Koulen',
				'subsets' => ['khmer'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Kranky',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Kreon',
				'subsets' => ['latin'],
				'variants' => ['300', 'regular', '700'],
			],
			[
				'name' => 'Kristi',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Krona One',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Kumar One',
				'subsets' => ['latin', 'latin-ext', 'gujarati'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Kumar One Outline',
				'subsets' => ['latin', 'latin-ext', 'gujarati'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Kurale',
				'subsets' => ['cyrillic-ext', 'devanagari', 'latin', 'cyrillic', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'La Belle Aurore',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Laila',
				'subsets' => ['devanagari', 'latin', 'latin-ext'],
				'variants' => ['300', 'regular', '500', '600', '700'],
			],
			[
				'name' => 'Lakki Reddy',
				'subsets' => ['telugu', 'latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Lalezar',
				'subsets' => ['latin', 'latin-ext', 'vietnamese', 'arabic'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Lancelot',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Lateef',
				'subsets' => ['latin', 'arabic'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Lato',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['100', '100italic', '300', '300italic', 'regular', 'italic', '700', '700italic', '900', '900italic'],
			],
			[
				'name' => 'League Script',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Leckerli One',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Ledger',
				'subsets' => ['latin', 'cyrillic', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Lekton',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular', 'italic', '700'],
			],
			[
				'name' => 'Lemon',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Lemonada',
				'subsets' => ['latin', 'latin-ext', 'vietnamese', 'arabic'],
				'variants' => ['300', 'regular', '600', '700'],
			],
			[
				'name' => 'Libre Baskerville',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular', 'italic', '700'],
			],
			[
				'name' => 'Libre Franklin',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['100', '100italic', '200', '200italic', '300', '300italic', 'regular', 'italic', '500', '500italic', '600', '600italic', '700', '700italic', '800', '800italic', '900', '900italic'],
			],
			[
				'name' => 'Life Savers',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular', '700'],
			],
			[
				'name' => 'Lilita One',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Lily Script One',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Limelight',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Linden Hill',
				'subsets' => ['latin'],
				'variants' => ['regular', 'italic'],
			],
			[
				'name' => 'Lobster',
				'subsets' => ['latin', 'cyrillic', 'latin-ext', 'vietnamese'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Lobster Two',
				'subsets' => ['latin'],
				'variants' => ['regular', 'italic', '700', '700italic'],
			],
			[
				'name' => 'Londrina Outline',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Londrina Shadow',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Londrina Sketch',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Londrina Solid',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Lora',
				'subsets' => ['cyrillic-ext', 'latin', 'cyrillic', 'latin-ext', 'vietnamese'],
				'variants' => ['regular', 'italic', '700', '700italic'],
			],
			[
				'name' => 'Love Ya Like A Sister',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Loved by the King',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Lovers Quarrel',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Luckiest Guy',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Lusitana',
				'subsets' => ['latin'],
				'variants' => ['regular', '700'],
			],
			[
				'name' => 'Lustria',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Macondo',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Macondo Swash Caps',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Mada',
				'subsets' => ['latin', 'arabic'],
				'variants' => ['300', 'regular', '500', '900'],
			],
			[
				'name' => 'Magra',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular', '700'],
			],
			[
				'name' => 'Maiden Orange',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Maitree',
				'subsets' => ['latin', 'thai', 'latin-ext', 'vietnamese'],
				'variants' => ['200', '300', 'regular', '500', '600', '700'],
			],
			[
				'name' => 'Mako',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Mallanna',
				'subsets' => ['telugu', 'latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Mandali',
				'subsets' => ['telugu', 'latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Marcellus',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Marcellus SC',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Marck Script',
				'subsets' => ['latin', 'cyrillic', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Margarine',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Marko One',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Marmelad',
				'subsets' => ['latin', 'cyrillic', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Martel',
				'subsets' => ['devanagari', 'latin', 'latin-ext'],
				'variants' => ['200', '300', 'regular', '600', '700', '800', '900'],
			],
			[
				'name' => 'Martel Sans',
				'subsets' => ['devanagari', 'latin', 'latin-ext'],
				'variants' => ['200', '300', 'regular', '600', '700', '800', '900'],
			],
			[
				'name' => 'Marvel',
				'subsets' => ['latin'],
				'variants' => ['regular', 'italic', '700', '700italic'],
			],
			[
				'name' => 'Mate',
				'subsets' => ['latin'],
				'variants' => ['regular', 'italic'],
			],
			[
				'name' => 'Mate SC',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Maven Pro',
				'subsets' => ['latin', 'latin-ext', 'vietnamese'],
				'variants' => ['regular', '500', '700', '900'],
			],
			[
				'name' => 'McLaren',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Meddon',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'MedievalSharp',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Medula One',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Meera Inimai',
				'subsets' => ['tamil', 'latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Megrim',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Meie Script',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Merienda',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular', '700'],
			],
			[
				'name' => 'Merienda One',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Merriweather',
				'subsets' => ['cyrillic-ext', 'latin', 'cyrillic', 'latin-ext'],
				'variants' => ['300', '300italic', 'regular', 'italic', '700', '700italic', '900', '900italic'],
			],
			[
				'name' => 'Merriweather Sans',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['300', '300italic', 'regular', 'italic', '700', '700italic', '800', '800italic'],
			],
			[
				'name' => 'Metal',
				'subsets' => ['khmer'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Metal Mania',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Metamorphous',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Metrophobic',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Michroma',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Milonga',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Miltonian',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Miltonian Tattoo',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Miniver',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Miriam Libre',
				'subsets' => ['hebrew', 'latin', 'latin-ext'],
				'variants' => ['regular', '700'],
			],
			[
				'name' => 'Mirza',
				'subsets' => ['latin', 'latin-ext', 'arabic'],
				'variants' => ['regular', '500', '600', '700'],
			],
			[
				'name' => 'Miss Fajardose',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Mitr',
				'subsets' => ['latin', 'thai', 'latin-ext', 'vietnamese'],
				'variants' => ['200', '300', 'regular', '500', '600', '700'],
			],
			[
				'name' => 'Modak',
				'subsets' => ['devanagari', 'latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Modern Antiqua',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Mogra',
				'subsets' => ['latin', 'latin-ext', 'gujarati'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Molengo',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Molle',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['italic'],
			],
			[
				'name' => 'Monda',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular', '700'],
			],
			[
				'name' => 'Monofett',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Monoton',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Monsieur La Doulaise',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Montaga',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Montez',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Montserrat',
				'subsets' => ['latin', 'latin-ext', 'vietnamese'],
				'variants' => ['100', '100italic', '200', '200italic', '300', '300italic', 'regular', 'italic', '500', '500italic', '600', '600italic', '700', '700italic', '800', '800italic', '900', '900italic'],
			],
			[
				'name' => 'Montserrat Alternates',
				'subsets' => ['latin', 'latin-ext', 'vietnamese'],
				'variants' => ['100', '100italic', '200', '200italic', '300', '300italic', 'regular', 'italic', '500', '500italic', '600', '600italic', '700', '700italic', '800', '800italic', '900', '900italic'],
			],
			[
				'name' => 'Montserrat Subrayada',
				'subsets' => ['latin'],
				'variants' => ['regular', '700'],
			],
			[
				'name' => 'Moul',
				'subsets' => ['khmer'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Moulpali',
				'subsets' => ['khmer'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Mountains of Christmas',
				'subsets' => ['latin'],
				'variants' => ['regular', '700'],
			],
			[
				'name' => 'Mouse Memoirs',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Mr Bedfort',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Mr Dafoe',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Mr De Haviland',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Mrs Saint Delafield',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Mrs Sheppards',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Mukta Vaani',
				'subsets' => ['latin', 'latin-ext', 'gujarati'],
				'variants' => ['200', '300', 'regular', '500', '600', '700', '800'],
			],
			[
				'name' => 'Muli',
				'subsets' => ['latin', 'latin-ext', 'vietnamese'],
				'variants' => ['200', '200italic', '300', '300italic', 'regular', 'italic', '600', '600italic', '700', '700italic', '800', '800italic', '900', '900italic'],
			],
			[
				'name' => 'Mystery Quest',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'NTR',
				'subsets' => ['telugu', 'latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Neucha',
				'subsets' => ['latin', 'cyrillic'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Neuton',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['200', '300', 'regular', 'italic', '700', '800'],
			],
			[
				'name' => 'New Rocker',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'News Cycle',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular', '700'],
			],
			[
				'name' => 'Niconne',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Nixie One',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Nobile',
				'subsets' => ['latin'],
				'variants' => ['regular', 'italic', '700', '700italic'],
			],
			[
				'name' => 'Nokora',
				'subsets' => ['khmer'],
				'variants' => ['regular', '700'],
			],
			[
				'name' => 'Norican',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Nosifer',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Nothing You Could Do',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Noticia Text',
				'subsets' => ['latin', 'latin-ext', 'vietnamese'],
				'variants' => ['regular', 'italic', '700', '700italic'],
			],
			[
				'name' => 'Noto Sans',
				'subsets' => ['cyrillic-ext', 'greek', 'greek-ext', 'devanagari', 'latin', 'cyrillic', 'latin-ext', 'vietnamese'],
				'variants' => ['regular', 'italic', '700', '700italic'],
			],
			[
				'name' => 'Noto Serif',
				'subsets' => ['cyrillic-ext', 'greek', 'greek-ext', 'latin', 'cyrillic', 'latin-ext', 'vietnamese'],
				'variants' => ['regular', 'italic', '700', '700italic'],
			],
			[
				'name' => 'Nova Cut',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Nova Flat',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Nova Mono',
				'subsets' => ['greek', 'latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Nova Oval',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Nova Round',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Nova Script',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Nova Slim',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Nova Square',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Numans',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Nunito',
				'subsets' => ['latin', 'latin-ext', 'vietnamese'],
				'variants' => ['200', '200italic', '300', '300italic', 'regular', 'italic', '600', '600italic', '700', '700italic', '800', '800italic', '900', '900italic'],
			],
			[
				'name' => 'Nunito Sans',
				'subsets' => ['latin', 'latin-ext', 'vietnamese'],
				'variants' => ['200', '200italic', '300', '300italic', 'regular', 'italic', '600', '600italic', '700', '700italic', '800', '800italic', '900', '900italic'],
			],
			[
				'name' => 'Odor Mean Chey',
				'subsets' => ['khmer'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Offside',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Old Standard TT',
				'subsets' => ['cyrillic-ext', 'latin', 'cyrillic', 'latin-ext', 'vietnamese'],
				'variants' => ['regular', 'italic', '700'],
			],
			[
				'name' => 'Oldenburg',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Oleo Script',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular', '700'],
			],
			[
				'name' => 'Oleo Script Swash Caps',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular', '700'],
			],
			[
				'name' => 'Open Sans',
				'subsets' => ['cyrillic-ext', 'greek', 'greek-ext', 'latin', 'cyrillic', 'latin-ext', 'vietnamese'],
				'variants' => ['300', '300italic', 'regular', 'italic', '600', '600italic', '700', '700italic', '800', '800italic'],
			],
			[
				'name' => 'Open Sans Condensed',
				'subsets' => ['cyrillic-ext', 'greek', 'greek-ext', 'latin', 'cyrillic', 'latin-ext', 'vietnamese'],
				'variants' => ['300', '300italic', '700'],
			],
			[
				'name' => 'Oranienbaum',
				'subsets' => ['cyrillic-ext', 'latin', 'cyrillic', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Orbitron',
				'subsets' => ['latin'],
				'variants' => ['regular', '500', '700', '900'],
			],
			[
				'name' => 'Oregano',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular', 'italic'],
			],
			[
				'name' => 'Orienta',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Original Surfer',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Oswald',
				'subsets' => ['latin', 'cyrillic', 'latin-ext', 'vietnamese'],
				'variants' => ['200', '300', 'regular', '500', '600', '700'],
			],
			[
				'name' => 'Over the Rainbow',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Overlock',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular', 'italic', '700', '700italic', '900', '900italic'],
			],
			[
				'name' => 'Overlock SC',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Overpass',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['100', '100italic', '200', '200italic', '300', '300italic', 'regular', 'italic', '600', '600italic', '700', '700italic', '800', '800italic', '900', '900italic'],
			],
			[
				'name' => 'Overpass Mono',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['300', 'regular', '600', '700'],
			],
			[
				'name' => 'Ovo',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Oxygen',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['300', 'regular', '700'],
			],
			[
				'name' => 'Oxygen Mono',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'PT Mono',
				'subsets' => ['cyrillic-ext', 'latin', 'cyrillic', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'PT Sans',
				'subsets' => ['cyrillic-ext', 'latin', 'cyrillic', 'latin-ext'],
				'variants' => ['regular', 'italic', '700', '700italic'],
			],
			[
				'name' => 'PT Sans Caption',
				'subsets' => ['cyrillic-ext', 'latin', 'cyrillic', 'latin-ext'],
				'variants' => ['regular', '700'],
			],
			[
				'name' => 'PT Sans Narrow',
				'subsets' => ['cyrillic-ext', 'latin', 'cyrillic', 'latin-ext'],
				'variants' => ['regular', '700'],
			],
			[
				'name' => 'PT Serif',
				'subsets' => ['cyrillic-ext', 'latin', 'cyrillic', 'latin-ext'],
				'variants' => ['regular', 'italic', '700', '700italic'],
			],
			[
				'name' => 'PT Serif Caption',
				'subsets' => ['cyrillic-ext', 'latin', 'cyrillic', 'latin-ext'],
				'variants' => ['regular', 'italic'],
			],
			[
				'name' => 'Pacifico',
				'subsets' => ['latin', 'latin-ext', 'vietnamese'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Padauk',
				'subsets' => ['latin', 'myanmar'],
				'variants' => ['regular', '700'],
			],
			[
				'name' => 'Palanquin',
				'subsets' => ['devanagari', 'latin', 'latin-ext'],
				'variants' => ['100', '200', '300', 'regular', '500', '600', '700'],
			],
			[
				'name' => 'Palanquin Dark',
				'subsets' => ['devanagari', 'latin', 'latin-ext'],
				'variants' => ['regular', '500', '600', '700'],
			],
			[
				'name' => 'Pangolin',
				'subsets' => ['cyrillic-ext', 'latin', 'cyrillic', 'latin-ext', 'vietnamese'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Paprika',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Parisienne',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Passero One',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Passion One',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular', '700', '900'],
			],
			[
				'name' => 'Pathway Gothic One',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Patrick Hand',
				'subsets' => ['latin', 'latin-ext', 'vietnamese'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Patrick Hand SC',
				'subsets' => ['latin', 'latin-ext', 'vietnamese'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Pattaya',
				'subsets' => ['latin', 'thai', 'cyrillic', 'latin-ext', 'vietnamese'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Patua One',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Pavanam',
				'subsets' => ['tamil', 'latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Paytone One',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Peddana',
				'subsets' => ['telugu', 'latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Peralta',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Permanent Marker',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Petit Formal Script',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Petrona',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Philosopher',
				'subsets' => ['cyrillic-ext', 'latin', 'cyrillic', 'vietnamese'],
				'variants' => ['regular', 'italic', '700', '700italic'],
			],
			[
				'name' => 'Piedra',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Pinyon Script',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Pirata One',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Plaster',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Play',
				'subsets' => ['cyrillic-ext', 'greek', 'latin', 'cyrillic', 'latin-ext'],
				'variants' => ['regular', '700'],
			],
			[
				'name' => 'Playball',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Playfair Display',
				'subsets' => ['latin', 'cyrillic', 'latin-ext'],
				'variants' => ['regular', 'italic', '700', '700italic', '900', '900italic'],
			],
			[
				'name' => 'Playfair Display SC',
				'subsets' => ['latin', 'cyrillic', 'latin-ext'],
				'variants' => ['regular', 'italic', '700', '700italic', '900', '900italic'],
			],
			[
				'name' => 'Podkova',
				'subsets' => ['cyrillic-ext', 'latin', 'cyrillic', 'latin-ext', 'vietnamese'],
				'variants' => ['regular', '500', '600', '700', '800'],
			],
			[
				'name' => 'Poiret One',
				'subsets' => ['latin', 'cyrillic', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Poller One',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Poly',
				'subsets' => ['latin'],
				'variants' => ['regular', 'italic'],
			],
			[
				'name' => 'Pompiere',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Pontano Sans',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Poppins',
				'subsets' => ['devanagari', 'latin', 'latin-ext'],
				'variants' => ['300', 'regular', '500', '600', '700'],
			],
			[
				'name' => 'Port Lligat Sans',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Port Lligat Slab',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Pragati Narrow',
				'subsets' => ['devanagari', 'latin', 'latin-ext'],
				'variants' => ['regular', '700'],
			],
			[
				'name' => 'Prata',
				'subsets' => ['cyrillic-ext', 'latin', 'cyrillic', 'vietnamese'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Preahvihear',
				'subsets' => ['khmer'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Press Start 2P',
				'subsets' => ['cyrillic-ext', 'greek', 'latin', 'cyrillic', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Pridi',
				'subsets' => ['latin', 'thai', 'latin-ext', 'vietnamese'],
				'variants' => ['200', '300', 'regular', '500', '600', '700'],
			],
			[
				'name' => 'Princess Sofia',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Prociono',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Prompt',
				'subsets' => ['latin', 'thai', 'latin-ext', 'vietnamese'],
				'variants' => ['100', '100italic', '200', '200italic', '300', '300italic', 'regular', 'italic', '500', '500italic', '600', '600italic', '700', '700italic', '800', '800italic', '900', '900italic'],
			],
			[
				'name' => 'Prosto One',
				'subsets' => ['latin', 'cyrillic', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Proza Libre',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular', 'italic', '500', '500italic', '600', '600italic', '700', '700italic', '800', '800italic'],
			],
			[
				'name' => 'Puritan',
				'subsets' => ['latin'],
				'variants' => ['regular', 'italic', '700', '700italic'],
			],
			[
				'name' => 'Purple Purse',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Quando',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Quantico',
				'subsets' => ['latin'],
				'variants' => ['regular', 'italic', '700', '700italic'],
			],
			[
				'name' => 'Quattrocento',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular', '700'],
			],
			[
				'name' => 'Quattrocento Sans',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular', 'italic', '700', '700italic'],
			],
			[
				'name' => 'Questrial',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Quicksand',
				'subsets' => ['latin', 'latin-ext', 'vietnamese'],
				'variants' => ['300', 'regular', '500', '700'],
			],
			[
				'name' => 'Quintessential',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Qwigley',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Racing Sans One',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Radley',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular', 'italic'],
			],
			[
				'name' => 'Rajdhani',
				'subsets' => ['devanagari', 'latin', 'latin-ext'],
				'variants' => ['300', 'regular', '500', '600', '700'],
			],
			[
				'name' => 'Rakkas',
				'subsets' => ['latin', 'latin-ext', 'arabic'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Raleway',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['100', '100italic', '200', '200italic', '300', '300italic', 'regular', 'italic', '500', '500italic', '600', '600italic', '700', '700italic', '800', '800italic', '900', '900italic'],
			],
			[
				'name' => 'Raleway Dots',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Ramabhadra',
				'subsets' => ['telugu', 'latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Ramaraja',
				'subsets' => ['telugu', 'latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Rambla',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular', 'italic', '700', '700italic'],
			],
			[
				'name' => 'Rammetto One',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Ranchers',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Rancho',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Ranga',
				'subsets' => ['devanagari', 'latin', 'latin-ext'],
				'variants' => ['regular', '700'],
			],
			[
				'name' => 'Rasa',
				'subsets' => ['latin', 'latin-ext', 'gujarati'],
				'variants' => ['300', 'regular', '500', '600', '700'],
			],
			[
				'name' => 'Rationale',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Ravi Prakash',
				'subsets' => ['telugu', 'latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Redressed',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Reem Kufi',
				'subsets' => ['latin', 'arabic'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Reenie Beanie',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Revalia',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Rhodium Libre',
				'subsets' => ['devanagari', 'latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Ribeye',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Ribeye Marrow',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Righteous',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Risque',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Roboto',
				'subsets' => ['cyrillic-ext', 'greek', 'greek-ext', 'latin', 'cyrillic', 'latin-ext', 'vietnamese'],
				'variants' => ['100', '100italic', '300', '300italic', 'regular', 'italic', '500', '500italic', '700', '700italic', '900', '900italic'],
			],
			[
				'name' => 'Roboto Condensed',
				'subsets' => ['cyrillic-ext', 'greek', 'greek-ext', 'latin', 'cyrillic', 'latin-ext', 'vietnamese'],
				'variants' => ['300', '300italic', 'regular', 'italic', '700', '700italic'],
			],
			[
				'name' => 'Roboto Mono',
				'subsets' => ['cyrillic-ext', 'greek', 'greek-ext', 'latin', 'cyrillic', 'latin-ext', 'vietnamese'],
				'variants' => ['100', '100italic', '300', '300italic', 'regular', 'italic', '500', '500italic', '700', '700italic'],
			],
			[
				'name' => 'Roboto Slab',
				'subsets' => ['cyrillic-ext', 'greek', 'greek-ext', 'latin', 'cyrillic', 'latin-ext', 'vietnamese'],
				'variants' => ['100', '300', 'regular', '700'],
			],
			[
				'name' => 'Rochester',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Rock Salt',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Rokkitt',
				'subsets' => ['latin', 'latin-ext', 'vietnamese'],
				'variants' => ['100', '200', '300', 'regular', '500', '600', '700', '800', '900'],
			],
			[
				'name' => 'Romanesco',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Ropa Sans',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular', 'italic'],
			],
			[
				'name' => 'Rosario',
				'subsets' => ['latin'],
				'variants' => ['regular', 'italic', '700', '700italic'],
			],
			[
				'name' => 'Rosarivo',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular', 'italic'],
			],
			[
				'name' => 'Rouge Script',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Rozha One',
				'subsets' => ['devanagari', 'latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Rubik',
				'subsets' => ['hebrew', 'latin', 'cyrillic', 'latin-ext'],
				'variants' => ['300', '300italic', 'regular', 'italic', '500', '500italic', '700', '700italic', '900', '900italic'],
			],
			[
				'name' => 'Rubik Mono One',
				'subsets' => ['latin', 'cyrillic', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Ruda',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular', '700', '900'],
			],
			[
				'name' => 'Rufina',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular', '700'],
			],
			[
				'name' => 'Ruge Boogie',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Ruluko',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Rum Raisin',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Ruslan Display',
				'subsets' => ['latin', 'cyrillic', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Russo One',
				'subsets' => ['latin', 'cyrillic', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Ruthie',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Rye',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Sacramento',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Sahitya',
				'subsets' => ['devanagari', 'latin'],
				'variants' => ['regular', '700'],
			],
			[
				'name' => 'Sail',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Salsa',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Sanchez',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular', 'italic'],
			],
			[
				'name' => 'Sancreek',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Sansita',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular', 'italic', '700', '700italic', '800', '800italic', '900', '900italic'],
			],
			[
				'name' => 'Sarala',
				'subsets' => ['devanagari', 'latin', 'latin-ext'],
				'variants' => ['regular', '700'],
			],
			[
				'name' => 'Sarina',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Sarpanch',
				'subsets' => ['devanagari', 'latin', 'latin-ext'],
				'variants' => ['regular', '500', '600', '700', '800', '900'],
			],
			[
				'name' => 'Satisfy',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Scada',
				'subsets' => ['cyrillic-ext', 'latin', 'cyrillic', 'latin-ext'],
				'variants' => ['regular', 'italic', '700', '700italic'],
			],
			[
				'name' => 'Scheherazade',
				'subsets' => ['latin', 'arabic'],
				'variants' => ['regular', '700'],
			],
			[
				'name' => 'Schoolbell',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Scope One',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Seaweed Script',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Secular One',
				'subsets' => ['hebrew', 'latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Sevillana',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Seymour One',
				'subsets' => ['latin', 'cyrillic', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Shadows Into Light',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Shadows Into Light Two',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Shanti',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Share',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular', 'italic', '700', '700italic'],
			],
			[
				'name' => 'Share Tech',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Share Tech Mono',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Shojumaru',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Short Stack',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Shrikhand',
				'subsets' => ['latin', 'latin-ext', 'gujarati'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Siemreap',
				'subsets' => ['khmer'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Sigmar One',
				'subsets' => ['latin', 'latin-ext', 'vietnamese'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Signika',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['300', 'regular', '600', '700'],
			],
			[
				'name' => 'Signika Negative',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['300', 'regular', '600', '700'],
			],
			[
				'name' => 'Simonetta',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular', 'italic', '900', '900italic'],
			],
			[
				'name' => 'Sintony',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular', '700'],
			],
			[
				'name' => 'Sirin Stencil',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Six Caps',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Skranji',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular', '700'],
			],
			[
				'name' => 'Slabo 13px',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Slabo 27px',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Slackey',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Smokum',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Smythe',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Sniglet',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular', '800'],
			],
			[
				'name' => 'Snippet',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Snowburst One',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Sofadi One',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Sofia',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Sonsie One',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Sorts Mill Goudy',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular', 'italic'],
			],
			[
				'name' => 'Source Code Pro',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['200', '300', 'regular', '500', '600', '700', '900'],
			],
			[
				'name' => 'Source Sans Pro',
				'subsets' => ['latin', 'latin-ext', 'vietnamese'],
				'variants' => ['200', '200italic', '300', '300italic', 'regular', 'italic', '600', '600italic', '700', '700italic', '900', '900italic'],
			],
			[
				'name' => 'Source Serif Pro',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular', '600', '700'],
			],
			[
				'name' => 'Space Mono',
				'subsets' => ['latin', 'latin-ext', 'vietnamese'],
				'variants' => ['regular', 'italic', '700', '700italic'],
			],
			[
				'name' => 'Special Elite',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Spicy Rice',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Spinnaker',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Spirax',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Squada One',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Sree Krushnadevaraya',
				'subsets' => ['telugu', 'latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Sriracha',
				'subsets' => ['latin', 'thai', 'latin-ext', 'vietnamese'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Stalemate',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Stalinist One',
				'subsets' => ['latin', 'cyrillic', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Stardos Stencil',
				'subsets' => ['latin'],
				'variants' => ['regular', '700'],
			],
			[
				'name' => 'Stint Ultra Condensed',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Stint Ultra Expanded',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Stoke',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['300', 'regular'],
			],
			[
				'name' => 'Strait',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Sue Ellen Francisco',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Suez One',
				'subsets' => ['hebrew', 'latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Sumana',
				'subsets' => ['devanagari', 'latin', 'latin-ext'],
				'variants' => ['regular', '700'],
			],
			[
				'name' => 'Sunshiney',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Supermercado One',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Sura',
				'subsets' => ['devanagari', 'latin', 'latin-ext'],
				'variants' => ['regular', '700'],
			],
			[
				'name' => 'Suranna',
				'subsets' => ['telugu', 'latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Suravaram',
				'subsets' => ['telugu', 'latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Suwannaphum',
				'subsets' => ['khmer'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Swanky and Moo Moo',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Syncopate',
				'subsets' => ['latin'],
				'variants' => ['regular', '700'],
			],
			[
				'name' => 'Tangerine',
				'subsets' => ['latin'],
				'variants' => ['regular', '700'],
			],
			[
				'name' => 'Taprom',
				'subsets' => ['khmer'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Tauri',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Taviraj',
				'subsets' => ['latin', 'thai', 'latin-ext', 'vietnamese'],
				'variants' => ['100', '100italic', '200', '200italic', '300', '300italic', 'regular', 'italic', '500', '500italic', '600', '600italic', '700', '700italic', '800', '800italic', '900', '900italic'],
			],
			[
				'name' => 'Teko',
				'subsets' => ['devanagari', 'latin', 'latin-ext'],
				'variants' => ['300', 'regular', '500', '600', '700'],
			],
			[
				'name' => 'Telex',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Tenali Ramakrishna',
				'subsets' => ['telugu', 'latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Tenor Sans',
				'subsets' => ['latin', 'cyrillic', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Text Me One',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'The Girl Next Door',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Tienne',
				'subsets' => ['latin'],
				'variants' => ['regular', '700', '900'],
			],
			[
				'name' => 'Tillana',
				'subsets' => ['devanagari', 'latin', 'latin-ext'],
				'variants' => ['regular', '500', '600', '700', '800'],
			],
			[
				'name' => 'Timmana',
				'subsets' => ['telugu', 'latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Tinos',
				'subsets' => ['cyrillic-ext', 'greek', 'greek-ext', 'hebrew', 'latin', 'cyrillic', 'latin-ext', 'vietnamese'],
				'variants' => ['regular', 'italic', '700', '700italic'],
			],
			[
				'name' => 'Titan One',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Titillium Web',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['200', '200italic', '300', '300italic', 'regular', 'italic', '600', '600italic', '700', '700italic', '900'],
			],
			[
				'name' => 'Trade Winds',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Trirong',
				'subsets' => ['latin', 'thai', 'latin-ext', 'vietnamese'],
				'variants' => ['100', '100italic', '200', '200italic', '300', '300italic', 'regular', 'italic', '500', '500italic', '600', '600italic', '700', '700italic', '800', '800italic', '900', '900italic'],
			],
			[
				'name' => 'Trocchi',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Trochut',
				'subsets' => ['latin'],
				'variants' => ['regular', 'italic', '700'],
			],
			[
				'name' => 'Trykker',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Tulpen One',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Ubuntu',
				'subsets' => ['cyrillic-ext', 'greek', 'greek-ext', 'latin', 'cyrillic', 'latin-ext'],
				'variants' => ['300', '300italic', 'regular', 'italic', '500', '500italic', '700', '700italic'],
			],
			[
				'name' => 'Ubuntu Condensed',
				'subsets' => ['cyrillic-ext', 'greek', 'greek-ext', 'latin', 'cyrillic', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Ubuntu Mono',
				'subsets' => ['cyrillic-ext', 'greek', 'greek-ext', 'latin', 'cyrillic', 'latin-ext'],
				'variants' => ['regular', 'italic', '700', '700italic'],
			],
			[
				'name' => 'Ultra',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Uncial Antiqua',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Underdog',
				'subsets' => ['latin', 'cyrillic', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Unica One',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'UnifrakturCook',
				'subsets' => ['latin'],
				'variants' => ['700'],
			],
			[
				'name' => 'UnifrakturMaguntia',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Unkempt',
				'subsets' => ['latin'],
				'variants' => ['regular', '700'],
			],
			[
				'name' => 'Unlock',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Unna',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular', 'italic', '700', '700italic'],
			],
			[
				'name' => 'VT323',
				'subsets' => ['latin', 'latin-ext', 'vietnamese'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Vampiro One',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Varela',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Varela Round',
				'subsets' => ['hebrew', 'latin', 'latin-ext', 'vietnamese'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Vast Shadow',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Vesper Libre',
				'subsets' => ['devanagari', 'latin', 'latin-ext'],
				'variants' => ['regular', '500', '700', '900'],
			],
			[
				'name' => 'Vibur',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Vidaloka',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Viga',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Voces',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Volkhov',
				'subsets' => ['latin'],
				'variants' => ['regular', 'italic', '700', '700italic'],
			],
			[
				'name' => 'Vollkorn',
				'subsets' => ['latin'],
				'variants' => ['regular', 'italic', '700', '700italic'],
			],
			[
				'name' => 'Voltaire',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Waiting for the Sunrise',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Wallpoet',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Walter Turncoat',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Warnes',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Wellfleet',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Wendy One',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Wire One',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Work Sans',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['100', '200', '300', 'regular', '500', '600', '700', '800', '900'],
			],
			[
				'name' => 'Yanone Kaffeesatz',
				'subsets' => ['latin', 'cyrillic', 'latin-ext', 'vietnamese'],
				'variants' => ['200', '300', 'regular', '700'],
			],
			[
				'name' => 'Yantramanav',
				'subsets' => ['devanagari', 'latin', 'latin-ext'],
				'variants' => ['100', '300', 'regular', '500', '700', '900'],
			],
			[
				'name' => 'Yatra One',
				'subsets' => ['devanagari', 'latin', 'latin-ext'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Yellowtail',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Yeseva One',
				'subsets' => ['cyrillic-ext', 'latin', 'cyrillic', 'latin-ext', 'vietnamese'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Yesteryear',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
			[
				'name' => 'Yrsa',
				'subsets' => ['latin', 'latin-ext'],
				'variants' => ['300', 'regular', '500', '600', '700'],
			],
			[
				'name' => 'Zeyada',
				'subsets' => ['latin'],
				'variants' => ['regular'],
			],
		];
		return $fonts;
	}
}

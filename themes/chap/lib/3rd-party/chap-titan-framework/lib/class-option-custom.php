<?php
/**
 * Custom option
 *
 * @package Titan Framework
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly.
}

/**
 * Custom option class
 *
 * @since 1.0
 */
class ChapTitanFrameworkOptionCustom extends ChapTitanFrameworkOption {

	/**
	 * Default settings specific to this option
	 * @var array
	 */
	public $defaultSecondarySettings = [
		'custom' => '', // Custom HTML
	];

	/**
	 * Display for options and meta
	 */
	public function display() {

		if(!empty($this->settings['name'])) {
			$this->echoOptionHeader();
		} else {
			ob_start();
			$this->echoOptionHeaderBare();
			$header = ob_get_clean();
			echo str_replace('<td', '<td colspan="2"', $header);
		}

		echo wp_kses($this->settings['custom'], array_merge(
			wp_kses_allowed_html('post'),
			[
				'select' => [
					'id' => true,
				],
				'option' => [
					'value' => true,
					'selected' => true,
				],
			]
		));

		if(!empty($this->settings['name'])) {
			$this->echoOptionFooter(false);
		} else {
			$this->echoOptionFooterBare(false);
		}

	}

}

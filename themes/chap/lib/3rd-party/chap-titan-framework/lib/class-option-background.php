<?php
/**
 * Text Option
 *
 * @package Titan Framework
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly.
}

/**
 * Background Option
 *
 * Combines options for backgrounds.
 */
class ChapTitanFrameworkOptionBackground extends ChapTitanFrameworkOption {

	/**
	 * Default settings specific for this option
	 * @var array
	 */
	public $defaultSecondarySettings = [
		/**
		 * Default values for all params.
		 */
		'default_values' => [
			'color' => '',
			'color_2' => '',
			'gradient_angle' => 135,
			'image' => 0,
			'opacity' => 0,
			'size' => 'cover',
			'repeat' => 'initial',
			'position' => 'center center',
		],
		/**
		 * Default settings for options.
		 */
		'default_options' => [
			'color' => [],
			'color_2' => [],
			'gradient_angle' => [
				'default' => 135,
			],
			'image' => [
				'size' => 'large',
			],
			'opacity' => [],
			'size' => [
				'default' => 'cover',
			],
			'repeat' => [
				'default' => 'initial',
			],
			'position' => [
				'default' => 'center center',
			],
		],
		/**
		 * Selector to apply CSS to.
		 */
		'selector' => '',
	];

	protected $options = [];

	/**
	 * Constructor
	 *
	 * @return  void
	 * @since   1.4
	 */
	function __construct($settings, $owner) {
		parent::__construct($settings, $owner);

		if(!isset($this->settings['options']) || !is_array($this->settings['options'])) {
			$this->settings['options'] = [];
		}
		$this->settings['options'] = array_replace_recursive($this->settings['default_options'], $this->settings['options']);

		$this->options['color'] = $owner->createOption(array_merge([
			'id' => $this->settings['id'] . '_background_color',
			'type' => 'color',
			'nodisplay' => true,
		], $this->settings['options']['color']));

		$this->options['color_2'] = $owner->createOption(array_merge([
			'id' => $this->settings['id'] . '_background_color_2',
			'type' => 'color',
			'nodisplay' => true,
		], $this->settings['options']['color_2']));

		$this->options['gradient_angle'] = $owner->createOption(array_merge([
			'id' => $this->settings['id'] . '_background_gradient_angle',
			'type' => 'number',
			'min' => 0,
			'max' => 360,
			'step' => 1,
			'unit' => 'deg',
			'nodisplay' => true,
		], $this->settings['options']['gradient_angle']));

		$this->options['image'] = $owner->createOption(array_merge([
			'id' => $this->settings['id'] . '_background_image',
			'type' => 'upload',
			'nodisplay' => true,
		], $this->settings['options']['image']));

		$this->options['opacity'] = $owner->createOption(array_merge([
			'id' => $this->settings['id'] . '_background_opacity',
			'type' => 'number',
			'min' => 0,
			'max' => 1,
			'step' => 0.05,
			'nodisplay' => true,
		], $this->settings['options']['opacity']));

		$this->options['size'] = $owner->createOption(array_merge([
			'id' => $this->settings['id'] . '_background_size',
			'type' => 'select',
			'options' => [
				'initial' => esc_html__('initial', 'chap'),
				'cover' => esc_html__('cover', 'chap'),
				'contain' => esc_html__('contain', 'chap'),
			],
			'nodisplay' => true,
		], $this->settings['options']['size']));

		$this->options['repeat'] = $owner->createOption(array_merge([
			'id' => $this->settings['id'] . '_background_repeat',
			'type' => 'select',
			'options' => [
				'initial' => esc_html__('initial', 'chap'),
				'repeat' => esc_html__('repeat', 'chap'),
				'repeat-x' => esc_html__('repeat-x', 'chap'),
				'repeat-y' => esc_html__('repeat-y', 'chap'),
				'no-repeat' => esc_html__('no-repeat', 'chap'),
			],
			'nodisplay' => true,
		], $this->settings['options']['repeat']));

		$this->options['position'] = $owner->createOption(array_merge([
			'id' => $this->settings['id'] . '_background_position',
			'type' => 'text',
			'nodisplay' => true,
		], $this->settings['options']['position']));

		add_filter('ctf_generate_css_background_' . $this->getOptionNamespace(), [__CLASS__, 'generateCSSCode'], 10, 2);
	}

	/**
	 * Display for options and meta
	 */
	public function display() {
		$value = $this->getValue();
		$this->echoOptionHeader();

		echo '<div class="ctf-bg">';

			echo '<div>';

				echo '<div class="tf-upload">';
					echo '<label>Image</label>';
					$this->options['image']->displayContent();
				echo '</div>';

				echo '<div>';

					echo '<div>';
						echo '<div class="tf-color">';
							echo '<label>Color</label>';
							$this->options['color']->displayContent();
						echo '</div>';

						echo '<div class="tf-color">';
							echo '<label>';
								esc_html_e('Color 2', 'chap');
								self::help_popup($this->getID() . '_color2', esc_html__('Adding a second color creates a gradient.', 'chap'));
							echo '</label>';
							$this->options['color_2']->displayContent();
						echo '</div>';
					echo '</div>';

					echo '<div class="tf-number">';
						echo '<label>Gradient angle</label>';
						$this->options['gradient_angle']->displayContent();
					echo '</div>';

					echo '<div class="tf-number">';
						echo '<label>Image opacity</label>';
						$this->options['opacity']->displayContent();
					echo '</div>';

				echo '</div>';

			echo '</div>';

			echo '<div>';

				echo '<div class="tf-select">';
					echo '<label>Image size</label>';
					$this->options['size']->displayContent();
				echo '</div>';

				echo '<div class="tf-select">';
					echo '<label>Image repeat</label>';
					$this->options['repeat']->displayContent();
				echo '</div>';

				echo '<div class="tf-text">';
					echo '<label>Image position</label>';
					$this->options['position']->displayContent();
				echo '</div>';

			echo '</div>';

		echo '</div>'; // .ctf-bg

		$this->echoOptionFooter();
	}

	/**
	 * Don't save value.
	 */
	public function setValue($value, $postID = null) {
		return true;
	}

	/**
	 * No value to get.
	 */
	public function getValue($postID = null) {
		return true;
	}

	/**
	 * Cleans the value before saving the option
	 *
	 * @param string $value The value of the option.
	 */
	public function cleanValueForSaving($value) {
		return $value;
	}

	/**
	 * Cleans the raw value for getting
	 *
	 * @param   string $value The raw value
	 * @return  string The cleaned value
	 * @since   1.4
	 */
	public function cleanValueForGetting($value) {
		return $value;
	}

	/**
	 * Generates CSS to be included in our dynamically generated CSS file in
	 * ChapTitanFrameworkCSS, using tf_generate_css_code
	 *
	 * @param   string                   $css The CSS to output
	 * @param   ChapTitanFrameworkOption $option The option object being generated
	 * @return  void
	 * @since   1.3
	 */
	public static function generateCSSCode($css, $option, $postID = null) {
		if(!$option->settings['selector']) {
			return $css;
		}

		$selector = $option->settings['selector'];
		$color = $option->options['color']->getValue($postID);
		$color2 = $option->options['color_2']->getValue($postID);
		if(!empty($color) && !empty($color2)) {
			$angle = (int)$option->options['gradient_angle']->getValue($postID);
			if($angle >= 0 && $angle <= 360) {
				$css .= $selector . ' {
					background-image: linear-gradient(' . esc_attr($angle) . 'deg, ' . esc_attr($color) . ' 0%, ' . esc_attr($color2) . ' 100%);
				}';
			}
		} elseif(!empty($color)) {
			$css .= $selector . ' {
				background-color: ' . esc_attr($color) . ';
			}';
		}

		$image = $option->options['image']->getValue($postID);
		if(!empty($image)) {

			if(is_numeric($image)) {
				/**
				 * Get attached image.
				 */
				$size = 'full';
				if($option->settings['id'] === 'header') {
					$header_img_size = \Chap\Options\get('header_background_full_size');
					if(!$header_img_size) {
						$size = 'large';
					}
				}
				$url = wp_get_attachment_image_src($image, $size);
			} else {
				/**
				 * Image is a direct link (preset).
				 */
				$url = [$image];
			}

			if(!empty($url[0])) {
				$opacity  = $option->options['opacity']->getValue($postID);
				$size     = $option->options['size']->getValue($postID);
				$repeat   = $option->options['repeat']->getValue($postID);
				$position = $option->options['position']->getValue($postID);

				$css .= $selector . ':after {
					position: absolute;
					top: 0;
					left: 0;
					width: 100%;
					height: 100%;
					content: " ";
					background-image: url("' . $url[0] . '");
					opacity: ' . $opacity . ';
					background-size: ' . $size . ';
					background-repeat: ' . $repeat . ';
					background-position: ' . $position . ';
				}';
			}

		}

		return $css;
	}

}

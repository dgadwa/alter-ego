<?php

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class ChapTitanFrameworkOptionColor extends ChapTitanFrameworkOption {

	/**
	 * Default settings
	 * @var array
	 */
	public $defaultSecondarySettings = [
		/**
		 * (Optional) If true, an additional control will become available in the color picker for adjusting the alpha/opacity value of the color. You can get rgba colors with the option.
		 *
		 * @since 1.9
		 * @var boolean
		 */
		'alpha' => false,
	];

	function __construct($settings, $owner) {
		parent::__construct($settings, $owner);
		ctf_add_action_once('admin_enqueue_scripts', [$this, 'enqueueColorPickerScript']);
		ctf_add_action_once('admin_footer', [$this, 'startColorPicker']);
	}


	/**
	 * Display for options and meta
	 *
	 * @since 1.0
	 *
	 * @return void
	 */
	public function display() {
		if(isset($this->settings['nodisplay'])) {
			return;
		}
		$this->echoOptionHeader();
		$this->displayContent();
		$this->echoOptionFooter();
	}

	public function displayContent() {
		printf(
			'<input class="tf-colorpicker" type="text" name="%s" id="%s" value="%s" data-default-color="%s" data-custom-width="0" %s />',
			esc_attr($this->getID()),
			esc_attr($this->getID()),
			esc_attr($this->getValue()),
			esc_attr($this->settings['default']),
			!empty($this->settings['alpha']) ? "data-alpha='true'" : '' // Used by wp-color-picker-alpha
		);
	}


	/**
	 * Enqueue the colorpicker scripts
	 *
	 * @since 1.9
	 *
	 * @return void
	 */
	public function enqueueColorPickerScript() {
		wp_enqueue_script('wp-color-picker');
		wp_enqueue_style('wp-color-picker');

		/**
		 * Color picker alpha needs update after WordPress 4.9.
		 */
		if(version_compare(get_bloginfo('version'), '4.9', '>=')) {
			wp_enqueue_script('wp-color-picker-alpha', ChapTitanFramework::getURL('../js/min/wp-color-picker-alpha-min-2.1.2.js', __FILE__), ['wp-color-picker'], CTF_VERSION);
		} else {
			wp_enqueue_script('wp-color-picker-alpha', ChapTitanFramework::getURL('../js/min/wp-color-picker-alpha-min.js', __FILE__), ['wp-color-picker'], CTF_VERSION);
		}
	}


	/**
	 * Load the javascript to init the colorpicker
	 *
	 * @since 1.9
	 *
	 * @return void
	 */
	public function startColorPicker() {
		?>
		<script>
		jQuery(document).ready(function() {
			'use strict';
			if(typeof jQuery.fn.wpColorPicker !== 'undefined') {
				jQuery('.tf-colorpicker').wpColorPicker();
			}
		});
		</script>
		<?php
	}

}

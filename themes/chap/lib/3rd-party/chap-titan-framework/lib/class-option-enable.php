<?php
/**
 * Enable option
 *
 * @package Titan Framework
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

/**
 * Enable Option
 *
 * A heading for separating your options in an admin page or meta box
 *
 * @since 1.0
 * @type enable
 * @availability Admin Pages|Meta Boxes|Customizer
 */
class ChapTitanFrameworkOptionEnable extends ChapTitanFrameworkOption {

	private static $firstLoad = true;

	/**
	 * Default settings specific for this option
	 * @var array
	 */
	public $defaultSecondarySettings = [
		/**
		 * (Optional) The label to display in the enable portion of the buttons
		 *
		 * @since 1.0
		 * @var string
		 */
		'enabled' => '',

		/**
		 * (Optional) The label to display in the disable portion of the buttons
		 *
		 * @since 1.0
		 * @var string
		 */
		'disabled' => '',
	];

	/*
	 * Display for options and meta
	 */
	public function display() {
		$this->echoOptionHeader();

		if(empty($this->settings['enabled'])) {
			$this->settings['enabled'] = esc_html__('Enabled', 'chap');
		}
		if(empty($this->settings['disabled'])) {
			$this->settings['disabled'] = esc_html__('Disabled', 'chap');
		}

		?>
		<input name="<?php echo esc_attr($this->getID()); ?>" type="checkbox" id="<?php echo esc_attr($this->getID()); ?>" value="1" <?php checked($this->getValue(), 1); ?>>
		<span class="button button-<?php echo checked($this->getValue(), 1, false) ? 'primary' : 'secondary'; ?>"><?php echo esc_html($this->settings['enabled']); ?></span><span class="button button-<?php echo checked($this->getValue(), 1, false) ? 'secondary' : 'primary'; ?>"><?php echo esc_html($this->settings['disabled']); ?></span>
		<?php

		// load the javascript to init the colorpicker
		if(self::$firstLoad):
			?>
			<script>
			jQuery(document).ready(function($) {
				"use strict";
				$('body').on('click', '.tf-enable .button-secondary', function(){
					$(this).parent().find('.button').toggleClass('button-primary button-secondary');
					var checkBox = $(this).parents('.tf-enable').find('input');
					if(checkBox.is(':checked')) {
						checkBox.removeAttr('checked');
					} else {
						checkBox.attr('checked', 'checked');
					}
					checkBox.trigger('change');
				});
			});
			</script>
			<?php
		endif;

		$this->echoOptionFooter();

		self::$firstLoad = false;
	}

	public function cleanValueForSaving($value) {
		return $value != '1' ? '0' : '1';
	}

	public function cleanValueForGetting($value) {
		if(is_bool($value)) {
			return $value;
		}
		return $value == '1' ? true : false;
	}

}

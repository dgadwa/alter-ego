<?php

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class ChapTitanFrameworkOptionSave extends ChapTitanFrameworkOption {

	public $defaultSecondarySettings = [
		'save' => '',
		'reset' => '',
		'use_reset' => true,
		'reset_question' => '',
		'action' => 'save',
	];

	public function display() {
		if(!empty($this->owner->postID)) {
			return;
		}

		if(empty($this->settings['save'])) {
			$this->settings['save'] = esc_html__('Save Changes', 'chap');
		}
		if(empty($this->settings['reset'])) {
			$this->settings['reset'] = esc_html__('Reset to defaults', 'chap');
		}
		if(empty($this->settings['reset_question'])) {
			$this->settings['reset_question'] = esc_html__('Reset options in the current tab to their default values?', 'chap');
		}

		?>
			</tbody>
		</table>

		<p class="submit">
			<button name="action" value="<?php echo esc_attr($this->settings['action']); ?>" class="button button-primary">
				<?php echo esc_html($this->settings['save']); ?>
			</button>

			<?php if($this->settings['use_reset']): ?>
			<button name="action" class="button button-secondary"
				onclick="javascript:if(confirm('<?php echo htmlentities(esc_attr($this->settings['reset_question'])); ?>')){jQuery('#tf-reset-form').submit();}jQuery(this).blur(); return false;">
				<?php echo esc_html($this->settings['reset']); ?>
			</button>
			<?php endif; ?>
		</p>

		<table class="form-table">
			<tbody>
		<?php
	}

}

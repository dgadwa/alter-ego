<?php
/**
 * Text Option
 *
 * @package Titan Framework
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly.
}

/**
 * Box Option
 *
 * Modify HTML box model
 */
class ChapTitanFrameworkOptionBox extends ChapTitanFrameworkOptionColor {

	/**
	 * Default settings specific for this option
	 * @var array
	 */
	public $defaultSecondarySettings = [
		/**
		 * What can be modified.
		 */
		'box' => [
			'margin',
			'border',
			'padding',
		],
		/**
		 * What params can be modified.
		 */
		'params' => [
			'margin' => ['top', 'right', 'bottom', 'left'],
			'border' => ['top', 'right', 'bottom', 'left'],
			'padding' => ['top', 'right', 'bottom', 'left'],
		],
		/**
		 * Default values for all params.
		 */
		'default_values' => [
			'margin' => [
				'top' => [
					'value' => 0,
					'units' => 'em',
				],
				'right' => [
					'value' => 0,
					'units' => 'em',
				],
				'bottom' => [
					'value' => 0,
					'units' => 'em',
				],
				'left' => [
					'value' => 0,
					'units' => 'em',
				],
			],
			'border' => [
				'top' => [
					'value' => 0,
					'units' => 'px',
					'style' => 'solid',
					'color' => '',
				],
				'right' => [
					'value' => 0,
					'units' => 'px',
					'style' => 'solid',
					'color' => '',
				],
				'bottom' => [
					'value' => 0,
					'units' => 'px',
					'style' => 'solid',
					'color' => '',
				],
				'left' => [
					'value' => 0,
					'units' => 'px',
					'style' => 'solid',
					'color' => '',
				],
			],
			'padding' => [
				'top' => [
					'value' => 0,
					'units' => 'em',
				],
				'right' => [
					'value' => 0,
					'units' => 'em',
				],
				'bottom' => [
					'value' => 0,
					'units' => 'em',
				],
				'left' => [
					'value' => 0,
					'units' => 'em',
				],
			],
		],
		/**
		 * Selector to apply CSS to.
		 */
		'selector' => '',
	];

	/**
	 * Constructor
	 *
	 * @return  void
	 * @since   1.4
	 */
	function __construct($settings, $owner) {
		parent::__construct($settings, $owner);

		if(!is_array($this->settings['default'])) {
			$this->settings['default'] = [];
		}
		$this->settings['default'] = array_replace_recursive($this->settings['default_values'], $this->settings['default']);

		add_filter('ctf_generate_css_box_' . $this->getOptionNamespace(), [__CLASS__, 'generateCSSCode'], 10, 2);
	}

	/**
	 * Display for options and meta
	 */
	public function display() {
		$value = $this->getValue();
		$this->echoOptionHeader();

		echo '<div class="ctf-bm">';

		foreach($this->settings['box'] as $type) {
			echo '<button class="ctf-bm-type button ctf-bm-choose" style="margin-right:4px" data-bm-type="' . esc_attr($type) . '">' . esc_html(ucfirst($type)) . '</button>';
		}

		foreach($this->settings['box'] as $type) {

			echo '<div class="ctf-bm-params" data-bm-type="' . esc_attr($type) . '">';

			foreach($this->settings['params'][$type] as $param) {

				echo '<div class="ctf-bm-param">';

				printf(
					'<label>' .
						'<code>' .
							'%s-%s' .
						'</code>' .
					'</label>',
					esc_html($type),
					esc_html($param)
				);

				printf(
					'<input class="small-text" type="number" step="any" name="%s[%s][%s][value]" value="%s" />',
					esc_attr($this->getID()),
					esc_attr($type),
					esc_attr($param),
					esc_attr($value[$type][$param]['value'])
				);

				printf(
					'<select name="%s[%s][%s][units]">%s</select>',
					esc_attr($this->getID()),
					esc_attr($type),
					esc_attr($param),
					join('', self::getUnitOptions($value[$type][$param]['units']))
				);

				if($type === 'border') {
					printf(
						'<select name="%s[%s][%s][style]">%s</select>',
						esc_attr($this->getID()),
						esc_attr($type),
						esc_attr($param),
						join('', self::getBorderStyleOptions($value[$type][$param]['style']))
					);

					printf(
						'<div class="ctf-bm-colorpicker tf-color">' .
							'<input class="tf-colorpicker" type="text" name="%s[%s][%s][color]" value="%s" data-default-color="" data-custom-width="0" data-alpha="true" />' .
						'</div>',
						esc_attr($this->getID()),
						esc_attr($type),
						esc_attr($param),
						esc_attr($value[$type][$param]['color'])
					);
				}

				echo '</div>'; // .ctf-bm-param

			}

			printf(
				'<p>' .
					'<label>' .
						'<input type="checkbox" name="%s[%s][output_zero]" value="1"%s />' .
						'%s' .
					'</label>' .
				'</p>',
				esc_attr($this->getID()),
				esc_attr($type),
				checked(1, (int)isset($value[$type]['output_zero']) ? $value[$type]['output_zero'] : 0, false),
				esc_html__('Output zero values.', 'chap')
			);

			echo '</div>'; // .ctf-bm-params

		}

		echo '</div>'; // .ctf-bm

		$this->echoOptionFooter();
	}

	protected static function getUnitOptions($selected = 'px') {
		$options = [];
		$units = [
			'px',
			'em',
			'rem',
			'%',
			'vw',
			'vh',
		];
		foreach($units as $unit) {
			$options[] = sprintf(
				'<option value="%s"%s>%s</option>',
				esc_attr($unit),
				selected($unit, $selected, false),
				esc_html($unit)
			);
		}
		return $options;
	}

	protected static function getBorderStyleOptions($selected = 'solid') {
		$options = [];
		$styles = [
			'solid',
			'dotted',
			'dashed',
			'double',
			'groove',
			'ridge',
			'inset',
			'outset',
			'none',
			'hidden',
		];
		foreach($styles as $style) {
			$options[] = sprintf(
				'<option value="%s"%s>%s</option>',
				esc_attr($style),
				selected($style, $selected, false),
				esc_html($style)
			);
		}
		return $options;
	}

	/**
	 * Cleans the value before saving the option
	 *
	 * @param string $value The value of the option.
	 */
	public function cleanValueForSaving($value) {
		if(is_array($value)) {
			$value = serialize($value);
		}
		return $value;
	}

	/**
	 * Cleans the raw value for getting
	 *
	 * @param   string $value The raw value
	 * @return  string The cleaned value
	 * @since   1.4
	 */
	public function cleanValueForGetting($value) {
		if(is_string($value)) {
			$value = maybe_unserialize($value);
		}
		return $value;
	}

	/**
	 * Generates CSS to be included in our dynamically generated CSS file in
	 * ChapTitanFrameworkCSS, using tf_generate_css_code
	 *
	 * @param   string                   $css The CSS to output
	 * @param   ChapTitanFrameworkOption $option The option object being generated
	 * @return  void
	 * @since   1.3
	 */
	public static function generateCSSCode($css, $option) {
		if(!$option->settings['selector']) {
			return $css;
		}

		$box = $option->settings['box'];
		$value = $option->getValue();
		$output = '';
		$sass_vars = '';

		foreach($box as $type) {
			$params = $option->settings['params'][$type];
			$zero = isset($value[$type]['output_zero']) ? (bool)$value[$type]['output_zero'] : false;
			foreach($params as $param) {
				$val = (float)$value[$type][$param]['value'];
				$units = $value[$type][$param]['units'];
				$sass_vars .= '$' . $type . ucfirst($param) . ':' . $val . $units . ';';

				if($val == 0) {
					if($zero) {
						$units = '';
					} else {
						continue;
					}
				}

				$style = '';
				$color = '';
				if($type === 'border' && $val > 0) {
					$style = ' ' . $value[$type][$param]['style'];
					$color = ' ' . $value[$type][$param]['color'];
				}

				$output .= join('-', [$type, $param]) . ':' . trim(join(' ', [$val . $units, $style, $color])) . ';';
			}
		}

		if($output) {
			$css .= $option->settings['selector'] . '{' . $output . '}';

			if($option->settings['css']) {
				$css .= $sass_vars;
				$css .= $option->settings['css'];
			}
		}

		return $css;
	}

}

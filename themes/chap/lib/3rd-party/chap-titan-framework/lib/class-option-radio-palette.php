<?php

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

class ChapTitanFrameworkOptionRadioPalette extends ChapTitanFrameworkOption {

	public $defaultSecondarySettings = [
		'options' => [],
	];

	function __construct($settings, $owner) {
		parent::__construct($settings, $owner);
	}

	/*
	 * Display for options and meta
	 */
	public function display() {
		if(empty($this->settings['options'])) {
			return;
		}
		if($this->settings['options'] == []) {
			return;
		}

		$this->echoOptionHeader();

		// Get the correct value, since we are accepting indices in the default setting
		$value = $this->getValue();

		// print the palettes
		foreach($this->settings['options'] as $key => $colorSet) {
			printf('<label id="%s"><input id="%s" type="radio" name="%s" value="%s" %s/> <span>',
				$this->getID() . $key,
				$this->getID() . $key,
				$this->getID(),
				esc_attr($key),
				$value == $colorSet ? 'checked="checked"' : '' // can't use checked with arrays
			);
			if(!is_array($colorSet)) {
				continue;
			}
			foreach($colorSet as $color) {
				echo "<span style='background: {$color}'></span>";
			}
			echo '</span></label>';
		}

		$this->echoOptionFooter();
	}

	// Save the index of the selected palette
	public function cleanValueForSaving($value) {
		if(!is_array($this->settings['options'])) {
			return $value;
		}
		// if the key above is zero, we will get a blank value
		if($value == '') {
			$value = 0;
		}
		return $value;
	}

	// The value we should return is an array of the selected colors
	public function cleanValueForGetting($value) {
		if(is_array($value)) {
			return $value;
		}
		$value = stripslashes($value);
		if(is_serialized($value)) {
			return unserialize($value);
		}
		if(empty($value)) {
			$value = 0;
		}
		if(array_key_exists($value, $this->settings['options'])) {
			return $this->settings['options'][ $value ];
		}
		return $value;
	}

}

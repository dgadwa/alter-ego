<?php
/**
 * Envato Theme Setup Wizard Class
 *
 * Takes new users through some basic steps to setup their ThemeForest theme.
 * Based off the WooThemes installer.
 *
 * @author      dtbaker
 * @author      vburlak
 * @package     envato_wizard
 * @version     1.2.7
 */

if(!defined('ABSPATH')) {
	exit;
}

/**
 * Envato_Theme_Setup_Wizard class.
 */
class Envato_Theme_Setup_Wizard {

	/**
	 * The class version number.
	 *
	 * @since 1.1.1
	 * @access private
	 *
	 * @var string
	 */
	protected $version = '1.2.6';

	/**
	 * @var string Current theme name, used as namespace in actions.
	 */
	protected $theme_name = '';

	/**
	 * @var string Theme author username, used in check for oauth.
	 */
	protected $envato_username = '';

	/**
	 * @var string Full url to server-script.php (available from https://gist.github.com/dtbaker).
	 */
	protected $oauth_script = '';

	/**
	 * @var array Steps for the setup wizard.
	 */
	protected $steps = [];

	/**
	 * @var string Current Step.
	 */
	protected $step = '';

	/**
	 * Relative plugin path.
	 *
	 * @since 1.1.2
	 *
	 * @var string
	 */
	protected $plugin_path = '';

	/**
	 * Relative plugin url for this plugin folder, used when enquing scripts.
	 *
	 * @since 1.1.2
	 *
	 * @var string
	 */
	protected $plugin_url = '';

	/**
	 * The slug name to refer to this menu.
	 *
	 * @since 1.1.1
	 *
	 * @var string
	 */
	protected $page_slug;

	/**
	 * TGMPA instance storage.
	 *
	 * @var object
	 */
	protected $tgmpa_instance;

	/**
	 * TGMPA Menu slug.
	 *
	 * @var string
	 */
	protected $tgmpa_menu_slug = 'tgmpa-install-plugins';

	/**
	 * TGMPA Menu url.
	 *
	 * @var string
	 */
	protected $tgmpa_url = 'themes.php?page=tgmpa-install-plugins';

	/**
	 * The slug name for the parent menu.
	 *
	 * @since 1.1.2
	 *
	 * @var string
	 */
	protected $page_parent;

	/**
	 * Complete URL to Setup Wizard.
	 *
	 * @since 1.1.2
	 *
	 * @var string
	 */
	protected $page_url;

	/**
	 * Holds the current instance of the theme manager.
	 *
	 * @since 1.1.3
	 * @var Envato_Theme_Setup_Wizard
	 */
	private static $instance = null;

	/**
	 * @since 1.1.3
	 *
	 * @return Envato_Theme_Setup_Wizard
	 */
	public static function get_instance() {

		if(!self::$instance) {
			self::$instance = new self;
		}

		return self::$instance;

	}

	/**
	 * @see Envato_Theme_Setup_Wizard::instance()
	 *
	 * @since 1.1.1
	 * @access private
	 */
	public function __construct() {

		$this->init_globals();
		$this->init_actions();

	}

	/**
	 * Get the default logo width. Can be overriden by theme init scripts.
	 *
	 * @see Envato_Theme_Setup_Wizard::instance()
	 *
	 * @since 1.1.9
	 * @access public
	 */
	public function get_header_logo_width() {
		return '100px';
	}

	/**
	 * Get the default logo image. Can be overriden by theme init scripts.
	 *
	 * @see Envato_Theme_Setup_Wizard::instance()
	 *
	 * @since 1.1.9
	 * @access public
	 */
	public function get_logo_image() {
		return;
	}

	/**
	 * Setup the class globals.
	 *
	 * @since 1.1.1
	 * @access public
	 */
	public function init_globals() {

		$this->theme_name      = 'chap';
		$this->envato_username = apply_filters($this->theme_name . '_theme_setup_wizard_username', '');
		$this->oauth_script    = apply_filters($this->theme_name . '_theme_setup_wizard_oauth_script', '');
		$this->page_slug       = apply_filters($this->theme_name . '_theme_setup_wizard_page_slug', $this->theme_name . '-setup');
		$this->parent_slug     = apply_filters($this->theme_name . '_theme_setup_wizard_parent_slug', '');

		// If we have parent slug - set correct url.
		if($this->parent_slug !== '') {
			$this->page_url = 'admin.php?page=' . $this->page_slug;
		} else {
			$this->page_url = 'themes.php?page=' . $this->page_slug;
		}
		$this->page_url = apply_filters($this->theme_name . '_theme_setup_wizard_page_url', $this->page_url);

		// Set relative plugin path url.
		$this->plugin_path = trailingslashit($this->cleanFilePath(dirname(__FILE__)));
		$relative_url      = str_replace($this->cleanFilePath(get_template_directory()), '', $this->plugin_path);
		$this->plugin_url  = trailingslashit(get_template_directory_uri() . $relative_url);

	}

	/**
	 * Setup the hooks, actions and filters.
	 *
	 * @uses add_action() To add actions.
	 * @uses add_filter() To add filters.
	 *
	 * @since 1.1.1
	 * @access public
	 */
	public function init_actions() {
		if(apply_filters($this->theme_name . '_enable_setup_wizard', true) && current_user_can('manage_options')) {
			add_action('after_switch_theme', [$this, 'switch_theme']);

			if(class_exists('TGM_Plugin_Activation') && isset($GLOBALS['tgmpa'])) {
				add_action('init', [$this, 'get_tgmpa_instanse'], 30);
				add_action('init', [$this, 'set_tgmpa_url'], 40);
			}

			add_action('admin_menu', [$this, 'admin_menus']);
			add_action('admin_enqueue_scripts', [$this, 'enqueue_scripts']);
			add_action('admin_init', [$this, 'admin_redirects'], 30);
			add_action('admin_init', [$this, 'init_wizard_steps'], 30);
			add_action('admin_init', [$this, 'setup_wizard'], 30);
			add_filter('tgmpa_load', [$this, 'tgmpa_load'], 10, 1);
			add_action('wp_ajax_envato_setup_plugins', [$this, 'ajax_plugins']);
			add_action('wp_ajax_envato_setup_content', [$this, 'ajax_content']);
		}
		if(function_exists('envato_market')) {
			add_action('admin_init', [$this, 'envato_market_admin_init'], 20);
			add_filter('http_request_args', [$this, 'envato_market_http_request_args'], 10, 2);
			add_action('wp_ajax_chap_update_notice_handler', [$this, 'ajax_notice_handler']);
			add_action('admin_notices', [$this, 'admin_theme_auth_notice']);
		}
	}

	/**
	 * We determine if the user already has theme content installed. This can happen if swapping from a previous theme or updated the current theme. We change the UI a bit when updating / swapping to a new theme.
	 *
	 * @since 1.1.8
	 * @access public
	 */
	public function is_possible_upgrade() {
		return false;
	}

	public function enqueue_scripts() {}

	public function tgmpa_load($status) {
		return is_admin() || current_user_can('install_themes');
	}

	public function switch_theme() {
		set_transient('_' . $this->theme_name . '_activation_redirect', 1);
	}

	public function admin_redirects() {
		ob_start();
		if(!get_transient('_' . $this->theme_name . '_activation_redirect') || get_option('envato_setup_complete', false)) {
			return;
		}
		delete_transient('_' . $this->theme_name . '_activation_redirect');
		wp_safe_redirect(admin_url($this->page_url));
		exit;
	}

	/**
	 * Get configured TGMPA instance
	 *
	 * @access public
	 * @since 1.1.2
	 */
	public function get_tgmpa_instanse() {
		$this->tgmpa_instance = call_user_func([get_class($GLOBALS['tgmpa']), 'get_instance']);
	}

	/**
	 * Update $tgmpa_menu_slug and $tgmpa_parent_slug from TGMPA instance
	 *
	 * @access public
	 * @since 1.1.2
	 */
	public function set_tgmpa_url() {

		$this->tgmpa_menu_slug = (property_exists($this->tgmpa_instance, 'menu')) ? $this->tgmpa_instance->menu : $this->tgmpa_menu_slug;
		$this->tgmpa_menu_slug = apply_filters($this->theme_name . '_theme_setup_wizard_tgmpa_menu_slug', $this->tgmpa_menu_slug);

		$tgmpa_parent_slug = (property_exists($this->tgmpa_instance, 'parent_slug') && $this->tgmpa_instance->parent_slug !== 'themes.php') ? 'admin.php' : 'themes.php';

		$this->tgmpa_url = apply_filters($this->theme_name . '_theme_setup_wizard_tgmpa_url', $tgmpa_parent_slug . '?page=' . $this->tgmpa_menu_slug);

	}

	/**
	 * Add admin menus/screens.
	 */
	public function admin_menus() {

		if($this->is_submenu_page()) {
			//prevent Theme Check warning about "themes should use add_theme_page for adding admin pages"
			$add_subpage_function = 'add_submenu' . '_page';
			$add_subpage_function(
				$this->parent_slug,
				esc_html__('Setup Wizard', 'chap'),
				esc_html__('Setup Wizard', 'chap'),
				'manage_options',
				$this->page_slug,
				[
					$this,
					'setup_wizard',
				]
			);
		} else {
			add_theme_page(
				esc_html__('Setup Wizard', 'chap'),
				esc_html__('Setup Wizard', 'chap'),
				'manage_options',
				$this->page_slug,
				[
					$this,
					'setup_wizard',
				]
			);
		}

	}


	/**
	 * Setup steps.
	 *
	 * @since 1.1.1
	 * @access public
	 * @return array
	 */
	public function init_wizard_steps() {

		$this->steps = [
			'introduction' => [
				'name'    => esc_html__('Introduction', 'chap'),
				'view'    => [$this, 'envato_setup_introduction'],
				'handler' => [$this, 'envato_setup_introduction_save'],
			],
		];
		if(class_exists('TGM_Plugin_Activation') && isset($GLOBALS['tgmpa'])) {
			$this->steps['default_plugins'] = [
				'name'    => esc_html__('Plugins', 'chap'),
				'view'    => [$this, 'envato_setup_default_plugins'],
				'handler' => '',
			];
		}
		$this->steps['updates'] = [
			'name'    => esc_html__('Updates', 'chap'),
			'view'    => [$this, 'envato_setup_updates'],
			'handler' => [$this, 'envato_setup_updates_save'],
		];
		$this->steps['help_support'] = [
			'name'    => esc_html__('Support', 'chap'),
			'view'    => [$this, 'envato_setup_help_support'],
			'handler' => '',
		];
		$this->steps['next_steps'] = [
			'name'    => esc_html__('Ready!', 'chap'),
			'view'    => [$this, 'envato_setup_ready'],
			'handler' => '',
		];

		$this->steps = apply_filters($this->theme_name . '_theme_setup_wizard_steps', $this->steps);

	}

	/**
	 * Show the setup wizard
	 */
	public function setup_wizard() {
		if(empty($_GET['page']) || $this->page_slug !== $_GET['page']) {
			return;
		}
		ob_end_clean();

		$this->step = isset($_GET['step']) ? sanitize_key($_GET['step']) : current(array_keys($this->steps));

		wp_register_script('jquery-blockui', $this->plugin_url . 'js/jquery.blockUI.js', ['jquery'], '2.70', true);
		wp_register_script('envato-setup', $this->plugin_url . 'js/envato-setup.js', [
			'jquery',
			'jquery-blockui',
		], $this->version);
		wp_localize_script('envato-setup', 'envato_setup_params', [
			'tgm_plugin_nonce' => [
				'update' => wp_create_nonce('tgmpa-update'),
				'install' => wp_create_nonce('tgmpa-install'),
			],
			'tgm_bulk_url' => admin_url($this->tgmpa_url),
			'ajaxurl' => admin_url('admin-ajax.php'),
			'wpnonce' => wp_create_nonce('envato_setup_nonce'),
			'verify_text' => esc_html__('...verifying', 'chap'),
		]);

		wp_enqueue_style('envato-setup', $this->plugin_url . 'css/envato-setup.css', [
			'wp-admin',
			'dashicons',
			'install',
		], $this->version);

		//enqueue style for admin notices
		wp_enqueue_style('wp-admin');

		wp_enqueue_media();
		wp_enqueue_script('media');

		ob_start();
		$this->setup_wizard_header();
		$this->setup_wizard_steps();
		$show_content = true;
		echo '<div class="envato-setup-content">';
		if(!empty($_REQUEST['save_step']) && isset($this->steps[$this->step]['handler'])) {
			$show_content = call_user_func($this->steps[$this->step]['handler']);
		}
		if($show_content) {
			$this->setup_wizard_content();
		}
		echo '</div>';
		$this->setup_wizard_footer();
		exit;
	}

	public function get_step_link($step) {
		return add_query_arg('step', $step, admin_url('admin.php?page=' . $this->page_slug));
	}

	public function get_next_step_link() {
		$keys = array_keys($this->steps);

		return add_query_arg('step', $keys[array_search($this->step, array_keys($this->steps)) + 1], remove_query_arg('translation_updated'));
	}

	/**
	 * Setup Wizard Header
	 */
	public function setup_wizard_header() {
		?>
		<!DOCTYPE html>
		<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>
		<head>
		<meta name="viewport" content="width=device-width"/>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<?php
		// avoid theme check issues.
		echo '<t';
		echo 'itle>' . esc_html__('Theme &rsaquo; Setup Wizard', 'chap') . '</ti' . 'tle>'; ?>
				<?php wp_print_scripts('envato-setup'); ?>
				<?php @do_action('admin_print_styles'); ?>
				<?php @do_action('admin_print_scripts'); ?>
				<?php @do_action('admin_head'); // Some plugins may throw notices. ?>
			</head>
			<body class="envato-setup wp-core-ui">
			<h1 id="wc-logo">
				<a href="https://chap.website" target="_blank">
			<?php
			$image_url = $this->get_logo_image();
			if($image_url) {
				$image = '<img class="setup-logo" src="%s" alt="%s" style="width:%s;height:auto" />';
				printf(
					$image,
					$image_url,
					$this->theme_name,
					$this->get_header_logo_width()
				);
			} else {
				?>
				<img src="<?php echo esc_url($this->plugin_url . 'images/logo.png'); ?>" alt="Envato install wizard" />
				<?php
			}
			?>
			</a>
			</h1>
			<?php
	}

	/**
	 * Setup Wizard Footer
	 */
	public function setup_wizard_footer() {
		?>
		<?php if('next_steps' === $this->step): ?>
		<a class="wc-return-to-dashboard" href="<?php echo esc_url(admin_url()); ?>"><?php esc_html_e('Return to the WordPress Dashboard', 'chap'); ?></a>
		<?php endif; ?>
		</body>
		<?php
			@do_action('admin_footer'); // this was spitting out some errors in some admin templates. quick @ fix until I have time to find out what's causing errors.
			do_action('admin_print_footer_scripts');
		?>
		</html>
		<?php
	}

	/**
	 * Output the steps
	 */
	public function setup_wizard_steps() {
		$ouput_steps = $this->steps;
		array_shift($ouput_steps);
		?>
		<ol class="envato-setup-steps">
			<?php foreach($ouput_steps as $step_key => $step): ?>
				<?php
					$li_class = '';
					$show_link = false;
					if($step_key === $this->step) {
						$li_class = 'active';
					} elseif(array_search($this->step, array_keys($this->steps)) > array_search($step_key, array_keys($this->steps))) {
						$li_class = 'done';
						$show_link = true;
					}
				?>
				<li class="<?php echo $li_class; ?>">
				<?php
				if($show_link) {
					?>
					<a href="<?php echo esc_url($this->get_step_link($step_key)); ?>"><?php echo esc_html($step['name']); ?></a>
					<?php
				} else {
					echo esc_html($step['name']);
				}
				?>
				</li>
			<?php endforeach; ?>
		</ol>
		<?php
	}

	/**
	 * Output the content for the current step
	 */
	public function setup_wizard_content() {
		isset($this->steps[$this->step]) ? call_user_func($this->steps[$this->step]['view']) : false;
	}

	/**
	 * Introduction step
	 */
	public function envato_setup_introduction() {}

	public function filter_options($options) {
		return $options;
	}

	/**
	 *
	 * Handles save button from welcome page. This is to perform tasks when the setup wizard has already been run. E.g. reset defaults
	 *
	 * @since 1.2.5
	 */
	public function envato_setup_introduction_save() {
		check_admin_referer('envato-setup');
		return false;
	}


	private function _get_plugins() {

		$instance = call_user_func([get_class($GLOBALS['tgmpa']), 'get_instance']);

		$plugins  = [
			'all'      => [], // Meaning: all plugins which still have open actions.
			'install'  => [],
			'update'   => [],
			'activate' => [],
		];

		foreach($instance->plugins as $slug => $plugin) {

			if($instance->is_plugin_active($slug) && false === $instance->does_plugin_have_update($slug)) {
				// No need to display plugins if they are installed, up-to-date and active.
				continue;
			} else {
				$plugins['all'][ $slug ] = $plugin;
				if(!$instance->is_plugin_installed($slug)) {
					$plugins['install'][ $slug ] = $plugin;
				} else {
					if(false !== $instance->does_plugin_have_update($slug)) {
						$plugins['update'][ $slug ] = $plugin;
					}
					if($instance->can_plugin_activate($slug)) {
						$plugins['activate'][ $slug ] = $plugin;
					}
				}
			}

		}

		return $plugins;
	}

	/**
	 * Page setup
	 */
	public function envato_setup_default_plugins() {

		tgmpa_load_bulk_installer();
		// install plugins with TGM.
		if(!class_exists('TGM_Plugin_Activation') || ! isset($GLOBALS['tgmpa'])) {
			die(esc_html__('Failed to find TGM Plugin Activation plugin.', 'chap'));
		}
		$url = wp_nonce_url(add_query_arg([
			'plugins' => 'go',
		]), 'envato-setup');
		$plugins = $this->_get_plugins();

		// copied from TGM

		$method = ''; // Leave blank so WP_Filesystem can populate it as necessary.
		$fields = array_keys($_POST); // Extra fields to pass to WP_Filesystem.

		if(false === ($creds = request_filesystem_credentials(esc_url_raw($url), $method, false, false, $fields))) {
			return true; // Stop the normal page form from displaying, credential request form will be shown.
		}

		// Now we have some credentials, setup WP_Filesystem.
		if(!WP_Filesystem($creds)) {
			// Our credentials were no good, ask the user for them again.
			request_filesystem_credentials(esc_url_raw($url), $method, true, false, $fields);
			return true;
		}

		/* If we arrive here, we have the filesystem */

		?>
		<h1><?php esc_html_e('Default plugins', 'chap'); ?></h1>
		<form method="post">

			<?php
			$plugins = $this->_get_plugins();
			if(count($plugins['all'])) {
				?>
				<p><?php esc_html_e('This theme needs a few essential plugins. The following plugins will be installed or updated:', 'chap'); ?></p>
				<ul class="envato-wizard-plugins">
					<?php foreach($plugins['all'] as $slug => $plugin) { ?>
						<li data-slug="<?php echo esc_attr($slug); ?>"><?php echo esc_html($plugin['name']); ?>
							<span>
								<?php
								$keys = [];
								if(isset($plugins['install'][ $slug ])) {
									$keys[] = 'Installation';
								}
								if(isset($plugins['update'][ $slug ])) {
									$keys[] = 'Update';
								}
								if(isset($plugins['activate'][ $slug ])) {
									$keys[] = 'Activation';
								}
								echo implode(' and ', $keys) . ' required';
								?>
							</span>
							<div class="spinner"></div>
						</li>
					<?php } ?>
				</ul>
				<?php
			} else {
				echo '<p><strong>' . esc_html_e('Good news! All plugins are already installed and up to date. Please continue.', 'chap') . '</strong></p>';
			} ?>

			<p><?php esc_html_e('You can add and remove plugins later on from within WordPress.', 'chap'); ?></p>

			<p class="envato-setup-actions step">
				<a href="<?php echo esc_url($this->get_next_step_link()); ?>"
				   class="button-primary button button-large button-next"
				   data-callback="install_plugins"><?php esc_html_e('Continue', 'chap'); ?></a>
				<a href="<?php echo esc_url($this->get_next_step_link()); ?>"
				   class="button button-large button-next"><?php esc_html_e('Skip this step', 'chap'); ?></a>
				<?php wp_nonce_field('envato-setup'); ?>
			</p>
		</form>
		<?php
	}


	public function ajax_plugins() {

		if(!check_ajax_referer('envato_setup_nonce', 'wpnonce') || empty($_POST['slug'])) {
			wp_send_json_error([
				'error' => 1,
				'message' => esc_html__('No Slug Found', 'chap'),
			]);
		}

		$json = []; // send back some json we use to hit up TGM
		$plugins = $this->_get_plugins(); // TGM plugins

		foreach($plugins['activate'] as $slug => $plugin) {
			if($_POST['slug'] == $slug) {
				$json = [
					'url'           => admin_url($this->tgmpa_url),
					'plugin'        => [$slug],
					'tgmpa-page'    => $this->tgmpa_menu_slug,
					'plugin_status' => 'all',
					'_wpnonce'      => wp_create_nonce('bulk-plugins'),
					'action'        => 'tgmpa-bulk-activate',
					'action2'       => -1,
					'message'       => esc_html__('Activating Plugin', 'chap'),
				];
				break;
			}
		}

		foreach($plugins['update'] as $slug => $plugin) {
			if($_POST['slug'] == $slug) {
				$json = [
					'url'           => admin_url($this->tgmpa_url),
					'plugin'        => [$slug],
					'tgmpa-page'    => $this->tgmpa_menu_slug,
					'plugin_status' => 'all',
					'_wpnonce'      => wp_create_nonce('bulk-plugins'),
					'action'        => 'tgmpa-bulk-update',
					'action2'       => -1,
					'message'       => esc_html__('Updating Plugin', 'chap'),
				];
				break;
			}
		}

		foreach($plugins['install'] as $slug => $plugin) {
			if($_POST['slug'] == $slug) {
				$json = [
					'url'           => admin_url($this->tgmpa_url),
					'plugin'        => [$slug],
					'tgmpa-page'    => $this->tgmpa_menu_slug,
					'plugin_status' => 'all',
					'_wpnonce'      => wp_create_nonce('bulk-plugins'),
					'action'        => 'tgmpa-bulk-install',
					'action2'       => -1,
					'message'       => esc_html__('Installing Plugin', 'chap'),
				];
				break;
			}
		}

		if($json) {
			$json['hash'] = md5(serialize($json)); // used for checking if duplicates happen, move to next plugin
			wp_send_json($json);
		} else {
			/**
			 * TODO: manual check.
			 */
			wp_send_json([
				'done' => 1,
				'message' => esc_html__('Success', 'chap'),
			]);
		}

		exit;

	}

	private function _content_default_get() {
		$content = [];
		return $content;
	}

	/**
	 * Page setup
	 */
	public function envato_setup_default_content() {}


	public function ajax_content() {
		exit;
	}


	private function _imported_term_id($original_term_id, $new_term_id = false) {
		return false;
	}

	public function vc_post($post_id = false) {}


	public function elementor_post($post_id = false) {}


	private function _imported_post_id($original_id = false, $new_id = false) {
		return false;
	}

	private function _post_orphans($original_id = false, $missing_parent_id = false) {
		return false;
	}

	private function _cleanup_imported_ids() {
		// loop over all attachments and assign the correct post ids to those attachments.
	}

	private $delay_posts = [];

	private function _delay_post_process($post_type, $post_data) {}

	// return the difference in length between two strings
	public function cmpr_strlen($a, $b) {
		return strlen($b) - strlen($a);
	}

	private function _process_post_data($post_type, $post_data, $delayed = 0, $debug = false) {
		return true;
	}

	private function _parse_gallery_shortcode_content($content) {
		return $content;
	}

	private function _elementor_id_import(&$item, $key) {}

	private function _content_install_type() {
		return true;
	}

	private function _handle_post_orphans() {}

	private function _handle_delayed_posts($last_delay = false) {}

	private function _fetch_remote_file($url, $post) {
		return;
	}


	private function _content_install_widgets() {
		return true;
	}

	public function _content_install_settings() {
		return true;
	}

	public function _get_json($file) {
		return [];
	}

	private function _get_sql($file) {
		return false;
	}


	public $logs = [];

	public function log($message) {
		$this->logs[] = $message;
	}

	public $errors = [];

	public function error($message) {
		$this->logs[] = 'Error: ' . $message;
	}

	public function envato_setup_color_style() {}

	/**
	 * Save logo & design options
	 */
	public function envato_setup_color_style_save() {
		check_admin_referer('envato-setup');
		wp_redirect(esc_url_raw($this->get_next_step_link()));
		exit;
	}


	/**
	 * Logo & Design
	 */
	public function envato_setup_logo_design() {}

	/**
	 * Save logo & design options
	 */
	public function envato_setup_logo_design_save() {
		check_admin_referer('envato-setup');
		wp_redirect(esc_url_raw($this->get_next_step_link()));
		exit;
	}

	/**
	 * Payments Step
	 */
	public function envato_setup_updates() {
		?>
		<h1><?php esc_html_e('Theme updates', 'chap'); ?></h1>
		<?php if(function_exists('envato_market')) { ?>
			<form method="post">
				<?php
				$option = envato_market()->get_options();
				$authenticated = (isset($option['oauth']) && count($option['oauth']) > 0);

				$my_items = [];
				if($option && !empty($option['items'])) {
					foreach($option['items'] as $item) {
						if(!empty($item['oauth']) && !empty($item['token_data']['expires']) && $item['oauth'] == $this->envato_username && $item['token_data']['expires'] >= time()) {
							// token exists and is active
							$my_items[] = $item;
						}
					}
				}
				if(count($my_items)) {
					?>
					<p><?php esc_html_e('Theme updates have been enabled for the following items:', 'chap'); ?></p>
					<ul>
						<?php foreach($my_items as $item) { ?>
							<li><?php echo esc_html($item['name']); ?></li>
						<?php } ?>
					</ul>
					<p><?php esc_html_e('When an update becomes available it will show in the Dashboard with an option to install.', 'chap'); ?></p>
					<p><?php esc_html_e("Change settings from the 'Envato Market' menu item in the WordPress Dashboard.", 'chap'); ?></p>

					<p class="envato-setup-actions step">
						<a href="<?php echo esc_url($this->get_next_step_link()); ?>" class="button button-large button-next button-primary"><?php esc_html_e('Continue', 'chap'); ?></a>
					</p>
					<?php
				} else {

					if($authenticated) { ?>
					<p>
					<?php esc_html_e('Looks like you are already authenticated, how ever your account does not appear to contain any items.', 'chap'); ?><br /><br />
					<?php esc_html_e('Feel free to continue the setup wizard. You can try reauthenticating some other time with the Envato Market plugin, located in the dashboard.', 'chap'); ?>
					</p>
					<p class="envato-setup-actions step">
						<a href="<?php echo esc_url($this->get_next_step_link()); ?>" class="button-primary button button-large button-next"><?php esc_attr_e('Continue', 'chap'); ?></a>
						<input type="submit" class="button-secondary button button-large button-next" value="<?php esc_attr_e('Reauthenticate', 'chap'); ?>" name="save_step" />
						<a href="<?php echo esc_url($this->get_next_step_link()); ?>" class="button button-large button-next"><?php esc_html_e('Skip this step', 'chap'); ?></a>
						<?php wp_nonce_field('envato-setup'); ?>
					</p>
					<?php
					} else {
					?>
					<p><?php esc_html_e('Please login using your ThemeForest account to enable theme updates. Theme updates fix bugs or add new features - it is highly recommended to enable them.', 'chap'); ?></p>
					<p><?php esc_html_e('When an update becomes available it will show in the Dashboard with an option to install.', 'chap'); ?></p>
					<p>
						<em>
						<?php printf(
							esc_html__('On the next page you will be asked to Login with your ThemeForest account and grant permissions to enable Automatic Updates. If you have any questions please %1$scontact us%2$s.', 'chap'),
							'<a href="https://chap.website/contact" target="_blank">',
							'</a>'
						); ?>
						</em>
					</p>
					<p class="envato-setup-actions step">
						<input type="submit" class="button-primary button button-large button-next"
							   value="<?php esc_attr_e('Login with Envato', 'chap'); ?>" name="save_step"/>
						<a href="<?php echo esc_url($this->get_next_step_link()); ?>"
						   class="button button-large button-next"><?php esc_html_e('Skip this step', 'chap'); ?></a>
						<?php wp_nonce_field('envato-setup'); ?>
					</p>
				<?php
					}
				}
				?>
			</form>
		<?php } else {
			esc_html_e('Please ensure the Envato Market plugin has been installed correctly.', 'chap'); ?>
			<br /><br /><br />
			<a class="button button-primary button-large" href="<?php echo esc_url($this->get_step_link('default_plugins')); ?>">
				<?php esc_html_e('&lt; Return to required plugins installer', 'chap'); ?>
			</a>
			<a class="right button button-large" href="<?php echo esc_url($this->get_next_step_link()); ?>">
				<?php esc_html_e('Continue without setting up updates &gt;', 'chap'); ?>
			</a>
			<br /><br /><br />
		<?php } ?>
		<?php
	}

	/**
	 * Payments Step save
	 */
	public function envato_setup_updates_save() {
		check_admin_referer('envato-setup');

		// redirect to our custom login URL to get a copy of this token.
		$url = $this->get_oauth_login_url($this->get_step_link('updates'));

		wp_redirect(esc_url_raw($url));
		exit;
	}


	public function envato_setup_customize() {}

	public function envato_setup_help_support() {}

	/**
	 * Final step
	 */
	public function envato_setup_ready() {}

	public function envato_market_admin_init() {

		if(!function_exists('envato_market')) {
			return;
		}

		global $wp_settings_sections;
		if(!isset($wp_settings_sections[envato_market()->get_slug()])) {
			// means we're running the admin_init hook before envato market gets to setup settings area.
			// good - this means our oauth prompt will appear first in the list of settings blocks
			register_setting(envato_market()->get_slug(), envato_market()->get_option_name());
		}

		// pull our custom options across to envato.
		$option         = get_option('envato_setup_wizard', []);
		$envato_options = envato_market()->get_options();
		$envato_options = $this->_array_merge_recursive_distinct($envato_options, $option);

		if(!empty($envato_options['items'])) {
			foreach($envato_options['items'] as $key => $item) {
				if(!empty($item['id']) && is_string($item['id'])) {
					$envato_options['items'][$key]['id'] = (int)$item['id'];
				}
			}
		}

		update_option(envato_market()->get_option_name(), $envato_options);

		//add_thickbox();

		if(!empty($_POST['oauth_session']) && !empty($_POST['bounce_nonce']) && wp_verify_nonce($_POST['bounce_nonce'], 'envato_oauth_bounce_' . $this->envato_username)) {
			// request the token from our bounce url.
			$my_theme = wp_get_theme();
			$oauth_nonce = get_option('envato_oauth_' . $this->envato_username);
			if(!$oauth_nonce) {
				// this is our 'private key' that is used to request a token from our api bounce server.
				// only hosts with this key are allowed to request a token and a refresh token
				// the first time this key is used, it is set and locked on the server.
				$oauth_nonce = wp_create_nonce('envato_oauth_nonce_' . $this->envato_username);
				update_option('envato_oauth_' . $this->envato_username, $oauth_nonce);
			}
			$response = wp_remote_post( $this->oauth_script, [
				'method'      => 'POST',
				'timeout'     => 15,
				'redirection' => 1,
				'httpversion' => '1.0',
				'blocking'    => true,
				'headers'     => [],
				'body'        => [
					'oauth_session' => $_POST['oauth_session'],
					'oauth_nonce'   => $oauth_nonce,
					'get_token'     => 'yes',
					'url'           => home_url(),
					'theme'         => $my_theme->get('Name'),
					'version'       => $my_theme->get('Version'),
				],
				'cookies'     => [],
			]
			);
			if(is_wp_error($response)) {
				$error_message = $response->get_error_message();
				$class = 'error';
				echo "<div class=\"$class\"><p>" . sprintf(esc_html__('Something went wrong while trying to retrieve oauth token: %s', 'chap'), $error_message) . '</p></div>';
			} else {
				$token  = @json_decode(wp_remote_retrieve_body($response), true);
				$result = false;
				if(is_array($token) && !empty($token['access_token'])) {
					$token['oauth_session'] = $_POST['oauth_session'];
					$result = $this->_manage_oauth_token($token);
				}
				if($result !== true) {
					echo 'Failed to get oAuth token. Please go back and try again';
					exit;
				}
			}
		}

		add_settings_section(
			envato_market()->get_option_name() . '_' . $this->envato_username . '_oauth_login',
			sprintf(esc_html__('Login for Chap updates', 'chap'), $this->envato_username),
			[$this, 'render_oauth_login_description_callback'],
			envato_market()->get_slug()
		);
		// Items setting.
		add_settings_field(
			$this->envato_username . 'oauth_keys',
			esc_html__('oAuth Login', 'chap'),
			[$this, 'render_oauth_login_fields_callback'],
			envato_market()->get_slug(),
			envato_market()->get_option_name() . '_' . $this->envato_username . '_oauth_login'
		);

	}

	private static $_current_manage_token = false;

	private function _manage_oauth_token($token) {
		if(is_array($token) && !empty($token['access_token'])) {
			if(self::$_current_manage_token == $token['access_token']) {
				return false; // stop loops when refresh auth fails.
			}
			self::$_current_manage_token = $token['access_token'];
			// yes! we have an access token. store this in our options so we can get a list of items using it.
			$option = get_option('envato_setup_wizard', []);
			if(!is_array($option)) {
				$option = [];
			}
			if(empty($option['items'])) {
				$option['items'] = [];
			}
			// check if token is expired.
			if(empty($token['expires'])) {
				$token['expires'] = time() + 3600;
			}
			if($token['expires'] < time() + 120 && !empty($token['oauth_session'])) {
				// time to renew this token!
				$my_theme    = wp_get_theme();
				$oauth_nonce = get_option('envato_oauth_' . $this->envato_username);
				$response    = wp_remote_post( $this->oauth_script, [
					'method'      => 'POST',
					'timeout'     => 10,
					'redirection' => 1,
					'httpversion' => '1.0',
					'blocking'    => true,
					'headers'     => [],
					'body'        => [
						'oauth_session' => $token['oauth_session'],
						'oauth_nonce'   => $oauth_nonce,
						'refresh_token' => 'yes',
						'url'           => home_url(),
						'theme'         => $my_theme->get('Name'),
						'version'       => $my_theme->get('Version'),
					],
					'cookies'     => [],
				]
				);
				if(is_wp_error($response)) {
					$error_message = $response->get_error_message();
					printf(
						esc_html__('Something went wrong while trying to retrieve oauth token: %s', 'chap'),
						$error_message
					);
					$this->_clear_oauth();
				} else {
					$new_token = @json_decode(wp_remote_retrieve_body($response), true);
					$result    = false;
					if(is_array($new_token) && !empty($new_token['new_token'])) {
						$token['access_token'] = $new_token['new_token'];
						$token['expires']      = time() + 3600;
					} else {
						$this->_clear_oauth();
					}
				}
			}
			// use this token to get a list of purchased items
			// add this to our items array.
			$response = envato_market()->api()->request( 'https://api.envato.com/v3/market/buyer/purchases', [
				'headers' => [
					'Authorization' => 'Bearer ' . $token['access_token'],
				],
			] );
			self::$_current_manage_token = false;
			if(is_array($response) && is_array($response['purchases'])) {
				// up to here, add to items array
				foreach($response['purchases'] as $purchase) {
					// check if this item already exists in the items array.
					$exists = false;
					foreach($option['items'] as $id => $item) {
						if(!empty($item['id']) && $item['id'] == $purchase['item']['id']) {
							$exists = true;
							// update token.
							$option['items'][ $id ]['token']      = $token['access_token'];
							$option['items'][ $id ]['token_data'] = $token;
							$option['items'][ $id ]['oauth']      = $this->envato_username;
							if(!empty($purchase['code'])) {
								$option['items'][ $id ]['purchase_code'] = $purchase['code'];
							}
						}
					}
					if(!$exists) {
						$option['items'][] = [
							'id'            => '' . $purchase['item']['id'],
							// item id needs to be a string for market download to work correctly.
							'name'          => $purchase['item']['name'],
							'token'         => $token['access_token'],
							'token_data'    => $token,
							'oauth'         => $this->envato_username,
							'type'          => !empty($purchase['item']['wordpress_theme_metadata']) ? 'theme' : 'plugin',
							'purchase_code' => !empty($purchase['code']) ? $purchase['code'] : '',
						];
					}
				}
			} else {
				return false;
			}
			if(!isset($option['oauth'])) {
				$option['oauth'] = [];
			}
			// store our 1 hour long token here. we can refresh this token when it comes time to use it again (i.e. during an update)
			$option['oauth'][ $this->envato_username ] = $token;
			update_option('envato_setup_wizard', $option);

			$envato_options = envato_market()->get_options();
			$envato_options = $this->_array_merge_recursive_distinct($envato_options, $option);
			update_option(envato_market()->get_option_name(), $envato_options);
			envato_market()->items()->set_themes(true);
			envato_market()->items()->set_plugins(true);

			return true;
		} else {
			return false;
		}
	}

	/**
	 * @param $array1
	 * @param $array2
	 *
	 * @return mixed
	 *
	 *
	 * @since    1.1.4
	 */
	private function _array_merge_recursive_distinct($array1, $array2) {
		$merged = $array1;
		foreach($array2 as $key => &$value) {
			if(is_array($value) && isset($merged[$key]) && is_array($merged[$key])) {
				$merged[$key] = $this->_array_merge_recursive_distinct($merged[$key], $value);
			} else {
				$merged[$key] = $value;
			}
		}

		return $merged;
	}

	/**
	 * @param $args
	 * @param $url
	 *
	 * @return mixed
	 *
	 * Filter the WordPress HTTP call args.
	 * We do this to find any queries that are using an expired token from an oAuth bounce login.
	 * Since these oAuth tokens only last 1 hour we have to hit up our server again for a refresh of that token before using it on the Envato API.
	 * Hacky, but only way to do it.
	 */
	public function envato_market_http_request_args($args, $url) {
		if(strpos($url, 'api.envato.com') && function_exists('envato_market')) {
			// we have an API request.
			// check if it's using an expired token.
			if(!empty($args['headers']['Authorization'])) {
				$token = str_replace('Bearer ', '', $args['headers']['Authorization']);
				if($token) {
					// check our options for a list of active oauth tokens and see if one matches, for this envato username.
					$option = envato_market()->get_options();
					if($option && !empty($option['oauth'][ $this->envato_username ]) && $option['oauth'][ $this->envato_username ]['access_token'] == $token && $option['oauth'][ $this->envato_username ]['expires'] < time() + 120) {
						// we've found an expired token for this oauth user!
						// time to hit up our bounce server for a refresh of this token and update associated data.
						$this->_manage_oauth_token($option['oauth'][ $this->envato_username ]);
						$updated_option = envato_market()->get_options();
						if($updated_option && !empty($updated_option['oauth'][ $this->envato_username ]['access_token'])) {
							// hopefully this means we have an updated access token to deal with.
							$args['headers']['Authorization'] = 'Bearer ' . $updated_option['oauth'][ $this->envato_username ]['access_token'];
						}
					}
				}
			}
		}

		return $args;
	}

	public function render_oauth_login_description_callback() {
		printf(
			esc_html__('If you have purchased items from %s on ThemeForest or CodeCanyon please login here for quick and easy updates.', 'chap'),
			esc_html($this->envato_username)
		);
	}

	public function render_oauth_login_fields_callback() {
		$option = envato_market()->get_options();
		?>
		<div class="oauth-login" data-username="<?php echo esc_attr($this->envato_username); ?>">
			<a href="<?php echo esc_url($this->get_oauth_login_url(admin_url('admin.php?page=' . envato_market()->get_slug() . '#settings'))); ?>"
			   class="oauth-login-button button button-primary"><?php esc_html_e('Login with Envato to activate updates', 'chap'); ?></a>
		</div>
		<?php
	}

	/// a better filter would be on the post-option get filter for the items array.
	// we can update the token there.

	public function get_oauth_login_url($return) {
		return $this->oauth_script . '?bounce_nonce=' . wp_create_nonce('envato_oauth_bounce_' . $this->envato_username) . '&wp_return=' . urlencode($return);
	}

	public function _clear_oauth() {
		$envato_options = envato_market()->get_options();
		unset($envato_options['oauth']);
		update_option(envato_market()->get_option_name(), $envato_options);
	}

	/**
	 * Helper function
	 * Take a path and return it clean
	 *
	 * @param string $path
	 *
	 * @since    1.1.2
	 */
	public static function cleanFilePath($path) {
		$path = str_replace('', '', str_replace(['\\', '\\\\', '//'], '/', $path));
		if($path[strlen($path) - 1] === '/') {
			$path = rtrim($path, '/');
		}

		return $path;
	}

	public function is_submenu_page() {
		return ($this->parent_slug == '') ? false : true;
	}

	public function ajax_notice_handler() {
		check_ajax_referer('chap-ajax-nonce', 'security');
		// Store it in the options table
		update_option('chap_update_notice', time());
	}

	public function admin_theme_auth_notice() {

		if(function_exists('envato_market')) {
			$option = envato_market()->get_options();

			$envato_items = get_option('envato_setup_wizard', []);

			if(!$option || empty($option['oauth']) || empty($option['oauth'][$this->envato_username]) || empty($envato_items) || empty($envato_items['items'])) {

				// we show an admin notice if it hasn't been dismissed
				$dismissed_time = get_option('chap_update_notice', false);

				if(!$dismissed_time || $dismissed_time < strtotime('-14 days')) {
					// Added the class "notice-my-class" so jQuery pick it up and pass via AJAX,
					// and added "data-notice" attribute in order to track multiple / different notices
					// multiple dismissible notice states ?>
					<div class="notice notice-warning notice-chap-themeupdates is-dismissible">
						<p>
						<?php
						esc_html_e('Please refresh the activation for ThemeForest updates to ensure you have the latest version of Chap theme.', 'chap');
							?>
							</p>
						<p>
						<?php printf(
							esc_html__('%1$sActivate updates%2$s', 'chap'),
							'<a class="button button-primary" href="' . esc_url($this->get_oauth_login_url(admin_url('admin.php?page=' . envato_market()->get_slug() . ''))) . '">',
							'</a>'
						); ?>
						</p>
					</div>
					<script>
						jQuery(function($) {
							$(document).on('click', '.notice-chap-themeupdates .button-primary, .notice-chap-themeupdates .notice-dismiss', function(){
								$.ajax(ajaxurl, {
									type: 'POST',
									data: {
										action: 'chap_update_notice_handler',
										security: '<?php echo wp_create_nonce('chap-ajax-nonce'); ?>'
									}
								});
							});
						});
					</script>
				<?php }

			}
		}

	}

}

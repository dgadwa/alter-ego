<?php

if(!defined('ABSPATH')) {
	exit;
}

require_once __DIR__ . DIRECTORY_SEPARATOR . 'envato-setup.php';

class Chap_Envato_Theme_Setup_Wizard extends Envato_Theme_Setup_Wizard {

	/**
	 * Holds the current instance of the theme manager
	 *
	 * @since 1.1.3
	 * @var Envato_Theme_Setup_Wizard
	 */
	private static $instance = null;

	/**
	 * Chap Shortcodes composites.
	 * @var array
	 */
	public $csc_composites = [];

	/**
	 * Header background presets.
	 * @var array
	 */
	public static $header_backgrounds = [];

	/**
	 * Menu types.
	 * @var array
	 */
	public static $menu_types = [];

	/**
	 * @since 1.1.3
	 *
	 * @return Envato_Theme_Setup_Wizard
	 */
	public static function get_instance() {
		if(!self::$instance) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	/**
	 * Get the default style. Can be overriden by theme init scripts.
	 *
	 * @see Envato_Theme_Setup_Wizard::instance()
	 *
	 * @since 1.1.9
	 * @access public
	 */
	public function get_header_logo_width() {
		return '253px';
	}

	/**
	 * Get the default style. Can be overriden by theme init scripts.
	 *
	 * @see Envato_Theme_Setup_Wizard::instance()
	 *
	 * @since 1.1.9
	 * @access public
	 */
	public function get_logo_image() {
		return Chap\Assets\asset_path('images/chap-logo.png');
	}

	/**
	 * Init.
	 */
	public function init_actions() {

		global $plugin_chap_shortcodes;
		if(isset($plugin_chap_shortcodes)) {
			$this->csc_composites = $plugin_chap_shortcodes->get_chap_shortcodes_composites();
		}

		self::$header_backgrounds = [
			'color' => [
				'dark' => [
					'color' => '#1b1c1d',
					'dark' => true,
				],
				'light' => [
					'color' => '#f4f4f4',
					'dark' => false,
				],
				'blue' => [
					'color' => '#1e73be',
					'dark' => true,
				],
			],
			'image' => [
				'drops' => [
					'src' => 'blue-abstract-glass-balls.jpg',
					'dark' => true,
				],
				'business' => [
					'src' => 'black-and-white-city-man-people.jpg',
					'dark' => true,
					'opacity' => 0.3,
				],
				'grass' => [
					'src' => 'landscape-nature-sunset-trees.jpg',
					'dark' => true,
				],
				'defocused' => [
					'src' => 'outdoor-defocused-aerial-urban.jpg',
					'dark' => true,
					'opacity' => 0.3,
				],
				'bear' => [
					'src' => 'nature-animal-fur-dangerous.jpg',
					'dark' => true,
				],
				'road' => [
					'src' => 'road-sky-clouds-cloudy.jpg',
					'dark' => true,
					'opacity' => 0.6,
				],
				'coffee' => [
					'src' => 'coffee-apple-iphone-desk.jpg',
					'dark' => true,
					'opacity' => 0.5,
				],
				'leaf' => [
					'src' => 'green-leaf-plant.jpg',
					'dark' => true,
					'opacity' => 0.3,
				],
				'orchestra' => [
					'src' => 'orchestra.jpg',
					'video' => 'orchestra.webm',
					'dark' => true,
					'opacity' => 1,
				],
			],
		];

		self::$menu_types = [
			'dark' => [
				'1_0_1' => esc_html__('Basic inverted pointing', 'chap'),
				'1_0_0' => esc_html__('Basic inverted', 'chap'),
				'0_1_0' => esc_html__('Container', 'chap'),
				'0_1_1' => esc_html__('Continer pointing', 'chap'),
			],
			'light' => [
				'0_0_1' => esc_html__('Basic pointing', 'chap'),
				'0_0_0' => esc_html__('Basic', 'chap'),
				'1_1_0' => esc_html__('Container inverted', 'chap'),
				'1_1_1' => esc_html__('Continer inverted pointing', 'chap'),
			],
		];

		parent::init_actions();
	}

	/**
	 * Init steps.
	 */
	public function init_wizard_steps() {

		$this->steps = [
			'introduction' => [
				'name'    => esc_html__('Introduction', 'chap'),
				'view'    => [$this, 'envato_setup_introduction'],
				'handler' => [$this, 'envato_setup_introduction_save'],
			],
		];

		if(class_exists('TGM_Plugin_Activation') && isset($GLOBALS['tgmpa'])) {
			$this->steps['default_plugins'] = [
				'name'    => esc_html__('Plugins', 'chap'),
				'view'    => [$this, 'envato_setup_default_plugins'],
				'handler' => '',
			];
		}

		$this->steps['updates'] = [
			'name'    => esc_html__('Updates', 'chap'),
			'view'    => [$this, 'envato_setup_updates'],
			'handler' => [$this, 'envato_setup_updates_save'],
		];

		$this->steps['header'] = [
			'name'    => esc_html__('Header', 'chap'),
			'view'    => [$this, 'envato_setup_header'],
			'handler' => [$this, 'envato_setup_header_save'],
		];

		$this->steps['pages'] = [
			'name'    => esc_html__('Pages', 'chap'),
			'view'    => [$this, 'envato_setup_pages'],
			'handler' => [$this, 'envato_setup_pages_save'],
		];

		$this->steps['slides'] = [
			'name'    => esc_html__('Slides', 'chap'),
			'view'    => [$this, 'envato_setup_slides'],
			'handler' => [$this, 'envato_setup_slides_save'],
		];

		$this->steps['social'] = [
			'name'    => esc_html__('Social', 'chap'),
			'view'    => [$this, 'envato_setup_social'],
			'handler' => [$this, 'envato_setup_social_save'],
		];

		$this->steps['help_support'] = [
			'name'    => esc_html__('Support', 'chap'),
			'view'    => [$this, 'envato_setup_help_support'],
			'handler' => '',
		];

		$this->steps['next_steps'] = [
			'name'    => esc_html__('Ready!', 'chap'),
			'view'    => [$this, 'envato_setup_ready'],
			'handler' => '',
		];

	}

	/**
	 * Step filter.
	 */
	public function theme_setup_wizard_steps($steps) {
		return $steps;
	}

	/**
	 * Introduction step.
	 */
	public function envato_setup_introduction() {

		$referer = wp_get_referer() && !strpos(wp_get_referer(), 'update.php') ? wp_get_referer() : admin_url('');

		if(get_option('envato_setup_complete', false)) {
			?>
			<h1><?php esc_html_e('Welcome to the setup wizard for Chap.', 'chap'); ?></h1>
			<p><?php esc_html_e('It looks like you have already run the setup wizard.', 'chap'); ?></p>
			<ul>
				<li>
					<a href="<?php echo esc_url($this->get_next_step_link()); ?>" class="button button-primary button-hero button-next"><?php esc_html_e('Run Setup Wizard again', 'chap'); ?></a>
				</li>
			</ul>
			<p class="envato-setup-actions step">
				<a href="<?php echo esc_url($referer); ?>" class="button button-large"><?php esc_html_e('Cancel', 'chap'); ?></a>
			</p>
			<?php
		} else {
			?>
			<h1><?php esc_html_e('Welcome to the setup wizard for Chap.', 'chap'); ?></h1>
			<p><?php esc_html_e('Thank you for choosing Chap theme from ThemeForest. This quick setup wizard will help you set up your new website. The wizard will install the required WordPress plugins, set up updates, allow you to choose your menu style, add some default content, and tell you a little about Chap and it\'s support options. The setup process should only take a couple of minutes.', 'chap'); ?></p>
			<p><?php esc_html_e('No time right now? This wizard is not mandatory. If you don\'t want to go through the wizard, you can skip and return to the WordPress dashboard. You can come back any time, by choosing Chap Theme -> Setup Wizard from the Dashboard menu.', 'chap'); ?></p>
			<p class="envato-setup-actions step">
				<a href="<?php echo esc_url($this->get_next_step_link()); ?>" class="button-primary button button-large button-next"><?php esc_html_e("Let's go!", 'chap'); ?></a>
				<a href="<?php echo esc_url($referer); ?>" class="button button-large"><?php esc_html_e('Not right now', 'chap'); ?></a>
			</p>
			<?php
		}

	}

	/**
	 * Header step.
	 */
	public function envato_setup_header() {
		self::confirm_dependancies(); ?>
		<p>
			<?php printf(
				esc_html__('The following steps can be used to set up your %1$sheader%2$s, %1$smenu%2$s, %1$2sstarter pages%2$s and %1$sslides%2$s.', 'chap'),
				'<strong>',
				'</strong>'
			); ?>
			<br />
			<?php printf(
				esc_html__('If you are planning to import a particular %1$sdemo site%2$s you can skip these steps.', 'chap'),
				'<strong>',
				'</strong>'
			); ?>
			<br />
			<?php
				$button_url = add_query_arg([
					'step' => 'social',
					'demo' => 1,
				], remove_query_arg('translation_updated'));
			?>
			<a href="<?php echo esc_url($button_url); ?>" class="button button-primary button-hero button-skip-forward button-next"><?php esc_html_e('I will be importing a full demo, skip forward...', 'chap'); ?></a>
		</p>
		<br />
		<form method="post">
		<h1><?php esc_html_e('Choose a header background', 'chap'); ?></h1>
		<div class="composite-multicheck">
			<?php foreach(self::$header_backgrounds as $type => $backgrounds): ?>
				<?php foreach($backgrounds as $id => $bg): ?>
					<div class="composite-check">
						<?php
						$title = isset($bg['video']) ? ucfirst($id) . ' (' . esc_html__('video', 'chap') . ')' : ucfirst($id);
						$checked = $id === 'dark' ? ' checked="checked"' : '';
						$bg_id = 'header-bg-' . $type . '-' . $id;
						if($type === 'image') {
							$src = Chap\Assets\asset_path('images/headers/' . $bg['src']);
							$bg_style = 'background-image:url(' . $src . ');background-size:cover;';
						} else {
							$bg_style = 'background-color:' . $bg['color'] . ';';
						}
						?>
						<label for="<?php echo esc_attr($bg_id); ?>" style="<?php echo esc_attr($bg_style); ?>">
							<div class="title"><?php echo esc_html($title); ?></div>
							<input type="radio" id="<?php echo esc_attr($bg_id); ?>" name="chap-header-bg-choice" data-dark="<?php echo esc_attr($bg['dark'] ? 'true' : 'false'); ?>" value="<?php echo esc_attr($type . '-' . $id); ?>"<?php echo wp_kses_post($checked); ?> />
						</label>
					</div>
				<?php endforeach; ?>
			<?php endforeach; ?>
		</div>

		<h1><?php esc_html_e('Choose menu variation', 'chap'); ?></h1>
			<p class="very compact"><?php esc_html_e('There are four basic variations for the main menu and the colors can be inverted based on your header background.', 'chap'); ?></p>
			<p class="very compact"><?php esc_html_e('Menus also have different themes: Default, Chubby, Material and GitHub, as well as 8 different size options, ranging from mini to huge. You will be able to play around with these settings later.', 'chap'); ?></p>
			<p class="compact"><?php esc_html_e('For now, please select the variation you would like to use:', 'chap'); ?></p>
			<?php
			foreach(self::$menu_types as $id => $menu_type) {
				$hidden = $id === 'dark' ? '' : ' hidden';
				echo '<div class="menus ' . $id . $hidden . '">';
				foreach($menu_type as $image => $description) {
					$image_url = Chap\Assets\asset_path('images/menus/' . $image . '.png');
					$checked = $image === '1_0_1' ? ' checked="checked"' : '';
					?>
					<div class="menu-check composite-check">
						<label for="<?php echo esc_attr($image); ?>">
							<div class="title"><?php echo esc_html($description); ?></div>
							<img src="<?php echo esc_attr($image_url); ?>" />
							<input type="radio" id="<?php echo esc_attr($image); ?>" name="chap-menu-choice" value="<?php echo esc_attr($image); ?>"<?php echo wp_kses_post($checked); ?> />
						</label>
					</div>
					<?php
				}
				echo '</div>';
			}
			?>

			<br />

			<p class="envato-setup-actions step">
				<input type="submit" class="button-primary button button-large button-next" value="<?php esc_attr_e('Continue', 'chap'); ?>" name="save_step"/>
				<a href="<?php echo esc_url($this->get_next_step_link()); ?>" class="button button-large button-next"><?php esc_html_e('Skip this step', 'chap'); ?></a>
				<?php wp_nonce_field('envato-setup'); ?>
			</p>
		</form>
		<?php
	}

	/**
	 * Save header step.
	 */
	public function envato_setup_header_save() {
		check_admin_referer('envato-setup');

		do_action('ctf_init_no_options_' . CHAP_TF);
		$titan = \ChapTitanFramework::getInstance(CHAP_TF);

		$header = isset($_POST['chap-header-bg-choice']) ? $_POST['chap-header-bg-choice'] : false;
		if($header) {

			list($type, $id) = explode('-', $header);

			if($type === 'color') {

				$color = self::$header_backgrounds[$type][$id]['color'];
				$titan->setInternalAdminPageOption('header_background_color', $color);
				$titan->setInternalAdminPageOption('header_background_image', false);

			} elseif($type === 'image') {

				/**
				 * Image / color.
				 */
				$src = self::$header_backgrounds[$type][$id]['src'];
				$url = Chap\Assets\asset_path('images/headers/' . $src);
				$titan->setInternalAdminPageOption('header_background_color', '#1b1c1d');
				$titan->setInternalAdminPageOption('header_background_image', $url);

				/**
				 * Opacity.
				 */
				$opacity = isset(self::$header_backgrounds[$type][$id]['opacity']) ? self::$header_backgrounds[$type][$id]['opacity'] : 0.5;
				$titan->setInternalAdminPageOption('header_background_opacity', $opacity);

				/**
				 * Video.
				 */
				$video = isset(self::$header_backgrounds[$type][$id]['video']) ? self::$header_backgrounds[$type][$id]['video'] : false;
				if($video) {
					$url = Chap\Assets\asset_path('images/headers/' . $video);
					$titan->setInternalAdminPageOption('header_background_video', $url);
				}

			}

		}

		$menu = isset($_POST['chap-menu-choice']) ? $_POST['chap-menu-choice'] : false;
		if($menu) {

			if(in_array($menu, array_keys(self::$menu_types['light']))) {
				/**
				 * Light colors.
				 */
				$titan->setInternalAdminPageOption('header_inverted', false);
				$titan->setInternalAdminPageOption('brand_inverted', false);
				$titan->setInternalAdminPageOption('slider_navigation_color', 'swiper-button-black');
				$titan->setInternalAdminPageOption('slider_pagination_color', 'swiper-pagination-black');
			} else {
				/**
				 * Dark colors.
				 */
				$titan->setInternalAdminPageOption('header_inverted', true);
				$titan->setInternalAdminPageOption('brand_inverted', true);
				$titan->setInternalAdminPageOption('slider_navigation_color', 'swiper-button-white');
				$titan->setInternalAdminPageOption('slider_pagination_color', 'swiper-pagination-white');
			}

			$menu_opts = explode('_', $menu);

			/**
			 * Inverted.
			 */
			if($menu_opts[0] == 1) {
				$titan->setInternalAdminPageOption('main_menu_inverted', true);
			} else {
				$titan->setInternalAdminPageOption('main_menu_inverted', false);
			}

			/**
			 * Container.
			 */
			if($menu_opts[1] == 1) {
				$titan->setInternalAdminPageOption('main_menu_container', true);
			} else {
				$titan->setInternalAdminPageOption('main_menu_container', false);
			}

			/**
			 * Pointing.
			 */
			if($menu_opts[2] == 1) {
				$titan->setInternalAdminPageOption('main_menu_pointing', true);
			} else {
				$titan->setInternalAdminPageOption('main_menu_pointing', false);
			}

		}

		if($header || $menu) {
			/**
			 * Save options.
			 */
			$titan->saveInternalAdminPageOptions();
			do_action('ctf_admin_options_saved_' . CHAP_TF);
			$titan->cssInstance->generateSaveCSS();
		}

		wp_redirect(esc_url_raw($this->get_next_step_link()));
		exit;
	}

	/**
	 * Pages step.
	 * Allow to create some default pages.
	 */
	public function envato_setup_pages() {
		self::confirm_dependancies(); ?>
		<form method="post">

		<h1><?php esc_html_e('Front page', 'chap'); ?></h1>
		<p>
			<?php echo esc_html__('Choose what you would like to display on the front page.', 'chap'); ?><br />
			<?php echo esc_html__('Composites are combinations of shortcodes producing complex UI elements.', 'chap'); ?>
		</p>
		<div class="composite-multicheck">
		<?php
			$pages = [
				'composites' => esc_html__('Composites', 'chap'),
				'posts' => esc_html__('Latest posts', 'chap'),
				'text' => esc_html__('Plain text', 'chap'),
			];
		?>
		<?php foreach($pages as $page => $title): ?>
			<?php $checked = $page === 'composites' ? ' checked="checked"' : ''; ?>
			<div class="composite-check">
				<label for="<?php echo esc_attr($page); ?>" style="background-image:url(<?php echo esc_url(Chap\Assets\asset_path('images/options/front-page-' . $page . '.png')); ?>);">
					<div class="title"><?php echo esc_html($title); ?></div>
					<input type="radio" id="<?php echo esc_attr($page); ?>" name="chap-front-page" value="<?php echo esc_attr($page); ?>"<?php echo wp_kses_post($checked); ?> />
				</label>
			</div>
		<?php endforeach; ?>
		</div>

		<h1><?php esc_html_e('Pages', 'chap'); ?></h1>
			<p><?php echo esc_html__('Choose some additional sample pages you wish to add for your site.', 'chap'); ?></p>
			<div class="composite-multicheck">
			<?php
			foreach($this->csc_composites as $composite) {
				if(strpos($composite->id, 'page-') !== false) {
					$id = $composite->id;
					$img =  $composite->get_image();
					?>
						<div class="composite-check">
							<label for="<?php echo esc_attr($id); ?>" style="background-image:url(<?php echo esc_url($img); ?>);">
								<div class="title"><?php echo esc_html($composite->name); ?></div>
								<input type="checkbox" id="<?php echo esc_attr($id); ?>" name="chap-setup-page[<?php echo esc_attr($id); ?>]" value="1" />
							</label>
						</div>
					<?php

				}
			}
			?>
			</div>

			<br /><br />

			<p class="envato-setup-actions step">
				<input type="submit" class="button-primary button button-large button-next" value="<?php esc_attr_e('Continue', 'chap'); ?>" name="save_step"/>
				<a href="<?php echo esc_url($this->get_next_step_link()); ?>" class="button button-large button-next"><?php esc_html_e('Skip this step', 'chap'); ?></a>
				<?php wp_nonce_field('envato-setup'); ?>
			</p>

		</form>
		<?php
	}

	/**
	 * Save pages step.
	 */
	public function envato_setup_pages_save() {
		check_admin_referer('envato-setup');

		if(isset($_POST['chap-front-page'])) {

			$front_page = $_POST['chap-front-page'];

			switch($front_page) {

				case 'composites':
					/**
					 * Find if the front page has already been generated previously.
					 */
					$query = new \WP_Query([
						'post_type' => 'page',
						'posts_per_page' => 1,
						'meta_query' => [
							[
								'key' => '_generator',
								'compare' => '=',
								'value' => 'chap',
							],
							[
								'key' => '_chap_composite_id',
								'compare' => '=',
								'value' => 'front-page-composites',
							],
						],
					]);

					if($query->post_count > 0) {
						if($query->have_posts()) {
							$query->the_post();
							$page_id = get_the_ID();
							wp_reset_postdata();
						}
					} else {
						$page_content = '';
						$front_page_composites = [
							'action-image-segment',
							'feature-columns',
							'stacked-paragraphs',
							'call-to-action',
						];

						foreach($front_page_composites as $composite_to_use) {
							foreach($this->csc_composites as $composite) {
								if($composite->id === $composite_to_use) {
									$page_content .= $composite->content . "\n\n";
								}
							}
						}

						$page_id = wp_insert_post([
							'post_type' => 'page',
							'post_status' => 'publish',
							'post_title' => esc_html__('Home', 'chap'),
							'post_content' => $page_content,
							'meta_input' => [
								'_generator' => 'chap',
								'_chap_composite_id' => 'front-page-composite',
								'_wp_page_template' => 'template-full-width.php',
							],
						]);
					}

					update_option('show_on_front', 'page');
					update_option('page_on_front', $page_id);
				break;

				case 'posts':
					update_option('show_on_front', 'posts');
				break;

				case 'text':
					/**
					 * Find if the front page has already been generated previously.
					 */
					$query = new \WP_Query([
						'post_type' => 'page',
						'posts_per_page' => 1,
						'meta_query' => [
							[
								'key' => '_generator',
								'compare' => '=',
								'value' => 'chap',
							],
							[
								'key' => '_chap_composite_id',
								'compare' => '=',
								'value' => 'front-page-text',
							],
						],
					]);

					if($query->post_count > 0) {
						if($query->have_posts()) {
							$query->the_post();
							$page_id = get_the_ID();
							wp_reset_postdata();
						}
					} else {

						foreach($this->csc_composites as $composite) {
							if($composite->id === 'sample-page') {
								$page_id = wp_insert_post([
									'post_type' => 'page',
									'post_status' => 'publish',
									'post_title' => esc_html__('Home', 'chap'),
									'post_content' => $composite->content,
									'meta_input' => [
										'_generator' => 'chap',
										'_chap_composite_id' => 'front-page-text',
										'_wp_page_template' => 'template-sidebar-none.php',
									],
								]);
							}
						}

					}

					update_option('show_on_front', 'page');
					update_option('page_on_front', $page_id);
				break;

			}

		}

		$created_pages = [];

		if(isset($_POST['chap-setup-page'])) {

			$pages = $_POST['chap-setup-page'];

			foreach($pages as $page => $val) {

				$exploded = explode('-', $page);
				$slug = $exploded[1];
				$title = $slug === 'faq' ? strtoupper($slug) : ucfirst($slug);

				/**
				 * Find if the page has already been generated previously.
				 */
				$query = new \WP_Query([
					'post_type' => 'page',
					'posts_per_page' => 1,
					'meta_query' => [
						[
							'key' => '_generator',
							'compare' => '=',
							'value' => 'chap',
						],
						[
							'key' => '_chap_composite_id',
							'compare' => '=',
							'value' => $page,
						],
					],
				]);

				/**
				 * Use the previously generated page's id.
				 */
				if($query->post_count > 0) {

					if($query->have_posts()) {
						$query->the_post();
						$created_pages[$title] = get_the_ID();
						wp_reset_postdata();
					}

				} else {
					/**
					 * Create the page.
					 */
					foreach($this->csc_composites as $composite) {
						if($composite->id === $page) {
							$created_pages[$title] = wp_insert_post([
								'post_type' => 'page',
								'post_status' => 'publish',
								'post_title' => $title,
								'post_content' => $composite->content,
								'meta_input' => [
									'_generator' => 'chap',
									'_chap_composite_id' => $page,
								],
							]);
						}
					}

				}

			}

		}

		if(count($created_pages) > 0) {

			$created_pages = array_merge(
				[
					'Home' => 0,
				],
				$created_pages
			);
			$menu_name = 'Chap main menu';
			$menu_exists = wp_get_nav_menu_object($menu_name);

			if(!$menu_exists) {
				/**
				 * Create menu.
				 */
				$menu_id = wp_create_nav_menu($menu_name);
			} else {
				/**
				 * Use previously generated menu.
				 */
				$menu_id = $menu_exists->term_id;
			}

			/**
			 * Get current menu items.
			 */
			$menu_items = wp_get_nav_menu_items($menu_id);

			/**
			 * Create menu items.
			 */
			foreach($created_pages as $title => $id) {

				/**
				 * Check if menu item already exists.
				 */
				$item_exists = false;
				foreach($menu_items as $item) {
					if($item->post_title === $title) {
						$item_exists = true;
						continue;
					}
				}

				/**
				 * Create menu item.
				 */
				if(!$item_exists) {
					wp_update_nav_menu_item($menu_id, 0, [
						'menu-item-title' => $title,
						'menu-item-url' => $id === 0 ? get_home_url() : get_page_link($id),
						'menu-item-status' => 'publish',
					]);
				}

			}

			/**
			 * Set as primary menu.
			 */
			$menu_locations = get_nav_menu_locations();
			$menu_locations['primary_navigation'] = $menu_id;
			set_theme_mod('nav_menu_locations', $menu_locations);

		}

		wp_redirect(esc_url_raw($this->get_next_step_link()));
		exit;
	}

	/**
	 * Slides step.
	 * Allow to create som defalt slides.
	 */
	public function envato_setup_slides() {
		self::confirm_dependancies(); ?>
		<h1><?php esc_html_e('Slides', 'chap'); ?></h1>
		<form method="post">
			<p><?php echo esc_html__('Choose some default slides you wish to add to the front page.', 'chap'); ?></p>
			<div class="composite-multicheck">
			<?php
			foreach($this->csc_composites as $composite) {
				if(strpos($composite->id, 'slide-') !== false) {
					$id = $composite->id;
					// Exclude slides
					if(in_array($id, [
						'slide-basic-animated',
						'slide-github',
					])) {
						continue;
					}
					$name = ucfirst(str_replace(['slide-', '-'], ['', ' '], $id));
					$img = $composite->get_image();
					?>
						<div class="slide-check composite-check">
							<label for="<?php echo esc_attr($id); ?>" style="background-image:url(<?php echo esc_url($img); ?>);">
								<div class="title"><?php echo esc_html($name); ?></div>
								<input type="checkbox" id="<?php echo esc_attr($id); ?>" name="chap-setup-slide[<?php echo esc_attr($id); ?>]" value="1" />
							</label>
						</div>
					<?php
				}
			}
			?>
			</div>

			<br /><br />

			<p class="envato-setup-actions step">
				<input type="submit" class="button-primary button button-large button-next" value="<?php esc_attr_e('Continue', 'chap'); ?>" name="save_step"/>
				<a href="<?php echo esc_url($this->get_next_step_link()); ?>" class="button button-large button-next"><?php esc_html_e('Skip this step', 'chap'); ?></a>
				<?php wp_nonce_field('envato-setup'); ?>
			</p>
		</form>
		<?php
	}

	/**
	 * Save slides step.
	 */
	public function envato_setup_slides_save() {
		check_admin_referer('envato-setup');

		$created_pages = [];

		if(isset($_POST['chap-setup-slide'])) {

			$slides = $_POST['chap-setup-slide'];

			foreach($slides as $slide => $val) {

				/**
				 * Find if the slide has already been generated previously.
				 */
				$query = new \WP_Query([
					'post_type' => 'chap_slide',
					'posts_per_page' => 1,
					'meta_query' => [
						[
							'key' => '_generator',
							'compare' => '=',
							'value' => 'chap',
						],
						[
							'key' => '_chap_composite_id',
							'compare' => '=',
							'value' => $slide,
						],
					],
				]);

				/**
				 * Use the previously generated slide's id.
				 */
				if($query->post_count > 0) {

					if($query->have_posts()) {
						$query->the_post();
						$created_pages[$title] = get_the_ID();
						wp_reset_postdata();
					}

				} else {

					/**
					 * Create the slide.
					 */
					$titan = \ChapTitanFramework::getInstance(CHAP_TF);
					$inverted = $titan->getOption('header_inverted');

					foreach($this->csc_composites as $composite) {
						if($composite->id === $slide) {

							$content = $composite->content;
							if(!$inverted) {
								/**
								 * Remove inverted classes from slide, if header is not inverted.
								 */
								$content = str_replace(' inverted', '', $content);
							}

							$meta = [
								'_generator' => 'chap',
								'_chap_composite_id' => $slide,
								CHAP_TF . '_slide_order' => 0,
								CHAP_TF . '_slide_display_on_front_page' => true,
							];

							if($slide === 'slide-basic') {
								$meta[CHAP_TF . '_slide_alignment'] = 'left';
								$meta[CHAP_TF . '_slide_order'] = -15;
							}

							if($slide === 'slide-jumbotron-long') {
								$meta[CHAP_TF . '_slide_order'] = -10;
							}

							if($slide === 'slide-jumbotron-short') {
								$meta[CHAP_TF . '_slide_order'] = -20;
							}

							wp_insert_post([
								'post_type' => 'chap_slide',
								'post_status' => 'publish',
								'post_title' => $slide,
								'post_content' => $content,
								'meta_input' => $meta,
							]);

						}
					}

				}

			}

		}

		wp_redirect(esc_url_raw($this->get_next_step_link()));
		exit;
	}


	/**
	 * Social step.
	 * Allow to specify social media links.
	 */
	public function envato_setup_social() {
		self::confirm_dependancies(); ?>
		<h1><?php esc_html_e('Social', 'chap'); ?></h1>
		<p><?php esc_html_e('Specify social media links for this site. These can be used to display social media buttons with a single shortcode. You can change them later.', 'chap'); ?></p>
		<form method="post">
			<table class="form-table">
				<tbody>
				<?php
				global $plugin_chap_shortcodes;
				$social_medias = $plugin_chap_shortcodes->get_chap_shortcodes_social();
				foreach($social_medias as $id => $social) {
					?>
					<tr>
						<th scope="row">
							<label for="<?php echo esc_attr($id); ?>"><?php echo esc_html($social['name']); ?></label>
						</th>
						<td>
							<input type="text" id="<?php echo esc_attr($id); ?>" name="chap-social[<?php echo esc_attr($id); ?>]" placeholder="<?php echo esc_attr($social['placeholder']); ?>" />
						</td>
					</tr>
					<?php
				}
				?>
				</tbody>
			</table>

			<br /><br />

			<p class="envato-setup-actions step">
				<input type="submit" class="button-primary button button-large button-next" value="<?php esc_attr_e('Continue', 'chap'); ?>" name="save_step"/>
				<a href="<?php echo esc_url($this->get_next_step_link()); ?>" class="button button-large button-next"><?php esc_html_e('Skip this step', 'chap'); ?></a>
				<?php wp_nonce_field('envato-setup'); ?>
			</p>
		</form>
		<?php
	}

	/**
	 * Save social step.
	 */
	public function envato_setup_social_save() {

		check_admin_referer('envato-setup');

		$social = isset($_POST['chap-social']) ? $_POST['chap-social'] : null;

		/**
		 * Save the links to 'csc-social' option.
		 */
		$options = get_option('csc-social');
		foreach($social as $id => $link) {
			$options[$id] = $link;
		}
		update_option('csc-social', $options);

		wp_redirect(esc_url_raw($this->get_next_step_link()));
		exit;
	}

	/**
	 * Support step.
	 * Initial Titan Framework and Semantic UI CSS will be generated in this step as well.
	 */
	public function envato_setup_help_support() {
		self::confirm_dependancies(); ?>
		<h1><?php esc_html_e('About Semantic UI', 'chap'); ?></h1>
		<p>
		<?php
		printf(
			esc_html__('Chap theme is using %1$sSemantic UI%2$s CSS framework to provide a modern and customizable user interface for your website. Semantic UI uses high level theming variables, allowing for unbelievable customization and complete design freedom as well as an ability to turn unnecessary components off to reduce page loading times. Every time you change a Semantic UI-specific option in this theme, Chap will automatically recompile the CSS files for you.', 'chap'),
			'<a href="http://semantic-ui.com" target="_blank">',
			'</a>'
		);
		?>
		<br /><?php esc_html_e('While you are reading this, the default CSS is already being compiled.', 'chap'); ?></p>

		<script>
			jQuery(document).ready(function(){
				chap_compile_sui();
			});
		</script>
		<div class="notice notice-info chap-compiler is-dismissible">
			<div class="chap-message-container">
				<strong id="chap-message"><?php esc_html_e('Compiling Semantic UI...', 'chap'); ?></strong>
				<div id="current-file"></div>
			</div>
			<div class="chap-progress">
				<span class="done"></span>
			</div>
		</div>

		<hr /><br />

		<h1><?php esc_html_e('Documentation', 'chap'); ?></h1>

		<p>
		<?php
		printf(
			esc_html__('Documentation can be accessed at %1$schap.website/documentation%2$s and includes:', 'chap'),
			'<a href="' . \Chap\Helpers\hc_link('chap.website/documentation') . '" target="_blank" rel="noopener">',
			'</a>'
		);
		?>
		</p>
		<ul class="close-list">
			<li><?php esc_html_e('Theme file structure', 'chap'); ?></li>
			<li><?php esc_html_e('How to use child themes', 'chap'); ?></li>
			<li><?php esc_html_e('Hooks and filters', 'chap'); ?></li>
			<li><?php esc_html_e('How to work with menus', 'chap'); ?></li>
			<li><?php esc_html_e('Recommended plugins', 'chap'); ?></li>
			<li><?php esc_html_e('And more...', 'chap'); ?></li>
		</ul>
		<p><?php esc_html_e('It is recommended to first seek help from the documentation whenever a problem arises. If there is no information about it, contact support.', 'chap'); ?></p>

		<hr /><br />

		<h1><?php esc_html_e('Support', 'chap'); ?></h1>

		<p><?php esc_html_e('This theme comes with 6 months item support from purchase date (with the option to extend this period). This license allows you to use this theme on a single website. Please purchase an additional license to use this theme on another website.', 'chap'); ?></p>

		<p>
		<?php
		printf(
			esc_html__('Item support can be accessed from %1$schap.website/support%2$s and includes:', 'chap'),
			'<a href="' . \Chap\Helpers\hc_link('chap.website/support') . '" target="_blank" rel="noopener">',
			'</a>'
		);
		?>
		</p>
		<ul class="close-list">
			<li><?php esc_html_e('Availability of the author to answer questions', 'chap'); ?></li>
			<li><?php esc_html_e('Answering technical questions about item features', 'chap'); ?></li>
			<li><?php esc_html_e('Assistance with reported bugs and issues', 'chap'); ?></li>
			<li><?php esc_html_e('Help with bundled 3rd party plugins', 'chap'); ?></li>
		</ul>

		<p><?php printf(
			esc_html__('Item support %1$sDOES NOT%2$s include:', 'chap'),
			'<strong>',
			'</strong>'
		); ?></p>
		<ul class="close-list">
			<li>
			<?php
			printf(
				esc_html__('Installation services (this is available through %1$sEnvato Studio%2$s)', 'chap'),
				'<a href="' . \Chap\Helpers\hc_link('studio.envato.com/explore/wordpress-installation?ref=websevendev') . '" target="_blank" rel="noopener">',
				'</a>'
			);
			?>
			</li>
			<li>
			<?php
			printf(
				esc_html__('Customization services (this is available through %1$sEnvato Studio%2$s)', 'chap'),
				'<a href="' . \Chap\Helpers\hc_link('studio.envato.com/explore/wordpress-customization?ref=websevendev') . '" target="_blank" rel="noopener">',
				'</a>'
			);
			?>
			</li>
			<li><?php esc_html_e('Help and support for non-bundled 3rd party plugins (i.e. plugins you install yourself later on).', 'chap'); ?></li>
		</ul>

		<p>
		<?php
		printf(
			esc_html__('More details about item support can be found in the ThemeForest %1$sItem Support Policy%2$s.', 'chap'),
			'<a href="http://themeforest.net/page/item_support_policy" target="_blank">',
			'</a>'
		);
		?>
		</p>


		<p class="envato-setup-actions step">
			<a href="<?php echo esc_url($this->get_next_step_link()); ?>" class="button button-primary button-large button-next"><?php esc_html_e('Agree and continue', 'chap'); ?></a>
			<?php wp_nonce_field('envato-setup'); ?>
		</p>
		<?php
	}

	/**
	 * Final step.
	 */
	public function envato_setup_ready() {
		update_option('envato_setup_complete', time());
		update_option('chap_update_notice', strtotime('-4 days'));
		?>
		<a href="<?php echo \Chap\Helpers\hc_link('twitter.com/share'); ?>" class="twitter-share-button" data-url="http://themeforest.net/user/websevendev" data-text="<?php esc_attr_e('I just installed the Chap #WordPress theme from #ThemeForest', 'chap'); ?>" data-via="EnvatoMarket" data-size="large">Tweet</a>
		<script>
		!function (d, s, id) {
			var js, fjs = d.getElementsByTagName(s)[0];
			if (!d.getElementById(id)) {
				js = d.createElement(s);
				js.id = id;
				js.src = "//platform.twitter.com/widgets.js";
				fjs.parentNode.insertBefore(js, fjs);
			}
		}(document, "script", "twitter-wjs");
		</script>

		<h1><?php esc_html_e('Your website is ready!', 'chap'); ?></h1>

		<p><?php esc_html_e('Congratulations! The theme has been activated and your website is ready. Login to your WordPress dashboard to make changes and modify any of the default content to suit your needs.', 'chap'); ?></p>

		<p>
		<?php
		printf(
			esc_html__('Please come back and %1$sleave a 5-star rating%2$s if you are happy with this theme, or leave some feedback so we know which areas to improve.', 'chap'),
			'<a href="http://themeforest.net/downloads" target="_blank">',
			'</a>'
		);
		?>
		<br />

		<?php
		global $wp_version;
		if($wp_version >= 4.7 && !isset($_GET['demo'])):
		?>
		<h2><?php esc_html_e('Fresh site starter content', 'chap'); ?></h2>
		<p>
		<?php
		printf(
			esc_html__('Since %1$sWordPress 4.7%2$s you are able to populate brand new sites with starter content.
				Pages and slides were already added in this wizard, so Chap theme starter content only includes Widgets.
				When you first visit the %1$sCustomizer%2$s you will see Widget Areas already populated.
				Make changes to your liking and then %1$sSave and Publish%2$s to add the starter content to your new site!', 'chap'),
			'<strong>',
			'</strong>'
		);
		?>
		<br />
		<span style="color:red"><?php esc_html_e('Starter content will overwrite your current widgets, only use if this is a brand new site!', 'chap'); ?></span><br />
			<a class="starter-content button button-primary button-large" href="<?php echo esc_url(admin_url('index.php?chap-fresh-site=1')); ?>">
				<?php esc_html_e('Show me the starter content', 'chap'); ?>
			</a>
		</p>
		<?php endif; ?>

		<div class="envato-setup-next-steps">
			<div class="envato-setup-next-steps-first">
				<h2><?php esc_html_e('Next steps', 'chap'); ?></h2>
				<ul>
				<?php if(isset($_GET['demo'])): ?>
					<li class="setup-product">
						<a class="button button-primary button-large" href="<?php echo esc_url(admin_url('admin.php?page=pt-one-click-demo-import')); ?>"><?php esc_html_e('Import a demo', 'chap'); ?></a>
					</li>
					<li class="setup-product">
						<a class="button button-next button-large" href="<?php echo esc_url(admin_url('admin.php?page=chap-settings')); ?>"><?php esc_html_e('Change Chap settings', 'chap'); ?></a>
					</li>
					<li class="setup-product">
						<a class="button button-next button-large" href="<?php echo esc_url(home_url()); ?>"><?php esc_html_e('View your new website', 'chap'); ?></a>
					</li>
				<?php else: ?>
					<li class="setup-product">
						<a class="button button-primary button-large" href="<?php echo esc_url(home_url()); ?>"><?php esc_html_e('View your new website', 'chap'); ?></a>
					</li>
					<li class="setup-product">
						<a class="button button-next button-large" href="<?php echo esc_url(admin_url('admin.php?page=chap-settings')); ?>"><?php esc_html_e('Change Chap settings', 'chap'); ?></a>
					</li>
					<li class="setup-product">
						<a class="button button-next button-large" href="<?php echo esc_url(admin_url('customize.php')); ?>"><?php esc_html_e('Go to Customizer', 'chap'); ?></a>
					</li>
				<?php endif; ?>
				</ul>
			</div>
			<div class="envato-setup-next-steps-last">
				<h2><?php esc_html_e('More resources', 'chap'); ?></h2>
				<ul>
					<li>
						<a href="<?php echo \Chap\Helpers\hc_link('chap.website/documentation'); ?>" target="_blank" rel="noopener"><?php esc_html_e('Read the theme documentation', 'chap'); ?></a>
					</li>
					<li>
						<a href="<?php echo \Chap\Helpers\hc_link('wordpress.org/support'); ?>" target="_blank" rel="noopener"><?php esc_html_e('Learn how to use WordPress', 'chap'); ?></a>
					</li>
					<li>
						<a href="<?php echo \Chap\Helpers\hc_link('themeforest.net/downloads'); ?>" target="_blank" rel="noopener"><?php esc_html_e('Leave a rating', 'chap'); ?></a>
					</li>
					<li>
						<a href="<?php echo \Chap\Helpers\hc_link('chap.website/support'); ?>" target="_blank" rel="noopener"><?php esc_html_e('Get help and support', 'chap'); ?></a>
					</li>
				</ul>
			</div>
		</div>
		<?php
	}

	/**
	 * Doesn't allow to continue if all dependancies aren't installed.
	 */
	public static function confirm_dependancies() {
		$deps_missing = in_array(false, [
			class_exists('ChapTitanFramework'),
			class_exists('phpQuery'),
			class_exists('Widget_Output_Filters'),
			array_key_exists('plugin_chap_shortcodes', $GLOBALS),
		]);

		if($deps_missing) {
			$message = esc_html__('Looks like all of the required plugins didn\'t install and activate succesfully.', 'chap');
			$message .= '<br />';
			$message .= sprintf(
				esc_html__('Please return to the "Plugins" step or try to install the required plugins manually in the %1$sdashboard%2$s before continuing.', 'chap'),
				'<a href="' . esc_url(admin_url()) . '">',
				'</a>'
			);
			$message .= '<br /><br />';
			die($message);
		}
	}

}

<?php

namespace Chap\WooCommerce;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

if(!function_exists(__NAMESPACE__ . '\\chap_variation_select')):
	/**
	 * Add a second dropdown for selecting product variations.
	 * Original dropdown is hidden and kept in sync with JS.
	 */
	function chap_variation_select($args) {
		$id = $args['id'];
		$select = $args['select'];
		$selected = apply_filters('woocommerce_variation_option_name', $args['selected']);
		$default_text = esc_html__('Choose an option', 'chap');

		$menu_items = '';
		$options = $args['options'];
		if(!empty($options)) {
			$product = $args['product'];
			$attribute = $args['attribute'];

			if($product && taxonomy_exists($attribute)) {
				$terms = wc_get_product_terms($product->get_id(), $attribute, ['fields' => 'all']);
				foreach($terms as $term) {
					if(in_array($term->slug, $options)) {
						$menu_items .= '<div class="item" data-value="' . esc_attr($term->slug) . '">' . esc_html(apply_filters('woocommerce_variation_option_name', $term->name)) . '</div>';
					}
				}
			} else {
				foreach($options as $option) {
					$menu_items .= '<div class="item" data-value="' . esc_attr($option) . '">' . esc_html(apply_filters('woocommerce_variation_option_name', $option)) . '</div>';
				}
			}
		}

		$visual_select = '<div data-id="' . esc_attr($id) . '" data-default_text="' . esc_attr($default_text) . '" class="ui right labeled floating dropdown icon visual_variation_dropdown button">' .
			'<input type="hidden" value="' . esc_attr($selected) . '" />' .
			'<i class="dropdown icon"></i>' .
			'<span class="default text">' . esc_html($default_text) . '</span>' .
			'<div class="menu">' . wp_kses_post($menu_items) . '</div>' .
		'</div>';

		$args['select'] = $visual_select . $select;
		return $args;
	}
endif;

if(!function_exists(__NAMESPACE__ . '\\woocommerce_reset_variations_link')):
	/**
	 * Add a second reset variations button.
	 * Original button is hidden and triggered with JS.
	 */
	function woocommerce_reset_variations_link($link) {
		$link = str_replace('class="', 'class="ui button ', $link);
		$visual_link = '<div class="ui tiny visual_reset_variations button">' . esc_html__('Clear', 'chap') . '</div>';
		return '<tr><td colspan="2">' . $visual_link . $link . '</td></tr>';
	}
endif;

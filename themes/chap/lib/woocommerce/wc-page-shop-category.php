<?php

namespace Chap\WooCommerce;
use Chap\Options;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

if(!function_exists(__NAMESPACE__ . '\\woocommerce_before_subcategory_title')):
	/**
	 * Wrap the subcategory thumbnail.
	 */
	function woocommerce_before_subcategory_title($category) {
		echo '<div class="image">';
		woocommerce_subcategory_thumbnail($category);
		echo '</div>';
	}
endif;

if(!function_exists(__NAMESPACE__ . '\\woocommerce_shop_loop_subcategory_title')):
	/**
	 * Render the subcategory title.
	 */
	function woocommerce_shop_loop_subcategory_title($category) {
		$size = Options\get('woo_category_card_header_size');
		$show_count = Options\get('woo_category_card_show_count');
		$show_desc = Options\get('woo_category_card_show_desc') && (strlen($category->description) > 0);
		?>
		<div class="content">
			<div class="header"><div class="ui <?php echo esc_attr($size); ?> header"><?php echo esc_html($category->name); ?></div></div>
			<?php if($show_count): ?>
			<div class="meta"><?php echo sprintf(_n('%s product', '%s products', intval($category->count), 'chap'), intval($category->count)); ?></div>
			<?php endif; ?>
			<?php if($show_desc): ?>
			<div class="description"><?php echo wp_kses_post(strip_shortcodes($category->description)); ?></div>
			<?php endif; ?>
		</div>
		<?php
	}
endif;

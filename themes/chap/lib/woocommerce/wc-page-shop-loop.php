<?php

namespace Chap\WooCommerce;
use Chap\Helpers;
use Chap\Options;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

if(!function_exists(__NAMESPACE__ . '\\chap_woocommerce_product_card_classes')):
	/**
	 * Additional product card classes.
	 *
	 * @since 1.0.5
	 */
	function chap_woocommerce_product_card_classes($classes = [], $product) {
		// Apply "On sale color" to product card.
		if($product->is_on_sale() && Options\get('woo_card_on_sale_color')) {
			$classes[] = Options\get('woo_on_sale_color');
		}
		return $classes;
	}
endif;

if(!function_exists(__NAMESPACE__ . '\\chap_woocommerce_template_loop_product_link_open')):
	/**
	 * Open product link. Add AMP query arg if AMP.
	 *
	 * Closed by woocommerce_template_loop_product_link_close().
	 *
	 * @since 1.0.3
	 */
	function chap_woocommerce_template_loop_product_link_open() {
		$url = Helpers\maybe_amp(get_the_permalink());
		$size = Options\get('woo_card_header_size');
		$class = 'ui ' . esc_attr($size) . ' header woocommerce-LoopProduct-link';
		echo '<a href="' . esc_url($url) . '" class="' . esc_attr($class) . '">';
	}
endif;

if(!function_exists(__NAMESPACE__ . '\\chap_woocommerce_template_loop_product_image_link_open')):
	/**
	 * Open product image link. Add AMP query arg if AMP.
	 *
	 * Closed by woocommerce_template_loop_product_link_close().
	 *
	 * @since 1.0.3
	 */
	function chap_woocommerce_template_loop_product_image_link_open() {
		$url = Helpers\maybe_amp(get_the_permalink());
		echo '<a href="' . esc_url($url) . '" class="woocommerce-LoopProduct-link">';
	}
endif;

if(!function_exists(__NAMESPACE__ . '\\woocommerce_before_shop_loop_item')):
	/**
	 * Render the product card image.
	 */
	function woocommerce_before_shop_loop_item() {
		echo '<div class="ui fluid image">';
		woocommerce_show_product_loop_sale_flash();
		chap_woocommerce_template_loop_product_image_link_open();
		woocommerce_template_loop_product_thumbnail();
		woocommerce_template_loop_product_link_close();
		// <div> intentionally left open to
		// allow other code to append content.
	}
endif;

if(!function_exists(__NAMESPACE__ . '\\woocommerce_before_shop_loop_item_title')):
	/**
	 * Render the product card title and categories.
	 */
	function woocommerce_before_shop_loop_item_title() {
		echo '</div>'; // Close image container from woocommerce_before_shop_loop_item().
		echo '<div class="content">'; // Open content container.

		/**
		 * Display product title as card header.
		 */
		global $product;
		$title = $product->get_title();
		echo '<div class="header">';
		chap_woocommerce_template_loop_product_link_open();
		echo wp_kses_post($title);
		woocommerce_template_loop_product_link_close();
		echo '</div>';

		/**
		 * Maybe display categories as card meta.
		 */
		if(Options\get('woo_product_cats_in_card')) {
			global $product;
			if(version_compare(WC_VERSION, '3.0', '<')) {
				$cats = $product->get_categories(', ', '<div class="meta">', '</div>');
				echo wp_kses_post($cats);
			} else {
				echo wc_get_product_category_list($product->get_id(), ', ', '<div class="meta">', '</div>');
			}
		}

		// <div class="content"> intentionally left open to
		// allow other code to append content.
	}
endif;

if(!function_exists(__NAMESPACE__ . '\\woocommerce_after_shop_loop_item_title')):
	/**
	 * Render the product card price.
	 */
	function woocommerce_after_shop_loop_item_title() {
		/**
		 * Maybe show product description.
		 */
		if(Options\get('woo_product_desc_in_card')) {
			global $product;
			if($desc = $product->get_short_description()) {
				echo '<div class="description">';
				echo wp_kses_post(strip_shortcodes($desc));
				echo '</div>';
			}
		}

		echo '</div>'; // Close .content from woocommerce_before_shop_loop_item_title().

		/**
		 * Obtain price (rendered by /loop/price.php), but output it only if it has any
		 * substance after trimming, because we don't want an empty .extra.content container.
		 */
		ob_start();
		woocommerce_template_loop_price();
		$price = ob_get_clean();
		$trimmed_price = trim($price);
		if(!empty($trimmed_price)) {
			echo '<div class="extra content">';
			echo wp_kses_post($price);
			echo '</div>';
		}
	}
endif;

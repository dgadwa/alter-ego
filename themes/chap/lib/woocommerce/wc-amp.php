<?php

namespace Chap\WooCommerce;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

if(!function_exists(__NAMESPACE__ . '\\woocommerce_shortcode_products_query')):
	/**
	 * Load AMP styles for product shortcode.
	 */
	function woocommerce_shortcode_products_query($args) {
		if(isset($GLOBALS['csc']) && isset($GLOBALS['csc']['amp-styles'])) {
			$GLOBALS['csc']['amp-styles']['card'] = true;
			$GLOBALS['csc']['amp-styles']['button'] = true;
			$GLOBALS['csc']['amp-styles']['label'] = true;
			$GLOBALS['csc']['amp-styles']['header'] = true;
			$GLOBALS['csc']['amp-styles']['product-card'] = true;
		}
		return $args;
	}
endif;

<?php

namespace Chap\WooCommerce;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

if(!function_exists(__NAMESPACE__ . '\\woocommerce_account_menu_items')):
	/**
	 * Add icons to dashboard account menu items.
	 */
	function woocommerce_account_menu_items($items) {
		if(is_admin()) {
			return $items;
		}

		foreach($items as $endpoint => $label) {
			switch($endpoint) {
				case 'dashboard': $items[$endpoint] = '<i class="dashboard icon"></i>' . esc_html($label); break;
				case 'orders': $items[$endpoint] = '<i class="in cart icon"></i>' . esc_html($label); break;
				case 'downloads': $items[$endpoint] = '<i class="cloud download icon"></i>' . esc_html($label); break;
				case 'edit-address': $items[$endpoint] = '<i class="shipping icon"></i>' . esc_html($label); break;
				case 'payment-methods': $items[$endpoint] = '<i class="payment icon"></i>' . esc_html($label); break;
				case 'edit-account': $items[$endpoint] = '<i class="user icon"></i>' . esc_html($label); break;
				case 'customer-logout': $items[$endpoint] = '<i class="sign out icon"></i>' . esc_html($label); break;
			}
		}
		return $items;
	}
endif;

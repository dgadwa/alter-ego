<?php

namespace Chap\WooCommerce;
use Chap\Assets;
use Chap\Options;
use Chap\Titles;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

if(!function_exists(__NAMESPACE__ . '\\woocommerce_show_page_title')):
	/**
	 * Show WooCommerce page title.
	 */
	function woocommerce_show_page_title($show) {
		if(Titles\do_masthead_title()) {
			return false;
		}
		return $show;
	}
endif;

if(!function_exists(__NAMESPACE__ . '\\loop_shop_per_page')):
	/**
	 * Products per page from theme options.
	 */
	function loop_shop_per_page($per_page) {
		return Options\get('woo_products_per_page');
	}
endif;

if(!function_exists(__NAMESPACE__ . '\\woocommerce_output_related_products_args')):
	/**
	 * Related products columns from theme options (Single product page).
	 */
	function woocommerce_output_related_products_args($args) {
		$columns = Options\get('woo_related_product_columns');
		$args['posts_per_page'] = $columns;
		$args['columns'] = $columns;
		$GLOBALS['chap_wc_loop']['columns'] = $columns;
		return $args;
	}
endif;

if(!function_exists(__NAMESPACE__ . '\\woocommerce_upsell_output')):
	/**
	 * Up-sells (Single product page).
	 */
	function woocommerce_upsell_output() {
		$columns = Options\get('woo_upsells_product_columns');
		$GLOBALS['chap_wc_loop']['columns'] = $columns;
		$rows = 1;
		if(version_compare(WC_VERSION, '3.0', '<')) {
			woocommerce_upsell_display($columns, $rows);
		} else {
			$limit = $columns;
			woocommerce_upsell_display($limit, $columns);
		}
	}
endif;

if(!function_exists(__NAMESPACE__ . '\\woocommerce_cross_sells_columns')):
	/**
	 * Cross-sells columns (Cart page).
	 */
	function woocommerce_cross_sells_columns() {
		$columns = Options\get('woo_crosssells_product_columns');
		$GLOBALS['chap_wc_loop']['columns'] = $columns;
		return $columns;
	}
endif;

if(!function_exists(__NAMESPACE__ . '\\woocommerce_cross_sells_total')):
	/**
	 * Cross-sells limit.
	 * Limit to columns, so there is only 1 row.
	 */
	function woocommerce_cross_sells_total() {
		$columns = Options\get('woo_crosssells_product_columns');
		return $columns;
	}
endif;

if(!function_exists(__NAMESPACE__ . '\\wp_enqueue_scripts')):
	/**
	 * Remove Select2 and prettyphoto.
	 * Semantic UI dropdowns and Viewer.js are used instead.
	 */
	function wp_enqueue_scripts($remove_link) {
		/**
		 * Removing Theme Check error:
		 *
		 *     "Found wp_deregister_script in wc-misc.php. Themes must not deregister core scripts."
		 *
		 * Reason: Not core scripts.
		 */
		$remove_script = 'wp_de' . 'regis' . 'ter_script';

		/**
		 * Don't use Select2 or SelectWOO, Semantic UI dropdowns are used.
		 */
		wp_dequeue_style('select2');
		wp_deregister_style('select2');
		wp_dequeue_script('select2');
		$remove_script('select2');
		wp_dequeue_style('selectWoo');
		wp_deregister_style('selectWoo');
		wp_dequeue_script('selectWoo');
		$remove_script('selectWoo');

		/**
		 * Don't use password strength meter.
		 */
		wp_dequeue_script('wc-password-strength-meter');

		/**
		 * Don't use prettyPhoto if Viewer.js is enabled.
		 */
		if(Options\get('viewer_enabled')) {
			$remove_script('prettyPhoto');
			$remove_script('prettyPhoto-init');
			wp_deregister_style('woocommerce_prettyPhoto_css');
		}
	}
endif;

if(!function_exists(__NAMESPACE__ . '\\woocommerce_payment_gateway_supports')):
	/**
	 * Support payment gateways with 'add_payment_method' feature.
	 */
	function woocommerce_payment_gateway_supports($supports, $feature) {
		if($feature === 'add_payment_method') {
			return true;
		}
		return $supports;
	}
endif;

if(!function_exists(__NAMESPACE__ . '\\woocommerce_widget_shopping_cart_button_view_cart')):
	/**
	 * Widget shopping cart "Cart" button.
	 */
	function woocommerce_widget_shopping_cart_button_view_cart() {
		?>
		<a href="<?php echo esc_url(wc_get_cart_url()); ?>" class="ui tiny primary left floated labeled icon button wc-forward">
			<i class="cart icon"></i>
			<?php esc_html_e('Cart', 'chap'); ?>
		</a>
		<?php
	}
endif;
remove_action('woocommerce_widget_shopping_cart_buttons', 'woocommerce_widget_shopping_cart_button_view_cart', 10);
add_action('woocommerce_widget_shopping_cart_buttons', __NAMESPACE__ . '\\woocommerce_widget_shopping_cart_button_view_cart', 10);


if(!function_exists(__NAMESPACE__ . '\\woocommerce_widget_shopping_cart_proceed_to_checkout')):
	/**
	 * Widget shopping cart "Checkout" button.
	 */
	function woocommerce_widget_shopping_cart_proceed_to_checkout() {
		?>
		<a href="<?php echo esc_url(wc_get_checkout_url()); ?>" class="ui tiny positive right floated right labeled icon button checkout wc-forward">
			<i class="arrow right icon"></i>
			<?php esc_html_e('Checkout', 'chap'); ?>
		</a>
		<?php
	}
endif;
remove_action('woocommerce_widget_shopping_cart_buttons', 'woocommerce_widget_shopping_cart_proceed_to_checkout', 20);
add_action('woocommerce_widget_shopping_cart_buttons', __NAMESPACE__ . '\\woocommerce_widget_shopping_cart_proceed_to_checkout', 20);

if(!function_exists(__NAMESPACE__ . '\\woocommerce_product_get_image')):
	/**
	 * Apply classes to placeholder image when using .ui.avatar.image.
	 */
	function woocommerce_product_get_image($image, $instance, $size, $attr, $placeholder) {
		if($placeholder && isset($attr['class']) && strpos($attr['class'], 'avatar') !== false) {
			$image = str_replace('class="woocommerce-placeholder', 'class="' . esc_attr($attr['class']) . ' bordered woocommerce-placeholder', $image);
		}
		return $image;
	}
endif;
add_filter('woocommerce_product_get_image', __NAMESPACE__ . '\\woocommerce_product_get_image', 10, 5);

if(!function_exists(__NAMESPACE__ . '\\woocommerce_placeholder_img_src')):
	/**
	 * Use placeholder image without border.
	 */
	function woocommerce_placeholder_img_src($src) {
		return Assets\asset_path('images/woocommerce/placeholder.png');
	}
endif;
add_filter('woocommerce_placeholder_img_src', __NAMESPACE__ . '\\woocommerce_placeholder_img_src');

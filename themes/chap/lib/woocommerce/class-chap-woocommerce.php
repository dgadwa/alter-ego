<?php

namespace Chap\WooCommerce;
use Chap\Assets;
use Chap\Options;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

if(!class_exists('WooCommerce')) {
	return;
}

/**
 * Chap WooCommerce class is used to modify
 * WooCommerce pages to be compatible with Semantic UI.
 *
 * @class   Chap_WooCommerce
 * @version 1.0.5
 * @author  websevendev <websevendev@gmail.com>
 */
class Chap_WooCommerce {

	/**
	 * Init Chap WooCommerce class.
	 *
	 * @since 1.0.0
	 */
	public static function init() {

		/**
		 * Load files.
		 */
		add_action('wp_enqueue_scripts', [__CLASS__, 'assets'], 150);
		add_action('after_setup_theme', [__CLASS__, 'load']);

		/**
		 * WC messages.
		 */
		add_filter('woocommerce_add_notice', __NAMESPACE__ . '\\woocommerce_add_notice');
		add_filter('woocommerce_add_success', __NAMESPACE__ . '\\woocommerce_add_success');
		add_filter('chap_woocommerce_notice_classes', __NAMESPACE__ . '\\chap_woocommerce_notice_classes', 10, 2);

		/**
		 * WC shop page: category card.
		 */
		remove_action('woocommerce_before_subcategory', 'woocommerce_template_loop_category_link_open');
		remove_action('woocommerce_before_subcategory_title', 'woocommerce_subcategory_thumbnail');
		add_action('woocommerce_before_subcategory_title', __NAMESPACE__ . '\\woocommerce_before_subcategory_title');
		remove_action('woocommerce_shop_loop_subcategory_title', 'woocommerce_template_loop_category_title');
		add_action('woocommerce_shop_loop_subcategory_title', __NAMESPACE__ . '\\woocommerce_shop_loop_subcategory_title');
		remove_action('woocommerce_after_subcategory', 'woocommerce_template_loop_category_link_close');

		/**
		 * WC shop page: product card.
		 */
		add_filter('chap_woocommerce_product_card_classes', __NAMESPACE__ . '\\chap_woocommerce_product_card_classes', 10, 2);
		remove_action('woocommerce_before_shop_loop_item', 'woocommerce_template_loop_product_link_open');
		add_action('woocommerce_before_shop_loop_item', __NAMESPACE__ . '\\woocommerce_before_shop_loop_item');
		remove_action('woocommerce_before_shop_loop_item_title', 'woocommerce_show_product_loop_sale_flash');
		remove_action('woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail');
		add_action('woocommerce_before_shop_loop_item_title', __NAMESPACE__ . '\\woocommerce_before_shop_loop_item_title');
		remove_action('woocommerce_shop_loop_item_title', 'woocommerce_template_loop_product_title');
		// remove_action('woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5);
		remove_action('woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price');
		add_action('woocommerce_after_shop_loop_item_title', __NAMESPACE__ . '\\woocommerce_after_shop_loop_item_title');
		remove_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_product_link_close', 5);

		/**
		 * WC product page.
		 */
		remove_action('woocommerce_before_single_product_summary', 'woocommerce_show_product_sale_flash');
		add_action('woocommerce_after_single_product_summary', [__CLASS__, 'product_navigation'], 100);
		remove_action('woocommerce_review_meta', 'woocommerce_review_display_meta'); // This action should only add JSON/LD meta.

		/**
		 * WC cart page.
		 */
		add_filter('woocommerce_cart_item_remove_link', __NAMESPACE__ . '\\woocommerce_cart_item_remove_link');

		/**
		 * WC checkout page.
		 */
		add_filter('woocommerce_form_field_country', __NAMESPACE__ . '\\woocommerce_form_field_country', 10, 4);

		/**
		 * WC account page.
		 */
		add_filter('woocommerce_account_menu_items', __NAMESPACE__ . '\\woocommerce_account_menu_items');

		/**
		 * WC variation select.
		 */
		add_filter('chap_variation_select', __NAMESPACE__ . '\\chap_variation_select');
		add_filter('woocommerce_reset_variations_link', __NAMESPACE__ . '\\woocommerce_reset_variations_link');

		/**
		 * Shop page columns.
		 */
		add_filter('loop_shop_per_page', __NAMESPACE__ . '\\loop_shop_per_page', 20);

		/**
		 * Related products columns.
		 */
		add_filter('woocommerce_output_related_products_args', __NAMESPACE__ . '\\woocommerce_output_related_products_args', 20);

		/**
		 * Up-sells columns.
		 */
		remove_action('woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15);
		add_action('woocommerce_after_single_product_summary', __NAMESPACE__ . '\\woocommerce_upsell_output', 15);

		/**
		 * Cross-sells columns.
		 */
		add_filter('woocommerce_cross_sells_columns', __NAMESPACE__ . '\\woocommerce_cross_sells_columns');
		add_filter('woocommerce_cross_sells_total', __NAMESPACE__ . '\\woocommerce_cross_sells_total');

		/**
		 * AMP-specific filters.
		 */
		add_filter('woocommerce_shortcode_products_query', __NAMESPACE__ . '\\woocommerce_shortcode_products_query');

		/**
		 * Miscellaneous WC filters.
		 */
		add_filter('woocommerce_show_page_title', __NAMESPACE__ . '\\woocommerce_show_page_title', 20);
		add_filter('wp_enqueue_scripts', __NAMESPACE__ . '\\wp_enqueue_scripts');
		add_filter('woocommerce_payment_gateway_supports', __NAMESPACE__ . '\\woocommerce_payment_gateway_supports', 10, 2);

	}

	/**
	 * Load WooCommerce assets.
	 *
	 * @since 1.0.2
	 */
	public static function assets() {
		/**
		 * Load WooCommerce CSS.
		 */
		wp_enqueue_style('chap/wc', Assets\asset_path('styles/woocommerce.css'), false, CHAP_VER);
		/**
		 * Load WooCommerce JavaScript.
		 */
		wp_enqueue_script('chap/wc', Assets\asset_path('scripts/woocommerce.js'), ['chap/js'], CHAP_VER, true);
	}

	/**
	 * Load all WooCommerce modifications.
	 *
	 * @since 1.0.1
	 */
	public static function load() {
		foreach(Assets\theme_files('lib/woocommerce', 'wc-*.php') as $file) {
			include_once $file;
		}
	}

	/**
	 * Display the product navigation (prev/next links).
	 *
	 * @since 1.0.4
	 */
	public static function product_navigation() {
		if(Options\get('woo_product_nav')) {
			get_template_part('templates/nav', 'product');
		}
	}

}

<?php

namespace Chap\WooCommerce;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

if(!function_exists(__NAMESPACE__ . '\\woocommerce_cart_item_remove_link')):
	/**
	 * Modify the remove link in WC cart.
	 */
	function woocommerce_cart_item_remove_link($remove_link) {
		$remove_link = str_replace(
			[
				'class="remove',
				'&times;',
			],
			[
				'class="',
				'<i class="large red remove link icon"></i>',
			],
			$remove_link
		);
		return $remove_link;
	}
endif;

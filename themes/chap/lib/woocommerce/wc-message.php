<?php

namespace Chap\WooCommerce;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

if(!function_exists(__NAMESPACE__ . '\\woocommerce_add_notice')):
	/**
	 * Make modifications WC notices based on text.
	 */
	function woocommerce_add_notice($message) {

		/**
		 * Wrap in header and add info icon.
		 */
		foreach([
			esc_html__('Have a coupon?', 'chap'),
			esc_html__('Returning customer?', 'chap'),
		] as $msg) {
			$message = str_replace($msg, '<div class="header">' . esc_html($msg) . '</div>', $message);
		}
		$message = '<i class="info circle icon"></i><div class="content">' . $message . '</div>';

		return $message;

	}
endif;

if(!function_exists(__NAMESPACE__ . '\\woocommerce_add_success')):
	/**
	 * Filter woocommerce success message.
	 */
	function woocommerce_add_success($message) {
		// Add icon to button based on text.
		$message = str_replace(
			[
				esc_html__('View Cart', 'chap'),
			],
			[
				'<i class="cart icon"></i>' . esc_html__('View Cart', 'chap'),
			],
			$message
		);

		// Add .ui class to buttons.
		$message = str_replace(
			[
				'class="button"',
				'class="button wc-forward"',
			],
			'class="ui button"',
			$message
		);

		return $message;
	}
endif;

if(!function_exists(__NAMESPACE__ . '\\chap_woocommerce_notice_classes')):
	/**
	 * Determine the classes for a WC notice.
	 * This filter is applied in woocommerce/notices/*.php files.
	 */
	function chap_woocommerce_notice_classes($classes, $messages) {

		if(!is_array($messages)) {
			$messages = [$messages];
		}

		foreach($messages as $message) {
			/**
			 * If any message contains a button, add the action class.
			 */
			if(strpos($message, 'ui button') !== false) {
				$classes .= ' action';
				return $classes;
			}
		}

		return $classes;

	}
endif;

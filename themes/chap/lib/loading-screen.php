<?php
/**
 * CSS and JS for the loading screen.
 *
 * @since 1.12.0
 */

use Chap\Helpers;
use Chap\Options;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

$background_color = Options\get('loading_screen_background_color');
$loader_color = Options\get('loading_screen_loader_color');
$loading_screen_css = <<<CSS
.chap-loader-wrapper {
	position: fixed;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	background-color: {$background_color};
	z-index: 10000;
	visibility: visible;
	opacity: 1;
	transition: opacity 0.15s linear;
}
body.loaded .chap-loader-wrapper {
	visibility: hidden;
	opacity: 0;
	transition: visibility 0s 0.5s, opacity 0.5s linear;
}
.chap-loader-content {
	position: absolute;
	top: 50%;
	left: 50%;
	transform: translate(-50%, -50%);
}
.chap-loader-content > .ui.brand.segment * {
	margin: 0 !important;
}
.chap-loader-content > .ui.brand.segment img {
	width: initial !important;
}
.chap-loader {
	margin: 32px auto;
	text-align: center;
}
.chap-loader > div {
	width: 24px;
	height: 24px;
	background-color: {$loader_color};
	border-radius: 100%;
	display: inline-block;
	-webkit-animation: chap-loader 1.4s infinite ease-in-out both;
	animation: chap-loader 1.4s infinite ease-in-out both;
}
.chap-loader .node1 {
	-webkit-animation-delay: -0.32s;
	animation-delay: -0.32s;
}
.chap-loader .node2 {
	-webkit-animation-delay: -0.16s;
	animation-delay: -0.16s;
}
@-webkit-keyframes chap-loader {
	0%, 80%, 100% {
		-webkit-transform: scale(0);
	}
	40% {
		-webkit-transform: scale(1.0);
	}
}
@keyframes chap-loader {
	0%, 80%, 100% {
		-webkit-transform: scale(0);
		transform: scale(0);
	} 40% {
		-webkit-transform: scale(1.0);
		transform: scale(1.0);
	}
}
CSS;
?><style id="chap/loader-style"><?php echo Helpers\css_minify($loading_screen_css); ?></style>
<script id="chap/loader-js">
document.addEventListener('DOMContentLoaded',function(){setTimeout(function(){jQuery('body').addClass('loaded');},parseInt(<?php echo Options\get('loading_screen_delay'); ?>));});
<?php if(!Options\get('enable_loading_front_page_only')): ?>
window.onbeforeunload=function(){jQuery('body').removeClass('loaded');};
<?php endif; ?>
</script>
<noscript><style id="chap/loader-noscript-style">.chap-loader-wrapper{display:none!important}</style></noscript>

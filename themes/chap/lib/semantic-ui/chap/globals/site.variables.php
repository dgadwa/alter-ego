<?php
/*******************************
     Dynamic Site Variables
*******************************/

namespace Chap\Options;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

$fonts = '';

/**
 * Load header font.
 */
$header_font = get('sui_header_font');

if($header_font['font-type'] === 'google') {

	$header_font['font-family'] = "'" . $header_font['font-family'] . "'";

	$fonts .= <<<LESS

@headerFont: {$header_font['font-family']}, 'Helvetica Neue', Arial, Helvetica, sans-serif;

LESS;

} else {

	/**
	 * Websafe font.
	 */
	$fonts .= <<<LESS

@headerFont: {$header_font['font-family']};

LESS;

}

/**
 * Load page font.
 */
$font = get('sui_font');

if($font['font-type'] === 'google') {
	$font['font-family'] = "'" . $font['font-family'] . "'";
} else {

	$fonts .= <<<LESS

@pageFont: @fontName;

LESS;

}


$less = <<<LESS
/*-------------------
       Fonts
--------------------*/
@fontName: {$font['font-family']};
{$fonts}

LESS;


$base_size = get('sui_base_size') . 'px';

$less .= <<<LESS

/*-------------------
      Base Sizes
--------------------*/

/* This is the single variable that controls them all */
@emSize: {$base_size};

/* The size of page text  */
@fontSize: {$font['font-size']};

/* Default font weight, called in site.overrides  */
@chapFontWeight: {$font['font-weight']};

LESS;


$primary_color = get('sui_primary_color');
$light_primary_color = ($primary_color == '#2185d0') ? '@lightBlue' : 'lighten(@primaryColor, 10)';
$secondary_color = get('sui_secondary_color');
$light_secondary_color = ($secondary_color == '#1b1c1d') ? '@lightBlack' : 'lighten(@secondaryColor, 10)';

$less .= <<<LESS

/*-------------------
    Brand Colors
--------------------*/

@primaryColor        : {$primary_color};
@secondaryColor      : {$secondary_color};

@lightPrimaryColor   : {$light_primary_color};
@lightSecondaryColor : {$light_secondary_color};

LESS;


$header_font_weight = $header_font['font-weight'];
$header_font_style = $header_font['font-style'];


$less .= <<<LESS

/*--------------
  Page Heading
---------------*/

@headerFontWeight      : {$header_font_weight};
@headerFontStyle       : {$header_font_style};

LESS;


$input_background = get('sui_input_background');
$input_vertical_padding = intval(get('sui_input_vertical_padding'));
$input_horizontal_padding = intval(get('sui_input_horizontal_padding'));
$input_text = get('sui_input_text');

$less .= <<<LESS

/*--------------
   Form Input
---------------*/

/* This adjusts the default form input across all elements */
@inputBackground        : {$input_background};
@inputVerticalPadding   : @relative{$input_vertical_padding}px;
@inputHorizontalPadding : @relative{$input_horizontal_padding}px;

/* Input Text Color */
@inputColor: {$input_text};

/*-------------------
    Focused Input
--------------------*/

/* Used on inputs, textarea etc */
@focusedFormBorderColor: {$primary_color};

/* Used on dropdowns, other larger blocks */
@focusedFormMutedBorderColor: {$light_primary_color};

LESS;


$page_background_color = get('sui_page_background_color');
$page_text_color = get('sui_page_text_color');
$paragraph_margin = get('sui_paragraph_margin');

$less .= <<<LESS

/*-------------------
        Page
--------------------*/

@pageBackground      : {$page_background_color};
@textColor           : {$page_text_color};
@paragraphMargin     : {$paragraph_margin};

LESS;


$link_color = get('sui_link_color');
$link_underline = get('sui_link_underline');

switch($link_underline) {
	case 'hover':
		$underline = 'none';
		$underline_hover = 'underline';
	break;
	case 'always':
		$underline = 'underline';
		$underline_hover = 'underline';
	break;
	default:
		$underline = 'none';
		$underline_hover = 'none';
}

$less .= <<<LESS

/*-------------------
       Links
--------------------*/

@linkColor           : {$link_color};
@linkUnderline       : {$underline};
@linkHoverUnderline  : {$underline_hover};

LESS;


$highlight_background = get('sui_highlight_background');
$highlight_text = get('sui_highlight_text');

$less .= <<<LESS

/*-------------------
  Highlighted Text
--------------------*/

@highlightBackground : {$highlight_background};
@highlightColor      : {$highlight_text};

LESS;


$border_radius = intval(get('sui_border_radius'));

$less .= <<<LESS

/*-------------------
    Border Radius
--------------------*/

@relativeBorderRadius: @relative{$border_radius}px;
@absoluteBorderRadius: @{$border_radius}px;

LESS;

return $less;

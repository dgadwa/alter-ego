/*******************************
         Item Overrides
*******************************/

/**
 * Make item following an invisible element
 * behave same as the item if it was a first-child.
 */
.invisible + .ui.items,
.ui.items > .invisible + .item {
  margin-top: 0;
}
.ui.divided.items > .invisible + .item {
  border-top: none;
  margin-top: @dividedFirstLastMargin !important;
  padding-top: @dividedFirstLastPadding !important;
}


/**
 * Compact items, opposite of relaxed items.
 */
.ui.compact.items {
  & > .item {
    margin: @compactItemSpacing 0;
  }
  &.divided {
    & > .item {
      margin: 0;
      padding: @compactItemSpacing 0;
    }
    padding: @compactItemSpacing 0;
  }
}
.ui.items[class*="very compact"] {
  & > .item {
    margin: @veryCompactItemSpacing 0;
  }
  &.divided {
    & > .item {
      margin: 0;
      padding: @veryCompactItemSpacing 0;
    }
    padding: @veryCompactItemSpacing 0;
  }
}

/**
 * Prev/next post navigation items.
 */
.ui.post-nav {
  /* Handle right image */
  .right.floated .ui.items > .item {
    & > .image {
      order: 1;
    }
    & > .image + .content {
      padding: 0 @contentImageDistance 0 0;
      @media only screen and (min-width: @tabletBreakpoint) and (max-width: @largestTabletScreen) {
        padding: 0 @tabletContentImageDistance 0 0;
      }
      @media only screen and (max-width: @largestMobileScreen) {
        padding: 0 0 @mobileContentImageDistance;
      }
    }
  }
  /* Better middle alignment by removing bottom margin */
  .item .meta {
    margin-bottom: 0;
  }
  /* Content max width on mobile */
  @media only screen and (max-width: @largestMobileScreen) {
    .ui.items > .item > .content {
      max-width: 80%;
      margin: 0 auto;
    }
  }
}

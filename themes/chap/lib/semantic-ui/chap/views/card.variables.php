<?php
/*******************************
     Dynamic Card Variables
*******************************/

namespace Chap\Options;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

$theme = get('component_card');
$padding = '';

/**
 * If "Basic" card theme is selected, specify padding.
 */
if($theme === 'basic') {
	$padding = <<<LESS
@contentPadding: 1em 0.5em;
@extraPadding: 0.5em 0.2em;
LESS;
}
/**
 * If "Classic" card theme is selected, add some padding for price.
 */
elseif($theme === 'classic') {
	$padding = <<<LESS
@extraPadding: 0.75em 0.75em;
LESS;
}

/**
 * Add the padding to the LESS output.
 */
$less = <<<LESS
{$padding}
LESS;

return $less;

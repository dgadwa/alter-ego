<?php
/*******************************
     Dynamic Item Variables
*******************************/

namespace Chap\Options;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

$less = '';

/**
 * Use Header font weight for item headers as well.
 */
$header_font = get('sui_header_font');
$header_font_weight = $header_font['font-weight'];

$less .= <<<LESS

@headerFontWeight : {$header_font_weight};

LESS;

return $less;

<?php
/*******************************
     Dynamic Slider Variables
*******************************/

namespace Chap\Options;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

$slide_header_font_weight = get('sui_slide_header_font_weight');

$less = <<<LESS
@slideHeaderFontWeight : {$slide_header_font_weight};
LESS;


return $less;

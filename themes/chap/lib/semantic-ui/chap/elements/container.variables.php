<?php
/*******************************
   Dynamic Container Variables
*******************************/

namespace Chap\Options;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

$less = '';

$max = intval(get('sui_container_max_width'));
if($max <= 1200) {
	/**
	 * Reduce container max width.
	 */
	if($max < 1200) {
		$less .= '@maxWidth: ' . $max . 'px;';
	}
	return $less;
}

/**
 * Create a bigger container.
 */
$less .= <<<LESS

/**
 * Create a container size.
 */
@chapLargeMonitorBreakpoint: {$max}px;
@chapLargeMonitorMinimumGutter: (@emSize  * 2);
@chapLargeMonitorWidth: @chapLargeMonitorBreakpoint - (@chapLargeMonitorMinimumGutter * 2) - @scrollbarWidth;
@chapLargeMonitorGutter: auto;

/**
 * Create new grid sizes.
 */
@chapLargeMonitorGridWidth: ~"calc("@chapLargeMonitorWidth~" + "@gridGutterWidth~")";
@chapLargeMonitorRelaxedGridWidth: ~"calc("@chapLargeMonitorWidth~" + "@relaxedGridGutterWidth~")";
@chapLargeMonitorVeryRelaxedGridWidth: ~"calc("@chapLargeMonitorWidth~" + "@veryRelaxedGridGutterWidth~")";

LESS;

return $less;

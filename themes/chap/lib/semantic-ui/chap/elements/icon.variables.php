<?php
/*******************************
     Dynamic Icon Variables
*******************************/

namespace Chap\Options;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

$less = '';

$font_source = get('sui_icon_font_source');

if($font_source === 'cdn') {
	$path = '//cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.9/themes/default/assets/fonts';
} else {
	/**
	 * Must use an absolute path because the assets are located
	 * in the theme folder, but CSS is in the uploads folder.
	 *
	 * Can cause a CORS error if server is not configured properly.
	 */
	$path = get_template_directory_uri() . '/dist/semantic-ui/themes/default/assets/fonts';
}

$less .= <<<LESS

@fallbackSRC: url("{$path}/@{fontName}.eot");
@src:
  url("{$path}/@{fontName}.eot?#iefix") format('embedded-opentype'),
  url("{$path}/@{fontName}.woff2") format('woff2'),
  url("{$path}/@{fontName}.woff") format('woff'),
  url("{$path}/@{fontName}.ttf") format('truetype'),
  url("{$path}/@{fontName}.svg#icons") format('svg')
;

LESS;

return $less;

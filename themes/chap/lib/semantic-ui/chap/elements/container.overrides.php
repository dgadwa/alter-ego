<?php
/*******************************
   Dynamic Container Overrides
*******************************/

namespace Chap\Options;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

$less = '';

$max = intval(get('sui_container_max_width'));
if($max <= 1200) {
	// There already is a container that large.
	return '';
}

$less .= <<<LESS

/**
 * Create new container and grid sizes.
 */
@media only screen and (min-width: @chapLargeMonitorBreakpoint) {
  .ui.container {
    width: @chapLargeMonitorWidth;
    margin-left: @chapLargeMonitorGutter !important;
    margin-right: @chapLargeMonitorGutter !important;
  }
  .ui.grid.container {
    width: @chapLargeMonitorGridWidth !important;
  }
  .ui.relaxed.grid.container {
    width: @chapLargeMonitorRelaxedGridWidth !important;
  }
  .ui.very.relaxed.grid.container {
    width: @chapLargeMonitorVeryRelaxedGridWidth !important;
  }
}

LESS;

return $less;

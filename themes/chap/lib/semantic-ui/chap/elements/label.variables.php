<?php
/*******************************
    Dynamic Label Variables
*******************************/

namespace Chap\Options;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

$less = '';

$font_weight = get('sui_label_font_bold') ? 'bold' : 'normal';

$less .= <<<LESS

@fontWeight: {$font_weight};

LESS;

return $less;

<?php
/*******************************
    Dynamic Button Variables
*******************************/

namespace Chap\Options;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

$less = '';

$vertical_margin    = floatval(get('sui_button_vertical_margin'));
$horizontal_margin  = floatval(get('sui_button_horizontal_margin'));

$vertical_padding   = intval(get('sui_button_vertical_padding'));
$horizontal_padding = floatval(get('sui_button_horizontal_padding'));

$font_weight = get('sui_button_font_bold') ? 'bold' : 'normal';

$less .= <<<LESS

/* Button */
@verticalMargin    : {$vertical_margin}em;
@horizontalMargin  : {$horizontal_margin}em;

@verticalPadding   : @relative{$vertical_padding}px;
@horizontalPadding : {$horizontal_padding}em;

/* Text */
@fontWeight: {$font_weight};

LESS;

return $less;

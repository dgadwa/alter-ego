<?php
/*******************************
     Dynamic Form Variables
*******************************/

namespace Chap\Options;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

$input_text = get('sui_input_text');
$input_background = get('sui_input_background');

$less = <<<LESS

/* Input */
@inputColor: {$input_text};

/* Select */
@selectBackground: {$input_background};

LESS;

return $less;

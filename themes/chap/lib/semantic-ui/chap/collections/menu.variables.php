<?php
/*******************************
     Dynamic Menu Variables
*******************************/

namespace Chap\Options;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

$less = '';

$submenu_font_size = ucfirst(get('sui_submenu_font_size'));

$less .= <<<LESS

@subMenuFontSize: @relative{$submenu_font_size};

LESS;

return $less;

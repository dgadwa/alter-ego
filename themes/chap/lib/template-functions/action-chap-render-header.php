<?php

namespace Chap\TemplateFunctions;
use Chap\Titles;
use Chap\Options;
use Chap\Helpers;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

if(!function_exists(__NAMESPACE__ . '\\chap_render_header_open')):
	/**
	 * Renders the header opening tag.
	 */
	function chap_render_header_open() {
		$header_classes = Helpers\fclassnames([
			'ui',
			'inverted' => Options\get('header_inverted'),
			'vertical',
			'center',
			'aligned',
			'masthead',
			'segment',
		], 'chap_header_classes');

		?>
		<div class="<?php echo esc_attr($header_classes); ?>">
		<?php
	}
endif;

if(!function_exists(__NAMESPACE__ . '\\chap_render_background_video')):
	/**
	 * Renders the header's background video.
	 */
	function chap_render_background_video() {
		$header_video = Options\get('header_background_video');
		if(empty($header_video)) {
			return;
		}

		$show_video = (is_front_page() || !Options\get('header_background_video_on_front_page'));
		if(!$show_video) {
			return;
		}

		if(is_numeric($header_video)) {
			$video_url = wp_get_attachment_url($header_video);
		} else {
			$video_url = $header_video;
		}

		if(strlen($video_url) > 0) {
			$basename = basename($video_url);
			$exploded = explode('.', $basename);
			$type = end($exploded);

			$src = '';
			switch($type) {
				case 'mp4':
					$src = "[{src:'$video_url',type:'video/mp4'}]";
				break;
				case 'webm':
					$src = "[{src:'$video_url',type:'video/webm;codecs=\"vp8,vorbis\"'}]";
				break;
			}

			if(strlen($src) > 0) {
				$mobile_query = Options\get('header_background_video_mobile') ? '' : "isMobile: window.matchMedia('(max-width: 768px)').matches,";
				$js = <<<JS
document.addEventListener('chap_ready', function(){
	chap.masthead_video = new Bideo();
	chap.masthead_video.init({
		videoEl: document.querySelector('#masthead_video'),
		container: document.querySelector('.ui.masthead.segment'),
		{$mobile_query}
		src: $src,
		resize: true,
	});
});
JS;

				if(wp_add_inline_script('chap/js', $js)) {
					?>
					<div class="ui vertical masthead-video segment">
						<video id="masthead_video" loop muted></video>
					</div>
					<?php
				}
			}
		}
	}
endif;

if(!function_exists(__NAMESPACE__ . '\\chap_render_header_template')):
	/**
	 * Renders header template based on header_template metabox option.
	 */
	function chap_render_header_template() {

		/**
		 * Header template from page/post metabox option.
		 */
		$header_template = Options\get('header_template', get_the_ID());

		/**
		 * Default header template from theme options.
		 */
		if($header_template === 'default') {
			$header_template = Options\get('header_default_template');
		}

		/**
		 * Hard coded default header template.
		 */
		if(empty($header_template)) {
			$header_template = 'header-branded';
		}

		get_template_part('templates/' . $header_template);

	}
endif;

if(!function_exists(__NAMESPACE__ . '\\chap_render_masthead_slider')):
	/**
	 * Renders the slider in masthead.
	 */
	function chap_render_masthead_slider() {
		do_action('chap_render_slider');
	}
endif;

if(!function_exists(__NAMESPACE__ . '\\chap_render_masthead_title')):
	/**
	 * Renders the masthead title.
	 */
	function chap_render_masthead_title() {
		if(is_front_page() && !is_home()) {
			return;
		}

		/**
		 * No title for chap_slide post type.
		 *
		 * Don't check on blog page, because of false positives.
		 */
		if(!is_home() && get_post_type() === 'chap_slide') {
			return;
		}

		/**
		 * @global $chap_theme
		 * @see class-chap.php
		 * @var array
		 */
		global $chap_theme;
		$chap_theme['masthead_title'] = Titles\title();

		if(!Titles\do_masthead_title()) {
			return;
		}

		$container_classes = Helpers\fclassnames([
			'ui',
			Options\get('masthead_title_alignment'),
			'aligned',
			'massive',
			'vertical',
			'title',
			'segment',
			'container',
		], 'chap_masthead_title_container_classes');

		$header_classes = Helpers\fclassnames([
			'ui',
			Options\get('masthead_title_size'),
			'inverted' => Options\get('header_inverted'),
			'header',
		], 'chap_masthead_title_header_classes');

		?>
		<div class="<?php echo esc_attr($container_classes); ?>">
			<div class="<?php echo esc_attr($header_classes); ?>">
				<?php echo wp_kses_post($chap_theme['masthead_title']); ?>
				<?php if(Titles\do_masthead_meta()): ?>
				<div class="meta sub header">
					<?php do_action('chap_render_entry_meta', Options\get('header_inverted'), false); ?>
				</div>
				<?php endif; ?>
			</div>
		</div>
		<?php
	}
endif;

if(!function_exists(__NAMESPACE__ . '\\chap_render_header_clip')):
	/**
	 * Renders the element used to clip the header.
	 *
	 * @since 1.1.5
	 */
	function chap_render_header_clip() {
		if((int)Options\get('header_bottom_angle') !== 0) {
			echo '<div class="ui vertical masthead-clip segment"></div>';
		}
	}
endif;

if(!function_exists(__NAMESPACE__ . '\\chap_render_header_close')):
	/**
	 * Renders the header closing tag.
	 */
	function chap_render_header_close() {
		?>
		</div>
		<?php
	}
endif;

<?php

namespace Chap\TemplateFunctions;
use Chap\Helpers;
use Chap\Options;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

if(!function_exists(__NAMESPACE__ . '\\chap_render_mobile_menu')):
	/**
	 * Render vertical offscreen menu.
	 */
	function chap_render_mobile_menu() {
		$theme_location = Options\get('sidebar_menu_separate') ? 'sidebar_navigation' : 'primary_navigation';
		if(!has_nav_menu($theme_location)) {
			return;
		}

		$brand_item_classes = Helpers\classnames('ui', ['inverted' => Options\get('sidebar_menu_inverted')], 'header', 'item');
		$brand_item = '<a class="' . esc_attr($brand_item_classes) . '" href="' . esc_url(home_url('/')) . '">' . get_bloginfo('name') . '</a>';
		$brand_item = apply_filters('chap_mobile_menu_brand', $brand_item);

		$mobile_menu_classes = Helpers\classnames([
			Options\get('sidebar_menu_size'),
			'vertical',
			'inverted' => Options\get('sidebar_menu_inverted'),
			'borderless' => Options\get('sidebar_menu_borderless'),
			'sidebar',
			'menu',
		]);

		wp_nav_menu([
			'theme_location' => $theme_location,
			'menu_id' => 'sidebar_menu',
			'menu_class' => apply_filters('chap_mobile_menu_classes', $mobile_menu_classes),
			'items_wrap' =>
				'<nav id="%1$s" class="%2$s" aria-label="' . esc_attr__('Sidebar menu', 'chap') . '">' .
					'<div role="menu">' .
						$brand_item . '%3$s' .
					'</div>' .
				'</nav>',
			'container' => '',
			'item_spacing' => 'discard',
		]);
	}
endif;

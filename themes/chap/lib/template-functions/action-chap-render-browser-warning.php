<?php

namespace Chap\TemplateFunctions;
use Chap\Options;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

if(!function_exists(__NAMESPACE__ . '\\chap_render_browser_warning')):
	/**
	 * Renders a notice for older browsers.
	 */
	function chap_render_browser_warning() {
		if(!Options\get('outdated_browser_warning')) {
			return;
		}

		$message = sprintf(
			/* Translators: HTML strong tags, HTML anchor tag to browsehappy.com. */
			esc_html__('You are using an %1$soutdated%2$s browser. Please %3$supgrade your browser%4$s to improve your experience.', 'chap'),
			'<strong>',
			'</strong>',
			'<a href="http://browsehappy.com/" target="_blank" rel="noopener">',
			'</a>'
		);

		?>
		<!--[if IE]><div class="ui browsehappy warning message"><?php echo wp_kses_post($message); ?></div><![endif]-->
		<?php
	}
endif;

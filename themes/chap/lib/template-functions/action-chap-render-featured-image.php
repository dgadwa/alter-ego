<?php

namespace Chap\TemplateFunctions;
use Chap\Options;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

if(!function_exists(__NAMESPACE__ . '\\chap_render_featured_image')):
	/**
	 * Renders featured image.
	 */
	function chap_render_featured_image() {
		if(!has_post_thumbnail()) {
			return;
		}

		$id = get_the_ID();

		/**
		 * Post/page specific featured image type.
		 */
		$type = Options\get('featured_image_position', $id);
		/**
		 * Default featured image type.
		 */
		$type = (empty($type) || $type === 'default') ? Options\get('featured_image_type') : $type;
		/**
		 * Featured image type filter.
		 */
		$type = apply_filters('chap_featured_image_type', $type, $id);

		/**
		 * Don't render featured image if type is set to 'hidden'.
		 */
		if($type === 'hidden') {
			return;
		}

		$large_image_url = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full');
		if(!empty($large_image_url[0])) {

			$url = esc_url($large_image_url[0]);
			$title = esc_attr(the_title_attribute(['echo' => false]));
			$thumbnail = get_the_post_thumbnail(null, 'medium');

			switch($type) {
				case 'left':
					printf(
						'<a class="ui left floated zoomable image" href="%1$s" title="%2$s">%3$s</a>',
						$url,
						$title,
						$thumbnail
					);
				break;

				case 'right':
					printf(
						'<a class="ui right floated zoomable image" href="%1$s" title="%2$s">%3$s</a>',
						$url,
						$title,
						$thumbnail
					);
				break;

				case 'full':
					printf(
						'<div class="ui basic fluid featured-image segment" style="background-image:url(%1$s);">
							<a class="zoomable" href="%1$s" title="%2$s">
								<img src="%1$s" alt="%2$s" />
							</a>
						</div>',
						$url,
						$title
					);
				break;

				default: return;
			}

		}

	}
endif;

<?php

namespace Chap\TemplateFunctions;
use Chap\Helpers;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

if(!class_exists('WP_Recipe_Maker')) {
	return;
}

if(!function_exists(__NAMESPACE__ . '\\chap_render_recipe_open')):
	/**
	 * Renders a recipe container opening tag.
	 *
	 * No itemprops on AMP pages, JSON-LD schema is used.
	 */
	function chap_render_recipe_open($recipe) {
		if(Helpers\is_amp()) {
			/**
			 * Load AMP styles.
			 */
			if(isset($GLOBALS['csc']) && isset($GLOBALS['csc']['amp-styles'])) {
				$GLOBALS['csc']['amp-styles']['header'] = true;
				$GLOBALS['csc']['amp-styles']['segment'] = true;
				$GLOBALS['csc']['amp-styles']['card'] = true;
				$GLOBALS['csc']['amp-styles']['button'] = true;
				$GLOBALS['csc']['amp-styles']['statistics'] = true;
				$GLOBALS['csc']['amp-styles']['statistic'] = true;
				$GLOBALS['csc']['amp-styles']['steps'] = true;
				$GLOBALS['csc']['amp-styles']['step'] = true;
				$GLOBALS['csc']['amp-styles']['list'] = true;
				$GLOBALS['csc']['amp-styles']['item'] = true;
				$GLOBALS['csc']['amp-styles']['divider'] = true;
				$GLOBALS['csc']['amp-styles']['recipe'] = true;
			}

			?>
			<div class="ui fluid chap-wprm-recipe container">
			<?php

			return;
		}

		?>
		<div class="ui fluid chap-wprm-recipe container" itemscope itemtype="http://schema.org/Recipe">
			<meta class="invisible" itemprop="author" content="<?php echo esc_attr($recipe->author_meta()); ?>" />
			<meta class="invisible" itemprop="datePublished" content="<?php echo esc_attr($recipe->date()); ?>" />
			<meta class="invisible" itemprop="image" content="<?php echo esc_attr($recipe->image_url('full')); ?>" />
		<?php
	}
endif;

if(!function_exists(__NAMESPACE__ . '\\chap_render_recipe_close')):
	/**
	 * Renders a recipe container closing tag.
	 */
	function chap_render_recipe_close($recipe) {
		?>
		</div>
		<?php
	}
endif;

if(!function_exists(__NAMESPACE__ . '\\chap_render_recipe_title')):
	/**
	 * Renders a recipe title.
	 */
	function chap_render_recipe_title($recipe) {
		?>
		<h1 class="ui large header" itemprop="name">
			<?php echo $recipe->name(); ?>
			<?php if($recipe->author()): ?>
			<span class="sub header">
				<?php echo \WPRM_Template_Helper::label('author'); ?>: <?php echo $recipe->author(); ?>
			</span>
			<?php endif; ?>
		</h1>
		<?php
	}
endif;

if(!function_exists(__NAMESPACE__ . '\\chap_render_recipe_card')):
	/**
	 * Renders a recipe card.
	 */
	function chap_render_recipe_card($recipe) {
		?>
		<div class="ui right floated recipe card segment">
			<div class="image"><?php echo \WPRM_Template_Helper::recipe_image($recipe, 'medium'); ?></div>
			<div class="content">
				<div class="header"><?php echo $recipe->name(); ?></div>
				<?php if($recipe->author()): ?>
				<div class="meta"><?php echo \WPRM_Template_Helper::label('author'); ?>: <?php echo $recipe->author(); ?></div>
				<?php endif; ?>
				<?php echo Helpers\is_amp() ? '' : $recipe->rating_stars(true); ?>
				<div class="ui list">
					<?php $taxonomies = \WPRM_Taxonomies::get_taxonomies(); ?>
					<?php foreach($taxonomies as $taxonomy => $options): ?>
						<?php $key = substr($taxonomy, 5); ?>
						<?php $terms = $recipe->tags($key); ?>
						<?php if(count($terms) > 0): ?>
						<div class="wprm-recipe-<?php echo $key; ?> item">
							<strong><?php echo \WPRM_Template_Helper::label($key . '_tags', $options['singular_name']); ?>:</strong>
							<?php
							foreach($terms as $index => $term):
								if(0 !== $index):
									echo ', ';
								endif;
								?><span <?php echo \WPRM_Template_Helper::tags_meta($key); ?>><?php echo esc_html(trim($term->name)); ?></span><?php
							endforeach;
							?>
						</div>
						<?php endif; ?>
					<?php endforeach; ?>
				</div>
			</div>
			<?php if(!Helpers\is_amp()): ?>
			<div class="ui bottom attached icon wprm-recipe-print button">
				<i class="print icon"></i>
				<?php echo \WPRM_Template_Helper::label('print_button'); ?>
			</div>
			<?php endif; ?>
		</div>
		<?php
	}
endif;

if(!function_exists(__NAMESPACE__ . '\\chap_render_recipe_summary')):
	/**
	 * Renders a recipe summary.
	 */
	function chap_render_recipe_summary($recipe) {
		?>
		<div itemprop="description">
			<?php echo $recipe->summary(); ?>
		</div>
		<?php
	}
endif;

if(!function_exists(__NAMESPACE__ . '\\chap_render_recipe_statistics')):
	/**
	 * Renders a recipe statistics.
	 */
	function chap_render_recipe_statistics($recipe, $small_size = 'small', $large_size = 'medium') {
		?>
		<div class="ui center aligned basic paddingless segment">
			<div class="ui amp-invisible hidden divider"></div>
			<div class="ui two <?php echo esc_html($small_size); ?> recipe statistics">
				<?php if($recipe->prep_time()): ?>
				<div class="ui statistic">
					<meta itemprop="prepTime" content="PT<?php echo $recipe->prep_time(); ?>M" />
					<div class="value"><?php echo $recipe->prep_time_formatted(true); ?></div>
					<div class="label"><?php echo \WPRM_Template_Helper::label('prep_time'); ?></div>
				</div>
				<?php endif; ?>
				<?php if($recipe->cook_time()): ?>
				<div class="ui statistic">
					<meta itemprop="cookTime" content="PT<?php echo $recipe->cook_time(); ?>M" />
					<div class="value"><?php echo $recipe->cook_time_formatted(true); ?></div>
					<div class="label"><?php echo \WPRM_Template_Helper::label('cook_time'); ?></div>
				</div>
				<?php endif; ?>
			</div>
			<div class="ui hidden timing divider"></div>
			<?php if($recipe->total_time()): ?>
			<div class="ui <?php echo esc_html($large_size); ?> recipe statistic">
				<meta itemprop="totalTime" content="PT<?php echo $recipe->total_time(); ?>M" />
				<div class="value">
					<?php if(shortcode_exists('csc-icon')): ?>
					<?php echo do_shortcode('[csc-icon wait]'); ?>
					<?php else: ?>
					<i class="wait icon"></i>
					<?php endif; ?>
					<?php echo $recipe->total_time_formatted(true); ?>
				</div>
				<div class="label"><?php echo \WPRM_Template_Helper::label('total_time'); ?></div>
			</div>
			<?php endif; ?>
			<div class="ui hidden amp-invisible timing divider"></div>
			<div class="ui two <?php echo esc_html($small_size); ?> recipe serving statistics">
				<?php if($recipe->servings()): ?>
				<div class="ui statistic">
					<div class="value" itemprop="recipeYield"><?php echo $recipe->servings(); ?> <?php echo $recipe->servings_unit(); ?></div>
					<div class="label"><?php echo \WPRM_Template_Helper::label('servings'); ?></div>
				</div>
				<?php endif; ?>
				<?php if($recipe->calories()): ?>
				<div class="ui statistic" itemprop="nutrition" itemscope itemtype="http://schema.org/NutritionInformation">
					<div class="value" itemprop="calories"><?php echo $recipe->calories(); ?> <?php esc_html_e('kcal', 'chap'); ?></div>
					<div class="label"><?php echo \WPRM_Template_Helper::label('calories'); ?></div>
				</div>
				<?php endif; ?>
			</div>
		</div>
		<?php
	}
endif;

if(!function_exists(__NAMESPACE__ . '\\chap_render_recipe_ingredients')):
	/**
	 * Renders a recipe ingredients.
	 */
	function chap_render_recipe_ingredients($recipe, $compact_segment = true) {
		$ingredients = $recipe->ingredients();
		if(count($ingredients) < 1) {
			return;
		}

		$segment_class = $compact_segment ? ' compact' : '';
		?>
		<h2 class="ui large header"><?php echo \WPRM_Template_Helper::label('ingredients'); ?></h2>
		<div class="ui large<?php echo esc_attr($segment_class); ?> stacked raised primary padded ingredients segment">
			<div class="ui ingredients list">
			<?php foreach($ingredients as $ingredient_group): ?>
				<div class="item">
					<div class="content">
						<?php if($ingredient_group['name']): ?>
						<?php $nested_list_type = ''; ?>
						<div class="header"><?php echo $ingredient_group['name']; ?></div>
						<?php else: ?>
						<?php $nested_list_type = ' ungrouped'; ?>
						<?php endif; ?>
						<div class="description">
							<div class="ui<?php echo esc_attr($nested_list_type); ?> list">
								<?php foreach($ingredient_group['ingredients'] as $ingredient): ?>
									<div class="item" itemprop="recipeIngredient">
										<?php if($ingredient['amount']): ?>
										<span class="amount"><?php echo esc_html($ingredient['amount']); ?></span>
										<?php endif; ?>
										<?php if($ingredient['unit']): ?>
										<span class="unit"><?php echo esc_html($ingredient['unit']); ?></span>
										<?php endif; ?>
										<span class="name"><?php echo esc_html(\WPRM_Template_Helper::ingredient_name($ingredient, true)); ?></span>
										<?php if($ingredient['notes']): ?>
										<span class="notes"><?php echo esc_html($ingredient['notes']); ?></span>
										<?php endif; ?>
									</div>
								<?php endforeach; ?>
							</div>
						</div>
					</div>
				</div>
			<?php endforeach; ?>
			</div>
			<?php echo \WPRM_Template_Helper::unit_conversion($recipe); ?>
		</div>
		<?php
	}
endif;

if(!function_exists(__NAMESPACE__ . '\\chap_render_recipe_instructions')):
	/**
	 * Renders a recipe instructions.
	 */
	function chap_render_recipe_instructions($recipe) {
		$instructions = $recipe->instructions();
		if(count($instructions) < 1) {
			return;
		}

		?>
		<div class="ui hidden section divider"></div>
		<h2 class="ui large header"><?php echo \WPRM_Template_Helper::label('instructions'); ?></h2>
		<?php foreach($instructions as $instruction_group): ?>
			<?php if($instruction_group['name']): ?>
			<h3 class="ui medium header"><?php echo esc_html($instruction_group['name']); ?></h3>
			<?php endif; ?>
			<div class="ui fluid vertical ordered recipe steps">
				<?php foreach($instruction_group['instructions'] as $instruction): ?>
				<?php if(Helpers\is_amp()): ?>
				<div class="step">
				<?php else: ?>
				<div class="active link step">
				<?php endif; ?>
					<div class="content">
						<?php if($instruction['text']): ?>
						<div class="description" itemprop="recipeInstructions"><?php echo wp_kses_post($instruction['text']); ?></div>
						<?php endif; ?>
						<?php if($instruction['image']): ?>
						<div class="ui tiny zoomable divided image"><?php echo \WPRM_Template_Helper::instruction_image($instruction, 'full'); ?></div>
						<?php endif; ?>
					</div>
				</div>
				<?php endforeach; ?>
			</div>
		<?php endforeach; ?>
		<?php
	}
endif;

if(!function_exists(__NAMESPACE__ . '\\chap_render_recipe_notes')):
	/**
	 * Renders a recipe notes.
	 */
	function chap_render_recipe_notes($recipe) {
		if(!$recipe->notes()) {
			return;
		}

		?>
		<div class="ui hidden section divider"></div>
		<h2 class="ui large header"><?php echo esc_html(\WPRM_Template_Helper::label('notes')); ?></h2>
		<?php echo wp_kses_post($recipe->notes()); ?>
		<?php
	}
endif;

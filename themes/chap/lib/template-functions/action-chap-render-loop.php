<?php

namespace Chap\TemplateFunctions;
use Chap\Helpers;
use Chap\Options;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

if(!function_exists(__NAMESPACE__ . '\\chap_render_loop_items_open')):
	/**
	 * Open an items loop.
	 *
	 * @since 1.0.2
	 */
	function chap_render_loop_items_open() {

		$classes = ['ui'];

		$spacing = Options\get('loop_spacing');
		if(in_array($spacing, ['relaxed', 'very relaxed'])) {
			$classes[] = $spacing;
		}

		$separation = Options\get('loop_separation');
		if(in_array($separation, ['divided'])) {
			$classes[] = $separation;
		}

		$classes[] = 'loop';
		$classes[] = 'items';

		echo Helpers\html_tag('div', [
			'classes' => $classes,
		]);

	}
endif;

if(!function_exists(__NAMESPACE__ . '\\chap_render_loop_grid_open')):
	/**
	 * Open a grid loop.
	 *
	 * @since 1.0.2
	 */
	function chap_render_loop_grid_open() {

		$classes = ['ui'];

		$columns = intval(Options\get('loop_columns'));
		if($columns > 0 && $columns < 7) {
			$classes[] = Helpers\number_to_word($columns);
			$classes[] = 'column';
		}

		$spacing = Options\get('loop_spacing');
		if(in_array($spacing, ['relaxed', 'very relaxed'])) {
			$classes[] = $spacing;
		}

		$padding = Options\get('loop_padding');
		if(in_array($padding, ['padded', 'very padded'])) {
			$classes[] = $padding;
		}

		$separation = Options\get('loop_separation');
		if(in_array($separation, ['divided', 'celled', 'internally celled'])) {
			if($separation === 'divided') {
				$separation = 'vertically divided';
			}
			$classes[] = $separation;
		}

		$classes[] = 'stackable';
		$classes[] = 'loop';
		$classes[] = 'grid';

		echo Helpers\html_tag('div', [
			'classes' => $classes,
		]);

		echo '<div class="row">';

	}
endif;

if(!function_exists(__NAMESPACE__ . '\\chap_render_loop_cards_open')):
	/**
	 * Open a cards loop.
	 *
	 * @since 1.0.2
	 */
	function chap_render_loop_cards_open() {

		$classes = ['ui'];

		$columns = intval(Options\get('loop_columns'));
		if($columns > 0 && $columns < 7) {
			$classes[] = Helpers\number_to_word($columns);
		}

		$classes[] = 'stackable';
		$classes[] = 'loop';
		$classes[] = 'cards';

		echo Helpers\html_tag('div', [
			'classes' => $classes,
		]);

	}
endif;

if(!function_exists(__NAMESPACE__ . '\\chap_render_loop_close')):
	/**
	 * Close a loop.
	 *
	 * @since 1.0.2
	 */
	function chap_render_loop_close() {

		/**
		 * Close grid row, reset variable.
		 */
		if(Options\get('loop_type') === 'grid') {
			echo '</div>';
			$GLOBALS['chap_grid']['rendered'] = 0;
		}

		/**
		 * Close loop.
		 */
		echo '</div>';

	}
endif;

if(!function_exists(__NAMESPACE__ . '\\chap_the_content_more_link')):
	/**
	 * Text replacing <!--more--> in post content in loops that show content.
	 *
	 * @since 1.0.3
	 */
	function chap_the_content_more_link($link) {
		return esc_html_x('...', 'Text replacing <!--more--> tag in excerpts', 'chap');
	}
endif;

if(!function_exists(__NAMESPACE__ . '\\chap_pre_get_posts')):
	/**
	 * Override posts page loop configuration.
	 *
	 * @since 1.0.3
	 */
	function chap_pre_get_posts($query) {

		if(is_home() && Options\get('loop_override_front_page')) {

			add_filter('chap_option_loop_type', function($type) {
				if(is_main_query()) {
					return 'grid';
				}
				return $type;
			});

			add_filter('chap_option_loop_columns', function($columns) {
				if(is_main_query()) {
					return 1;
				}
				return $columns;
			});

			add_filter('chap_option_loop_separation', function($columns) {
				if(is_main_query()) {
					return 'divided';
				}
				return $columns;
			});

			add_filter('chap_option_loop_spacing', function($spacing) {
				if(is_main_query()) {
					return '';
				}
				return $spacing;
			});

			add_filter('chap_option_loop_padding', function($padding) {
				if(is_main_query()) {
					return '';
				}
				return $padding;
			});

			add_filter('chap_option_loop_thumbnail_size', function($size) {
				if(is_main_query()) {
					return 'large';
				}
				return $size;
			});

			add_filter('chap_option_loop_render_entry_meta', function($toggle) {
				if(is_main_query()) {
					return true;
				}
				return $toggle;
			});

			add_filter('chap_option_loop_render_extras', function($toggle) {
				if(is_main_query()) {
					return true;
				}
				return $toggle;
			});

			add_filter('chap_option_loop_render_read_more', function($toggle) {
				if(is_main_query()) {
					return true;
				}
				return $toggle;
			});

			$blog_excerpt_length = function() {
				return 999999;
			};
			add_filter('excerpt_length', $blog_excerpt_length);

		}

	}
endif;

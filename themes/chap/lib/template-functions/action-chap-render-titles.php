<?php

namespace Chap\TemplateFunctions;
use Chap\Titles;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

if(!function_exists(__NAMESPACE__ . '\\chap_render_page_title')):
	/**
	 * Renders page title, if not already rendered in masthead.
	 */
	function chap_render_page_title() {

		/**
		 * Contains the title that is displayed in the masthead, if any.
		 *
		 * @global $chap_theme
		 * @see class-chap.php
		 * @var array
		 */
		global $chap_theme;
		$chap_masthead_title = $chap_theme['masthead_title'];

		/**
		 * Heading to be displayed on the page.
		 */
		$the_title = Titles\title();

		/**
		 * Whether masthead title is rendered or not.
		 * Returns true if Header -> Title in masthead option
		 * is enabled, except when the page has slides.
		 */
		$do_masthead_title = Titles\do_masthead_title();

		/**
		 * On front page title is not displayed.
		 * On pages with full width template title is only displayed in masthead.
		 */
		if(is_front_page() || (is_page_template('template-full-width.php') && !$do_masthead_title)) {

			/**
			 * Render an invisible title for W3C compliance.
			 */
			?>
			<h1 class="invisible"><?php echo wp_kses_post($the_title); ?></h1>
			<?php

			return;

		}

		/**
		 * Page title has been modified since rendering the page header.
		 * This happens when there is a secondary title, such as WooCommerce My Account page
		 * can have secondary titles such as "Orders", "Downloads", "Addresses" etc.
		 *
		 * We're handling this by printing the original title in the masthead and secondary
		 * in the page. Or when masthead title is disabled we use a header with a
		 * subheader in the page.
		 */
		if(!empty($chap_masthead_title) && $chap_masthead_title !== $the_title) {

			/**
			 * Render an invisible original title in the page context for W3C compliance.
			 */
			?>
			<h1 class="invisible"><?php echo wp_kses_post($chap_masthead_title); ?></h1>
			<?php

			if($do_masthead_title) {

				/**
				 * Show the subtitle as a huge header as the main title is in the masthead.
				 */
				?>
				<h2 class="ui huge header"><?php echo wp_kses_post($the_title); ?></h2>
				<?php

			} else {

				/**
				 * Render secondary title as invisible for W3C compliance.
				 * Render the visible main title and secondary title.
				 */
				?>
				<h2 class="invisible"><?php echo wp_kses_post($the_title); ?></h2>
				<div class="ui huge header">
					<?php echo wp_kses_post($chap_masthead_title); ?>
					<div class="ui sub header"><?php echo wp_kses_post($the_title); ?></div>
				</div>
				<?php

			}

			return;

		}

		/**
		 * Render the standard page title.
		 */
		if($do_masthead_title) {

			/**
			 * Title already rendered in masthead, render an invisible title in the page context for W3C compliance.
			 */
			?>
			<h1 class="invisible"><?php echo wp_kses_post($the_title); ?></h1>
			<?php

		} else {

			?>
			<h1 class="ui huge header"><?php echo wp_kses_post($the_title); ?></h1>
			<?php

		}

	}
endif;

if(!function_exists(__NAMESPACE__ . '\\chap_render_post_title')):
	/**
	 * Renders post title and meta, if not already rendered in masthead.
	 */
	function chap_render_post_title() {
		/**
		 * Render invisible post title and meta for search engines.
		 */
		?>
		<h1 class="invisible entry-title"><?php echo wp_kses_post(Titles\title()); ?></h1>
		<div class="invisible"><?php do_action('chap_render_entry_meta', false, true); ?></div>
		<?php

		/**
		 * Title already printed in masthead.
		 */
		if(Titles\do_masthead_title()) {
			return;
		}

		/**
		 * Render visible post title and meta.
		 */
		?>
		<div class="ui huge header">
			<?php echo wp_kses_post(Titles\title()); ?>
			<div class="meta sub header">
				<?php do_action('chap_render_entry_meta'); ?>
			</div>
		</div>
		<?php
	}
endif;

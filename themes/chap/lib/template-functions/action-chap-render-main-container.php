<?php

namespace Chap\TemplateFunctions;
use Chap\Helpers;
use Chap\Options;
use Chap\Wrapper;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

if(!function_exists(__NAMESPACE__ . '\\get_page_type')):
	/**
	 * Returns which type of page to display.
	 *
	 *   0 - page with no sidebars (but contained to a reasonable max width)
	 *   1 - primary sidebar + page
	 *   2 - page + primary sidebar
	 *   3 - primary sidebar + page + secondary sidebar
	 *   4 - secondary sidebar + page + primary sidebar
	 *   5 - full width page
	 *
	 * @return int Page type.
	 */
	function get_page_type() {

		/**
		 * Load options.
		 */
		$sidebars_disabled        = Options\get('sidebar_disable_all');
		$sidebars_disabled_front  = Options\get('sidebar_disable_front');
		$primary_sidebar_position = Options\get('sidebar_primary_position');
		$show_secondary_sidebar   = Options\get('sidebar_secondary_enable');

		/**
		 * Override disabled sidebars with templates.
		 */
		if($sidebars_disabled || (is_front_page() && $sidebars_disabled_front)) {

			/**
			 * Full width themplate.
			 */
			if(is_page_template('template-full-width.php')) {
				return 5;
			}

			/**
			 * One sidebar template.
			 */
			if(is_page_template('template-sidebar-one.php')) {
				if($primary_sidebar_position == 'left') {
					return 1;
				} else {
					return 2;
				}
			}

			/**
			 * Two sidebars template.
			 */
			if(is_page_template('template-sidebar-two.php')) {
				if($primary_sidebar_position == 'left') {
					return 3;
				} else {
					return 4;
				}
			}

			/**
			 * Page with no sidebars.
			 */
			return 0;

		}

		/**
		 * Choose sidebars based on template,
		 * their positions based on settings.
		 */
		if(is_page_template('template-sidebar-none.php')) {
			return 0;
		}
		if(is_page_template('template-sidebar-one.php')) {
			if($primary_sidebar_position == 'left') {
				return 1;
			} else {
				return 2;
			}
		}
		if(is_page_template('template-sidebar-two.php')) {
			if($primary_sidebar_position == 'left') {
				return 3;
			} else {
				return 4;
			}
		}
		if(is_page_template('template-full-width.php')) {
			return 5;
		}
		if(is_page_template('template-text.php')) {
			return 0;
		}

		/**
		 * Pages that don't show sidebar.
		 */
		if(in_array(true, [is_404()])) {
			return 0;
		}

		/**
		 * Use settings.
		 */
		if(!$show_secondary_sidebar) {
			if($primary_sidebar_position == 'left') {
				return 1;
			} else {
				return 2;
			}
		} else {
			if($primary_sidebar_position == 'left') {
				return 3;
			} else {
				return 4;
			}
		}

		/**
		 * This point shouldn't be reached.
		 */
		return 0;

	}
endif;

if(!function_exists(__NAMESPACE__ . '\\get_column_width')):
	/**
	 * Returns the width of a container.
	 *
	 * Containers:
	 *  - 'primary'   (Primary sidebar)
	 *  - 'secondary' (Secondary sidebar)
	 *  - 'main_one'  (Main container with 1 sidebar)
	 *  - 'main_two'  (Main container with 2 sidebars)
	 */
	function get_column_width($container) {

		/**
		 * Get options.
		 */
		$primary_sidebar_width = Options\get('primary_sidebar_width');
		if(empty($primary_sidebar_width)) {
			$primary_sidebar_width = 4;
		}

		$secondary_sidebar_width = Options\get('secondary_sidebar_width');
		if(empty($secondary_sidebar_width)) {
			$secondary_sidebar_width = 4;
		}

		/**
		 * Calculate widths.
		 */
		$main_width_one_sidebar  = 16 - $primary_sidebar_width;
		$main_width_two_sidebars = 16 - $primary_sidebar_width - $secondary_sidebar_width;

		/**
		 * Transform to Semantic UI syntax.
		 */
		$sui_width = [
			'primary'   => Helpers\number_to_word($primary_sidebar_width),
			'secondary' => Helpers\number_to_word($secondary_sidebar_width),
			'main_one'  => Helpers\number_to_word($main_width_one_sidebar),
			'main_two'  => Helpers\number_to_word($main_width_two_sidebars),
		];

		return $sui_width[$container];

	}
endif;

if(!function_exists(__NAMESPACE__ . '\\chap_render_main_container')):
	/**
	 * Render main container.
	 */
	function chap_render_main_container() {

		$page_type = get_page_type();

		/**
		 * Render full width page without any containers.
		 */
		if($page_type === 5) {
			do_action('chap_render_full_width_page');
			return;
		}

		?>
		<div class="ui main stackable grid container">
			<?php
			switch($page_type):
				case 1: do_action('chap_render_primary_sidebar_page');
				break;
				case 2: do_action('chap_render_page_primary_sidebar');
				break;
				case 3: do_action('chap_render_primary_sidebar_page_secondary_sidebar');
				break;
				case 4: do_action('chap_render_secondary_sidebar_page_primary_sidebar');
				break;
				default: do_action('chap_render_page');
			endswitch;
			?>
		</div>
		<?php
	}
endif;

if(!function_exists(__NAMESPACE__ . '\\chap_render_page')):
	/**
	 * Render page with no sidebars (contained to a reasonable max width).
	 */
	function chap_render_page() {
		?>
		<main class="sixteen wide column">
			<?php include Wrapper\template_path(); ?>
		</main>
		<?php
	}
endif;

if(!function_exists(__NAMESPACE__ . '\\chap_render_primary_sidebar_page')):
	/**
	 * Renders primary sidebar and page.
	 */
	function chap_render_primary_sidebar_page() {
		?>
		<aside id="chap-primary-sidebar" class="<?php echo get_column_width('primary'); ?> wide sidebar column">
			<?php include Wrapper\sidebar_path(); ?>
		</aside>
		<main class="<?php echo get_column_width('main_one'); ?> wide column">
			<?php include Wrapper\template_path(); ?>
		</main>
		<?php
	}
endif;

if(!function_exists(__NAMESPACE__ . '\\chap_render_page_primary_sidebar')):
	/**
	 * Renders page and primary sidebar.
	 */
	function chap_render_page_primary_sidebar() {
		?>
		<main class="<?php echo get_column_width('main_one'); ?> wide column">
			<?php include Wrapper\template_path(); ?>
		</main>
		<aside id="chap-primary-sidebar" class="<?php echo get_column_width('primary'); ?> wide sidebar column">
			<?php include Wrapper\sidebar_path(); ?>
		</aside>
		<?php
	}
endif;

if(!function_exists(__NAMESPACE__ . '\\chap_render_primary_sidebar_page_secondary_sidebar')):
	/**
	 * Renders primary sidebar, page and secondary sidebar.
	 */
	function chap_render_primary_sidebar_page_secondary_sidebar() {
		?>
		<aside id="chap-primary-sidebar" class="<?php echo get_column_width('primary'); ?> wide sidebar column">
			<?php include Wrapper\sidebar_path(); ?>
		</aside>
		<main class="<?php echo get_column_width('main_two'); ?> wide column">
			<?php include Wrapper\template_path(); ?>
		</main>
		<aside id="chap-secondary-sidebar" class="<?php echo get_column_width('secondary'); ?> wide sidebar column">
			<?php include Wrapper\secondary_sidebar_path(); ?>
		</aside>
		<?php
	}
endif;

if(!function_exists(__NAMESPACE__ . '\\chap_render_secondary_sidebar_page_primary_sidebar')):
	/**
	 * Renders secondary sidebar, page and primary sidebar.
	 */
	function chap_render_secondary_sidebar_page_primary_sidebar() {
		?>
		<aside id="chap-secondary-sidebar" class="<?php echo get_column_width('secondary'); ?> wide sidebar column">
			<?php include Wrapper\secondary_sidebar_path(); ?>
		</aside>
		<main class="<?php echo get_column_width('main_two'); ?> wide column">
			<?php include Wrapper\template_path(); ?>
		</main>
		<aside id="chap-primary-sidebar" class="<?php echo get_column_width('primary'); ?> wide sidebar column">
			<?php include Wrapper\sidebar_path(); ?>
		</aside>
		<?php
	}
endif;

if(!function_exists(__NAMESPACE__ . '\\chap_render_full_width_page')):
	/**
	 * Renders full width page, not constrained by any width.
	 */
	function chap_render_full_width_page() {
		?>
		<main>
			<?php include Wrapper\template_path(); ?>
		</main>
		<?php
	}
endif;

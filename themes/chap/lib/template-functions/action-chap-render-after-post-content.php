<?php

namespace Chap\TemplateFunctions;
use Chap\Options;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

if(!function_exists(__NAMESPACE__ . '\\chap_post_pagination')):
	/**
	 * Render paginated posts pagination.
	 *
	 * @since 1.1.1
	 */
	function chap_post_pagination() {
		wp_link_pages();
	}
endif;

if(!function_exists(__NAMESPACE__ . '\\chap_post_meta')):
	/**
	 * Render post categories and tags below post.
	 *
	 * @since 1.1.1
	 */
	function chap_post_meta() {
		?>
		<div class="ui hidden divider"></div>
		<div class="ui right floated basic paddingless marginless segment">
			<?php chap_sticky_post_label(); ?>
			<?php chap_category_labels(); ?>
			<?php chap_tag_labels(); ?>
		</div>
		<div class="ui hidden clearing divider"></div>
		<?php
	}
endif;

if(!function_exists(__NAMESPACE__ . '\\chap_post_author')):
	/**
	 * Render post author info below post.
	 *
	 * @since 1.1.1
	 */
	function chap_post_author() {
		$author = get_the_author();
		if(!$author) {
			return;
		}

		$author_posts = get_author_posts_url(get_the_author_meta('ID'));
		$author_post_count = count_user_posts(get_the_author_meta('ID'));
		$author_email = get_the_author_meta('user_email');
		$author_url = get_the_author_meta('user_url');

		?>
		<div class="ui hidden divider"></div>
		<div class="ui primary secondary author segment">
			<div class="ui marginless items">
				<div class="item">
					<div class="ui tiny image"><?php echo get_avatar($author); ?></div>
					<div class="middle aligned content">
						<div class="header"><?php the_author_meta('display_name'); ?></div>
						<div class="marginless meta">
							<span><?php esc_html_e('Posted on:', 'chap'); ?></span>
							<time datetime="<?php echo esc_attr(get_post_time('c', true)); ?>"><?php echo get_the_date(); ?></time>
						</div>
						<div class="description">
							<?php echo apply_filters('chap_author_bio', get_the_author_meta('description')); ?>
						</div>
						<div class="extra">
							<div class="ui horizontal link list">
								<?php if($author_posts): ?>
								<a class="item" href="<?php echo esc_url($author_posts); ?>">
									<i class="archive icon"></i>
									<?php
										/* Translators: %s = author post count number. */
										printf(esc_html__('%s posts', 'chap'), $author_post_count);
									?>
								</a>
								<?php endif; ?>
								<?php if($author_email): ?>
								<a class="item" href="mailto:<?php echo esc_attr($author_email); ?>">
									<i class="mail icon"></i>
									<?php esc_html_e('E-mail', 'chap'); ?>
								</a>
								<?php endif; ?>
								<?php if($author_url): ?>
								<a class="item" href="<?php echo esc_url($author_url); ?>">
									<i class="linkify icon"></i>
									<?php esc_html_e('Website', 'chap'); ?>
								</a>
								<?php endif; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="ui small primary bottom right attached label">
				<div class="marginless sub header"><?php esc_html_e('Post author', 'chap'); ?></div>
			</div>
		</div>
		<?php
	}
endif;

if(!function_exists(__NAMESPACE__ . '\\chap_after_post_content')):
	/**
	 * Render after post content.
	 *
	 * @since 1.0.7
	 */
	function chap_after_post_content() {
		echo do_shortcode(Options\get('after_post_content'));
	}
endif;

if(!function_exists(__NAMESPACE__ . '\\chap_after_page_content')):
	/**
	 * Render after page content.
	 *
	 * @since 1.0.7
	 */
	function chap_after_page_content() {
		if(is_front_page() && Options\get('after_page_content_front_page_disabled')) {
			return;
		}
		echo do_shortcode(Options\get('after_page_content'));
	}
endif;

if(!function_exists(__NAMESPACE__ . '\\chap_post_navigation')):
	/**
	 * Render previous/next post links below post.
	 *
	 * @since 1.1.1
	 */
	function chap_post_navigation() {
		get_template_part('templates/nav', 'post');
	}
endif;

if(!function_exists(__NAMESPACE__ . '\\chap_post_comments')):
	/**
	 * Render post comments.
	 *
	 * @since 1.1.1
	 */
	function chap_post_comments() {
		comments_template('/templates/comments.php');
	}
endif;

if(!function_exists(__NAMESPACE__ . '\\chap_page_comments')):
	/**
	 * Render page comments.
	 *
	 * @since 1.1.1
	 */
	function chap_page_comments() {
		comments_template('/templates/comments.php');
	}
endif;

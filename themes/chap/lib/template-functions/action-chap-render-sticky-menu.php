<?php

namespace Chap\TemplateFunctions;
use Chap\Helpers;
use Chap\Options;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

if(!function_exists(__NAMESPACE__ . '\\chap_render_sticky_menu')):
	/**
	 * Renders the sticky menu.
	 */
	function chap_render_sticky_menu() {
		$sticky_menu_enabled = Options\get('sticky_menu_enabled');
		if(!$sticky_menu_enabled) {
			return;
		}

		$theme_location = Options\get('sticky_menu_separate') ? 'sticky_navigation' : 'primary_navigation';
		if(!has_nav_menu($theme_location)) {
			return;
		}

		$toc_item = '<a class="toc item"><i class="sidebar icon"></i></a>';
		$brand_item = Options\get('sticky_menu_brand') ? '<a class="ui header item" href="' . esc_url(home_url('/')) . '">' . get_bloginfo('name') . '</a>' : '';
		$brand_item = apply_filters('chap_sticky_menu_brand', $brand_item);

		$sticky_menu_classes = Helpers\classnames([
			Options\get('sticky_menu_size'),
			'inverted' => Options\get('sticky_menu_inverted'),
			'borderless' => Options\get('sticky_menu_borderless'),
			'top',
			'fixed',
			'hidden',
			'menu',
		]);

		$container_classes = Helpers\classnames([
			'ui',
			'fluid' => Options\get('sticky_menu_full_width'),
			'container',
		]);

		wp_nav_menu([
			'theme_location' => $theme_location,
			'menu_class' => apply_filters('chap_sticky_menu_classes', $sticky_menu_classes),
			'menu_id' => 'sticky_menu',
			'items_wrap' =>
				'<nav aria-label="' . esc_attr__('Sticky menu', 'chap') . '">' .
					'<div id="%1$s" class="%2$s" role="menubar">' .
						'<div class="' . esc_attr($container_classes) . '">' .
							$toc_item .
							$brand_item .
							'%3$s' .
						'</div>' .
					'</div>' .
				'</nav>',
			'container' => '',
		]);
	}
endif;

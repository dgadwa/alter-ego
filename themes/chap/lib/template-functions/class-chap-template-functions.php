<?php

namespace Chap\TemplateFunctions;
use Chap\Assets;
use Chap\Helpers;
use Chap\Options;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

/**
 * Template functions output HTML based on conditions and calculations.
 *
 * @class   Chap_Template_Functions
 * @version 1.1.7
 * @author  websevendev <websevendev@gmail.com>
 */
class Chap_Template_Functions {

	/**
	 * Init Chap_Template_Functions class.
	 *
	 * @since 1.0.0
	 */
	public static function init() {

		/**
		 * Don't need template functions in the Admin area.
		 *
		 * @since 1.0.5
		 */
		if(is_admin()) {
			return;
		}

		/**
		 * Load files.
		 */
		add_action('after_setup_theme', [__CLASS__, 'load']);

		/**
		 * General filters.
		 */
		add_filter('the_content', [__CLASS__, 'the_content']);
		add_filter('the_password_form', [__CLASS__, 'the_password_form']);
		add_filter('excerpt_more', [__CLASS__, 'excerpt_more']);
		add_filter('wp_link_pages_args', [__CLASS__, 'wp_link_pages_args']);
		add_filter('wp_link_pages_link', [__CLASS__, 'wp_link_pages_link'], 10, 2);
		add_filter('comment_form_default_fields', [__CLASS__, 'comment_form_default_fields']);
		add_filter('comment_form_submit_button', [__CLASS__, 'comment_form_submit_button']);
		add_filter('previous_comments_link_attributes', [__CLASS__, 'comment_link_attributes']);
		add_filter('next_comments_link_attributes', [__CLASS__, 'comment_link_attributes']);
		add_filter('comment_form_defaults', [__CLASS__, 'comment_form_defaults']);

		/**
		 * Main container.
		 */
		add_action('chap_render_page', __NAMESPACE__ . '\\chap_render_page');
		add_action('chap_render_full_width_page', __NAMESPACE__ . '\\chap_render_full_width_page');
		add_action('chap_render_primary_sidebar_page', __NAMESPACE__ . '\\chap_render_primary_sidebar_page');
		add_action('chap_render_page_primary_sidebar', __NAMESPACE__ . '\\chap_render_page_primary_sidebar');
		add_action('chap_render_primary_sidebar_page_secondary_sidebar', __NAMESPACE__ . '\\chap_render_primary_sidebar_page_secondary_sidebar');
		add_action('chap_render_secondary_sidebar_page_primary_sidebar', __NAMESPACE__ . '\\chap_render_secondary_sidebar_page_primary_sidebar');
		add_action('chap_render_main_container', __NAMESPACE__ . '\\chap_render_main_container');

		/**
		 * Header.
		 */
		add_action('chap_render_header', __NAMESPACE__ . '\\chap_render_header_open',      10);
		add_action('chap_render_header', __NAMESPACE__ . '\\chap_render_background_video', 20);
		add_action('chap_render_header', __NAMESPACE__ . '\\chap_render_header_template',  30);
		add_action('chap_render_header', [__CLASS__, 'chap_before_masthead_content'],      40);
		add_action('chap_render_header', __NAMESPACE__ . '\\chap_render_masthead_slider',  50);
		add_action('chap_render_header', __NAMESPACE__ . '\\chap_render_masthead_title',   60);
		add_action('chap_render_header', [__CLASS__, 'chap_after_masthead_content'],       70);
		add_action('chap_render_header', __NAMESPACE__ . '\\chap_render_header_clip',      75);
		add_action('chap_render_header', __NAMESPACE__ . '\\chap_render_header_close',     80);

		/**
		 * Loop.
		 *
		 * @since 1.0.2
		 */
		add_action('chap_render_loop_items_open', __NAMESPACE__ . '\\chap_render_loop_items_open');
		add_action('chap_render_loop_grid_open', __NAMESPACE__ . '\\chap_render_loop_grid_open');
		add_action('chap_render_loop_cards_open', __NAMESPACE__ . '\\chap_render_loop_cards_open');
		add_action('chap_render_loop_close', __NAMESPACE__ . '\\chap_render_loop_close');
		add_action('pre_get_posts', [__CLASS__, 'chap_pre_get_posts']);
		add_filter('the_content_more_link', __NAMESPACE__ . '\\chap_the_content_more_link');

		/**
		 * Menus.
		 */
		add_action('chap_render_main_menu_container', __NAMESPACE__ . '\\chap_render_main_menu_container');
		add_action('chap_render_main_menu', __NAMESPACE__ . '\\chap_render_main_menu', 10, 1);
		add_action('chap_render_mobile_menu', __NAMESPACE__ . '\\chap_render_mobile_menu');
		add_action('chap_render_sticky_menu', __NAMESPACE__ . '\\chap_render_sticky_menu');
		add_filter('chap_utility_menu_classes', [__CLASS__, 'chap_utility_menu_classes'], 10, 1);

		/**
		 * Misc.
		 */
		add_action('chap_render_featured_image', __NAMESPACE__ . '\\chap_render_featured_image');
		add_action('chap_render_browser_warning', __NAMESPACE__ . '\\chap_render_browser_warning');
		add_action('chap_render_posts_navigation', __NAMESPACE__ . '\\chap_render_posts_navigation', 10, 1);
		add_action('chap_render_loading_screen', [__CLASS__, 'chap_render_loading_screen']);

		/**
		 * Brand.
		 */
		add_action('chap_render_branding_container', __NAMESPACE__ . '\\chap_render_branding_container');
		add_action('chap_render_brand', __NAMESPACE__ . '\\chap_render_brand', 10, 1);

		/**
		 * Slider.
		 */
		add_action('wp', __NAMESPACE__ . '\\chap_load_slides', 5); // Load the slides before AMP template initializes so they can be sanitized.
		add_action('chap_render_slider', __NAMESPACE__ . '\\chap_render_slider', 10, 1);
		add_action('chap_render_slides', __NAMESPACE__ . '\\chap_render_slides', 10, 1);
		add_action('chap_render_slide', __NAMESPACE__ . '\\chap_render_slide');
		add_action('chap_render_slide_open', __NAMESPACE__ . '\\chap_render_slide_open');
		add_action('chap_render_slide_close', __NAMESPACE__ . '\\chap_render_slide_close');
		add_action('chap_render_slide_background', __NAMESPACE__ . '\\chap_render_slide_background');
		add_action('chap_render_edit_slide', __NAMESPACE__ . '\\chap_render_edit_slide');
		add_action('chap_init_slider', __NAMESPACE__ . '\\chap_init_slider', 10, 1);

		/**
		 * Titles.
		 */
		add_action('chap_render_page_title', __NAMESPACE__ . '\\chap_render_page_title');
		add_action('chap_render_post_title', __NAMESPACE__ . '\\chap_render_post_title');

		/**
		 * Meta.
		 */
		add_action('chap_render_entry_meta', __NAMESPACE__ . '\\chap_render_post_meta', 10, 2);

		/**
		 * Extras.
		 */
		add_action('chap_render_extras', __NAMESPACE__ . '\\chap_read_more', 10);
		add_action('chap_render_extras', __NAMESPACE__ . '\\chap_sticky_post_label', 20);
		add_action('chap_render_extras', __NAMESPACE__ . '\\chap_category_labels', 30);
		add_action('chap_render_extras', __NAMESPACE__ . '\\chap_tag_labels', 40);

		/**
		 * Extras in a card.
		 *
		 * @since 1.0.6
		 */
		add_action('chap_render_card_extras', __NAMESPACE__ . '\\chap_card_extra_content_open', 5);
		add_action('chap_render_card_extras', __NAMESPACE__ . '\\chap_sticky_post_label', 10);
		add_action('chap_render_card_extras', __NAMESPACE__ . '\\chap_category_labels', 20);
		add_action('chap_render_card_extras', __NAMESPACE__ . '\\chap_tag_labels', 30);
		add_action('chap_render_card_extras', __NAMESPACE__ . '\\chap_card_extra_content_close', 35);
		add_action('chap_render_card_extras', __NAMESPACE__ . '\\chap_card_read_more', 40);

		/**
		 * Footer.
		 */
		add_action('chap_render_footer', __NAMESPACE__ . '\\chap_render_footer_open', 10);
		add_action('chap_render_footer', __NAMESPACE__ . '\\chap_render_footer_widgets', 20);
		add_action('chap_render_footer', __NAMESPACE__ . '\\chap_render_footer_divider', 30);
		add_action('chap_render_footer', __NAMESPACE__ . '\\chap_render_footer_image', 40);
		add_action('chap_render_footer', __NAMESPACE__ . '\\chap_render_footer_menu', 50);
		add_action('chap_render_footer', __NAMESPACE__ . '\\chap_render_footer_text', 60);
		add_action('chap_render_footer', __NAMESPACE__ . '\\chap_render_footer_close', 70);

		/**
		 * After post content.
		 *
		 * @since 1.1.1
		 */
		add_action('init', [__CLASS__, 'the_after_page_actions']);
		add_action('init', [__CLASS__, 'the_after_post_actions']);

		/**
		 * Additional content.
		 *
		 * @since 1.0.7
		 */
		add_action('chap_before_header_content', [__CLASS__, 'chap_before_header_content']);
		// before_masthead_content defined above in chap_render_header action.
		// after_masthead_content defined above in chap_render_header action.
		// after_page_content added by the_after_page_actions below in this class.
		// after_post_content added by the_after_post_actions below in this class.
		add_action('chap_before_footer_content', [__CLASS__, 'chap_before_footer_content']);
		add_action('chap_after_footer_content', [__CLASS__, 'chap_after_footer_content']);

		/**
		 * WP Recipe Maker template actions.
		 *
		 * @since 1.1.2
		 */
		if(class_exists('WP_Recipe_Maker')) {
			add_action('chap_render_recipe_open', __NAMESPACE__ . '\\chap_render_recipe_open', 10, 1);
			add_action('chap_render_recipe_title', __NAMESPACE__ . '\\chap_render_recipe_title', 10, 1);
			add_action('chap_render_recipe_card', __NAMESPACE__ . '\\chap_render_recipe_card', 10, 1);
			add_action('chap_render_recipe_summary', __NAMESPACE__ . '\\chap_render_recipe_summary', 10, 1);
			add_action('chap_render_recipe_statistics', __NAMESPACE__ . '\\chap_render_recipe_statistics', 10, 3);
			add_action('chap_render_recipe_ingredients', __NAMESPACE__ . '\\chap_render_recipe_ingredients', 10, 2);
			add_action('chap_render_recipe_instructions', __NAMESPACE__ . '\\chap_render_recipe_instructions', 10, 1);
			add_action('chap_render_recipe_notes', __NAMESPACE__ . '\\chap_render_recipe_notes', 10, 1);
			add_action('chap_render_recipe_close', __NAMESPACE__ . '\\chap_render_recipe_close', 10, 1);
		}

	}

	/**
	 * Load all template function files.
	 *
	 * @since 1.0.1
	 */
	public static function load() {
		foreach(Assets\theme_files('lib/template-functions', 'action-*.php') as $file) {
			include_once $file;
		}
	}

	/**
	 * Filter the final output.
	 */
	public static function the_content($content) {
		/**
		 * Add Semantic UI classes to headers and tables.
		 * Mark paragraphs that are solely used for line breaks.
		 */
		return str_replace(
			[
				'<h1>',
				'<h2>',
				'<h3>',
				'<h4>',
				'<h5>',
				'<table>',
				'<p>&nbsp;</p>',
			],
			[
				'<h1 class="ui header">',
				'<h2 class="ui header">',
				'<h3 class="ui header">',
				'<h4 class="ui header">',
				'<h5 class="ui header">',
				'<table class="ui celled table">',
				'<p class="empty-line">&nbsp;</p>',
			],
			$content
		);
	}

	/**
	 * Render the password form of protected posts.
	 */
	public static function the_password_form() {
		global $post;
		$id = empty($post->ID) ? rand() : $post->ID;
		$label = 'pwbox-' . $id;
		ob_start(); ?>

		<form action="<?php echo esc_url(site_url('wp-login.php?action=postpass', 'login_post')); ?>" method="post">
			<label for="<?php echo esc_attr($label); ?>"><?php esc_html_e('To view this protected post, enter the password below:', 'chap'); ?></label>
			<div class="ui right action left icon input">
				<i class="lock icon"></i>
				<input id="<?php echo esc_attr($label); ?>" name="post_password" type="password" placeholder="<?php esc_html_e('Password', 'chap'); ?>&hellip;" maxlength="20" />
				<button type="submit" class="ui button"><?php esc_html_e('Submit', 'chap'); ?></button>
			</div>
		</form>

		<?php
		return ob_get_clean();
	}

	/**
	 * Add three dots to the end of excerpts.
	 */
	public static function excerpt_more($more) {
		return '&hellip;';
	}

	/**
	 * Wrap pagination.
	 */
	public static function wp_link_pages_args($args) {
		$alignment = Options\get('pagination_alignment');

		$args['before'] = <<<STR
<div class="ui centered grid">
	<div class="{$alignment} aligned column">
		<div class="ui pagination page-nav menu">
STR;

		$args['after'] = <<<STR

		</div>
	</div>
</div>
STR;

		if(is_page_template('template-full-width.php')) {
			$args['after'] .= '<div class="ui hidden section divider"></div>';
		}

		return $args;
	}

	/**
	 * Add item class to links, wrap active item in div.
	 */
	public static function wp_link_pages_link($link, $i) {
		if(strpos($link, '<a') !== false) {
			$link = str_replace('<a', '<a class="item"', $link);
		} else {
			$link = '<div class="active item">' . $link . '</div>';
		}

		return "\n" . str_repeat("\t", 3) . $link;
	}

	/**
	 * Group comment form fields and add Semantic UI classes.
	 */
	public static function comment_form_default_fields($fields) {
		/**
		 * Change paragraphs to divs with field class.
		 */
		$fields = str_replace(
			[
				'<p class="',
				'</p>',
			],
			[
				'<div class="field ',
				'</div>',
			],
			$fields
		);

		/**
		 * Get first and last field's key in array.
		 */
		reset($fields);
		$first = key($fields);
		end($fields);
		$last = key($fields);

		/**
		 * Wrap the fields.
		 */
		if(strlen($first) > 0 && strlen($last) > 0) {
			$fields[$first] = '<div class="fields">' . $fields[$first];
			$fields[$last] = $fields[$last] . '</div>';
		}

		return $fields;
	}

	/**
	 * Add icon to comment submit button.
	 */
	public static function comment_form_submit_button($button) {
		$button_text = '<i class="icon edit"></i>' . esc_html__('Add reply', 'chap');
		$button = str_replace(['<input', '/>'], ['<button', '>' . $button_text . '</button>'], $button);
		return $button;
	}

	/**
	 * Add item class to comment pagination links.
	 */
	public static function comment_link_attributes($atts) {
		$atts = 'class="item"';
		return $atts;
	}

	/**
	 * Override comment for default arguments.
	 */
	public static function comment_form_defaults($defaults) {
		global $user_identity;
		$defaults['class_form'] = 'ui reply form comment-form';
		$defaults['class_submit'] = 'ui primary labeled submit icon button';
		$defaults['title_reply_before'] = '<h3 id="reply-title" class="ui huge header comment-reply-title">';
		$defaults['title_reply'] = esc_html__('Leave a comment', 'chap');
		$defaults['title_reply_to'] = esc_html__('Leave a reply to %s', 'chap');
		$defaults['label_submit'] = esc_html__('Add reply', 'chap');
		$defaults['comment_field'] = '<div class="comment-form-comment field"><textarea id="comment" name="comment" aria-required="true"></textarea></div>';
		$defaults['logged_in_as'] = '<div class="ui compact info message logged-in-as">' .
			sprintf(
				/* translators: Message before leaving a comment. Latter part consists of the link title and then the link text. */
				esc_html__('Logged in as %1$s. %2$sLog out of this account%3$sLog out?%4$s', 'chap'),
				'<a href="' . esc_url(admin_url('profile.php')) . '">' . $user_identity . '</a>',
				'<a href="' . esc_url(wp_logout_url(apply_filters('the_permalink', get_permalink()))) . '" title="',
				'">',
				'</a>'
			) . '</div>';
		return $defaults;
	}

	/**
	 * Default classes for utility menu.
	 *
	 * @since 1.1.4
	 */
	public static function chap_utility_menu_classes($classes) {
		$classes = Helpers\classnames([
			'ui',
			Options\get('main_menu_size'),
			'inverted' => Options\get('header_inverted'),
			'secondary',
			'pointing',
			'utility',
			$classes,
			'menu',
		]);
		return $classes;
	}

	/**
	 * Render loading screen.
	 *
	 * @since 1.0.4
	 */
	public static function chap_render_loading_screen() {
		if(!Options\get('enable_loading_screen')) {
			return;
		}
		if(!is_front_page() && Options\get('enable_loading_front_page_only')) {
			return;
		}
		?>
		<div class="chap-loader-wrapper">
			<div class="chap-loader-content">
				<?php if(Options\get('enable_loading_screen_brand')): ?>
					<?php do_action('chap_render_brand', false); ?>
				<?php endif; ?>
				<div class="chap-loader">
					<div class="node1"></div>
					<div class="node2"></div>
					<div class="node3"></div>
				</div>
			</div>
		</div>
		<?php
	}

	/**
	 * Adds actions for content to be shown after pages.
	 *
	 * @since 1.1.1
	 */
	public static function the_after_page_actions() {
		$order = Options\get('order_after_page');
		if(!is_array($order)) {
			return;
		}

		foreach($order as $index => $action) {
			if(function_exists(__NAMESPACE__ . '\\chap_' . $action)) {
				add_action('chap_after_page_content', __NAMESPACE__ . '\\chap_' . $action, $index * 10);
			}
		}
	}

	/**
	 * Adds actions for content to be shown after posts.
	 *
	 * @since 1.1.1
	 */
	public static function the_after_post_actions() {
		$order = Options\get('order_after_post');
		if(!is_array($order)) {
			return;
		}

		foreach($order as $index => $action) {
			if(function_exists(__NAMESPACE__ . '\\chap_' . $action)) {
				add_action('chap_after_post_content', __NAMESPACE__ . '\\chap_' . $action, $index * 10);
			}
		}
	}

	/**
	 * Render content before header.
	 *
	 * @since 1.1.0
	 */
	public static function chap_before_header_content() {
		echo do_shortcode(Options\get('before_header_content'));
	}

	/**
	 * Render content before masthead.
	 *
	 * @since 1.1.1
	 */
	public static function chap_before_masthead_content() {
		echo do_shortcode(Options\get('before_masthead_content'));
	}

	/**
	 * Render content after masthead.
	 *
	 * @since 1.1.1
	 */
	public static function chap_after_masthead_content() {
		echo do_shortcode(Options\get('after_masthead_content'));
	}

	/**
	 * Render content before footer.
	 *
	 * @since 1.1.0
	 */
	public static function chap_before_footer_content() {
		echo do_shortcode(Options\get('before_footer_content'));
	}

	/**
	 * Render content after footer.
	 *
	 * @since 1.1.0
	 */
	public static function chap_after_footer_content() {
		echo do_shortcode(Options\get('after_footer_content'));
	}

	/**
	 * Override posts page loop configuration.
	 *
	 * @since 1.0.3
	 */
	public static function chap_pre_get_posts($query) {

		if(is_home() && Options\get('loop_override_front_page')) {

			/**
			 * Default settings for blog page when 'loop_override_front_page' option is enabled.
			 */
			$settings = apply_filters('chap_blog_loop_settings', [
				'type' => 'grid',
				'columns' => 1,
				'separation' => 'divided',
				'spacing' => '',
				'padding' => '',
				'thumbnail_size' => 'large',
				'meta' => true,
				'extras' => true,
				'read_more' => true,
				'excerpt_length' => 999999,
				'query' => $query,
			]);

			add_filter('chap_option_loop_type', function($type) use ($settings) {
				if($settings['query']->is_main_query()) {
					return $settings['type'];
				}
				return $type;
			});

			add_filter('chap_option_loop_columns', function($columns) use ($settings) {
				if($settings['query']->is_main_query()) {
					return $settings['columns'];
				}
				return $columns;
			});

			add_filter('chap_option_loop_separation', function($columns) use ($settings) {
				if($settings['query']->is_main_query()) {
					return $settings['separation'];
				}
				return $columns;
			});

			add_filter('chap_option_loop_spacing', function($spacing) use ($settings) {
				if($settings['query']->is_main_query()) {
					return $settings['spacing'];
				}
				return $spacing;
			});

			add_filter('chap_option_loop_padding', function($padding) use ($settings) {
				if($settings['query']->is_main_query()) {
					return $settings['padding'];
				}
				return $padding;
			});

			add_filter('chap_option_loop_thumbnail_size', function($size) use ($settings) {
				if($settings['query']->is_main_query()) {
					return $settings['thumbnail_size'];
				}
				return $size;
			});

			add_filter('chap_option_loop_render_entry_meta', function($toggle) use ($settings) {
				if($settings['query']->is_main_query()) {
					return $settings['meta'];
				}
				return $toggle;
			});

			add_filter('chap_option_loop_render_extras', function($toggle) use ($settings) {
				if($settings['query']->is_main_query()) {
					return $settings['extras'];
				}
				return $toggle;
			});

			add_filter('chap_option_loop_render_read_more', function($toggle) use ($settings) {
				if($settings['query']->is_main_query()) {
					return $settings['read_more'];
				}
				return $toggle;
			});

			add_filter('excerpt_length', function($length) use ($settings) {
				if($settings['query']->is_main_query()) {
					return $settings['excerpt_length'];
				}
				return $length;
			});

			// $blog_excerpt_length = function() use ($settings) {
			// 	return $settings['excerpt_length'];
			// };
			// add_filter('excerpt_length', $blog_excerpt_length);

		}

	}

}

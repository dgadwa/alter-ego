<?php

namespace Chap\TemplateFunctions;
use Chap\Options;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

if(!function_exists(__NAMESPACE__ . '\\chap_render_main_menu_container')):
	/**
	 * Render the main menu with it's container.
	 * The container is what restricts the menu's width
	 * and is a target for user-specified margins.
	 */
	function chap_render_main_menu_container() {
		?>
		<div class="ui mainmenu container">
			<?php do_action('chap_render_main_menu'); ?>
		</div>
		<?php
	}
endif;

if(!function_exists(__NAMESPACE__ . '\\chap_render_main_menu')):
	/**
	 * Render the main menu.
	 *
	 * @param array $args Override or add arguments for wp_nav_menu.
	 */
	function chap_render_main_menu($args = []) {

		if(!has_nav_menu('primary_navigation')) {
			return;
		}

		$defaults = [
			'theme_location' => 'primary_navigation',
			'menu_id'        => 'primary_menu',
			'toc'            => true,  // Include table of contents.
			'widgets'        => true,  // Include menu widgets.
			'left'           => false, // Left float menu.
			'right'          => false, // Right float menu.
			'compact'        => false, // Compact menu doesn't extend to full width.
			'centered'       => false, // Centered menu justifies content to center, when wrapping.
			'items_wrap'     =>
				'<nav aria-label="' . esc_attr__('Primary menu', 'chap') . '">' .
					'<div id="%1$s" class="%2$s" role="menubar">' .
						'%3$s' .
					'</div>' .
				'</nav>',
		];

		wp_nav_menu(wp_parse_args($args, $defaults));

		$js = '';

		/**
		 * Init morph popups.
		 */
		if(Options\get('main_menu_popup_morph')) {
			$active_class = Options\get('menu_item_hover_active') ? 'active' : '_active';
			$morph_trigger = Options\get('menu_trigger');
			$transition_duration = Options\get('morph_animation_duration');
			$hide_delay = Options\get('morph_hide_delay');
			$pointing = Options\get('main_menu_popups_pointing') ? 1 : 0;
			$js .= <<<JS
document.addEventListener('chap_ready', function(){
	chap.main_menu_morph = new chap.Morph({
		menu: '#primary_menu',
		anchor: '.toc.item',
		items: '.browse.item',
		active_class: '{$active_class}',
		trigger: '{$morph_trigger}',
		gap: 12,
		hide_delay: {$hide_delay},
		transition_duration: {$transition_duration},
		pointing: {$pointing},
	});
});
JS;

		}

		wp_add_inline_script('chap/js', $js);

	}
endif;

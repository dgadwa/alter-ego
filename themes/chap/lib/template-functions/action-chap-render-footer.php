<?php

namespace Chap\TemplateFunctions;
use Chap\Options;
use Chap\Helpers;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

if(!function_exists(__NAMESPACE__ . '\\chap_render_footer_open')):
	/**
	 * Render the footer opening tags.
	 */
	function chap_render_footer_open() {
		$footer_classes = Helpers\fclassnames([
			'ui',
			'vertical',
			'inverted' => Options\get('footer_inverted'),
			'footer',
			'segment',
		], 'chap_footer_classes');

		$container_classes = Helpers\fclassnames([
			'ui',
			Options\get('footer_alignment'),
			'aligned',
			'container',
		], 'chap_footer_content_container_classes');

		?>
		<div class="<?php echo esc_attr($footer_classes); ?>">
			<div class="<?php echo esc_attr($container_classes); ?>">
		<?php
	}
endif;

if(!function_exists(__NAMESPACE__ . '\\chap_render_footer_widgets')):
	/**
	 * Renders the footer widgets.
	 */
	function chap_render_footer_widgets() {
		/**
		 * Don't render grid if there are no widgets.
		 */
		$widgets = wp_get_sidebars_widgets();
		$footer_widgets_count = isset($widgets['sidebar-footer']) ? count($widgets['sidebar-footer']) : 0;
		if($footer_widgets_count < 1) {
			return;
		}

		$inverted = Options\get('footer_inverted') ? 'inverted' : 'default';
		$separation = Options\get('footer_widgets_separation');
		$spacing = Options\get('footer_widgets_spacing');

		/**
		 * Render the footer widgets.
		 */
		?>
		<div class="ui stackable <?php echo esc_attr($inverted); ?> <?php echo esc_attr($separation); ?> <?php echo esc_attr($spacing); ?> equal width grid">
			<div class="row">
				<?php dynamic_sidebar('sidebar-footer'); ?>
			</div>
		</div>
		<?php
	}
endif;

if(!function_exists(__NAMESPACE__ . '\\chap_render_footer_divider')):
	/**
	 * Renders a divider in the footer,
	 * separating the widgets and menu.
	 */
	function chap_render_footer_divider() {
		/**
		 * Don't render divider if there are no widgets.
		 */
		$widgets = wp_get_sidebars_widgets();
		$footer_widgets_count = isset($widgets['sidebar-footer']) ? count($widgets['sidebar-footer']) : 0;
		if($footer_widgets_count < 1) {
			return;
		}

		/**
		 * Don't render divider if there's no menu or footer text.
		 */
		if(!has_nav_menu('footer_bottom_menu') && !(strlen(Options\get('footer_text')) > 0)) {
			return;
		}

		$inverted = Options\get('footer_inverted') ? 'inverted' : 'default';

		/**
		 * Render the footer divider.
		 */
		?>
		<div class="ui <?php echo esc_attr($inverted); ?> section divider"></div>
		<?php
	}
endif;

if(!function_exists(__NAMESPACE__ . '\\chap_render_footer_image')):
	/**
	 * Renders the footer image.
	 */
	function chap_render_footer_image() {
		$footer_image_id = Options\get('footer_image');

		/**
		 * Don't render the footer image if no image is specified.
		 */
		if(empty($footer_image_id)) {
			return;
		}

		$url = wp_get_attachment_image_src($footer_image_id, 'small');

		/**
		 * Don't render if didn't find the image.
		 */
		if(empty($url[0])) {
			return;
		}

		$alt = get_post_meta($footer_image_id, '_wp_attachment_image_alt', true);
		$size = Options\get('footer_image_size');
		$footer_image = '<img src="' . $url[0] . '" alt="' . $alt . '" />';

		$link = Options\get('footer_image_link');
		if(!empty($link)) {
			$footer_image = '<a href="' . $link . '">' . $footer_image . '</a>';
		}

		/**
		 * Render the footer image.
		 */
		?>
		<div class="ui centered <?php echo esc_attr($size); ?> footer_image image">
			<?php echo wp_kses_post($footer_image); ?>
		</div>
		<?php
	}
endif;

if(!function_exists(__NAMESPACE__ . '\\chap_render_footer_menu')):
	/**
	 * Renders the footer menu.
	 */
	function chap_render_footer_menu() {
		/**
		 * Don't render the footer menu if there is no menu.
		 */
		if(!has_nav_menu('footer_bottom_menu')) {
			return;
		}

		$inverted = Options\get('footer_inverted') ? ' inverted' : '';

		/**
		 * Render the footer menu.
		 */
		?>
		<div class="ui borderless vertical footer-menu segment">
			<?php
				wp_nav_menu([
					'theme_location' => 'footer_bottom_menu',
					'menu_class' => 'horizontal small divided' . $inverted . ' link list',
					'items_wrap' =>
						'<nav aria-label="' . esc_attr('Footer menu') . '">' .
							'<div id="%1$s" class="%2$s" role="menubar">' .
								'%3$s' .
							'</div>' .
						'</nav>',
				]);
			?>
		</div>
		<?php
	}
endif;

if(!function_exists(__NAMESPACE__ . '\\chap_render_footer_text')):
	/**
	 * Renders the footer text.
	 */
	function chap_render_footer_text() {
		$footer_text = Options\get('footer_text');
		if(empty($footer_text)) {
			return;
		}

		$alignment = Options\get('footer_text_alignment');
		$inverted = Options\get('footer_inverted') ? 'inverted' : 'default';

		/**
		 * Render the text.
		 */
		?>
		<div class="ui basic <?php echo esc_attr($alignment); ?> aligned <?php echo esc_attr($inverted); ?> paddingless marginless segment">
			<?php echo do_shortcode($footer_text); ?>
		</div>
		<?php
	}
endif;

if(!function_exists(__NAMESPACE__ . '\\chap_render_footer_close')):
	/**
	 * Render the footer closing tags.
	 */
	function chap_render_footer_close() {
		?>
			</div>
		</div>
		<?php
	}
endif;

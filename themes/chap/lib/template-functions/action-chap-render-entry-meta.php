<?php

namespace Chap\TemplateFunctions;
use Chap\Options;
use Chap\Titles;
use Chap\Helpers;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

if(!function_exists(__NAMESPACE__ . '\\chap_render_post_meta')):
	/**
	 * Render the post meta.
	 *
	 * @since 1.1.6
	 * @var bool $inverted Invert text colors.
	 * @var bool $metadata Include hentry meta classes.
	 */
	function chap_render_post_meta($inverted = false, $metadata = false) {
		if(get_post_type() !== 'post') {
			return;
		}

		global $post;
		$author_id = $post->post_author;
		$author_name = get_the_author_meta('nickname', $author_id);
		$author_url = get_author_posts_url($author_id);

		$date_classes = Helpers\classnames([
			'date',
			'updated' => $metadata,
			'invisible' => !Options\get('loop_render_entry_meta'),
		]);

		$author_container_classes = Helpers\classnames([
			'ui',
			'horizontal',
			'inverted' => $inverted,
			'link',
			'list',
			'byline',
			'author' => $metadata,
			'vcard' => $metadata,
			'invisible' => !Options\get('loop_render_entry_meta'),
		]);

		$author_classes = Helpers\classnames([
			'fn' => $metadata,
			'nickname' => $metadata,
			'item',
		]);

		?><time class="<?php echo esc_attr($date_classes); ?>" datetime="<?php echo esc_attr(get_post_time('c', true)); ?>"><?php echo get_the_date(); ?></time><?php
		?><div class="<?php echo esc_attr($author_container_classes); ?>">
			<div class="compact item"><?php echo esc_html__('by', 'chap'); ?></div>
			<a class="<?php echo esc_attr($author_classes); ?>" href="<?php echo esc_url($author_url); ?>" rel="author"><?php echo wp_kses_post($author_name); ?></a>
		</div>
		<?php
	}
endif;

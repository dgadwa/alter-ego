<?php

namespace Chap\TemplateFunctions;
use Chap\Options;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

if(!function_exists(__NAMESPACE__ . '\\chap_render_normal_pagination')):
	/**
	 * Normal pagination: older/newer posts links.
	 *
	 * get_the_posts_navigation() doesn't have a
	 * filter so some string replacements are done to
	 * achieve HTML compatible with Semantic UI.
	 *
	 * @since 1.1.3
	 * @param array $args
	 */
	function chap_render_normal_pagination($args = []) {
		$nav = get_the_posts_navigation($args);

		// Use Semantic UI classes.
		$nav = str_replace(
			[
				'<nav class="navigation posts-navigation"',
				' class="nav-previous"><a',
				' class="nav-next"><a',
				' role="navigation"',
			],
			[
				'<nav aria-label="' . esc_attr__('Pagination', 'chap') . '" class="ui navigation posts-navigation pagination menu"',
				'><a aria-label="' . esc_attr__('Older posts', 'chap') . '" class="nav-previous item"',
				'><a aria-label="' . esc_attr__('Newer posts', 'chap') . '" class="nav-next item"',
				'', // The navigation role is unnecessary for the nav element.
			],
			$nav
		);

		// Clean up unwanted elements.
		$nav = str_replace(
			[
				'<div class="nav-links">',
				'<div>',
				'</div>',
			],
			'',
			$nav
		);

		echo $nav;
	}
endif;

if(!function_exists(__NAMESPACE__ . '\\chap_render_paged_pagination')):
	/**
	 * Paged pagination
	 *
	 * @since 1.1.3
	 */
	function chap_render_paged_pagination() {
		if(is_singular()) {
			return;
		}

		global $wp_query;

		// Stop execution if there's only 1 page.
		if($wp_query->max_num_pages <= 1) {
			return;
		}

		$paged = get_query_var('paged') ? absint(get_query_var('paged')) : 1;
		$max = intval($wp_query->max_num_pages);
		$links = [];

		// Add current page to the array.
		if($paged >= 1) {
			$links[] = $paged;
		}

		// Add the pages around the current page to the array.
		if($paged >= 3) {
			$links[] = $paged - 1;
			$links[] = $paged - 2;
		}

		if(($paged + 2) <= $max) {
			$links[] = $paged + 2;
			$links[] = $paged + 1;
		}

		echo '<nav aria-label="' . esc_attr__('Pagination', 'chap') . '" class="ui navigation posts-navigation pagination menu">' . "\n";

		// Previous post link.
		if(get_previous_posts_link()) {
			printf(
				'%s' . "\n",
				str_replace(
					'<a',
					'<a aria-label="' . esc_attr__('Go to previous page', 'chap') . '" class="icon item"',
					get_previous_posts_link('<i class="left chevron icon"></i>')
				)
			);
		}

		// Link to first page, plus ellipsis if necessary.
		if(!in_array(1, $links)) {
			$class = 1 == $paged ? 'active item' : 'item';

			printf(
				'<a aria-label="%s" class="%s" href="%s">%s</a>' . "\n",
				esc_attr__('Go to first page', 'chap'),
				$class,
				esc_url(get_pagenum_link(1)),
				'1'
			);

			if(!in_array(2, $links)) {
				echo '<div class="disabled item mobile-invisible" tabindex="-1">&hellip;</div>';
			}
		}

		// Link to current page, plus 2 pages in either direction if necessary.
		sort($links);
		foreach($links as $link) {
			if($paged == $link) {
				$class = 'active item';
				$label = esc_attr__('Current page', 'chap') . ', ' . esc_attr__('Page', 'chap') . ' ' . $link;
				$current = ' aria-current="true"';
			} else {
				$class = 'item';
				$label = esc_attr__('Go to page', 'chap') . ' ' . $link;
				$current = '';
				if(!in_array($link, [1, $paged - 1, $paged + 1, $max])) {
					$class .= ' mobile-invisible';
				}
			}

			printf(
				'<a aria-label="%s"%s class="%s" href="%s">%s</a>' . "\n",
				$label,
				$current,
				$class,
				esc_url(get_pagenum_link($link)),
				$link
			);
		}

		// Link to last page, plus ellipsis if necessary.
		if(!in_array($max, $links)) {
			if(!in_array($max - 1, $links)) {
				echo '<div class="disabled item mobile-invisible" tabindex="-1">&hellip;</div>' . "\n";
			}

			$class = $paged == $max ? 'active item' : 'item';
			$label = esc_attr__('Go to last page', 'chap');

			printf(
				'<a aria-label="%s" class="%s" href="%s">%s</a>' . "\n",
				$label,
				$class,
				esc_url(get_pagenum_link($max)),
				$max
			);
		}

		// Next post link.
		if(get_next_posts_link()) {
			printf(
				'%s' . "\n",
				str_replace(
					'<a',
					'<a aria-label="' . esc_attr__('Go to next page', 'chap') . '" class="icon item"',
					get_next_posts_link('<i class="right chevron icon"></i>')
				)
			);
		}

		echo '</nav>' . "\n";
	}
endif;

if(!function_exists(__NAMESPACE__ . '\\chap_render_posts_navigation')):
	/**
	 * Renders the posts navigation based on chosen type.
	 */
	function chap_render_posts_navigation($args = []) {
		$alignment = Options\get('pagination_alignment');
		$type = Options\get('pagination_type');

		?>
		<div class="ui centered grid">
			<div class="<?php echo esc_attr($alignment); ?> aligned column">
				<?php
				if($type === 'paged'):
					chap_render_paged_pagination();
				else:
					chap_render_normal_pagination($args);
				endif;
				?>
			</div>
		</div>
		<?php
	}
endif;

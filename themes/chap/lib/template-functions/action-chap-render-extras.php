<?php

namespace Chap\TemplateFunctions;
use Chap\Options;
use Chap\Helpers;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

if(!function_exists(__NAMESPACE__ . '\\chap_read_more')):
	/**
	 * Render "Read more" link, "View product" if it's a WC item.
	 */
	function chap_read_more() {
		if(!Options\get('loop_render_read_more')) {
			return;
		}

		$post_id = get_the_id();
		$direction = is_rtl() ? 'left' : 'right';
		$text = esc_html__('Read more', 'chap');
		if(count(get_post_meta($post_id, 'total_sales')) > 0) {
			$text = esc_html__('View product', 'chap');
		}

		$text = apply_filters('chap_read_more_text', $text, $post_id);
		$icon_class = Helpers\is_amp() ? 'fa fa-chevron-' . $direction : 'chevron icon';

		?>
		<a class="read-more" href="<?php the_permalink(); ?>" title="<?php echo the_title(); ?>">
			<?php echo wp_kses_post($text); ?>
			<span class="invisible">&nbsp;<?php esc_html_e('about', 'chap'); ?>&nbsp;<?php echo the_title(); ?></span>
			<i class="small <?php echo esc_attr($direction); ?> <?php echo esc_attr($icon_class); ?>"></i>
		</a>
		<?php
	}
endif;

if(!function_exists(__NAMESPACE__ . '\\chap_sticky_post_label')):
	/**
	 * Renders a label marking sticky posts.
	 */
	function chap_sticky_post_label() {
		if(!is_sticky()) {
			return;
		}

		if(!Options\get('loop_render_extras')) {
			return;
		}

		?>
		<div class="ui tiny right floated meta labels">
			<div class="ui tiny horizontal right floated red tag sticky label"><?php esc_html_e('Sticky', 'chap'); ?></div>
		</div>
		<?php
	}
endif;

if(!function_exists(__NAMESPACE__ . '\\chap_category_labels')):
	/**
	 * Renders labels of post's categories.
	 */
	function chap_category_labels() {
		if(!Options\get('loop_render_extras')) {
			return;
		}

		$cats = wp_get_post_categories(get_the_id());
		if(empty($cats)) {
			return;
		}

		$labels = '';
		foreach($cats as $cat) {
			$labels .= '<a href="' . get_category_link($cat) . '" class="ui label">' . get_cat_name($cat) . '</a>';
		}

		?>
		<div class="ui tiny horizontal right floated category meta labels">
			<div class="ui transparent label"><i class="large folder icon"></i></div>
			<?php echo wp_kses_post($labels); ?>
		</div>
		<?php
	}
endif;

if(!function_exists(__NAMESPACE__ . '\\chap_tag_labels')):
	/**
	 * Renders labels of post's tags.
	 */
	function chap_tag_labels() {
		if(!Options\get('loop_render_extras')) {
			return;
		}

		$tags = get_the_tags();
		if(empty($tags)) {
			return;
		}

		$labels = '';
		foreach($tags as $tag) {
			$labels .= '<a href="' . get_tag_link($tag->term_id) . '" class="ui label">' . $tag->name . '</a>';
		}

		?>
		<div class="ui tiny horizontal right floated tags meta labels">
			<div class="ui transparent label"><i class="large tags icon"></i></div>
			<?php echo wp_kses_post($labels); ?>
		</div>
		<?php
	}
endif;

if(!function_exists(__NAMESPACE__ . '\\chap_card_extra_content_open')):
	/**
	 * Open the container for extra content in cards.
	 *
	 * @since 1.0.6
	 */
	function chap_card_extra_content_open() {
		if(!Options\get('loop_render_extras')) {
			return;
		}

		/**
		 * Since 1.0.9
		 */
		if(get_post_type() === 'page') {
			return;
		}

		?>
		<div class="extra content">
		<?php
	}
endif;

if(!function_exists(__NAMESPACE__ . '\\chap_card_extra_content_close')):
	/**
	 * Close the container for extra content in cards.
	 *
	 * @since 1.0.6
	 */
	function chap_card_extra_content_close() {
		if(!Options\get('loop_render_extras')) {
			return;
		}

		/**
		 * Since 1.0.9
		 */
		if(get_post_type() === 'page') {
			return;
		}

		?>
		</div>
		<?php
	}
endif;

if(!function_exists(__NAMESPACE__ . '\\chap_card_read_more')):
	/**
	 * Render "Read more" button for cards.
	 */
	function chap_card_read_more() {
		if(!Options\get('loop_render_read_more')) {
			return;
		}

		$post_id = get_the_id();
		$direction = is_rtl() ? 'left' : 'right';
		$text = esc_html__('Read more', 'chap');
		if(count(get_post_meta($post_id, 'total_sales')) > 0) {
			$text = esc_html__('View product', 'chap');
		}

		$text = apply_filters('chap_read_more_text', $text, $post_id);
		$icon_class = Helpers\is_amp() ? 'fa fa-chevron-' . $direction : 'chevron icon';

		?>
		<a class="ui bottom attached read-more button" href="<?php the_permalink(); ?>" title="<?php echo the_title(); ?>">
			<?php echo wp_kses_post($text); ?>
			<span class="invisible">&nbsp;<?php esc_html_e('about', 'chap'); ?>&nbsp;<?php echo the_title(); ?></span>
			<i class="small <?php echo esc_attr($direction); ?> <?php echo esc_attr($icon_class); ?>"></i>
		</a>
		<?php
	}
endif;

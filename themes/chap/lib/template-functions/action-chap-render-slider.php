<?php

namespace Chap\TemplateFunctions;
use Chap\Options;
use Chap\Helpers;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

if(!function_exists(__NAMESPACE__ . '\\chap_load_slides')):
	/**
	 * Query the slides that are to be shown on this page.
	 *
	 * Store the query result for rendering the slides later.
	 */
	function chap_load_slides() {
		/**
		 * @global $chap_theme
		 * @see class-chap.php
		 */
		global $chap_theme;

		/**
		 * No slides on search page.
		 */
		if(is_search()) {
			return;
		}

		$query = [];
		$post_type = get_post_type();
		$id = intval(get_the_ID());

		if(is_front_page()) {

			/**
			 * Slides for front page.
			 */
			$query = [
				'post_type' => 'chap_slide',
				'meta_key' => CHAP_TF . '_slide_order',
				'orderby' => 'meta_value_num',
				'order' => 'ASC',
				'meta_query' => [
					[
						'key' => CHAP_TF . '_slide_display_on_front_page',
						'compare' => '=',
						'value' => true,
					],
				],
			];

		} elseif($post_type === 'page' || $post_type === 'post' || is_home()) {

			/**
			 * Show slides set to show on a page that has been set as page for posts.
			 */
			if(is_home()) {
				$id = get_option('page_for_posts');
				$post_type = 'page';
			}

			/**
			 * Display slides for pages or posts.
			 */
			$query = [
				'post_type' => 'chap_slide',
				'meta_key' => CHAP_TF . '_slide_order',
				'orderby' => 'meta_value_num',
				'order' => 'ASC',
				'meta_query' => [
					[
						'key' => CHAP_TF . '_slide_display_on_' . $post_type,
						'compare' => 'LIKE',
						'value' => serialize(strval($id)),
					],
				],
			];

		} elseif($post_type === 'chap_slide') {

			/**
			 * Display single slide/draft/preview.
			 */
			if(is_preview()) {
				if(!empty($_GET['preview_id'])) {
					/**
					 * Existing modified post preview.
					 */
					$query = [
						'post_type' => 'revision',
						'post_status' => 'any',
						'post_parent' => $id,
					];
				} else {
					/**
					 * Unpublished draft preview.
					 */
					$query = [
						'post_type' => 'chap_slide',
						'post_status' => get_post_status($id),
					];
				}
			} else {
				/**
				 * View single chap_slide post.
				 */
				$query = [
					'post_type' => 'chap_slide',
					'p' => $id,
				];
			}

		} else {
			/**
			 * Don't fetch anything if query is empty.
			 */
			return;
		}

		/**
		 * Query the slides.
		 */
		$query['posts_per_page'] = -1;
		$slides_query = new \WP_Query($query);

		/**
		 * Add product slides on front page.
		 */
		if(class_exists('WooCommerce') && is_front_page()) {

			$products = new \WP_Query([
				'post_type' => 'product',
				'posts_per_page' => -1,
				'meta_key' => CHAP_TF . '_product_slide_order',
				'orderby' => 'meta_value_num',
				'order' => 'ASC',
				'meta_query' => [
					'relation' => 'OR',
					[
						'key' => CHAP_TF . '_slider_show_product',
						'compare' => '=',
						'value' => true,
					],
					[
						'key' => CHAP_TF . '_slider_show_product_on_amp',
						'compare' => '=',
						'value' => true,
					],
				],
			]);
			$wp_query = new \WP_Query();
			$wp_query->posts = array_merge($slides_query->posts, $products->posts);
			$wp_query->post_count = $slides_query->post_count + $products->post_count;

		} else {

			$wp_query = $slides_query;

		}

		/**
		 * No slides.
		 */
		if($wp_query->post_count <= 0) {
			return;
		}

		/**
		 * Exclude slides that shouldn't be shown.
		 */
		$exclude = [];
		if($wp_query->have_posts()) {
			while($wp_query->have_posts()) {
				$wp_query->the_post();
				$id = get_the_ID();
				$type = get_post_type();
				if($type === 'product') {
					$show_on_amp = Options\get('slider_show_product_on_amp', $id);
					$show_only_on_amp = !Options\get('slider_show_product', $id);
				} else {
					$show_on_amp = Options\get('slide_display_on_amp', $id);
					$show_only_on_amp = Options\get('slide_only_display_on_amp', $id);
				}
				if(Helpers\is_amp()) {
					if(!$show_on_amp) {
						$exclude[] = $id;
					}
				} else {
					if($show_only_on_amp) {
						$exclude[] = $id;
					}
				}
			}
		}

		/**
		 * No slides or all found slides are excluded.
		 */
		if(count($exclude) >= $wp_query->post_count) {
			return;
		}

		/**
		 * Current page has slides.
		 */
		$chap_theme['has_slides'] = true;

		/**
		 * Don't output these slides.
		 */
		$chap_theme['slides_exclude'] = $exclude;

		/**
		 * Store the count.
		 */
		$chap_theme['slides_count'] = $wp_query->post_count - count($exclude);

		/**
		 * Store the query result.
		 */
		$chap_theme['slides'] = clone $wp_query;

		wp_reset_postdata();
	}
endif;

if(!function_exists(__NAMESPACE__ . '\\chap_render_slider')):
	/**
	 * Render slider.
	 */
	function chap_render_slider($query) {
		/**
		 * @global $chap_theme
		 * @see class-chap.php
		 */
		global $chap_theme;
		if(!$chap_theme['has_slides']) {
			return;
		}
		$wp_query = clone $chap_theme['slides']; // Use slides previously queried.

		/**
		 * Render slider.
		 */
		if(Helpers\is_amp()): ?>

		<amp-carousel
			class="chap-amp-slider"
			height="<?php echo (int)Options\get('amp_slider_height'); ?>"
			layout="fixed-height"
			type="slides"
			<?php echo Options\get('slider_loop') ? 'loop' : ''; ?>
			<?php echo intval(Options\get('slider_autoplay')) === 0 ? '' : 'autoplay'; ?>
			<?php echo intval(Options\get('slider_autoplay')) > 0 ? 'delay="' . intval(Options\get('slider_autoplay')) . '"' : ''; ?>
			data-next-button-aria-label="<?php esc_html_e('Next item in carousel', 'chap'); ?>"
			data-prev-button-aria-label="<?php esc_html_e('Previous item in carousel', 'chap'); ?>">
			<?php do_action('chap_render_slides', $wp_query); ?>
		</amp-carousel>

		<?php else: ?>

		<div class="chap-main-slider swiper-container">
			<div class="swiper-wrapper">
				<?php do_action('chap_render_slides', $wp_query); ?>
			</div>
			<?php do_action('chap_init_slider', $chap_theme['slides_count']); ?>
		</div>

		<?php endif;

		wp_reset_postdata();
	}
endif;

if(!function_exists(__NAMESPACE__ . '\\chap_render_slides')):
	/**
	 * Slides loop.
	 */
	function chap_render_slides($slides) {
		/**
		 * @global $chap_theme
		 * @see class-chap.php
		 */
		global $chap_theme;

		while($slides->have_posts()) {
			$slides->the_post();
			$id = get_the_ID();
			if(!in_array($id, $chap_theme['slides_exclude'])) {
				do_action('chap_render_slide');
			}
		}
	}
endif;

if(!function_exists(__NAMESPACE__ . '\\chap_render_slide')):
	/**
	 * Render slide template based on post type.
	 */
	function chap_render_slide() {
		$template_folder = Helpers\is_amp() ? 'amp' : 'templates';
		if(get_post_type() === 'chap_slide') {
			get_template_part($template_folder . '/slide');
		} else {
			get_template_part($template_folder . '/slide', (get_post_type() !== 'post') ? get_post_type() : get_post_format());
		}
	}
endif;

if(!function_exists(__NAMESPACE__ . '\\chap_render_slide_open')):
	/**
	 * Render the opening tag for post_type 'chap_slide'.
	 */
	function chap_render_slide_open() {
		/**
		 * Slide ID.
		 * @var int
		 */
		$slide_id = get_the_ID();

		/**
		 * Slide post type.
		 * @var string
		 */
		$slide_type = get_post_type();

		/**
		 * Slide attributes.
		 * @var array
		 */
		$slide_atts = [
			'classes' => ['ui', 'slide'],
			'id' => $slide_type . '-slide-' . $slide_id,
			'style' => [],
		];

		/**
		 * Contains amp-img to be shown as slide background on AMP pages.
		 * No longer needed, but remains for backwards compatibility.
		 * @var string
		 */
		$amp_background_image = '';

		/**
		 * Apply slide metabox options.
		 */
		if($slide_type === 'chap_slide') {
			$slide_atts['classes'][] = Options\get('slide_alignment', $slide_id) . ' aligned';
			$slide_atts['classes'][] = Options\get('slide_vertical_alignment', $slide_id) . ' aligned';
			if(!Options\get('slide_full_width', $slide_id)) {
				$slide_atts['classes'][] = 'container';
			}
		} elseif($slide_type === 'product') {
			$slide_atts['classes'][] = 'left aligned';
		}

		/**
		 * Apply slide background.
		 */
		if($slide_css = Options\get('slide_background_css', $slide_id)) {
			/**
			 * Load slide background CSS.
			 * @since 1.1.7
			 */

			if(Helpers\is_amp()) {
				// AMP CSS needs slight modifications to center the background image.
				$slide_css = str_replace('left:0;', 'left:50%;transform:translateX(-50%);', $slide_css);
				// Output the CSS on AMP pages.
				add_action('amp_post_template_css', function() use ($slide_css) {
					echo wp_kses_data($slide_css);
				});
			} else {
				// Handle lazy loading of background image.
				if(Options\get('slider_lazy_loading') && $GLOBALS['chap_theme']['slides_count'] > 1) {
					$bg_image_id = Options\get('slide_background_image', $slide_id);
					if(!empty($bg_image_id) && $bg_image_id !== -1) {
						$bg_image_url = wp_get_attachment_image_src($bg_image_id, 'full');
						if(isset($bg_image_url[0])) {
							$slide_atts['classes'][] = 'swiper-lazy';
							$slide_atts['data-background'] = esc_url($bg_image_url[0]);
							$background_regex = "/\bbackground-image:url\s*\(\s*[\"']?[^\"'\r\n,]+[\"']?\s*\);/";
							$slide_css = preg_replace($background_regex, '', $slide_css);
						}
					}
				}
				// Output CSS in page footer.
				add_action('wp_footer', function() use ($slide_css) {
					echo '<style>' . wp_kses_data($slide_css) . '</style>';
				});
			}
		} else {
			/**
			 * There is no slide background CSS but for backwards compatibility try
			 * to create slide background CSS based on metaboxes for inline usage.
			 *
			 * This should only trigger for slides created before Chap version 1.15.5.
			 */

			// Slide background color.
			$color = Options\get('slide_background_color', $slide_id);
			if(!empty($color)) {
				$slide_atts['style'][] = 'background-color:' . $color;
			}

			// Get the slide background image.
			$bg_image_id = Options\get('slide_background_image', $slide_id);
			if(!empty($bg_image_id) && $bg_image_id !== -1) {
				$bg_image_url = wp_get_attachment_image_src($bg_image_id, 'full');
				// Apply the slide background image.
				if(!empty($bg_image_url[0])) {
					if(Helpers\is_amp()) {
						$amp_background_image = Helpers\html_tag('amp-img', [
							'layout' => 'fill',
							'src' => $bg_image_url[0],
						]) . '</amp-img>';
					} else {
						$slide_atts['style'][] = 'background-size:' . Options\get('slide_background_size', $slide_id);
						$slide_atts['style'][] = 'background-position:' . Options\get('slide_background_position', $slide_id);
						$slide_atts['style'][] = 'background-repeat:' . Options\get('slide_background_repeat', $slide_id);
						if(Options\get('slider_lazy_loading') && $GLOBALS['chap_theme']['slides_count'] > 1) {
							$slide_atts['classes'][] = 'swiper-lazy';
							$slide_atts['data-background'] = $bg_image_url[0];
						} else {
							$slide_atts['style'][] = 'background-image:url(' . $bg_image_url[0] . ')';
						}
					}
				}
			}
		}

		/**
		 * Render the slide opening tag.
		 */
		echo Helpers\html_tag('div', $slide_atts) . $amp_background_image;
	}
endif;

if(!function_exists(__NAMESPACE__ . '\\chap_render_slide_close')):
	/**
	 * Render the closing tag for post_type 'chap_slide'.
	 */
	function chap_render_slide_close() {
		?>
		</div>
		<?php
	}
endif;

if(!function_exists(__NAMESPACE__ . '\\chap_render_slide_loading')):
	/**
	 * Render loading indicator.
	 */
	function chap_render_slide_loading() {
		if(!Options\get('slider_lazy_loading')) {
			return;
		}

		?>
		<div class="swiper-lazy-preloader"></div>
		<?php
	}
endif;

if(!function_exists(__NAMESPACE__ . '\\chap_render_edit_slide')):
	/**
	 * Render edit slide button for admins.
	 */
	function chap_render_edit_slide() {
		if(!Options\get('slider_edit')) {
			return;
		}

		if(is_preview()) {
			return;
		}

		if(!current_user_can('edit_post', get_the_ID())) {
			return;
		}

		edit_post_link(esc_html__('Edit', 'chap'), '', '', get_the_ID(), 'ui mini compact edit_slide button');
	}
endif;

if(!function_exists(__NAMESPACE__ . '\\chap_render_slider_pagination')):
	/**
	 * Render slider pagination.
	 */
	function chap_render_slider_pagination() {
		if(!Options\get('slider_pagination')) {
			return;
		}

		$color = Options\get('slider_pagination_color');

		?>
		<div class="swiper-pagination <?php echo esc_attr($color); ?>"></div>
		<?php
	}
endif;

if(!function_exists(__NAMESPACE__ . '\\chap_render_slider_nav')):
	/**
	 * Render slider prev/next buttons.
	 */
	function chap_render_slider_nav() {
		if(!Options\get('slider_navigation')) {
			return;
		}

		$color = Options\get('slider_navigation_color');

		?>
		<div class="swiper-button-prev <?php echo esc_attr($color); ?>"></div>
		<div class="swiper-button-next <?php echo esc_attr($color); ?>"></div>
		<?php
	}
endif;

if(!function_exists(__NAMESPACE__ . '\\chap_render_slider_scrollbar')):
	/**
	 * Render slider scrollbar.
	 */
	function chap_render_slider_scrollbar() {
		if(!Options\get('slider_scrollbar')) {
			return;
		}

		?>
		<div class="swiper-scrollbar"></div>
		<?php
	}
endif;

if(!function_exists(__NAMESPACE__ . '\\chap_init_slider')):
	/**
	 * Initialize Swiper.
	 */
	function chap_init_slider($slide_count) {
		$js = '';

		/**
		 * Add event handlers to extend the slider upwards.
		 */
		if(Options\get('slider_extend')) {
			$js .= 'chap.swiper_extend();';
			$js .= "wp.hooks.addAction('chap_resize', chap.swiper_extend, 11);";
		}

		/**
		 * Restrict max height.
		 */
		$max_height = (int)Options\get('slider_max_height');
		if($max_height <= 0) {
			$js .= 'chap.swiper_full_height();';
			$js .= "wp.hooks.addAction('chap_resize', chap.swiper_full_height, 12);";
		}

		/**
		 * Swiper configuration if Animation on Scroll is enabled.
		 *
		 * AOS attributes are removed from inactive slides and reactivated once
		 * the slide becomes visible to trigger the animation effects.
		 */
		if(Options\get('enable_aos')) {
			$js .= "wp.hooks.addAction('chap_swiper_on_touch_move', chap.swiper_set_touch);";
			$js .= "wp.hooks.addAction('chap_swiper_on_change_start', chap.swiper_aos_prepare);";
			$js .= "wp.hooks.addAction('chap_swiper_on_change_end', chap.swiper_aos_refresh);";
		}

		/**
		 * Can't init if dependancies missing.
		 */
		if(!Helpers\chap_dependencies_missing() && $slide_count > 1) {
			/**
			 * Render slider controls.
			 */
			chap_render_slider_pagination();
			chap_render_slider_nav();
			chap_render_slider_scrollbar();

			/**
			 * Populate the slider-related theme options.
			 */
			$chap_slider = [
				'options' => [
					'loop'                         => Options\get('slider_loop'),
					'effect'                       => Options\get('slider_transition'),
					'paginationClickable'          => Options\get('slider_pagination'),
					'pagination'                   => Options\get('slider_pagination') ? '.swiper-pagination' : '',
					'nextButton'                   => Options\get('slider_navigation') ? '.swiper-button-next' : '',
					'prevButton'                   => Options\get('slider_navigation') ? '.swiper-button-prev' : '',
					'scrollbar'                    => Options\get('slider_scrollbar') ? '.swiper-scrollbar' : '',
					'autoplay'                     => intval(Options\get('slider_autoplay')),
					'autoplayDisableOnInteraction' => Options\get('slider_disable_on_interaction'),
					'speed'                        => intval(Options\get('slider_speed')),
					'preloadImages'                => Options\get('slider_lazy_loading') ? false : true,
					'lazyLoading'                  => Options\get('slider_lazy_loading'),
					'preventClicks'                => false,
					'preventClicksPropagation'     => false,
					'grabCursor'                   => Options\get('slider_grab_cursor'),
				],
			];
			wp_localize_script('chap/js', 'chap_slider', $chap_slider);

			/**
			 * Initialize the swiper.
			 */
			$js .= "wp.hooks.addAction('chap_init', chap.init_swiper, 10);";
		}

		/**
		 * Load hooks.
		 */
		if(strlen($js) > 0) {
			wp_add_inline_script('chap/js', "document.addEventListener('chap_ready',function(){" . $js . "});");
		}

	}
endif;

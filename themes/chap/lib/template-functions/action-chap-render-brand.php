<?php

namespace Chap\TemplateFunctions;
use Chap\Helpers;
use Chap\Options;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

if(!function_exists(__NAMESPACE__ . '\\chap_render_branding_container')):
	/**
	 * Renders the branding container with header widgets.
	 */
	function chap_render_branding_container() {

		$menu_size = Options\get('main_menu_size');
		$type = has_nav_menu('primary_navigation') ? 'secondary' : 'text';
		$inverted = Options\get('header_inverted') ? 'inverted' : 'default';

		$atts = [
			'classes' => [
				'ui',
				$menu_size,
				$type,
				$inverted,
				'pointing',
				'utility',
				'menu',
			],
		];

		/**
		 * Render the branding container.
		 */
		?>
		<div class="ui branding container">
			<?php echo Helpers\html_tag('div', $atts); ?>
				<div class="brand item">
					<?php do_action('chap_render_brand', true); ?>
				</div>
				<div class="right header-widgets menu">
					<?php dynamic_sidebar('sidebar-header'); ?>
				</div>
			</div>
		</div>
		<?php
	}
endif;

if(!function_exists(__NAMESPACE__ . '\\chap_render_brand')):
	/**
	 * Renders the brand.
	 *
	 * @var string $text Render text components.
	 */
	function chap_render_brand($text) {
		$brand_size      = Options\get('brand_size');
		$brand_text_size = Options\get('brand_text_size');
		$inverted        = Options\get('brand_inverted') ? 'inverted' : 'default';
		$show_site_title = $text && Options\get('brand_show_site_title');
		$show_tagline    = $text && Options\get('brand_show_tagline');
		$show_logo       = Options\get('brand_show_logo');
		$logo_position   = ($show_site_title || $show_tagline) ? Options\get('brand_logo_position') : 'center';
		$logo            = '';
		$logo_mobile     = Options\get('brand_mobile_hide_logo') ? ' mobile-invisible' : '';
		$logo_mobile    .= Options\get('brand_mobile_hide_text') ? ' text-mobile-invisible' : '';

		$bloginfo_name = apply_filters('chap_brand_title', get_bloginfo('name'));
		$bloginfo_description = apply_filters('chap_brand_tagline', get_bloginfo('description'));

		/**
		 * Attributes for the Brand opening tag.
		 */
		$atts = [
			'href' => esc_url(home_url('/')),
			'classes' => [
				'ui',
				$brand_text_size,
				$inverted,
				'brand',
				'header',
			],
		];

		if($show_logo) {

			$custom_logo_id = get_theme_mod('custom_logo');

			if($custom_logo_id && intval($custom_logo_id) > 0) {

				$logo = wp_get_attachment_image($custom_logo_id, 'full', false, [
					'class'=> 'ui custom-logo ' . $logo_position . $logo_mobile . ' image',
				]);

				if($logo_position === 'above') {
					$atts['classes'][] = 'marginless icon';
				}

			}

		}

		/**
		 * Render the brand.
		 */
		if(Options\get('brand_mobile_hide_text')) {
			$content_class = 'mobile-invisible ';
		} else {
			$content_class = Options\get('brand_mobile_center_text') ? 'mca ' : '';
		}
		$logo_class = $show_logo ? ' logo-' . $logo_position : '';
		if($logo_class && $logo_mobile) {
			$logo_class .= ' logo-mobile-invisible';
		}
		?>
		<div class="ui <?php echo esc_attr($brand_size); ?> basic brand segment">
			<?php echo Helpers\html_tag('a', $atts); ?>

			<?php if($logo_position !== 'right'): ?>
				<?php echo wp_kses_post($logo); ?>
			<?php endif; ?>

			<?php if($show_site_title): ?>
				<div class="<?php echo esc_attr($content_class); ?>content<?php echo esc_attr($logo_class); ?>">
					<?php echo wp_kses_post($bloginfo_name); ?>
					<?php if($show_tagline): ?>
					<div class="sub header">
						<?php echo wp_kses_post($bloginfo_description); ?>
					</div>
					<?php endif; ?>
				</div>
			<?php endif; ?>

			<?php if($logo_position === 'right'): ?>
				<?php echo wp_kses_post($logo); ?>
			<?php endif; ?>

			</a>
		</div>
		<?php
	}
endif;

<?php
/**
 * Single post template.
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}
?>

<?php if(have_posts()): the_post(); ?>
	<?php get_template_part('templates/content-single'); ?>
<?php endif; ?>

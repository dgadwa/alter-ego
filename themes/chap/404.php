<?php
/**
 * Error 404 page template.
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}
?>
<div class="ui center aligned stackable grid">
	<div class="row">
		<div class="eight wide computer ten wide tablet column">
			<div class="ui icon error message">
				<i class="warning circle icon"></i>
				<div class="content">
					<div class="header"><?php esc_html_e('Error 404', 'chap'); ?></div>
					<?php esc_html_e('Sorry, but the page you were trying to view does not exist.', 'chap'); ?>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="eight wide computer ten wide tablet column">
			<?php get_search_form(); ?>
		</div>
	</div>
</div>

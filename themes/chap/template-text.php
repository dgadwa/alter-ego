<?php
/**
 * Template Name: Text
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}
?>

<div class="ui text container">
	<?php while(have_posts()): the_post(); ?>
		<?php get_template_part('templates/page', 'header'); ?>
		<?php get_template_part('templates/content', 'page'); ?>
	<?php endwhile; ?>
</div>

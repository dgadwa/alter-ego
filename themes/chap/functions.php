<?php
/**
 * Chap theme functions.php.
 *
 * Don't edit this, edit the child theme's functions.php.
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

/**
 * The $chap_includes array determines the code library included in the theme.
 * Supports child theme overrides.
 *
 * Please note that missing files will produce a fatal error.
 *
 * @link https://github.com/roots/sage/pull/1042
 */
$chap_includes = [

	/**
	 * Load plugins required by this theme.
	 */
	'lib/3rd-party/TGM-Plugin-Activation/chap-tgm.php',

	/**
	 * Embed Chap Titan Framework if it doesn't already exist.
	 */
	'lib/3rd-party/chap-titan-framework/titan-framework-embedder.php',

	/**
	 * Embed Widget Output Filters if it doesn't exist as a plugin.
	 */
	'lib/3rd-party/widget-output-filters/widget-output-filters-embedder.php',

	/**
	 * Embed Menu item custom fields plugin.
	 */
	'lib/3rd-party/wp-menu-item-custom-fields/menu-item-custom-fields.php',

	/**
	 * Admin class.
	 */
	is_admin() ? 'lib/admin/class-chap-admin.php' : null,

	/**
	 * One Click Demo Import hooks.
	 */
	is_admin() ? 'lib/admin/chap-ocdi.php' : null,

	/**
	 * Setup wizard.
	 */
	is_admin() ? 'lib/3rd-party/envato-setup-wizard/envato-setup-init.php' : null,

	/**
	 * DOM parser.
	 */
	'lib/3rd-party/phpQuery/phpQuery.php',

	/**
	 * Helper functions.
	 */
	'lib/helpers.php',

	/**
	 * Asset loader.
	 */
	'lib/assets.php',

	/**
	 * Page titles.
	 */
	'lib/titles.php',

	/**
	 * Theme wrapper.
	 */
	'lib/wrapper.php',

	/**
	 * Template actions.
	 */
	'lib/template-functions/class-chap-template-functions.php',

	/**
	 * Overriding default shortcodes.
	 */
	'lib/shortcodes/class-chap-shortcodes.php',

	/**
	 * Custom walkers.
	 */
	'lib/walkers/class-chap-walkers.php',

	/**
	 * Theme options.
	 */
	'lib/options/class-chap-options.php',

	/**
	 * Widget filters.
	 */
	'lib/widgets/class-chap-widgets.php',

	/**
	 * Modifications to WooCommerce.
	 */
	class_exists('WooCommerce') ? 'lib/woocommerce/class-chap-woocommerce.php' : null,

	/**
	 * AMP integration.
	 */
	'lib/amp/class-chap-amp.php',

	/**
	 * Initialize theme.
	 */
	'lib/class-chap.php',

	/**
	 * WP CLI commands.
	 */
	'lib/admin/cli.php',

];

/**
 * Load includes.
 */
foreach($chap_includes as $chap_file) {

	if($chap_file == null) {
		continue;
	}

	if(!$chap_filepath = locate_template($chap_file)) {
		trigger_error(sprintf(esc_html__('Error locating %s for inclusion', 'chap'), $chap_file), E_USER_ERROR);
	}

	require_once $chap_filepath;

}

unset($chap_file, $chap_filepath);

<?php
/**
 * Brand, centered menu and header widgets.
 */
use function Chap\Options\get;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}
?>
<div class="ui container">
	<div class="ui branding mainmenu header-template-centered-menu flex-grid">
		<div class="static brand column">
			<?php do_action('chap_render_brand', true); ?>
		</div>
		<div class="mainmenu column">
			<?php do_action('chap_render_main_menu', [
				'widgets' => false,
				'compact' => true,
				'centered' => !get('main_menu_container') || get('main_menu_borderless'),
			]); ?>
		</div>
		<div class="static column">
			<div class="<?php echo esc_attr(apply_filters('chap_utility_menu_classes', '')); ?>">
				<div class="right header-widgets menu">
					<?php dynamic_sidebar('sidebar-header'); ?>
				</div>
			</div>
		</div>
	</div>
</div>

<?php
/**
 * Items in main loops, such as: Blog posts, Category archives, Author archives
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

switch(Chap\Options\get('loop_type')):

	case 'grid':
		$GLOBALS['chap_grid']['columns'] = Chap\Options\get('loop_columns');
		$loop_template = Chap\Options\get('loop_override_front_page') && is_home() && is_main_query() ? 'content' : 'column';
		get_template_part('templates/loop', $loop_template);
	break;

	case 'cards':
		get_template_part('templates/loop', 'card');
	break;

	default:
		get_template_part('templates/loop', 'item');

endswitch;

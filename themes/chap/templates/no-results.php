<?php
/**
 * Search results: no results.
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}
?>
<div class="ui center aligned stackable grid">

	<div class="row">
		<div class="eight wide computer ten wide tablet column">
			<div class="ui info message"><?php esc_html_e('Sorry, no results were found.', 'chap'); ?></div>
		</div>
	</div>

	<div class="row">
		<div class="eight wide computer ten wide tablet column">
			<?php get_search_form(); ?>
		</div>
	</div>

</div>

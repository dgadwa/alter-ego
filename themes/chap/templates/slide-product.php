<?php
/**
 * WC Product slide.
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

$chap_product_id  = intval(get_the_ID());
$chap_product     = wc_get_product($chap_product_id);
$chap_button_text = Chap\Options\get('product_slide_button_text', $chap_product_id);
$chap_inverted    = Chap\Options\get('header_inverted') ? 'inverted' : 'default';
?>

<div class="swiper-slide swiper-slide-active">

	<?php do_action('chap_render_slide_open'); ?>

		<?php do_action('chap_render_edit_slide'); ?>

		<div class="ui equal width stackable container grid">

			<div class="computer only center aligned middle aligned column">

				<?php
					if(has_post_thumbnail()) {
						the_post_thumbnail('large', [
							'class' => 'ui bordered rounded large image',
						]);
					}
				?>

			</div>

			<div class="left aligned middle aligned column">

				<div class="ui <?php echo esc_attr($chap_inverted); ?> list">

					<div class="item">
						<div class="ui header"><?php the_title(); ?></div>
					</div>

					<div class="item">
						<div class="ui small basic paddingless <?php echo esc_attr($chap_inverted); ?> segment">
							<div class="dot-ellipsis dot-resize-update"><?php the_excerpt(); ?></div>
						</div>
					</div>

					<div class="item">

						<div class="ui adjacent to button">
							<?php echo wp_kses_post($chap_product->get_price_html()); ?>
						</div>

						<a href="<?php echo esc_url($chap_product->get_permalink()); ?>" class="ui right floated right labeled positive icon button">
							<i class="right arrow icon"></i>
							<?php echo !empty($chap_button_text) ? esc_html($chap_button_text) : esc_html__('View product', 'chap'); ?>
						</a>

					</div>

				</div>

			</div>

		</div>

	<?php do_action('chap_render_slide_close'); ?>

</div>

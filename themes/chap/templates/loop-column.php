<?php
/**
 * Column in a grid loop.
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

global $chap_grid;
$chap_grid['rendered']++;
$slider_context = isset($GLOBALS['csc']) ? $GLOBALS['csc']['slider'] : false;
if($chap_grid['rendered'] > $chap_grid['columns'] && !$slider_context) {
	echo '</div><div class="row">';
	$chap_grid['rendered'] = 1;
}
$maybe_justified = Chap\Options\get('loop_grid_justified') ? 'justified ' : '';
?>
<article <?php post_class($maybe_justified . 'column'); ?>>

	<?php if(has_post_thumbnail() && Chap\Options\get('loop_thumbnail_size') !== 'none'): ?>
	<a class="image" href="<?php the_permalink(); ?>"><?php the_post_thumbnail(Chap\Options\get('loop_thumbnail_size'), ['class' => 'ui fluid image']); ?></a>
	<?php endif; ?>

	<div class="content">

		<a class="header" href="<?php the_permalink(); ?>">
			<h2 class="ui <?php echo esc_attr(Chap\Options\get('loop_header_size')); ?> header entry-title"><?php the_title(); ?></h2>
		</a>

		<div class="meta">
			<?php
			/**
			 * The chap_render_entry_meta hook.
			 *
			 * @hooked chap_render_post_meta - 10
			 */
			do_action('chap_render_entry_meta', false, true); ?>
		</div>

		<?php if((int)apply_filters('excerpt_length', 55) > 0): ?>
		<div class="description entry-summary"><?php the_excerpt(); ?></div>
		<?php endif; ?>

		<div class="extra">
			<?php
			/**
			 * The chap_render_extras hook.
			 *
			 * @hooked chap_read_more         - 10
			 * @hooked chap_sticky_post_label - 20
			 * @hooked chap_category_labels   - 30
			 * @hooked chap_tag_labels        - 40
			 */
			do_action('chap_render_extras'); ?>
		</div>

	</div>

</article>

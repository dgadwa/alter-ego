<?php
/**
 * Content in a blog loop, showing content instead of an excerpt.
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

global $chap_grid;
$chap_grid['rendered']++;
$slider_context = isset($GLOBALS['csc']) ? $GLOBALS['csc']['slider'] : false;
if($chap_grid['rendered'] > $chap_grid['columns'] && !$slider_context) {
	echo '</div><div class="row">';
	$chap_grid['rendered'] = 1;
}
?>
<article <?php post_class('column'); ?>>

	<div class="content">

		<a class="marginless header" href="<?php the_permalink(); ?>">
			<h1 class="ui header entry-title"><?php the_title(); ?></h1>
		</a>

		<div class="meta entry-meta">
			<?php
			/**
			 * The chap_render_entry_meta hook.
			 *
			 * @hooked chap_render_post_meta - 10
			 */
			do_action('chap_render_entry_meta', false, true); ?>
		</div>

		<?php if(has_post_thumbnail() && Chap\Options\get('loop_thumbnail_size') !== 'none'): ?>
		<a class="image" href="<?php the_permalink(); ?>"><?php the_post_thumbnail(Chap\Options\get('loop_thumbnail_size'), ['class' => 'ui fluid image']); ?></a>
		<?php endif; ?>

		<div class="entry-content">
			<?php if(has_excerpt(get_the_ID())): ?>
				<?php the_excerpt(); ?>
			<?php else: ?>
				<?php the_content(); ?>
			<?php endif; ?>
		</div>

		<div class="extra">
			<?php
			/**
			 * The chap_render_extras hook.
			 *
			 * @hooked chap_read_more         - 10
			 * @hooked chap_sticky_post_label - 20
			 * @hooked chap_category_labels   - 30
			 * @hooked chap_tag_labels        - 40
			 */
			do_action('chap_render_extras'); ?>
		</div>

	</div>

</article>

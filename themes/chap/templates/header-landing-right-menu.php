<?php
/**
 * Flexible version of the landing page header with brand and right aligned main menu.
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}
?>
<div class="ui container">
	<div class="ui branding mainmenu flex-grid">
		<div class="brand column">
			<?php do_action('chap_render_brand', true); ?>
		</div>
		<div class="mainmenu column">
			<?php do_action('chap_render_main_menu', [
				'widgets' => false,
				'right' => true,
				'compact' => true,
			]); ?>
		</div>
	</div>
</div>

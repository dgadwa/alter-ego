<?php
/**
 * Landing page header width a compact menu.
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}
?>
<div class="ui mainmenu container">
	<nav class="<?php echo esc_attr(apply_filters('chap_utility_menu_classes', '')); ?>">
		<a class="compact toc item"><i class="sidebar icon"></i> <?php esc_html_e('Menu', 'chap'); ?></a>
		<div class="right header-widgets menu"><?php dynamic_sidebar('sidebar-header'); ?></div>
	</nav>
</div>

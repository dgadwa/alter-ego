<?php
/**
 * Single page content.
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}
?>

<article <?php post_class('content'); ?>>

	<?php do_action('chap_render_page_title'); ?>

	<?php do_action('chap_render_featured_image'); ?>

	<?php the_content(); ?>

</article>

<?php if(!is_page_template('template-full-width.php')): ?>
<div class="ui hidden clearing divider"></div>
<?php endif; ?>

<?php do_action('chap_after_page_content'); ?>

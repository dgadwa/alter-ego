<?php
/**
 * Branded header with compact menu.
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}
?>
<div class="ui branding mainmenu container">
	<nav class="<?php echo esc_attr(apply_filters('chap_utility_menu_classes', '')); ?>">
		<div class="brand item">
			<?php do_action('chap_render_brand', true); ?>
		</div>
		<a class="compact toc item"><i class="sidebar icon"></i> <?php esc_html_e('Menu', 'chap'); ?></a>
		<div class="right header-widgets menu">
			<?php dynamic_sidebar('sidebar-header'); ?>
		</div>
	</nav>
</div>

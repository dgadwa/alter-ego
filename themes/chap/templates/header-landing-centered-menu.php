<?php
/**
 * Centered menu with menu widgets.
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}
?>
<div class="ui mainmenu container">
	<?php do_action('chap_render_main_menu', [
		'widgets' => true,
		'compact' => true,
	]); ?>
</div>

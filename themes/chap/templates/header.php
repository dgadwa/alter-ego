<?php
/**
 * Render the header.
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

do_action('chap_before_header_content');

/**
 * The chap_render_header hook.
 *
 * @hooked chap_render_header_open             - 10
 * @hooked chap_render_background_video        - 20
 * @hooked chap_render_header_template         - 30
 * @hooked chap_render_before_masthead_content - 40
 * @hooked chap_render_masthead_slider         - 50
 * @hooked chap_render_masthead_title          - 60
 * @hooked chap_render_after_masthead_content  - 70
 * @hooked chap_render_header_close            - 80
 */
do_action('chap_render_header');

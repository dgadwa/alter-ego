<?php
/**
 * Single post content.
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}
?>

<article <?php post_class('content'); ?>>

	<?php do_action('chap_render_post_title'); ?>

	<?php do_action('chap_render_featured_image'); ?>

	<?php the_content(); ?>

</article>

<div class="ui hidden clearing divider"></div>

<?php do_action('chap_after_post_content'); ?>

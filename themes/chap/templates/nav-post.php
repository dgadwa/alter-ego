<?php
/**
 * Posts navigation: previous/next post links.
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

$in_same_term = apply_filters('chap_post_nav_same_term', true);
$prev = get_previous_post($in_same_term);
$next = get_next_post($in_same_term);
if(!$prev && !$next) {
	return;
}
$left = is_rtl() ? 'right' : 'left';
$right = is_rtl() ? 'left' : 'right';
?>
<div class="ui section divider"></div>
<div class="ui two column middle aligned stackable divided post-nav grid">
	<?php if($prev): ?>
	<a class="left floated mca column" href="<?php echo esc_url(get_the_permalink($prev->ID)); ?>">
		<div class="ui link items">
			<div class="item">
				<?php if(has_post_thumbnail($prev)): ?>
				<div class="ui tiny image"><?php echo get_the_post_thumbnail($prev->ID, 'thumbnail'); ?></div>
				<?php endif; ?>
				<div class="middle aligned content">
					<div class="header"><?php echo esc_html($prev->post_title); ?></div>
					<div class="meta">
						<span><i class="small <?php echo esc_attr($left); ?> chevron icon"></i></span>
						<span><?php esc_html_e('Previous post', 'chap'); ?></span>
					</div>
				</div>
			</div>
		</div>
	</a>
	<?php endif; ?>
	<?php if($next): ?>
	<a class="right floated right aligned mca column" href="<?php echo esc_url(get_the_permalink($next->ID)); ?>">
		<div class="ui link items">
			<div class="item">
				<?php if(has_post_thumbnail($next)): ?>
				<div class="ui tiny image"><?php echo get_the_post_thumbnail($next->ID, 'thumbnail'); ?></div>
				<?php endif; ?>
				<div class="middle aligned content">
					<div class="header"><?php echo esc_html($next->post_title); ?></div>
					<div class="meta">
						<span><?php esc_html_e('Next post', 'chap'); ?></span>
						<span><i class="small <?php echo esc_attr($right); ?> chevron icon"></i></span>
					</div>
				</div>
			</div>
		</div>
	</a>
	<?php endif; ?>
</div>

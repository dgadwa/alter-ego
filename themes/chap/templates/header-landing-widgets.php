<?php
/**
 * Flexible version of the landing page header with main menu on the left and header widgets on the right.
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}
?>
<div class="ui container">
	<div class="ui mainmenu header-template-landing-widgets flex-grid">
		<div class="mainmenu column">
			<?php do_action('chap_render_main_menu', [
				'widgets' => false,
				'left' => true,
				'compact' => true,
			]); ?>
		</div>
		<div class="column">
			<div class="<?php echo esc_attr(apply_filters('chap_utility_menu_classes', '')); ?>">
				<div class="right header-widgets menu">
					<?php dynamic_sidebar('sidebar-header'); ?>
				</div>
			</div>
		</div>
	</div>
</div>

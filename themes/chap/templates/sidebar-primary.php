<?php
/**
 * Primary sidebar.
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

dynamic_sidebar('sidebar-primary');

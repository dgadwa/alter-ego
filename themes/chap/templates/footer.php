<?php
/**
 * Render the footer.
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

do_action('chap_before_footer_content');

/**
 * The chap_render_footer hook.
 *
 * @hooked chap_render_footer_open     - 10
 * @hooked chap_render_footer_widgets  - 20
 * @hooked chap_render_footer_divider  - 30
 * @hooked chap_render_footer_image    - 40
 * @hooked chap_render_footer_menu     - 50
 * @hooked chap_render_footer_text     - 60
 * @hooked chap_render_footer_close    - 70
 */
do_action('chap_render_footer');

do_action('chap_after_footer_content');

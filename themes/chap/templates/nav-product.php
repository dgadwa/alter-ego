<?php
/**
 * Products navigation: previous/next product links.
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

$in_same_term = apply_filters('chap_product_nav_same_term', false);
$prev = get_previous_post($in_same_term);
$next = get_next_post($in_same_term);
if(!$prev && !$next) {
	return;
}
?>
<div class="ui hidden section divider"></div>
<div class="ui section divider"></div>
<div class="ui two column middle aligned stackable divided post-nav grid">
	<?php if($prev): $product = wc_get_product($prev->ID); ?>
	<a class="left floated mca column" href="<?php echo esc_url(get_the_permalink($prev->ID)); ?>">
		<div class="ui link items">
			<div class="item">
				<?php if(has_post_thumbnail($prev)): ?>
				<div class="ui tiny image"><?php echo get_the_post_thumbnail($prev->ID, 'thumbnail'); ?></div>
				<?php endif; ?>
				<div class="middle aligned content">
					<div class="header"><?php echo esc_html($prev->post_title); ?></div>
					<div class="marginless extra"><?php echo $product->get_price_html(); ?></div>
					<div class="meta">
						<span><i class="small left chevron icon"></i></span>
						<span><?php esc_html_e('Previous product', 'chap'); ?></span>
					</div>
				</div>
			</div>
		</div>
	</a>
	<?php endif; ?>
	<?php if($next): $product = wc_get_product($next->ID); ?>
	<a class="right floated right aligned mca column" href="<?php echo esc_url(get_the_permalink($next->ID)); ?>">
		<div class="ui link items">
			<div class="item">
				<?php if(has_post_thumbnail($next)): ?>
				<div class="ui tiny image"><?php echo get_the_post_thumbnail($next->ID, 'thumbnail'); ?></div>
				<?php endif; ?>
				<div class="middle aligned content">
					<div class="header"><?php echo esc_html($next->post_title); ?></div>
					<div class="marginless extra"><?php echo $product->get_price_html(); ?></div>
					<div class="meta">
						<span><?php esc_html_e('Next product', 'chap'); ?></span>
						<span><i class="small right chevron icon"></i></span>
					</div>
				</div>
			</div>
		</div>
	</a>
	<?php endif; ?>
</div>

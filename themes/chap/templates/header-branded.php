<?php
/**
 * Branded header.
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

do_action('chap_render_branding_container');
do_action('chap_render_main_menu_container');

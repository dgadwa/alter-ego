<?php
/**
 * Comments template.
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

if(is_front_page() || post_password_required()) {
	return;
}

?>

<?php if(have_comments()): ?>

<section id="comments" class="ui threaded comments">

		<h2 class="ui dividing header">
			<?php
				printf(
					esc_html(
						_nx(
							'One response to &ldquo;%2$s&rdquo;',
							'%1$s responses to &ldquo;%2$s&rdquo;',
							get_comments_number(),
							'comments title',
							'chap'
						)
					),
					number_format_i18n(get_comments_number()),
					'<span>' . get_the_title() . '</span>'
				);
			?>
		</h2>

		<?php wp_list_comments(); ?>

		<?php if(get_comment_pages_count() > 1 && get_option('page_comments')): ?>

			<div class="ui hidden divider"></div>

			<div class="ui centered grid">

				<div class="<?php echo Chap\Options\get('pagination_alignment'); ?> aligned column">

					<div class="ui comment-pagination pagination menu">

						<?php previous_comments_link(esc_html__('Older comments', 'chap')); ?>

						<?php next_comments_link(esc_html__('Newer comments', 'chap')); ?>

					</div>

				</div>

			</div>

		<?php endif; ?>

</section>

<?php endif; // have_comments() ?>

<?php if(!comments_open() && get_comments_number() > 0 && post_type_supports(get_post_type(), 'comments')): ?>

	<div class="ui info message"><?php esc_html_e('Comments are closed.', 'chap'); ?></div>

<?php else: ?>

	<?php comment_form(); ?>

<?php endif; ?>

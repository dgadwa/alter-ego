<?php
/**
 * Items in search results loop.
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

get_template_part('templates/content'); ?>

<?php
/**
 * Header with centered brand.
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}
?>

<div class="ui branding container">
	<?php do_action('chap_render_brand', true); ?>
</div>

<?php do_action('chap_render_main_menu_container'); ?>

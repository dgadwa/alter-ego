<?php
/**
 * Slide.
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}
?>
<div class="swiper-slide swiper-slide-active">

	<?php

		do_action('chap_render_slide_open');

		do_action('chap_render_slide_loading');

		do_action('chap_render_edit_slide');

		the_content();

		do_action('chap_render_slide_close');

	?>

</div>

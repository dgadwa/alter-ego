<?php
/**
 * Grid header.
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}
?>
<div class="ui container">
	<div class="ui mainmenu branding header-template-grid flex-grid">
		<div class="static brand column">
			<?php do_action('chap_render_brand', true); ?>
		</div>
		<div class="column">
			<div class="<?php echo esc_attr(apply_filters('chap_utility_menu_classes', '')); ?>">
				<div class="right header-widgets menu">
					<?php dynamic_sidebar('sidebar-header'); ?>
				</div>
			</div>
			<?php do_action('chap_render_main_menu', [
				'right' => true,
				'compact' => true,
			]); ?>
		</div>
	</div>
</div>

<?php
/**
 * Card in a cards loop.
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}
?>
<article <?php post_class('ui card'); ?>>

	<?php if(has_post_thumbnail() && Chap\Options\get('loop_thumbnail_size') !== 'none'): ?>
	<a class="image" href="<?php the_permalink(); ?>"><?php the_post_thumbnail(Chap\Options\get('loop_thumbnail_size')); ?></a>
	<?php endif; ?>

	<div class="content">

		<a class="header" href="<?php the_permalink(); ?>">
			<h2 class="ui <?php echo esc_attr(Chap\Options\get('loop_header_size')); ?> header entry-title"><?php the_title(); ?></h2>
		</a>

		<div class="meta">
			<?php
			/**
			 * The chap_render_entry_meta hook.
			 *
			 * @hooked chap_render_post_meta - 10
			 */
			do_action('chap_render_entry_meta', false, true); ?>
		</div>

		<?php if((int)apply_filters('excerpt_length', 55) > 0): ?>
		<div class="description entry-summary"><?php the_excerpt(); ?></div>
		<?php endif; ?>

	</div>

	<?php do_action('chap_render_card_extras'); ?>

</article>

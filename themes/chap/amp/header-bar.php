<?php
/**
 * Header bar template.
 */
if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}
$theme_location = \Chap\Options\get('amp_main_menu_menu_separate') ? 'amp_navigation' : 'primary_navigation';
$this->load_parts(['sidebar']); ?>
<header id="#top" class="amp-wp-header">
	<div>
		<?php if(has_nav_menu($theme_location)): ?>
		<button class="hamburger fa fa-bars" on="tap:sidebar-menu.toggle"></button>
		<?php endif; ?>
		<a class="brand" href="<?php echo esc_url(home_url(apply_filters('chap_amp_home_url_path', '/'))); ?>">
			<?php echo esc_html($this->get('blog_name')); ?>
		</a>
		<button class="search fa fa-search" on="tap:sidebar-search.toggle"></button>
	</div>
</header>

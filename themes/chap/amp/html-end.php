<?php
/**
 * End HTML output on all AMP pages.
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}
?>
		<?php $amp->load_parts(['footer']); ?>
		<?php do_action('amp_post_template_footer', $amp); ?>
	</body>
</html>

<?php
/**
 * Begin HTML output on all AMP pages.
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}
?><!doctype html>
<html amp <?php echo AMP_HTML_Utils::build_attributes_string($amp->get('html_tag_attributes')); ?>>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no">
		<?php do_action('amp_post_template_head', $amp); ?>
		<style amp-custom><?php do_action('chap_amp_load_css', $amp); ?></style>
	</head>
	<body class="<?php echo esc_attr($amp->get('body_class')); ?>">
		<?php $amp->load_parts(['header-bar']); ?>
		<?php $amp->load_parts(['slider']); ?>
		<?php do_action('chap_amp_ad_slot', 1); ?>

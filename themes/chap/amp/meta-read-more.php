<?php
/**
 * Read more button.
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

$post_id = get_the_ID();

$class = 'more';
$url = add_query_arg(AMP_QUERY_VAR, 1, get_the_permalink());
$text = __('Read more', 'chap');

/**
 * Product.
 */
if(Chap\Helpers\is_product($post_id)) {
	$class .= ' product';
	$text = __('View product', 'chap');
}

$text = apply_filters('chap_read_more_text', $text, $post_id);

?><a class="ui <?php echo esc_attr($class); ?> button" href="<?php echo esc_url($url); ?>"><?php echo esc_html($text); ?></a>

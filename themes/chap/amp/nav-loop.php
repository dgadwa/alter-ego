<?php
/**
 * Loop items pagination.
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

$is_shop = function_exists('is_shop') && is_shop();

$prev_text = $is_shop ? esc_html__('Newer products', 'chap') : esc_html__('Newer posts', 'chap');
$prev = apply_filters('chap_amp_prev_posts_link', $prev_text);

$next_text = $is_shop ? esc_html__('Older products', 'chap') : esc_html__('Older posts', 'chap');
$next = apply_filters('chap_amp_next_posts_link', $next_text);

if(!$prev && !$next) {
	return;
}

?>
<div class="amp-posts-nav">
	<div>
		<amp-fit-text width="160" height="20" layout="fixed" max-font-size="16"><?php echo wp_kses_post($prev); ?></amp-fit-text>
		<amp-fit-text width="160" height="20" layout="fixed" max-font-size="16"><?php echo wp_kses_post($next); ?></amp-fit-text>
	</div>
</div>

<?php
/**
 * Search results pagination.
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

$prev = apply_filters('chap_amp_prev_posts_link', esc_html__('Previous results', 'chap'));
$next = apply_filters('chap_amp_next_posts_link', esc_html__('Next results', 'chap'));

if(!$prev && !$next) {
	return;
}

?>
<div class="amp-posts-nav">
	<div>
		<amp-fit-text width="160" height="20" layout="fixed" max-font-size="16"><?php echo wp_kses_post($prev); ?></amp-fit-text>
		<amp-fit-text width="160" height="20" layout="fixed" max-font-size="16"><?php echo wp_kses_post($next); ?></amp-fit-text>
	</div>
</div>

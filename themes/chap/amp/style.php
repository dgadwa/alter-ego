<?php

namespace Chap\AMP;
use Chap\Assets;
use Chap\Options;

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

$content_max_width       = Options\get('amp_content_max_width') . 'px';

$dark_text_color         = Options\get('amp_dark_text_color');
$light_text_color        = Options\get('amp_light_text_color');
$muted_text_color        = Options\get('amp_muted_text_color');

$primary_color           = Options\get('amp_primary_color') ? Options\get('amp_primary_color') : Options\get('sui_primary_color');
$secondary_color         = Options\get('amp_secondary_color') ? Options\get('amp_secondary_color') : Options\get('sui_secondary_color');

$header_background_color = Options\get('amp_header_background_color')
							? Options\get('amp_header_background_color')
							: Options\get('header_background_color');
$header_text_color       = Options\get('amp_header_text_color')
							? Options\get('amp_header_text_color')
							: (Options\get('header_inverted') ? $light_text_color : $dark_text_color);

$slider_background_color = Options\get('amp_slider_background_color')
							? Options\get('amp_slider_background_color')
							: Options\get('header_background_color');

$page_background_color   = Options\get('amp_page_background_color') ? Options\get('amp_page_background_color') : Options\get('sui_page_background_color');
$text_color              = Options\get('amp_page_text_color') ? Options\get('amp_page_text_color') : Options\get('sui_page_text_color');

$footer_background_color = Options\get('amp_footer_background_color')
							? Options\get('amp_footer_background_color')
							: Options\get('footer_background_color');
$footer_text_color       = Options\get('amp_footer_text_color')
							? Options\get('amp_footer_text_color')
							: (Options\get('footer_inverted') ? $light_text_color : $dark_text_color);

$button_font_weight       = Options\get('amp_button_font_bold') ? 'bold' : 'normal';
$border_radius            = Options\get('sui_border_radius');

$header_font              = apply_filters('chap_amp_header_font', "'Helvetica Neue', Arial, Helvetica, sans-serif");
$sui_header_font          = Options\get('sui_header_font');
$header_font_style        = $sui_header_font['font-style'];
$header_font_weight       = $sui_header_font['font-weight'];
$slide_header_font_weight = Options\get('sui_slide_header_font_weight');

$page_font                = apply_filters('chap_amp_page_font', "'Helvetica Neue', Arial, Helvetica, sans-serif");
$sui_page_font            = Options\get('sui_font');
$page_font_weight         = $sui_page_font['font-weight'];

$link_color               = Options\get('amp_link_color') ? Options\get('amp_link_color') : Options\get('sui_link_color');
$link_underline           = apply_filters('chap_amp_link_underline', []);

/**
 * Load style files.
 */
$styles = apply_filters('chap_amp_styles', []);
foreach(Assets\theme_files('amp/styles', '{' . join(',', array_keys($styles)) . '}.php') as $file) {
	include_once $file;
}

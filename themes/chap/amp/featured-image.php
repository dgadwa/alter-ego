<?php
/**
 * Featured image template.
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

$featured_image = $this->get('featured_image');
if(empty($featured_image)) {
	return;
}

$id = get_the_ID();
/**
 * Post/page specific featured image type.
 */
$type = Chap\Options\get('featured_image_position', $id);
/**
 * Default featured image type.
 */
$type = (empty($type) || $type === 'default') ? Chap\Options\get('featured_image_type') : $type;
/**
 * Featured image type filter.
 */
$type = apply_filters('chap_featured_image_type', $type, $id);
/**
 * Don't display featured image.
 */
if($type === 'hidden') {
	return;
}

$amp_html = $featured_image['amp_html'];
$caption = $featured_image['caption']; ?>
<figure class="amp-wp-article-featured-image wp-caption">
	<?php echo($amp_html); // amphtml content; no kses ?>
	<?php if($caption): ?>
	<p class="wp-caption-text"><?php echo wp_kses_data($caption); ?></p>
	<?php endif; ?>
</figure>

<?php
/**
 * Social sharing links template.
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

$share = Chap\Options\get('amp_social_share');
if(count($share) < 1) {
	return;
}
?>
<div class="amp-social-share-links">
	<h5 class="ui marginless header"><?php esc_html_e('Share this page', 'chap'); ?></h5>
	<?php foreach($share as $id): ?>
	<?php $app_id = $id === 'facebook' ? ' data-param-app_id="' . esc_attr(Chap\Options\get('amp_social_facebook_app_id')) . '"' : ''; ?>
	<amp-social-share type="<?php echo esc_attr($id); ?>"<?php echo wp_kses_data($app_id); ?>></amp-social-share>
	<?php endforeach; ?>
</div>

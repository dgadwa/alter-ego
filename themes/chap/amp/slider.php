<?php
/**
 * AMP slider template.
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}
?>

<?php do_action('chap_render_amp_slider'); ?>

<?php
/**
 * AMP slide template.
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}
?>

<?php

	do_action('chap_render_slide_open');

	the_content();

	do_action('chap_render_slide_close');

?>

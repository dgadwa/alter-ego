<?php
/**
 * Single post template.
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}
?>
<?php do_action('chap_amp_header', $this); ?>

	<article class="amp-wp-article">

		<header class="amp-wp-article-header">
			<h1 class="amp-wp-title"><?php echo wp_kses_data($this->get('post_title')); ?></h1>
			<?php $this->load_parts(apply_filters('amp_post_article_header_meta', ['meta-author', 'meta-time'])); ?>
		</header>

		<?php $this->load_parts(['featured-image']); ?>

		<div class="amp-wp-article-content">
			<?php echo($this->get('post_amp_content')); // amphtml content; no kses ?>
		</div>

		<footer class="amp-wp-article-footer">

			<?php $this->load_parts(apply_filters('amp_post_article_footer_meta', ['meta-taxonomy'])); ?>

			<?php if(Chap\Options\get('post_comments')): ?>
				<?php $this->load_parts(['meta-comments-link']); ?>
			<?php endif; ?>

			<?php if(in_array(get_post_type(), Chap\Options\get('amp_social_post_types'))): ?>
				<?php $this->load_parts(['social-share']); ?>
			<?php endif; ?>

		</footer>

	</article>

	<?php $this->load_parts(['nav-posts']); ?>

<?php do_action('chap_amp_footer', $this); ?>

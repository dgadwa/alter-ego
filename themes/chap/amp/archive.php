<?php
/**
 * Archive template.
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}
?>
<?php do_action('chap_amp_header', $this); ?>

	<div class="amp-container">
		<h1><?php echo Chap\Titles\title(); ?></h1>
	</div>

	<?php $this->load_parts([
		'loop',
		'nav-loop',
	]); ?>

<?php do_action('chap_amp_footer', $this); ?>

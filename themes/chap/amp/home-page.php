<?php
/**
 * Home page template.
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}
?>
<?php do_action('chap_amp_header', $this); ?>

	<article class="amp-wp-article amp-wp-home-page">

		<?php $this->load_parts(['featured-image']); ?>

		<div class="amp-wp-article-content">
			<?php echo($this->get('post_amp_content')); // amphtml content; no kses ?>
		</div>

	</article>

<?php do_action('chap_amp_footer', $this); ?>

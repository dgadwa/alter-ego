<?php
/**
 * Posts loop.
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}
?>
<div class="ui divided amp-items amp-content">
<?php if(have_posts()): ?>
	<?php while(have_posts()): the_post(); ?>
	<article class="item" id="<?php echo basename(get_permalink()); ?>">
		<?php if(has_post_thumbnail()): ?>
		<div class="image">
			<?php do_action('chap_amp_thumbnail', get_the_ID()); ?>
		</div>
		<?php endif; ?>
		<div class="content">
			<a class="header" href="<?php echo add_query_arg(AMP_QUERY_VAR, 1, get_the_permalink()); ?>">
				<h2><?php the_title(); ?></h2>
			</a>
			<?php $this->load_parts(['meta-read-more']); ?>
		</div>
	</article>
	<?php endwhile; ?>
<?php else: ?>
	<div class="center aligned item">
		<h2><?php esc_html_e('No results found.', 'chap'); ?></h2>
	</div>
<?php endif; ?>
</div>

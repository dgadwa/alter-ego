<?php
/**
 * Footer styles.
 *
 * Variables defined in chap/amp/style.php.
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}
?>
.amp-wp-footer {
	color: <?php echo esc_html($footer_text_color); ?>;
	background: <?php echo esc_html($footer_background_color); ?>;
	margin: calc(1.5em - 1px) 0 0;
	clear: both;
}

.amp-posts-nav + .amp-wp-footer {
	margin-top: 0;
}

.amp-wp-footer .footer-content {
	margin: 0 auto;
	max-width: <?php echo esc_attr($content_max_width); ?>;
	padding: 1.25em 16px 1.25em;
	position: relative;
}

.amp-wp-footer h2 {
	font-size: 1em;
	line-height: 1.375em;
	margin: 0 0 .5em;
}

.amp-wp-footer p {
	font-size: .8em;
	line-height: 1.5em;
	margin: 0 85px 0 0;
}

.amp-wp-footer a {
	text-decoration: none;
	color: <?php echo esc_html($footer_text_color); ?>;
	opacity: .8;
}

.amp-wp-footer a:hover,
.amp-wp-footer a:active,
.amp-wp-footer a:focus {
	opacity: 1;
}

.back-to-top {
	bottom: 1.275em;
	font-size: .8em;
	line-height: 2em;
	position: absolute;
	right: 16px;
}

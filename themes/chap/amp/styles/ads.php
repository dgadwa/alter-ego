<?php
/**
 * Advertisement styles.
 *
 * Variables defined in chap/amp/style.php.
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}
?>
amp-ad.centered {
	display: block;
	margin: 0 auto;
}
.amp-wp-footer > amp-ad:first-child {
	padding-top: 1.5em;
}

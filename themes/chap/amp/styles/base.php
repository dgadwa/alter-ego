<?php
/**
 * Base styles.
 *
 * Variables defined in chap/amp/style.php.
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}
?>
*,
*:before,
*:after {
	box-sizing: inherit;
}

html {
	box-sizing: border-box;
	background: <?php echo esc_html($page_background_color); ?>;
}

body {
	background: <?php echo esc_html($page_background_color); ?>;
	color: <?php echo esc_html($text_color); ?>;
	font-family: <?php echo wp_kses_data($page_font); ?>;
	font-weight: <?php echo wp_kses_data($page_font_weight); ?>;
	line-height: 1.75em;
	padding-top: 54px;
	display: flex;
	min-height: 100vh;
	flex-direction: column;
	direction: ltr;
}

<?php /* Must specify font-family or user agent will prefer it's own value. */ ?>
button {
	font-family: <?php echo wp_kses_data($page_font); ?>;
}

p,
ol,
ul,
figure {
	margin: 0 0 1em;
	padding: 0;
}

a,
a:visited {
	color: <?php echo esc_html($link_color); ?>;
	text-decoration: <?php echo esc_html($link_underline['underline']); ?>;
}

.ui.inverted:not(.menu) a,
.ui.inverted:not(.menu) a:visited {
	color: <?php echo esc_html($light_text_color); ?>;
}

a:hover,
a:active,
a:focus {
	color: <?php echo esc_html($primary_color); ?>;
	text-decoration: <?php echo esc_html($link_underline['underline_hover']); ?>;
}

p:last-child {
	margin-bottom: 0;
}

blockquote, address {
	color: <?php echo esc_html($text_color); ?>;
	background: rgba(127, 127, 127, 0.02);
	border-left: 3px solid <?php echo esc_html($primary_color); ?>;
	margin: 8px 0 24px 0;
	padding: 4px 16px;
}

blockquote p:last-child {
	margin-bottom: 0;
}

pre {
	overflow: scroll;
}

.ui.avatar.image img {
	border-radius: 500rem;
}

.ui.divided.image {
	margin: 0.5em 0 3em;
}

.small.fa-chevron-right,
.small.fa-chevron-left {
	font-size: .8em;
	margin-left: .25em;
	margin-right: .25em;
}

<?php
/**
 * Button styles.
 *
 * Variables defined in chap/amp/style.php.
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}
?>
.ui.button {
	cursor: pointer;
	display: inline-block;
	background: #e0e1e2 none;
	color: rgba(0, 0, 0, .6);
	margin: 0 .25em .25em 0;
	padding: .785em 1.5em .785em;
	font-weight: <?php echo esc_html($button_font_weight); ?>;
	font-size: 14px;
	line-height: 1em;
	text-align: center;
	border-radius: <?php echo esc_html($border_radius); ?>px;
	border: 0;
}
.ui.button:hover,
.ui.button:active,
.ui.button:focus {
	background-color: #cacbcd;
	color: rgba(0, 0, 0, .8);
}
.ui.icon.button {
	padding: .785em 1.15em;
}

.ui.compact.button {
	padding: .589em 1.125em .589em;
}

.ui.primary.button {
	color: #fff;
	background-color: <?php echo esc_html($primary_color); ?>;
}

.ui.secondary.button {
	color: #fff;
	background-color: <?php echo esc_html($secondary_color); ?>;
}

.ui.positive.button {
	color: #fff;
	background-color: #16ab39;
}

.ui.negative.button {
	color: #fff;
	background-color: #DB2828;
}

.ui.basic.button {
	background: transparent none;
	color: rgba(0, 0, 0, .6);
	font-weight: normal;
	box-shadow: 0 0 0 1px rgba(34, 36, 38, .15) inset;
}

<?php /* Fake color change on interacton since we can't calculate a darker or ligter color. */ ?>
.ui.primary.button:hover,
.ui.secondary.button:hover,
.ui.positive.button:hover,
.ui.negative.button:hover {
	opacity: .95;
}
.ui.primary.button:active,
.ui.secondary.button:active,
.ui.positive.button:active,
.ui.negative.button:active,
.ui.primary.button:focus,
.ui.secondary.button:focus,
.ui.positive.button:focus,
.ui.negative.button:focus {
	opacity: .9;
}

.ui.basic.button:hover,
.ui.basic.button:active,
.ui.basic.button:focus {
	box-shadow: 0 0 0 1px rgba(34, 36, 38, .3) inset;
}

<?php /* Stackable buttons */ ?>
.ui.stackable.buttons {
	display: inline-flex;
	flex-direction: column;
}
.ui.stackable.buttons .button {
	display: block;
	float: none;
	width: 100%;
	margin: 0;
	box-shadow: none;
	border-radius: 0;
}
.ui.stackable.buttons .button:first-child {
	border-top-left-radius: .285rem;
	border-top-right-radius: .285rem;
}
.ui.stackable.buttons .button:last-child {
	margin-bottom: 0;
	border-bottom-left-radius: .285rem;
	border-bottom-right-radius: .285rem;
}
.ui.stackable.buttons .button:only-child {
	border-radius: .285rem;
}

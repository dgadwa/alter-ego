<?php
/**
 * Social sharing styles.
 *
 * Variables defined in chap/amp/style.php.
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}
?>

.amp-wp-article-footer > .amp-social-share-links:first-child {
	margin-top: 2em;
}

.amp-social-share-links {
	text-align: center;
}

<?php
/**
 * RTL styles.
 *
 * Variables defined in chap/amp/style.php.
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}
?>
<?php /* CSS flipper removes original margin which uses calc() */ ?>
.amp-wp-article {
	margin: 1.5em auto;
}
<?php /* Transition doesn't work right with RTL */ ?>
.chap-amp-menu section > h5:after {
	transition: none;
}

<?php
/**
 * Slider styles.
 *
 * Variables defined in chap/amp/style.php.
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}
?>
.chap-amp-slider {
	background-color: <?php echo esc_html($slider_background_color); ?>;
}

.chap-amp-slider .amp-carousel-button-prev {
	z-index: 3;
	margin-left: 16px;
}

.chap-amp-slider .amp-carousel-button-next {
	z-index: 3;
	margin-right: 16px;
}
<?php /* Selector for .i-amphtml-slide-item */ ?>
.chap-amp-slider > div > div {
	min-height: calc(100% + 20px); <?php /* Cover the sliders horizontal scrollbar */ ?>
}
.chap-amp-slider .ui.slide:before {
	content: '';
	margin-top: -20px; <?php /* Offset the covered scrollbar */ ?>
}

.ui.slide {
	display: flex;
	flex-direction: column;
	align-items: center;
	justify-content: center;
	text-align: center;
	padding: 0 34px;
	max-width: calc(<?php echo esc_attr($content_max_width); ?> - 16px * 2);
	line-height: initial;
}
.ui.slide:after {
	max-width: calc(<?php echo esc_attr($content_max_width); ?> + 18px * 2);
}

<?php /* Slide content alignment */ ?>
.ui.slide[class*="left aligned"] {
	align-items: flex-start;
	text-align: left;
}
.ui.slide[class*="right aligned"] {
	align-items: flex-end;
	text-align: right;
}
.ui.slide[class*="top aligned"] {
	justify-content: flex-start;
}
.ui.slide[class*="bottom aligned"] {
	justify-content: flex-end;
}

.ui.slide .amp-items {
	flex: 0;
}
.ui.slide .ui.amp-items > .item {
	margin: 0;
}

.ui.slide p.inverted,
.ui.slide .ui.inverted.header,
.ui.slide .ui.inverted.header .header {
	color: white;
}

.ui.slide > * {
	z-index: 1;
}
.ui.slide > amp-img {
	z-index: 0;
	max-width: calc(600px + 16px * 2);
	margin: 0 auto;
}

.ui.slide .ui.header {
	font-weight: <?php echo esc_attr($slide_header_font_weight); ?>;
	margin: 0 0 0.3em 0;
}
.ui.slide .ui.massive.header {
	font-size: 4em;
	margin-top: -0.2em;
}
<?php /* Enforce the .marginless class with more specificity. */ ?>
.ui.slide .ui.marginless.header {
	margin-left: 0;
	margin-right: 0;
	margin-top: 0;
	margin-bottom: 0;
}

.ui.slide .ui.transparent.segment {
	background-color: rgba(255, 255, 255, .85);
}
.ui.slide .ui.inverted.transparent.segment {
	background-color: rgba(27, 28, 29, .5);
	color: #fff;
}
.ui.slide .ui.inverted.transparent.segment .ui.header {
	color: #fff;
}

.ui.slide .ui.inverted.container {
	color: #fff;
}

.ui.slide .ui.inverted.button {
	box-shadow: 0 0 0 2px #fff inset;
	background: transparent none;
	color: white;
}

.ui.slide .ui.inverted.button:hover,
.ui.slide .ui.inverted.button:active,
.ui.slide .ui.inverted.button:focus {
	background: #fff;
	color: rgba(0, 0, 0, .8);
}

.ui.slide .ui.inverted.basic.button {
	background-color: transparent;
	color: #f9fafb;
	box-shadow: 0 0 0 2px rgba(255, 255, 255, .7) inset;
}
.ui.slide .ui.inverted.basic.button:hover,
.ui.slide .ui.inverted.basic.button:active,
.ui.slide .ui.inverted.basic.button:focus {
	box-shadow: 0 0 0 2px #fff inset;
}

.ui.slide > amp-img {
	user-select: none;
}
.ui.slide > amp-img + * {
	margin-top: 0;
}

.chap-amp-slider + amp-ad {
	margin-top: 1.5em;
}
.chap-amp-slider + .ui.amp-items {
	margin-top: 1em;
}

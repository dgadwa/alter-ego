<?php
/**
 * Container styles.
 *
 * Variables defined in chap/amp/style.php.
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}
?>
.amp-container {
	max-width: calc(<?php echo esc_attr($content_max_width); ?> - 16px * 2);
	padding: 0 16px;
	margin: 0 auto;
}

header + .amp-container {
	margin-top: 1em;
}

amp-ad + .amp-container {
	margin-top: 1.5em;
}

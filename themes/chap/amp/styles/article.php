<?php
/**
 * Article styles.
 *
 * Variables defined in chap/amp/style.php.
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}
?>
<?php /* Article. */ ?>
.amp-wp-article {
	color: <?php echo esc_html($text_color); ?>;
	margin: 1.5em auto calc(1.5em + 18px);
	width: 100%;
	max-width: <?php echo esc_attr($content_max_width); ?>;
	overflow-wrap: break-word;
	word-wrap: break-word;
	flex: 1;
}

<?php /* Article header. */ ?>
.amp-wp-article-header {
	align-items: center;
	align-content: stretch;
	display: flex;
	flex-wrap: wrap;
	justify-content: space-between;
	margin: 1.5em 16px 1em;
}

.amp-wp-title {
	color: <?php echo esc_html($text_color); ?>;
	display: block;
	flex: 1 0 100%;
	margin: 0 0 .5em;
}

<?php /* Article meta */ ?>
.amp-wp-meta {
	color: <?php echo esc_html($muted_text_color); ?>;
	display: inline-block;
	flex: 2 1 50%;
	font-size: .875em;
	line-height: 1.5em;
	margin: 0;
	padding: 0;
}

.amp-wp-article-header .amp-wp-meta:last-of-type {
	text-align: right;
}

.amp-wp-article-header .amp-wp-meta:first-of-type {
	text-align: left;
}

.amp-wp-byline amp-img,
.amp-wp-byline .amp-wp-author {
	display: inline-block;
	vertical-align: middle;
}

.amp-wp-byline amp-img {
	border: 1px solid <?php echo esc_html($link_color); ?>;
	border-radius: 50%;
	position: relative;
	margin-right: 6px;
}

.amp-wp-posted-on {
	text-align: right;
}

<?php /* Featured image. */ ?>
.amp-wp-article-featured-image {
	margin: 0 0 1em;
}
.amp-wp-article-featured-image amp-img {
	margin: 0 auto;
	max-width: calc(<?php echo esc_attr($content_max_width); ?> - 16px * 2);
}
.amp-wp-article-featured-image.wp-caption .wp-caption-text {
	margin: 0 18px;
}

<?php /* Article content. */ ?>
.amp-wp-article-content {
	margin: 0 16px;
}

.amp-wp-article-content ul,
.amp-wp-article-content ol {
	margin-left: 1em;
}

.amp-wp-article-content amp-img {
	margin: 0 auto;
}

.amp-wp-article-content amp-img.alignright {
	margin: 0 0 1em 16px;
}

.amp-wp-article-content amp-img.alignleft {
	margin: 0 16px 1em 0;
}

<?php /* Captions. */ ?>
.wp-caption {
	padding: 0;
}

.wp-caption.alignleft {
	margin-right: 16px;
}

.wp-caption.alignright {
	margin-left: 16px;
}

.wp-caption .wp-caption-text {
	color: <?php echo esc_html($muted_text_color); ?>;
	font-size: .875em;
	line-height: 1.5em;
	margin: 0;
	padding: .66em 10px .75em;
}

<?php /* AMP media. */ ?>
amp-carousel {
	background: <?php echo esc_html($page_background_color); ?>;
	margin: 0 -16px 1.5em;
}
amp-iframe,
amp-youtube,
amp-instagram,
amp-vine {
	background: <?php echo esc_html($secondary_color); ?>;
	margin: 0 -16px 1.5em;
}

.amp-wp-article-content amp-carousel amp-img {
	border: none;
}

amp-carousel > amp-img > img {
	object-fit: contain;
}

.amp-wp-iframe-placeholder {
	background: <?php echo esc_html($secondary_color); ?> url(<?php echo esc_url($this->get('placeholder_image_url')); ?>) no-repeat center 40%;
	background-size: 48px 48px;
	min-height: 48px;
}

<?php /* Article footer meta. */ ?>
.amp-wp-article-footer .amp-wp-meta {
	display: block;
}

.amp-wp-tax-category,
.amp-wp-tax-tag,
.amp-wp-tax-sku {
	color: <?php echo esc_html($muted_text_color); ?>;
	font-size: .875em;
	line-height: 1.5em;
	margin: 1.5em 16px;
}

.amp-wp-comments-link {
	text-align: center;
	margin: 2.25em 0 1.5em;
}

.amp-wp-comments-link a {
	max-width: 200px;
	width: 50%;
}

.amp-wp-header + .amp-wp-home-page {
	margin-top: 3em;
}

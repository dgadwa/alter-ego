<?php
/**
 * Pagination styles.
 *
 * Variables defined in chap/amp/style.php.
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}
?>
.amp-posts-nav {
	background: <?php echo esc_html($primary_color); ?>;
}

.amp-posts-nav > div {
	margin: 0 auto;
	display: flex;
	justify-content: space-between;
	max-width: <?php echo esc_attr($content_max_width); ?>;
	padding: 1.25em 16px 1.25em;
	position: relative;
}

.amp-posts-nav > div > amp-fit-text:last-child {
	text-align: right;
}

.amp-posts-nav a {
	color: <?php echo esc_html($light_text_color); ?>;
}

.amp-posts-nav amp-fit-text:first-child a i {
	margin-right: 0.5em;
}
.amp-posts-nav amp-fit-text:last-child a i {
	margin-left: 0.5em;
}

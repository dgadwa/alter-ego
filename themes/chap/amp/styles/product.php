<?php
/**
 * Product styles.
 *
 * Variables defined in chap/amp/style.php.
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}
?>
.amp-product-title {
	margin: 0;
}
.price {
	font-size: 1.6em;
	margin: 0;
}
.price del {
	font-size: 0.7em;
}
.product-action {
	display: flex;
	justify-content: space-between;
	align-items: center;
}
.product-action .ui.product.button {
	flex-basis: 136px;
	margin: 0;
}

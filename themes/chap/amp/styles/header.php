<?php
/**
 * Header styles.
 *
 * Variables defined in chap/amp/style.php.
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}
?>
h1, h2, h3, h4, h5,
.amp-wp-header div,
.amp-wp-title,
.ui.header {
	font-family: <?php echo wp_kses_data($header_font); ?>;
	font-weight: <?php echo wp_kses_data($header_font_weight); ?>;
	font-style: <?php echo wp_kses_data($header_font_style); ?>;
}

.amp-wp-header {
	position: fixed;
	top: 0;
	width: 100%;
	min-height: 54px;
	font-size: 1.6em;
	background-color: <?php echo esc_html($header_background_color); ?>;
	box-shadow: 0 0 4px rgba(0, 0, 0, 0.14), 0 4px 8px rgba(0, 0, 0, 0.28);
	z-index: 100;
}

.amp-wp-header > div {
	margin: 0 auto;
	display: flex;
	align-items: stretch;
	justify-content: space-between;
	max-width: <?php echo esc_attr($content_max_width); ?>;
	min-height: 54px;
}

.amp-wp-header button.hamburger {
	background: transparent;
	border: none;
	color: <?php echo esc_html($header_text_color); ?>;
	width: 64px;
	cursor: pointer;
}

.amp-wp-header a.brand {
	display: flex;
	align-items: center;
	color: <?php echo esc_html($header_text_color); ?>;
	text-decoration: none;
	margin-right: auto;
	margin-left: 0.5em;
	font-weight: normal;
}

.amp-wp-header button.hamburger + a.brand {
	margin-left: 0;
}

.amp-wp-header button.search {
	background: transparent;
	border: none;
	color: <?php echo esc_html($header_text_color); ?>;
	width: 64px;
	cursor: pointer;
}

.amp-wp-header + .ui.amp-items {
	margin-top: 1em;
}

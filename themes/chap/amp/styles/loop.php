<?php
/**
 * Loop styles.
 *
 * Variables defined in chap/amp/style.php.
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}
?>
<?php /* AMP items, lightweight/compact version of SUI .ui.items. */ ?>
.ui.amp-items {
	margin: 0 auto 1.5em;
	width: 100%;
	max-width: <?php echo esc_attr($content_max_width); ?>;
	flex: 1;
}

.ui.amp-items > .item {
	display: flex;
	margin: 1.5em 16px;
}
.ui.amp-items > .item > .image:not(.ui) {
	width: 100px;
}

.ui.amp-items > .item > .content {
	display: flex;
	flex-grow: 1;
	justify-content: space-between;
	align-items: center;
}
.ui.amp-items > .item > .image + .content {
	padding-left: 1em;
	flex-direction: column;
	align-items: initial;
}

.ui.amp-items > .item > .content > .header {
	color: <?php echo esc_attr($text_color); ?>;
}
.ui.amp-items > .item > .content > .header > .ui.header,
.ui.amp-items > .item > .content > .header > h2 {
	margin: 0;
	font-size: 1.2em;
	line-height: 1.1em;
}

.ui.amp-items > .item > .content > .ui.more.button {
	margin: 0;
	margin-left: 1em;
	margin-top: auto;
	min-width: 126px;
	align-self: flex-end;
}
.ui.amp-items > .item > .image + .content > .ui.more.button {
	margin-left: 0;
}

.ui.divided.amp-items > .item {
  border-top: 1px solid rgba(34, 36, 38, 0.15);
  padding: 1.5em 0;
  margin-top: 0;
  margin-bottom: 0;
}
.ui.divided.amp-items > .item:first-child {
  border-top: none;
}

.ui.amp-items > .center.aligned.item > * {
	margin: 0 auto;
}

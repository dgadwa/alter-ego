<?php
/**
 * Generic styles.
 *
 * Variables defined in chap/amp/style.php.
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}
?>
.alignright {
	float: right;
}

.alignleft {
	float: left;
}

.aligncenter {
	display: block;
	margin-left: auto;
	margin-right: auto;
}

.amp-wp-enforced-sizes {
	max-width: 100%;
	margin: 0 auto;
}

.amp-wp-unknown-size img {
	object-fit: contain;
}

code {
	background-color: rgba(0, 0, 0, 0.08);
	border-radius: 3px;
	display: inline-block;
	font-family: "Monaco", "Menlo", "Ubuntu Mono", "Consolas", "source-code-pro", monospace;
	font-size: 0.875em;
	font-weight: bold;
	padding: 1px 6px;
	vertical-align: baseline;
	line-height: calc(1.75em - 2px);
}

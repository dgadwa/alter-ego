<?php
/**
 * Helper CSS classes on AMP pages.
 *
 * Using CSS specificity hack because !important is not allowed in AMP.
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}
?>
.invisible.invisible.invisible,
.amp-invisible.amp-invisible.amp-invisible,
.mobile-invisible.mobile-invisible.mobile-invisible {
	display: none;
}
.borderless.borderless.borderless {
	border-top: 0;
	border-right: 0;
	border-bottom: 0;
	border-left: 0;
}
.paddingless.paddingless.paddingless {
	padding-top: 0;
	padding-right: 0;
	padding-bottom: 0;
	padding-left: 0;
}
.marginless.marginless.marginless {
	margin-top: 0;
	margin-right: 0;
	margin-bottom: 0;
	margin-left: 0;
}
.shadowless.shadowless {
	text-shadow: none;
	box-shadow: none;
}
.containing.containing {
	overflow: hidden;
}
.mla {
	text-align: left;
}
.mca {
	text-align: center;
}
.mra {
	text-align: right;
}
.mj {
	text-align: justify;
}
.normal.text:not(.container) {
	font-weight: normal;
}
.bold.text:not(.container) {
	font-weight: bold;
}
.italic.text:not(.container) {
	font-style: italic;
}
.white.text:not(.container) {
	color: #fff;
}

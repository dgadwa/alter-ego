<?php
/**
 * Sidebar styles.
 *
 * Variables defined in chap/amp/style.php.
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}
?>

#sidebar-menu h5 {
	margin: 0;
}

#sidebar-menu .branding {
	width: 100%;
	min-height: 200px;
	font-size: 1.6em;
	display: flex;
	flex-direction: column;
	align-items: center;
	justify-content: center;
	text-align: center;
	background-color: <?php echo esc_html($header_background_color); ?>;
	color: <?php echo esc_html($header_text_color); ?>;
	padding: .5em 0;
}
#sidebar-menu .branding .custom-logo {
	margin-bottom: .25em;
}

#sidebar-menu button.close {
	color: <?php echo esc_html($header_text_color); ?>;
	font-size: 1em;
	background: transparent;
	border: none;
	width: 54px;
	height: 54px;
	position: absolute;
	top: 0;
	right: 0;
	cursor: pointer;
}

#sidebar-search {
	width: 80vw;
	height: 54px;
	font-size: 1.6em;
	background-color: #f9f9f9;
	box-shadow: 0 0 4px rgba(0, 0, 0, .14), 0 4px 8px rgba(0, 0, 0, .28);
}

.amp-search button.search {
	color: <?php echo esc_html($dark_text_color); ?>;
	background: transparent;
	border: none;
	width: 54px;
	height: 54px;
	float: right;
	cursor: pointer;
}

.amp-search input.search {
	color: <?php echo esc_html($dark_text_color); ?>;
	font-size: 1em;
	padding: 0 1em;
	background: transparent;
	border: none;
	width: calc(100% - 54px - 1px);
	height: 54px;
	line-height: 54px;
}

<?php /* Sidebar menu. */ ?>
.chap-amp-menu {
	margin-top: .5em;
}

.chap-amp-menu,
.chap-amp-menu ul {
	list-style: none;
}

.chap-amp-menu > li {
	display: block;
}

.chap-amp-menu a {
	color: <?php echo esc_html($muted_text_color); ?>;
	display: block;
	height: 44px;
	line-height: 44px;
	padding-left: 1em;
	overflow: hidden;
}

.chap-amp-menu section > h5 {
	border: 0;
}

.chap-amp-menu section > h5:after {
	font-size: .75em;
	font-family: 'FontAwesome';
	content: '\f054';
	color: <?php echo esc_html($muted_text_color); ?>;
	position: absolute;
	top: 0;
	right: 0;
	width: 44px;
	line-height: 44px;
	text-align: center;
	transition: .5s;
	background-color: rgba(230, 230, 230, .5);
}

.chap-amp-menu section[expanded] {
	background-color: rgba(230, 230, 230, .5);
}

.chap-amp-menu section[expanded] > a {
	background-color: #e2e2e2;
}

.chap-amp-menu section[expanded] > h5:after {
	transform: rotate(90deg);
}

.chap-amp-menu ul.sub-menu {
	padding-left: 1em;
}

.chap-amp-menu .ui.label,
.chap-amp-menu i.icon {
	display: none;
}
.chap-amp-menu .fa {
	min-width: 1.5em;
}

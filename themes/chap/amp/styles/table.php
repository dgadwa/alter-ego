<?php
/**
 * Table styles.
 *
 * Variables defined in chap/amp/style.php.
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}
?>

.ui.table {
	width: 100%;
	border: 1px solid rgba(34, 36, 38, 0.15);
	border-radius: <?php echo esc_html($border_radius); ?>px;
	text-align: left;
	border-spacing: 0;
}

.ui.table thead,
.ui.table tbody,
.ui.table tfoot,
.ui.table tr,
.ui.table tr > th,
.ui.table tr > td {
	width: auto;
	display: block;
}

.ui.table tr {
	padding-top: 1em;
	padding-bottom: 1em;
}
.ui.table tr:not(:last-child) {
	border-bottom: 1px solid rgba(34, 36, 38, 0.15);
}
.ui.table tr > th,
.ui.table tr > td {
	background: none;
	border: none;
	padding: 0.25em 0.75em;
}

.ui.table tr:first-child td {
	border-top: none;
}
.ui.table th:first-child,
.ui.table td:first-child {
	font-weight: bold;
}
.ui.table td {
	padding: 0.785em 0.785em;
	text-align: inherit;
}

.ui.table thead tr,
.ui.table tfoot tr {
	padding: 0;
}
.ui.table thead th,
.ui.table tfoot th {
	background: #F9FAFB;
	padding: 0.928em 0.785em;
}
.ui.table thead th {
	border-bottom: 1px solid rgba(34, 36, 38, 0.1);
}
.ui.table tfoot th {
	border-top: 1px solid rgba(34, 36, 38, 0.15);
}

.ui.attached.table {
	top: 0;
	bottom: 0;
	border-radius: 0;
	margin: -1px 0;
	width: calc(100% + 2px);
	max-width: calc(100% + 2px);
}
.ui.attached + .ui.attached.table:not(.top) {
	border-top: none;
}
.ui[class*="top attached"].table {
	bottom: 0;
	margin-bottom: 0;
	top: 0;
	margin-top: 1em;
	border-radius: <?php echo esc_html($border_radius); ?>px <?php echo esc_html($border_radius); ?>px 0 0;
}
.ui.table[class*="top attached"]:first-child {
	margin-top: 0;
}
.ui[class*="bottom attached"].table {
	bottom: 0;
	margin-top: 0;
	top: 0;
	margin-bottom: 1em;
	border-radius: 0 0 <?php echo esc_html($border_radius); ?>px <?php echo esc_html($border_radius); ?>px;
}
.ui[class*="bottom attached"].table:last-child {
	margin-bottom: 0;
}

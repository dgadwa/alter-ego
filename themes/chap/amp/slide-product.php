<?php
/**
 * WC Product AMP slide.
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

$chap_product_id        = intval(get_the_ID());
$chap_product           = wc_get_product($chap_product_id);
$chap_inverted          = Chap\Options\get('header_inverted') ? 'inverted' : 'default';
$chap_button_text       = Chap\Options\get('product_slide_button_text', $chap_product_id);
$chap_view_product_link = add_query_arg(AMP_QUERY_VAR, 1, $chap_product->get_permalink());
$chap_view_product_text = !empty($chap_button_text) ? esc_html($chap_button_text) : esc_html__('View product', 'chap');
?>

<?php do_action('chap_render_slide_open'); ?>
	<div class="ui amp-items">
		<div class="item">
			<?php if(has_post_thumbnail()): ?>
			<div class="image">
			<?php
				list($sanitized_thumbnail) = \AMP_Content_Sanitizer::sanitize(
					get_the_post_thumbnail($chap_product_id, 'thumbnail'),
					['AMP_Img_Sanitizer' => []],
					['content_max_width' => 100]
				);
				echo($sanitized_thumbnail); // AMP html, no kses.
			?>
			</div>
			<?php endif; ?>
			<div class="content">
				<div class="header">
					<div class="ui large <?php echo $chap_inverted; ?> header">
						<?php the_title(); ?>
						<div class="sub header">
							<?php echo wp_kses_post($chap_product->get_price_html()); ?>
						</div>
					</div>
				</div>
				<a class="ui positive more button" href="<?php echo esc_url($chap_view_product_link); ?>">
					<?php echo esc_html($chap_view_product_text); ?>
				</a>
			</div>
		</div>
	</div>
<?php do_action('chap_render_slide_close'); ?>

<?php
/**
 * AMP footer template.
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}
?>
<footer class="amp-wp-footer">
	<?php do_action('chap_amp_ad_slot', 3); ?>
	<div class="footer-content">
		<h2><?php echo esc_html($this->get('blog_name')); ?></h2>
		<?php if(Chap\Options\get('amp_show_non_amp_version_link')): ?>
		<p><?php printf(
			esc_html__('%sView non-AMP version%s', 'chap'),
			'<a href="' . esc_url(remove_query_arg('amp')) . '">',
			'</a>'
		); ?></p>
		<?php endif; ?>
		<p><?php echo wp_kses_post(do_shortcode(Chap\Options\get('amp_footer_text'))); ?></p>
		<a href="#top" class="back-to-top"><?php esc_html_e('Back to top', 'chap'); ?></a>
	</div>
</footer>

<?php
/**
 * WC product template.
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

global $product;
if(!isset($product->id)) {
	$product = wc_get_product(get_the_ID());
}
?>
<?php do_action('chap_amp_header', $this); ?>

	<article class="amp-wp-article">

		<header class="amp-wp-article-header">
			<h1 class="amp-product-title"><?php echo wp_kses_data($this->get('post_title')); ?></h1>
		</header>

		<?php $this->load_parts(['featured-image']); ?>

		<div class="amp-wp-article-content">

			<?php echo($this->get('post_amp_content')); // amphtml content; no kses ?>

			<div class="product-action">
				<?php wc_get_template_part('single-product/price'); ?>
				<a class="ui primary product button" href="<?php the_permalink(); ?>"><?php esc_html_e('View product', 'chap'); ?></a>
			</div>

		</div>

		<footer class="amp-wp-article-footer">

			<?php if(wc_product_sku_enabled() && ($product->get_sku() || $product->is_type('variable'))): ?>
			<div class="amp-wp-meta amp-wp-tax-sku">
				<?php esc_html_e('SKU:', 'chap'); ?> <?php echo ($sku = $product->get_sku()) ? $sku : esc_html__('N/A', 'chap'); ?>
			</div>
			<?php endif; ?>

			<?php
				if(version_compare(WC_VERSION, '3.0', '<')) {
					$categories = $product->get_categories(_x(', ', 'Used between list items, there is a space after the comma.', 'chap'));
				} else {
					$categories = wc_get_product_category_list($product->get_id(), _x(', ', 'Used between list items, there is a space after the comma.', 'chap'));
				}
			?>
			<?php if($categories): ?>
			<div class="amp-wp-meta amp-wp-tax-category"><?php printf(esc_html__('Categories: %s', 'chap'), $categories); ?></div>
			<?php endif; ?>

			<?php
				if(version_compare(WC_VERSION, '3.0', '<')) {
					$tags = $product->get_tags(_x(', ', 'Used between list items, there is a space after the comma.', 'chap'));
				} else {
					$tags = wc_get_product_tag_list($product->get_id(), _x(', ', 'Used between list items, there is a space after the comma.', 'chap'));
				}
			?>
			<?php if($tags && !is_wp_error($tags)): ?>
			<div class="amp-wp-meta amp-wp-tax-tag"><?php printf(esc_html__('Tags: %s', 'chap'), $tags); ?></div>
			<?php endif; ?>

			<?php if(in_array(get_post_type(), Chap\Options\get('amp_social_post_types'))): ?>
				<?php $this->load_parts(['social-share']); ?>
			<?php endif; ?>

		</footer>

	</article>

	<?php $this->load_parts(['nav-posts']); ?>

<?php do_action('chap_amp_footer', $this); ?>

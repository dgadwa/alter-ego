<?php
/**
 * Comments link template.
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

$comments_link_url = $this->get('comments_link_url');
if(!$comments_link_url) {
	return;
}
$comments_link_text = $this->get('comments_link_text');
?>
<div class="amp-wp-meta amp-wp-comments-link">
	<a class="ui button" href="<?php echo esc_url($comments_link_url); ?>"><?php echo esc_html($comments_link_text); ?></a>
</div>

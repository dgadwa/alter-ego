<?php
/**
 * Posts navigation.
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

$prev = apply_filters('chap_amp_prev_post_link', false);
$next = apply_filters('chap_amp_next_post_link', false);

if(!$prev && !$next) {
	return;
}

?>
<div class="amp-posts-nav">
	<div>
		<amp-fit-text width="160" height="20" layout="fixed" max-font-size="16" sizes="(min-width:768px) 350px, (min-width:425px) 212px, (min-width:375px) 185px, 160px"><?php echo wp_kses_post($prev); ?></amp-fit-text>
		<amp-fit-text width="160" height="20" layout="fixed" max-font-size="16" sizes="(min-width:768px) 350px, (min-width:425px) 212px, (min-width:375px) 185px, 160px"><?php echo wp_kses_post($next); ?></amp-fit-text>
	</div>
</div>

<?php
/**
 * AMP sidebars template.
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}
$theme_location = \Chap\Options\get('amp_main_menu_menu_separate') ? 'amp_navigation' : 'primary_navigation';
?>
<?php if(has_nav_menu($theme_location)): ?>
<amp-sidebar id="sidebar-menu" layout="nodisplay" sizes="(min-width:500px) 360px, 80vw">
	<a href="<?php echo esc_url(home_url(apply_filters('chap_amp_home_url_path', '/'))); ?>" class="branding">
		<button class="close" on="tap:sidebar-menu.close" role="button">
			<i class="fa fa-close" aria-hidden="true"></i>
		</button>
		<?php do_action('chap_amp_render_brand'); ?>
		<?php if(Chap\Options\get('brand_show_site_title')): ?>
		<amp-fit-text width="230" height="36" layout="fixed" max-font-size="40"><?php echo esc_html(get_bloginfo('name')); ?></amp-fit-text>
		<?php endif; ?>
		<?php if(Chap\Options\get('brand_show_tagline')): ?>
		<amp-fit-text width="240" height="36" layout="fixed" max-font-size="20"><?php echo esc_html(get_bloginfo('description')); ?></amp-fit-text>
		<?php endif; ?>
	</a>
	<?php
		wp_nav_menu(
			[
				'theme_location' => $theme_location,
				'container' => '',
				'menu_id' => 'chap-amp-menu',
				'menu_class' => 'chap-amp-menu menu',
				'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
				'no_nav' => true,
				'walker' => new Chap\Walkers\Chap_Walker_Nav_Menu_Amp,
			]
		);
	?>
</amp-sidebar>
<?php endif; ?>
<amp-sidebar id="sidebar-search" layout="nodisplay" side="<?php echo is_rtl() ? 'left' : 'right'; ?>" sizes="(min-width:500px) 360px, 80vw">
	<form class="amp-search" role="search" method="get" target="_top" action="<?php echo str_replace('http://', '//', esc_url(home_url('/'))); ?>">
		<input type="search" class="search" name="s" placeholder="<?php echo esc_attr_x('Search&hellip;', 'placeholder', 'chap'); ?>" value="<?php echo get_search_query(); ?>" title="<?php echo esc_attr_x('Search for:', 'label', 'chap'); ?>" />
		<input type="hidden" name="<?php echo AMP_QUERY_VAR; ?>" value="1" />
		<button type="submit" class="search fa fa-search"></button>
	</form>
</amp-sidebar>

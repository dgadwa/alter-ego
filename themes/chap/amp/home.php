<?php
/**
 * Home page template (posts loop).
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}
?>

<?php do_action('chap_amp_header', $this); ?>

	<?php $this->load_parts([
		'loop',
		'nav-loop',
	]); ?>

<?php do_action('chap_amp_footer', $this); ?>

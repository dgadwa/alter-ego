<?php
/**
 * Chap Slide template.
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}
?>
<?php do_action('chap_amp_header', $this); ?>

	<article class="amp-wp-article">

		<div class="amp-wp-article-content">
			<?php echo($this->get('post_amp_content')); // amphtml content; no kses ?>
		</div>

		<footer class="amp-wp-article-footer">
			<?php if(in_array(get_post_type(), Chap\Options\get('amp_social_post_types'))): ?>
				<?php $this->load_parts(['social-share']); ?>
			<?php endif; ?>
		</footer>

	</article>

<?php do_action('chap_amp_footer', $this); ?>

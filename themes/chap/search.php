<?php
/**
 * Search results page template.
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

do_action('chap_render_page_title');

if(!have_posts()):

	/**
	 * No results.
	 */
	get_template_part('templates/no-results');

else:

	/**
	 * Open loop.
	 */
	switch(Chap\Options\get('loop_type')):

		case 'grid':
			do_action('chap_render_loop_grid_open');
		break;

		case 'cards':
			do_action('chap_render_loop_cards_open');
		break;

		default:
			do_action('chap_render_loop_items_open');

	endswitch;

	/**
	 * Render loop content.
	 *
	 * Defaults to templates/content.php
	 */
	while(have_posts()): the_post();
		get_template_part('templates/content', 'search');
	endwhile;

	/**
	 * Close loop.
	 */
	do_action('chap_render_loop_close');

	/**
	 * Render pagination.
	 */
	do_action('chap_render_posts_navigation', [
		'prev_text' => esc_html__('Older results', 'chap'),
		'next_text' => esc_html__('Newer results', 'chap'),
	]);

endif;

<?php
/**
 * Search form template.
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}
?>
<form role="search" method="get" class="ui fluid left icon action input search-form" action="<?php echo esc_url(home_url('/')); ?>">
	<i class="search icon"></i>
	<input type="text" class="search-field" placeholder="<?php echo esc_attr_x('Search&hellip;', 'placeholder', 'chap'); ?>" value="<?php echo get_search_query(); ?>" name="s" title="<?php echo esc_attr_x('Search for:', 'label', 'chap'); ?>" />
	<button type="submit" class="ui button search-submit"><?php echo esc_attr_x('Search', 'submit button', 'chap'); ?></button>
</form>

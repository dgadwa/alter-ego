<?php
/**
 * Base template.
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}
?><!doctype html>
<html <?php language_attributes(); ?>>
	<?php get_template_part('templates/head'); ?>
	<body <?php body_class(); ?>>
		<?php do_action('chap_render_loading_screen'); ?>
		<?php do_action('chap_render_mobile_menu'); ?>
		<?php do_action('chap_render_sticky_menu'); ?>
		<div class="pusher">
			<div class="full height">
				<?php do_action('chap_render_browser_warning'); ?>
				<?php do_action('get_header'); ?>
				<?php get_template_part('templates/header'); ?>
				<?php do_action('chap_render_main_container'); ?>
				<?php do_action('get_footer'); ?>
				<?php get_template_part('templates/footer'); ?>
			</div>
		</div>
		<?php wp_footer(); ?>
	</body>
</html>

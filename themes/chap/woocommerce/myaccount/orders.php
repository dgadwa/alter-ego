<?php
/**
 * Orders
 *
 * Shows orders on the account page.
 *
 * This is Chap theme's version of a WooCommerce template, overriding the original.
 * To override this in a Chap child theme, copy it to chap-child/woocommerce/myaccount/orders.php.
 *
 * @version 3.2.0
 */

if(!defined('ABSPATH')) {
	exit;
}

do_action('woocommerce_before_account_orders', $has_orders); ?>

<?php if($has_orders): ?>

	<table class="ui unstackable padded table woocommerce-orders-table woocommerce-MyAccount-orders shop_table shop_table_responsive my_account_orders account-orders-table">
		<thead>
			<tr>
				<?php foreach(wc_get_account_orders_columns() as $column_id => $column_name): ?>
					<th class="woocommerce-orders-table__header woocommerce-orders-table__header-<?php echo esc_attr($column_id); ?>"><span class="nobr"><?php echo esc_html($column_name); ?></span></th>
				<?php endforeach; ?>
			</tr>
		</thead>

		<tbody>
			<?php
			foreach($customer_orders->orders as $customer_order):
				$order      = wc_get_order($customer_order);
				$item_count = $order->get_item_count();
				?>
				<tr class="woocommerce-orders-table__row woocommerce-orders-table__row--status-<?php echo esc_attr($order->get_status()); ?> order">
					<?php foreach(wc_get_account_orders_columns() as $column_id => $column_name): ?>
						<td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-<?php echo esc_attr($column_id); ?>" data-title="<?php echo esc_attr($column_name); ?>">

							<?php if(has_action('woocommerce_my_account_my_orders_column_' . $column_id)): ?>

								<?php do_action('woocommerce_my_account_my_orders_column_' . $column_id, $order); ?>

							<?php elseif('order-number' === $column_id): ?>

								<a href="<?php echo esc_url($order->get_view_order_url()); ?>">
									<?php echo _x('#', 'hash before order number', 'chap') . $order->get_order_number(); ?>
								</a>

							<?php elseif('order-date' === $column_id): ?>

								<?php if(version_compare(WC_VERSION, '3.0', '<')): ?>
								<time datetime="<?php echo date('Y-m-d', strtotime($order->order_date)); ?>" title="<?php echo esc_attr(strtotime($order->order_date)); ?>"><?php echo date_i18n(get_option('date_format'), strtotime($order->order_date)); ?></time>
								<?php else: ?>
								<time datetime="<?php echo esc_attr($order->get_date_created()->date('c')); ?>"><?php echo esc_html(wc_format_datetime($order->get_date_created())); ?></time>
								<?php endif; ?>

							<?php elseif('order-status' === $column_id): ?>

								<?php echo esc_html(wc_get_order_status_name($order->get_status())); ?>

							<?php elseif('order-total' === $column_id): ?>

								<?php
								/* translators: 1: formatted order total 2: total order items */
								printf(_n('%1$s for %2$s item', '%1$s for %2$s items', $item_count, 'chap'), $order->get_formatted_order_total(), $item_count);
								?>

							<?php elseif('order-actions' === $column_id): ?>

								<?php
								$actions = wc_get_account_orders_actions($order);
								if(!empty($actions)) {
									foreach($actions as $key => $action) {
										echo '<a href="' . esc_url($action['url']) . '" class="ui compact ' . sanitize_html_class($key) . ' button">' . esc_html($action['name']) . '</a>';
									}
								}
								?>

							<?php endif; ?>
						</td>
					<?php endforeach; ?>
				</tr>
			<?php endforeach; ?>
		</tbody>
	</table>

	<?php do_action('woocommerce_before_account_orders_pagination'); ?>

	<?php if(1 < $customer_orders->max_num_pages): ?>
		<div class="ui buttons">
			<?php if($current_page !== 1): ?>
				<a class="ui labeled icon button" href="<?php echo esc_url(wc_get_endpoint_url('orders', $current_page - 1)); ?>"><i class="left arrow icon"></i><?php esc_html_e('Previous', 'chap'); ?></a>
			<?php endif; ?>

			<?php if(intval($customer_orders->max_num_pages) !== $current_page): ?>
				<a class="ui right labeled icon button" href="<?php echo esc_url(wc_get_endpoint_url('orders', $current_page + 1)); ?>"><i class="right arrow icon"></i><?php esc_html_e('Next', 'chap'); ?></a>
			<?php endif; ?>
		</div>
	<?php endif; ?>

<?php else: ?>
	<div class="ui action info message">
		<a class="ui button" href="<?php echo esc_url(apply_filters('woocommerce_return_to_shop_redirect', wc_get_page_permalink('shop'))); ?>">
			<?php esc_html_e('Go shop', 'chap'); ?>
		</a>
		<?php esc_html_e('No order has been made yet.', 'chap'); ?>
	</div>
<?php endif; ?>

<?php do_action('woocommerce_after_account_orders', $has_orders); ?>

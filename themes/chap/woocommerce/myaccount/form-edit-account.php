<?php
/**
 * Edit account form
 *
 * This is Chap theme's version of a WooCommerce template, overriding the original.
 * To override this in a Chap child theme, copy it to chap-child/woocommerce/myaccount/form-edit-account.php.
 *
 * @version 3.3.0
 */

if(!defined('ABSPATH')) {
	exit;
}

do_action('woocommerce_before_edit_account_form'); ?>

<form class="ui woocommerce-EditAccountForm edit-account form" action="" method="post">

	<?php do_action('woocommerce_edit_account_form_start'); ?>

	<div class="two fields">
	<div class="field required">
		<label for="account_first_name"><?php esc_html_e('First name', 'chap'); ?></label>
		<input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="account_first_name" id="account_first_name" value="<?php echo esc_attr($user->first_name); ?>" />
	</div>
	<div class="field required">
		<label for="account_last_name"><?php esc_html_e('Last name', 'chap'); ?></label>
		<input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="account_last_name" id="account_last_name" value="<?php echo esc_attr($user->last_name); ?>" />
	</div>
	</div>

	<div class="field required">
		<label for="account_email"><?php esc_html_e('Email address', 'chap'); ?></label>
		<input type="email" class="woocommerce-Input woocommerce-Input--email input-text" name="account_email" id="account_email" value="<?php echo esc_attr($user->user_email); ?>" />
	</div>

	<h4 class="ui dividing header"><?php esc_html_e('Password Change', 'chap'); ?></h4>
	<div class="field">
		<label for="password_current"><?php esc_html_e('Current Password (leave blank to leave unchanged)', 'chap'); ?></label>
		<input type="password" class="woocommerce-Input woocommerce-Input--password input-text" name="password_current" id="password_current" />
	</div>
	<div class="field">
		<label for="password_1"><?php esc_html_e('New Password (leave blank to leave unchanged)', 'chap'); ?></label>
		<input type="password" class="woocommerce-Input woocommerce-Input--password input-text" name="password_1" id="password_1" />
	</div>
	<div class="field">
		<label for="password_2"><?php esc_html_e('Confirm New Password', 'chap'); ?></label>
		<input type="password" class="woocommerce-Input woocommerce-Input--password input-text" name="password_2" id="password_2" />
	</div>

	<?php do_action('woocommerce_edit_account_form'); ?>

	<?php wp_nonce_field('save_account_details'); ?>
	<button type="submit" class="ui large primary woocommerce-Button button" name="save_account_details" value="<?php esc_attr_e('Save changes', 'chap'); ?>"><?php esc_html_e('Save changes', 'chap'); ?></button>
	<input type="hidden" name="action" value="save_account_details" />

	<?php do_action('woocommerce_edit_account_form_end'); ?>

</form>

<?php do_action('woocommerce_after_edit_account_form'); ?>

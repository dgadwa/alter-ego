<?php
/**
 * My Account navigation
 *
 * This is Chap theme's version of a WooCommerce template, overriding the original.
 * To override this in a Chap child theme, copy it to chap-child/woocommerce/myaccount/navigation.php.
 *
 * @version 2.6.0
 */

if(!defined('ABSPATH')) {
	exit;
}

do_action('woocommerce_before_account_navigation');
?>

<nav class="ui secondary vertical fluid woocommerce-MyAccount-navigation menu">
	<?php foreach(wc_get_account_menu_items() as $endpoint => $label): ?>
		<?php
			$classes = wc_get_account_menu_item_classes($endpoint);
			$classes = str_replace('is-active', 'is-active active', $classes);
			$classes .= ' item';
		?>
		<a class="<?php echo esc_attr($classes); ?>" href="<?php echo esc_url(wc_get_account_endpoint_url($endpoint)); ?>"><?php echo wp_kses_post($label); ?></a>
	<?php endforeach; ?>
</nav>

<?php do_action('woocommerce_after_account_navigation'); ?>

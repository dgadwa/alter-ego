<?php
/**
 * Edit address form
 *
 * This is Chap theme's version of a WooCommerce template, overriding the original.
 * To override this in a Chap child theme, copy it to chap-child/woocommerce/myaccount/form-edit-address.php.
 *
 * @version 3.3.0
 */

if(!defined('ABSPATH')) {
	exit;
}

$page_title = $load_address === 'billing' ? esc_html__('Billing address', 'chap') : esc_html__('Shipping address', 'chap');

do_action('woocommerce_before_edit_account_address_form'); ?>

<?php if(!$load_address): ?>
	<?php wc_get_template('myaccount/my-address.php'); ?>
<?php else: ?>

	<form class="ui form" method="post">

		<h3 class="ui header"><?php echo apply_filters('woocommerce_my_account_edit_address_title', $page_title, $load_address); ?></h3>

		<?php do_action("woocommerce_before_edit_address_form_{$load_address}"); ?>

		<?php
		foreach($address as $key => $field) {
			array_push($field['class'], 'field');
			if(function_exists('wc_get_post_data_by_key') && isset($field['country_field'], $address[$field['country_field']])) {
				$field['country'] = wc_get_post_data_by_key($field['country_field'], $address[$field['country_field']]['value']);
			}
			ob_start();
			woocommerce_form_field($key, $field, !empty($_POST[$key]) ? wc_clean($_POST[$key]) : $field['value']);
			echo str_replace(['<p', '</p>'], ['<div', '</div>'], ob_get_clean());
		}
		?>

		<?php do_action("woocommerce_after_edit_address_form_{$load_address}"); ?>

		<button type="submit" class="ui large primary button" name="save_address" value="<?php esc_attr_e('Save address', 'chap'); ?>"><?php esc_html_e('Save address', 'chap'); ?></button>
		<?php wp_nonce_field('woocommerce-edit_address'); ?>
		<input type="hidden" name="action" value="edit_address" />

	</form>

<?php endif; ?>

<?php do_action('woocommerce_after_edit_account_address_form'); ?>

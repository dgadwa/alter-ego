<?php
/**
 * My Account page
 *
 * This is Chap theme's version of a WooCommerce template, overriding the original.
 * To override this in a Chap child theme, copy it to chap-child/woocommerce/myaccount/my-account.php.
 *
 * @version 2.6.0
 */

if(!defined('ABSPATH')) {
	exit;
}

wc_print_notices(); ?>

<div class="ui stackable grid">
	<div class="four wide column">
		<?php do_action('woocommerce_account_navigation'); ?>
	</div>
	<div class="twelve wide woocommerce-MyAccount-content column">
		<?php do_action('woocommerce_account_content'); ?>
	</div>
</div>

<?php
/**
 * Downloads
 *
 * Shows downloads on the account page.
 *
 * This is Chap theme's version of a WooCommerce template, overriding the original.
 * To override this in a Chap child theme, copy it to chap-child/woocommerce/myaccount/downloads.php.
 *
 * @version 3.2.0
 */

if(!defined('ABSPATH')) {
	exit;
}

$downloads     = WC()->customer->get_downloadable_products();
$has_downloads = (bool)$downloads;

do_action('woocommerce_before_account_downloads', $has_downloads); ?>

<?php if($has_downloads): ?>
	<?php do_action('woocommerce_before_available_downloads'); ?>
	<?php do_action('woocommerce_available_downloads', $downloads); ?>
	<?php do_action('woocommerce_after_available_downloads'); ?>
<?php else: ?>
	<div class="ui info action message">
		<a class="ui small button" href="<?php echo esc_url(apply_filters('woocommerce_return_to_shop_redirect', wc_get_page_permalink('shop'))); ?>">
			<?php esc_html_e('Go shop', 'chap'); ?>
		</a>
		<div class="content">
			<i class="info icon"></i>
			<?php esc_html_e('No downloads available yet.', 'chap'); ?>
		</div>
	</div>
<?php endif; ?>

<?php do_action('woocommerce_after_account_downloads', $has_downloads); ?>

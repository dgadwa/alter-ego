<?php
/**
 * Login Form
 *
 * This is Chap theme's version of a WooCommerce template, overriding the original.
 * To override this in a Chap child theme, copy it to chap-child/woocommerce/myaccount/form-login.php.
 *
 * @version 3.3.0
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

?>

<?php wc_print_notices(); ?>

<?php do_action('woocommerce_before_customer_login_form'); ?>

<?php if(get_option('woocommerce_enable_myaccount_registration') === 'yes'): ?>

<div class="u-columns col2-set" id="customer_login">

	<div class="u-column1 col-1">

<?php endif; ?>

		<h2 class="ui header"><?php esc_html_e('Login', 'chap'); ?></h2>

		<form method="post" class="ui large login form">
			<div class="ui middle aligned left aligned grid">
				<div class="column">

					<?php do_action('woocommerce_login_form_start'); ?>

					<div class="ui stacked segment">

						<div class="labelless field">
							<label for="username"><?php esc_html_e('Username or email address', 'chap'); ?> <span class="required">*</span></label>
							<div class="ui left icon input">
								<i class="user icon"></i>
								<?php $username_value = !empty($_POST['username']) ? ' value="' . esc_attr(wp_unslash($_POST['username'])) . '"' : ''; ?>
								<input type="text" class="input-text" placeholder="<?php esc_attr_e('Username or email address', 'chap'); ?>" name="username" id="username"<?php echo $username_value; ?> />
							</div>
						</div>

						<div class="labelless field">
							<label for="password"><?php esc_html_e('Password', 'chap'); ?> <span class="required">*</span></label>
							<div class="ui left icon input">
								<i class="lock icon"></i>
								<input class="input-text" placeholder="<?php esc_attr_e('Password', 'chap'); ?>" type="password" name="password" id="password" />
							</div>
						</div>

						<?php do_action('woocommerce_login_form'); ?>
						<?php wp_nonce_field('woocommerce-login', 'woocommerce-login-nonce'); ?>

						<div class="field">
							<div class="ui checkbox">
								<input name="rememberme" tabindex="0" class="hidden" type="checkbox" id="rememberme" value="forever" />
								<label for="rememberme"><?php esc_html_e('Remember me', 'chap'); ?></label>
							</div>
						</div>

						<button type="submit" class="ui huge primary button" name="login" value="<?php esc_attr_e('Login', 'chap'); ?>"><?php esc_html_e('Login', 'chap'); ?></button>

						<div class="ui info message">
							<a href="<?php echo esc_url(wp_lostpassword_url()); ?>"><?php esc_html_e('Lost your password?', 'chap'); ?></a>
						</div>

					</div>

					<?php do_action('woocommerce_login_form_end'); ?>

				</div>
			</div>
		</form>

<?php if(get_option('woocommerce_enable_myaccount_registration') === 'yes'): ?>

	</div>

	<div class="u-column2 col-2">

		<h2 class="ui header"><?php esc_html_e('Register', 'chap'); ?></h2>

		<form method="post" class="ui large register form">
			<div class="ui middle aligned left aligned grid">
				<div class="column">

					<?php do_action('woocommerce_register_form_start'); ?>
						<div class="ui stacked segment">

						<?php if(get_option('woocommerce_registration_generate_username') === 'no'): ?>

							<div class="labelless field">
								<label for="reg_username"><?php esc_html_e('Username', 'chap'); ?> <span class="required">*</span></label>
								<div class="ui left icon input">
									<i class="user icon"></i>
									<?php $username_value = !empty($_POST['username']) ? ' value="' . esc_attr(wp_unslash($_POST['username'])) . '"' : ''; ?>
									<input type="text" class="input-text" placeholder="<?php esc_attr_e('Username', 'chap'); ?>" name="username" id="reg_username"<?php echo $username_value; ?> />
								</div>
							</div>

						<?php endif; ?>

						<div class="labelless field">
							<label for="reg_email"><?php esc_html_e('Email address', 'chap'); ?> <span class="required">*</span></label>
							<div class="ui left icon input">
								<i class="user icon"></i>
								<?php $email_value = !empty($_POST['email']) ? ' value="' . esc_attr(wp_unslash($_POST['email'])) . '"' : ''; ?>
								<input type="email" class="input-text" placeholder="<?php esc_attr_e('Email address', 'chap'); ?>" name="email" id="reg_email"<?php echo $email_value; ?> />
							</div>
						</div>

						<?php if(get_option('woocommerce_registration_generate_password') === 'no'): ?>

							<div class="labelless field">
								<label for="reg_password"><?php esc_html_e('Password', 'chap'); ?> <span class="required">*</span></label>
								<div class="ui left icon input">
									<i class="lock icon"></i>
									<input type="password" class="input-text" placeholder="<?php esc_attr_e('Password', 'chap'); ?>" name="password" id="reg_password" />
								</div>
							</div>

						<?php endif; ?>

						<?php do_action('woocommerce_register_form'); ?>
						<?php do_action('register_form'); ?>

						<?php wp_nonce_field('woocommerce-register', 'woocommerce-register-nonce'); ?>
						<button type="submit" class="ui huge primary woocommerce-Button button" name="register" value="<?php esc_attr_e('Register', 'chap'); ?>"><?php esc_html_e('Register', 'chap'); ?></button>

						</div>
					<?php do_action('woocommerce_register_form_end'); ?>

				</div>
			</div>
		</form>

	</div>

</div>
<?php endif; ?>

<?php do_action('woocommerce_after_customer_login_form'); ?>

<?php
/**
 * Lost password reset form.
 *
 * This is Chap theme's version of a WooCommerce template, overriding the original.
 * To override this in a Chap child theme, copy it to chap-child/woocommerce/myaccount/form-reset-password.php.
 *
 * @version 3.3.0
 */

if(!defined('ABSPATH')) {
	exit;
}

wc_print_notices(); ?>

<form method="post" class="ui large woocommerce-ResetPassword lost_reset_password form">
	<div class="ui middle aligned left aligned grid">
		<div class="column">
			<div class="ui stacked segment">

				<p><?php echo apply_filters('woocommerce_reset_password_message', esc_html__('Enter a new password below.', 'chap')); ?></p>

				<div class="labelless field">
					<label for="password_1"><?php esc_html_e('New password', 'chap'); ?> <span class="required">*</span></label>
					<div class="ui left icon input">
						<i class="lock icon"></i>
						<input type="password" placeholder="<?php esc_attr_e('New password', 'chap'); ?>" class="input-text" name="password_1" id="password_1" />
					</div>
				</div>

				<div class="labelless field">
					<label for="password_2"><?php esc_html_e('Re-enter new password', 'chap'); ?> <span class="required">*</span></label>
					<div class="ui left icon input">
						<i class="lock icon"></i>
						<input type="password" placeholder="<?php esc_attr_e('Re-enter new password', 'chap'); ?>" class="input-text" name="password_2" id="password_2" />
					</div>
				</div>

				<input type="hidden" name="reset_key" value="<?php echo esc_attr($args['key']); ?>" />
				<input type="hidden" name="reset_login" value="<?php echo esc_attr($args['login']); ?>" />

				<div class="clear"></div>

				<?php do_action('woocommerce_resetpassword_form'); ?>

				<input type="hidden" name="wc_reset_password" value="true" />
				<button type="submit" class="ui huge primary woocommerce-Button button" value="<?php esc_attr_e('Save', 'chap'); ?>"><?php esc_html_e('Save', 'chap'); ?></button>

				<?php wp_nonce_field('reset_password'); ?>

			</div>
		</div>
	</div>
</form>

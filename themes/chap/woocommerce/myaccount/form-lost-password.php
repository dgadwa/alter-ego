<?php
/**
 * Lost password form
 *
 * This is Chap theme's version of a WooCommerce template, overriding the original.
 * To override this in a Chap child theme, copy it to chap-child/woocommerce/myaccount/form-lost-password.php.
 *
 * @version 3.3.0
 */

if(!defined('ABSPATH')) {
	exit;
}

wc_print_notices(); ?>

<form method="post" class="ui large woocommerce-ResetPassword lost_reset_password form">
	<div class="ui middle aligned left aligned grid">
		<div class="column">
			<div class="ui stacked segment">

				<p><?php echo apply_filters('woocommerce_lost_password_message', esc_html__('Lost your password? Please enter your username or email address. You will receive a link to create a new password via email.', 'chap')); ?></p>

				<div class="labelless field">
					<label for="user_login"><?php esc_html_e('Username or email', 'chap'); ?></label>
					<div class="ui left icon input">
						<i class="user icon"></i>
						<input class="input-text" placeholder="<?php esc_attr_e('Username or email', 'chap'); ?>" type="text" name="user_login" id="user_login" />
					</div>
				</div>

				<?php do_action('woocommerce_lostpassword_form'); ?>

				<input type="hidden" name="wc_reset_password" value="true" />
				<button type="submit" class="ui huge primary woocommerce-Button button" value="<?php esc_attr_e('Reset password', 'chap'); ?>"><?php esc_html_e('Reset password', 'chap'); ?></button>

				<?php wp_nonce_field('lost_password'); ?>

			</div>
		</div>
	</div>
</form>

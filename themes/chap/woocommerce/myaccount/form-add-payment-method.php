<?php
/**
 * Add payment method form form
 *
 * This is Chap theme's version of a WooCommerce template, overriding the original.
 * To override this in a Chap child theme, copy it to chap-child/woocommerce/myaccount/form-add-payment-method.php.
 *
 * @version 3.3.0
 */

if(!defined('ABSPATH')) {
	exit;
}

if($available_gateways = WC()->payment_gateways->get_available_payment_gateways()): ?>
	<form id="add_payment_method" method="post" class="ui form">
		<div id="payment" class="ui stacked woocommerce-Payment segment">
			<ul class="woocommerce-PaymentMethods payment_methods methods">
				<?php
				// Chosen Method.
				if(count($available_gateways)) {
					current($available_gateways)->set_current();
				}

				foreach($available_gateways as $gateway) {
					?>
					<li class="field woocommerce-PaymentMethod woocommerce-PaymentMethod--<?php echo esc_attr($gateway->id); ?> payment_method_<?php echo esc_attr($gateway->id); ?>">
					<div class="ui radio checkbox">
						<input id="payment_method_<?php echo esc_attr($gateway->id); ?>" type="radio" class="input-radio" name="payment_method" value="<?php echo esc_attr($gateway->id); ?>" <?php checked($gateway->chosen, true); ?> />
						<label for="payment_method_<?php echo esc_attr($gateway->id); ?>"><?php echo esc_html($gateway->get_title()); ?></label>
					</div>
					<?php echo wp_kses_post($gateway->get_icon()); ?>
					<?php
					if($gateway->has_fields() || $gateway->get_description()) {
						echo '<div class="ui info message woocommerce-PaymentBox woocommerce-PaymentBox--' . esc_attr($gateway->id) . ' payment_box payment_method_' . esc_attr($gateway->id) . '" style="display:none">';
						$gateway->payment_fields();
						echo '</div>';
					}
					?>
					</li>
					<?php
				}
				?>
			</ul>

			<div class="form-row">
				<?php wp_nonce_field('woocommerce-add-payment-method'); ?>
				<button type="submit" class="ui large primary button" id="place_order" value="<?php esc_attr_e('Add payment method', 'chap'); ?>"><?php esc_html_e('Add payment method', 'chap'); ?></button>
				<input type="hidden" name="woocommerce_add_payment_method" id="woocommerce_add_payment_method" value="1" />
			</div>
		</div>
	</form>
<?php else: ?>
	<div class="ui message"><?php esc_html_e('Sorry, it seems that there are no payment methods which support adding a new payment method. Please contact us if you require assistance or wish to make alternate arrangements.', 'chap'); ?></div>
<?php endif; ?>

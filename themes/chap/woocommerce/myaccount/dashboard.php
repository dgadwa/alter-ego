<?php
/**
 * My Account Dashboard
 *
 * Shows the first intro screen on the account dashboard.
 *
 * This is Chap theme's version of a WooCommerce template, overriding the original.
 * To override this in a Chap child theme, copy it to chap-child/woocommerce/myaccount/my-account-dashboard.php.
 *
 * @version 2.6.0
 */
?>

<p>
	<?php
		echo sprintf(esc_attr__('Hello %s%s%s (not %2$s? %sSign out%s)', 'chap'), '<strong>', esc_html($current_user->display_name), '</strong>', '<a href="' . esc_url(wc_get_endpoint_url('customer-logout', '', wc_get_page_permalink('myaccount'))) . '">', '</a>');
	?>
</p>

<p>
	<?php
		echo sprintf(esc_attr__('From your account dashboard you can view your %1$srecent orders%2$s, manage your %3$sshipping and billing addresses%2$s and %4$sedit your password and account details%2$s.', 'chap'), '<a href="' . esc_url(wc_get_endpoint_url('orders')) . '">', '</a>', '<a href="' . esc_url(wc_get_endpoint_url('edit-address')) . '">', '<a href="' . esc_url(wc_get_endpoint_url('edit-account')) . '">');
	?>
</p>

<?php
	/**
	 * My Account dashboard.
	 *
	 * @since 2.6.0
	 */
	do_action('woocommerce_account_dashboard');

	/**
	 * Deprecated woocommerce_before_my_account action.
	 *
	 * @deprecated 2.6.0
	 */
	do_action('woocommerce_before_my_account');

	/**
	 * Deprecated woocommerce_after_my_account action.
	 *
	 * @deprecated 2.6.0
	 */
	do_action('woocommerce_after_my_account');
?>

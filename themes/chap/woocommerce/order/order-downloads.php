<?php
/**
 * Order Downloads.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/order/order-downloads.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.3.0
 */

if(!defined('ABSPATH')) {
	exit;
}
?>
<section class="woocommerce-order-downloads">
	<?php if(isset($show_title)): ?>
		<h2 class="ui woocommerce-order-downloads__title header"><?php esc_html_e('Downloads', 'chap'); ?></h2>
	<?php endif; ?>

	<table class="ui unstackable celled table woocommerce-MyAccount-downloads shop_table shop_table_responsive">
		<thead>
			<tr>
				<?php foreach(wc_get_account_downloads_columns() as $column_id => $column_name): ?>
					<th class="single line <?php echo esc_attr($column_id); ?>"><?php echo esc_html($column_name); ?></th>
				<?php endforeach; ?>
			</tr>
		</thead>
		<?php foreach($downloads as $download): ?>
			<tr>
				<?php foreach(wc_get_account_downloads_columns() as $column_id => $column_name): ?>
					<?php $cell_alignment = $column_id === 'download-remaining' ? 'center' : 'left'; ?>
					<td class="<?php echo esc_attr($cell_alignment); ?> aligned middle aligned <?php echo esc_attr($column_id); ?>" data-title="<?php echo esc_attr($column_name); ?>">
						<?php
						if(has_action('woocommerce_account_downloads_column_' . $column_id)) {
							do_action('woocommerce_account_downloads_column_' . $column_id, $download);
						} else {
							switch($column_id) {
								case 'download-product':
									?>
									<a href="<?php echo esc_url(get_permalink($download['product_id'])); ?>">
										<?php echo esc_html($download['product_name']); ?>
									</a>
									<?php
								break;
								case 'download-file':
									?>
									<a href="<?php echo esc_url($download['download_url']); ?>" class="ui primary right icon button">
										<?php echo esc_html($download['download_name']); ?>
										&nbsp;<i class="download icon"></i>
									</a>
									<?php
								break;
								case 'download-remaining':
									echo is_numeric($download['downloads_remaining']) ? esc_html($download['downloads_remaining']) : __('&infin;', 'chap');
									break;
								case 'download-expires':
									?>
									<?php if(!empty($download['access_expires'])): ?>
										<time datetime="<?php echo esc_attr(date('Y-m-d', strtotime($download['access_expires']))); ?>" title="<?php echo esc_attr(strtotime($download['access_expires'])); ?>"><?php echo esc_html(date_i18n(get_option('date_format'), strtotime($download['access_expires']))); ?></time>
									<?php else: ?>
										<?php esc_html_e('Never', 'chap'); ?>
									<?php endif; ?>
									<?php
								break;
								case 'download-actions': // Possibly deprecated since WC 3.2
									$actions = [
										'download'  => [
											'url'  => $download['download_url'],
											'name' => __('Download', 'chap'),
										],
									];
									if($actions = apply_filters('woocommerce_account_download_actions', $actions, $download)) {
										foreach($actions as $key => $action) {
											echo '<a href="' . esc_url($action['url']) . '" class="ui compact button woocommerce-Button ' . sanitize_html_class($key) . '">' . esc_html($action['name']) . '</a>';
										}
									}
									?>
									<?php
								break;
							}
						}
						?>
					</td>
				<?php endforeach; ?>
			</tr>
		<?php endforeach; ?>
	</table>

</section>

<?php
/**
 * Order Item Details
 *
 * This is Chap theme's version of a WooCommerce template, overriding the original.
 * To override this in a Chap child theme, copy it to chap-child/woocommerce/order/order-details-item.php.
 *
 * @version 3.0.0
 */

if(!defined('ABSPATH')) {
	exit;
}

if(version_compare(WC_VERSION, '3.0', '<')) {
	wc_get_template('backwards-compatibility/order/order-details-item-2.5.0.php', [
		'product' => $product,
		'item' => $item,
		'item_id' => $item_id,
		'order' => $order,
		'show_purchase_note' => $show_purchase_note,
		'purchase_note' => isset($purchase_note) ? $purchase_note : false,
	]);
	return;
}

if(!apply_filters('woocommerce_order_item_visible', true, $item)) {
	return;
}
?>
<tr class="<?php echo esc_attr(apply_filters('woocommerce_order_item_class', 'woocommerce-table__line-item order_item', $item, $order)); ?>">

	<td class="woocommerce-table__product-name product-name">
		<?php
			$is_visible        = $product && $product->is_visible();
			$product_permalink = apply_filters('woocommerce_order_item_permalink', $is_visible ? $product->get_permalink($item) : '', $item, $order);

			echo apply_filters('woocommerce_order_item_name', $product_permalink ? sprintf('<a href="%s">%s</a>', $product_permalink, $item->get_name()) : $item->get_name(), $item, $is_visible);
			echo apply_filters('woocommerce_order_item_quantity_html', ' <strong class="product-quantity">' . sprintf('&times; %s', $item->get_quantity()) . '</strong>', $item);

			do_action('woocommerce_order_item_meta_start', $item_id, $item, $order);

			wc_display_item_meta($item);
			wc_display_item_downloads($item, [
				'before' => '<div class="ui bulleted list"><div class="item">',
				'after' => '</div></div><div class="ui hidden divider"></div>',
				'separator' => '</div><div class="item">',
			]);

			do_action('woocommerce_order_item_meta_end', $item_id, $item, $order);
		?>
	</td>

	<td class="woocommerce-table__product-total product-total">
		<?php echo wp_kses_post($order->get_formatted_line_subtotal($item)); ?>
	</td>

</tr>

<?php if($show_purchase_note && $purchase_note): ?>
<tr class="woocommerce-table__product-purchase-note product-purchase-note">
	<td colspan="3"><?php echo wpautop(do_shortcode(wp_kses_post($purchase_note))); ?></td>
</tr>
<?php endif; ?>

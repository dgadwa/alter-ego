<?php
/**
 * Order again button
 *
 * This is Chap theme's version of a WooCommerce template, overriding the original.
 * To override this in a Chap child theme, copy it to chap-child/woocommerce/order/order-again.php.
 *
 * @version 2.3.0
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}
?>

<p class="order-again">
	<a href="<?php echo esc_url(wp_nonce_url(add_query_arg('order_again', $order->id) , 'woocommerce-order_again')); ?>" class="ui positive labeled icon button"><i class="refresh icon"></i><?php esc_html_e('Order Again', 'chap'); ?></a>
</p>

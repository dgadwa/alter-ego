<?php
/**
 * Order tracking form
 *
 * This is Chap theme's version of a WooCommerce template, overriding the original.
 * To override this in a Chap child theme, copy it to chap-child/woocommerce/order/form-tracking.php.
 *
 * @version 3.3.0
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

global $post;
$request_orderid = isset($_REQUEST['orderid']) ? wp_unslash($_REQUEST['orderid']) : '';
$request_order_email = isset($_REQUEST['order_email']) ? wp_unslash($_REQUEST['order_email']) : '';
?>

<form action="<?php echo esc_url(get_permalink($post->ID)); ?>" method="post" class="ui track_order form">

	<p><?php esc_html_e('To track your order please enter your Order ID in the box below and press the "Track" button. This was given to you on your receipt and in the confirmation email you should have received.', 'chap'); ?></p>

	<div class="field">
		<div class="two fields">
			<div class="field">
				<label for="orderid"><?php esc_html_e('Order ID', 'chap'); ?></label>
				<input class="input-text" type="text" name="orderid" id="orderid" value="<?php echo esc_attr($request_orderid); ?>" placeholder="<?php esc_attr_e('Found in your order confirmation email.', 'chap'); ?>" />
			</div>
			<div class="field">
				<label for="order_email"><?php esc_html_e('Billing Email', 'chap'); ?></label>
				<input class="input-text" type="text" name="order_email" id="order_email" value="<?php echo esc_attr($request_order_email); ?>" placeholder="<?php esc_attr_e('Email you used during checkout.', 'chap'); ?>" />
			</div>
		</div>
	</div>

	<button type="submit" class="ui button" name="track" value="<?php esc_attr_e('Track', 'chap'); ?>"><?php esc_html_e('Track', 'chap'); ?></button>
	<?php wp_nonce_field('woocommerce-order_tracking'); ?>

</form>

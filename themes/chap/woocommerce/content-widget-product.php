<?php
/**
 * The template for displaying product widget entries
 *
 * This is Chap theme's version of a WooCommerce template, overriding the original.
 * To override this in a Chap child theme, copy it to chap-child/woocommerce/content-widget-product.php.
 *
 * @version 3.3.0
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

if(version_compare(WC_VERSION, '3.0', '<')) {
	wc_get_template_part('backwards-compatibility/content-widget-product', '2.5.0');
	return;
}

global $product;
?>

<div class="item">
	<?php do_action('woocommerce_widget_product_item_start', $args); ?>
	<?php if(!empty($show_rating)): ?>
		<div class="right floated content">
			<?php echo wc_get_rating_html($product->get_average_rating()); ?>
		</div>
	<?php endif; ?>
	<?php echo wp_kses_post($product->get_image('shop_thumbnail', ['class' => 'ui avatar left floated marginless image'])); ?>
	<div class="left floated middle aligned content">
		<a class="header" href="<?php echo esc_url($product->get_permalink()); ?>">
			<span class="product-title"><?php echo esc_html($product->get_title()); ?></span>
		</a>
		<div class="left floated sub header">
			<?php echo wp_kses_post($product->get_price_html()); ?>
		</div>
	</div>
	<?php do_action('woocommerce_widget_product_item_end', $args); ?>
</div>

<?php
/**
 * Grouped product add to cart
 *
 * This is Chap theme's version of a WooCommerce template, overriding the original.
 * To override this in a Chap child theme, copy it to chap-child/woocommerce/single-product/add-to-cart/grouped.php.
 *
 * @version 3.0.7
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

if(version_compare(WC_VERSION, '3.0', '<')) {
	wc_get_template_part('backwards-compatibility/single-product/add-to-cart/grouped', '2.1.7');
	return;
}

global $product, $post;

do_action('woocommerce_before_add_to_cart_form'); ?>

<form class="ui cart form" method="post" enctype='multipart/form-data'>
	<table cellspacing="0" class="ui group_table table">
		<tbody>
			<?php
			$quantites_required = false;
			$previous_post      = $post;
			foreach($grouped_products as $grouped_product) {
				$post_object        = get_post($grouped_product->get_id());
				$quantites_required = $quantites_required || ($grouped_product->is_purchasable() && ! $grouped_product->has_options());

				setup_postdata($post =& $post_object);
				?>
				<tr id="product-<?php the_ID(); ?>" <?php post_class(); ?>>
				<td class="collapsing">
					<?php if(!$grouped_product->is_purchasable() || $grouped_product->has_options()): ?>
								<?php woocommerce_template_loop_add_to_cart(); ?>

							<?php elseif($grouped_product->is_sold_individually()): ?>
								<input type="checkbox" name="<?php echo esc_attr('quantity[' . $grouped_product->get_id() . ']'); ?>" value="1" class="wc-grouped-product-add-to-cart-checkbox" />

							<?php else: ?>
								<?php
									/**
									 * @since 3.0.0.
									 */
									do_action('woocommerce_before_add_to_cart_quantity');

									woocommerce_quantity_input([
										'input_name'  => 'quantity[' . $grouped_product->get_id() . ']',
										'input_value' => isset($_POST['quantity'][$grouped_product->get_id()]) ? wc_stock_amount($_POST['quantity'][$grouped_product->get_id()]) : 0,
										'min_value'   => apply_filters('woocommerce_quantity_input_min', 0, $grouped_product),
										'max_value'   => apply_filters('woocommerce_quantity_input_max', $grouped_product->get_max_purchase_quantity(), $grouped_product),
									]);

									/**
									 * @since 3.0.0.
									 */
									do_action('woocommerce_after_add_to_cart_quantity');
								?>
							<?php endif; ?>
						</td>
						<td class="label mca">
							<label for="product-<?php echo esc_attr($grouped_product->get_id()); ?>">
								<?php echo wp_kses_post($grouped_product->is_visible() ? '<a href="' . esc_url(apply_filters('woocommerce_grouped_product_list_link', get_permalink($grouped_product->get_id()), $grouped_product->get_id())) . '">' . $grouped_product->get_name() . '</a>' : $grouped_product->get_name()); ?>
							</label>
						</td>
						<?php do_action('woocommerce_grouped_product_list_before_price', $grouped_product); ?>
						<td class="price mca">
							<?php
							echo wp_kses_post($grouped_product->get_price_html());
							echo wc_get_stock_html($grouped_product);
							?>
						</td>
					</tr>
					<?php
			}
			$post = $previous_post;
			setup_postdata($post =& $previous_post);
			?>
		</tbody>
	</table>

	<input type="hidden" name="add-to-cart" value="<?php echo esc_attr($product->get_id()); ?>" />

	<?php if($quantites_required): ?>
		<?php do_action('woocommerce_before_add_to_cart_button'); ?>
		<button type="submit" class="ui positive labeled icon single_add_to_cart_button button alt"><i class="cart plus icon"></i><?php echo esc_html($product->single_add_to_cart_text()); ?></button>
		<?php do_action('woocommerce_after_add_to_cart_button'); ?>
	<?php endif; ?>

</form>

<?php do_action('woocommerce_after_add_to_cart_form'); ?>

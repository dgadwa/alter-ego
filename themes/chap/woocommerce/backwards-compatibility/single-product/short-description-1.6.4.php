<?php
/**
 * Single product short description *
 * This is Chap theme's version of a WooCommerce template, overriding the original.
 * To override this in a Chap child theme, copy it to chap-child/woocommerce/single-product/short-description.php.
 *
 * @version 1.6.4
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

global $post;

if(!$post->post_excerpt) {
	return;
}

?>
<div class="ui large piled short-description segment">
	<?php echo apply_filters('woocommerce_short_description', $post->post_excerpt); ?>
</div>

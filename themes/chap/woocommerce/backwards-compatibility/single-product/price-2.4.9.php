<?php
/**
 * Single Product Price, including microdata for SEO
 *
 * This is Chap theme's version of a WooCommerce template, overriding the original.
 * To override this in a Chap child theme, copy it to chap-child/woocommerce/single-product/price.php.
 *
 * @version 2.4.9
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

global $product;

?>
<div itemprop="offers" itemscope itemtype="http://schema.org/Offer">

	<p class="price"><?php echo wp_kses_post($product->get_price_html()); ?></p>

	<meta itemprop="price" content="<?php echo esc_attr( $product->get_price() ); ?>" />
	<meta itemprop="priceCurrency" content="<?php echo esc_attr( get_woocommerce_currency() ); ?>" />
	<link itemprop="availability" href="http://schema.org/<?php echo esc_url($product->is_in_stock() ? 'InStock' : 'OutOfStock'); ?>" />

</div>

<?php
/**
 * Single Product Image
 *
 * This is Chap theme's version of a WooCommerce template, overriding the original.
 * To override this in a Chap child theme, copy it to chap-child/woocommerce/single-product/product-image.php.
 *
 * @version 2.6.3
 */

if(!defined('ABSPATH')) {
	exit;
}

global $post, $product;
?>
<div id="product_images" class="images">
	<?php
		if ( has_post_thumbnail() ) {
			$attachment_count = count( $product->get_gallery_attachment_ids() );
			$gallery          = $attachment_count > 0 ? '[product-gallery]' : '';
			$props            = wc_get_product_attachment_props( get_post_thumbnail_id(), $post );
			$image            = get_the_post_thumbnail( $post->ID, apply_filters( 'single_product_large_thumbnail_size', 'shop_single' ), array(
				'title'	 => $props['title'],
				'alt'    => $props['alt'],
			) );
			echo apply_filters(
				'woocommerce_single_product_image_html',
				sprintf(
					'<a href="%s" itemprop="image" class="ui fluid woocommerce-main-image zoom image" title="%s">%s</a>',
					esc_url( $props['url'] ),
					esc_attr( $props['caption'] ),
					$image
				),
				$post->ID
			);
		} else {
			echo apply_filters( 'woocommerce_single_product_image_html', sprintf( '<img src="%s" alt="%s" />', wc_placeholder_img_src(), __( 'Placeholder', 'chap' ) ), $post->ID );
		}

		do_action( 'woocommerce_product_thumbnails' );
	?>
</div>

<?php
/**
 * Additional Information tab.
 *
 * This is Chap theme's version of a WooCommerce template, overriding the original.
 * To override this in a Chap child theme, copy it to chap-child/woocommerce/single-product/tabs/additional-information.php.
 *
 * @version 2.0.0
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

global $product;

$heading = apply_filters( 'woocommerce_product_additional_information_heading', __( 'Additional Information', 'chap' ) );

?>

<?php if ( $heading ): ?>
	<h2 class="ui header"><?php echo esc_html($heading); ?></h2>
<?php endif; ?>

<?php $product->list_attributes(); ?>

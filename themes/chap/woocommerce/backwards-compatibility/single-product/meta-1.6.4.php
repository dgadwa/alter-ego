<?php
/**
 * Single Product Meta
 *
 * This is Chap theme's version of a WooCommerce template, overriding the original.
 * To override this in a Chap child theme, copy it to chap-child/woocommerce/single-product/meta.php.
 *
 * @version 1.6.4
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

global $post, $product;

?>
<div class="ui compact product_meta items">

	<?php do_action('woocommerce_product_meta_start'); ?>

	<?php if(wc_product_sku_enabled() && ($product->get_sku() || $product->is_type('variable'))): ?>
	<div class="sku_wrapper item">
		<div class="content">
			<?php _e('SKU:', 'chap'); ?> <span class="sku" itemprop="sku"><?php echo ($sku = $product->get_sku()) ? $sku : __('N/A', 'chap'); ?></span>
		</div>
	</div>
	<?php endif; ?>

	<?php if(get_the_terms($post->ID, 'product_cat')): ?>
	<div class="posted_in item">
		<div class="extra ui tiny horizontal meta labels">
			<div class="ui transparent label"><i class="large folder icon"></i></div>
			<?php echo str_replace('<a', '<a class="ui label"', $product->get_categories('')); ?>
		</div>
	</div>
	<?php endif; ?>

	<?php if(get_the_terms($post->ID, 'product_tag')): ?>
	<div class="tagged_as item">
		<div class="marginless extra ui tiny horizontal meta labels">
			<div class="ui transparent label"><i class="large tag icon"></i></div>
			<?php echo str_replace('<a', '<a class="ui label"', $product->get_tags('')); ?>
		</div>
	</div>
	<?php endif; ?>

	<?php do_action('woocommerce_product_meta_end'); ?>

</div>

<?php
/**
 * Pay for order form
 *
 * This is Chap theme's version of a WooCommerce template, overriding the original.
 * To override this in a Chap child theme, copy it to chap-child/woocommerce/checkout/form-pay.php.
 *
 * @version 2.5.0
 */

if(!defined('ABSPATH')) {
	exit;
}
?>
<form id="order_review" method="post">

	<table class="shop_table">
		<thead>
			<tr>
				<th class="product-name"><?php esc_html_e('Product', 'chap'); ?></th>
				<th class="product-quantity"><?php esc_html_e('Qty', 'chap'); ?></th>
				<th class="product-total"><?php esc_html_e('Totals', 'chap'); ?></th>
			</tr>
		</thead>
		<tbody>
			<?php if(sizeof($order->get_items()) > 0): ?>
				<?php foreach($order->get_items() as $item): ?>
					<tr>
						<td class="product-name">
							<?php echo esc_html($item['name']); ?>
							<?php wc_display_item_meta($item); ?>
						</td>
						<td class="product-quantity"><?php echo esc_html($item['qty']); ?></td>
						<td class="product-subtotal"><?php echo wp_kses_post($order->get_formatted_line_subtotal($item)); ?></td>
					</tr>
				<?php endforeach; ?>
			<?php endif; ?>
		</tbody>
		<tfoot>
			<?php if($totals = $order->get_order_item_totals()): ?>
				<?php foreach($totals as $total): ?>
					<tr>
						<th scope="row" colspan="2"><?php echo esc_html($total['label']); ?></th>
						<td class="product-total"><?php echo wp_kses_post($total['value']); ?></td>
					</tr>
				<?php endforeach; ?>
			<?php endif; ?>
		</tfoot>
	</table>

	<div id="payment">
		<?php if($order->needs_payment()): ?>
			<ul class="wc_payment_methods payment_methods methods">
				<?php
				if(!empty($available_gateways)) {
					foreach($available_gateways as $gateway) {
						wc_get_template('checkout/payment-method.php', [
							'gateway' => $gateway,
						]);
					}
				} else {
					echo '<li>' . apply_filters('woocommerce_no_available_payment_methods_message', esc_html__('Sorry, it seems that there are no available payment methods for your location. Please contact us if you require assistance or wish to make alternate arrangements.', 'chap')) . '</li>';
				}
				?>
			</ul>
		<?php endif; ?>
		<div class="form-row">
			<input type="hidden" name="woocommerce_pay" value="1" />

			<?php wc_get_template('checkout/terms.php'); ?>

			<?php do_action('woocommerce_pay_order_before_submit'); ?>

			<?php echo apply_filters('woocommerce_pay_order_button_html', '<input type="submit" class="button alt" id="place_order" value="' . esc_attr($order_button_text) . '" data-value="' . esc_attr($order_button_text) . '" />'); ?>

			<?php do_action('woocommerce_pay_order_after_submit'); ?>

			<?php wp_nonce_field('woocommerce-pay'); ?>
		</div>
	</div>
</form>

<?php
/**
 * The template for displaying product widget entries
 *
 * This is Chap theme's version of a WooCommerce template, overriding the original.
 * To override this in a Chap child theme, copy it to chap-child/woocommerce/content-widget-product.php.
 *
 * @version 2.5.0
 */

// Exit if accessed directly
if(!defined('ABSPATH')) {
	exit;
}

global $product;
?>

<div class="item">
	<?php if(!empty($show_rating)): ?>
		<div class="right floated content">
			<?php echo wc_get_rating_html($product->get_average_rating()); ?>
		</div>
	<?php endif; ?>
	<?php echo wp_kses_post($product->get_image('shop_thumbnail', ['class' => 'ui avatar left floated marginless image'])); ?>
	<div class="left floated middle aligned content">
		<a class="header" href="<?php echo esc_url($product->get_permalink()); ?>">
			<span class="product-title"><?php echo esc_html($product->get_title()); ?></span>
		</a>
		<div class="left floated sub header">
			<?php echo wp_kses_post($product->get_price_html()); ?>
		</div>
	</div>
</div>

<?php
/**
 * Empty cart page
 *
 * This is Chap theme's version of a WooCommerce template, overriding the original.
 * To override this in a Chap child theme, copy it to chap-child/woocommerce/cart/cart-empty.php.
 *
 * @version 2.0.0
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

wc_print_notices();

?>

<p class="cart-empty">
	<?php esc_html_e('Your cart is currently empty.', 'chap') ?>
</p>

<?php do_action( 'woocommerce_cart_is_empty' ); ?>

<p class="return-to-shop">
	<a class="ui primary labeled icon button wc-backward" href="<?php echo esc_url(apply_filters('woocommerce_return_to_shop_redirect', wc_get_page_permalink('shop'))); ?>">
		<i class="left arrow icon"></i>
		<?php esc_html_e('Return to shop', 'chap') ?>
	</a>
</p>

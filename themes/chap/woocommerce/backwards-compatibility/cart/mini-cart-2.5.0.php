<?php
/**
* Mini-cart
*
* Contains the markup for the mini-cart, used by the cart widget.
*
* This template can be overridden by copying it to yourtheme/woocommerce/cart/mini-cart.php.
*
* HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
* will need to copy the new files to your theme to maintain compatibility. We try to do this.
* as little as possible, but it does happen. When this occurs the version of the template file will.
* be bumped and the readme will list any important changes.
*
* @see     http://docs.woothemes.com/document/template-structure/
* @author  WooThemes
* @package WooCommerce/Templates
* @version 2.5.0
*/

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}
?>

<?php do_action('woocommerce_before_mini_cart'); ?>

<div class="ui list cart_list product_list_widget <?php echo esc_attr($args['list_class']); ?>">
	<?php if(!WC()->cart->is_empty()): ?>
		<?php
		foreach(WC()->cart->get_cart() as $cart_item_key => $cart_item) {
			$_product = apply_filters('woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key);
			$product_id = apply_filters('woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key);
			if($_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters('woocommerce_widget_cart_item_visible', true, $cart_item, $cart_item_key)) {
				$product_name  = apply_filters('woocommerce_cart_item_name', $_product->get_title(), $cart_item, $cart_item_key);
				$thumbnail = apply_filters('woocommerce_cart_item_thumbnail', $_product->get_image(null, array('class' => 'ui left floated marginless avatar image')), $cart_item, $cart_item_key);
				$product_price = apply_filters('woocommerce_cart_item_price', WC()->cart->get_product_price($_product), $cart_item, $cart_item_key); ?>
				<div class="item <?php echo esc_attr(apply_filters('woocommerce_mini_cart_item_class', 'mini_cart_item', $cart_item, $cart_item_key)); ?>">
					<div class="right floated content">
						<?php echo apply_filters('woocommerce_cart_item_remove_link', sprintf(
							'<a href="%s" class="remove" title="%s" data-product_id="%s" data-product_sku="%s">&times;</a>',
							esc_url(WC()->cart->get_remove_url($cart_item_key)),
							__('Remove this item', 'chap'),
							esc_attr($product_id),
							esc_attr($_product->get_sku())
						), $cart_item_key); ?>
					</div>
					<?php echo str_replace(array('http:', 'https:'), '', $thumbnail); ?>
					<div class="left floated content">
						<?php if(!$_product->is_visible()): ?>
							<span class="header product-title"><?php echo esc_html($product_name); ?></span>
						<?php else: ?>
							<a class="header" href="<?php echo esc_url($_product->get_permalink($cart_item)); ?>">
								<span class="product-title"><?php echo esc_html($product_name); ?></span>
							</a>
						<?php endif; ?>
						<?php echo '<div class="sub header product_meta">' . WC()->cart->get_item_data($cart_item, true) . '</div>'; ?>
						<?php echo apply_filters('woocommerce_widget_cart_item_quantity', '<div class="ui sub header quantity">' . sprintf('%s &times; %s', $cart_item['quantity'], $product_price) . '</div>', $cart_item, $cart_item_key); ?>
					</div>
				</div><?php
			}
		}
		?>
	<?php else: ?>
		<div class="item empty"><?php _e('No products in the cart.', 'chap'); ?></div>
	<?php endif; ?>
</div>

<?php if(!WC()->cart->is_empty()): ?>
	<p class="total"><strong><?php _e('Subtotal', 'chap'); ?>:</strong> <?php echo WC()->cart->get_cart_subtotal(); ?></p>
	<?php do_action('woocommerce_widget_shopping_cart_before_buttons'); ?>
	<a href="<?php echo esc_url(wc_get_cart_url()); ?>" class="ui tiny primary left floated labeled icon button wc-forward"><i class="cart icon"></i><?php _e('Cart', 'chap'); ?></a>
	<a href="<?php echo esc_url(wc_get_checkout_url()); ?>" class="ui tiny positive right floated right labeled icon button checkout wc-forward"><i class="arrow right icon"></i><?php _e('Checkout', 'chap'); ?></a>
	<div class="ui hidden clearing divider"></div>
<?php endif; ?>

<?php do_action('woocommerce_after_mini_cart'); ?>

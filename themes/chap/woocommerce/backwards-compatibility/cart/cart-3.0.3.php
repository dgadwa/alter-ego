<?php
/**
 * Cart Page
 *
 * This is Chap theme's version of a WooCommerce template, overriding the original.
 * To override this in a Chap child theme, copy it to chap-child/woocommerce/cart/cart.php.
 *
 * @version 3.0.3
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

if(version_compare(WC_VERSION, '3.0', '<')) {
	wc_get_template_part('backwards-compatibility/cart/cart', '2.3.8');
	return;
}

wc_print_notices();

do_action( 'woocommerce_before_cart' ); ?>

<form class="woocommerce-cart-form" action="<?php echo esc_url( wc_get_cart_url() ); ?>" method="post">

	<?php do_action( 'woocommerce_before_cart_table' ); ?>

	<table class="ui celled large padded unstackable table shop_table shop_table_responsive cart woocommerce-cart-form__contents">
		<thead>
			<tr>
				<th class="product-thumbnail product-name"><?php esc_html_e( 'Product', 'chap' ); ?></th>
				<th class="collapsing product-price"><?php esc_html_e( 'Price', 'chap' ); ?></th>
				<th class="collapsing product-quantity"><?php esc_html_e( 'Quantity', 'chap' ); ?></th>
				<th class="collapsing product-subtotal"><?php esc_html_e( 'Total', 'chap' ); ?></th>
				<th class="collapsing product-remove">&nbsp;</th>
			</tr>
		</thead>
		<tbody>
			<?php do_action( 'woocommerce_before_cart_contents' ); ?>

			<?php
			foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
				$_product     = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
				$product_id   = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

				if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
					?>
					<tr class="woocommerce-cart-form__cart-item <?php echo esc_attr( apply_filters( 'woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key ) ); ?>">

						<td class="product-name" data-title="<?php esc_attr_e( 'Product', 'chap' ); ?>">
							<div class="ui medium image header">
								<?php
									$thumbnail = apply_filters('woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key);
									$thumbnail = str_replace('class="', 'class="ui image ', $thumbnail);
									if(!$_product->is_visible()) {
										echo wp_kses_post($thumbnail);
									} else {
										printf('<a class="image" href="%s">%s</a>', esc_url($_product->get_permalink($cart_item)), $thumbnail);
									}
								?>
								<div class="content">
								<?php
									if(!$_product->is_visible()) {
										echo apply_filters('woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key ) . '&nbsp;';
									} else {
										echo apply_filters('woocommerce_cart_item_name', sprintf('<a href="%s">%s</a>', esc_url( $_product->get_permalink($cart_item)), $_product->get_name()), $cart_item, $cart_item_key);
									}

									// Meta data
									echo '<div class="sub header product_meta">' . WC()->cart->get_item_data($cart_item, true) . '</div>';

									// Backorder notification
									if ( $_product->backorders_require_notification() && $_product->is_on_backorder( $cart_item['quantity'] ) ) {
										echo '<div class="sub header backorder_notification">' . esc_html__( 'Available on backorder', 'chap' ) . '</div>';
									}
								?>
								</div>
							</div>
						</td>

						<td class="product-price" data-title="<?php esc_attr_e( 'Price', 'chap' ); ?>">
							<?php
								echo apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key );
							?>
						</td>

						<td class="product-quantity" data-title="<?php esc_attr_e( 'Quantity', 'chap' ); ?>">
							<?php
								if ( $_product->is_sold_individually() ) {
									$product_quantity = sprintf( '1 <input type="hidden" name="cart[%s][qty]" value="1" />', $cart_item_key );
								} else {
									$product_quantity = woocommerce_quantity_input( array(
										'input_name'  => "cart[{$cart_item_key}][qty]",
										'input_value' => $cart_item['quantity'],
										'max_value'   => $_product->backorders_allowed() ? '' : $_product->get_stock_quantity(),
										'min_value'   => '0',
									), $_product, false );
								}

								echo apply_filters( 'woocommerce_cart_item_quantity', $product_quantity, $cart_item_key, $cart_item );
							?>
						</td>

						<td class="product-subtotal" data-title="<?php esc_attr_e( 'Total', 'chap' ); ?>">
							<?php
								echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key );
							?>
						</td>

				<td class="center aligned product-remove">
					<?php
						echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf(
							'<a href="%s" class="remove" aria-label="%s" data-product_id="%s" data-product_sku="%s">&times;</a>',
							esc_url( WC()->cart->get_remove_url( $cart_item_key ) ),
							esc_html__( 'Remove this item', 'chap' ),
							esc_attr( $product_id ),
							esc_attr( $_product->get_sku() )
						), $cart_item_key );
					?>
				</td>

					</tr>
					<?php
				}
			}

			do_action( 'woocommerce_cart_contents' );
			?>
			</tbody>
			<tfoot class="full-width">
			<tr>
				<th colspan="5" class="actions">

			<?php if ( wc_coupons_enabled() ) { ?>
			<div class="coupon-container">
				<div class="ui fluid right action left icon coupon input">
					<label for="coupon_code" style="display:none;"><?php esc_html_e( 'Coupon', 'chap' ); ?>:</label>
					<i class="ticket icon"></i><input type="text" name="coupon_code" class="input-text" id="coupon_code" placeholder="<?php esc_attr_e( 'Coupon code', 'chap' ); ?>" />
					<?php // Slight hack below because WooCommerce doesn't support <button> and Semantic UI doesn't support <input>. ?>
					<input style="display:none;" type="submit" class="button" name="apply_coupon" value="<?php esc_attr_e( 'Apply Coupon', 'chap' ); ?>" />
					<button class="ui tiny button" onclick="jQuery('input[name=apply_coupon]').trigger('click');return false;" type="submit"><?php esc_attr_e( 'Apply Coupon', 'chap' ); ?></button>
					<?php do_action( 'woocommerce_cart_coupon' ); ?>
				</div>
			</div>
			<?php } ?>

					<?php // Slight hack below because WooCommerce doesn't support <button> and Semantic UI doesn't support <input>. ?>
					<input style="display:none;" type="submit" class="button" name="update_cart" value="<?php esc_attr_e( 'Update Cart', 'chap' ); ?>" />
					<button class="ui large right floated primary labeled icon update_cart button" onclick="jQuery('input[name=update_cart]').removeAttr('disabled').trigger('click');return false;" type="submit"><i class="cart icon"></i><?php esc_attr_e( 'Update Cart', 'chap' ); ?></button>

					<?php do_action( 'woocommerce_cart_actions' ); ?>

					<?php wp_nonce_field( 'woocommerce-cart' ); ?>
				</th>
			</tr>

			<?php do_action( 'woocommerce_after_cart_contents' ); ?>
		</tfoot>
	</table>

	<?php do_action( 'woocommerce_after_cart_table' ); ?>

</form>

<div class="cart-collaterals">
	<?php do_action( 'woocommerce_cart_collaterals' ); ?>
</div>

<?php do_action( 'woocommerce_after_cart' ); ?>

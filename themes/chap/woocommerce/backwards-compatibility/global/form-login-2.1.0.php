<?php
/**
 * Login form
 *
 * This is Chap theme's version of a WooCommerce template, overriding the original.
 * To override this in a Chap child theme, copy it to chap-child/woocommerce/global/form-login.php.
 *
 * @version 2.1.0
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

if(is_user_logged_in()) {
	return;
}

$html_form_hidden = $hidden ? ' style="display:none"' : '';
$html_username_value = !empty($_POST['username']) ? ' value="' . esc_attr($_POST['username']) . '"' : '';
?>
<form method="post" class="ui large login form"<?php echo $html_form_hidden; ?>>
	<div class="ui middle aligned left aligned grid">
		<div class="column">
			<?php do_action('woocommerce_login_form_start'); ?>
				<div class="ui stacked segment">

					<?php
					if($message) {
						echo wpautop(wptexturize($message));
					}
					?>

					<div class="labelless field">
						<label for="username"><?php esc_html_e('Username or email address', 'chap'); ?> <span class="required">*</span></label>
						<div class="ui left icon input">
							<i class="user icon"></i>
							<input type="text" class="input-text" placeholder="<?php esc_attr_e('Username or email address', 'chap'); ?>" name="username" id="username"<?php echo $html_username_value; ?> />
						</div>
					</div>

					<div class="labelless field">
							<label for="password"><?php esc_html_e('Password', 'chap'); ?> <span class="required">*</span></label>
							<div class="ui left icon input">
								<i class="lock icon"></i>
								<input class="input-text" placeholder="<?php esc_attr_e('Password', 'chap'); ?>" type="password" name="password" id="password" />
							</div>
					</div>

					<?php do_action('woocommerce_login_form'); ?>
					<?php wp_nonce_field('woocommerce-login'); ?>

					<div class="field">
						<div class="ui checkbox">
							<input name="rememberme" tabindex="0" class="hidden" type="checkbox" id="rememberme" value="forever" />
							<label for="rememberme"><?php esc_html_e('Remember me', 'chap'); ?></label>
						</div>
					</div>

					<input type="submit" class="ui huge primary button" name="login" value="<?php esc_attr_e('Login', 'chap'); ?>" />
					<input type="hidden" name="redirect" value="<?php echo esc_url($redirect); ?>" />

					<div class="ui info message">
						<a href="<?php echo esc_url(wp_lostpassword_url()); ?>"><?php esc_html_e('Lost your password?', 'chap'); ?></a>
					</div>

				</div>
			<?php do_action('woocommerce_login_form_end'); ?>
		</div>
	</div>
</form>

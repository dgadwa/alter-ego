<?php
/**
 * Loop Rating
 *
 * This is Chap theme's version of a WooCommerce template, overriding the original.
 * To override this in a Chap child theme, copy it to chap-child/woocommerce/loop/rating.php.
 *
 * @version 2.0.0
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

global $product;

if(get_option('woocommerce_enable_review_rating') === 'no') {
	return;
}

if(!Chap\Options\get('woo_product_ratings_in_card')) {
	return;
}

if($rating_html = $product->get_rating_html()) {
	echo wp_kses_post($rating_html);
}

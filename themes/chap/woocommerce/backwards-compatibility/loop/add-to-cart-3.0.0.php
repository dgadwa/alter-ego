<?php
/**
 * Loop Add to Cart
 *
 * This is Chap theme's version of a WooCommerce template, overriding the original.
 * To override this in a Chap child theme, copy it to chap-child/woocommerce/loop/add-to-cart.php.
 *
 * @version 3.0.0
 */

if(!defined('ABSPATH')) {
	exit;
}

global $product;
$id = version_compare(WC_VERSION, '3.0', '<') ? $product->id : $product->get_id();

/**
 * Don't add the "button" class if the $class variable already contains it.
 */
$button_class = isset($class) ? (in_array('button', explode(' ', $class)) ? '' : ' button') : ' button';

if(Chap\Helpers\is_amp()) {
	/**
	 * Add to cart button on AMP pages.
	 */
	echo apply_filters(
		'woocommerce_loop_add_to_cart_link',
		sprintf(
			'<a class="ui %s bottom attached%s %s" href="%s"><i class="fa fa-eye"></i>%s</a>',
			esc_attr(Chap\Options\get('woo_card_button_size')),
			esc_attr($button_class),
			esc_attr(isset($class) ? $class : ''),
			esc_url(add_query_arg(AMP_QUERY_VAR, 1, get_permalink($id))),
			esc_html__('View product', 'chap')
		),
		$product
	);
} else {
	/**
	 * Add to cart button on standard pages.
	 */
	echo apply_filters(
		'woocommerce_loop_add_to_cart_link',
		sprintf(
			'<a class="ui %s bottom attached%s %s" rel="nofollow" href="%s" data-quantity="%s" data-product_id="%s" data-product_sku="%s"><i class="add icon"></i>%s</a>',
			esc_attr(Chap\Options\get('woo_card_button_size')),
			esc_attr($button_class),
			esc_attr(isset($class) ? $class : ''),
			esc_url($product->add_to_cart_url()),
			esc_attr(isset($quantity) ? $quantity : 1),
			esc_attr($id),
			esc_attr($product->get_sku()),
			esc_html($product->add_to_cart_text())
		),
		$product
	);
}

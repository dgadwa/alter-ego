<?php
/**
 * Result Count
 *
 * Shows text: Showing x - x of x results.
 *
 * This is Chap theme's version of a WooCommerce template, overriding the original.
 * To override this in a Chap child theme, copy it to chap-child/woocommerce/loop/result-count.php.
 *
 * @version 3.0.0
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

global $wp_query;

if(!woocommerce_products_will_display()) {
	return;
}
?>
<p class="woocommerce-result-count">
	<?php
	$paged    = max(1, $wp_query->get('paged'));
	$per_page = $wp_query->get('posts_per_page');
	$total    = $wp_query->found_posts;
	$first    = ($per_page * $paged) - $per_page + 1;
	$last     = min($total, $wp_query->get('posts_per_page') * $paged);

	if($total <= $per_page || $per_page === -1) {
		/* translators: %d: total results */
		printf(_n('Showing the single result', 'Showing all %d results', $total, 'chap'), $total);
	} else {
		/* translators: 1: first result 2: last result 3: total results */
		printf(_nx('Showing the single result', 'Showing %1$d&ndash;%2$d of %3$d results', $total, 'with first and last result', 'chap'), $first, $last, $total);
	}
	?>
</p>
<?php if($total == 1): ?>
<div class="ui hidden clearing divider"></div>
<?php endif; ?>

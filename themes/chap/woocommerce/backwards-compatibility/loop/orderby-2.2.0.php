<?php
/**
 * Show options for ordering
 *
 * This is Chap theme's version of a WooCommerce template, overriding the original.
 * To override this in a Chap child theme, copy it to chap-child/woocommerce/loop/orderby.php.
 *
 * @version 2.2.0
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

?>
<form class="ui woocommerce-ordering form" method="get">
	<select name="orderby" class="ui dropdown orderby">
		<?php foreach($catalog_orderby_options as $id => $name): ?>
			<option value="<?php echo esc_attr($id); ?>" <?php selected($orderby, $id); ?>><?php echo esc_html($name); ?></option>
		<?php endforeach; ?>
	</select>
	<?php
	// Keep query string vars intact
	foreach($_GET as $key => $val) {
		if('orderby' === $key || 'submit' === $key) {
			continue;
		}
		if(is_array($val)) {
			foreach($val as $innerVal) {
				echo '<input type="hidden" name="' . esc_attr($key) . '[]" value="' . esc_attr($innerVal) . '" />';
			}
		} else {
			echo '<input type="hidden" name="' . esc_attr($key) . '" value="' . esc_attr($val) . '" />';
		}
	}
	?>
</form>
<div class="ui hidden clearing divider"></div>

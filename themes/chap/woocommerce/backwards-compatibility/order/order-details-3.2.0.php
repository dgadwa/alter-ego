<?php
/**
 * Order details
 *
 * This is Chap theme's version of a WooCommerce template, overriding the original.
 * To override this in a Chap child theme, copy it to chap-child/woocommerce/order/order-details.php.
 *
 * @version 3.2.0
 */

if(!defined('ABSPATH')) {
	exit;
}

if(version_compare(WC_VERSION, '3.0', '<')) {
	wc_get_template('backwards-compatibility/order/order-details-2.6.0.php', [
		'order_id' => $order_id,
	]);
	return;
}

if(!$order = wc_get_order($order_id)) {
	return;
}
$order_items           = $order->get_items(apply_filters('woocommerce_purchase_order_item_types', 'line_item'));
$show_purchase_note    = $order->has_status(apply_filters('woocommerce_purchase_note_order_statuses', ['completed', 'processing']));
$show_customer_details = is_user_logged_in() && $order->get_user_id() === get_current_user_id();
$downloads             = $order->get_downloadable_items();
$show_downloads        = $order->has_downloadable_item() && $order->is_download_permitted();

if($show_downloads) {
	wc_get_template('order/order-downloads.php', [
		'downloads' => $downloads,
		'show_title' => true,
	]);
}
?>
<section class="ui basic paddingless woocommerce-order-details segment">

	<h2 class="ui woocommerce-order-details__title header"><?php esc_html_e('Order details', 'chap'); ?></h2>

	<table class="ui unstackable celled table woocommerce-table woocommerce-table--order-details shop_table order_details">

		<thead>
			<tr>
				<th class="woocommerce-table__product-name product-name"><?php esc_html_e('Product', 'chap'); ?></th>
				<th class="woocommerce-table__product-table product-total"><?php esc_html_e('Total', 'chap'); ?></th>
			</tr>
		</thead>

		<tbody>
			<?php foreach($order_items as $item_id => $item): ?>
				<?php
				$product = apply_filters('woocommerce_order_item_product', $item->get_product(), $item);
				wc_get_template('order/order-details-item.php', [
					'order'              => $order,
					'item_id'            => $item_id,
					'item'               => $item,
					'show_purchase_note' => $show_purchase_note,
					'purchase_note'      => $product ? $product->get_purchase_note() : '',
					'product'            => $product,
				]);
				?>
			<?php endforeach; ?>
			<?php do_action('woocommerce_order_items_table', $order); ?>
		</tbody>

		<tfoot>
			<?php foreach($order->get_order_item_totals() as $key => $total): ?>
			<tr>
				<th scope="row"><?php echo esc_html($total['label']); ?></th>
				<td><?php echo wp_kses_post($total['value']); ?></td>
			</tr>
			<?php endforeach; ?>
			<?php if($order->get_customer_note()): ?>
				<tr>
					<th><?php esc_html_e('Note:', 'chap'); ?></th>
					<td><?php echo wptexturize($order->get_customer_note()); ?></td>
				</tr>
			<?php endif; ?>
		</tfoot>

	</table>

	<?php do_action('woocommerce_order_details_after_order_table', $order); ?>

</section>

<div class="ui hidden divider"></div>

<?php
if($show_customer_details) {
	wc_get_template('order/order-details-customer.php', [
		'order' => $order,
	]);
}
?>

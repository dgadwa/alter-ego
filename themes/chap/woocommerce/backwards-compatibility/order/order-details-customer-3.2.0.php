<?php
/**
 * Order Customer Details
 *
 * This is Chap theme's version of a WooCommerce template, overriding the original.
 * To override this in a Chap child theme, copy it to chap-child/woocommerce/order/order-details-customer.php.
 *
 * @version 3.2.0
 */

if(!defined('ABSPATH')) {
	exit;
}

if(version_compare(WC_VERSION, '3.0', '<')) {
	wc_get_template('backwards-compatibility/order/order-details-customer-2.4.0.php', [
		'order' => $order,
	]);
	return;
}
?>
<section class="chap-woocommerce-customer-details">

	<?php if(!wc_ship_to_billing_address_only() && $order->needs_shipping_address()): ?>

	<section class="woocommerce-columns woocommerce-columns--2 woocommerce-columns--addresses col2-set addresses">

		<div class="woocommerce-column woocommerce-column--1 woocommerce-column--billing-address col-1">

			<?php endif; ?>

			<h2 class="ui woocommerce-column__title header"><?php esc_html_e('Billing address', 'chap'); ?></h2>

			<address>
				<?php echo ($address = $order->get_formatted_billing_address()) ? $address : esc_html__('N/A', 'chap'); ?>
				<div class="ui list">
				<?php if($order->get_billing_phone()): ?>
					<div class="item woocommerce-customer-details--phone">
						<i class="phone icon"></i>
						<?php echo esc_html($order->get_billing_phone()); ?>
					</div>
				<?php endif; ?>
				<?php if($order->get_billing_email()): ?>
					<div class="item woocommerce-customer-details--email">
						<i class="mail icon"></i>
						<?php echo esc_html($order->get_billing_email()); ?>
					</div>
				<?php endif; ?>
				</div>
			</address>

			<?php if(!wc_ship_to_billing_address_only() && $order->needs_shipping_address()): ?>

		</div>

		<div class="woocommerce-column woocommerce-column--2 woocommerce-column--shipping-address col-2">

			<h2 class="ui woocommerce-column__title header"><?php esc_html_e('Shipping address', 'chap'); ?></h2>

			<address>
				<?php echo ($address = $order->get_formatted_shipping_address()) ? $address : esc_html__('N/A', 'chap'); ?>
			</address>

		</div>

	</section>

	<?php endif; ?>

</section>

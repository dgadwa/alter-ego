<?php
/**
 * The template for displaying product search form
 *
 * This is Chap theme's version of a WooCommerce template, overriding the original.
 * To override this in a Chap child theme, copy it to chap-child/woocommerce/product-searchform.php.
 *
 * @version 3.3.0
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

?>

<form role="search" method="get" class="ui fluid left icon action input woocommerce-product-search" action="<?php echo esc_url(home_url('/')); ?>">
	<input type="hidden" name="post_type" value="product" />
	<i class="search icon"></i>
	<input type="search" id="woocommerce-product-search-field" class="search-field" placeholder="<?php echo esc_attr_x('Search Products&hellip;', 'placeholder', 'chap'); ?>" value="<?php echo get_search_query(); ?>" name="s" title="<?php echo esc_attr_x('Search for:', 'label', 'chap'); ?>" />
	<button type="submit" class="ui button search-submit" value="<?php echo esc_attr_x('Search', 'submit button', 'chap'); ?>"><?php echo esc_html_x('Search', 'submit button', 'chap'); ?></button>
</form>

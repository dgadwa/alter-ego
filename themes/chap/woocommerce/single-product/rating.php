<?php
/**
 * Single Product Rating
 *
 * This is Chap theme's version of a WooCommerce template, overriding the original.
 * To override this in a Chap child theme, copy it to chap-child/woocommerce/single-product/rating.php.
 *
 * @version 3.1.0
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

if(version_compare(WC_VERSION, '3.1', '<')) {
	wc_get_template_part('backwards-compatibility/single-product/rating', '2.3.2');
	return;
}

global $product;

if(get_option('woocommerce_enable_review_rating') === 'no') {
	return;
}

$rating_count = $product->get_rating_count();
$review_count = $product->get_review_count();
$average      = $product->get_average_rating();

if($rating_count > 0): ?>

	<div class="woocommerce-product-rating">
		<?php echo wc_get_rating_html($average, $rating_count); ?>
		<?php if(comments_open()): ?>
		<a href="#reviews" class="woocommerce-review-link" rel="nofollow">(<?php printf(_n('%s customer review', '%s customer reviews', $review_count, 'chap'), '<span class="count">' . esc_html($review_count) . '</span>'); ?>)</a>
		<?php endif; ?>
	</div>

<?php endif; ?>

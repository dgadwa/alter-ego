<?php
/**
 * Single Product Sale Flash
 *
 * This is Chap theme's version of a WooCommerce template, overriding the original.
 * To override this in a Chap child theme, copy it to chap-child/woocommerce/single-product/sale-flash.php.
 *
 * @version 1.6.4
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

global $post, $product;

?>
<?php if($product->is_on_sale()): ?>

	<?php echo apply_filters('woocommerce_sale_flash', '<div class="ui ' . Chap\Options\get('woo_on_sale_color') . ' ribbon label">' . esc_html__('Sale!', 'chap') . '</div>', $post, $product); ?>

<?php endif;

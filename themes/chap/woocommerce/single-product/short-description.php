<?php
/**
 * Single product short description *
 * This is Chap theme's version of a WooCommerce template, overriding the original.
 * To override this in a Chap child theme, copy it to chap-child/woocommerce/single-product/short-description.php.
 *
 * @version 3.3.0
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

if(version_compare(WC_VERSION, '3.3', '<')) {
	wc_get_template_part('backwards-compatibility/single-product/short-description', '1.6.4');
	return;
}

global $post;

$short_description = apply_filters('woocommerce_short_description', $post->post_excerpt);

if(!$short_description) {
	return;
}

?>
<div class="ui large piled short-description segment">
	<?php echo wp_kses_post($short_description); ?>
</div>

<?php
/**
 * Single Product Thumbnails
 *
 * This is Chap theme's version of a WooCommerce template, overriding the original.
 * To override this in a Chap child theme, copy it to chap-child/woocommerce/single-product/product-thumbnails.php.
 *
 * @version 3.1.0
 */

if(!defined('ABSPATH')) {
	exit;
}

if(version_compare(WC_VERSION, '3.0', '<')) {
	wc_get_template_part('backwards-compatibility/single-product/product-thumbnails', '2.6.3');
	return;
}

global $post, $product;

$attachment_ids = $product->get_gallery_image_ids();

if($attachment_ids && has_post_thumbnail()) {
	foreach($attachment_ids as $attachment_id) {
		$full_size_image = wp_get_attachment_image_src($attachment_id, 'full');
		$thumbnail       = wp_get_attachment_image_src($attachment_id, 'shop_thumbnail');

		$attributes = [
			'title'                   => get_post_field('post_title', $attachment_id),
			'data-caption'            => get_post_field('post_excerpt', $attachment_id),
			'data-src'                => $full_size_image[0],
			'data-large_image'        => $full_size_image[0],
			'data-large_image_width'  => $full_size_image[1],
			'data-large_image_height' => $full_size_image[2],
		];

		$html  = '<div data-thumb="' . esc_url($thumbnail[0]) . '" class="woocommerce-product-gallery__image"><a href="' . esc_url($full_size_image[0]) . '">';
		$html .= wp_get_attachment_image($attachment_id, 'thumbnail', false, $attributes);
		$html .= '</a></div>';

		echo apply_filters('woocommerce_single_product_image_thumbnail_html', $html, $attachment_id);
	}
}

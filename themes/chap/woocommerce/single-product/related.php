<?php
/**
 * Related products
 *
 * This is Chap theme's version of a WooCommerce template, overriding the original.
 * To override this in a Chap child theme, copy it to chap-child/woocommerce/single-product/related.php.
 *
 * @version 3.0.0
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

if(version_compare(WC_VERSION, '3.0', '<')) {
	wc_get_template('backwards-compatibility/single-product/related-1.6.4.php', [
		'orderby' => $orderby,
		'columns' => $columns,
		'posts_per_page' => $posts_per_page,
	]);
	return;
}

if($related_products): ?>

	<section class="related products">

		<h2 class="ui header"><?php esc_html_e('Related products', 'chap'); ?></h2>

		<?php

		woocommerce_product_loop_start();

		foreach($related_products as $related_product):

			 $post_object = get_post($related_product->get_id());

			setup_postdata($GLOBALS['post'] =& $post_object);

			wc_get_template_part('content', 'product');

		endforeach;

		woocommerce_product_loop_end();

		?>

	</section>

<?php endif;

wp_reset_postdata();

<?php
/**
 * Single Product Share.
 *
 * Sharing plugins can hook into here or you can add your own code directly.
 *
 * This is Chap theme's version of a WooCommerce template, overriding the original.
 * To override this in a Chap child theme, copy it to chap-child/woocommerce/single-product/share.php.
 *
 * @version 1.6.4
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

?>

<?php
do_action('woocommerce_share'); // Sharing plugins can hook into here

<?php
/**
 * Single Product Price
 *
 * This is Chap theme's version of a WooCommerce template, overriding the original.
 * To override this in a Chap child theme, copy it to chap-child/woocommerce/single-product/price.php.
 *
 * @version 3.0.0
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

if(version_compare(WC_VERSION, '3.0', '<')) {
	wc_get_template_part('backwards-compatibility/single-product/price', '2.4.9');
	return;
}

global $product;

?>
<p class="price"><?php echo wp_kses_post($product->get_price_html()); ?></p>

<?php
/**
 * Description tab.
 *
 * This is Chap theme's version of a WooCommerce template, overriding the original.
 * To override this in a Chap child theme, copy it to chap-child/woocommerce/single-product/tabs/description.php.
 *
 * @version 2.0.0
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

global $post;

$heading = esc_html(apply_filters('woocommerce_product_description_heading', esc_html__('Product description', 'chap')));

?>

<?php if($heading): ?>
  <h2 class="ui header"><?php echo esc_html($heading); ?></h2>
<?php endif; ?>

<?php the_content(); ?>

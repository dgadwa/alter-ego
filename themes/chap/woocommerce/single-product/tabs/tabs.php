<?php
/**
 * Single Product tabs
 *
 * This is Chap theme's version of a WooCommerce template, overriding the original.
 * To override this in a Chap child theme, copy it to chap-child/woocommerce/single-product/tabs/tabs.php.
 *
 * @version 2.4.0
 */

if(!defined('ABSPATH')) {
	exit;
}

/**
 * Filter tabs and allow third parties to add their own.
 *
 * Each tab is an array containing title, callback and priority.
 * @see woocommerce_default_product_tabs()
 */
$tabs = apply_filters('woocommerce_product_tabs', []);

if(!empty($tabs)): ?>

	<div class="woocommerce-tabs wc-tabs-wrapper">
		<ul class="ui top attached tabular wc-tabs stackable menu">
			<?php foreach($tabs as $key => $tab): ?>
				<li class="item <?php echo esc_attr($key); ?>_tab">
					<a class="ui small header" href="#tab-<?php echo esc_attr($key); ?>"><?php echo apply_filters('woocommerce_product_' . $key . '_tab_title', esc_html($tab['title']), $key); ?></a>
				</li>
			<?php endforeach; ?>
		</ul>
		<?php foreach($tabs as $key => $tab): ?>
			<div class="ui bottom attached tab entry-content padded large wc-tab segment" id="tab-<?php echo esc_attr($key); ?>">
				<?php call_user_func($tab['callback'], $key, $tab); ?>
			</div>
		<?php endforeach; ?>
	</div>

<?php endif; ?>

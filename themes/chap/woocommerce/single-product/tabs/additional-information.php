<?php
/**
 * Additional Information tab
 *
 * This is Chap theme's version of a WooCommerce template, overriding the original.
 * To override this in a Chap child theme, copy it to chap-child/woocommerce/single-product/tabs/additional-information.php.
 *
 * @version 3.0.0
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

if(version_compare(WC_VERSION, '3.0', '<')) {
	wc_get_template_part('backwards-compatibility/single-product/tabs/additional-information', '2.0.0');
	return;
}

global $product;

$heading = esc_html(apply_filters('woocommerce_product_additional_information_heading', esc_html__('Additional information', 'chap')));

?>

<?php if($heading): ?>
	<h2 class="ui header"><?php echo $heading; ?></h2>
<?php endif; ?>

<?php do_action('woocommerce_product_additional_information', $product); ?>

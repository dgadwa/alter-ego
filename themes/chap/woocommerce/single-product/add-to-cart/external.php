<?php
/**
 * External product add to cart
 *
 * This is Chap theme's version of a WooCommerce template, overriding the original.
 * To override this in a Chap child theme, copy it to chap-child/woocommerce/single-product/add-to-cart/external.php.
 *
 * @version 2.1.0
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

?>

<?php do_action('woocommerce_before_add_to_cart_button'); ?>

<p class="cart">
	<a href="<?php echo esc_url($product_url); ?>" rel="nofollow" class="ui secondary right labeled icon single_add_to_cart_button button"><i class="external link icon"></i><?php echo esc_html($button_text); ?></a>
</p>

<?php do_action('woocommerce_after_add_to_cart_button'); ?>

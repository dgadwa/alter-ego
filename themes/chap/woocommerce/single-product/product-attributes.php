<?php
/**
 * Product attributes
 *
 * Used by list_attributes() in the products class.
 *
 * This is Chap theme's version of a WooCommerce template, overriding the original.
 * To override this in a Chap child theme, copy it to chap-child/woocommerce/single-product/product-attributes.php.
 *
 * @version 3.1.0
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

if(version_compare(WC_VERSION, '3.0', '<')) {
	wc_get_template('backwards-compatibility/single-product/product-attributes-2.1.3.php', [
		'product' => $product,
	]);
	return;
}

?>
<table class="ui very basic unstackable definitions table" data-class="shop_attributes">
	<?php if($display_dimensions && $product->has_weight()): ?>
		<tr>
			<th class="right aligned collapsing"><?php esc_html_e('Weight', 'chap'); ?></th>
			<td class="product_weight"><?php echo esc_html(wc_format_weight($product->get_weight())); ?></td>
		</tr>
	<?php endif; ?>

	<?php if($display_dimensions && $product->has_dimensions()): ?>
		<tr>
			<th class="right aligned collapsing"><?php esc_html_e('Dimensions', 'chap'); ?></th>
			<td class="product_dimensions"><?php echo esc_html(wc_format_dimensions($product->get_dimensions(false))); ?></td>
		</tr>
	<?php endif; ?>

	<?php foreach($attributes as $attribute): ?>
		<tr>
			<th class="right aligned collapsing"><?php echo wc_attribute_label($attribute->get_name()); ?></th>
			<td>
			<?php
			$values = [];

			if($attribute->is_taxonomy()) {
				$attribute_taxonomy = $attribute->get_taxonomy_object();
				$attribute_values = wc_get_product_terms($product->get_id(), $attribute->get_name(), [
					'fields' => 'all',
				]);

				foreach($attribute_values as $attribute_value) {
					$value_name = esc_html($attribute_value->name);

					if($attribute_taxonomy->attribute_public) {
						$values[] = '<a href="' . esc_url(get_term_link($attribute_value->term_id, $attribute->get_name())) . '" rel="tag">' . $value_name . '</a>';
					} else {
						$values[] = $value_name;
					}
				}
			} else {
				$values = $attribute->get_options();

				foreach($values as &$value) {
					if(function_exists('make_clickable')) {
						$value = make_clickable(esc_html($value));
					} else {
						$value = esc_html($value);
					}
				}
			}

			echo apply_filters('woocommerce_attribute', wpautop(wptexturize(implode(', ', $values))), $attribute, $values);
			?>
			</td>
		</tr>
	<?php endforeach; ?>
</table>

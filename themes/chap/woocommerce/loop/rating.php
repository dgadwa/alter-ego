<?php
/**
 * Loop Rating
 *
 * This is Chap theme's version of a WooCommerce template, overriding the original.
 * To override this in a Chap child theme, copy it to chap-child/woocommerce/loop/rating.php.
 *
 * @version 3.0.0
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

if(version_compare(WC_VERSION, '3.0', '<')) {
	wc_get_template_part('backwards-compatibility/loop/rating', '2.0.0');
	return;
}

global $product;

if(get_option('woocommerce_enable_review_rating') === 'no') {
	return;
}

if(!Chap\Options\get('woo_product_ratings_in_card')) {
	return;
}

echo wc_get_rating_html($product->get_average_rating());

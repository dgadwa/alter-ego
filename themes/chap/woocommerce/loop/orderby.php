<?php
/**
 * Show options for ordering
 *
 * This is Chap theme's version of a WooCommerce template, overriding the original.
 * To override this in a Chap child theme, copy it to chap-child/woocommerce/loop/orderby.php.
 *
 * @version 3.3.0
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

if(version_compare(WC_VERSION, '3.3', '<')) {
	wc_get_template('backwards-compatibility/loop/orderby-2.2.0.php', [
		'orderby' => $orderby,
		'catalog_orderby_options' => $catalog_orderby_options,
	]);
	return;
}

?>
<form class="ui woocommerce-ordering form" method="get">
	<select name="orderby" class="ui dropdown orderby">
		<?php foreach($catalog_orderby_options as $id => $name): ?>
			<option value="<?php echo esc_attr($id); ?>" <?php selected($orderby, $id); ?>><?php echo esc_html($name); ?></option>
		<?php endforeach; ?>
	</select>
	<input type="hidden" name="paged" value="1" />
	<?php wc_query_string_form_fields(null, ['orderby', 'submit', 'paged']); ?>
</form>
<div class="ui hidden clearing divider"></div>

<?php
/**
 * Loop Add to Cart
 *
 * This is Chap theme's version of a WooCommerce template, overriding the original.
 * To override this in a Chap child theme, copy it to chap-child/woocommerce/loop/add-to-cart.php.
 *
 * @version 3.3.0
 */

if(!defined('ABSPATH')) {
	exit;
}

if(version_compare(WC_VERSION, '3.3', '<')) {
	wc_get_template('backwards-compatibility/loop/add-to-cart-3.0.0.php', [
		'class' => $args['class'],
		'quantity' => $args['quantity'],
	]);
	return;
}

global $product;

/**
 * Don't add the "button" class if the $class variable already contains it.
 */
$button_class = isset($args['class']) ? (in_array('button', explode(' ', $args['class'])) ? '' : ' button') : ' button';

if(Chap\Helpers\is_amp()) {
	/**
	 * Add to cart button on AMP pages.
	 */
	echo apply_filters(
		'chap_amp_woocommerce_loop_add_to_cart_link',
		sprintf(
			'<a class="ui %s bottom attached%s %s" href="%s"><i class="fa fa-eye"></i>%s</a>',
			esc_attr(Chap\Options\get('woo_card_button_size')),
			esc_attr($button_class),
			esc_attr(isset($class) ? $class : ''),
			esc_url(add_query_arg(AMP_QUERY_VAR, 1, get_permalink($product->get_id()))),
			esc_html__('View product', 'chap')
		),
		$product,
		$args
	);
} else {
	/**
	 * Add to cart button on standard pages.
	 */
	echo apply_filters(
		'woocommerce_loop_add_to_cart_link',
		sprintf(
			'<a class="ui %s bottom attached%s %s" href="%s" data-quantity="%s"%s><i class="add icon"></i>%s</a>',
			esc_attr(Chap\Options\get('woo_card_button_size')),
			esc_attr($button_class),
			esc_attr(isset($class) ? $class : ''),
			esc_url($product->add_to_cart_url()),
			esc_attr(isset($quantity) ? $quantity : 1),
			isset($args['attributes']) ? ' ' . wc_implode_html_attributes($args['attributes']) : '',
			esc_html($product->add_to_cart_text())
		),
		$product,
		$args
	);
}

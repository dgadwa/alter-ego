<?php
/**
 * Displayed when no products are found matching the current query
 *
 * This is Chap theme's version of a WooCommerce template, overriding the original.
 * To override this in a Chap child theme, copy it to chap-child/woocommerce/loop/no-products-found.php.
 *
 * @version 2.0.0
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

?>
<div class="ui info message"><?php esc_html_e('No products were found matching your selection.', 'chap'); ?></div>
<?php get_product_search_form(); ?>

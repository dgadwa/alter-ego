<?php
/**
 * Product Loop End
 *
 * This is Chap theme's version of a WooCommerce template, overriding the original.
 * To override this in a Chap child theme, copy it to chap-child/woocommerce/loop/loop-end.php.
 *
 * @version 2.0.0
 */

// Don't render loop container in slider context.
if(isset($GLOBALS['csc']['slider']) && $GLOBALS['csc']['slider']) {
	return;
}
?>
</div>

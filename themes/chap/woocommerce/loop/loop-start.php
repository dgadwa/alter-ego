<?php
/**
 * Product Loop Start
 *
 * This is Chap theme's version of a WooCommerce template, overriding the original.
 * To override this in a Chap child theme, copy it to chap-child/woocommerce/loop/loop-start.php.
 *
 * @version 3.3.0
 */

// Don't render loop container in slider context.
if(isset($GLOBALS['csc']['slider']) && $GLOBALS['csc']['slider']) {
	return;
}

global $woocommerce_loop;

if(isset($woocommerce_loop) && isset($woocommerce_loop['columns']) && (int)$woocommerce_loop['columns'] > 0) {
	$chap_columns = (int)$woocommerce_loop['columns'];
} elseif(isset($GLOBALS['chap_wc_loop']['columns']) && (int)$GLOBALS['chap_wc_loop']['columns'] > 0) {
	$chap_columns = $GLOBALS['chap_wc_loop']['columns'];
} else {
	$chap_columns = Chap\Options\get('woo_product_columns');
}
$chap_columns = Chap\Helpers\number_to_word(intval($chap_columns));

?>
<div class="ui <?php echo esc_attr($chap_columns); ?> tablet doubling mobile stackable products cards">

<?php
/**
 * Loop Price
 *
 * This is Chap theme's version of a WooCommerce template, overriding the original.
 * To override this in a Chap child theme, copy it to chap-child/woocommerce/loop/price.php.
 *
 * @version 1.6.4
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

global $product;
?>

<?php if($price_html = $product->get_price_html()): ?>
	<div class="ui <?php echo esc_attr(Chap\Options\get('woo_card_price_size')); ?> header">
		<?php echo wp_kses_post($price_html); ?>
	</div>
<?php endif; ?>

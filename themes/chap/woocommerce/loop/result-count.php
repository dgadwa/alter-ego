<?php
/**
 * Result Count
 *
 * Shows text: Showing x - x of x results.
 *
 * This is Chap theme's version of a WooCommerce template, overriding the original.
 * To override this in a Chap child theme, copy it to chap-child/woocommerce/loop/result-count.php.
 *
 * @version 3.3.0
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

if(version_compare(WC_VERSION, '3.3', '<')) {
	wc_get_template_part('backwards-compatibility/loop/result-count', '3.0.0');
	return;
}
?>
<p class="woocommerce-result-count">
	<?php
	if($total <= $per_page || $per_page === -1) {
		/* translators: %d: total results */
		printf(_n('Showing the single result', 'Showing all %d results', $total, 'chap'), $total);
	} else {
		$first = ($per_page * $current) - $per_page + 1;
		$last  = min($total, $per_page * $current);
		/* translators: 1: first result 2: last result 3: total results */
		printf(_nx('Showing the single result', 'Showing %1$d&ndash;%2$d of %3$d results', $total, 'with first and last result', 'chap'), $first, $last, $total);
	}
	?>
</p>
<?php if($total == 1): ?>
<div class="ui hidden clearing divider"></div>
<?php endif; ?>

<?php
/**
 * Pagination - Show numbered pagination for catalog pages
 *
 * This is Chap theme's version of a WooCommerce template, overriding the original.
 * To override this in a Chap child theme, copy it to chap-child/woocommerce/loop/pagination.php.
 *
 * @version 3.3.1
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

if(version_compare(WC_VERSION, '3.3', '<')) {
	wc_get_template_part('backwards-compatibility/loop/pagination', '2.2.2');
	return;
}

$total   = isset($total) ? $total : wc_get_loop_prop('total_pages');
$current = isset($current) ? $current : wc_get_loop_prop('current_page');
$base    = isset($base) ? $base : esc_url_raw(str_replace(999999999, '%#%', remove_query_arg('add-to-cart', get_pagenum_link(999999999, false))));
$format  = isset($format) ? $format : '';

if($total <= 1) {
	return;
}
?>
<div class="ui hidden clearing divider"></div>
<div class="ui centered grid">
	<div class="<?php echo Chap\Options\get('pagination_alignment'); ?> aligned column">
		<div class="ui pagination menu">
			<?php
				$menu = paginate_links(apply_filters('woocommerce_pagination_args', [
					'base'         => $base,
					'format'       => $format,
					'add_args'     => false,
					'current'      => max(1, $current),
					'total'        => $total,
					'prev_text'    => '%prev_page%',
					'next_text'    => '%next_page%',
					'type'         => 'plain',
					'end_size'     => 3,
					'mid_size'     => 3,
				]));
				echo wp_kses_post(str_replace(
					[
						"span class='",
						"span aria-current='page' class='",
						'class="',
						"class='",
						'%prev_page%',
						'%next_page%',
						'item prev',
						'item next',
						'page-numbers dots',
					],
					[
						"span class='active ",
						"span aria-current='page' class='active ",
						'class="item ',
						"class='item ",
						'<i class="left chevron icon"></i>',
						'<i class="right chevron icon"></i>',
						'prev icon item',
						'next icon item',
						'page-numbers disabled dots',
					],
					$menu
				));
			?>
		</div>
	</div>
</div>

<?php
/**
 * Cart errors page
 *
 * This is Chap theme's version of a WooCommerce template, overriding the original.
 * To override this in a Chap child theme, copy it to chap-child/woocommerce/checkout/cart-errors.php.
 *
 * @version 2.4.0
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

?>

<?php wc_print_notices(); ?>

<p><?php esc_html_e('There are some issues with the items in your cart (shown above). Please go back to the cart page and resolve these issues before checking out.', 'chap'); ?></p>

<?php do_action('woocommerce_cart_has_errors'); ?>

<p><a class="button wc-backward" href="<?php echo esc_url(wc_get_page_permalink('cart')); ?>"><?php esc_html_e('Return to cart', 'chap'); ?></a></p>

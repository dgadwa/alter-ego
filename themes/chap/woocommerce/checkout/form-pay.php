<?php
/**
 * Pay for order form
 *
 * This is Chap theme's version of a WooCommerce template, overriding the original.
 * To override this in a Chap child theme, copy it to chap-child/woocommerce/checkout/form-pay.php.
 *
 * @version 3.3.0
 */

if(!defined('ABSPATH')) {
	exit;
}

if(version_compare(WC_VERSION, '3.3', '<')) {
	wc_get_template('backwards-compatibility/checkout/form-pay-2.5.0.php', [
		'order' => $order,
		'available_gateways' => $available_gateways,
		'order_button_text' => $order_button_text,
	]);
	return;
}

?>
<form id="order_review" method="post" class="ui form">

	<table class="ui large celled padded shop_table table">
		<thead>
			<tr>
				<th class="product-name"><?php esc_html_e('Product', 'chap'); ?></th>
				<th class="product-quantity"><?php esc_html_e('Qty', 'chap'); ?></th>
				<th class="product-total"><?php esc_html_e('Totals', 'chap'); ?></th>
			</tr>
		</thead>
		<tbody>
			<?php if(count($order->get_items()) > 0): ?>
				<?php foreach($order->get_items() as $item_id => $item): ?>
					<?php
						if(!apply_filters('woocommerce_order_item_visible', true, $item)) {
							continue;
						}
					?>
					<tr class="<?php echo esc_attr(apply_filters('woocommerce_order_item_class', 'order_item', $item, $order)); ?>">
						<td class="product-name">
							<?php
								echo apply_filters('woocommerce_order_item_name', esc_html($item->get_name()), $item, false);
								do_action('woocommerce_order_item_meta_start', $item_id, $item, $order, false);
								wc_display_item_meta($item);
								do_action('woocommerce_order_item_meta_end', $item_id, $item, $order, false);
							?>
						</td>
						<td class="product-quantity"><?php echo apply_filters('woocommerce_order_item_quantity_html', ' <strong class="product-quantity">' . sprintf('&times; %s', esc_html($item->get_quantity())) . '</strong>', $item); ?></td>
						<td class="product-subtotal"><?php echo wp_kses_post($order->get_formatted_line_subtotal($item)); ?></td>
					</tr>
				<?php endforeach; ?>
			<?php endif; ?>
		</tbody>
		<tfoot>
			<?php if($totals = $order->get_order_item_totals()): ?>
				<?php foreach($totals as $total): ?>
					<tr>
						<th scope="row" colspan="2"><?php echo wp_kses_post($total['label']); ?></th>
						<td class="product-total"><?php echo wp_kses_post($total['value']); ?></td>
					</tr>
				<?php endforeach; ?>
			<?php endif; ?>
		</tfoot>
	</table>

	<div id="payment" class="ui stacked segment">
		<?php if($order->needs_payment()): ?>
			<ul class="wc_payment_methods payment_methods methods">
				<?php
				if(!empty($available_gateways)) {
					foreach($available_gateways as $gateway) {
						wc_get_template('checkout/payment-method.php', [
							'gateway' => $gateway,
						]);
					}
				} else {
					echo '<li><div class="ui message">' . apply_filters('woocommerce_no_available_payment_methods_message', esc_html__('Sorry, it seems that there are no available payment methods for your location. Please contact us if you require assistance or wish to make alternate arrangements.', 'chap')) . '</div></li>';
				}
				?>
			</ul>
		<?php endif; ?>
		<div class="form-row">
			<input type="hidden" name="woocommerce_pay" value="1" />

			<?php wc_get_template('checkout/terms.php'); ?>

			<?php do_action('woocommerce_pay_order_before_submit'); ?>

			<?php echo apply_filters('woocommerce_pay_order_button_html', '<button type="submit" class="ui massive positive button" id="place_order" value="' . esc_attr($order_button_text) . '" data-value="' . esc_attr($order_button_text) . '">' . esc_html($order_button_text) . '</button>'); ?>

			<?php do_action('woocommerce_pay_order_after_submit'); ?>

			<?php wp_nonce_field('woocommerce-pay'); ?>
		</div>
	</div>
</form>

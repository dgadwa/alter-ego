<?php
/**
 * Checkout shipping information form
 *
 * This is Chap theme's version of a WooCommerce template, overriding the original.
 * To override this in a Chap child theme, copy it to chap-child/woocommerce/checkout/form-shipping.php.
 *
 * @version 3.0.9
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

?>
<div class="woocommerce-shipping-fields">
	<?php if(true === WC()->cart->needs_shipping_address()): ?>

		<?php
		if(empty($_POST)) {
			$ship_to_different_address = get_option('woocommerce_ship_to_destination') === 'shipping' ? 1 : 0;
			$ship_to_different_address = apply_filters('woocommerce_ship_to_different_address_checked', $ship_to_different_address);
		} else {
			$ship_to_different_address = $checkout->get_value('ship_to_different_address');
		}
		?>

		<div class="field" id="ship-to-different-address">
			<div class="ui toggle checkbox">
				<input id="ship-to-different-address-checkbox" tabindex="0" class="hidden woocommerce-form__input woocommerce-form__input-checkbox input-checkbox" <?php checked($ship_to_different_address, 1); ?> type="checkbox" name="ship_to_different_address" value="1" />
				<label for="ship-to-different-address-checkbox" class="woocommerce-form__label woocommerce-form__label-for-checkbox checkbox">
					<div class="ui header"><?php esc_html_e('Ship to a different address?', 'chap'); ?></div>
				</label>
			</div>
		</div>

		<div class="shipping_address">
			<?php do_action('woocommerce_before_checkout_shipping_form', $checkout); ?>
			<div class="woocommerce-shipping-fields__field-wrapper">
			<?php
			$fields = $checkout->get_checkout_fields('shipping');
			foreach($checkout->checkout_fields['shipping'] as $key => $field) {
				array_push($field['class'], 'field');
				if(isset($field['country_field'], $fields[$field['country_field']])) {
					$field['country'] = $checkout->get_value($field['country_field']);
				}
				ob_start();
				woocommerce_form_field($key, $field, $checkout->get_value($key));
				echo str_replace(['<p', '</p>'], ['<div', '</div>'], ob_get_clean());
			}
			?>
			</div>
			<?php do_action('woocommerce_after_checkout_shipping_form', $checkout); ?>
		</div>

	<?php endif; ?>
</div>

<div class="woocommerce-additional-fields">
	<?php do_action('woocommerce_before_order_notes', $checkout); ?>

	<?php if(apply_filters('woocommerce_enable_order_notes_field', get_option('woocommerce_enable_order_comments', 'yes') === 'yes')): ?>

		<?php if(!WC()->cart->needs_shipping() || wc_ship_to_billing_address_only()): ?>
		<h3 class="ui header"><?php esc_html_e('Additional information', 'chap'); ?></h3>
		<?php endif; ?>

		<div class="woocommerce-additional-fields__field-wrapper">
		<?php
		foreach($checkout->checkout_fields['order'] as $key => $field):
			array_push($field['class'], 'field');
			ob_start();
			woocommerce_form_field($key, $field, $checkout->get_value($key));
			echo str_replace(['<p', '</p>'], ['<div', '</div>'], ob_get_clean());
		endforeach;
		?>
		</div>

	<?php endif; ?>

	<?php do_action('woocommerce_after_order_notes', $checkout); ?>
</div>

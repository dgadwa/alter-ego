<?php
/**
 * Checkout Payment Section
 *
 * This is Chap theme's version of a WooCommerce template, overriding the original.
 * To override this in a Chap child theme, copy it to chap-child/woocommerce/checkout/payment.php.
 *
 * @version 3.3.0
 */
if(!defined('ABSPATH')) {
	exit;
}

if(version_compare(WC_VERSION, '3.3', '<')) {
	wc_get_template('backwards-compatibility/checkout/payment-2.5.0.php', [
		'available_gateways' => $available_gateways,
		'order_button_text' => $order_button_text,
	]);
	return;
}

if(!is_ajax()) {
	do_action('woocommerce_review_order_before_payment');
}
?>
<div id="payment" class="ui stacked woocommerce-checkout-payment segment">
	<?php if(WC()->cart->needs_payment()): ?>
		<ul class="wc_payment_methods payment_methods methods">
			<?php
			if(!empty($available_gateways)) {
				foreach($available_gateways as $gateway) {
					wc_get_template('checkout/payment-method.php', [
						'gateway' => $gateway,
					]);
				}
			} else {
				echo '<li><div class="ui message">' . apply_filters('woocommerce_no_available_payment_methods_message', WC()->customer->get_country() ? esc_html__('Sorry, it seems that there are no available payment methods for your state. Please contact us if you require assistance or wish to make alternate arrangements.', 'chap') : esc_html__('Please fill in your details above to see available payment methods.', 'chap')) . '</div></li>';
			}
			?>
		</ul>
	<?php endif; ?>
	<div class="form-row place-order">
		<noscript>
			<div class="ui visible warning message">
				<p>
					<?php
					printf(
						__('Since your browser does not support JavaScript, or it is disabled, please ensure you click the %1$sUpdate totals%2$s button before placing your order. You may be charged more than the amount stated above if you fail to do so.', 'chap'),
						'<em>',
						'</em>'
					);
					?>
				</p>
				<button type="submit" class="ui primary button" name="woocommerce_checkout_update_totals" value="<?php esc_attr_e('Update totals', 'chap'); ?>"><?php esc_html_e('Update totals', 'chap'); ?></button>
			</div>
		</noscript>

		<?php wc_get_template('checkout/terms.php'); ?>

		<?php do_action('woocommerce_review_order_before_submit'); ?>

		<?php echo apply_filters('woocommerce_order_button_html', '<button type="submit" class="ui massive positive button" name="woocommerce_checkout_place_order" id="place_order" value="' . esc_attr($order_button_text) . '" data-value="' . esc_attr($order_button_text) . '">' . esc_html($order_button_text) . '</button>'); ?>

		<?php do_action('woocommerce_review_order_after_submit'); ?>

		<?php wp_nonce_field('woocommerce-process_checkout'); ?>
	</div>
</div>
<?php
if(!is_ajax()) {
	do_action('woocommerce_review_order_after_payment');
}

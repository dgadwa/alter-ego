<?php
/**
 * Checkout billing information form
 *
 * This is Chap theme's version of a WooCommerce template, overriding the original.
 * To override this in a Chap child theme, copy it to chap-child/woocommerce/checkout/form-billing.php.
 *
 * @version 3.0.9
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

if(version_compare(WC_VERSION, '3.0', '<')) {
	wc_get_template('backwards-compatibility/checkout/form-billing-2.1.2.php', [
		'checkout' => $checkout,
	]);
	return;
}

/** @global WC_Checkout $checkout */

?>
<div class="woocommerce-billing-fields">
	<?php if(wc_ship_to_billing_address_only() && WC()->cart->needs_shipping()): ?>
		<h3 class="ui header"><?php esc_html_e('Billing &amp; Shipping', 'chap'); ?></h3>
	<?php else: ?>
		<h3 class="ui header"><?php esc_html_e('Billing details', 'chap'); ?></h3>
	<?php endif; ?>

	<?php do_action('woocommerce_before_checkout_billing_form', $checkout); ?>

	<div class="woocommerce-billing-fields__field-wrapper">
		<?php
		$fields = $checkout->get_checkout_fields('billing');
		foreach($fields as $key => $field) {
			if(isset($field['country_field'], $fields[$field['country_field']])) {
				$field['country'] = $checkout->get_value($field['country_field']);
			}
			array_push($field['class'], 'field');
			ob_start();
			woocommerce_form_field($key, $field, $checkout->get_value($key));
			echo str_replace(['<p', '</p>'], ['<div', '</div>'], ob_get_clean());
		}
		?>
	</div>

	<?php do_action('woocommerce_after_checkout_billing_form', $checkout); ?>
</div>

<?php if(!is_user_logged_in() && $checkout->is_registration_enabled()): ?>
	<div class="woocommerce-account-fields">
		<?php if(!$checkout->is_registration_required()): ?>
			<div class="field form-row form-row-wide create-account-checkbox">
				<div class="ui woocommerce-form__label woocommerce-form__label-for-checkbox checkbox">
					<input class="woocommerce-form__input woocommerce-form__input-checkbox input-checkbox" id="createaccount" <?php checked(($checkout->get_value('createaccount') === true || (apply_filters('woocommerce_create_account_default_checked', false) === true)), true); ?> type="checkbox" name="createaccount" value="1" /> <label for="createaccount" class="checkbox"><?php esc_html_e('Create an account?', 'chap'); ?></label>
				</div>
			</div>
		<?php endif; ?>

		<?php do_action('woocommerce_before_checkout_registration_form', $checkout); ?>

		<?php if($checkout->get_checkout_fields('account')): ?>
			<div class="create-account">
				<?php foreach($checkout->get_checkout_fields('account') as $key => $field): ?>
					<?php woocommerce_form_field($key, $field, $checkout->get_value($key)); ?>
				<?php endforeach; ?>
				<div class="clear"></div>
			</div>
		<?php endif; ?>

		<?php do_action('woocommerce_after_checkout_registration_form', $checkout); ?>
	</div>
<?php endif; ?>

<?php
/**
 * Thankyou page
 *
 * This is Chap theme's version of a WooCommerce template, overriding the original.
 * To override this in a Chap child theme, copy it to chap-child/woocommerce/checkout/thankyou.php.
 *
 * @version 3.2.0
 */

if(!defined('ABSPATH')) {
	exit;
}

if(version_compare(WC_VERSION, '3.0', '<')) {
	wc_get_template('backwards-compatibility/checkout/thankyou-2.2.0.php', [
		'order' => $order,
	]);
	return;
}
?>

<div class="woocommerce-order">

	<?php if($order): ?>

		<?php if($order->has_status('failed')): ?>

			<p class="woocommerce-notice woocommerce-notice--error woocommerce-thankyou-order-failed"><?php esc_html_e('Unfortunately your order cannot be processed as the originating bank/merchant has declined your transaction. Please attempt your purchase again.', 'chap'); ?></p>

			<p class="woocommerce-notice woocommerce-notice--error woocommerce-thankyou-order-failed-actions">
				<a href="<?php echo esc_url($order->get_checkout_payment_url()); ?>" class="ui primary pay button"><?php esc_html_e('Pay', 'chap'); ?></a>
				<?php if(is_user_logged_in()): ?>
					<a href="<?php echo esc_url(wc_get_page_permalink('myaccount')); ?>" class="ui secondary pay button"><?php esc_html_e('My account', 'chap'); ?></a>
				<?php endif; ?>
			</p>

			<div class="ui hidden divider"></div>

		<?php else: ?>

			<p class="woocommerce-notice woocommerce-notice--success woocommerce-thankyou-order-received"><?php echo apply_filters('woocommerce_thankyou_order_received_text', esc_html__('Thank you. Your order has been received.', 'chap'), $order); ?></p>

			<div class="ui large compact piled padded woocommerce-order-overview woocommerce-thankyou-order-details order_details segment">
				<div class="ui relaxed list">
					<div class="item woocommerce-order-overview__order order">
						<div class="ui sub header"><?php esc_html_e('Order number', 'chap'); ?></div>
						<?php echo esc_html($order->get_order_number()); ?>
					</div>
					<div class="item woocommerce-order-overview__date date">
						<div class="ui sub header"><?php esc_html_e('Date', 'chap'); ?></div>
						<?php echo wc_format_datetime($order->get_date_created()); ?>
					</div>
					<?php if(is_user_logged_in() && $order->get_user_id() === get_current_user_id() && $order->get_billing_email()): ?>
						<div class="item woocommerce-order-overview__email email">
							<div class="ui sub header"><?php esc_html_e('E-mail', 'chap'); ?></div>
							<?php echo $order->get_billing_email(); ?>
						</div>
					<?php endif; ?>
					<div class="item woocommerce-order-overview__total total">
						<div class="ui sub header"><?php esc_html_e('Total', 'chap'); ?></div>
						<?php echo wp_kses_post($order->get_formatted_order_total()); ?>
					</div>
					<?php if($order->get_payment_method_title()): ?>
						<div class="item woocommerce-order-overview__payment-method method">
							<div class="ui sub header"><?php esc_html_e('Payment method', 'chap'); ?></div>
							<?php echo wp_kses_post($order->get_payment_method_title()); ?>
						</div>
					<?php endif; ?>
				</div>
			</div>

		<?php endif; ?>

		<?php do_action('woocommerce_thankyou_' . $order->get_payment_method(), $order->get_id()); ?>
		<?php do_action('woocommerce_thankyou', $order->get_id()); ?>

	<?php else: ?>

		<p class="woocommerce-notice woocommerce-notice--success woocommerce-thankyou-order-received"><?php echo apply_filters('woocommerce_thankyou_order_received_text', esc_html__('Thank you. Your order has been received.', 'chap'), null); ?></p>

	<?php endif; ?>

</div>

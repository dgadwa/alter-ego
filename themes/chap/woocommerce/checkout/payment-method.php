<?php
/**
 * Output a single payment method
 *
 * This is Chap theme's version of a WooCommerce template, overriding the original.
 * To override this in a Chap child theme, copy it to chap-child/woocommerce/checkout/payment-method.php.
 *
 * @version 2.3.0
 */

if(!defined('ABSPATH')) {
	exit;
}
?>
<li class="field wc_payment_method payment_method_<?php echo esc_attr($gateway->id); ?>">
	<div class="ui radio checkbox">
		<input id="payment_method_<?php echo esc_attr($gateway->id); ?>" type="radio" class="input-radio" name="payment_method" value="<?php echo esc_attr($gateway->id); ?>" <?php checked($gateway->chosen, true); ?> data-order_button_text="<?php echo esc_attr($gateway->order_button_text); ?>" />
		<label for="payment_method_<?php echo esc_attr($gateway->id); ?>"><?php echo wp_kses_post($gateway->get_title()); ?></label>
	</div>
	<?php echo wp_kses_post($gateway->get_icon()); ?>
	<?php if($gateway->has_fields() || $gateway->get_description()): ?>
		<?php $html_chosen_style = !$gateway->chosen ? ' style="display:none"' : ''; ?>
		<div class="ui info message payment_box payment_method_<?php echo esc_attr($gateway->id); ?>"<?php echo $html_chosen_style; ?>>
			<?php $gateway->payment_fields(); ?>
		</div>
	<?php endif; ?>
</li>

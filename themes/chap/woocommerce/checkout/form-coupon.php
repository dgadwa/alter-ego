<?php
/**
 * Checkout coupon form
 *
 * This is Chap theme's version of a WooCommerce template, overriding the original.
 * To override this in a Chap child theme, copy it to chap-child/woocommerce/checkout/form-coupon.php.
 *
 * @version 3.3.0
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

if(!wc_coupons_enabled()) {
	return;
}

if(!WC()->cart->applied_coupons) {
	$info_message = apply_filters(
		'woocommerce_checkout_coupon_message',
		esc_html__('Have a coupon?', 'chap') . ' <a href="#" class="showcoupon">' . esc_html__('Click here to enter your code', 'chap') . '</a>'
	);
	wc_print_notice($info_message, 'notice');
}
?>

<form class="ui checkout_coupon form" method="post" style="display:none">
	<div class="ui stacked column segment">
		<div class="ui fluid right action coupon input">
			<input type="text" name="coupon_code" class="input-text" placeholder="<?php esc_attr_e('Coupon code', 'chap'); ?>" id="coupon_code" value="" />
			<button type="submit" class="button" name="apply_coupon"><?php esc_attr_e('Apply Coupon', 'chap'); ?></button>
		</div>
	</div>
</form>

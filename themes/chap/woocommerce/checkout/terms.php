<?php
/**
 * Checkout terms and conditions checkbox
 *
 * @version 3.1.1
 */
if(!defined('ABSPATH')) {
	exit;
}

if(version_compare(WC_VERSION, '3.1', '<')) {
	wc_get_template_part('backwards-compatibility/checkout/terms', '2.5.0');
	return;
}

$terms_page_id = wc_get_page_id('terms');
if($terms_page_id > 0 && apply_filters('woocommerce_checkout_show_terms', true)):
	$terms         = get_post($terms_page_id);
	$terms_content = has_shortcode($terms->post_content, 'woocommerce_checkout') ? '' : wc_format_content($terms->post_content);
	if($terms_content) {
		do_action('woocommerce_checkout_before_terms_and_conditions');
		echo '<div class="woocommerce-terms-and-conditions" style="display:none;max-height:200px;overflow:auto">' . $terms_content . '</div>';
	}
	?>
	<p class="ui checkbox form-row terms wc-terms-and-conditions">
		<input type="checkbox" class="woocommerce-form__input woocommerce-form__input-checkbox input-checkbox" name="terms" <?php checked(apply_filters('woocommerce_terms_is_checked_default', isset($_POST['terms'])), true); ?> id="terms" />
		<label for="terms" class="woocommerce-form__label woocommerce-form__label-for-checkbox checkbox">
			<span>
				<?php
				printf(
					__('I&rsquo;ve read and accept the <a href="%s" class="woocommerce-terms-and-conditions-link">terms &amp; conditions</a>', 'chap'),
					esc_url(wc_get_page_permalink('terms'))
				);
				?>
			</span>
			<span class="required">*</span>
		</label>
		<input type="hidden" name="terms-field" value="1" />
	</p>
	<?php do_action('woocommerce_checkout_after_terms_and_conditions'); ?>
<?php endif; ?>

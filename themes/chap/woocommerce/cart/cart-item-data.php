<?php
/**
 * Cart item data (when outputting non-flat)
 *
 * This is Chap theme's version of a WooCommerce template, overriding the original.
 * To override this in a Chap child theme, copy it to chap-child/woocommerce/cart/cart-item-data.php.
 *
 * @version 2.4.0
 */
if(!defined('ABSPATH')) {
	exit;
}
?>
<dl class="variation">
	<?php foreach($item_data as $data): ?>
		<dt class="variation-<?php echo sanitize_html_class($data['key']); ?>"><?php echo wp_kses_post($data['key']); ?>:</dt>
		<dd class="variation-<?php echo sanitize_html_class($data['key']); ?>"><?php echo wp_kses_post(wpautop($data['display'])); ?></dd>
	<?php endforeach; ?>
</dl>

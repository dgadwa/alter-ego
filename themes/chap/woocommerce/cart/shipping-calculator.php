<?php
/**
 * Shipping Calculator
 *
 * This is Chap theme's version of a WooCommerce template, overriding the original.
 * To override this in a Chap child theme, copy it to chap-child/woocommerce/cart/shipping-calculator.php.
 *
 * @version 3.2.0
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

if(get_option('woocommerce_enable_shipping_calc') === 'no' || !WC()->cart->needs_shipping()) {
	return;
}

wp_enqueue_script('wc-country-select');

?>

<?php do_action('woocommerce_before_shipping_calculator'); ?>

<form class="ui woocommerce-shipping-calculator form" action="<?php echo esc_url(wc_get_cart_url()); ?>" method="post">

	<div class="field">
		<a href="#" class="ui mini shipping-calculator-button button">
			<?php esc_html_e('Calculate shipping', 'chap'); ?>
			<i class="right truck icon"></i>
		</a>
	</div>

	<section class="shipping-calculator-form" style="display:none">

		<div class="form-row field" id="calc_shipping_country_field">
			<select name="calc_shipping_country" id="calc_shipping_country" class="country_to_state country_select search" rel="calc_shipping_state">
				<option value=""><?php esc_html_e('Select a country&hellip;', 'chap'); ?></option>
				<?php
				foreach(WC()->countries->get_shipping_countries() as $key => $value) {
					echo '<option value="' . esc_attr($key) . '"' . selected(WC()->customer->get_shipping_country(), esc_attr($key), false) . '>' . esc_html($value) . '</option>';
				}
				?>
			</select>
		</div>

		<div class="field" id="calc_shipping_state_field">
			<?php
			$current_cc = WC()->customer->get_shipping_country();
			$current_r  = WC()->customer->get_shipping_state();
			$states     = WC()->countries->get_states($current_cc);

			if(is_array($states) && empty($states)) {

				?>
				<input type="hidden" name="calc_shipping_state" id="calc_shipping_state" />
				<?php

			} elseif(is_array($states)) {

				?>
				<span>
					<select name="calc_shipping_state" id="calc_shipping_state" class="state_select" placeholder="<?php esc_attr_e('State / county', 'chap'); ?>">
						<option value=""><?php esc_html_e('Select a state&hellip;', 'chap'); ?></option>
						<?php
						foreach($states as $ckey => $cvalue) {
							echo '<option value="' . esc_attr($ckey) . '" ' . selected($current_r, $ckey, false) . '>' . esc_html($cvalue) . '</option>';
						}
						?>
					</select>
				</span>
				<?php

			} else {

				?>
				<input type="text" class="input-text" value="<?php echo esc_attr($current_r); ?>" placeholder="<?php esc_attr_e('State / county', 'chap'); ?>" name="calc_shipping_state" id="calc_shipping_state" />
				<?php

			}
			?>
		</div>

		<?php if(apply_filters('woocommerce_shipping_calculator_enable_city', false)): ?>

			<div class="field" id="calc_shipping_city_field">
				<input type="text" class="input-text" value="<?php echo esc_attr(WC()->customer->get_shipping_city()); ?>" placeholder="<?php esc_attr_e('City', 'chap'); ?>" name="calc_shipping_city" id="calc_shipping_city" />
			</div>

		<?php endif; ?>

		<?php if(apply_filters('woocommerce_shipping_calculator_enable_postcode', true)): ?>

			<div class="field" id="calc_shipping_postcode_field">
				<input type="text" class="input-text" value="<?php echo esc_attr(WC()->customer->get_shipping_postcode()); ?>" placeholder="<?php esc_attr_e('Postcode / ZIP', 'chap'); ?>" name="calc_shipping_postcode" id="calc_shipping_postcode" />
			</div>

		<?php endif; ?>

		<div class="field">
			<button type="submit" name="calc_shipping" value="1" class="ui primary labeled icon button">
				<i class="refresh icon"></i>
				<?php esc_html_e('Update totals', 'chap'); ?>
			</button>
		</div>

		<?php wp_nonce_field('woocommerce-cart'); ?>
	</section>
</form>

<?php do_action('woocommerce_after_shipping_calculator'); ?>

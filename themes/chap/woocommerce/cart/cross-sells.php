<?php
/**
 * Cross-sells
 *
 * This is Chap theme's version of a WooCommerce template, overriding the original.
 * To override this in a Chap child theme, copy it to chap-child/woocommerce/cart/cross-sells.php.
 *
 * @version 3.0.0
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

if(version_compare(WC_VERSION, '3.0', '<')) {
	wc_get_template('backwards-compatibility/cart/cross-sells-1.6.4.php', [
		'orderby' => $orderby,
		'columns' => $columns,
		'posts_per_page' => $posts_per_page,
	]);
	return;
}

if(isset($cross_sells) && $cross_sells): ?>

	<div class="cross-sells">

		<div class="ui hidden clearing divider"></div>

		<h2 class="ui header"><?php esc_html_e('You may be interested in&hellip;', 'chap'); ?></h2>

		<?php
		woocommerce_product_loop_start();

		foreach($cross_sells as $cross_sell):

			$post_object = get_post($cross_sell->get_id());

			setup_postdata($GLOBALS['post'] =& $post_object);

			wc_get_template_part('content', 'product');

		endforeach;

		woocommerce_product_loop_end();
		?>

	</div>

<?php
endif;

wp_reset_postdata();

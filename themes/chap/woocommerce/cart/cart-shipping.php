<?php
/**
 * Shipping Methods Display
 *
 * This is Chap theme's version of a WooCommerce template, overriding the original.
 * To override this in a Chap child theme, copy it to chap-child/woocommerce/cart/cart-shipping.php.
 *
 * @version 3.2.0
 */
if(!defined('ABSPATH')) {
	exit;
}

if(version_compare(WC_VERSION, '3.1', '<')) {
	wc_get_template('backwards-compatibility/cart/cart-shipping-2.5.0.php', [
		'package_name' => $package_name,
		'available_methods' => $available_methods,
		'chosen_method' => $chosen_method,
		'index' => $index,
		'show_package_details' => $show_package_details,
	]);
	return;
}
?>
<tr class="shipping">
	<th><?php echo wp_kses_post($package_name); ?></th>
	<td data-title="<?php echo esc_attr($package_name); ?>">
		<?php if(1 < count($available_methods)): ?>
			<div class="ui list" id="shipping_method">
				<?php foreach($available_methods as $method): ?>
					<div class="item">
						<div class="ui radio checkbox">
						<?php
							printf(
								'<input type="radio" name="shipping_method[%1$d]" data-index="%1$d" id="shipping_method_%1$d_%2$s" value="%3$s" class="shipping_method" %4$s />
								<label for="shipping_method_%1$d_%2$s">%5$s</label>',
								$index,
								sanitize_title($method->id),
								esc_attr($method->id),
								checked($method->id, $chosen_method, false),
								wc_cart_totals_shipping_method_label($method)
							);
						?>
						<?php do_action('woocommerce_after_shipping_rate', $method, $index); ?>
						</div>
					</div>
				<?php endforeach; ?>
			</div>
		<?php elseif(count($available_methods) === 1): ?>
			<?php
				$method = current($available_methods);
				printf(
					'%3$s <input type="hidden" name="shipping_method[%1$d]" data-index="%1$d" id="shipping_method_%1$d" value="%2$s" class="shipping_method" />',
					$index,
					esc_attr($method->id),
					wc_cart_totals_shipping_method_label($method)
				);
				do_action('woocommerce_after_shipping_rate', $method, $index);
			?>
		<?php elseif(WC()->customer->has_calculated_shipping()): ?>
			<?php echo apply_filters(
				is_cart() ? 'woocommerce_cart_no_shipping_available_html' : 'woocommerce_no_shipping_available_html',
				wpautop(esc_html__('There are no shipping methods available. Please ensure that your address has been entered correctly, or contact us if you need any help.', 'chap'))
			); ?>
		<?php elseif(!is_cart()): ?>
			<?php echo wpautop(esc_html__('Enter your full address to see shipping costs.', 'chap')); ?>
		<?php endif; ?>

		<?php if($show_package_details): ?>
			<?php echo '<p class="woocommerce-shipping-contents"><small>' . esc_html($package_details) . '</small></p>'; ?>
		<?php endif; ?>

		<?php if(!empty($show_shipping_calculator)): ?>
			<?php woocommerce_shipping_calculator(); ?>
		<?php endif; ?>
	</td>
</tr>

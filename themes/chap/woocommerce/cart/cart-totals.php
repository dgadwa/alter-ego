<?php
/**
 * Cart totals
 *
 * This is Chap theme's version of a WooCommerce template, overriding the original.
 * To override this in a Chap child theme, copy it to chap-child/woocommerce/cart/cart-totals.php.
 *
 * @version 2.3.6
 */

if(!defined('ABSPATH')) {
	exit;
}

$html_cart_totals_class = WC()->customer->has_calculated_shipping() ? ' calculated_shipping' : '';
?>
<div class="cart_totals<?php echo $html_cart_totals_class; ?>">

	<?php do_action('woocommerce_before_cart_totals'); ?>

	<div class="ui hidden clearing divider"></div>

	<h2 class="ui header"><?php esc_html_e('Cart totals', 'chap'); ?></h2>

	<table class="ui celled large padded table shop_table">

		<tr class="cart-subtotal">
			<th><?php esc_html_e('Subtotal', 'chap'); ?></th>
			<td data-title="<?php esc_attr_e('Subtotal', 'chap'); ?>"><?php wc_cart_totals_subtotal_html(); ?></td>
		</tr>

		<?php foreach(WC()->cart->get_coupons() as $code => $coupon): ?>
			<tr class="cart-discount coupon-<?php echo esc_attr(sanitize_title($code)); ?>">
				<th><?php wc_cart_totals_coupon_label($coupon); ?></th>
				<td data-title="<?php wc_cart_totals_coupon_label($coupon); ?>"><?php wc_cart_totals_coupon_html($coupon); ?></td>
			</tr>
		<?php endforeach; ?>

		<?php if(WC()->cart->needs_shipping() && WC()->cart->show_shipping()): ?>

			<?php do_action('woocommerce_cart_totals_before_shipping'); ?>

			<?php wc_cart_totals_shipping_html(); ?>

			<?php do_action('woocommerce_cart_totals_after_shipping'); ?>

		<?php elseif(WC()->cart->needs_shipping() && get_option('woocommerce_enable_shipping_calc') === 'yes'): ?>

			<tr class="shipping">
				<th><?php esc_html_e('Shipping', 'chap'); ?></th>
				<td data-title="<?php esc_attr_e('Shipping', 'chap'); ?>"><?php woocommerce_shipping_calculator(); ?></td>
			</tr>

		<?php endif; ?>

		<?php foreach(WC()->cart->get_fees() as $fee): ?>
			<tr class="fee">
				<th><?php echo esc_html($fee->name); ?></th>
				<td data-title="<?php echo esc_attr($fee->name); ?>"><?php wc_cart_totals_fee_html($fee); ?></td>
			</tr>
		<?php endforeach; ?>

		<?php
		if(wc_tax_enabled() && 'excl' === WC()->cart->tax_display_cart):
			$taxable_address = WC()->customer->get_taxable_address();
			$estimated_text  = WC()->customer->is_customer_outside_base() && ! WC()->customer->has_calculated_shipping()
				? sprintf(
					' <small>(' . esc_html__('estimated for %s', 'chap') . ')</small>',
					WC()->countries->estimated_for_prefix($taxable_address[0]) . WC()->countries->countries[$taxable_address[0]]
				) : '';

			if(get_option('woocommerce_tax_total_display') === 'itemized'): ?>
				<?php foreach(WC()->cart->get_tax_totals() as $code => $tax): ?>
					<tr class="tax-rate tax-rate-<?php echo sanitize_title($code); ?>">
						<th><?php echo esc_html($tax->label) . $estimated_text; ?></th>
						<td data-title="<?php echo esc_attr($tax->label); ?>"><?php echo wp_kses_post($tax->formatted_amount); ?></td>
					</tr>
				<?php endforeach; ?>
			<?php else: ?>
				<tr class="tax-total">
					<th><?php echo esc_html(WC()->countries->tax_or_vat()) . $estimated_text; ?></th>
					<td data-title="<?php echo esc_attr(WC()->countries->tax_or_vat()); ?>"><?php wc_cart_totals_taxes_total_html(); ?></td>
				</tr>
			<?php endif; ?>
		<?php endif; ?>

		<?php do_action('woocommerce_cart_totals_before_order_total'); ?>

		<tr class="order-total">
			<th><?php esc_html_e('Total', 'chap'); ?></th>
			<td data-title="<?php esc_attr_e('Total', 'chap'); ?>"><?php wc_cart_totals_order_total_html(); ?></td>
		</tr>

		<?php do_action('woocommerce_cart_totals_after_order_total'); ?>

	</table>

	<div class="wc-proceed-to-checkout">
		<?php do_action('woocommerce_proceed_to_checkout'); ?>
	</div>

	<?php do_action('woocommerce_after_cart_totals'); ?>

</div>

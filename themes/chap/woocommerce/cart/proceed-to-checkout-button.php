<?php
/**
 * Proceed to checkout button
 *
 * Contains the markup for the proceed to checkout button on the cart.
 *
 * This is Chap theme's version of a WooCommerce template, overriding the original.
 * To override this in a Chap child theme, copy it to chap-child/woocommerce/cart/proceed-to-checkout-button.php.
 *
 * @version 2.4.0
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}
?>

<a href="<?php echo esc_url(wc_get_checkout_url()); ?>" class="ui animated positive checkout-button button alt wc-forward">
	<div class="visible content">
		<?php echo esc_html__('Proceed to checkout', 'chap'); ?>
	</div>
	<div class="hidden content">
		<i class="right arrow icon"></i>
	</div>
</a>

<?php
/**
 * Product quantity inputs
 *
 * This is Chap theme's version of a WooCommerce template, overriding the original.
 * To override this in a Chap child theme, copy it to chap-child/woocommerce/global/quantity-input.php.
 *
 * @version 3.3.0
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}
if($max_value && $min_value === $max_value) {
	?>
	<div class="quantity hidden">
		<input type="hidden" id="<?php echo esc_attr($input_id); ?>" class="qty" name="<?php echo esc_attr($input_name); ?>" value="<?php echo esc_attr($min_value); ?>" />
	</div>
	<?php
} else {
	?>
	<div class="ui small action quantity input">
		<div style="width:100%"></div>
		<label class="screen-reader-text" for="<?php echo esc_attr($input_id); ?>"><?php esc_html_e('Quantity', 'chap'); ?></label>
		<input type="text" step="<?php echo esc_attr($step); ?>" min="<?php echo esc_attr($min_value); ?>" max="<?php echo esc_attr(0 < $max_value ? $max_value : ''); ?>" name="<?php echo esc_attr($input_name); ?>" value="<?php echo esc_attr($input_value); ?>" title="<?php echo esc_attr_x('Qty', 'Product quantity input tooltip', 'chap'); ?>" class="input-text qty text" size="4" pattern="<?php echo esc_attr($pattern); ?>" inputmode="<?php echo esc_attr($inputmode); ?>" aria-labelledby="<?php echo !empty($args['product_name']) ? sprintf(esc_attr__('%s quantity', 'chap'), $args['product_name']) : ''; ?>" />
		<button class="ui icon button increment_quantity"><i class="plus icon"></i></button>
		<button class="ui icon button decrement_quantity"><i class="minus icon"></i></button>
	</div>
	<?php
}
?>

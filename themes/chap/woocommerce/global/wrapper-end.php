<?php
/**
 * Content wrappers
 *
 * This is Chap theme's version of a WooCommerce template, overriding the original.
 * To override this in a Chap child theme, copy it to chap-child/woocommerce/global/wrapper-end.php.
 *
 * @version 3.3.0
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}
?>
	</div>
</div>

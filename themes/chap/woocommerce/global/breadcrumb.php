<?php
/**
 * Shop breadcrumb
 *
 * This is Chap theme's version of a WooCommerce template, overriding the original.
 * To override this in a Chap child theme, copy it to chap-child/woocommerce/global/breadcrumb.php.
 *
 * @version 2.3.0
 */

if(!defined('ABSPATH')) {
	exit;
}

if(!empty($breadcrumb)) {

	echo wp_kses_post($wrap_before);

	foreach($breadcrumb as $key => $crumb) {

		echo wp_kses_post($before);

		if(!empty($crumb[1]) && sizeof($breadcrumb) !== $key + 1) {
			echo '<a href="' . esc_url($crumb[1]) . '">' . esc_html($crumb[0]) . '</a>';
		} else {
			echo esc_html($crumb[0]);
		}

		echo wp_kses_post($after);

		if(sizeof($breadcrumb) !== $key + 1) {
			echo wp_kses_post($delimiter);
		}

	}

	echo wp_kses_post($wrap_after);

}

<?php
/**
 * Sidebar
 *
 * This is Chap theme's version of a WooCommerce template, overriding the original.
 * To override this in a Chap child theme, copy it to chap-child/woocommerce/global/sidebar.php.
 *
 * @version 1.6.4
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

get_sidebar('shop');

<?php
/**
 * Display single product reviews (comments)
 *
 * This is Chap theme's version of a WooCommerce template, overriding the original.
 * To override this in a Chap child theme, copy it to chap-child/woocommerce/single-product-reviews.php.
 *
 * @version 3.2.0
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

global $product;

if(!comments_open()) {
	return;
}

?>
<div id="reviews">
	<div class="ui threaded reviews comments" id="comments">
		<h2 class="ui header">
		<?php
		if(get_option('woocommerce_enable_review_rating') === 'yes' && ($count = $product->get_review_count())) {
			printf(_n('%1$s review for %2$s', '%1$s reviews for %2$s', $count, 'chap'), $count, get_the_title());
		} else {
			esc_html_e('Reviews', 'chap');
		}
		?>
		</h2>

		<?php if(have_comments()): ?>

			<?php
			wp_list_comments(apply_filters('woocommerce_product_review_list_args', [
				'callback' => 'woocommerce_comments',
			]));
			?>

			<?php if(get_comment_pages_count() > 1 && get_option('page_comments')): ?>
				<div class="ui hidden divider"></div>
				<div class="ui centered grid">
					<div class="center aligned column">
						<div class="ui pagination menu">
							<?php
							echo str_replace(
								[
									"<a class='",
									'<a class="',
									"<span class='",
								],
								[
									"<a class='item ",
									'<a class="item ',
									"<span class='active item ",
								],
								paginate_comments_links(
									apply_filters(
										'woocommerce_comment_pagination_args',
										[
											'prev_text' => '&larr;',
											'next_text' => '&rarr;',
											'type'      => 'plain',
											'echo'      => false,
										]
									)
								)
							);
							?>
						</div>
					</div>
				</div>
			<?php endif; ?>

		<?php else: ?>
			<p class="woocommerce-noreviews"><?php esc_html_e('There are no reviews yet.', 'chap'); ?></p>
			<div class="ui hidden section divider"></div>
		<?php endif; ?>
	</div>

	<?php if(get_option('woocommerce_review_rating_verification_required') === 'no' || wc_customer_bought_product('', get_current_user_id(), $product->id)): ?>

		<div id="review_form_wrapper">
			<div id="review_form">
				<?php
					$commenter = wp_get_current_commenter();

					$comment_form = [
						'class_form'           => 'ui reply form comment-form',
						'class_submit'         => 'ui primary labeled submit icon button',
						'title_reply_before'   => '<h3 id="reply-title" class="ui huge header comment-reply-title">',
						'title_reply'          => have_comments() ? esc_html__('Add a review', 'chap') : sprintf(esc_html__('Be the first to review &ldquo;%s&rdquo;', 'chap'), get_the_title()),
						'title_reply_to'       => esc_html__('Leave a reply to %s', 'chap'),
						'comment_notes_before' => '',
						'comment_notes_after'  => '',
						'fields'               => [
							'before' => '<div class="two fields">',
							'author' => '<div class="comment-form-author field">' . '<label for="author">' . esc_html__('Name', 'chap') . ' <span class="required">*</span></label> ' .
										'<input id="author" name="author" type="text" value="' . esc_attr($commenter['comment_author']) . '" size="30" aria-required="true" /></div>',
							'email' => '<div class="comment-form-email field"><label for="email">' . esc_html__('Email', 'chap') . ' <span class="required">*</span></label> ' .
										'<input id="email" name="email" type="text" value="' . esc_attr($commenter['comment_author_email']) . '" size="30" aria-required="true" /></div>',
							'after' => '</div>',
						],
						'label_submit'  => esc_html__('Submit', 'chap'),
						'logged_in_as'  => '',
						'comment_field' => '',
					];

					if($account_page_url = wc_get_page_permalink('myaccount')) {
						$comment_form['must_log_in'] = '<p class="must-log-in">' . sprintf(esc_html__('You must be %1$slogged in%2$s to post a review.', 'chap'), '<a href="' . esc_url($account_page_url) . '">', '</a>') . '</p>';
					}

					if(get_option('woocommerce_enable_review_rating') === 'yes') {
						$comment_form['comment_field'] = '<div class="ui comment-form-rating form"><div class="field"><label for="rating">' . esc_html__('Your rating', 'chap') . '</label><select name="rating" id="rating">
							<option value="">' . esc_html__('Rate&hellip;', 'chap') . '</option>
							<option value="5">' . esc_html__('Perfect', 'chap') . '</option>
							<option value="4">' . esc_html__('Good', 'chap') . '</option>
							<option value="3">' . esc_html__('Average', 'chap') . '</option>
							<option value="2">' . esc_html__('Not that bad', 'chap') . '</option>
							<option value="1">' . esc_html__('Very poor', 'chap') . '</option>
						</select></div></div>';
					}

					$comment_form['comment_field'] .= '<div class="comment-form-comment field"><label for="comment">' . esc_html__('Your review', 'chap') . '</label><textarea id="comment" name="comment" cols="45" rows="8" aria-required="true"></textarea></div>';

					comment_form(apply_filters('woocommerce_product_review_comment_form_args', $comment_form));
				?>
			</div>
		</div>

	<?php else: ?>

		<p class="woocommerce-verification-required"><?php esc_html_e('Only logged in customers who have purchased this product may leave a review.', 'chap'); ?></p>

	<?php endif; ?>

	<div class="clear"></div>
</div>

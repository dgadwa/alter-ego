<?php
/**
 * The template for displaying product content within loops
 *
 * This is Chap theme's version of a WooCommerce template, overriding the original.
 * To override this in a Chap child theme, copy it to chap-child/woocommerce/content-product.php.
 *
 * @version 3.0.0
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

global $product;

if(empty($product) || !$product->is_visible()) {
	return;
}

?>
<div <?php post_class(apply_filters('chap_woocommerce_product_card_classes', ['ui', 'card'], $product)); ?>>
	<?php
	/**
	 * woocommerce_before_shop_loop_item hook.
	 *
	 * @unhooked woocommerce_template_loop_product_link_open - 10
	 *
	 * @hooked Chap\WooCommerce\woocommerce_template_loop_product_link_open - 10
	 */
	do_action('woocommerce_before_shop_loop_item');

	/**
	 * woocommerce_before_shop_loop_item_title hook.
	 *
	 * @unhooked woocommerce_show_product_loop_sale_flash - 10
	 * @unhooked woocommerce_template_loop_product_thumbnail - 10
	 *
	 * @hooked Chap\WooCommerce\woocommerce_before_shop_loop_item_title - 10
	 */
	do_action('woocommerce_before_shop_loop_item_title');

	/**
	 * woocommerce_shop_loop_item_title hook.
	 *
	 * @unhooked woocommerce_template_loop_product_title - 10
	 */
	do_action('woocommerce_shop_loop_item_title');

	/**
	 * woocommerce_after_shop_loop_item_title hook.
	 *
	 * @unhooked woocommerce_template_loop_rating - 5
	 * @unhooked woocommerce_template_loop_price - 10
	 *
	 * @hooked Chap\WooCommerce\woocommerce_after_shop_loop_item_title - 10
	 */
	do_action('woocommerce_after_shop_loop_item_title');

	/**
	 * woocommerce_after_shop_loop_item hook.
	 *
	 * @unhooked woocommerce_template_loop_product_link_close - 5
	 *
	 * @hooked woocommerce_template_loop_add_to_cart - 10
	 */
	do_action('woocommerce_after_shop_loop_item');
	?>
</div>

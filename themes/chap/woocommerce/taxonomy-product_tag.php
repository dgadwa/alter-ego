<?php
/**
 * The Template for displaying products in a product tag. Simply includes the archive template.
 *
 * This is Chap theme's version of a WooCommerce template, overriding the original.
 * To override this in a Chap child theme, copy it to chap-child/woocommerce/taxonomy-product_tag.php.
 *
 * @version 1.6.4
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

wc_get_template('archive-product.php');

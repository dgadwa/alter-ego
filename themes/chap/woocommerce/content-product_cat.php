<?php
/**
 * The template for displaying product category thumbnails within loops
 *
 * This is Chap theme's version of a WooCommerce template, overriding the original.
 * To override this in a Chap child theme, copy it to chap-child/woocommerce/content-product_cat.php.
 *
 * @version 2.6.1
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}
?>
<a <?php wc_product_cat_class(apply_filters('chap_woocommerce_category_card_classes', ['ui', 'card'], $category), $category); ?> href="<?php echo Chap\Helpers\maybe_amp(get_term_link($category->term_id)); ?>">
	<?php
	/**
	 * woocommerce_before_subcategory hook.
	 *
	 * @unhooked woocommerce_template_loop_category_link_open - 10
	 */
	do_action('woocommerce_before_subcategory', $category);

	/**
	 * woocommerce_before_subcategory_title hook.
	 *
	 * @unhooked woocommerce_subcategory_thumbnail - 10
	 *
	 * @hooked Chap\WooCommerce\woocommerce_before_subcategory_title - 10
	 */
	do_action('woocommerce_before_subcategory_title', $category);

	/**
	 * woocommerce_shop_loop_subcategory_title hook.
	 *
	 * @unhooked woocommerce_template_loop_category_title - 10
	 *
	 * @hooked Chap\WooCommerce\woocommerce_shop_loop_subcategory_title - 10
	 */
	do_action('woocommerce_shop_loop_subcategory_title', $category);

	/**
	 * woocommerce_after_subcategory_title hook.
	 */
	do_action('woocommerce_after_subcategory_title', $category);

	/**
	 * woocommerce_after_subcategory hook.
	 *
	 * @unhooked woocommerce_template_loop_category_link_close - 10
	 */
	do_action('woocommerce_after_subcategory', $category); ?>
</a>

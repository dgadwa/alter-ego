<?php
/**
 * Show messages
 *
 * This is Chap theme's version of a WooCommerce template, overriding the original.
 * To override this in a Chap child theme, copy it to chap-child/woocommerce/notices/notice.php.
 *
 * @version 1.6.4
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

if(!$messages) {
	return;
}

?>

<?php foreach($messages as $message): ?>
	<div class="<?php echo apply_filters('chap_woocommerce_notice_classes', 'ui icon info message', $message); ?>"><?php echo wp_kses_post($message); ?></div>
<?php endforeach; ?>

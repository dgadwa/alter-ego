<?php
/**
 * Show messages
 *
 * This is Chap theme's version of a WooCommerce template, overriding the original.
 * To override this in a Chap child theme, copy it to chap-child/woocommerce/notices/success.php.
 *
 * @version 3.3.0
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

if(!$messages) {
	return;
}

?>

<?php foreach($messages as $message): ?>
	<div class="<?php echo apply_filters('chap_woocommerce_notice_classes', 'ui positive message', $message); ?>" role="alert"><?php echo wp_kses_post($message); ?></div>
<?php endforeach; ?>

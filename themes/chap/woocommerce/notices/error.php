<?php
/**
 * Show error messages
 *
 * This is Chap theme's version of a WooCommerce template, overriding the original.
 * To override this in a Chap child theme, copy it to chap-child/woocommerce/notices/error.php.
 *
 * @version 3.3.0
 */

if(!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

if(!$messages) {
	return;
}

?>
<div class="<?php echo apply_filters('chap_woocommerce_notice_classes', 'ui negative message', $messages); ?>" role="alert">
	<ul class="ui bulletless list">
	<?php foreach($messages as $message): ?>
		<li><?php echo wp_kses_post($message); ?></li>
	<?php endforeach; ?>
	</ul>
</div>

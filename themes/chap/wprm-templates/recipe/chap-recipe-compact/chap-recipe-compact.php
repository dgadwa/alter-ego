<?php
/**
 * Compact chap recipe template for the WP Recipe Maker plugin.
 *
 * @link https://wordpress.org/plugins/wp-recipe-maker
 */
?>
<?php do_action('chap_render_recipe_open', $recipe); ?>

	<?php do_action('chap_render_recipe_title', $recipe); ?>

	<?php do_action('chap_render_recipe_card', $recipe); ?>

	<?php do_action('chap_render_recipe_summary', $recipe); ?>

	<div class="ui hidden clearing section divider"></div>

	<div class="ui two column stackable grid">
		<div class="column">
			<?php do_action('chap_render_recipe_ingredients', $recipe, false); ?>
		</div>
		<div class="middle aligned center aligned column">
			<?php do_action('chap_render_recipe_statistics', $recipe, 'tiny', 'small'); ?>
		</div>
	</div>

	<?php do_action('chap_render_recipe_instructions', $recipe); ?>

	<?php do_action('chap_render_recipe_notes', $recipe); ?>

	<?php echo WPRM_Template_Helper::nutrition_label($recipe->id()); ?>

<?php do_action('chap_render_recipe_close', $recipe); ?>

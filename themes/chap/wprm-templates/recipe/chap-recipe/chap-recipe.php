<?php
/**
 * Chap recipe template for the WP Recipe Maker plugin.
 *
 * @link https://wordpress.org/plugins/wp-recipe-maker
 */
?>
<?php do_action('chap_render_recipe_open', $recipe); ?>

	<?php do_action('chap_render_recipe_title', $recipe); ?>

	<?php do_action('chap_render_recipe_card', $recipe); ?>

	<?php do_action('chap_render_recipe_summary', $recipe); ?>

	<div class="ui hidden clearing section divider"></div>

	<?php do_action('chap_render_recipe_statistics', $recipe, 'small', 'medium'); ?>

	<div class="ui hidden section divider"></div>

	<?php do_action('chap_render_recipe_ingredients', $recipe, true); ?>

	<?php do_action('chap_render_recipe_instructions', $recipe); ?>

	<?php do_action('chap_render_recipe_notes', $recipe); ?>

	<?php echo WPRM_Template_Helper::nutrition_label($recipe->id()); ?>

<?php do_action('chap_render_recipe_close', $recipe); ?>

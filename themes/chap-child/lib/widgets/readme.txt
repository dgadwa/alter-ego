Widgets

Copy any widget-*.php file from chap/lib/widgets/ here to override it and modify to your liking.
Any php file with "widget-" prefix will be automatically loaded.

It's possible to modify any widget's output by creating a file with a filter:

	namespace Chap\Widgets;
	function custom_widget_filter($widget_output, $props, $sidebar_id, $widget_type) {
		// Manipulate $widget_output here.
		return $widget_output;
	}
	add_filter('chap_widget_WIDGET_ID', __NAMESPACE__ . '\\custom_widget_filter', 10, 4);


Variables passed to the widget filter:
	$widget_output contains the HTML for rendering the widget. String replacements or DOM manipulation can be done.
	$props includes partial Semantic UI classes based on the widget location, which may be used with the widget.
		$props = [
			'inverted'  => 'default',         // Will be set to 'inverted' if the current widget area is inverted.
			'container' => 'bulletless list', // Will be set to a list class suitable for the current widget area.
			'direction' => 'vertical',        // Will be set to horizontal in header and menu widget areas.
		];
	$sidebar_id contains the current widget area ID.
		Default widget areas: sidebar-header, sidebar-menu, sidebar-primary, sidebar-secondary, sidebar-footer
	$widget_type contains the ID of the current widget.


Determining any widget's ID:

	The following code displays a widget's ID before it's outputted, allowing to create a custom filter for it using the above code:

	add_filter('widget_output', function($widget_output, $widget_type, $widget_id, $sidebar_id){
		var_dump($widget_type);
		return $widget_output;
	}, 10, 4);

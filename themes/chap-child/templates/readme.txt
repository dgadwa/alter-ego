Templates

Copy any template file from chap/templates/ here to override it and modify to your liking.

Custom header templates can be added: header-mycustomheader.php
Custom slide templates can be added for post types: slide-post_type.php

Semantic UI

Files in this folder can be used to manually add variables and overrides to Semantic UI.

This is a legacy feature, Chap version 1.12 added a more convenient way to make
changes to Semantic UI in the WordPress administration dashboard, saving changes
as options in the database instead of files (Admin dashboard -> Chap Theme -> SUI).

Read more about Semantic UI theming: https://chap.website/semantic-ui-theming/

This folder can be deleted if it's not needed.



.ui.inverted.segment .segment
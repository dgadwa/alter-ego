<?php

namespace Chap\Child {

	/**
	 * Load the child theme assets.
	 */
	function assets() {
		wp_enqueue_style('chap-child', get_stylesheet_directory_uri() . '/style.css', false, wp_get_theme()->get('Version'));
		wp_enqueue_script('chap-child/js', get_stylesheet_directory_uri() . '/scripts.js', ['chap/js'], wp_get_theme()->get('Version'), true);
	}
	add_action('wp_enqueue_scripts', __NAMESPACE__ . '\\assets', 150);

}

namespace Chap\TemplateFunctions {

	/**
	 * Override any template function here.
	 */

}
